/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/lightOS.hpp>
using namespace lightOS;
using namespace std;

using libkernel::message;

bool lightOS::registerFilesystem(	libkernel::message_port &Port,
									const char *name)
{
	libkernel::shared_memory shm(sizeof(register_fs_info));
	register_fs_info *info = shm.address<register_fs_info>();
	strncpy(info->name, name, MAX_FILESYSTEM_NAME);
	
	message msg(libkernel::message_port::vfs, libkernel::message::vfs_register_fs, 0, shm, libkernel::shared_memory::transfer_ownership);
	Port.send(msg);
	message reply(libkernel::message_port::vfs, libkernel::message::vfs_register_fs);
	Port.wait(reply);
	
	return static_cast<bool>(reply.param1());
}
void lightOS::waitForFilesystemRegistration(libkernel::message_port &Port,
											const char *name)
{
	libkernel::shared_memory shm(sizeof(register_fs_info));
	register_fs_info *info = shm.address<register_fs_info>();
	strncpy(info->name, name, MAX_FILESYSTEM_NAME);
	
	message msg(libkernel::message_port::vfs, libkernel::message::vfs_wait_fs_registration, 0, shm, libkernel::shared_memory::transfer_ownership);
	Port.send(msg);
	message reply(libkernel::message_port::vfs, libkernel::message::vfs_wait_fs_registration);
	
	Port.wait(reply);
}
bool lightOS::mount(libkernel::message_port &Port,
					const char *name,
					const char *fs,
					const char *device)
{
	libkernel::shared_memory shm(sizeof(mount_fs_info));
	mount_fs_info *info = shm.address<mount_fs_info>();
	strncpy(info->name, name, MAX_PATH);
	strncpy(info->filesystem, fs, MAX_FILESYSTEM_NAME);
	strncpy(info->device, device, MAX_PATH);
	
	message msg(libkernel::message_port::vfs, libkernel::message::vfs_mount, 0, shm, libkernel::shared_memory::transfer_ownership);
	Port.send(msg);
	message reply(libkernel::message_port::vfs, libkernel::message::vfs_mount);
	Port.wait(reply);
	return static_cast<bool>(reply.param1());
}
pair<unsigned long, string> lightOS::getFilesystemFromName(	libkernel::message_port &Port,
															const char *name)
{
	libkernel::shared_memory shm(sizeof(file::name_info));
	file::name_info *info = shm.address<file::name_info>();
	strncpy(info->name, name, MAX_PATH);
	
	message msg(libkernel::message_port::vfs, libkernel::message::vfs_find_fs, 0, shm, libkernel::shared_memory::transfer_ownership);
	Port.send(msg);
	message reply(libkernel::message_port::vfs, libkernel::message::vfs_find_fs);
	Port.wait(reply);
	if (reply.attribute() == libkernel::shared_memory::transfer_ownership)
	{
		libkernel::shared_memory shared = reply.get_shared_memory();
		file::name_info *tmp = shared.address<file::name_info>();
		return pair<unsigned long, string>(reply.param1(), string(tmp->name));
	}
	return pair<unsigned long, string>(reply.param1(), string(&name[reply.param2()]));
}
void lightOS::getFilesystemInfo(libkernel::message_port &Port,
								const char *name,
								size_t &size,
								size_t &free)
{
	pair<unsigned long, string> result = getFilesystemFromName(Port, name);
	if (result.first == 0)
	{
		size = 0;
		free = 0;
	}
	else
	{
		message request(result.first, libkernel::message::fs_get_info);
		Port.send(request);
		message reply(result.first, libkernel::message::fs_get_info);
		Port.wait(reply);
		size = reply.param1();
		free = reply.param2();
	}
}
bool lightOS::setWorkingDirectory(	libkernel::message_port &Port,
									const char *path,
									unsigned long pid)
{
	libkernel::shared_memory shm(sizeof(file::name_info));
	file::name_info *info = shm.address<file::name_info>();
	strncpy(info->name, path, MAX_PATH);
	
	message msg(libkernel::message_port::vfs, libkernel::message::vfs_set_working_dir, pid, shm, libkernel::shared_memory::transfer_ownership);
	Port.send(msg);
	message reply(libkernel::message_port::vfs, libkernel::message::vfs_set_working_dir);
	Port.wait(reply);
	return static_cast<bool>(reply.param1());
}
string lightOS::getWorkingDirectory(libkernel::message_port &Port,
									unsigned long pid)
{
	message msg(libkernel::message_port::vfs, libkernel::message::vfs_get_working_dir, pid);
	Port.send(msg);
	message reply(libkernel::message_port::vfs, libkernel::message::vfs_get_working_dir);
	Port.wait(reply);
	if (reply.attribute() == libkernel::shared_memory::transfer_ownership)
	{
		libkernel::shared_memory shm = reply.get_shared_memory();
		file::name_info *info = shm.address<file::name_info>();
		info->name[MAX_PATH] = '\0';
		return string(info->name);
	}
	return string();
}
bool lightOS::setConsoleName(	libkernel::message_port &Port,
								const string &filename,
								processId pid)
{
	libkernel::shared_memory shm(sizeof(file::name_info));
	file::name_info *info = shm.address<file::name_info>();
	strncpy(info->name,
			filename.c_str(),
			MAX_PATH);
	info->name[MAX_PATH] = '\0';
	message request(libkernel::message_port::vfs, libkernel::message::vfs_set_console, pid, shm, libkernel::shared_memory::transfer_ownership);
	Port.send(request);
	message reply(libkernel::message_port::vfs, libkernel::message::vfs_set_console);
	Port.get(reply);
	return static_cast<bool>(reply.param1());
}
string lightOS::getConsoleName(	libkernel::message_port &Port,
								processId pid)
{
	message request(libkernel::message_port::vfs, libkernel::message::vfs_get_console, pid);
	Port.send(request);
	message reply(libkernel::message_port::vfs, libkernel::message::vfs_get_console);
	Port.wait(reply);
	if (reply.attribute() == libkernel::shared_memory::transfer_ownership)
	{
		libkernel::shared_memory shm = reply.get_shared_memory();
		file::name_info *info = shm.address<file::name_info>();
		info->name[MAX_PATH] = '\0';
		return string(info->name);
	}
	return string();
}
vector<triple<string,string,string> > lightOS::getMountpoints(libkernel::message_port &Port)
{
	vector<triple<string,string,string> > result;
	message msg(libkernel::message_port::vfs, libkernel::message::vfs_get_mount_count);
	Port.send(msg);
	message reply(libkernel::message_port::vfs, libkernel::message::vfs_get_mount_count);
	Port.wait(reply);
	result.reserve(reply.param1());
	for (size_t i=0;i < reply.param1();i++)
	{
		message msg0(libkernel::message_port::vfs, libkernel::message::vfs_get_mount_info, i);
		Port.send(msg0);
		message msg1(libkernel::message_port::vfs, libkernel::message::vfs_get_mount_info);
		Port.wait(msg1);
		
		if (msg1.attribute() == libkernel::shared_memory::transfer_ownership)
		{
			libkernel::shared_memory shm = msg1.get_shared_memory();
			mount_fs_info *info = shm.address<mount_fs_info>();
			result.push_back(triple<string,string,string>(info->device, info->name, info->filesystem));
		}
	}
	return result;
}
string lightOS::file::get_name() const
{
	message msg(mFilesystemPort, libkernel::message::fs_get_filename);
	mPort.send(msg);
	mPort.wait(msg);
	if (msg.attribute() != libkernel::shared_memory::transfer_ownership)return string();
	libkernel::shared_memory shm = msg.get_shared_memory();
	return string(shm.address<char>());
}
vector<libkernel::port_id_t> lightOS::file::get_opener_list() const
{
	message msg(mFilesystemPort, libkernel::message::fs_get_opener_count);
	mPort.send(msg);
	mPort.wait(msg);
	
	vector<libkernel::port_id_t> List;
	for (size_t i = 0;i < msg.param1();i++)
	{
		message req(mFilesystemPort, libkernel::message::fs_enum_opener, i);
		mPort.send(req);
		message reply(mFilesystemPort, libkernel::message::fs_enum_opener);
		mPort.wait(reply);
		if (reply.param1() != 0)
			List.push_back(reply.param1());
	}
	return List;
}
bool lightOS::file::create(	const char *name,
							size_t type,
							unsigned long flags,
							size_t blocksize,
							size_t blockcount)
{
	pair<unsigned long, string> dest = getFilesystemFromName(mPort, name);
	if (dest.first == 0)return false;
	mFilesystemPort = dest.first;
	
	libkernel::shared_memory shm(sizeof(info));
	info *i = shm.address<info>();
	strncpy(i->name, dest.second.c_str(), MAX_PATH);
	i->type = type;
	i->blocksize = blocksize;
	i->blockcount = blockcount;
	
	message msg(mFilesystemPort, libkernel::message::fs_create_file, flags, shm, libkernel::shared_memory::transfer_ownership);
	mPort.send(msg);
	message reply(mFilesystemPort, libkernel::message::fs_create_file);
	mPort.wait(reply);
	
	if (static_cast<bool>(reply.param1()) == false)
	{
		mFilesystemPort = 0;
		return false;
	}
	
	mType = type;
	mBlocksize = blocksize;
	mBlockcount = blockcount;
	return true;
}
size_t lightOS::file::exists(	const char *name,
								libkernel::message_port &Port)
{
	pair<unsigned long, string> dest = getFilesystemFromName(Port, name);
	if (dest.first == 0)return lightOS::file::none;
	
	libkernel::shared_memory shm(sizeof(name_info));
	name_info *i = shm.address<name_info>();
	strncpy(i->name, dest.second.c_str(), MAX_PATH);
	
	message msg(dest.first, libkernel::message::fs_exists_file, 0, shm, libkernel::shared_memory::transfer_ownership);
	Port.send(msg);
	message reply(dest.first, libkernel::message::fs_exists_file);
	Port.wait(reply);
	
	return reply.param1();
}
bool lightOS::file::open(	const char *name,
							unsigned long flags)
{
	pair<unsigned long, string> dest = getFilesystemFromName(mPort, name);
	if (dest.first == 0)return false;
	mFilesystemPort = dest.first;
	
	libkernel::shared_memory shm(sizeof(info));
	info *i = shm.address<info>();
	strncpy(i->name, dest.second.c_str(), MAX_PATH);
	
	message msg(mFilesystemPort, libkernel::message::fs_open_file, flags, shm, libkernel::shared_memory::transfer_ownership);
	mPort.send(msg);
	message reply(mFilesystemPort, libkernel::message::fs_open_file);
	mPort.wait(reply);
	
	if (static_cast<bool>(reply.param1()) == false)
	{
		mFilesystemPort = 0;
		return false;
	}
	
	updateFileInfo();
	return true;
}
unsigned long file::execute(const char *name,
							libkernel::message_port &Port,
							unsigned long flags)
{
	size_t size = strlen(name);
	libkernel::shared_memory shm(size + 1);
	strcpy(shm.address<char>(), name);
	
	message msg(libkernel::message_port::init, libkernel::message::init_execute, flags, shm, libkernel::shared_memory::transfer_ownership);
	Port.send(msg);
	message reply(libkernel::message_port::init, libkernel::message::init_execute);
	Port.wait(reply);
	
	return reply.param1();
}
unsigned long file::write(libkernel::shared_memory &shm, unsigned long block)
{
	message msg(mFilesystemPort, libkernel::message::fs_write_file, block, shm, libkernel::shared_memory::transfer_ownership);
	mPort.send(msg);
	message reply(mFilesystemPort, libkernel::message::fs_write_file, block);
	mPort.wait(reply);
	
	return reply.param2();
}
libkernel::shared_memory file::read(unsigned long block, unsigned long size)
{
	message msg(mFilesystemPort, libkernel::message::fs_read_file, block, size, 0);
	mPort.send(msg);
	message reply(mFilesystemPort, libkernel::message::fs_read_file, block);
	mPort.wait(reply);
	
	if (reply.param2() == static_cast<size_t>(-1))mEof = true;
	
	if (reply.attribute() == libkernel::shared_memory::none)return libkernel::shared_memory();
	return libkernel::shared_memory(reply.get_shared_memory());
}
libkernel::shared_memory file::try_read(unsigned long block, unsigned long size)
{
	message reply(mFilesystemPort, libkernel::message::fs_read_file, block);
	mPort.peek(reply);
	
	if (reply.param2() == static_cast<size_t>(-1))mEof = true;
	
	if (reply.attribute() == libkernel::shared_memory::none)return libkernel::shared_memory();
	return libkernel::shared_memory(reply.get_shared_memory());
}
libkernel::shared_memory file::poll_read(unsigned long block)
{
	message msg(mFilesystemPort, libkernel::message::fs_read_file, block);
	if (mPort.peek(msg) == false)return libkernel::shared_memory();
	if (msg.attribute() == libkernel::shared_memory::none)return libkernel::shared_memory();
	return libkernel::shared_memory(msg.get_shared_memory());
}
bool file::close()
{
	if (mFilesystemPort == 0)return 0;
	
	message msg(mFilesystemPort, libkernel::message::fs_close_file);
	mPort.send(msg);
	message reply(mFilesystemPort, libkernel::message::fs_close_file);
	mPort.wait(reply);
	
	bool result = static_cast<bool>(reply.param1());
	if (result)mFilesystemPort = 0;
	return result;
}
const string file::iterator::operator *() const
{
	return *const_cast<iterator&>(*this);
}
string file::iterator::operator *()
{
	if (mFile->mFilesystemPort == 0 ||
		mFile->mType != file::directory)return string();
	
	message msg(mFile->mFilesystemPort, libkernel::message::fs_read_file, mIndex);
	mFile->mPort.send(msg);
	message reply(mFile->mFilesystemPort, libkernel::message::fs_read_file, mIndex);
	mFile->mPort.wait(reply);
	
	if (reply.attribute() != libkernel::shared_memory::transfer_ownership)return string();
	
	libkernel::shared_memory shm = reply.get_shared_memory();
	name_info *info = shm.address<name_info>();
	info->name[MAX_PATH] = '\0';
	
	return string(info->name);
}
void file::updateFileInfo() const
{
	if (mFilesystemPort == 0)return;
	
	message msg(mFilesystemPort, libkernel::message::fs_file_info);
	mPort.send(msg);
	message reply(mFilesystemPort, libkernel::message::fs_file_info);
	mPort.wait(reply);
	
	mType = reply.param1();
	mBlocksize = reply.param2();
	mBlockcount = reply.param3();
}
void file::received(libkernel::port_id_t PortID, message &msg)
{
	if (msg.type() == libkernel::message::fs_read_file)
	{
		if (msg.param2() == static_cast<size_t>(-1))mEof = true;
		else
		{
			libkernel::shared_memory shm(msg.get_shared_memory());
			this->callback_read(shm, block);
		}
	}
	else
	{
		cerr << "File::received(): received an unhandled message" << endl;
	}
}
void file::callback_read(	const libkernel::shared_memory &shm,
							size_t block)
{
	cerr << "file::callback_read(): unhandled read" << endl;
}

libkernel::port_id_t lightOS::create_pipe_name(std::string &name)
{
	name = "pipe://";
	
	// Create Port
	libkernel::message_port Port;
	
	// Create name
	char cname[63];
	ulltostr(Port.id(), cname, cname + 62, 10);
	name.append(cname);
	
	return Port.release();
}
pair<libkernel::port_id_t, libkernel::port_id_t> lightOS::create_pipe(std::string &name)
{
	// Create name & port
	libkernel::port_id_t PortID = create_pipe_name(name);
	libkernel::message_port Port(PortID);
	
	// Create pipe
	file Pipe(Port, false);
	Port.release();
	if (Pipe.create(name.c_str(), file::stream, access::write) == false)
		return pair<libkernel::port_id_t,libkernel::port_id_t>(0, 0);
	
	return Pipe.release();
}
