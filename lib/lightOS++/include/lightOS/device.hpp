/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_DEVICE_HPP
#define LIGHTOS_DEVICE_HPP

#include <libserver/keyboard.hpp>

/*! \addtogroup lightOS lightOS library */
/*@{*/

namespace lightOS
{
  /*! Data packet from the keyboard driver */
  struct keyboardData
  {
    libserver::key_t key;
    unsigned         ctrl  : 1;
    unsigned         alt   : 1;
    unsigned         altgr : 1;
  };

  /*! Data packet from the mouse driver */
  struct mouseData
  {
    unsigned right    : 1;
    unsigned middle   : 1;
    unsigned left     : 1;
    unsigned button4  : 1;
    unsigned button5  : 1;
    int x;
    int y;
    int z;
  };
}

/*@}*/

#endif
