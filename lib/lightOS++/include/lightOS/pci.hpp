/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_PCI_HPP
#define LIGHTOS_PCI_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstddef>
#include <cstdint>
#include <vector>
#include <utility>
#include <lightOS/lightOS.hpp>

// TODO: Move to libserver
namespace lightOS
{
	namespace pci
	{
		#define PCI_DEVICE_NONE 0xFFFF
		
		struct deviceAddr
		{
			uint16_t bus;
			uint16_t device;
			uint16_t function;
			
			inline deviceAddr(	uint16_t b = PCI_DEVICE_NONE,
								uint16_t d = PCI_DEVICE_NONE,
								uint16_t f = PCI_DEVICE_NONE)
				: bus(b), device(d), function(f){}
			operator bool() const
			{
				if (bus == PCI_DEVICE_NONE ||
					device == PCI_DEVICE_NONE ||
					function == PCI_DEVICE_NONE)
					return false;
				return true;
			}
		};
		
		/*! The pci header */
		struct deviceInfo
		{
			/*! Vendor identifier */
			uint16_t vendorId;
			/*! Device identifier */
			uint16_t deviceId;
			/*! Command */
			uint16_t command;
			/*! Status */
			uint16_t status;
			/*! Revision */
			uint8_t revision;
			/*! Programming-level interface */
			uint8_t interface;
			/*! Subclass code */
			uint8_t subclass;
			/*! Baseclass code */
			uint8_t baseclass;
			/*! \todo CLG */
			uint8_t clg;
			/*! Latency */
			uint8_t latency;
			/*! Header type */
			uint8_t header;
			/*! Build-in self-test */
			uint8_t bist;
			/*! The IRQ number, if any */
			uint8_t irq;
			
			uint64_t mm_space[6];
			uint64_t mm_space_size[6];
			uint32_t io_space[6];
			uint32_t io_space_size[6];
		}__attribute__((packed));
		
		namespace command
		{
			enum
			{
				enable_io				= 0x01,
				enable_mm				= 0x02,
				enable_busmaster		= 0x04
			};
		}
		
		std::vector<deviceAddr> enumDevices(libkernel::message_port &Port);
		deviceInfo getDeviceInfo(const deviceAddr &address, libkernel::message_port &Port);
		deviceAddr findDevice(size_t vendorId, size_t deviceId, libkernel::message_port &Port);
		bool enableBusmaster(const deviceAddr &address, libkernel::message_port &Port);
	}
}

/*@}*/

#endif
