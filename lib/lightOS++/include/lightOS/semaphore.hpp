/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_SEMAPHORE_HPP
#define LIGHTOS_SEMAPHORE_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <vector>
#include <lightOS/atom.hpp>
#include <lightOS/signal.hpp>

namespace lightOS
{
	template<bool userspace=true>
	class semaphore{};
	
	template<>
	class semaphore<true>
	{
		public:
			inline semaphore(long val = 1)
				: mAtom(val){}
			inline void down()
			{
				if (mAtom.dec_and_test() == false)
				{
					mSignal.wait();
				}
			}
			inline void up()
			{
				++mAtom;
				mSignal.pulse(signal::one);
			}
			inline operator long() const{return mAtom;}
		private:
			atom mAtom;
			signal mSignal;
	};
}

/*@}*/

#endif
