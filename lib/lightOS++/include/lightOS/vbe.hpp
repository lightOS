/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_VBE_HPP
#define LIGHTOS_VBE_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstddef>
#include <iostream>
namespace lightOS
{
	namespace C
	{
		#include <lightOS/lightOS.h>
	}
}

// TODO: Move to the vbe gui driver itself
namespace lightOS
{
	/*! Namespace of the kernel syscalls for VBE (=VESA BIOS Extensions) */
	namespace vbe
	{
		/*! One VBE video mode */
		class mode
		{
			friend void get_modes(std::vector<mode*> &);
			public:
				/*! Get width */
				inline size_t width(){return mWidth;}
				/*! Get height */
				inline size_t height(){return mHeight;}
				/*! Get the bits per pixel */
				inline size_t bits_per_pixel(){return mBitsPerPixel;}
				/*! Set this video mode */
				inline void *set()
					{return C::lightOS_vbe_set_mode(mIndex);}
				/*! The destructor */
				inline ~mode(){};
			protected:
				/*! The constructor */
				inline mode(size_t width,
							size_t height,
							size_t bpp,
							size_t index)
					: mWidth(width), mHeight(height), mBitsPerPixel(bpp), mIndex(index){}
			private:
				/*! The modes width */
				size_t mWidth;
				/*! The modes height */
				size_t mHeight;
				/*! The modes bits per pixel */
				size_t mBitsPerPixel;
				/*! The modes index */
				size_t mIndex;
		};
		
		/*! Get the VBE version
		 *\param[in,out] major major version number
		 *\param[in,out] minor minor version number */
		inline void get_version(size_t &major,
								size_t &minor)
			{C::lightOS_vbe_get_version(&major, &minor);}
		/*! Get the VBE modes
		 *\return std::vector with all VBE modes */
		inline void get_modes(std::vector<mode*> &modes)
		{
			size_t count = C::lightOS_vbe_get_mode_count();
			
			modes.reserve(count);
			for (size_t i = 0;i < count;i++)
			{
				size_t width, height, bpp;
				C::lightOS_vbe_enumerate_mode(i, &width, &height, &bpp);
				mode *Mode = new mode(width, height, bpp, i);
				modes.push_back(Mode);
			}
		}
	}
}

/*@}*/

#endif
