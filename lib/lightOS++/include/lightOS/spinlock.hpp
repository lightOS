/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_SPINLOCK_HPP
#define LIGHTOS_SPINLOCK_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <lightOS/atom.hpp>

#undef DECLARE_SPINLOCK
#undef LOCK
#undef UNLOCK

#ifdef _LIGHTOS_SMP
	#define LOCK(name) name.lock();
	#define UNLOCK(name) name.unlock();
#else
	#define LOCK(name)
	#define UNLOCK(name)
#endif

namespace lightOS
{
	class spinlock
	{
		public:
			inline spinlock(bool locked=false)
				: mAtom(locked ? 0 : 1){}
			inline void lock()
			{
				while (!mAtom.compare_and_exchange(1, 0)){}
			}
			inline void unlock()
			{
				mAtom = 1;
			}
		protected:
			atom mAtom;
	};
}

/*@}*/

#endif
