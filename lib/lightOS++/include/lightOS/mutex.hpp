/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_MUTEX_HPP
#define LIGHTOS_MUTEX_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <vector>
#include <lightOS/atom.hpp>
#include <lightOS/signal.hpp>

namespace lightOS
{
	template<bool userspace=true>
	class mutex{};
	
	template<>
	class mutex<true>
	{
		public:
			inline mutex(bool locked=false)
				: mAtom(locked ? 0 : 1){}
			inline void lock()
			{
				while (true)
				{
					if (mAtom.compare_and_exchange(1, 0))return;
					mSignal.wait();
				}
			}
			inline void unlock()
			{
				mAtom = 1;
				mSignal.pulse(signal::one);
			}
		private:
			atom mAtom;
			signal mSignal;
	};
}

/*@}*/

#endif
