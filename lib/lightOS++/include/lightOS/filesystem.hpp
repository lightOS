/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_FILESYSTEM_HPP
#define LIGHTOS_FILESYSTEM_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstdio>
#include <vector>
#include <string>
#include <utility>
#include <lightOS/lightOS.hpp>
#include <libserver/fs.h>
#include <libc/internal/stdio.h>

#define MAX_FILESYSTEM_NAME 10
#define MAX_PATH 1024

namespace lightOS
{
	struct register_fs_info
	{
		char name[MAX_FILESYSTEM_NAME+1];
	};
	struct mount_fs_info
	{
		char name[MAX_PATH+1];
		char filesystem[MAX_FILESYSTEM_NAME+1];
		char device[MAX_PATH+1];
	};
	
	bool registerFilesystem(libkernel::message_port &Port,
							const char *name);
	void waitForFilesystemRegistration(	libkernel::message_port &Port,
										const char *name);
	bool mount(	libkernel::message_port &Port,
				const char *name,
				const char *fs,
				const char *device="");
	std::pair<unsigned long, std::string> getFilesystemFromName(libkernel::message_port &Port,
																const char *name);
	void getFilesystemInfo(	libkernel::message_port &Port,
							const char *name,
							size_t &size,
							size_t &free);
	// TODO: Could use thread port here
	bool setWorkingDirectory(	libkernel::message_port &Port,
								const char *path,
								unsigned long pid=get_process_id());
	std::string getWorkingDirectory(libkernel::message_port &Port,
									unsigned long pid = get_process_id());
	bool setConsoleName(libkernel::message_port &Port,
						const std::string &filename,
						processId pid = get_process_id());
	std::string getConsoleName(	libkernel::message_port &Port,
								processId pid = get_process_id());
	std::vector<std::triple<std::string,std::string,std::string> > getMountpoints(libkernel::message_port &Port);
	
	inline void waitForDevfs(libkernel::message_port &Port)
	{
		Port.wait_for_other(libkernel::message_port::vfs);
		while (getFilesystemFromName(Port, "dev://").first == 0){wait(1);}
	}
	
	namespace access
	{
		/*! The access flags */
		enum
		{
			info			= _LIBSERVER_FILE_ACCESS_INFO,
			read			= _LIBSERVER_FILE_ACCESS_READ,
			write			= _LIBSERVER_FILE_ACCESS_WRITE,
			mask			= _LIBSERVER_FILE_ACCESS_MASK
		};
	}
	
	namespace attr
	{
		/*! The attribute flags */
		enum
		{
			tty				= _LIBSERVER_FILE_ATTRIBUTE_TTY,
			mask			= _LIBSERVER_FILE_ATTRIBUTE_MASK
		};
	}
	
	/*! File */
	class file : public libkernel::message_port::wait_group::handler
	{
		public:
			class iterator;
			typedef const iterator										const_iterator;
			
		private:
			friend class file::iterator;
		
		public:
			/*! The filetype */
			enum
			{
					/*! invalid */
					none		= _LIBSERVER_FILE_TYPE_NONE,
					/*! block file */
					block		= _LIBSERVER_FILE_TYPE_BLOCK,
					/*! stream file */
					stream		= _LIBSERVER_FILE_TYPE_STREAM,
					/*! directory */
					directory	= _LIBSERVER_FILE_TYPE_DIRECTORY
			};
			
			typedef _LIBSERVER_file_info info;
			typedef _LIBSERVER_file_name_info name_info;
			
			/*! The constructor */
			inline file() : mFilesystemPort(0), mEof(false){}
			/*! The copy-constructor */
			inline file(const file &File)
				: libkernel::message_port::wait_group::handler(File), mPort(File.mPort), mFilesystemPort(File.mFilesystemPort), mType(File.mType), mBlocksize(File.mBlocksize), mBlockcount(File.mBlockcount), mEof(false)
			{
				file &r = const_cast<file&>(File);
				r.mFilesystemPort = 0;
				r.mType = file::none;
				r.mBlockcount = 0;
				r.mBlocksize = 0;
			}
			/*! The constructor */
			inline file(FILE *file)
				: mPort(file->osdep.port, false), mFilesystemPort(file->osdep.fsport), mEof(false){}
			/*! The constructor
			 *\param[in] Port reference to the port that should be used*/
			inline file(libkernel::message_port &Port, unsigned long filesystem=0)
				: mPort(Port), mFilesystemPort(filesystem), mEof(false){}
			/*! The constructor. Creates a new file.
			 *\param[in] name the filename
			 *\param[in] type the filetype
			 *\param[in] blocksize size of one block
			 *\param[in] blockcount total number of blocks */
			inline file(const char *name,
						size_t type,
						unsigned long flags,
						size_t blocksize=1,
						size_t blockcount=0)
				: mFilesystemPort(0), mEof(false)
			{
				create(name, type, flags, blocksize, blockcount);
			}
			/*! The constructor. Openes a file.
			 *\param[in] name the filename */
			inline file(const char *name, unsigned long flags)
				: mFilesystemPort(0), mEof(false)
			{
				open(name, flags);
			}
			/*!\return the filetype */
			inline size_t filetype() const{return mType;}
			/*!\return the size of one block */
			inline size_t blocksize() const{updateFileInfo();return mBlocksize;}
			/*!\return the total number of blocks in the file */
			inline size_t blockcount() const{updateFileInfo();return mBlockcount;}
			
			inline iterator begin(){return iterator(*this, 0);}
			inline const_iterator begin() const{return const_iterator(*this, 0);}
			inline iterator end(){return iterator(*this, blockcount());}
			inline const_iterator end() const{return const_iterator(*this, blockcount());}
			
			std::string get_name() const;
			std::vector<libkernel::port_id_t> get_opener_list() const;
			
			/*! Create a file
			 *\param[in] name the filename
			 *\param[in] type the filetype
			 *\param[in] blocksize the size of one block
			 *\param[in] blockcount the total number of blocks
			 *\return true, if successfull, false otherwise */
			bool create(const char *name,
						size_t type,
						unsigned long flags,
						size_t blocksize=1,
						size_t blockcount=0);
			/*! Does a file exist?
			 *\param[in] name the filename
			 *\param[in] Port the port, that should be used for communication
			 *\return true, if file exists, false otherwise */
			static size_t exists(	const char *name,
									libkernel::message_port &Port);
			/*! Does a file exist?
			 *\param[in] name the filename
			 *\return true, if file exists, false otherwise */
			inline size_t exists(const char *name){return exists(name, mPort);}
			/*! Open an existing file
			 *\param[in] name the filename
			 *\return true, if successfull, false otherwise */
			bool open(	const char *name,
						unsigned long flags);
			/*! Execute a file
			 *\param[in] name the filename
			 *\param[in] Port the port
			 *\param[in] flags the lightOS::thread flags
			 *\return 0 if failed, processid otherwise */
			static unsigned long execute(	const char *name,
											libkernel::message_port &Port,
											unsigned long flags);
			/*! Write a shared-memory entity to the file.
			 *\param[in] shm the shared-memory entity
			 *\param[in] block the block number
			 *\return number of bytes written */
			unsigned long write(libkernel::shared_memory &shm, unsigned long block=0);
			/*! Write a buffer to a file
			 *\param[in] buffer pointer to the buffer
			 *\param[in] size the number of bytes to write
			 *\param[in] block the block number
			 *\return number of bytes written */
			template<class T>
			unsigned long write(const T *buffer, unsigned long size, unsigned long block=0)
			{
				if (buffer == 0)return 0;
				if (mFilesystemPort == 0)return 0;
				libkernel::shared_memory shm(size);
				memcpy(shm.address<void>(), reinterpret_cast<const void*>(buffer), size);
				return write(shm, block);
			}
			/*! Write a buffer to a file
			 *\note the size of the buffer is determined by the buffer type.
			 *\param[in] buffer reference to the buffer
			 *\param[in] block the block number
			 *\return the number of bytes written */
			template<class T>
			inline unsigned long write(const T &buffer, unsigned long block=0)
			{
				return write<T>(&buffer, sizeof(T), block);
			}
			
			template<class T>
			inline unsigned long read(T &buffer, unsigned long block=0)
			{
				return read<T>(&buffer, block, sizeof(T));
			}
			template<class T>
			unsigned long read(T *buffer, unsigned long block, unsigned long size)
			{
				libkernel::shared_memory shm = read(block, size);
				if (!shm)return 0;
				memcpy(buffer, shm.address<void>(), shm.size());
				return shm.size();
			}
			libkernel::shared_memory read(unsigned long block, unsigned long size);
			libkernel::shared_memory try_read(unsigned long block, unsigned long size);
			libkernel::shared_memory poll_read(unsigned long block);
			/*! Close the file
			 *\return true, if successfully close, false otherwise */
			bool close();
			/*! The destructor */
			inline ~file(){close();}
			/*! The = operator */
			/*! The copy-constructor */
			inline file &operator = (const file &File)
			{
				mPort = File.mPort;
				mFilesystemPort = File.mFilesystemPort;
				mType = File.mType;
				mBlockcount = File.mBlockcount;
				mBlocksize = File.mBlocksize;
				
				file &r = const_cast<file&>(File);
				r.mFilesystemPort = 0;
				r.mType = file::none;
				r.mBlockcount = 0;
				r.mBlocksize = 0;
				return *this;
			}
			/*! The bool cast operator */
			inline operator bool() const{return mFilesystemPort != 0;}
			inline bool eof() const{return mEof;}
			inline std::pair<libkernel::port_id_t,libkernel::port_id_t> release()
			{
				std::pair<libkernel::port_id_t, libkernel::port_id_t> ret(mPort.release(), mFilesystemPort);
				mFilesystemPort = 0;
				return ret;
			}
			inline std::pair<libkernel::port_id_t,libkernel::port_id_t> id()
			{
				return std::pair<libkernel::port_id_t,libkernel::port_id_t>(mPort.id(), mFilesystemPort);
			}
			
			/*! port callback interface: received a message */
			virtual void received(libkernel::port_id_t PortID, libkernel::message &msg);
			
			virtual void callback_read(	const libkernel::shared_memory &shm,
										size_t block);
			
			class iterator
			{
				friend class file;
				public:
					inline iterator(const file &File, size_t index)
						: mFile(&File), mIndex(index){}
					/*! The copy-constructor */
					inline iterator(const iterator &x)
						: mFile(x.mFile), mIndex(x.mIndex){}
					/*! The = operator */
					inline iterator &operator = (const iterator &x)
					{
						mFile = x.mFile;
						mIndex = x.mIndex;
						return *this;
					}
					/*! The == operator */
					inline bool operator == (const iterator &x) const
					{
						if (mFile != x.mFile)return false;
						if (mIndex != x.mIndex)return false;
						return true;
					}
					/*! The != operator */
					inline bool operator != (const iterator &x) const{return !(*this == x);}
					/*! The unary * operator */
					const std::string operator *() const;
					/*! The unary * operator */
					std::string operator *();
					/*! The prefix ++ operator */
					inline iterator &operator ++ ()
					{
						++mIndex;
						return *this;
					}
					/*! The prefix -- operator */
					inline iterator &operator -- ()
					{
						--mIndex;
						return *this;
					}
					/*! The postfix ++ operator */
					inline iterator operator ++ (int)
					{
						iterator tmp(*mFile, mIndex);
						++(*this);
						return tmp;
					}
					/*! The postfix -- operator */
					inline iterator operator -- (int)
					{
						iterator tmp(*mFile, mIndex);
						--(*this);
						return tmp;
					}
					/*! The += operator */
					inline iterator &operator += (int n)
					{
						if (n >= 0)while(n--)++(*this);
						else while(n++)--(*this);
						return *this;
					}
					/*! The -= operator */
					inline iterator &operator -= (int n){return (*this += -n);}
					/*! The + operator */
					inline iterator operator + (int n)
					{
						iterator tmp(*this);
						tmp += n;
						return tmp;
					}
					/*! The - operator */
					inline iterator operator - (int n)
					{
						return (*this + (-n));
					}
				private:
					const file *mFile;
					size_t mIndex;
			};
		private:
			void updateFileInfo() const;
			
			libkernel::message_port mPort;
			libkernel::port_id_t mFilesystemPort;
			mutable size_t mType;
			mutable size_t mBlocksize;
			mutable size_t mBlockcount;
			bool mEof;
	};
	
	libkernel::port_id_t create_pipe_name(std::string &name);
	std::pair<libkernel::port_id_t, libkernel::port_id_t> create_pipe(std::string &name);
}

/*@}*/

#endif
