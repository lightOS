/*
lightOS library
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_NET_HPP
#define LIGHTOS_NET_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstddef>
#include <cstdint>
#include <ostream>
#include <libkernel/message.hpp>

// TODO: Move to libserver
namespace lightOS
{
	namespace net
	{
		typedef size_t ethId;
		const ethId eth_all = static_cast<size_t>(-1);
		const uint32_t ip_any = 0xFFFFFFFF;
		const uint16_t port_random = static_cast<uint16_t>(-1);
		const uint16_t port_any = 0;
		
		class ethernet_address
		{
			public:
				inline ethernet_address(bool broadcast = false)
				{
					if (broadcast == true)
					{
						mAddress[0] = 0xFF;
						mAddress[1] = 0xFF;
						mAddress[2] = 0xFF;
						mAddress[3] = 0xFF;
						mAddress[4] = 0xFF;
						mAddress[5] = 0xFF;
					}
					else
					{
						mAddress[0] = 0;
						mAddress[1] = 0;
						mAddress[2] = 0;
						mAddress[3] = 0;
						mAddress[4] = 0;
						mAddress[5] = 0;
					}
				}
				inline ethernet_address(uint64_t addr)
				{
					mAddress[5] = static_cast<uint8_t>((addr >> 40) & 0xFF);
					mAddress[4] = static_cast<uint8_t>((addr >> 32) & 0xFF);
					mAddress[3] = static_cast<uint8_t>((addr >> 24) & 0xFF);
					mAddress[2] = static_cast<uint8_t>((addr >> 16) & 0xFF);
					mAddress[1] = static_cast<uint8_t>((addr >> 8) & 0xFF);
					mAddress[0] = static_cast<uint8_t>(addr & 0xFF);
				}
				inline ethernet_address(uint8_t *byte)
				{
					mAddress[0] = byte[0];
					mAddress[1] = byte[1];
					mAddress[2] = byte[2];
					mAddress[3] = byte[3];
					mAddress[4] = byte[4];
					mAddress[5] = byte[5];
				}
				inline ethernet_address(uint8_t byte0,
										uint8_t byte1,
										uint8_t byte2,
										uint8_t byte3,
										uint8_t byte4,
										uint8_t byte5)
				{
					mAddress[0] = byte0;
					mAddress[1] = byte1;
					mAddress[2] = byte2;
					mAddress[3] = byte3;
					mAddress[4] = byte4;
					mAddress[5] = byte5;
				}
				inline void to(uint8_t *byte)
				{
					byte[0] = mAddress[0];
					byte[1] = mAddress[1];
					byte[2] = mAddress[2];
					byte[3] = mAddress[3];
					byte[4] = mAddress[4];
					byte[5] = mAddress[5];
				}
				inline bool compare_to(uint8_t *byte)
				{
					if (byte[0] != mAddress[0])return false;
					if (byte[1] != mAddress[1])return false;
					if (byte[2] != mAddress[2])return false;
					if (byte[3] != mAddress[3])return false;
					if (byte[4] != mAddress[4])return false;
					if (byte[5] != mAddress[5])return false;
					return true;
				}
				inline void to(uint64_t &qword)
				{
					qword = (static_cast<uint64_t>(mAddress[5]) << 40) |
							(static_cast<uint64_t>(mAddress[4]) << 32) |
							(static_cast<uint64_t>(mAddress[3]) << 24) |
							(static_cast<uint64_t>(mAddress[2]) << 16) |
							(static_cast<uint64_t>(mAddress[1]) << 8) |
							static_cast<uint64_t>(mAddress[0]);
				}
				inline bool operator == (const ethernet_address &addr) const
				{
					if (mAddress[0] != addr.mAddress[0])return false;
					if (mAddress[1] != addr.mAddress[1])return false;
					if (mAddress[2] != addr.mAddress[2])return false;
					if (mAddress[3] != addr.mAddress[3])return false;
					if (mAddress[4] != addr.mAddress[4])return false;
					if (mAddress[5] != addr.mAddress[5])return false;
					return true;
				}
				inline bool operator != (const ethernet_address &addr) const
				{
					return !(*this == addr);
				}
				
				uint8_t mAddress[6];
		};
		
		inline std::ostream &operator << (std::ostream &s, ethernet_address &a)
		{
			s << std::hex;
			if (a.mAddress[0] < 0x10)s << '0';
			s << static_cast<size_t>(a.mAddress[0]) << ':';
			if (a.mAddress[1] < 0x10)s << '0';
			s << static_cast<size_t>(a.mAddress[1]) << ':';
			if (a.mAddress[2] < 0x10)s << '0';
			s << static_cast<size_t>(a.mAddress[2]) << ':';
			if (a.mAddress[3] < 0x10)s << '0';
			s << static_cast<size_t>(a.mAddress[3]) << ':';
			if (a.mAddress[4] < 0x10)s << '0';
			s << static_cast<size_t>(a.mAddress[4]) << ':';
			if (a.mAddress[5] < 0x10)s << '0';
			s << static_cast<size_t>(a.mAddress[5]);
			return s;
		}
		
		class ipv4_address
		{
			public:
				
				inline ipv4_address(uint32_t ip = 0)
					: mAddress(ip){}
				bool operator == (const ipv4_address &addr) const
				{
					if (mAddress != addr.mAddress)return false;
					return true;
				}
				bool operator != (const ipv4_address &addr) const
				{
					return !(*this == addr);
				}
				ipv4_address operator & (const ipv4_address &addr) const
				{
					return ipv4_address(mAddress & addr.mAddress);
				}
				ipv4_address operator | (const ipv4_address &addr) const
				{
					return ipv4_address(mAddress | addr.mAddress);
				}
				ipv4_address operator ~ () const
				{
					ipv4_address tmp(~mAddress);
					return tmp;
				}
				
				uint32_t mAddress;
		};
		
		inline std::ostream &operator << (std::ostream &s, ipv4_address &a)
		{
			s << std::dec << (a.mAddress & 0xFF) << "." << ((a.mAddress >> 8) & 0xFF) << "." << ((a.mAddress >> 16) & 0xFF) << "." << ((a.mAddress >> 24) & 0xFF);
			return s;
		}
		
		namespace ethernet_type
		{
			enum
			{
				ipv4		= 0x800,
				arp			= 0x0806,
				ipv6		= 0x86DD
			};
		}
		
		namespace ip_type
		{
			enum
			{
				icmp		= 0x0001,
				tcp			= 0x0006,
				udp			= 0x0011
			};
		}
		
		namespace icmp_type
		{
			enum
			{
				echo_reply		= 0x00,
				echo			= 0x08
			};
		}
		
		struct nic_info
		{
			ethernet_address		ethernetAddress;
			ipv4_address			ipv4Address;
			ipv4_address			ipv4SubnetMask;
			ipv4_address			ipv4BroadcastAddress;
			ipv4_address			ipv4Dns;
			ipv4_address			ipv4Dhcp;
			ipv4_address			ipv4Router;
			size_t					rx;
			size_t					tx;
			size_t					rx_size;
			size_t					tx_size;
		};
		
		struct bind_socket_info
		{
			ethId					ethID;
			uint16_t				dstPort;
			uint16_t				srcPort;
			ipv4_address			dstIP;
		};
		
		inline uint16_t hton16(uint16_t value)
		{
			return ((value & 0xFF) << 8) | ((value >> 8) & 0xFF);
		}
		inline uint32_t hton32(uint32_t value)
		{
			return ((value & 0xFF) << 24) | (((value >> 8) & 0xFF) << 16) | (((value >> 16) & 0xFF) << 8) | ((value >> 24) & 0xFF);
		}
		inline uint16_t ntoh16(uint16_t value)
		{
			return hton16(value);
		}
		inline uint32_t ntoh32(uint32_t value)
		{
			return hton32(value);
		}
		
		inline size_t ones_complement_checksum(	uint16_t *data,
												size_t size,
												size_t startval = 0,
												size_t ignore = static_cast<size_t>(-1))
		{
			size_t checksum = startval;
			for (size_t i = 0;i < (size / 2);i++)
				if (i != ignore)
					checksum += data[i];
			if ((size % 2) != 0)
				checksum += reinterpret_cast<uint8_t*>(data)[size - 1];
			return checksum;
		}
		
		inline uint16_t ones_complement_checksum_end(size_t checksum)
		{
			size_t result = checksum;
			while ((result >> 16) != 0)
				result = (result & 0xFFFF) + (result >> 16);
			return static_cast<uint16_t>(~result);
		}
		
		std::pair<bool, lightOS::net::ipv4_address> parse_ipv4_address(const std::string &ip);
		ethId register_nic(const nic_info &info, libkernel::message_port &Port);
		size_t enum_nic(libkernel::message_port &Port);
		bool get_nic_info(nic_info &info, ethId id, libkernel::message_port &Port);
		bool set_nic_info(nic_info &info, ethId id, libkernel::message_port &Port);
		void nic_received_frame(void *frame, size_t size, libkernel::message_port &Port);
		std::vector<std::pair<lightOS::net::ethernet_address, lightOS::net::ipv4_address> > arp_cache(libkernel::message_port &Port);
		std::vector<std::pair<std::string, lightOS::net::ipv4_address> > dns_cache(libkernel::message_port &Port);
		bool arp_clear_cache(libkernel::message_port &Port);
		bool dns_clear_cache(libkernel::message_port &Port);
		bool resolve(	const std::string &domain,
						lightOS::net::ipv4_address &result,
						libkernel::message_port &Port);
	}
	
	class socket
	{
		public:
			enum type
			{
				icmp,
				raw_icmp,
				udp,
				raw_udp,
				tcp,
				raw_tcp
			};
			inline bool resolve(const std::string &domain, 
								lightOS::net::ipv4_address &result)
			{
				return lightOS::net::resolve(domain, result, mPort);
			}
			
			bool create(type Type);
			bool bind(	lightOS::net::ethId eth,
						uint16_t srcPort = lightOS::net::port_random,
						lightOS::net::ipv4_address dstIp = lightOS::net::ip_any,
						uint16_t dstPort = lightOS::net::port_any);
			bool connect(	lightOS::net::ipv4_address dstIp = lightOS::net::ip_any,
							uint16_t dstPort = lightOS::net::port_any);
			template<class T>
			bool send(	const T *buffer,
						size_t size,
						lightOS::net::ipv4_address ip = lightOS::net::ipv4_address())
			{
				libkernel::shared_memory shm(size);
				memcpy(	shm.address<void>(),
						buffer,
						size);
				return send(shm, ip);
			}
			bool send(	libkernel::shared_memory shm,
						lightOS::net::ipv4_address ip = lightOS::net::ipv4_address());
			std::pair<libkernel::shared_memory, lightOS::net::ipv4_address> receive(lightOS::net::ipv4_address ip = lightOS::net::ipv4_address());
			bool disconnect();
			void destroy();
			inline ~socket(){destroy();}
		private:
			bool mBSocket;
			libkernel::message_port mPort;
	};
}

/*@}*/

#endif
