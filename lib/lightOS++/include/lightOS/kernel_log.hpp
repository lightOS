/*
lightOS library
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOSPP_KERNEL_LOG_HPP
#define LIGHTOSPP_KERNEL_LOG_HPP


/*
 * Libkernel includes
 */
#include <libkernel/log.hpp>

/*
 * Standard C++ includes
 */
#include <vector>
#include <string>


/*! \addtogroup lightOSpp lightOS++ library */
/*@{*/

namespace lightOS
{
  /** One entry of the kernel's log */
  struct kernel_log_entry
  {
    /** The message of the log entry */
    std::string          message;
    /** The severity of the log entry */
    libkernel::log_level level;
  };

  /** Get the kernel's log entries */
  std::vector<lightOS::kernel_log_entry> get_kernel_log();
}

/*@}*/

#endif
