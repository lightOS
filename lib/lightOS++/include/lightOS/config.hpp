/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_CONFIG_HPP
#define LIGHTOS_CONFIG_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <string>
#include <vector>
#include <utility>
#include <iostream>
#include <lightOS/filesystem.hpp>

namespace lightOS
{
	namespace config
	{
		template<class value_policy>
		class section;
		
		template<class value_policy>
		class entity
		{
			public:
				inline entity(section<value_policy> *parent = 0, size_t line = 0)
					: mParent(parent), mLine(line){}
				virtual const std::string &name() const=0;
				inline virtual bool is_section() const{return false;}
				inline size_t line() const{return mLine;}
				inline section<value_policy> *parent() const{return mParent;}
			protected:
				section<value_policy> *mParent;
				size_t mLine;
		};
		
		template<class value_policy>
		class section : public entity<value_policy>
		{
			public:
				inline section()
					: entity<value_policy>(0, 0){}
				inline section(const char *name, section *parent, size_t line)
					: entity<value_policy>(parent, line), mName(name){}
				inline virtual const std::string &name() const{return mName;}
				inline virtual bool is_section() const{return true;}
				inline void add(entity<value_policy> *Entity)
				{
					mEntities.push_back(Entity);
				}
				inline size_t entity_count() const{return mEntities.size();}
				inline const entity<value_policy> &operator [] (size_t index) const{return *mEntities[index];}
				inline section *get_section(const std::string &key) const
				{
					std::string::const_iterator found = std::find<std::string::const_iterator>(key.begin(), key.end(), ':');
					if (found == key.end())
					{
						for (size_t i = 0;i < mEntities.size();i++)
						{
							if (mEntities[i]->is_section() == true &&
								mEntities[i]->name() == key)
							{
								return static_cast<section*>(mEntities[i]);
							}
						}
						return 0;
					}
					if ((found + 1) == key.end() ||
						(found + 2) == key.end() ||
						*(found + 1) != ':')return 0;
					std::string sectionname(key.begin(), found);
					std::string remain(found + 2, key.end());
					for (size_t i = 0;i < mEntities.size();i++)
						if (mEntities[i]->is_section() == true &&
							mEntities[i]->name() == sectionname)
							return (*static_cast<const section*>(mEntities[i])).get_section(remain);
					return 0;
				}
				inline const value_policy *operator [] (const std::string &key) const
				{
					// TODO: Use get_section()
					std::string::const_iterator found = std::find<std::string::const_iterator>(key.begin(), key.end(), ':');
					if (found == key.end())
					{
						for (size_t i = 0;i < mEntities.size();i++)
						{
							if (mEntities[i]->is_section() == false &&
								mEntities[i]->name() == key)
							{
								return static_cast<const value_policy*>(mEntities[i]);
							}
						}
						return 0;
					}
					if ((found + 1) == key.end() ||
						(found + 2) == key.end() ||
						*(found + 1) != ':')return 0;
					std::string sectionname(key.begin(), found);
					std::string remain(found + 2, key.end());
					for (size_t i = 0;i < mEntities.size();i++)
						if (mEntities[i]->is_section() == true &&
							mEntities[i]->name() == sectionname)
							return (*static_cast<const section*>(mEntities[i]))[remain];
					return 0;
				}
			private:
				std::string mName;
				std::vector<entity<value_policy>*> mEntities;
		};
		
		class key_value_pair : public entity<key_value_pair>
		{
			public:
				key_value_pair(const std::string &line, section<key_value_pair> *parent, size_t iline);
				inline virtual const std::string &name() const{return mKey;}
				inline const std::string &get_value() const{return mValue;}
			private:
				std::string mKey;
				std::string mValue;
		};
		
		class key_only : public entity<key_only>
		{
			public:
				key_only(const std::string &line, section<key_only> *parent, size_t iline);
				inline virtual const std::string &name() const{return mKey;}
			private:
				std::string mKey;
		};
		
		template<class value_policy>
		section<value_policy> *parse(const char *name)
		{
			section<value_policy> *result = new section<value_policy>();
			
			// Open the file
			lightOS::file cfg(name, access::read);
			if (!cfg)
			{
				delete result;
				return 0;
			}
			
			// Read the file
			char *buffer = new char[cfg.blocksize() * cfg.blockcount() + 1];
			cfg.read(buffer, 0, cfg.blockcount());
			buffer[cfg.blocksize() * cfg.blockcount()] = '\0';
			
			// Parse the file
			size_t iline = 1;
			std::string config(buffer);
			delete []buffer;
			std::string::iterator cur = config.begin();
			section<value_policy> *cur_section = result;
			while (cur != config.end())
			{
				std::string::iterator next = std::find<std::string::iterator>(cur, config.end(), '\n');
				
				std::string line(cur, next);
				line.trim();
				if (line.length() != 0 && line[0] != '#')
				{
					if (line.length() > 8 &&
						strncmp(line.c_str(), "section ", 8) == 0)
					{
						cur_section = new section<value_policy>(&line.c_str()[8], cur_section, iline);
						cur_section->parent()->add(cur_section);
					}
					else if (line == "end")
					{
						if (cur_section->parent() != 0)
							cur_section = cur_section->parent();
						else
						{
							// TODO: Throw an exception
							std::cerr << "config::parse(): end used without section" << std::endl;
						}
					}
					else
					{
						value_policy *policy = new value_policy(line, cur_section, iline);
						cur_section->add(policy);
					}
				}
				if (next == config.end())break;
				cur = ++next;
				++iline;
			}
			
			return result;
		}
	}
}

/*@}*/

#endif
