/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIGHTOS_HPP
#define LIGHTOS_LIGHTOS_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstddef>
#include <vector>
#include <string>
#include <cstring>
#include <utility>
#include <libkernel/message.hpp>
#include <lightOS/c++utils.hpp>
#include <libkernel/type.h>
#include <libkernel/wait.h>
#include <libkernel/kernel.h>

namespace lightOS
{
	/*! Typedef processId */
	typedef _LIBKERNEL_process_id_t processId;
	/*! Typedef threadId */
	typedef _LIBKERNEL_thread_id_t threadId;
	
  // TODO: move to libkernel
	namespace thread
	{
		enum
		{
			/*! thread is suspended */
			suspended	= _LIBKERNEL_THREAD_SUSPENDED,
			/*! thread flags mask */
			mask		= _LIBKERNEL_THREAD_MASK
		};
	}
	
  // TODO: move to libkernel
	namespace process
	{
		enum
		{
			/*! process is a server*/
			server		= _LIBKERNEL_PROCESS_SERVER,
			/*! process flags mask */
			mask		= _LIBKERNEL_PROCESS_MASK
		};
	}
	
	// TODO: move to libkernel
	namespace context
	{
		/*! Flags for lightOS::map() */
 	enum flags
		{
			write			= 0x01,
			user			= 0x02,
			execute			= 0x04,
			global			= 0x08,
			write_through	= 0x10,
			cache_disable	= 0x20,
			copy_on_write	= 0x40
		};
	}
	
  // TODO: Move to libkernel
	namespace memory
	{
		enum
		{
			/*! Application does not care */
			none					= _LIBKERNEL_MEMORY_NONE,
			/*! Memory below 1MB is needed */
			below_1mb				= _LIBKERNEL_MEMORY_BELOW_1MB,
			/*! Memory below 16MB is needed */
			below_16mb				= _LIBKERNEL_MEMORY_BELOW_16MB,
			/*! Memory below 4GB is needed */
			below_4gb				= _LIBKERNEL_MEMORY_BELOW_4GB,
			/*! Mask for the memory types */
			type_mask				= _LIBKERNEL_MEMORY_TYPE_MASK,
			/*! Physically continuous memory? */
			continuous				= _LIBKERNEL_MEMORY_CONTINUOUS
		};
	}
	
	/*! Get the kernels minor and major version
	 *\return the major & minor version number */
	inline std::pair<size_t,size_t> get_kernel_version()
	{
		std::pair<size_t,size_t> version;
		_LIBKERNEL_kernel_version(&version.first, &version.second);
		return version;
	}
	
	/*! Get information about the memory usage
	 *\return the amount of total & free memory */
	inline std::pair<size_t,size_t> get_memory_info()
	{
		std::pair<size_t,size_t> memory;
		_LIBKERNEL_get_memory_info(&memory.first, &memory.second);
		return memory;
	}
	
	/*! Get the command-line
	 *\return the command-line*/
/* TODO
	inline char *getCommandLine()
	{
		char *cmdline = new char[C::getCommandLineLength() + 1];
		C::getCommandLine(cmdline);
		return cmdline;
	}*/
	
	/*! Get the current time
	 *\param [in,out] hour the current hour
	 *\param [in,out] minute the current minute
	 *\param [in,out] second the current second */
	inline void get_time(size_t &hour, size_t &minute, size_t &second)
		{_LIBKERNEL_get_time(&hour, &minute, &second);}
	
	/*! Get the current date
	*\param[in,out] dayofweek the day of week
	*\param[in,out] dayofmonth the day of month
	*\param[in,out] month the month
	*\param[in,out] year the year
	*\param[in,out] summertime summertime? */
	inline void get_date(	size_t &dayofweek,
							size_t &dayofmonth,
							size_t &month,
							size_t &year,
							bool &summertime)
	{
		size_t stime;
		_LIBKERNEL_get_date(&dayofweek, &dayofmonth, &month, &year, &stime);
		summertime = (stime != 0);
	}
	
	/*! Create a new thread
	 *\param[in] entryPoint the entryPoint for the thread
	 *\param[in] parameter the parameter
	 *\param[in] flags thread creation flags
	 *\return the thread's id or 0 */
	template<class T>
	inline threadId create_thread(void (*entryPoint)(T), T parameter=T(), size_t flags=0)
	{
		return _LIBKERNEL_create_thread(reinterpret_cast<void (*)(size_t)>(entryPoint),
										static_cast<size_t>(parameter),
										flags);
	}
	
	/*! Suspend a thread
	*\param[in] id the thread's id
	*\return successfull? */
	inline bool suspend_thread(threadId id)
		{return (_LIBKERNEL_suspend_thread(id) != 0);}
	
	/*! Resume a thread
	*\param[in] id the thread's id
	*\return successfull? */
	inline bool resume_thread(threadId id)
		{return (_LIBKERNEL_resume_thread(id) != 0);}
	
	/*! Destroy an existing thread
	 *\param[in] tid the thread's id
	 *\return true, if successfull, false otherwise */
	inline bool destroyThread(threadId id)
		{return (_LIBKERNEL_destroy_thread(id) != 0);}
	
	/*! Destroy a process
	 *\param[in] pid the process id
	 *\return true, if successfull, false otherwise */
	inline bool destroy_process(processId id)
		{return (_LIBKERNEL_destroy_process(id) != 0);}
	
	/*! Get the process's id
	 *\return this process's id */
	inline processId get_process_id()
		{return _LIBKERNEL_get_process_id();}
	
	/*! Get the thread's id
	*\return this thread's id */
	inline threadId get_thread_id()
		{return _LIBKERNEL_get_thread_id();}
	
	/*! Get information about the port
	*\param[in] id the portId
	*\return the processId of the owning process and the threadId of the waiting thread, if any */
	inline std::pair<processId,threadId> get_port_info(libkernel::port_id_t id)
	{
		std::pair<processId,threadId> value;
		_LIBKERNEL_get_port_info(id, &value.first, &value.second);
		return value;
	}
	
	inline bool set_standard_port(	processId pid,
									libkernel::port_id_t stdPort,
									libkernel::port_id_t Port,
									libkernel::port_id_t fsPort)
	{
		size_t res = _LIBKERNEL_set_standard_port(pid, stdPort, Port, fsPort);
		return (res != 0);
	}
	
	inline std::pair<libkernel::port_id_t,libkernel::port_id_t> get_standard_port(	processId pid,
														libkernel::port_id_t stdPort)
	{
		libkernel::port_id_t res1, res2;
		_LIBKERNEL_get_standard_port(pid, stdPort, &res1, &res2);
		return std::pair<libkernel::port_id_t, libkernel::port_id_t>(res1, res2);
	}
	
	inline bool transfer_port(	libkernel::port_id_t port,
								processId pid)
		{return (_LIBKERNEL_transfer_port(port, pid) != 0);}
	
	/*! Get the process list
	 *\return list of processId|process name pairs */
	inline std::vector<std::triple<processId,std::string,size_t> > get_process_list()
	{
		size_t count = _LIBKERNEL_get_process_count();
		
		std::vector<std::triple<processId,std::string,size_t> > list;
		list.reserve(count);
		for (size_t i=0;i < count;i++)
		{
			processId id = _LIBKERNEL_enum_processes(i);
			char *name = new char[_LIBKERNEL_get_process_name_length(id)+1];
			_LIBKERNEL_get_process_name(id, name);
			size_t usedMem = _LIBKERNEL_get_process_memory_usage(id);
			list.push_back(std::triple<processId,std::string,size_t>(id, name, usedMem));
			delete []name;
		};
		return list;
	}
	
	/*! Wait a certain amount of milliseconds
	 *\param[in] ms number of milliseconds */
	inline void wait(size_t ms)
		{_LIBKERNEL_wait(ms);}
	
	/*! Get the number of ticks (10ms) since system bootup
	*\return number of ticks */
	inline size_t get_tick_count()
		{return _LIBKERNEL_get_tick_count();}
	
	/*! Get the uptime
	 *\param[in,out] day the number of days
	 *\param[in,out] hour the number of hours
	 *\param[in,out] minute the number of minutes
	 *\param[in,out] second the number of seconds */
	inline void uptime(	size_t &day,
						size_t &hour,
						size_t &minute,
						size_t &second)
	{
		second = get_tick_count() / 1000;
		day = second / (60 * 60 * 24);
		second -= day * (60 * 60 * 24);
		hour = second / (60 * 60);
		second -= hour * (60 * 60);
		minute = second / 60;
		second -= minute * 60;
	}
	
	/*! Request access to an I/O port range
	 *\param[in] port the first I/O port in the range
	 *\param[in] count the number of ports after the first one
	 *\return true, if successfull, false otherwise */
	inline bool allocate_io_port_range(uint16_t port, size_t count)
		{return (_LIBKERNEL_allocate_io_port_range(port, count) != 0);}
	/*! Free a previously requested I/O port range
	 *\param[in] port the first I/O port in the range
	 *\param[in] count the number of ports after tge first one */
	inline void free_io_port_range(uint16_t port, size_t count)
		{_LIBKERNEL_free_io_port_range(port, count);}
	/*! Setup a DMA channel
	 *\param[in] channel the dma channel
	 *\param[in] address physical address of the buffer
	 *\param[in] count number of bytes
	 *\param[in] mode the dma mode
	 *\return true, if successfull, false otherwise */
	inline bool setupDMAChannel(size_t channel,
								void *address,
								size_t count,
								size_t mode)
		{return (_LIBKERNEL_setup_dma_channel(channel, address, count, mode) != 0);}
	/*! Execute a memory region
	 *\param[in] data pointer to the memory region
	 *\param[in] size size of the memory region
	 *\param[in] cmdline the command-line
	 *\param[in] flags process/thread creation flags
	 *\return the processId, if successfull, 0 otherwise */
	inline processId execute(	const void *data,
								size_t size,
								const char *cmdline,
								size_t flags,
								char *libName)
		{return _LIBKERNEL_execute(data, size, cmdline, flags, libName);}
	/*! Get the physical address from a virtual address
	 *\param[in] address the virtual address
	 *\return the physical address */
	inline void *get_physical_address(const void *address)
		{return _LIBKERNEL_get_physical_address(address);}
	
	inline void* allocate_region(	size_t size,
									size_t type,
									void *physical = 0)
		{return _LIBKERNEL_allocate_memory_region(size, type, physical);}
	inline void free_region(void *address)
		{_LIBKERNEL_free_memory_region(address);}
	typedef _LIBKERNEL_system_config_t system_config;
	inline void get_system_configuration(system_config &config)
		{_LIBKERNEL_get_system_configuration(&config);}
	
	inline std::vector<std::triple<std::string, void*, size_t> > get_library_list()
	{
		// Get the number of libraries
		size_t count = _LIBKERNEL_get_library_count();
		
		// Get information for every single library
		std::vector<std::triple<std::string, void*, size_t> > result;
		for (size_t i = 0;i < count;i++)
		{
			size_t size;
			_LIBKERNEL_syscall_param_t address;
			size_t length = _LIBKERNEL_get_library_name_length(i);
			char *str = new char[length + 1];
			_LIBKERNEL_get_library_info(i, str, length, &address, &size);
			result.push_back(std::triple<std::string, void*, size_t>(std::string(str), reinterpret_cast<void*>(address), size));
			delete []str;
		}
		return result;
	}
	
	struct range
	{
		uintptr_t address;
		size_t size;
	};
	
	inline std::vector<range> range_allocator(size_t &capacity)
	{
		std::vector<range> mResult;
		
		size_t count = _LIBKERNEL_range_allocator_count(&capacity);
		
		for (size_t i=0;i < count;i++)
		{
			range Range;
      _LIBKERNEL_syscall_param_t address;
			_LIBKERNEL_enum_range_allocator(i, &address, &Range.size);
      Range.address = address;
			mResult.push_back(Range);
		}
		
		return mResult;
	}
}

#include <lightOS/filesystem.hpp>

/*@}*/

#endif
