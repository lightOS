/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_CACHE_HPP
#define LIGHTOS_CACHE_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <vector>
#include <utility>
#include <lightOS/filesystem.hpp>

namespace lightOS
{
	class cache
	{
		public:
			class changeset
			{
				friend class cache;
				public:
					void add_blocks(void *buffer, size_t block, size_t count);
				private:
					/* (block number ; number of blocks ; pointer to buffer) */
					std::vector<std::triple<size_t,size_t,void*> > mBlockRanges;
			};
			
			bool open(const char *filename, size_t flags);
			void const *block(size_t block);
			bool block(	void *buffer,
						size_t block);
			bool block(	void *buffer,
						size_t block,
						size_t count);
			void *uncache_block(size_t block);
			bool commit(changeset &change);
			size_t blocksize() const{return mFile.blocksize();}
			size_t blockcount() const{return mFile.blockcount();}
		protected:
			void *find(size_t block);
		private:
			lightOS::file mFile;
			/* (block number ; pointer to buffer) */
			std::vector<std::pair<size_t,void*> > mCache;
	};
}

/*@}*/

#endif
