/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_ATOM_HPP
#define LIGHTOS_ATOM_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <vector>
#include <libarch/atomic.h>
#include <lightOS/lightOS.hpp>

// TODO: implement the C++0x way of stuff
namespace lightOS
{
	class atom
	{
		public:
			inline atom(int val):mAtom(val){}
			inline atom &operator + (int i)
			{
				atomic_add(&mAtom, i);
				return *this;
			}
			inline atom &operator - (int i)
			{
				atomic_sub(&mAtom, i);
				return *this;
			}
			inline bool sub_and_test(int i)
			{
				return atomic_sub_and_test(&mAtom, i);
			}
			inline atom &operator ++()
			{
				atomic_inc(&mAtom);
				return *this;
			}
			inline bool inc_and_test()
			{
				return atomic_inc_and_test(&mAtom);
			}
			inline atom &operator --()
			{
				atomic_dec(&mAtom);
				return *this;
			}
			inline bool dec_and_test()
			{
				return atomic_dec_and_test(&mAtom);
			}
			inline bool compare_and_exchange(int cmpval, int newval)
			{
				return atomic_compare_and_exchange(&mAtom, cmpval, newval);
			}
			inline atom &operator = (int i)
			{
				mAtom = i;
				return *this;
			}
			inline operator int() const{return mAtom;}
		private:
			_LIBARCH_atomic_int_t mAtom;
	};
}

/*@}*/

#endif
