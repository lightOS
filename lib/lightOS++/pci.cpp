/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <lightOS/pci.hpp>
using namespace lightOS;
using namespace std;

using libkernel::message;

vector<pci::deviceAddr> pci::enumDevices(libkernel::message_port &Port)
{
	message msg(libkernel::message_port::pci, libkernel::message::pci_enum_devices);
	Port.send(msg);
	Port.wait(msg);
	vector<pci::deviceAddr> result;
	if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
	{
		libkernel::shared_memory shm = msg.get_shared_memory();
		pci::deviceAddr *device = shm.address<pci::deviceAddr>();
		for (size_t i=0;i < msg.param1();i++)result.push_back(device[i]);
	}
	return result;
}
pci::deviceInfo pci::getDeviceInfo(const deviceAddr &address, libkernel::message_port &Port)
{
	message msg(libkernel::message_port::pci, libkernel::message::pci_get_device_info, address.bus, address.device, address.function);
	Port.send(msg);
	message reply(libkernel::message_port::pci, libkernel::message::pci_get_device_info);
	Port.wait(reply);
	pci::deviceInfo info;
	if (reply.attribute() == libkernel::shared_memory::transfer_ownership)
	{
		libkernel::shared_memory shm = reply.get_shared_memory();
		memcpy(&info, shm.address<void>(), sizeof(pci::deviceInfo));
	}
	return info;
}
pci::deviceAddr pci::findDevice(size_t vendorId, size_t deviceId, libkernel::message_port &Port)
{
	message msg(libkernel::message_port::pci, libkernel::message::pci_find_device, vendorId, deviceId);
	Port.send(msg);
	message reply(libkernel::message_port::pci, libkernel::message::pci_find_device);
	Port.wait(reply);
	return deviceAddr(reply.param1(), reply.param2(), reply.param3());
}
bool pci::enableBusmaster(const deviceAddr &address, libkernel::message_port &Port)
{
	message msg(libkernel::message_port::pci, libkernel::message::pci_enable_busmaster, address.bus, address.device, address.function);
	Port.send(msg);
	message reply(libkernel::message_port::pci, libkernel::message::pci_enable_busmaster);
	Port.wait(reply);
	return static_cast<bool>(reply.param1());
}
