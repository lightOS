/*
lightOS library
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libkernel/kernel.h>
#include <lightOS/kernel_log.hpp>

std::vector<lightOS::kernel_log_entry> lightOS::get_kernel_log()
{
  std::vector<lightOS::kernel_log_entry> result;

  size_t index = 0;
  while (true)
  {
    kernel_log_entry entry;

    /* Get the entries message length */
    size_t size = _LIBKERNEL_get_log_entry(index,
                                           0,
                                           0,
                                           reinterpret_cast<_LIBKERNEL_log_level*>(&entry.level));

    /* No entry available at that index? */
    if (size == 0)
      break;

    /* Get the actual message */
    char *array = new char[size];
    _LIBKERNEL_get_log_entry(index,
                             size,
                             array,
                             reinterpret_cast<_LIBKERNEL_log_level*>(&entry.level));

    /* Push the log entry */
    entry.message.assign(array);
    result.push_back(entry);
    delete []array;

    ++index;
  }

  return result;
}
