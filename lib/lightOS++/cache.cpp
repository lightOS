/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/cache.hpp>
using namespace lightOS;
using namespace std;

void cache::changeset::add_blocks(void *buffer, size_t block, size_t count)
{
	mBlockRanges.push_back(triple<size_t,size_t,void*>(block, count, buffer));
}
bool cache::open(const char *filename, size_t flags)
{
	return mFile.open(filename, flags);
}
void const *cache::block(size_t block)
{
	if (block >= mFile.blockcount())return 0;
	
	/* Search block in cache */
	void *found = find(block);
	if (found != 0)return found;
	
	/* Load the block from the device */
	auto_array<uint8_t> buf(new uint8_t[mFile.blocksize()]);
	if (mFile.read(buf.get(), block, mFile.blocksize()) != mFile.blocksize())
		return 0;
	mCache.push_back(pair<size_t,void*>(block, buf.get()));
	return buf.release();
}
bool cache::block(void *buffer, size_t block)
{
	void const *buf = this->block(block);
	if (buf == 0)return false;
	memcpy(	reinterpret_cast<uint8_t*>(buffer),
			buf,
			mFile.blocksize());
	return true;
}
bool cache::block(void *buffer, size_t block, size_t count)
{
	if (count == 0)return false;
	if ((block + count) >= mFile.blockcount())return false;
	
	/* get blocks */
	for (size_t i = 0;i < count;i++)
		if (this->block(reinterpret_cast<uint8_t*>(buffer) + i * mFile.blocksize(), block + i) == false)
			return false;
	
	return true;
}
void *cache::uncache_block(size_t block)
{
	if (block >= mFile.blockcount())return 0;
	
	/* Search */
	for (vector<pair<size_t,void*> >::iterator i = mCache.begin();i != mCache.end();i++)
		if ((*i).first == block)
		{
			void *buf = (*i).second;
			mCache.erase(i);
			return buf;
		}
	
	/* Load the block from the device */
	auto_array<uint8_t> buf(new uint8_t[mFile.blocksize()]);
	if (mFile.read(buf.get(), block, mFile.blocksize()) != mFile.blocksize())
		return 0;
	return buf.release();
}
void *cache::find(size_t block)
{
	for (size_t i = 0;i < mCache.size();i++)
		if (mCache[i].first == block)
			return mCache[i].second;
	return 0;
}
bool cache::commit(changeset &change)
{
	/* Go through changeset */
	for (size_t i = 0;i < change.mBlockRanges.size();i++)
	{
		bool markedForDeletion = false;
		for (size_t y = 0;y < change.mBlockRanges[i].second;y++)
		{
			size_t block = y + change.mBlockRanges[i].first;
			void *cachedBlock = find(block);
			if (cachedBlock == 0 ||
				memcmp(	cachedBlock,
						reinterpret_cast<uint8_t*>(change.mBlockRanges[i].third) + y * mFile.blocksize(),
						mFile.blocksize())
				!= 0)
			{
				/* Write to device */
				// TODO: What should we do if this fails?
				//cout << "writing sector #" << dec << (y + change.mBlockRanges[i].first) << endl;
				if (mFile.write(reinterpret_cast<uint8_t*>(change.mBlockRanges[i].third) + y * mFile.blocksize(),
								mFile.blocksize(),
								block)
					== false)
				{
					cerr << "cache: failed to write block " << dec << block << endl;
				}
				
				/* Add to cache */
				if (cachedBlock == 0)
				{
					if (change.mBlockRanges[i].second == 1)
					{
						mCache.push_back(pair<size_t,void*>(block, change.mBlockRanges[i].third));
					}
					else
					{
						void *newEntry = new uint8_t[mFile.blocksize()];
						memcpy(	newEntry,
								reinterpret_cast<uint8_t*>(change.mBlockRanges[i].third) + y * mFile.blocksize(),
								mFile.blocksize());
						mCache.push_back(pair<size_t,void*>(block, newEntry));
						markedForDeletion = true;
					}
				}
				else
				{
					/* Update the cache */
					memcpy(	cachedBlock,
							reinterpret_cast<uint8_t*>(change.mBlockRanges[i].third) + y * mFile.blocksize(),
							mFile.blocksize());
				}
			}
		}
		if (markedForDeletion)delete []reinterpret_cast<uint8_t*>(change.mBlockRanges[i].third);
	}
	
	return true;
}
