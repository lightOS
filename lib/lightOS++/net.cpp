/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <string>
#include <algorithm>
#include <lightOS/net.hpp>
using namespace lightOS;
using namespace std;

using libkernel::message;

pair<bool, lightOS::net::ipv4_address> net::parse_ipv4_address(const string &ip)
{
	pair<bool, ipv4_address> result(false, 0);
	string::iterator iter = ip.begin();
	for (size_t i = 0;i < 4;i++)
	{
		string::iterator iend = find<string::iterator>(iter, ip.end(), '.');
		if ((iend == ip.end() && i != 3) ||
			(iend != ip.end() && i == 3))
			return result;
		string tmp = string(iter, iend);
		unsigned long part = strtoul(tmp.c_str(), 0, 10);
		if (part > 255)
			return result;
		result.second.mAddress |= (part << (i * 8));
		iter = iend + 1;
	}
	result.first = true;
	return result;
}
net::ethId net::register_nic(const nic_info &info, libkernel::message_port &Port)
{
	libkernel::shared_memory shm(sizeof(net::nic_info));
	memcpy(	shm.address<void>(),
			&info,
			sizeof(nic_info));
	message msg(libkernel::message_port::net, libkernel::message::net_register_nic, 0, shm, libkernel::shared_memory::transfer_ownership);
	Port.send(msg);
	message reply(libkernel::message_port::net, libkernel::message::net_register_nic);
	Port.wait(reply);
	return reply.param1();
}
size_t net::enum_nic(libkernel::message_port &Port)
{
	message msg(libkernel::message_port::net, libkernel::message::net_enum_nic);
	Port.send(msg);
	message reply(libkernel::message_port::net, libkernel::message::net_enum_nic);
	Port.wait(reply);
	return reply.param1();
}
bool net::get_nic_info(nic_info &info, ethId id, libkernel::message_port &Port)
{
	message msg(libkernel::message_port::net, libkernel::message::net_get_nic_info, id);
	Port.send(msg);
	message reply(libkernel::message_port::net, libkernel::message::net_get_nic_info, id);
	Port.wait(reply);
	if (reply.attribute() != libkernel::shared_memory::transfer_ownership)return false;
	libkernel::shared_memory shm(reply.get_shared_memory());
	memcpy(	&info,
			shm.address<void>(),
			sizeof(nic_info));
	return true;
}
bool net::set_nic_info(nic_info &info, ethId id, libkernel::message_port &Port)
{
	libkernel::shared_memory shm(sizeof(nic_info));
	memcpy(	shm.address<void>(),
			&info,
			sizeof(nic_info));
	message request(libkernel::message_port::net, libkernel::message::net_set_nic_info, id, shm, libkernel::shared_memory::transfer_ownership);
	Port.send(request);
	message reply(libkernel::message_port::net, libkernel::message::net_set_nic_info, id);
	return static_cast<bool>(reply.param2());
}
void net::nic_received_frame(void *frame, size_t size, libkernel::message_port &Port)
{
	libkernel::shared_memory shm(size);
	memcpy(	shm.address<void>(),
			frame,
			size);
	message msg(libkernel::message_port::net, libkernel::message::net_nic_receive, 0, shm, libkernel::shared_memory::transfer_ownership);
	Port.send(msg);
}
vector<pair<net::ethernet_address, net::ipv4_address> > net::arp_cache(libkernel::message_port &Port)
{
	vector<pair<net::ethernet_address, net::ipv4_address> > result;
	message request(libkernel::message_port::net, libkernel::message::net_arp_count);
	Port.send(request);
	message reply(libkernel::message_port::net, libkernel::message::net_arp_count);
	Port.wait(reply);
	for (size_t i = 0;i < reply.param1();i++)
	{
		message request2(libkernel::message_port::net, libkernel::message::net_arp_entry, i);
		Port.send(request2);
		message reply2(libkernel::message_port::net, libkernel::message::net_arp_entry);
		Port.wait(reply2);
		if (reply2.param3() != 0)
		{
			uint64_t addr = static_cast<uint64_t>(reply2.param1()) | (static_cast<uint64_t>(reply2.param2()) << 32);
			result.push_back(pair<net::ethernet_address, net::ipv4_address>(net::ethernet_address(addr), reply2.param3()));
		}
	}
	return result;
}
vector<pair<string, lightOS::net::ipv4_address> > net::dns_cache(libkernel::message_port &Port)
{
	vector<pair<string, lightOS::net::ipv4_address> > result;
	message request(libkernel::message_port::net, libkernel::message::net_dns_count);
	Port.send(request);
	message reply(libkernel::message_port::net, libkernel::message::net_dns_count);
	Port.wait(reply);
	for (size_t i = 0;i < reply.param1();i++)
	{
		message request2(libkernel::message_port::net, libkernel::message::net_dns_entry, i);
		Port.send(request2);
		message reply2(libkernel::message_port::net, libkernel::message::net_dns_entry);
		Port.wait(reply2);
		libkernel::shared_memory shm = reply2.get_shared_memory();
		if (reply2.attribute() != libkernel::shared_memory::none)
		{
			result.push_back(pair<string, lightOS::net::ipv4_address>(string(shm.address<char>(), shm.size()), reply2.param1()));
		}
	}
	return result;
}
bool net::arp_clear_cache(libkernel::message_port &Port)
{
	message request(libkernel::message_port::net, libkernel::message::net_arp_clear);
	Port.send(request);
	message reply(libkernel::message_port::net, libkernel::message::net_arp_clear);
	Port.wait(reply);
	return static_cast<bool>(reply.param1());
}
bool net::dns_clear_cache(libkernel::message_port &Port)
{
	message request(libkernel::message_port::net, libkernel::message::net_dns_clear);
	Port.send(request);
	message reply(libkernel::message_port::net, libkernel::message::net_dns_clear);
	Port.wait(reply);
	return static_cast<bool>(reply.param1());
}
bool net::resolve(	const std::string &domain,
					lightOS::net::ipv4_address &result,
					libkernel::message_port &Port)
{
	libkernel::shared_memory shm(domain.length() + 1);
	strcpy(	shm.address<char>(),
			domain.c_str());
	message msg(libkernel::message_port::net, libkernel::message::net_resolve, 0, shm, libkernel::shared_memory::transfer_ownership);
	Port.send(msg);
	message reply(libkernel::message_port::net, libkernel::message::net_resolve);
	Port.wait(reply);
	result.mAddress = reply.param2();
	return reply.param1();
}

bool socket::create(type Type)
{
	message msg(libkernel::message_port::net, libkernel::message::net_socket_create, Type);
	mPort.send(msg);
	message reply(libkernel::message_port::net, libkernel::message::net_socket_create);
	mPort.wait(reply);
	mBSocket = static_cast<bool>(reply.param1());
	return mBSocket;
}
bool socket::bind(	lightOS::net::ethId eth,
					uint16_t srcPort,
					lightOS::net::ipv4_address dstIp,
					uint16_t dstPort)
{
	libkernel::shared_memory shm(sizeof(net::bind_socket_info));
	net::bind_socket_info *Info = shm.address<net::bind_socket_info>();
	Info->ethID = eth;
	Info->srcPort = srcPort;
	Info->dstIP = dstIp;
	Info->dstPort = dstPort;
	message request(libkernel::message_port::net, libkernel::message::net_socket_bind, 0, shm, libkernel::shared_memory::transfer_ownership);
	mPort.send(request);
	message reply(libkernel::message_port::net, libkernel::message::net_socket_bind);
	mPort.wait(reply);
	return static_cast<bool>(reply.param1());
}
bool socket::connect(	lightOS::net::ipv4_address dstIp,
						uint16_t dstPort)
{
	message request(libkernel::message_port::net, libkernel::message::net_socket_connect, dstIp.mAddress, dstPort);
	mPort.send(request);
	message reply(libkernel::message_port::net, libkernel::message::net_socket_connect);
	mPort.wait(reply);
	return static_cast<bool>(reply.param1());
}
bool socket::send(	libkernel::shared_memory shm,
					net::ipv4_address ip)
{
	message request(libkernel::message_port::net, libkernel::message::net_socket_send, ip.mAddress, shm, libkernel::shared_memory::transfer_ownership);
	mPort.send(request);
	message reply(libkernel::message_port::net, libkernel::message::net_socket_send);
	mPort.wait(reply);
	return static_cast<bool>(reply.param1());
}
pair<libkernel::shared_memory, lightOS::net::ipv4_address> socket::receive(lightOS::net::ipv4_address ip)
{
	message reply(libkernel::message_port::net, libkernel::message::net_socket_receive);
	mPort.wait(reply);
	return pair<libkernel::shared_memory,lightOS::net::ipv4_address>(reply.get_shared_memory(), reply.param1());
}
bool socket::disconnect()
{
	message msg(libkernel::message_port::net, libkernel::message::net_socket_disconnect);
	mPort.send(msg);
	mPort.wait(msg);
	return static_cast<bool>(msg.param1());
}
void socket::destroy()
{
	if (mBSocket)
	{
		disconnect();
		
		message msg(libkernel::message_port::net, libkernel::message::net_socket_destroy);
		mPort.send(msg);
		mBSocket = false;
	}
}
