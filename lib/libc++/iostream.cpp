/*
lightOS library
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <ostream>
#include <cstdio>

// TODO This is platform dependant should be replaced through standard compliant iostream library
//      implementation, which call the libc functions.

template<class charT>
class output : public std::basic_ostream<charT>
{
	public:
		inline output(FILE *file): mFile(file){}
		virtual output &operator << (const charT c)
		{
			charT s[2] = {c, '\0'};
			return *this << &s[0];
		}
		virtual output &operator << (const charT *s)
		{
			if (s != 0)
			{
				size_t length = strlen(s);
				if (length != 0)
				{
					fwrite(s, length, 1, mFile);
				}
			}
			return *this;
		}
		virtual void flush()
		{
			fflush(mFile);
		}
	private:
		FILE *mFile;
};

output<char> cout(stdout);
output<char> cerr(stderr);
output<char> clog(stdout);

namespace std
{
	ostream &cout = ::cout;
	ostream &cerr = ::cerr;
	ostream &clog = ::clog;
	//TODO: wostream const &wcout = output<wchar_t>();
	//      wostream const &wcerr = output<wchar_t>();
	//      wostream const &wclog = output<wchar_t>();
}
