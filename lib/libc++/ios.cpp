/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <ios>
using namespace std;

ios_base::fmtflags ios_base::flags(fmtflags fmtfl)
{
	ios_base::fmtflags tmp = _fmtflags;
	_fmtflags = fmtfl;
	return tmp;
}
ios_base::fmtflags ios_base::setf(ios_base::fmtflags fmtfl)
{
	ios_base::fmtflags tmp = _fmtflags;
	_fmtflags = fmtfl | _fmtflags;
	return tmp;
}
ios_base::fmtflags ios_base::setf(ios_base::fmtflags fmtfl, ios_base::fmtflags mask)
{
	ios_base::fmtflags tmp = _fmtflags;
	_fmtflags = fmtfl | (_fmtflags & (~mask));
	return tmp;
}

ios_base &std::dec(ios_base &base)
{
	base.setf(ios_base::dec, ios_base::basefield);
	return base;
}
ios_base &std::hex(ios_base &base)
{
	base.setf(ios_base::hex, ios_base::basefield);
	return base;
}
ios_base &std::oct(ios_base &base)
{
	base.setf(ios_base::oct, ios_base::basefield);
	return base;
}

ios_base::fmtflags std::operator | (ios_base::fmtflags fl1, ios_base::fmtflags fl2)
{
	return static_cast<ios_base::fmtflags>(static_cast<int>(fl1) | static_cast<int>(fl2));
}
ios_base::fmtflags std::operator & (ios_base::fmtflags fl1, ios_base::fmtflags fl2)
{
	return static_cast<ios_base::fmtflags>(static_cast<int>(fl1) & static_cast<int>(fl2));
}
ios_base::fmtflags std::operator ~ (ios_base::fmtflags fl)
{
	return static_cast<ios_base::fmtflags>(~static_cast<int>(fl));
}
