/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_VECTOR
#define LIBCPP_VECTOR

/*! \addtogroup libcpp libc++ */
/*@{*/

#include <cstddef>
#include <memory>
#include <algorithm>
#include <iterator>

namespace std
{
	/*! Standard vector class
	 *\todo add function max_size() -> numeric limits */
	template<class T, class Allocator = allocator<T> >
	class vector
	{
		public:
			/*! Typedef reference */
			typedef typename Allocator::reference								reference;
			/*! Typedef const_reference */
			typedef typename Allocator::const_reference							const_reference;
			/*! Typedef iterator */
			typedef sequential_iterator<T, vector<T, Allocator> >				iterator;
			/*! Typedef const_iterator */
			typedef const_sequential_iterator<T, vector<T, Allocator> >			const_iterator;
			/*! Typedef const_reverse_iterator */
			typedef std::const_reverse_iterator<T, iterator>							const_reverse_iterator;
			/*! Typedef reverse_iterator */
			typedef std::reverse_iterator<T, iterator>								reverse_iterator;
			/*! Typedef size_type */
			typedef size_t														size_type;
			/*! Typedef difference_type */
			typedef typename Allocator::difference_type							difference_type;
			/*! Typedef value_type */
			typedef T															value_type;
			/*! Typedef allocator_type */
			typedef Allocator													allocator_type;
			/*! Typedef pointer */
			typedef typename Allocator::pointer									pointer;
			/*! Typedef const_pointer */
			typedef typename Allocator::const_pointer							const_pointer;
			
			/*! The constructor */
			inline explicit vector(const allocator_type &a = allocator_type())
				: mSize(0), mCapacity(0), mData(0), mAllocator(a){}
			/*! The constructor
			 *\param[in] n number of elements of value
			 *\param[in] value elements value */
			inline explicit vector(size_type n, const value_type &value = value_type(), const allocator_type &a = allocator_type())
				: mSize(0), mCapacity(0), mData(0), mAllocator(a){resize(n, value);}
			/*! The constructor */
			inline vector(const vector<value_type, allocator_type> &x)
				: mSize(0), mCapacity(0), mData(0), mAllocator(x.mAllocator){assign(x.begin(), x.end());}
			/*! The constructor
			 *\param[in] first iterator pointing to the first object
			 *\param[in] last iterator pointing to the last object */
			template<class InputIterator>
			inline vector(InputIterator first, InputIterator last)
				: mCapacity(0), mSize(0), mData(0){assign<InputIterator>(first, last);}
			/*! Assign
			 *\param[in] first the iterator pointer to the first value to assign
			 *\param[in] last iterator pointing to the last value to assign
			 *\todo Find a more generic but fast solution */
			template<class InputIterator>
			void assign(InputIterator first, InputIterator last)
			{
				// TODO: Destruct old objects
				size_type size = last.getOffset() - first.getOffset();
				reserve(size);
				mSize = size;
				for (size_type i=0;i < mSize;i++,first++)mAllocator.construct(&mData[i], *first);
			}
			/*! Assign
			 *\param[in] n number of objects to assign
			 *\param[in] t the object value  */
			void assign(size_type n, const value_type& t)
			{
				reserve(n);
				mSize = n;
				for (size_type i=0;i < mSize;i++)mAllocator.construct(&mData[i], t);
			}
			/*! Get an iterator for the vector's beginning */
			inline iterator begin(){return iterator(*this, 0);}
			/*! Get a const iterator for the vector's beginning */
			inline const_iterator begin() const{return const_iterator(*this, 0);}
			/*! Get an iterator for the string's beginning */
			inline reverse_iterator rbegin(){return reverse_iterator(iterator(*this, mSize - 1));}
			/*! Get a const iterator for the string's beginning */
			inline const_reverse_iterator rbegin() const{return const_reverse_iterator(iterator(*this, mSize - 1));}
			/*! Get an iterator for the vector's end */
			inline iterator end(){return iterator(*this, mSize);}
			/*! Get a const iterator for the vector's end */
			inline const_iterator end() const{return const_iterator(*this, mSize);}
			/*! Get an iterator for the string's reverse end */
			inline reverse_iterator rend(){return reverse_iterator(iterator(*this, -1));}
			/*! Get a const iterator for the string's reverse end */
			inline const_reverse_iterator rend() const{return const_reverse_iterator(iterator(*this, -1));}
			/*! \return Reference to the first object */
			inline reference front(){return mData[0];}
			/*! \return Constant reference to the first object */
			inline const_reference front() const{return mData[0];}
			/*! \return Reference to the last object */
			inline reference back(){return mData[mSize-1];}
			/*! \return Constant reference to the last object */
			inline const_reference back() const{return mData[mSize-1];}
			/*! Get the number of objects in the vector
			 *\return number of objects in the vector */
			inline size_type size() const{return mSize;}
			/*! Get the capacity
			 *\return the vector's capacity */
			inline size_type capacity() const{return mCapacity;}
			/*! Is the container empty? */
			inline bool empty() const{return (mCapacity == 0);}
			/*! Reserve
			 *\param[in] n number of objects to reserve space for */
			void reserve(size_type n)
			{
				if (mCapacity < n)
				{
					size_type oldCapacity = mCapacity;
					mCapacity = n * 2;
					pointer tmp = mData;
					mData = mAllocator.allocate(mCapacity);
					
					if (tmp != 0)
					{
						memcpy(mData, tmp, mSize * sizeof(value_type));
						mAllocator.deallocate(tmp, oldCapacity);
					}
				}
			}
			/*! Resize the vector
			 *\param[in] m minimum number of objects
			 *\param[in] c value to initialize the remaining objects */
			void resize(size_type m, value_type c = value_type())
			{
				if (mSize < m)
				{
					reserve(m);
					for (size_type i=0;i < (m - mSize);i++)
						mAllocator.construct(&mData[mSize + i], c);
					mSize = m;
				}
			}
			/*! Insert an object at the end
			 *\param[in] x the object */
			inline void push_back(const value_type &x)
			{
				reserve(mSize + 1);
				mAllocator.construct(&mData[mSize], x);
				++mSize;
			}
			/*! Delete the object at the end of the container */
			inline void pop_back()
			{
				--mSize;
				mAllocator.destroy(&mData[mSize]);
			}
			/*! Remove an element from the vector
			 *\param[in] pos the position in the vector */
			iterator erase(iterator pos)
			{
				unsigned int off = pos.getOffset();
				mAllocator.destroy(&mData[off]);
				--mSize;
				memmove(&mData[off], &mData[off + 1], sizeof(value_type) * (mSize - off));
				return iterator(*this, off);
			}
			/*! Remove the elements between first and last
			 *\param[in] first first element to remove
			 *\param[in] last last element to remove */
			iterator erase(iterator first, iterator last)
			{
				unsigned int off1 = first.getOffset();
				unsigned int off2 = last.getOffset();
				for (size_type i=off1;i < off2;i++)mAllocator.destroy(&mData[i]);
				memmove(&mData[off1], &mData[off2], sizeof(value_type) * (mSize - off2));
				mSize -= off2 - off1;
				return iterator(*this, off1);
			}
			/*! Remove all elements */
			inline void clear(){erase(begin(), end());}
			/*! Swap two vectors
			 *\param[in] x the other vector */
			void swap(vector<value_type, Allocator> &x)
			{
				std::swap<size_type>(mSize, x.mSize);
				std::swap<size_type>(mCapacity, x.mCapacity);
				std::swap<allocator_type>(mAllocator, x.mAllocator);
				std::swap<pointer>(mData, x.mData);
			}
			/*! Get the allocator */
			allocator_type get_allocator() const{return allocator_type(mAllocator);}
			/*! Return the element at position n
			 *\todo throw an exception if (n >= mSize) */
			inline reference at(size_type n){return mData[n];}
			/*! Return the element at position n
			 *\todo throw an exception if (n >= mSize) */
			inline const_reference at(size_type n) const{return mData[n];}
			/*! Destructor */
			~vector()
			{
				if (mData != 0)mAllocator.deallocate(mData, mCapacity);
			}
			
			/*! The [] operator */
			inline reference operator [] (size_type n){return mData[n];}
			/*! The [] operator */
			inline const_reference operator [] (size_type n) const{return mData[n];}
			/*! The = operator */
			inline const vector<value_type, Allocator> &operator = (const vector<value_type, Allocator> &x)
			{
				assign(x.begin(), x.end());
				return *this;
			}
		private:
			/*! The number of objects in the vector */
			size_type mSize;
			/*! The vectors size */
			size_type mCapacity;
			/*! Pointer to the array of objects */
			pointer mData;
			/*! The allocator */
			allocator_type mAllocator;
	};
}

/*@}*/

#endif
