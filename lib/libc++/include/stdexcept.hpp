/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_STDEXCEPT
#define LIBCPP_STDEXCEPT

/*! \addtogroup libcpp libc++ */
/*@{*/

#include <string>

namespace std
{
	/*! Standard exception class */
	class exception
	{
		public:
			inline exception() throw(){}
			exception(const exception &) throw();
			exception &operator = (const exception &) throw();
			inline virtual ~exception() throw(){}
			virtual const char *what() const throw();
		private:
	};
	
	/*! Standard logic_error class */
	class logic_error
	{
		public:
			inline explicit logic_error(const char *what_arg){s = what_arg;}
			//inline explicit logic_error(const string &what_arg)
			//	: error(what_arg){}
			inline virtual ~logic_error() throw(){}
			virtual const char *what() const throw();
		private:
			const char *s;
			//std::string error;
	};
}

/*@}*/

#endif
