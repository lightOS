/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_IOS
#define LIBCPP_IOS

/*! \addtogroup libcpp libc++ */
/*@{*/

#include <string>
#include <iosfwd>

namespace std
{
	/*! Standard ios_base class */
	class ios_base
	{
		public:
			enum __fmtflags
			{
				__dec			= 0x01,
				__hex			= 0x02,
				__oct			= 0x04,
				__basefield		= 0x07
			};
			typedef __fmtflags fmtflags;
			static const fmtflags dec = __dec;
			static const fmtflags hex = __hex;
			static const fmtflags oct = __oct;
			static const fmtflags basefield = __basefield;
			
			inline fmtflags flags() const{return _fmtflags;}
			fmtflags flags(fmtflags fmtfl);
			fmtflags setf(fmtflags fmtfl);
			fmtflags setf(fmtflags fmtfl, fmtflags mask);
		protected:
			inline virtual ~ios_base(){}
		private:
			fmtflags _fmtflags;
	};
	
	ios_base &dec(ios_base &base);
	ios_base &hex(ios_base &base);
	ios_base &oct(ios_base &base);
	
	/*! | operator for ios_base::fmtflags */
	ios_base::fmtflags operator | (ios_base::fmtflags fl1, ios_base::fmtflags fl2);
	/*! & operator for ios_base::fmtflags */
	ios_base::fmtflags operator & (ios_base::fmtflags fl1, ios_base::fmtflags fl2);
	/*! ~ operator for ios_base::fmtflags */
	ios_base::fmtflags operator ~ (ios_base::fmtflags fl);
	
	/*! Standard basic_ios class template */
	template<class charT, class traits=std::char_traits<charT> >
	class basic_ios : public ios_base
	{
		public:
			typedef charT char_type;
			typedef traits traits_type;
			
			inline virtual ~basic_ios(){}
	};
	
	class ios : public basic_ios<char>{};
}

/*@}*/

#endif
