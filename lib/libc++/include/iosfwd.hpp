/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_IOS
#define LIBCPP_IOS

/*! \addtogroup libcpp libc++ */
/*@{*/

namespace std
{
	template<class charT>class char_traits;
	template<>class char_traits<char>;
	template<>class char_traits<wchar_t>;
	
	template<class T>class allocator;
	
	template<class charT, class traits = char_traits<charT> >class basic_ios;
	
	template<class charT, class traits = char_traits<charT> >class basic_streambuf;
	
	template<class charT, class traits = char_traits<charT> >class basic_istream;
	template<class charT, class traits = char_traits<charT> >class basic_ostream;
	template<class charT, class traits = char_traits<charT> >class basic_iostream;
	
	template<class charT, class traits = char_traits<charT>, class Allocator = allocator<charT> >
	
	typedef basic_ios<char> ios;
	typedef basic_ios<wchar_t> wios;
	
	typedef basic_streambuf<char> streambuf;
	typedef basic_istream<char> istream;
	typedef basic_ostream<char> ostream;
	typedef basic_iostream<char> iostream;
	
	typedef basic_streambuf<wchar_t> wstreambuf;
	typedef basic_istream<wchar_t> wistream;
	typedef basic_ostream<wchar_t> wostream;
	typedef basic_iostream<wchar_t> wiostream;
}

/*@}*/

#endif
