/*
lightOS libc++
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_SSTREAM
#define LIBCPP_SSTREAM

/*! \addtogroup libcpp libc++ */
/*@{*/

#include <string>
#include <streambuf>

namespace std
{
	/*! Standard basic_stringbuf  */
	template<class charT, class traits, class Allocator>
	class basic_stringbuf : public basic_streambuf<charT, traits>
	{
		public:
			// Types
			using basic_streambuf<charT, traits>::char_type;
			using basic_streambuf<charT, traits>::int_type;
			using basic_streambuf<charT, traits>::pos_type;
			using basic_streambuf<charT, traits>::off_type;
			using basic_streambuf<charT, traits>::traits_type;
			
			// Constructors
			explicit basic_stringbuf(ios_base::openmode which = ios_base::in | ios_base::out);
			explicit basic_stringbuf(	const basic_string<charT, traits, Allocator> &str,
										ios_base::openmode which = ios_base::in | ios_base::out);
			
			// Get and Set
			basic_string<charT, traits, Allocator> str() const;
			void str(const basic_string<charT, traits, Allocator> &s);
		
		protected:
			// Overridden virtual functions
			virtual int_type underflow();
			virtual int_type pbackfail(int_type c = traits::eof());
			virtual int_type overflow(int_type c = traits::eof());
			virtual basic_streambuf<charT, traits> *setbuf(charT *, streamsize);
			virtual pos_type seekoff(	off_type off,
										ios_base::seekdir way,
										ios_base::openmode which = ios_base::in | ios_base::out);
			virtual pos_type seekpos(	pos_type sp,
										ios_base::openmode which = ios_base::in | ios_base::out);
		
		private:
			ios_base::openmode mode;
	};
};

/*@}*/

#endif
