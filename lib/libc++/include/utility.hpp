/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_UTILITY
#define LIBCPP_UTILITY

/*! \addtogroup libcpp libc++ */
/*@{*/

namespace std
{
	// 20.2.1 Operators
	
	namespace rel_ops
	{
		template<class T>
		bool operator != (const T &x, const T &y)
		{
			return !(x == y);
		}
		template<class T>
		bool operator > (const T &x, const T &y)
		{
			return (y < x);
		}
		template<class T>
		bool operator <= (const T &x, const T &y)
		{
			return !(y < x);
		}
		template<class T>
		bool operator >= (const T &x, const T &y)
		{
			return !(x < y);
		}
	}
	
	// 20.2.2 Pairs
	
	/*! The std::pair template class */
	template<class Type1, class Type2>
	struct pair
	{
		typedef Type1 first_type;
		typedef Type2 second_type;
		
		/*! The constructor */
		inline pair()
			: first(Type1()), second(Type2()){}
		/*! The constructor
		 *\param[in] First the first varaible
		 *\param[in] Second the second variable */
		inline pair(const first_type &x, const second_type &y)
			: first(x), second(y){}
		/*! The copy-constructor */
		template<class U, class V>
		inline pair(const pair<U, V> &p)
			: first(p.first), second(p.second){}
		
		/*! First variable */
		first_type first;
		/*! Second variable */
		second_type second;
	};
	
	/*! The == operator */
	template<class T1, class T2>
	bool operator == (const pair<T1, T2> &x, const pair<T1, T2> &y)
	{
		return ((x.first == y.first) && (x.second == y.second));
	}
	/*! The < operator */
	template<class T1, class T2>
	bool operator < (const pair<T1, T2> &x, const pair<T1, T2> &y)
	{
		return ((x.first < y.first) || (!(y.first < x.first) && x.second < y.second));
	}
	
	/*! Make a template */
	template<class Type1, class Type2>
	pair<Type1, Type2> make_pair(const Type1 &t1, const Type2 &t2)
	{
		return pair<Type1, Type2>(t1, t2);
	}
	
	
	
	// NOTE HACK FIXME: Not standard
	/*! The std::triple template class */
	template<class Type1, class Type2, class Type3>
	struct triple
	{
		typedef Type1 first_type;
		typedef Type2 second_type;
		typedef Type3 third_type;
		
		/*! The constructor */
		inline triple()
			: first(), second(), third(){}
		/*! The constructor
		 *\param[in] x the first varaible
		 *\param[in] y the second variable
		 *\param[in] z the third variable*/
		inline triple(const first_type &x, const second_type &y, const third_type &z)
			: first(x), second(y), third(z){}
		/*! The copy-constructor */
		template<class U, class V, class W>
		inline triple(const triple<U, V, W> &p)
			: first(p.first), second(p.second()), third(p.third){}
		
		/*! First variable */
		first_type first;
		/*! Second variable */
		second_type second;
		/*! Third variable */
		third_type third;
	};
}

/*@}*/

#endif
