/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_ITERATOR
#define LIBCPP_ITERATOR

/*! \addtogroup libcpp libc++ */
/*@{*/

#include <cstddef>
#include <memory>

//TODO: Iterator tag structures

namespace std
{
	struct input_iterator_tag{};
	struct output_iterator_tag{};
	struct forward_iterator_tag{};
	struct bidirectional_iterator_tag{};
	struct random_access_iterator_tag{};
	
	template<class T, class Distance>
	struct input_iterator{};
	
	struct output_iterator{};
	
	template<class T, class Distance>
	struct forward_iterator{};
	
	template<class T, class Distance>
	struct bidirectional_iterator{};
	
	template<class T, class Distance>
	struct random_access_iterator{};
	
	/*! The constant iterator for sequentual containter (Currently bidirectional)
	*\todo Define a type distance and make it a random-access iterator */
	template<class T, class Container>
	class const_sequential_iterator
	{
		public:
			/*! The constructor
			 *\param[in] x reference to the mContainertainer
			 *\param[in] off offset into the mContainertainer*/
			inline const_sequential_iterator(const Container &x, size_t off)
				: mContainer(x), mOffset(off){}
			/*! The copy-constructor */
			inline const_sequential_iterator(const const_sequential_iterator &x)
				: mContainer(x.mContainer), mOffset(x.mOffset){}
			/*! The == operator */
			inline bool operator == (const const_sequential_iterator &x) const
			{
				if (&mContainer != &x.mContainer)return false;
				if (mOffset != x.mOffset)return false;
				return true;
			}
			/*! The != operator */
			inline bool operator != (const const_sequential_iterator &x) const{return !(*this == x);}
			/*! The unary * operator */
			inline const T &operator *()
			{
				return mContainer[realOffset()];
			}
			/*! The -> operator */
			inline const T &operator ->()
			{
				return mContainer[realOffset()];
			}
			/*! The prefix ++ operator */
			inline const_sequential_iterator &operator ++ ()
			{
				++mOffset;
				return *this;
			}
			/*! The prefix -- operator */
			inline const_sequential_iterator &operator -- ()
			{
				--mOffset;
				return *this;
			}
			/*! The postfix ++ operator */
			inline const_sequential_iterator operator ++ (int)
			{
				const_sequential_iterator tmp(*this);
				++(*this);
				return tmp;
			}
			/*! The postfix -- operator */
			inline const_sequential_iterator operator -- (int)
			{
				const_sequential_iterator tmp(*this);
				--(*this);
				return tmp;
			}
			/*! The += operator */
			inline const_sequential_iterator &operator += (int n)
			{
				if (n >= 0)while(n--)++(*this);
				else while(n++)--(*this);
				return *this;
			}
			/*! The -= operator */
			inline const_sequential_iterator &operator -= (int n){return (*this += -n);}
			/*! The + operator */
			inline const_sequential_iterator operator + (int n)
			{
				const_sequential_iterator tmp(*this);
				tmp += n;
				return tmp;
			}
			/*! The - operator */
			inline const_sequential_iterator operator - (int n)
			{
				return (*this + (-n));
			}
			/*! Non-standard: Get the current offset */
			inline size_t getOffset() const{return mOffset;}
			/*! Get the real offset */
			inline size_t realOffset() const
			{
				if (mOffset == 0)return 0;
				//TODO: Throw exception?
				//if (mContainer.size == 0)return 0;
				return (mOffset % mContainer.size());
			}
			/*! Get the container */
			const Container &container() const{return mContainer;}
			/*! Get the offset */
			const size_t &offset() const{return mOffset;}
			/*! Get the offset */
			size_t &offset(){return mOffset;}
		protected:
			/*! Get the container */
			Container &container(){return const_cast<Container&>(mContainer);}
			/*! The = operator */
			inline const_sequential_iterator &operator = (const const_sequential_iterator &x)
			{
				mOffset = x.mOffset;
				mContainer = x.mContainer;
				return *this;
			}
		private:
			/*! Reference to the Container */
			const Container &mContainer;
			/*! Current offset */
			size_t mOffset;
	};
	
	/*! The iterator for a sequential container (Currently bidirectional)
	 *\todo Define a type distance and make it a random-access iterator */
	template<class T, class Container>
	class sequential_iterator : public const_sequential_iterator<T, Container>
	{
		public:
			/*! The constructor
			 *\param[in] x reference to the mContainertainer
			 *\param[in] off offset into the mContainertainer*/
			inline sequential_iterator(Container &x, size_t off)
				: const_sequential_iterator<T, Container>(x, off){}
			/*! The copy-constructor */
			inline sequential_iterator(const sequential_iterator &x)
				: const_sequential_iterator<T, Container>(x){}
			/*! The copy-constructor */
			inline sequential_iterator(const const_sequential_iterator<T, Container> &x)
				: const_sequential_iterator<T, Container>(x){}
			/*! The = operator */
			inline sequential_iterator &operator = (const sequential_iterator &x)
			{
				const_sequential_iterator<T, Container>::container() = x.const_sequential_iterator<T, Container>::container();
				const_sequential_iterator<T, Container>::offset() = x.const_sequential_iterator<T, Container>::offset();
				return *this;
			}
			/*! The = operator */
			inline sequential_iterator &operator = (const const_sequential_iterator<T, Container> &x)
			{
				const_sequential_iterator<T, Container>::container() = x.container();
				const_sequential_iterator<T, Container>::offset() = x.offset();
				return *this;
			}
			/*! The unary * operator */
			inline T &operator *()
			{
				return const_sequential_iterator<T, Container>::container()[const_sequential_iterator<T, Container>::realOffset()];
			}
			/*! The -> operator */
			inline T &operator ->()
			{
				return const_sequential_iterator<T, Container>::container()[const_sequential_iterator<T, Container>::realOffset()];
			}
			/*! The prefix ++ operator */
			inline sequential_iterator &operator ++ ()
			{
				++const_sequential_iterator<T, Container>::offset();
				return *this;
			}
			/*! The prefix -- operator */
			inline sequential_iterator &operator -- ()
			{
				--const_sequential_iterator<T, Container>::offset();
				return *this;
			}
			/*! The postfix ++ operator */
			inline sequential_iterator operator ++ (int)
			{
				sequential_iterator tmp(*this);
				++(*this);
				return tmp;
			}
			/*! The postfix -- operator */
			inline sequential_iterator operator -- (int)
			{
				sequential_iterator tmp(*this);
				--(*this);
				return tmp;
			}
			/*! The += operator */
			inline sequential_iterator &operator += (int n)
			{
				if (n >= 0)while(n--)++(*this);
				else while(n++)--(*this);
				return *this;
			}
			/*! The -= operator */
			inline sequential_iterator &operator -= (int n){return (*this += -n);}
			/*! The + operator */
			inline sequential_iterator operator + (int n)
			{
				sequential_iterator tmp(*this);
				tmp += n;
				return tmp;
			}
			/*! The - operator */
			inline sequential_iterator operator - (int n)
			{
				return (*this + (-n));
			}
	};
	
	/*! The constant reverse iterator  */
	template<class T, class Iterator>
	class const_reverse_iterator
	{
		public:
			/*! The constructor */
			inline const_reverse_iterator(){}
			/*! The constructor */
			inline const_reverse_iterator(const Iterator &x)
				: mIterator(x){}
			/*! The copy-constructor */
			inline const_reverse_iterator(const const_reverse_iterator &x)
				: mIterator(x.mIterator){}
			/*! The == operator */
			inline bool operator == (const const_reverse_iterator &x) const
			{
				if (mIterator != x.mIterator)return false;
				return true;
			}
			/*! The != operator */
			inline bool operator != (const const_reverse_iterator &x) const{return !(*this == x);}
			/*! The unary * operator */
			inline const T &operator *() 
			{
				return *mIterator;
			}
			/*! The -> operator */
			inline const T &operator ->()
			{
				return *mIterator;
			}
			/*! The prefix ++ operator */
			inline const_reverse_iterator &operator ++ ()
			{
				--mIterator;
				return *this;
			}
			/*! The prefix -- operator */
			inline const_reverse_iterator &operator -- ()
			{
				++mIterator;
				return *this;
			}
			/*! The postfix ++ operator */
			inline const_reverse_iterator operator ++ (int)
			{
				const_reverse_iterator tmp(*this);
				++(*this);
				return tmp;
			}
			/*! The postfix -- operator */
			inline const_reverse_iterator operator -- (int)
			{
				const_reverse_iterator tmp(*this);
				--(*this);
				return tmp;
			}
			/*! The += operator */
			inline const_reverse_iterator &operator += (int n)
			{
				if (n >= 0)while(n--)++(*this);
				else while(n++)--(*this);
				return *this;
			}
			/*! The -= operator */
			inline const_reverse_iterator &operator -= (int n){return (*this += -n);}
			/*! The + operator */
			inline const_reverse_iterator operator + (int n)
			{
				const_reverse_iterator tmp(*this);
				tmp += n;
				return tmp;
			}
			/*! The - operator */
			inline const_reverse_iterator operator - (int n)
			{
				return (*this + (-n));
			}
		protected:
			Iterator mIterator;
	};
	
	/*! The reverse iterator */
	template<class T, class Iterator>
	class reverse_iterator : public const_reverse_iterator<T, Iterator>
	{
		public:
			/*! The constructor */
			inline reverse_iterator(){}
			/*! The constructor */
			inline reverse_iterator(const Iterator &x)
				: const_reverse_iterator<T, Iterator>(x){}
			/*! The copy-constructor */
			inline reverse_iterator(const reverse_iterator &x)
				: const_reverse_iterator<T, Iterator>(x){}
			/*! The copy-constructor */
			inline reverse_iterator(const const_reverse_iterator<T, Iterator> &x)
				: const_reverse_iterator<T, Iterator>(x){}
			/*! The = operator */
			inline reverse_iterator &operator = (const reverse_iterator &x)
			{
				const_reverse_iterator<T, Iterator>::mIterator = x.reverse_iterator<T, Iterator>::mIterator;
				return *this;
			}
			/*! The = operator */
			inline reverse_iterator &operator = (const const_reverse_iterator<T, Iterator> &x)
			{
				const_reverse_iterator<T, Iterator>::mIterator = x.mIterator;
				return *this;
			}
			/*! The unary * operator */
			inline T &operator *()
			{
				return *const_reverse_iterator<T, Iterator>::mIterator;
			}
			/*! The -> operator */
			inline T &operator ->()
			{
				return *const_reverse_iterator<T, Iterator>::mIterator;
			}
			/*! The prefix ++ operator */
			inline reverse_iterator &operator ++ ()
			{
				const_reverse_iterator<T, Iterator>::operator ++();
				return *this;
			}
			/*! The prefix -- operator */
			inline reverse_iterator &operator -- ()
			{
				const_reverse_iterator<T, Iterator>::operator --();
				return *this;
			}
			/*! The postfix ++ operator */
			inline reverse_iterator operator ++ (int)
			{
				reverse_iterator tmp(*this);
				++(*this);
				return tmp;
			}
			/*! The postfix -- operator */
			inline reverse_iterator operator -- (int)
			{
				reverse_iterator tmp(*this);
				--(*this);
				return tmp;
			}
			/*! The += operator */
			inline reverse_iterator &operator += (int n)
			{
				if (n >= 0)while(n--)++(*this);
				else while(n++)--(*this);
				return *this;
			}
			/*! The -= operator */
			inline reverse_iterator &operator -= (int n){return (*this += -n);}
			/*! The + operator */
			inline reverse_iterator operator + (int n)
			{
				reverse_iterator tmp(*this);
				tmp += n;
				return tmp;
			}
			/*! The - operator */
			inline reverse_iterator operator - (int n)
			{
				return (*this + (-n));
			}
	};
}

/*@}*/

#endif
