/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_NEW
#define LIBCPP_NEW

/*! \addtogroup libcpp libc++ */
/*@{*/

void *operator new (unsigned long size) throw();
void *operator new[] (unsigned long size) throw();
void *operator new (unsigned long size, void* memory) throw();
void *operator new[] (unsigned long size, void* memory) throw();
void operator delete (void * p);
void operator delete[] (void * p);
void operator delete (void *p, void *q);
void operator delete[] (void *p, void *q);

/*@}*/

#endif
