/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_STREAMBUF
#define LIBCPP_STREAMBUF

/*! \addtogroup libcpp libc++ */
/*@{*/

#include <ios>

namespace std
{
	/*! Standard basic_streambuf 
	 *\todo locales */
	template<class charT, class traits>
	class basic_streambuf
	{
		public:
			// Types
			typedef charT							char_type;
			typedef typename traits::int_type		int_type;
			typedef typename traits::pos_type		pos_type;
			typedef typename traits::off_type		off_type;
			typedef traits							traits_type;
			
			//! The constructor
			inline basic_streambuf()
				: mOutBegin(), mOutNext(), mOutEnd(){}
			//! The destructor
			inline virtual ~basic_streambuf(){}
			
			int_type sputc(char_type c)
			{
				if (mOutNext == 0 ||
					mOutNext >= mOutNext)return overflow(traits::to_int_type(c));
				*mOutNext = c;
				++mOutNext;
				return traits::to_int_type(c);
			}
			inline int pubsync(){return sync();}
		protected:
			virtual int sync(){return -1;}
			virtual int_type overflow(int_type c = traits::eof()){return traits_type::eof();}
		private:
			char_type *mOutBegin;
			char_type *mOutNext;
			char_type *mOutEnd;
	};
}

/*@}*/

#endif
