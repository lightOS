/*
lightOS libc++
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_LIMITS
#define LIBCPP_LIMITS

/*! \addtogroup libcpp libc++ */
/*@{*/

#include <climits>

namespace std
{
	/*! The rounding mode for floating point arithmetic */
	enum float_round_style
	{
		/*! The rounding style is indeterminable */
		round_indeterminate			= -1,
		/*! The rounding style is towards zero */
		round_towards_zero			= 0,
		/*! The rounding style is to the nearest representable value */
		round_to_nearest			= 1,
		/*! The rounding style is toward infinity */
		round_toward_infinity		= 2,
		/*! The rounding style is toward negative infinity */
		round_toward_neg_infinity	= 3,
	};
	
	/*! The presence or absence of denormalization */
	enum float_denorm_style
	{
		/*! It cannot be determined whether or not the type allows denormalized values */
		denorm_indeterminate		= -1,
		/*! The type does not allow denormalized values */
		denorm_absent				= 0,
		/*! The type does allow denormalized values */
		denorm_present				= 1,
	};
	
	/*! Default numeric_limits class template */
	template<class T>class numeric_limits
	{
		public:
			static const bool is_specialized = false;
			static T min() throw(){return 0;}
			static T max() throw(){return 0;}
			
			static const int digits = 0;
			static const int digits10 = 0;
			static const bool is_signed = false;
			static const bool is_integer = false;
			static const bool is_exact = false;
			static const int radix = 0;
			static T epsilon() throw(){return 0;}
			static T round_error() throw(){return 0;}
			
			static const bool has_infinity = false;
			static const bool has_quiet_NaN = false;
			static const bool has_signaling_NaN = false;
			static const float_denorm_style has_denorm = denorm_absent;
			static const bool has_denorm_loss = false;
			static T infinity() throw(){return 0;}
			static T quiet_NaN() throw(){return 0;}
			static T signaling_NaN() throw(){return 0;}
			static T denorm_min() throw(){return 0;}
			
			static const bool is_iec559 = false;
			static const bool is_bounded = false;
			static const bool is_modulo = false;
			
			static const bool traps = false;
			static const bool tinyness_before = false;
			static const float_round_style round_style = round_toward_zero;
	};
	
	// TODO numeric_limits<bool>
	
	/*! Specialization of the numeric_limits class template for char */
	template<>class numeric_limits<char>
	{
		public:
			static const bool is_specialized = true;
			static T min() throw(){return CHAR_MIN;}
			static T max() throw(){return CHAR_MAX;}
			
			/*static const int digits = 0;
			static const int digits10 = 0;*/
			static const bool is_signed = (_LIBC_SIGNED_CHAR == 1) ? true : false;
			static const bool is_integer = true;
			/*static const bool is_exact = false;
			static const int radix = 0;*/
			
			static const bool is_iec559 = false;
			static const bool is_bounded = true;
			static const bool is_modulo = true;
			
			/*static const bool traps = false;
			static const bool tinyness_before = false;
			static const float_round_style round_style = round_toward_zero;*/
	};
	
	/*! Specialization of the numeric_limits class template for signed char */
	template<>class numeric_limits<signed char>
	{
		public:
			static const bool is_specialized = true;
			static T min() throw(){return SCHAR_MIN;}
			static T max() throw(){return SCHAR_MAX;}
			
			/*static const int digits = 0;
			static const int digits10 = 0;*/
			static const bool is_signed = true;
			static const bool is_integer = true;
			/*static const bool is_exact = false;
			static const int radix = 0;*/
			
			static const bool is_iec559 = false;
			static const bool is_bounded = true;
			static const bool is_modulo = true;
			
			/*static const bool traps = false;
			static const bool tinyness_before = false;
			static const float_round_style round_style = round_toward_zero;*/
	};
	
	/*! Specialization of the numeric_limits class template for unsigned char */
	template<>class numeric_limits<unsigned char>
	{
		public:
			static const bool is_specialized = true;
			static T min() throw(){return 0;}
			static T max() throw(){return UCHAR_MAX;}
			
			/*static const int digits = 0;
			static const int digits10 = 0;*/
			static const bool is_signed = false;
			static const bool is_integer = true;
			/*static const bool is_exact = false;
			static const int radix = 0;*/
			
			static const bool is_iec559 = false;
			static const bool is_bounded = true;
			static const bool is_modulo = true;
			
			/*static const bool traps = false;
			static const bool tinyness_before = false;
			static const float_round_style round_style = round_toward_zero;*/
	};
	
	// TODO wchar_t
	
	/*! Specialization of the numeric_limits class template for short */
	template<>class numeric_limits<short>
	{
		public:
			static const bool is_specialized = true;
			static T min() throw(){return SHRT_MIN;}
			static T max() throw(){return SHRT_MAX;}
			
			/*static const int digits = 0;
			static const int digits10 = 0;*/
			static const bool is_signed = true;
			static const bool is_integer = true;
			/*static const bool is_exact = false;
			static const int radix = 0;*/
			
			static const bool is_iec559 = false;
			static const bool is_bounded = true;
			static const bool is_modulo = true;
			
			/*static const bool traps = false;
			static const bool tinyness_before = false;
			static const float_round_style round_style = round_toward_zero;*/
	};
	
	/*! Specialization of the numeric_limits class template for int */
	template<>class numeric_limits<int>
	{
		public:
			static const bool is_specialized = true;
			static T min() throw(){return INT_MIN;}
			static T max() throw(){return INT_MAX;}
			
			/*static const int digits = 0;
			static const int digits10 = 0;*/
			static const bool is_signed = true;
			static const bool is_integer = true;
			/*static const bool is_exact = false;
			static const int radix = 0;*/
			
			static const bool is_iec559 = false;
			static const bool is_bounded = true;
			static const bool is_modulo = true;
			
			/*static const bool traps = false;
			static const bool tinyness_before = false;
			static const float_round_style round_style = round_toward_zero;*/
	};
	
	/*! Specialization of the numeric_limits class template for long */
	template<>class numeric_limits<long>
	{
		public:
			static const bool is_specialized = true;
			static T min() throw(){return LONG_MIN;}
			static T max() throw(){return LONG_MAX;}
			
			/*static const int digits = 0;
			static const int digits10 = 0;*/
			static const bool is_signed = true;
			static const bool is_integer = true;
			/*static const bool is_exact = false;
			static const int radix = 0;*/
			
			static const bool is_iec559 = false;
			static const bool is_bounded = true;
			static const bool is_modulo = true;
			
			/*static const bool traps = false;
			static const bool tinyness_before = false;
			static const float_round_style round_style = round_toward_zero;*/
	};
	
	/*! Specialization of the numeric_limits class template for long long */
	template<>class numeric_limits<long long>
	{
		public:
			static const bool is_specialized = true;
			static T min() throw(){return LLONG_MIN;}
			static T max() throw(){return LLONG_MAX;}
			
			/*static const int digits = 0;
			static const int digits10 = 0;*/
			static const bool is_signed = true;
			static const bool is_integer = true;
			/*static const bool is_exact = false;
			static const int radix = 0;*/
			
			static const bool is_iec559 = false;
			static const bool is_bounded = true;
			static const bool is_modulo = true;
			
			/*static const bool traps = false;
			static const bool tinyness_before = false;
			static const float_round_style round_style = round_toward_zero;*/
	};
	
	/*! Specialization of the numeric_limits class template for short */
	template<>class numeric_limits<unsigned short>
	{
		public:
			static const bool is_specialized = true;
			static T min() throw(){return 0;}
			static T max() throw(){return USHRT_MAX;}
			
			/*static const int digits = 0;
			static const int digits10 = 0;*/
			static const bool is_signed = false;
			static const bool is_integer = true;
			/*static const bool is_exact = false;
			static const int radix = 0;*/
			
			static const bool is_iec559 = false;
			static const bool is_bounded = true;
			static const bool is_modulo = true;
			
			/*static const bool traps = false;
			static const bool tinyness_before = false;
			static const float_round_style round_style = round_toward_zero;*/
	};
	
	/*! Specialization of the numeric_limits class template for int */
	template<>class numeric_limits<unsigned int>
	{
		public:
			static const bool is_specialized = true;
			static T min() throw(){return 0;}
			static T max() throw(){return UINT_MAX;}
			
			/*static const int digits = 0;
			static const int digits10 = 0;*/
			static const bool is_signed = false;
			static const bool is_integer = true;
			/*static const bool is_exact = false;
			static const int radix = 0;*/
			
			static const bool is_iec559 = false;
			static const bool is_bounded = true;
			static const bool is_modulo = true;
			
			/*static const bool traps = false;
			static const bool tinyness_before = false;
			static const float_round_style round_style = round_toward_zero;*/
	};
	
	/*! Specialization of the numeric_limits class template for unsigned long */
	template<>class numeric_limits<unsigned long>
	{
		public:
			static const bool is_specialized = true;
			static T min() throw(){return 0;}
			static T max() throw(){return ULONG_MAX;}
			
			/*static const int digits = 0;
			static const int digits10 = 0;*/
			static const bool is_signed = false;
			static const bool is_integer = true;
			/*static const bool is_exact = false;
			static const int radix = 0;*/
			
			static const bool is_iec559 = false;
			static const bool is_bounded = true;
			static const bool is_modulo = true;
			
			/*static const bool traps = false;
			static const bool tinyness_before = false;
			static const float_round_style round_style = round_toward_zero;*/
	};
	
	/*! Specialization of the numeric_limits class template for unsigned long long */
	template<>class numeric_limits<unsigned long long>
	{
		public:
			static const bool is_specialized = true;
			static T min() throw(){return 0;}
			static T max() throw(){return ULLONG_MAX;}
			
			/*static const int digits = 0;
			static const int digits10 = 0;*/
			static const bool is_signed = false;
			static const bool is_integer = true;
			/*static const bool is_exact = false;
			static const int radix = 0;*/
			
			static const bool is_iec559 = false;
			static const bool is_bounded = true;
			static const bool is_modulo = true;
			
			/*static const bool traps = false;
			static const bool tinyness_before = false;
			static const float_round_style round_style = round_toward_zero;*/
	};
	
	// TODO float
	// TODO double
	// TODO long double
}

/*@}*/

#endif
