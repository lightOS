/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_OSTREAM
#define LIBCPP_OSTREAM

/*! \addtogroup libcpp libc++ */
/*@{*/

#include <ios>
#include <string>
#include <cstdlib>
#include <streambuf>

namespace std
{
	/*! Standard basic_ostream class template */
	template<class charT, class traits=char_traits<charT> >
	class basic_ostream : virtual public std::basic_ios<charT, traits>
	{
		public:
			inline virtual ~basic_ostream(){}
			inline basic_ostream &operator << (short n)
				{return *this << static_cast<long>(n);}
			inline basic_ostream &operator << (unsigned short n)
				{return *this << static_cast<unsigned long>(n);}
			inline basic_ostream &operator << (int n)
				{return *this << static_cast<long>(n);}
			inline basic_ostream &operator << (unsigned int n)
				{return *this << static_cast<unsigned long>(n);}
			inline basic_ostream &operator << (long n)
			{
				unsigned int base = 10;
				if ((ios_base::flags() & ios_base::dec) == ios_base::dec)base = 10;
				else if ((ios_base::flags() & ios_base::hex) == ios_base::hex)base = 16;
				else if ((ios_base::flags() & ios_base::oct) == ios_base::oct)base = 8;
				charT value[32];
				ltostr(n, value, value + 32, base);
				return *this << value;
			}
			inline basic_ostream &operator << (unsigned long n)
			{
				unsigned int base = 10;
				if ((ios_base::flags() & ios_base::dec) == ios_base::dec)base = 10;
				else if ((ios_base::flags() & ios_base::hex) == ios_base::hex)base = 16;
				else if ((ios_base::flags() & ios_base::oct) == ios_base::oct)base = 8;
				charT value[32];
				ultostr(n, value, value + 32, base);
				return *this << value;
			}
			virtual basic_ostream &operator << (const char c)=0;
			virtual basic_ostream &operator << (const unsigned char c)
				{return (*this << static_cast<const char>(c));}
			virtual basic_ostream &operator << (const char *s)=0;
			virtual basic_ostream &operator << (const unsigned char *s)
				{return (*this << reinterpret_cast<const char *>(s));}
			virtual void flush()=0;
			inline basic_ostream &operator << (ios_base &(*pf)(ios_base &))
			{
				pf(*this);
				return *this;
			}
			inline basic_ostream &operator << (basic_ostream &(*pf)(basic_ostream &))
			{
				return pf(*this);
			}
	};
	
	/*! Define the standard ostream class */
	typedef basic_ostream<char> ostream;
	
	/*! Define the standard wostream class */
	typedef basic_ostream<wchar_t> wostream;
	
	ostream &endl(ostream &os);
	wostream &endl(wostream &wos);
	
	template<class T>
	inline basic_ostream<T> &operator << (basic_ostream<T> &os, const basic_string<T> &s)
	{
		if (s.c_str() != 0)os << s.c_str();
		return os;
	}
}

/*@}*/

#endif
