/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_TYPEINFO
#define LIBCPP_TYPEINFO

namespace std
{
	class type_info
	{
		public:
			/*! The == operator */
			inline bool operator == (const type_info &t) const{return (this == &t);}
			/*! The != operator */
			inline bool operator != (const type_info &t) const{return (this == &t);}
			/*!\todo */
			bool before(const type_info &) const;
			/*! Get the class name */
			inline const char* name() const{return _name;}
			/*! The destructor */
			virtual ~type_info();
		protected:
			/*! The constructor */
			explicit type_info(const char *n): _name(n){}
			/*! The constructor */
			inline type_info(){}
			/*! The copy-constructor  */
			type_info(const type_info &rhs);
		private:
			/*! The type name */
			const char *_name;
			/*! The = operator */
			type_info &operator = (const type_info &rhs);
	};
}

#endif
