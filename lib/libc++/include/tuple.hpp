/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_TUPLE
#define LIBCPP_TUPLE

/*! \addtogroup libcpp libc++ */
/*@{*/

namespace std
{
/*	template<	class T1, class T2, class T3 = void, class T4 = void, class T5 = void,
				class T6 = void, class T7 = void, class T8 = void, class T9 = void, class T10 = void>
	class tuple
	{
		public:
			tuple() : t1(), t2(), t3(), t4(), t5(), t6(), t7(), t8(), t9(), t10(){}
			explicit tuple(	const T1 &_t1, const T2 &_t2, const T3 &_t3, const T4 &_t4, const T5 &_t5,
							const T6 &_t6, const T7 &_t7, const T8 &_t8, const T9 &_t9, const T10 &_t10)
				: t1(_t1), t2(_t2), t3(_t3), t4(_t4), t5(_t5), t6(_t6), t7(_t7), t8(_t8), t9(_t9), t10(_t10){}
		private:
			T1 t1;T2 t2;T3 t3;T4 t4;T5 t5;T6 t6;T7 t7;T8 t8;T9 t9;T10 t10;
	};*/
}

/*@}*/

#endif
