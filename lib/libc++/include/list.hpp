/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_LIST
#define LIBCPP_LIST

/*! \addtogroup libcpp libc++ */
/*@{*/

#include <cstddef>
#include <memory>

namespace std
{
	template<class T>
	struct __list_data
	{
		T data;
		struct __list_data<T> *next;
		struct __list_data<T> *prev;
	};
	
	template<class T>
	class __list_iterator
	{
		public:
			/*! The copy-constructor */
			inline __list_iterator(const __list_iterator &x):mData(x.mData){}
			/*! The = operator */
			inline __list_iterator &operator = (const __list_iterator &x)
			{
				mData = x.mData;
				return *this;
			}
			/*! The == operator */
			inline bool operator == (const __list_iterator &x)
			{
				if (mData != x.mData)return false;
				return true;
			}
			/*! The != operator */
			inline bool operator != (const __list_iterator &x){return !(*this == x);}
			/*! The unary * operator */
			inline const T &operator *() const{return mData->data;}
			/*! The unary * operator */
			inline T &operator *(){return mData->data;}
			/*! The -> operator */
			inline T &operator ->(){return mData->data;}
			/*! The -> operator for const objects */
			inline const T &operator ->() const{return mData->data;}
			/*! The prefix ++ operator */
			inline __list_iterator &operator ++ (){mData = mData->next;return *this;}
			/*! The prefix ++ operator */
			inline __list_iterator &operator -- (){mData = mData->prev;return *this;}
			/*! The postfix ++ operator */
			inline __list_iterator operator ++ (int)
			{
				__list_iterator tmp(mData);
				++(*this);
				return tmp;
			}
			/*! The postfix -- operator */
			inline __list_iterator operator -- (int)
			{
				__list_iterator tmp(mData);
				--(*this);
				return tmp;
			}
			inline __list_iterator(__list_data<T> *Data = 0):mData(Data){}
			
			__list_data<T> *mData;
	};
	
	/*!\todo docu */
	template<class T, class Allocator = allocator<T> >
	class list
	{
		public:
			typedef typename Allocator::reference								reference;
			typedef typename Allocator::const_reference							const_reference;
			//TODO
			typedef size_t														size_type;
			//TODO
			typedef T															value_type;
			typedef Allocator													allocator_type;
			typedef typename Allocator::pointer									pointer;
			typedef typename Allocator::const_pointer							const_pointer;
			typedef __list_iterator<T>											iterator;
			typedef const __list_iterator<T>									const_iterator;
			
			inline explicit list()
				: mFront(0), mBack(0), n(0)
			{mEnd.prev = 0;mEnd.next = 0;}
			inline explicit list(size_type n, const T &value = T())
				: mFront(0), mBack(0), n(0), mEnd(0)
			{
				mEnd.prev = 0;
				mEnd.next = 0;
				for (unsigned int i=0;i < n;i++)push_back(value);
			}
			inline iterator begin()
			{
				if (mFront == 0)return iterator(&mEnd);
				return iterator(mFront);
			}
			inline const_iterator begin() const
			{
				if (mFront == 0)return const_iterator(&mEnd);
				return const_iterator(mFront);
			}
			inline iterator end(){return iterator(&mEnd);}
			inline const_iterator end() const{return const_iterator(&mEnd);}
			inline reference front(){return mFront->data;}
			inline const_reference front() const{return mFront->data;}
			inline reference back(){return mBack->data;}
			inline const_reference back() const{return mBack->data;}
			inline size_type size() const{return n;}
			inline bool empty() const{return (n == 0);}
			void pop_back()
			{
				if (mBack != 0)
				{
					__list_data<T> *node = mBack;
					mBack = mBack->prev;
					if (mBack == 0)mFront = 0;
					--n;
					delete node;
					mEnd.prev = mBack;
					if (mBack != 0)mBack->next = &mEnd;
				}
			}
			void pop_front()
			{
				if (mFront != 0)
				{
					__list_data<T> *node = mFront;
					mFront = mFront->next;
					if (mFront == 0)mBack = 0;
					--n;
					delete node;
					mEnd.prev = mBack;
					if (mBack != 0)mBack->next = &mEnd;
				}
			}
			void push_back(const T &x)
			{
				__list_data<T> *node = new __list_data<T>;
				node->data = x;
				node->next = 0;
				node->prev = mBack;
				if (mBack == 0)mFront = node;
				else mBack->next = node;
				mBack = node;
				++n;
				mEnd.prev = mBack;
				if (mBack != 0)mBack->next = &mEnd;
			}
			void push_front(const T &x)
			{
				__list_data<T> *node = new __list_data<T>;
				node->data = x;
				node->next = mFront;
				node->prev = 0;
				if (mFront == 0)mBack = node;
				else mFront->prev = node;
				mFront = node;
				++n;
				mEnd.prev = mBack;
				if (mBack != 0)mBack->next = &mEnd;
			}
			iterator erase(iterator i)
			{
				if (i.mData->prev == 0)
					mFront = i.mData->next;
				else
					i.mData->prev->next = i.mData->next;
				
				if (i.mData->next == 0)
				{
					mBack = i.mData->prev;
				}
				else
					i.mData->next->prev = i.mData->prev;
				
				if (mBack != 0)mBack->next = &mEnd;
				--n;
				
				// TODO free memory, return iterator
				return iterator(0);
			}
			inline ~list()
			{
				//TODO
			}
			
		private:
			__list_data<T> *mFront;
			__list_data<T> *mBack;
			__list_data<T> mEnd;
			size_type n;
	};
}

/*@}*/

#endif
