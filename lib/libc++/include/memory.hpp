/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_MEMORY
#define LIBCPP_MEMORY

/*! \addtogroup libcpp libc++ */
/*@{*/

#include <new>
#include <cstddef>

namespace std
{
	template <class T> class allocator;
	
	/*! Standard allocator template class specialized for void */
	template<>
	class allocator<void>
	{
		public:
			typedef void *			pointer;
			typedef const void *	const_pointer;
			typedef void			value_type;
			
			template<class U>
			struct rebind
			{
				typedef allocator<U> other;
			};
	};
	
	/*! Standard allocator template class */
	template<class T>
	class allocator
	{
		public:
			/*! Typedef value_type */
			typedef T				value_type;
			/*! Typedef pointer */
			typedef T*				pointer;
			/*! Typedef const_pointer */
			typedef const T*		const_pointer;
			/*! Typedef reference */
			typedef T&				reference;
			/*! Typedef const_reference */
			typedef const T&		const_reference;
			/*! Typedef size_type */
			typedef size_t			size_type;
			/*! Typedef difference_type */
			typedef ptrdiff_t		difference_type;
			
			/*! The constructor */
			inline allocator() throw(){}
			/*! The copy-constructor */
			inline allocator(const allocator &a) throw(){}
			/*! Get the address
			 *\param[in] x reference to the object
			 *\return pointer to the object */
			inline pointer address(reference x) const{return &x;}
			/*! Get the address
			 *\param[in] x const reference to the object
			 *\return const pointer to the object */
			inline const_pointer address(const_reference x) const{return &x;}
			/*! Allocate space for n objects
			 *\param[in] n the number of objects
			 *\return pointer to the memory location */
			inline pointer allocate(size_type n, allocator<void>::const_pointer hint = 0)
			{
				return reinterpret_cast<pointer>(::operator new(n * sizeof(T))); 
			}
			/*! Deallocate space for n objects
			 *\param[in] p pointer to the space
			 *\param[in] n the number of objects */
			inline void deallocate(pointer p, size_type n)
			{
				::operator delete(p); 
			}
			//TODO: size_type max_size() const; -> max. size of size_type from numeric_limits
			/*! Construct object
			 *\param[in] p pointer to the memory
			 *\param[in] val value */
			inline void construct(pointer p, const_reference val)
			{
				new (static_cast<void*>(p)) T(val);
			}
			/*! Destruct object
			 *\param[in] p pointer to the object */
			inline void destroy(pointer p)
			{
				p->~T();
			}
			/*! The destructor */
			inline ~allocator() throw(){}
	};
	
	/*! Standard auto_ptr class */
	template<class T>
	class auto_ptr
	{
		public:
			/*! The constructor */
			explicit auto_ptr(T *p = 0) throw()
			{
				instance = p;
			}
			/*! Copy constructor */
			auto_ptr(const auto_ptr &p) throw()
			{
				instance = p.instance;
				const_cast<auto_ptr&>(p).instance = 0;
			}
			/*! The destructor */
			~auto_ptr() throw()
			{
				if (instance != 0)delete instance;
			}
			/*! = operator */
			auto_ptr &operator = (const auto_ptr &p) throw()
			{
				instance = p.instance;
				const_cast<auto_ptr&>(p).instance = 0;
				return *this;
			}
			/*! * operator */
			T &operator* () const throw()
			{
				return *instance;
			}
			/*! -> operator */
			T *operator-> () const throw()
			{
				return instance;
			}
			/*! Get the pointer */
			T *get() const throw(){return instance;}
			/*! Release the pointer */
			T *release() throw()
			{
				T *tmp = instance;
				instance = 0;
				return tmp;
			}
			/*! Set new pointer */
			T *reset(T *p = 0) throw()
			{
				T *tmp = instance;
				instance = p;
				return tmp;
			}
		private:
			/*! The instance */
			T *instance;
	};
	
	/*! auto_array class
	 *\note lightOS libc++ extension */
	template<class T>
	class auto_array
	{
		public:
			/*! The constructor */
			explicit auto_array(T *p = 0) throw()
			{
				instance = p;
			}
			/*! Copy constructor */
			auto_array(const auto_array &p) throw()
			{
				instance = p.instance;
				const_cast<auto_array&>(p).instance = 0;
			}
			/*! The destructor */
			~auto_array() throw()
			{
				if (instance != 0)delete []instance;
			}
			/*! = operator */
			auto_array &operator = (const auto_array &p) throw()
			{
				instance = p.instance;
				const_cast<auto_array&>(p).instance = 0;
				return *this;
			}
			/*! * operator */
			T &operator* () const throw()
			{
				return *instance;
			}
			/*! -> operator */
			T *operator-> () const throw()
			{
				return instance;
			}
			/*! Get the pointer */
			T *get() const throw(){return instance;}
			/*! Release the pointer */
			T *release() throw()
			{
				T *tmp = instance;
				instance = 0;
				return tmp;
			}
			/*! Set new pointer */
			T *reset(T *p = 0) throw()
			{
				T *tmp = instance;
				instance = p;
				return tmp;
			}
		private:
			/*! The instance */
			T *instance;
	};
}

/*@}*/

#endif
