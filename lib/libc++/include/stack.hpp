/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_STACK
#define LIBCPP_STACK

/*! \addtogroup libcpp libc++ */
/*@{*/

namespace std
{
	/*! The standard stack container adapter */
	template<class Container>
	class stack
	{
		public:
			/*! Typedef value_type */
			typedef typename Container::value_type				value_type;
			/*! Typedef size_type */
			typedef typename Container::size_type				size_type;
			/*! Typedef container_type */
			typedef Container									container_type;
			
			/*! The constructor */
			inline explicit stack(const container_type &container = container_type())
				: mContainer(container){}
			/*! Is the container empty? */
			inline bool empty() const{return mContainer.empty();}
			/*! Get the container's size */
			inline size_type size() const{return mContainer.size();}
			/*! Get a reference to the topmost element in the container */
			value_type &top(){return mContainer.back();}
			/*! Get a constant reference to the topmost element in the container */
			const value_type &top() const{return mContainer.back();}
			/*! Push a value */
			void push(const value_type &value){mContainer.push_back(value);}
			/*! Pop the last value */
			void pop(){mContainer.pop_back();}
			
			/*! The == operator */
			inline bool operator == (const stack &x) const{return (mContainer == x.mContainer);}
			/*! The < operator */
			inline bool operator < (const stack &x) const{return (mContainer < x.mContainer);}
			/*! The != operator */
			inline bool operator != (const stack &x) const{return !(*this == x);}
			/*! The > operator */
			inline bool operator > (const stack &x) const{return  (mContainer > x.mContainer);}
			/*! The >= operator */
			inline bool operator >= (const stack &x) const{return (mContainer >= x.mContainer);}
			/*! The <= operator */
			inline bool operator <= (const stack &x) const{return (mContainer <= x.mContainer);}
		private:
			/*! The container */
			container_type mContainer;
	};
}

/*@}*/

#endif
