/*
lightOS libc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIBCPP_STRING
#define LIBCPP_STRING

/*! \addtogroup libcpp libc++ */
/*@{*/

#include <cctype>
#include <cstdint>
//#include <type_traits>
#include <cstring>
#include <memory>
#include <iterator>
#include <algorithm>

//TODO: throw exceptions, when compiling with __LIBCPP_EXCEPTIONS
//TODO: throw extra exceptions, when compiling with __LIBCPP_STRING_EXCEPTIONS

namespace std
{
	/*! Typedef streamoff */
	typedef intmax_t streamoff;
	/*! Typedef streampos */
	typedef uintmax_t streampos;
	
	/*! Base class for char_traits */
	template<class charT, class intT>
	class char_traits_base
	{
		public:
			/*! Typedef char_type */
			typedef charT					char_type;
			/*! Typedef int_type */
			typedef intT					int_type;
			/*! Typedef off_type */
			typedef streamoff				off_type;
			/*! Typedef pos_type */
			typedef streampos				pos_type;
			
			/*! Assign
			 *\param[in] c1 the destination char_type
			 *\param[in] c2 the source char_type */
			static void assign(char_type &c1, char_type &c2)
			{
				c1 = c2;
			}
			/*! Assign s2 to s1 beginning from index 0 to n
			 *\param[in,out] s1 the destination string
			 *\param[in] s2 the character
			 *\param[in] n the number of characters to assign
			 *\return pointer to the destination string (s1) */
			static char_type *assign(char_type *s1, const char_type s2, size_t n)
			{
				for (size_t i=0;i < n;i++)s1[i] = s2;
				return s1;
			}
			/*! Equal char_type?
			 *\param[in] c1 the first character
			 *\param[in] c2 the second character
			 *\return true, if c1 == c2, false otherwise */
			static bool eq(const char_type &c1, const char_type &c2)
			{
				return (c1 == c2);
			}
			/*! Smaller char_type?
			 *\param[in] c1 the first character
			 *\param[in] c2 the second character
			 *\return true, if c1 < c2, false otherwise */
			static bool lt(const char_type &c1, const char_type &c2)
			{
				return (c1 < c2);
			}
			/*! Get the end of file character */
			static int_type eof(){return static_cast<int_type>(-1);}
			/*! Get a character that does not equal eof */
			static int_type not_eof(const int_type &c){return !eq_int_type(c, eof()) ? c : 0;}
			/*! Equal int_type?
			 *\param[in] i1 the first integer type
			 *\param[in] i2 the second integer type
			 *\return true, if i1 == i2, false otherwise */
			static bool eq_int_type(const int_type &i1, const int_type &i2)
			{
				return (i1 == i2);
			}
			/*! Convert int_type to char_type
			 *\param[in] c the integer type
			 *\return the character type */
			static char_type to_char_type(const int_type &c)
			{
				return static_cast<char_type>(c);
			}
			/*! Convert char_type to int_type
			 *\param[in] c the character type
			 *\return the integer type */
			static int_type to_int_type(const char_type &c)
			{
				return static_cast<int_type>(c);
			}
			/*! Move n characters of the string s2 to s1
			 *\param[in] s1 the destination string
			 *\param[in] s2 the source string
			 *\param[in] n the number of moved characters
			 *\return the destination string */
			static char_type* move(char_type *s1, const char_type *s2, size_t n)
			{
				memmove(s1, s2, n * sizeof(char_type));
				return s1;
			}
			//TODO: generic compare
			//TODO: generic length
			//TODO: generic copy
			/*! Find a character in the first n characters of a string
			 *\param[in] s the string
			 *\param[in] n the number of characters
			 *\param[in] a the character to find
			 *\return pointer to the character in the string or 0 */
			static const char_type *find(const char_type *s, size_t n, const char_type &a)
			{
				for (;n > 0;--n)
				{
					if (eq(*s, a))return s;
					++s;
				}
				return 0;
			}
		private:
			char_traits_base();
			char_traits_base(const char_traits_base &);
			virtual ~char_traits_base();
	};
	
	/*! Standard char_traits class */
	template<class charT>
	class char_traits : public char_traits_base<charT, intmax_t>{};
	
	/*! Standard char_traits class for char */
	template<>
	class char_traits<char> : public char_traits_base<char, intmax_t>
	{
		public:
			using char_traits_base<char, intmax_t>::assign;
			/*! Assign s2 to s1 beginning from index 0 to n
			 *\param[in,out] s1 the destination string
			 *\param[in] s2 the character
			 *\param[in] n the number of characters to assign
			 *\return pointer to the destination string (s1) */
			static char_type *assign(char_type *s1, const char_type s2, size_t n){memset(s1, s2, n);return s1;}
			/*! Compare n characters from s1 with s2
			 *\param[in] s1 the first string
			 *\param[in] s2 the second string
			 *\param[in] n the number of characters to compare
			 *\return 0, if s1 == s2 */
			static int compare(const char_type *s1, const char *s2, size_t n){return memcmp(s1, s2, n);}
			/*! Get the string length
			 *\param[in] s the string
			 *\return the string length */
			static size_t length(const char_type *s){return strlen(s);}
			/*! Copy the n characters from the source to the destination string
			 *\param[in] dst the destination string
			 *\param[in] src the source string
			 *\param[in] n the number of copied characters
			 *\return the destination string */
			static char_type *copy(char_type *dst, const char_type *src, size_t n){return strncpy(dst, src, n);}
	};
	
	/*! Standard char_traits class for wchar_t
	 *\todo char_traits for wchar_t */
	template<>
	class char_traits<wchar_t> : public char_traits_base<wchar_t, intmax_t>
	{
		public:
			typedef wchar_t					char_type;
	};
		
	/*! Standard basic_string class */
	template<class charT, class traits=char_traits<charT>, class Allocator=allocator<charT> >
	class basic_string
	{
		public:
			/*! Typedef traits_type */
			typedef traits																		traits_type;
			/*! Typedef value_type */
			typedef typename traits::char_type													value_type;
			/*! Typedef allocator_type */
			typedef Allocator																	allocator_type;
			/*! Typedef size_type */
			typedef typename Allocator::size_type												size_type;
			/*! Typedef difference_type */
			typedef typename Allocator::difference_type											difference_type;
			/*! Typedef reference */
			typedef typename Allocator::reference												reference;
			/*! Typedef const_reference */
			typedef typename Allocator::const_reference											const_reference;
			/*! Typedef pointer */
			typedef typename Allocator::pointer													pointer;
			/*! Typedef const_pointer */
			typedef typename Allocator::const_pointer											const_pointer;
			/*! Typedef iterator */
			typedef sequential_iterator<charT, basic_string<charT, traits, Allocator> >			iterator;
			/*! Typedef const_iterator */
			typedef const_sequential_iterator<charT, basic_string<charT, traits, Allocator> >	const_iterator;
			/*! Typedef const_reverse_iterator */
			typedef std::const_reverse_iterator<charT, iterator>										const_reverse_iterator;
			/*! Typedef reverse_iterator  */
			typedef std::reverse_iterator<charT, iterator>											reverse_iterator;
			/*! Typedef npos */
			static const size_type npos = -1;
			
			/*! The constructor
			 *\param[in] a the allocator */
			inline explicit basic_string(const allocator_type &a = allocator_type())
				: mString(0), mLength(0), mCapacity(0), mAllocator(a){}
			/*! The constructor */
			inline basic_string(const charT *str,
								size_type n = npos,
								const allocator_type &a = allocator_type())
				: mString(0), mLength(0), mCapacity(0), mAllocator(a){assign(str, n);}
			/*! The constructor */
			inline basic_string(size_type n, charT c, const allocator_type &a = allocator_type())
				: mString(0), mLength(n), mCapacity(0), mAllocator(a){assign(n, c);}
			/*! The copy-constructor */
			inline basic_string(const basic_string &str,
								size_type pos = 0,
								size_type n = npos,
								const allocator_type &a = allocator_type())
				: mString(0), mLength(0), mCapacity(0), mAllocator(a){assign(str, pos, n);}
			/*! The constructor */
			template<class InputIterator>
			inline basic_string(InputIterator first, InputIterator last)
				: mString(0), mLength(0), mCapacity(0)
			{
				for (;first != last;first++)
					append(1, *first);
				/*TODO: Specialize this template function for sequential_iterator
				mLength = last.getOffset() - first.getOffset();
				mCapacity = mLength + 1;
				mString = new char[mCapacity];
				
				for (unsigned int i = 0;i < mLength;i++)
				{
					mString[i] = *first++;
				}
				mString[mLength] = '\0';*/
			}
			/*! Reserve at least res_arg of space for the string
			 *\param[in] res_arg minimum capacity */
			void reserve(size_type res_arg = 0, bool copy = true)
			{
				if (mCapacity < res_arg)
				{
					charT *tmp = mString;
					mString = mAllocator.allocate(res_arg * 2);
					if (tmp != 0)
					{
						if (copy == true)traits_type::copy(mString, tmp, mLength+1);
						delete []tmp;
					}
					mCapacity = res_arg * 2;
				}
			}
			/*! Resize the string
			 *\param[in] m new minimum string size
			 *\param[in] c character to fill up the string */
			void resize(size_type m, charT c = charT())
			{
				if (mLength < m)
				{
					reserve(m+1);
					for (size_type i=0;i < (m - mLength);i++)
						traits_type::assign(mString[mLength + i], c);
					mLength = m;
					mString[mLength] = '\0';
				}
			}
			/*! Append n times the character c to the string
			 *\param[in] n the number of times the character should be appended
			 *\param[in] c the character to append */
			basic_string &append(size_type n, charT c)
			{
				reserve(mLength + n + 1);
				traits_type::assign(&mString[mLength], c, n);
				mLength += n;
				mString[mLength] = '\0';
				return *this;
			}
			/*! Append a string to this string
			 *\param[in] s the string, which should be appended
			 *\param[in] pos the position within the string that should be appended
			 *\param[in] n the number of characters to append */
			basic_string &append(	const basic_string &s,
									size_type pos = 0,
									size_type n = npos)
			{
				return append(&s.mString[pos], n);
			}
			/*! Append a character sequence of length characters to this string
			 *\param[in] s the character sequence
			 *\param[in] n the number of characters to append */
			basic_string &append(const charT *s, size_type length = npos)
			{
				if (s == 0)return *this;
				if (length == npos)length = traits_type::length(s);
				reserve(mLength + length + 1);
				traits::copy(&mString[mLength], s, length + 1);
				mLength += length;
				return *this;
			}
			/*! Assign n characters of c to this string
			 *\param[in] n the number of characters
			 *\param[in] c the character */
			basic_string &assign(size_type n, charT c)
			{
				reserve(n+1, false);
				traits_type::assign(mString, c, n);
				mLength = n;
				mString[mLength] = '\0';
				return *this;
			}
			/*! Assign n characters of another string (starting at index pos) to this string
			 *\param[in] s the other string
			 *\param[in] pos the starting position
			 *\param[in] n the number of characters */
			basic_string &assign(	const basic_string &s,
									size_type pos = 0,
									size_type n = npos)
			{
				if (s.mString == 0)return *this;
				return assign(&s.mString[pos], n);
			}
			/*! Assign n characters of a character sequence to this string
			 *\param[in] s the characters sequence
			 *\param[in] n the number of characters */
			basic_string &assign(const charT *s, size_type n = npos)
			{
				if (s == 0)return *this;
				size_t strlength = traits_type::length(s);
				if (n == npos)n = strlength;
				if (n > strlength)n = strlength;
				reserve(n+1, false);
				mLength = n;
				traits_type::copy(mString, s, n);
				mString[mLength] = '\0';
				return *this;
			}
			/*! Remove an element from the string
			 *\param[in] pos the position in the string */
			void erase(iterator pos)
			{
				unsigned int off = pos.getOffset();
				for (unsigned int i=(off + 1);i < (mLength + 1);i++)
				{
					mString[i-1] = mString[i];
				}
				--mLength;
			}
			/*! Remove the elements between first and last
			 *\param[in] first first element to remove
			 *\param[in] last last element to remove */
			void erase(iterator first, iterator last)
			{
				unsigned int off1 = first.getOffset();
				unsigned int off2 = last.getOffset();
				unsigned int move = mLength - off2 + 1;
				for (unsigned int i=0;i < move;i++)
				{
					mString[off1 + i] = mString[off2 + i];
				}
				mLength -= off2 - off1;
			}
			basic_string &trim()
			{
				if (mString == 0 || mLength == 0)return *this;
				size_t index = 0;
				while (	index < mLength &&
						isspace(mString[index]) == true)++index;
				size_t index2 = mLength - 1;
				while (	index2 > 0 &&
						isspace(mString[index2]) == true)--index2;
				size_t i = 0;
				for (;i < (index2 - index + 1);i++)
					mString[i] = mString[index + i];
				mLength = index2 - index + 1;
				mString[i] = '\0';
				return *this;
			}
			/*! Copy n characters from this string to s
			 *\param[in,out] s the copied character sequence
			 *\param[in] n the number of characters
			 *\param[in] pos the position to start copying
			 *\return the number of characters copied */
			size_type copy(charT *s, size_type n, size_type pos = 0) const
			{
				memset(s, 0, n+1);
				if (mString == 0)return 0;
				if (n > (mLength - pos))n = mLength;
				traits_type::copy(s, &mString[pos], n);
				return n;
			}
			/*! Swap two strings */
			void swap(basic_string<charT, traits_type, allocator_type> &s)
			{
				std::swap<char*>(mString, s.mString);
				std::swap<size_type>(mLength, s.mLength);
				std::swap<size_type>(mCapacity, s.mCapacity);
				std::swap<allocator_type>(mAllocator, s.mAllocator);
			}
			/*! Copy n characters starting at pos and return it as a string object */
			basic_string substr(size_type pos, size_type n = npos){return basic_string(*this, pos, n);}
			/*! Get an iterator for the string's beginning */
			inline iterator begin(){return iterator(*this, 0);}
			/*! Get a const iterator for the string's beginning */
			inline const_iterator begin() const{return const_iterator(*this, 0);}
			/*! Get an iterator for the string's beginning */
			inline reverse_iterator rbegin(){return reverse_iterator(iterator(*this, mLength - 1));}
			/*! Get a const iterator for the string's beginning */
			inline const_reverse_iterator rbegin() const{return const_reverse_iterator(iterator(*this, mLength - 1));}
			/*! Get an iterator for the string's end */
			inline iterator end(){return iterator(*this, mLength);}
			/*! Get a const iterator for the string's end */
			inline const_iterator end() const{return const_iterator(*this, mLength);}
			/*! Get an iterator for the string's reverse end */
			inline reverse_iterator rend(){return reverse_iterator(iterator(*this, -1));}
			/*! Get a const iterator for the string's reverse end */
			inline const_reverse_iterator rend() const{return const_reverse_iterator(iterator(*this, -1));}
			/*! Get the string length
			 *\return the string length */
			inline size_type length() const{return mLength;}
			/*! Get the string length
			 *\return the string length */
			inline size_type size() const{return mLength;}
			/*! Get the string capacity
			 *\return the string capacity */
			inline size_type capacity() const{return mCapacity;}
			/*! Is empty? */
			inline bool empty() const{return (mLength == 0);}
			/*! Get the c-style string */
			inline const charT *c_str() const{return mString;}
			/*! Get the c-style string */
			inline const charT *data() const{return mString;}
			/*! Get the allocator */
			inline allocator_type get_allocator() const{return allocator_type(mAllocator);}
			/*! The destructor */
			inline ~basic_string()
			{
				if (mString != 0)delete []mString;
			}
			
			/*! The [] operator */
			inline reference operator [] (size_type n)
			{
				return mString[n];
			}
			/*! The [] operator */
			inline const_reference operator [] (size_type n) const
			{
				return mString[n];
			}
			/*! The = operator */
			inline basic_string &operator = (const basic_string &str){assign(str);return *this;}
			/*! The = operator */
			inline basic_string &operator = (const charT *str){assign(str);return *this;}
			/*! The == operator */
			inline bool operator == (const basic_string &str) const
			{
				if (mLength != str.mLength)return false;
				return (traits_type::compare(mString, str.mString, mLength) == 0);
			}
			/*! The != operator */
			inline bool operator != (const basic_string &str) const{return !(*this == str);}
			/*! The == operator */
			inline bool operator == (const char *str) const
			{
				if (mLength != traits_type::length(str))return false;
				return (traits_type::compare(mString, str, mLength) == 0);
			}
			/*! The != operator */
			inline bool operator != (const char *str) const{return !(*this == str);}
		private:
			/*! Pointer to the character sequence */
			charT *mString;
			/*! The string length */
			size_type mLength;
			/*! The size of the allocation */
			size_type mCapacity;
			/*! The allocator */
			allocator_type mAllocator;
	};
	
	/*! Define the standard string class */
	typedef basic_string<char> string;
	
	/*! Define the standard wstring class */
	typedef basic_string<wchar_t> wstring;	
}

/*@}*/

#endif
