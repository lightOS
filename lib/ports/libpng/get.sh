#!/bin/bash

if [ ! -e libpng-$1-no-config.tar.bz2 ]
then
  wget http://prdownloads.sourceforge.net/libpng/libpng-$1-no-config.tar.bz2?download
fi

if [ ! -e libpng-$1/README ]
then
  tar -xf libpng-$1-no-config.tar.bz2 -C .
fi

if [ ! -d src ]
then
  ln -s libpng-$1 src
fi
