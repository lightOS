#!/bin/bash

if [ ! -e freetype-$1.tar.bz2 ]
then
  wget http://www.very-clever.com/download/nongnu/freetype/freetype-$1.tar.bz2
fi

if [ ! -e freetype-$1/README ]
then
  tar -xf freetype-$1.tar.bz2 -C .
fi

if [ ! -d src ]
then
  ln -s freetype-$1 src
fi
