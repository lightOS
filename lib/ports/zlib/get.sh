#!/bin/bash

if [ ! -e zlib-$1.tar.bz2 ]
then
  wget http://www.zlib.net/zlib-$1.tar.bz2
fi

if [ ! -e zlib-$1/README ]
then
  tar -xf zlib-$1.tar.bz2 -C .
fi

if [ ! -d src ]
then
  ln -s zlib-$1 src
  patch -p0 < zlib.patch
fi
