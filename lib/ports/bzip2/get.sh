#!/bin/bash

if [ ! -e bzip2-$1.tar.gz ]
then
  wget http://www.bzip.org/$1/bzip2-$1.tar.gz
fi

if [ ! -e bzip2-$1/README ]
then
  tar -xf bzip2-$1.tar.gz -C .
fi

if [ ! -d src ]
then
  ln -s bzip2-$1 src
fi
