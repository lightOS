/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdlib.h>
#include <string.h>
#include <libOS/LIBUNIX_glue.h>
#include <libOS/lightOS/internal/curses.h>
#include <libkernel/kernel.h>
#include <libkernel/message_noprefix.h>

bool _LIBOS_init_window(struct _LIBOS_WINDOW *osdep,
                        size_t width,
                        size_t height,
                        bool curscr)
{
  osdep->isshm = curscr;

  if (osdep->isshm)
  {
    osdep->shm = _LIBKERNEL_create_shared_memory(width * height * 2);
    if (osdep->shm.id == 0)
      return false;
    memset(osdep->shm.address, 0, width * height * 2);
  }
  else
  {
    osdep->data = calloc(width * height * 2, 1);
    if (osdep->data == NULL)
      return false;
  }
  return true;
}

char *_LIBOS_window_data(struct _LIBOS_WINDOW *osdep)
{
  return osdep->isshm ? osdep->shm.address : osdep->data;
}

// TODO
#include <libc/internal/stdio.h>

void _LIBOS_screen_update(struct _LIBOS_WINDOW *screen,
                          FILE *outfile)
{
  message_t request = _LIBKERNEL_create_message_shm(outfile->osdep.fsport,
                                                    MSG_FS_WRITE_FILE,
                                                    0,
                                                    &screen->shm,
                                                    SHM_READ_ONLY);
  _LIBKERNEL_send_message(outfile->osdep.port, &request);
  message_t reply = _LIBKERNEL_create_message(outfile->osdep.fsport,
                                              MSG_FS_WRITE_FILE,
                                              0,
                                              0,
                                              0);
  _LIBKERNEL_add_wait_message(outfile->osdep.port, &reply);
  _LIBKERNEL_wait_message(&reply);
}

void _LIBOS_deinit_window(struct _LIBOS_WINDOW *osdep)
{
  if (osdep->isshm)
  {
    // TODO
  }
  else
    free(osdep->data);
}
