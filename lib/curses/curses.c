/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>
#include <libOS/LIBUNIX_glue.h>
#include <libOS/LIBC_glue.h>

// TODO
#include <libserver/console.h>
#include <libOS/lightOS/internal/curses.h>

// TODO: curses mode can be paused through endwin() and started again through a refresh

#define MAKE_COLOR_PAIR(foreground, background)  (((background) << 4) | (foreground))

// Define the SCREEN modes
#define SCREEN_MODE_ECHO                         0x01
#define SCREEN_MODE_CBREAK                       0x02
#define SCREEN_MODE_KEYPAD                       0x04
#define SCREEN_MODE_NEWLINE_TRANSLATION          0x08

// Define the WINDOW modes
#define WINDOW_MODE_NO_DELAY                     0x01

struct _LIBUNIX_WINDOW
{
  size_t x;
  size_t y;
  size_t width;
  size_t height;
  size_t cursor;
  size_t color;
  size_t mode;
  _LIBOS_WINDOW osdep;
  struct _LIBUNIX_WINDOW *parent;
  struct _LIBUNIX_SCREEN *screen;
};

struct _LIBUNIX_SCREEN
{
  size_t width;
  size_t height;
  FILE *infile;
  FILE *outfile;
  WINDOW *curscr;
  WINDOW *stdscr;
  size_t mode;
};

// Global variables
int COLS = 0;
int LINES = 0;
WINDOW *curscr = NULL;
WINDOW *stdscr = NULL;

// File-local variables
static SCREEN *_LIBUNIX_cur_screen = NULL;


//
// File-local functions
//

static int _LIBUNIX_move_cursor(int x, int y)
{
  assert(_LIBUNIX_cur_screen != NULL);

  // Set the 'hardware' cursor
  _LIBSERVER_CONSOLE_SET_CURSOR(_LIBUNIX_cur_screen->outfile, x, y);
  return OK;
}


//
// Initialization functions
//

SCREEN *newterm(char *type,
                FILE *outfile,
                FILE *infile)
{
  // NOTE: type is ignored
  assert(outfile != NULL);
  assert(infile != NULL);

  // Allocate memory for the SCREEN
  SCREEN *Screen = malloc(sizeof(SCREEN));
  if (Screen == NULL)
    return NULL;

  // Initialize the SCREEN
  Screen->width = 80;
  Screen->height = 25;
  Screen->infile = infile;
  Screen->outfile = outfile;
  Screen->mode = SCREEN_MODE_ECHO | SCREEN_MODE_NEWLINE_TRANSLATION;

  // Allocate memory for stdscr
  Screen->stdscr = malloc(sizeof(WINDOW));
  if (Screen->stdscr == NULL)
    goto error_stdscr;
  if (_LIBOS_init_window(&Screen->stdscr->osdep,
                         Screen->width,
                         Screen->height,
                         FALSE) == FALSE)
    goto error_stdscr_init;

  // Initialize the stdscr
  Screen->stdscr->x = 0;
  Screen->stdscr->y = 0;
  Screen->stdscr->width = Screen->width;
  Screen->stdscr->height = Screen->height;
  Screen->stdscr->cursor = 0;
  Screen->stdscr->color = MAKE_COLOR_PAIR(COLOR_WHITE, COLOR_BLACK);
  Screen->stdscr->mode = 0;
  Screen->stdscr->parent = NULL;
  Screen->stdscr->screen = Screen;

  // Allocate memory for curscr
  Screen->curscr = malloc(sizeof(WINDOW));
  if (Screen->curscr == NULL)goto error_curscr;
  if (_LIBOS_init_window(&Screen->curscr->osdep,
                         Screen->width,
                         Screen->height,
                         TRUE) == FALSE)
    goto error_curscr_init;

  // Initialize the curscr
  Screen->curscr->x = 0;
  Screen->curscr->y = 0;
  Screen->curscr->width = Screen->width;
  Screen->curscr->height = Screen->height;
  Screen->curscr->cursor = 0;
  Screen->curscr->color = MAKE_COLOR_PAIR(COLOR_WHITE, COLOR_BLACK);
  Screen->curscr->mode = 0;
  Screen->curscr->parent = NULL;
  Screen->curscr->screen = Screen;

  // Disable 'hardware' echoing
  // TODO: libOS
  _LIBSERVER_CONSOLE_HW_ECHO(Screen->outfile, false);

  // Clear the screen
  // TODO: libOS
  _LIBSERVER_CONSOLE_CLEAR(Screen->outfile);

  // Set current SCREEN, stdscr & curscr
  if (_LIBUNIX_cur_screen == NULL)
    set_term(Screen);

  return Screen;

  error_curscr_init:
    free(Screen->curscr);
  error_curscr:
    _LIBOS_deinit_window(&Screen->stdscr->osdep);
  error_stdscr_init:
    free(Screen->stdscr);
  error_stdscr:
    free(Screen);
  return NULL;
}

SCREEN *set_term(SCREEN *newterm)
{
  curscr = newterm->curscr;
  stdscr = newterm->stdscr;
  COLS = newterm->width;
  LINES = newterm->height;

  SCREEN *tmp = _LIBUNIX_cur_screen;
  _LIBUNIX_cur_screen = newterm;
  return tmp;
}

WINDOW *initscr()
{
  if (newterm(NULL, stdout, stdin) == NULL)
  {
    // TODO: What should we do here?
    _LIBOS_WARNING("libunix", "failed");
    abort();
  }
  return stdscr;
}

void _LIBUNIX_getyx(const WINDOW *win, int *y, int *x)
{
  *y = (win->cursor / 2) / win->width;
  *x = (win->cursor / 2) % win->width;
}

void _LIBUNIX_getbegyx(const WINDOW *win, int *y, int *x)
{
  *y = win->y;
  *x = win->x;
}

void _LIBUNIX_getmaxyx(const WINDOW *win, int *y, int *x)
{
  *y = win->height;
  *x = win->width;
}


//
// Control the modes
//

// TODO: libOS?
int echo()
{
  assert(_LIBUNIX_cur_screen != NULL);
  _LIBUNIX_cur_screen->mode |= SCREEN_MODE_ECHO;
  return OK;
}

int noecho()
{
  assert(_LIBUNIX_cur_screen != NULL);
  _LIBUNIX_cur_screen->mode &= ~SCREEN_MODE_ECHO;
  return OK;
}

int cbreak()
{
  assert(_LIBUNIX_cur_screen != NULL);
  _LIBUNIX_cur_screen->mode |= SCREEN_MODE_CBREAK;
  fflush(_LIBUNIX_cur_screen->outfile);
  fprintf(_LIBUNIX_cur_screen->outfile, "\x1B]B");
  fflush(_LIBUNIX_cur_screen->outfile);
  return OK;
}

int nocbreak()
{
  assert(_LIBUNIX_cur_screen != NULL);
  _LIBUNIX_cur_screen->mode &= ~SCREEN_MODE_CBREAK;
  fflush(_LIBUNIX_cur_screen->outfile);
  fprintf(_LIBUNIX_cur_screen->outfile, "\x1B[B");
  fflush(_LIBUNIX_cur_screen->outfile);
  return OK;
}

int raw()
{
  _LIBOS_WARNING("libunix", "not implemented");
  // TODO
}

int noraw()
{
  _LIBOS_WARNING("libunix", "not implemented");
  // TODO
}

int nl()
{
  assert(_LIBUNIX_cur_screen != NULL);
  _LIBUNIX_cur_screen->mode |= SCREEN_MODE_NEWLINE_TRANSLATION;
  return OK;
}

int nonl()
{
  assert(_LIBUNIX_cur_screen != NULL);
  _LIBUNIX_cur_screen->mode &= ~SCREEN_MODE_NEWLINE_TRANSLATION;
  return OK;
}

int keypad(WINDOW *win, bool bf)
{
  assert(win != NULL);
  if (bf == true)win->mode |= SCREEN_MODE_KEYPAD;
  else win->mode &= ~SCREEN_MODE_KEYPAD;
  return OK;
}

int nodelay(WINDOW *win, bool bf)
{
  assert(win != NULL);

  if (bf != FALSE)
    win->mode |= WINDOW_MODE_NO_DELAY;
  else
    win->mode &= ~WINDOW_MODE_NO_DELAY;

  return OK;
}


//
// Control the cursor modes
//

int curs_set(int visibility)
{
  assert(_LIBUNIX_cur_screen != NULL);

  fflush(_LIBUNIX_cur_screen->outfile);
  const char *string = NULL;
  if (visibility == 0)
    string = "\x1B[V0";
  else
    string = "\x1B[V1";
  fprintf(_LIBUNIX_cur_screen->outfile, string);
  fflush(_LIBUNIX_cur_screen->outfile);
  return OK;
}


//
// Window management
//

WINDOW *newwin(int nlines, int ncols, int begin_y, int begin_x)
{
  assert(_LIBUNIX_cur_screen != NULL);

  if (ncols == 0)
    ncols = COLS - begin_x;
  if (nlines == 0)
    nlines = LINES - begin_y;

  // Allocate memory for the window
  WINDOW *window = malloc(sizeof(WINDOW));
  if (window == NULL)
    return NULL;
  if (_LIBOS_init_window(&window->osdep, nlines, ncols, FALSE) == FALSE)
  {
    free(window);
    return NULL;
  }

  // Initialize the window
  window->x = begin_x;
  window->y = begin_y;
  window->width = ncols;
  window->height = nlines;
  window->cursor = 0;
  window->color = MAKE_COLOR_PAIR(COLOR_WHITE, COLOR_BLACK);
  window->mode = 0;
  window->parent = stdscr;
  window->screen = _LIBUNIX_cur_screen;

  return window;
}

int endwin()
{
  assert(_LIBUNIX_cur_screen != NULL);

  // Restore the cursor
  if (curs_set(1) == ERR)return ERR;

  // Enable 'hardware' echoing
  // TODO: libOS
  fflush(_LIBUNIX_cur_screen->outfile);
  fprintf(_LIBUNIX_cur_screen->outfile, "\x1B[E");
  fflush(_LIBUNIX_cur_screen->outfile);

  // Clear the screen
  // TODO: libOS
  fprintf(_LIBUNIX_cur_screen->outfile, "\x1B[C");
  fflush(_LIBUNIX_cur_screen->outfile);

  // Deallocate memory for the SCREEN, curscr & stdscr
  // TODO: delscreen()
  // free(_cur_screen->stdscr);
  // free(_cur_screen->curscr);
  // free(_cur_screen);

  // Reset current SCREEN, stdscr & curscr
  // _cur_screen = NULL;
  // stdscr = NULL;
  // curscr = NULL;
  // COLS = 0;
  // LINES = 0;

  return OK;
}

int wclear(WINDOW *win)
{
  assert(win != NULL);

  // Clear the screen
  win->cursor = 0;
  memset(_LIBOS_window_data(&win->osdep),
         0,
         win->width * win->height * 2);

  return OK;
}

int werase(WINDOW *win)
{
  return wclear(win);
}

int wmove(WINDOW *win, int y, int x)
{
  assert(win != NULL);
  assert(x < win->width);
  assert(y < win->height);

  win->cursor = (win->width * y + x) * 2;
  return OK;
}

int waddch(WINDOW *win, const chtype ch)
{
  assert(win != NULL);

  // Get a pointer to the memory
  char *Data = _LIBOS_window_data(&win->osdep);

  // Write the character to the memory
  if (ch == '\n')
  {
    // NOTE: to static terminal size
    win->cursor -= win->cursor % 160;
    win->cursor += 160;
  }
  else
  {
    Data[win->cursor] = (char)ch;
    Data[win->cursor + 1] = win->color;
  }

  // Move the cursor
  win->cursor += 2;

  // TODO: replace spaces with the background character (should be \0)
  // TODO: Attributes
  // TODO: Wrapping
  // TODO: Special character processing
  return OK;
}

int mvwaddch(WINDOW *win, int y, int x, const chtype ch)
{
  assert(win != NULL);

  // Move the cursor
  if (wmove(win, y, x) != OK)return ERR;

  // Add the character
  return waddch(win, ch);
}

int waddstr(WINDOW *win, const char *str)
{
  return waddnstr(win, str, -1);
}

int waddnstr(WINDOW *win, const char *str, int n)
{
  assert(win != NULL);

  if (n == -1)n = strlen(str);

  for (int i = 0;i < n;i++)
  {
    if (str[i] == '\0')break;
    waddch(win, str[i]);
  }

  return OK;
}

int mvwaddstr(WINDOW *win, int y, int x, const char *str)
{
  return mvwaddnstr(win, y, x, str, -1);
}

int mvwaddnstr(WINDOW *win, int y, int x, const char *str, int n)
{
  assert(win != NULL);

  // Move the cursor
  if (wmove(win, y, x) != OK)return ERR;

  // Add the string
  return waddnstr(win, str, n);
}

static int var_wprintw(WINDOW *win, const char *fmt, va_list args)
{
  va_list argcopy;
  va_copy(argcopy, args);
  int result = vsnprintf(0, 0, fmt, argcopy);
  va_end(argcopy);
  if (result < 0)return ERR;
  char *array = malloc(result + 1);
  result = vsnprintf(array, result + 1, fmt, args);
  if (result < 0)
  {
    free(array);
    return ERR;
  }
  waddstr(win, array);
  free(array);
  return result;
}

int printw(const char *fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  int result = var_wprintw(stdscr, fmt, args);
  va_end(args);
  return result;
}

int wprintw(WINDOW *win, const char *fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  int result = var_wprintw(win, fmt, args);
  va_end(args);
  return result;
}

static int var_mvwprintw(WINDOW *win, int y, int x, const char *fmt, va_list args)
{
  va_list argcopy;
  va_copy(argcopy, args);
  int result = vsnprintf(0, 0, fmt, argcopy);
  va_end(argcopy);
  if (result < 0)return ERR;
  char *array = malloc(result + 1);
  result = vsnprintf(array, result + 1, fmt, args);
  if (result < 0)
  {
    free(array);
    return ERR;
  }
  mvwaddstr(win, y, x, array);
  free(array);
  return result;
}

int mvprintw(int y, int x, const char *fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  int result = var_mvwprintw(stdscr, y, x, fmt, args);
  va_end(args);
  return result;
}

int mvwprintw(WINDOW *win, int y, int x, const char *fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  int result = var_mvwprintw(win, y, x, fmt, args);
  va_end(args);
  return result;
}





int wattroff(WINDOW *win, int attrs)
{
  assert(win != NULL);
  
  if ((attrs & A_REVERSE) != 0)
  {
    win->color = MAKE_COLOR_PAIR(COLOR_WHITE, COLOR_BLACK);
  }
  
  if ((attrs & (~(A_REVERSE))) != 0)
  {
    _LIBOS_WARNING("libunix", "unimplemented attribute");
    abort();
  }
  
  return OK;
}
int wattron(WINDOW *win, int attrs)
{
  assert(win != NULL);
  
  if ((attrs & A_REVERSE) != 0)
  {
    win->color = MAKE_COLOR_PAIR(COLOR_BLACK, COLOR_WHITE);
  }
  
  if ((attrs & (~(A_REVERSE))) != 0)
  {
    _LIBOS_WARNING("libunix", "unimplemented attribute");
    abort();
  }
  
  return OK;
}


int mvwgetch(WINDOW *win, int y, int x)
{
  assert(win != NULL);
  
  // Move the cursor
  if (wmove(win, y, x) != OK)return ERR;
  
  return wgetch(win);
}
int wgetch(WINDOW *win)
{
  assert(win != NULL);
  
  // TODO: Is window a Pad?
  wrefresh(win);

  // TODO FIXME HACK
  if ((win->mode & WINDOW_MODE_NO_DELAY) != 0)
    return ERR;

  _LIBSERVER_key_t Key;
  int result;
  char tmp[10];
  size_t length = 0;
  do
  {
    int character = fgetc(win->screen->infile);
    if (character == EOF)
      return ERR;

    tmp[length++] = (char)character;

    if ((win->mode & SCREEN_MODE_KEYPAD) == SCREEN_MODE_KEYPAD)
    {
      result = _LIBSERVER_convert_from_ecma48(tmp, length, &Key);
      assert(result != _LIBSERVER_CONVERT_FAILED);
    }
    else
    {
      Key.character = tmp[0];
      Key.code      = _LIBSERVER_KEYCODE_NONE;
      result        = _LIBSERVER_CONVERT_OK;
    }

    if (result == _LIBSERVER_CONVERT_FAILED)
      return ERR;
  } while (result == _LIBSERVER_CONVERT_NEED_INPUT);

  int retval = Key.character;
  if (Key.character == L'\0')
    retval = _LIBOS_KEYCODE_TO_LIBUNIX(Key.code);

  // Newline translation disabled?
  if ((win->mode & SCREEN_MODE_NEWLINE_TRANSLATION) == 0 && retval == '\n')
    retval = KEY_ENTER;

  // Print the character
  if ((_LIBUNIX_cur_screen->mode & SCREEN_MODE_ECHO) != 0)
    if (retval < 256)
      if (waddch(win, (chtype)retval) == ERR)
        return ERR;

  return retval;
}


int wrefresh(WINDOW *win)
{
  assert(win != NULL);

  if (wnoutrefresh(win) == ERR)return ERR;
  return doupdate();
}

int wnoutrefresh(WINDOW *win)
{
  assert(win != NULL);

  // Copy window data to curscr
  size_t offsrc = 0;
  size_t offdest = (win->y * curscr->width + win->x) * 2;
  size_t cpywidth = win->width;
  if ((win->width + win->x) > curscr->width)
    cpywidth = curscr->width - win->x;
  size_t cpyheight = win->height;
  if ((win->height + win->y) > curscr->height)
    cpyheight = curscr->height - win->y;
  for (size_t i = 0;i < cpyheight;i++)
  {
    memcpy(_LIBOS_window_data(&curscr->osdep) + offdest,
           _LIBOS_window_data(&win->osdep) + offsrc,
           cpywidth * 2);
    offdest += curscr->width * 2;
    offsrc += win->width * 2;
  }

  size_t curx = ((win->cursor / 2) % win->width) + win->x;
  size_t cury = ((win->cursor / 2) / win->width) + win->y;
  curscr->cursor = ((cury * curscr->width) + curx) * 2;

  return OK;
}

int doupdate()
{
  _LIBOS_screen_update(&curscr->osdep, _LIBUNIX_cur_screen->outfile);

  // Set the cursor
  _LIBUNIX_move_cursor((curscr->cursor / 2) % curscr->width,
                       (curscr->cursor / 2) / curscr->width);
  return OK;
}

int beep()
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
  return 0;
}
int delwin(WINDOW *win)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
  return 0;
}
int scrollok(WINDOW *win, bool bf)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
  return 0;
}
int wscrl(WINDOW *win, int n)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
  return 0;
}
bool isendwin(void)
{
  _LIBOS_WARNING("libunix", "not implemented");
  //abort();
  return FALSE;
}
