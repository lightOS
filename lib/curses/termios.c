/*
lightOS libunix
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <termios.h>
#include <libOS/LIBUNIX_glue.h>

// TODO
#include <libOS/LIBC_glue.h>
#include <stdlib.h>

int tcgetattr(int fildes, struct termios *termios_p)
{
  _LIBOS_WARNING("libunix", "not implemented");
	
	// Get the FILE structure
	FILE *File = _LIBUNIX_get_file(fildes);
	if (File == NULL)
	{
		// TODO: set errno
		return -1;
	}
	
	return 0;
}
int tcsetattr(int fildes, int optional_actions, const struct termios *termios_p)
{
  _LIBOS_WARNING("libunix", "not implemented");
	
	// Get the FILE structure
	FILE *File = _LIBUNIX_get_file(fildes);
	if (File == NULL)
	{
		// TODO: set errno
		return -1;
	}
	
	return 0;
}
