/*
lightOS libunix
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBOS_LIGHTOS_INTERNAL_CURSES_H
#define LIGHTOS_LIBOS_LIGHTOS_INTERNAL_CURSES_H


/*
 * Standard C includes
 */
#include <stdbool.h>

/*
 * Libkernel includes
 */
#include <libkernel/type.h>


/*! \addtogroup libunix libunix */
/*@{*/
/*! \addtogroup libunix_os libOS */
/*@{*/
/*! \addtogroup libunix_os_curses curses */
/*@{*/



/*
 * Types
 */

/** \TODO documentation */
typedef struct _LIBOS_WINDOW
{
  bool isshm;
  void *data;                      // if isshm = false
  _LIBKERNEL_shared_memory_t shm;  // if isshm = true
} _LIBOS_WINDOW;



/*@}*/
/*@}*/
/*@}*/

#endif
