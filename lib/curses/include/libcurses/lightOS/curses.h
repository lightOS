/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBOS_LIGHTOS_CURSES_H
#define LIGHTOS_LIBOS_LIGHTOS_CURSES_H


/*
 * Standard C includes
 */
#include <stdbool.h>

/*
 * Libarch includes
 */
#include <libarch/type.h>
#include <libarch/arch/console.h>

/*
 * Libserver includes
 */
#include <libserver/keyboard.h>


/*! \addtogroup libunix libunix */
/*@{*/
/*! \addtogroup libunix_os libOS */
/*@{*/
/*! \addtogroup libunix_os_curses curses */
/*@{*/



/*
 * Macros
 */

/* Colors */
#define _LIBOS_COLOR_BLACK             _LIBARCH_CONSOLE_BLACK
#define _LIBOS_COLOR_BLUE              _LIBARCH_CONSOLE_BLUE
#define _LIBOS_COLOR_GREEN             _LIBARCH_CONSOLE_GREEN
#define _LIBOS_COLOR_CYAN              _LIBARCH_CONSOLE_CYAN
#define _LIBOS_COLOR_RED               _LIBARCH_CONSOLE_RED
#define _LIBOS_COLOR_MAGENTA           _LIBARCH_CONSOLE_MAGENTA
#define _LIBOS_COLOR_WHITE             _LIBARCH_CONSOLE_WHITE
#define _LIBOS_COLOR_YELLOW            _LIBARCH_CONSOLE_YELLOW

/* Keycode translation */
#define _LIBOS_KEYCODE_TO_LIBUNIX(code)     (code + 0x100)
#define _LIBOS_KEYCODE_TO_LIBSERVER(code)   (code - 0x100)

/* Keycodes
   _LIBSERVER_KEYCODE_USERDEF + 1        equals _LIBOS_KEY_ENTER */
// TODO _LIBOS_KEY_CODE_YES
// TODO _LIBOS_KEY_BREAK
#define _LIBOS_KEY_DOWN                _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_ARROW_DOWN)
#define _LIBOS_KEY_UP                  _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_ARROW_UP)
#define _LIBOS_KEY_LEFT                _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_ARROW_LEFT)
#define _LIBOS_KEY_RIGHT               _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_ARROW_RIGHT)
#define _LIBOS_KEY_HOME                _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_POS1)
#define _LIBOS_KEY_BACKSPACE           ('\r')
#define _LIBOS_KEY_F0                  _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_F0)
#define _LIBOS_KEY_F(n)                _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_F##n)
// TODO: _LIBOS_KEY_DL
// TODO: _LIBOS_KEY_IL
#define _LIBOS_KEY_DC                  _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_ERASE)
#define _LIBOS_KEY_IC                  _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_INSERT)
// TODO _LIBOS_KEY_EIC
// TODO _LIBOS_KEY_CLEAR
// TODO _LIBOS_KEY_EOS
// TODO _LIBOS_KEY_EOL
// TODO _LIBOS_KEY_SF
// TODO _LIBOS_KEY_SR
#define _LIBOS_KEY_NPAGE               _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_PAGE_DOWN)
#define _LIBOS_KEY_PPAGE               _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_PAGE_UP)
// TODO _LIBOS_KEY_STAB
// TODO _LIBOS_KEY_CTAB
// TODO _LIBOS_KEY_CATAB
#define _LIBOS_KEY_ENTER               _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_USERDEF + 1)
// TODO _LIBOS_KEY_SRESET
// TODO _LIBOS_KEY_RESET
// TODO _LIBOS_KEY_PRINT
// TODO _LIBOS_KEY_LL
#define _LIBOS_KEY_A1                  _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_NUMPAD_POS1)
#define _LIBOS_KEY_A3                  _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_NUMPAD_PAGE_UP)
#define _LIBOS_KEY_B2                  _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_NUMPAD_CENTER)
#define _LIBOS_KEY_C1                  _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_NUMPAD_END)
#define _LIBOS_KEY_C3                  _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_NUMPAD_PAGE_DOWN)
#define _LIBOS_KEY_END                 _LIBOS_KEYCODE_TO_LIBUNIX(_LIBSERVER_KEYCODE_END)



/*@}*/
/*@}*/
/*@}*/

#endif
