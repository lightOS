/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBUNIX_CURSES_H
#define LIGHTOS_LIBUNIX_CURSES_H


/*
 * Standard C includes
 */
#include <wchar.h>                     // WEOF, wint_t
#include <stdio.h>                     // EOF
#include <stdbool.h>                   // bool, true, false

/*
 * libOS includes
 */
#include <libOS/this/curses.h>


/*! \addtogroup libunix libunix */
/*@{*/
/*! \addtogroup libunix_curses curses */
/*@{*/


/*
 * Specification compliance
 * X/Open Curses, Issue 4, Version 2, July 1996
 * http://opengroup.org/onlinepubs/007908775/xcurses/curses.h.html
 */
#define _XOPEN_CURSES
#define _XOPEN_SOURCE_EXTENDED 1

/*
 * Macros
 */

/** Boolean false value */
#define FALSE                          false
/** Boolean true value */
#define TRUE                           true
/** Function return value for success */
#define OK                             0
/** Function return value for failure */
#define ERR                            -2


/* The following symbolic constants are used to manipulate objects of type attr_t */

/** Alternate character set */
#define WA_ALTCHARSET                  0x0001
/** Blinking */
#define WA_BLINK                       0x0002
/** Extra bright or bold */
#define WA_BOLD                        0x0004
/** Half bright */
#define WA_DIM                         0x0008
/** Horizontal highlight */
#define WA_HORIZONTAL                  0x0010
/** Invisible */
#define WA_INVIS                       0x0020
/** Left highlight */
#define WA_LEFT                        0x0040
/** Low highlight */
#define WA_LOW                         0x0080
/** Protected */
#define WA_PROTECT                     0x0100
/** Reverse video */
#define WA_REVERSE                     0x0200
/** Right highlight */
#define WA_RIGHT                       0x0400
/** Best highlighting mode of the terminal */
#define WA_STANDOUT                    0x0800
/** Top highlight */
#define WA_TOP                         0x1000
/** Underlining */
#define WA_UNDERLINE                   0x2000
/** Vertical highlight */
#define WA_VERTICAL                    0x4000


/* The following symbolic constants are used to manipulate attribute bits in objects of type chtype */

/** Alternate character set */
#define A_ALTCHARSET                   0x00000100
/*! Blinking */
#define A_BLINK                        0x00000200
/*! Extra bright or bold */
#define A_BOLD                         0x00000400
/*! Half bright */
#define A_DIM                          0x00000800
/*! Invisible */
#define A_INVIS                        0x00001000
/*! Protected */
#define A_PROTECT                      0x00002000
/*! Reverse video */
#define A_REVERSE                      0x00004000
/*! Best highlighting mode of the terminal */
#define A_STANDOUT                     0x00008000
/*! Underlining */
#define A_UNDERLINE                    0x00010000


/** Bit-mask to extract attributes */
#define A_ATTRIBUTES                   (A_ALTCHARSET | A_BLINK | A_BOLD | A_DIM | A_INVIS | \
                                        A_PROTECT | A_REVERSE | A_STANDOUT | A_UNDERLINE)
/** Bit-mask to extract a character */
#define A_CHARTEXT                     0x000000FF
/** Bit-mask to extract colour-pair information */
#define A_COLOR                        0xFFF00000


/* Internal helper macros */
#define _LIBUNIX_TO_LIBOS_COLOR(x)     (x >> 20)
#define _LIBUNIX_FROM_LIBOS_COLOR(x)   (x << 20)


/* Colour-related Macros
 * NOTE: The libOS macros must be between 0x000 and 0xFFF */

/** Black */
#define COLOR_BLACK                    _LIBUNIX_FROM_LIBOS_COLOR(_LIBOS_COLOR_BLACK)
/** Blue */
#define COLOR_BLUE                     _LIBUNIX_FROM_LIBOS_COLOR(_LIBOS_COLOR_BLUE)
/** Green */
#define COLOR_GREEN                    _LIBUNIX_FROM_LIBOS_COLOR(_LIBOS_COLOR_GREEN)
/** Cyan */
#define COLOR_CYAN                     _LIBUNIX_FROM_LIBOS_COLOR(_LIBOS_COLOR_CYAN)
/** Red */
#define COLOR_RED                      _LIBUNIX_FROM_LIBOS_COLOR(_LIBOS_COLOR_RED)
/** Magenta */
#define COLOR_MAGENTA                  _LIBUNIX_FROM_LIBOS_COLOR(_LIBOS_COLOR_MAGENTA)
/** White */
#define COLOR_WHITE                    _LIBUNIX_FROM_LIBOS_COLOR(_LIBOS_COLOR_WHITE)
/** Yellow */
#define COLOR_YELLOW                   _LIBUNIX_FROM_LIBOS_COLOR(_LIBOS_COLOR_YELLOW)


// TODO: ACS_, WACS_ macros


/* The following symbolic constants representing function key values are defined */

#ifdef _LIBOS_KEY_YES
  /** Used to indicate that a wchar_t variable contains a key code */
  #define KEY_CODE_YES                 _LIBOS_KEY_CODE_YES
#endif
#ifdef _LIBOS_KEY_BREAK
  /** Break key */
  #define KEY_BREAK                    _LIBOS_KEY_BREAK
#endif
#ifdef _LIBOS_KEY_DOWN
  /** Down arrow key */
  #define KEY_DOWN                     _LIBOS_KEY_DOWN
#endif
#ifdef _LIBOS_KEY_UP
  /** Up arrow key */
  #define KEY_UP                       _LIBOS_KEY_UP
#endif
#ifdef _LIBOS_KEY_LEFT
  /** Left arrow key */
  #define KEY_LEFT                     _LIBOS_KEY_LEFT
#endif
#ifdef _LIBOS_KEY_RIGHT
  /** Right arrow key */
  #define KEY_RIGHT                    _LIBOS_KEY_RIGHT
#endif
#ifdef _LIBOS_KEY_HOME
  /** Pos1 key */
  #define KEY_HOME                     _LIBOS_KEY_HOME
#endif
#ifdef _LIBOS_KEY_BACKSPACE
  /** Backspace */
  #define KEY_BACKSPACE                _LIBOS_KEY_BACKSPACE
#endif
#ifdef _LIBOS_KEY_F0
  /** zeroth function key */
  #define KEY_F0                       _LIBOS_KEY_F0
#endif
#ifdef _LIBOS_KEY_F
  /** Function keys; space for 64 keys is reserved  */
  #define KEY_F(n)                     _LIBOS_KEY_F(n)
#endif
#ifdef _LIBOS_KEY_DL
  /** Delete line */
  #define KEY_DL                       _LIBOS_KEY_DL
#endif
#ifdef _LIBOS_KEY_IL
  /** Insert line */
  #define KEY_IL                       _LIBOS_KEY_IL
#endif
#ifdef _LIBOS_KEY_DC
  /*! Delete character */
  #define KEY_DC                       _LIBOS_KEY_DC
#endif
#ifdef _LIBOS_KEY_IC
  /*! Insert char or enter insert mode */
  #define KEY_IC                       _LIBOS_KEY_IC
#endif
#ifdef _LIBOS_KEY_EIC
  /** Exit insert char mode */
  #define KEY_EIC                      _LIBOS_KEY_EIC
#endif
#ifdef _LIBOS_KEY_CLEAR
  /** Clear screen */
  #define KEY_CLEAR                    _LIBOS_KEY_CLEAR
#endif
#ifdef _LIBOS_KEY_EOS
  /** Clear to end of screen */  
  #define KEY_EOS                      _LIBOS_KEY_EOS
#endif
#ifdef _LIBOS_KEY_EOL
  /** Clear to end of line */
  #define KEY_EOL                      _LIBOS_KEY_EOL
#endif
#ifdef _LIBOS_KEY_SF
  /** Scroll 1 line forward */ 
  #define KEY_SF                       _LIBOS_KEY_SF
#endif
#ifdef _LIBOS_KEY_SR
  /** Scroll 1 line backward (reverse) */
  #define KEY_SR                       _LIBOS_KEY_SR
#endif
#ifdef _LIBOS_KEY_NPAGE
  /*! Next page */
  #define KEY_NPAGE                    _LIBOS_KEY_NPAGE
#endif
#ifdef _LIBOS_KEY_PPAGE
  /*! Previous page */
  #define KEY_PPAGE                    _LIBOS_KEY_PPAGE
#endif
#ifdef _LIBOS_KEY_STAB
  /** Set tab  */
  #define KEY_STAB                     _LIBOS_KEY_STAB
#endif
#ifdef _LIBOS_KEY_CTAB
  /** Clear tab  */
  #define KEY_CTAB                     _LIBOS_KEY_CTAB
#endif
#ifdef _LIBOS_KEY_CATAB
  /** Clear all tabs  */
  #define KEY_CATAB                    _LIBOS_KEY_CATAB
#endif
#ifdef _LIBOS_KEY_ENTER
  /*! Enter or send */
  #define KEY_ENTER                    _LIBOS_KEY_ENTER
#endif
#ifdef _LIBOS_KEY_SRESET
  /** Soft (partial) reset */
  #define KEY_SRESET                   _LIBOS_KEY_SRESET
#endif
#ifdef _LIBOS_KEY_RESET
  /** Reset or hard reset */ 
  #define KEY_RESET                    _LIBOS_KEY_RESET
#endif
#ifdef _LIBOS_KEY_PRINT
  /** Print or copy */
  #define KEY_PRINT                    _LIBOS_KEY_PRINT
#endif
#ifdef _LIBOS_KEY_LL
  /** Home down or bottom */
  #define KEY_LL                       _LIBOS_KEY_LL
#endif
#ifdef _LIBOS_KEY_A1
  /*! Upper left of keypad */
  #define KEY_A1                       _LIBOS_KEY_A1
#endif
#ifdef _LIBOS_KEY_A3
  /*! Upper right of keypad */
  #define KEY_A3                       _LIBOS_KEY_A3
#endif
#ifdef _LIBOS_KEY_B2
  /*! Center of keypad */
  #define KEY_B2                       _LIBOS_KEY_B2
#endif
#ifdef _LIBOS_KEY_C1
  /*! Lower left of keypad */
  #define KEY_C1                       _LIBOS_KEY_C1
#endif
#ifdef _LIBOS_KEY_C3
  /*! Lower right of keypad */
  #define KEY_C3                       _LIBOS_KEY_C3
#endif
#ifdef _LIBOS_KEY_END
  /*! End Key */
  #define KEY_END                      _LIBOS_KEY_END
#endif


/* The following symbolic constants representing function key values are also defined */

// TODO: function keys



/*
 * Types
 */

/* Forward declaractions */
struct _LIBUNIX_SCREEN;
struct _LIBUNIX_WINDOW;

/** An OR-ed set of attributes 
 *
 *  An integral type that can contain at least an unsigned short. The type attr_t is used
 *  to hold an OR-ed set of attributes defined in <curses.h> that begin with the prefix WA_. */
typedef uint16_t attr_t;

/** A character, attributes  and a colour-pair
 *
 *  An integral type that can contain at least an unsigned char and attributes. Values of
 *  type chtype are formed by OR-ing together an unsigned char value and zero or more of the
 *  base attribute flags defined in that have the A_ prefix. The application can extract
 *  these components of a chtype value using the base masks defined in <curses.h> for this
 *  purpose. The chtype data type also contains a colour-pair. Values of type chtype are
 *  formed by OR-ing together an unsigned char value, a colour pair, and zero or more of
 *  the attributes defined in <curses.h> that begin with the prefix A_. The application
 *  can extract these components of a chtype value using the masks defined in <curses.h>
 *  for this purpose. */
typedef uint32_t chtype;

/** \TODO cchar_t References a string of wide characters
 *  A type that can reference a string of wide characters of up to an implementation-dependent
 *  length, a colour-pair, and zero or more attributes from the set of all attributes defined
 *  in this document. A null cchar_t object is an object that references a empty wide-character
 *  string. Arrays of cchar_t objects are terminated by a null cchar_t object. */

/** An opaque terminal representation */
typedef struct _LIBUNIX_SCREEN SCREEN;

/** An opaque window representation */
typedef struct _LIBUNIX_WINDOW WINDOW;


/*
 * global variables
 */

extern int COLS;
extern int LINES;
extern WINDOW *stdscr;
extern WINDOW *curscr;



/*
 * Function macros
 */

// TODO void   getparyx(WINDOW *win, int y, int x);
// TODO: _LIBOS_?
#define getyx(win, y, x)               _LIBUNIX_getyx(win, &(y), &(x))
#define getbegyx(win, y, x)            _LIBUNIX_getbegyx(win, &(y), &(x))
#define getmaxyx(win, y, x)            _LIBUNIX_getmaxyx(win, &(y), &(x))



/*
 * Functions
 */

#ifdef __cplusplus
  extern "C"
  {
#endif

  // Initialization functions
  SCREEN *newterm(char *type, FILE *outfile, FILE *infile);
  WINDOW *initscr();
  SCREEN *set_term(SCREEN *newterm);

  // Control the modes
  int echo(void);
  int noecho(void);
  int cbreak(void);
  int nocbreak(void);
  int raw(void);
  int noraw(void);
  int nl(void);
  int nonl(void);
  int keypad(WINDOW *win, bool bf);

  // Control the cursor modes
  int curs_set(int visibility);

  // TODO
  int beep(void);
  bool isendwin(void);

  // Window management
  WINDOW *newwin(int nlines, int ncols, int begin_y, int begin_x);
  int endwin();
  int wclear(WINDOW *win);
  int werase(WINDOW *win);
  void _LIBUNIX_getyx(const WINDOW *win, int *y, int *x);
  void _LIBUNIX_getbegyx(const WINDOW *win, int *y, int *x);
  void _LIBUNIX_getmaxyx(const WINDOW *win, int *y, int *x);

  int wattroff(WINDOW *win, int attrs);
  int wattron(WINDOW *win, int attrs);
  int scrollok(WINDOW *win, bool bf);
  int nodelay(WINDOW *win, bool bf);

  int wscrl(WINDOW *win, int n);

  int wmove(WINDOW *win, int y, int x);
  int waddch(WINDOW *win, const chtype ch);
  int mvwaddch(WINDOW *win, int y, int x, const chtype ch);
  int waddstr(WINDOW *win, const char *str);
  int waddnstr(WINDOW *win, const char *str, int n);
  int mvwaddstr(WINDOW *win, int y, int x, const char *str);
  int mvwaddnstr(WINDOW *win, int y, int x, const char *str, int n);

  int mvprintw(int y, int x, const char *fmt, ...);
  int mvwprintw(WINDOW *win, int y, int x, const char *fmt, ...);
  int printw(const char *fmt, ...);
  int wprintw(WINDOW *win, const char *fmt, ...);

  int mvwgetch(WINDOW *win, int y, int x);
  int wgetch(WINDOW *win);

  int wnoutrefresh(WINDOW *win);
  int wrefresh(WINDOW *win);
  int doupdate();

  int delwin(WINDOW *);

  #define addch(c)                        waddch(stdscr, c)
  #define addnstr(s, n)                     waddnstr(stdscr, s, n)
  #define addstr(s)                       waddstr(stdscr, s)
  #define attroff(a)                        wattroff(stdscr, a)
  #define attron(a)                       wattron(stdscr, a)
  #define clear()                         wclear(stdscr);
  #define getch()                         wgetch(stdscr)
  #define erase()                         werase(stdscr)
  #define move(y, x)                        wmove(stdscr, y, x)
  #define mvaddch(y, x, ch)                   mvwaddch(stdscr, y, x, ch)
  #define mvaddnstr(y, x, str, n)                 mvwaddnstr(stdscr, y, x, str, n)
  #define mvaddstr(y, x, str)                   mvwaddstr(stdscr, y, x, str)
  #define mvgetch(win, y, x)                    mvwgetch(stdscr, y, x)
  #define refresh()                       wrefresh(stdscr)

#ifdef __cplusplus
  }
#endif



/*@}*/
/*@}*/

#endif
