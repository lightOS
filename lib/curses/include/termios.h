/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBUNIX_TERMIOS_H
#define LIGHTOS_LIBUNIX_TERMIOS_H

/*! \addtogroup libunix libunix */
/*@{*/

/*! Change attributes immediately */
#define TCSANOW                         0
/*! Change attributes when output has drained */
#define TCSADRAIN                       1
/*! Change attributes when output has drained; also flush pending input */
#define TCSAFLUSH                       2

#define IEXTEN                          0
#define ISIG                          1
#define IXON                          2

#define OPOST                         0

/*! Used for terminal special characters */
typedef unsigned char cc_t;
/*! Used for terminal baud rates */
typedef unsigned int speed_t;
/*! Used for terminal modes */
typedef unsigned int tcflag_t;

struct termios
{
  /*! input modes */
  tcflag_t c_iflag;
  /*! output modes */
  tcflag_t c_oflag;
  /*! control modes */
  // TODO tcflag_t c_cflag;
  /*! local modes */
  tcflag_t c_lflag;
  /*! control chars */
  // TODO cc_t c_cc[NCCS];
};

#ifdef __cplusplus
  extern "C"
  {
#endif

  int tcgetattr(int, struct termios *);
  int tcsetattr(int, int, const struct termios *);

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
