/*
lightOS libunix
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <libOS/LIBUNIX_glue.h>

// TODO
#include <libOS/LIBC_glue.h>
#include <stdlib.h>

//
// Current working directory
//
char *getcwd(char *buf, size_t size)
{
	size_t result = _LIBOS_get_work_directory(buf, size);
	if (result == 0)
	{
		// TODO: Set errno to failed
    _LIBOS_WARNING("libc", "failed");
	}
	else if (result > size)
	{
		// TODO Set errno to ERANGE
    _LIBOS_WARNING("libc", "ERANGE");
	}
	else
    return buf;
	return NULL;
}
int chdir(const char *path)
{
	size_t result = _LIBOS_set_work_directory(path);
	if (result == 0)
    return -1;
	return 0;
}
int fchdir(int fd)
{
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
  return 0;
}

//
// Process management
//
pid_t getpid()
{
	return _LIBUNIX_getpid();
}

//
// TTY
//
int isatty(int fd)
{
	FILE *file = _LIBUNIX_get_file(fd);
	return _LIBUNIX_isatty(file);
}

//
// Temporary file
//
static char *mktemps(char *templ, int suffixlen)
{
	size_t numX = 0;
	char *cur = NULL;
	char *end = templ + strlen(templ) - suffixlen - 2;
	while (*end == 'X')
	{
		++numX;
		cur = end;
		--end;
	}
	
	if (cur == 0)return NULL;
	_LIBOS_get_unique_filename(cur, numX);
	if (suffixlen != 0)
		*(templ + strlen(templ)) = 'X';
	return templ;
}
char *mktemp(char *templ)
{
	return mktemps(templ, 0);
}
int mkstemps(char *templ, int suffixlen)
{
	templ = mktemps(templ, suffixlen);
	if (templ == NULL)return -1;
	
	// FIXME should use the UNIX open call
	FILE *File = fopen(templ, "wb+");
	if (File == NULL)return -1;
	return _LIBUNIX_get_fd(File);
}
int mkstemp(char *templ)
{
	return mkstemps(templ, 0);
}


uid_t geteuid()
{
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
  return 0;
}

int access(const char *path, int amode)
{
  _LIBOS_WARNING("libunix", path);
	abort();
	return 0;
}

int chown(const char *path, uid_t owner, gid_t group)
{
	// TODO
  _LIBOS_WARNING("libunix", path);
	abort();
	return 0;
}

int close(int fildes)
{
  _LIBOS_WARNING("libunix", "not implemented");
	
	// TODO
	FILE *File = _LIBUNIX_get_file(fildes);
	return fclose(File);
}

int link(const char *path1, const char *path2)
{
	// TODO
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
	return 0;
}

ssize_t read(int fildes, void *buf, size_t nbyte)
{
  _LIBOS_WARNING("libunix", "not implemented");
	
	// TODO
	FILE *File = _LIBUNIX_get_file(fildes);
	return fread(buf, 1, nbyte, File);
}

int rmdir(const char *path)
{
	//TODO
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
	return 0;
}

int unlink(const char *path)
{
	// TODO
  _LIBOS_WARNING("libunix", "not implemented");
	//abort();
	return 0;
}

ssize_t write(int fildes, const void *buf, size_t nbyte)
{
  _LIBOS_WARNING("libunix", "not implemented");
	
	//TODO
	FILE *File = _LIBUNIX_get_file(fildes);
	return fwrite(buf, 1, nbyte, File);
}

void vfork()
{
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
}
void execv()
{
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
}
void execvp()
{
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
}
void dup2()
{
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
}
void getloadavg()
{
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
}
void putenv()
{
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
}
void getpwnam()
{
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
}
void getlogin()
{
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
}
long int pathconf(const char *path, int name)
{
  _LIBOS_WARNING("libunix", path);
  if (name == _PC_PATH_MAX)
    return 1024;
  abort();
}
unsigned alarm(unsigned seconds)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
int execve(const char *path, char *const argv[], char *const envp[])
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
int pipe(int fildes[2])
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
char *ttyname(int fildes)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
off_t lseek(int fildes, off_t offset, int whence)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
pid_t fork()
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
unsigned sleep(unsigned seconds)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
int dup(int fildes)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
uid_t getuid(void)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
gid_t getgid(void)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
gid_t getegid(void)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
int setuid(uid_t uid)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
int setgid(gid_t gid)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
}
