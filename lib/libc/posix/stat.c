/*
lightOS libunix
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <libOS/LIBC_glue.h>
#include <libOS/LIBUNIX_glue.h>

int stat(const char *path,
         struct stat *buffer)
{
  return _LIBOS_stat(path, buffer);
}

int fstat(int fildes,
          struct stat *buffer)
{
  return _LIBOS_fstat(fildes, buffer);
}

// TODO
#include <stdio.h>
#include <libOS/LIBC_glue.h>

int chmod(const char *path, mode_t mode)
{
  _LIBOS_WARNING("libunix", path);
  return 0;
}

int lstat(  const char *path,
      struct stat *buf)
{
  _LIBOS_WARNING("libunix", path);
  // FIXME: If we support symbolic links lstat shows the stats of the link not of the linked file
  return stat(path, buf);
}

int mkdir(const char *path, mode_t mode)
{
  _LIBOS_WARNING("libunix", path);
  abort();
  return 0;
}

mode_t umask(mode_t cmask)
{
  _LIBOS_WARNING("libunix", "not implemented");
  return 0777;
}
