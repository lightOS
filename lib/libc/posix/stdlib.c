/*
lightOS libunix
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdlib.h>
#include <libc/LIBUNIX_glue.h>

int setenv(const char *envname,
           const char *envval,
           int overwrite)
{
  return _LIBC_setenv(envname, envval, overwrite);
}

int unsetenv(const char *name)
{
  return _LIBC_unsetenv(name);
}

void _exit(int status)
{
  _Exit(status);
}
