/*
lightOS libunix
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <libOS/LIBUNIX_glue.h>

#include <libOS/LIBC_glue.h>

typedef void (*signal_handler)(int);

signal_handler signal_handler_array[SIGMAX + 1] =
{
	SIG_IGN,
	SIG_DFL,										// 1
	SIG_DFL,										// 2
	SIG_DFL,										// 3
	SIG_DFL,										// 4
	SIG_DFL,										// 5
	SIG_DFL,										// 6
	SIG_DFL,										// 7
	SIG_DFL,										// 8
	SIG_DFL,										// 9
	SIG_DFL,										// 10
	SIG_DFL,										// 11
	SIG_DFL,										// 12
};

void _signal_handler(int sig)
{
	// Unknown signal?
	if (sig > SIGMAX)
	{
		fprintf(stderr, "signal: unknown signal raised %i\n", sig);
		_exit(EXIT_FAILURE);
	}
	
	// Should we ignore the signal?
	if (signal_handler_array[sig] != SIG_IGN)
	{
		// Call the signal handler
		signal_handler_array[sig](sig);
	}
	
	// Return from the signal
	_LIBUNIX_signal_handler_end();
}

void _signal_default_handler(int sig)
{
	printf("signal: default handler for %i\n", sig);
	_exit(EXIT_FAILURE);
}

void _signal_init()
{
	_LIBUNIX_signal_handler(_signal_handler);
}

void (*signal(int sig, void (*func)(int)))(int)
{
	// Unknown signal?
	if (sig > SIGMAX)
	{
		fprintf(stderr, "signal: unkown signal %i\n", sig);
		return SIG_ERR;
	}
	
	// Return the old handler, save the new one
	signal_handler tmp = signal_handler_array[sig];
	signal_handler_array[sig] = func;
	return tmp;
}

int kill(pid_t pid, int sig)
{
	return _LIBUNIX_signal(pid, sig);
}

int raise(int sig)
{
	return _LIBUNIX_signal(getpid(), sig);
}


//!!!!!!!!!!!!!!!!!!!! HACK
#include <stdlib.h>
#include <stdio.h>
int sigaction(int sig,
              const struct sigaction *restrict act,
              struct sigaction *restrict oact)
{
  _LIBOS_WARNING("libunix", "not implemented");
	// HACK abort();
	return 0;
}

int sigaddset(sigset_t *set, int blub)
{
  _LIBOS_WARNING("libunix", "not implemented");
  // HACK abort();
  return 0;
}

int sigemptyset(sigset_t *set)
{
  _LIBOS_WARNING("libunix", "not implemented");
  // HACK abort();
  return 0;
}

int sigfillset(sigset_t *set)
{
  _LIBOS_WARNING("libunix", "not implemented");
	abort();
	return 0;
}

int sigprocmask(int blub, const sigset_t *set1, sigset_t *set2)
{
  _LIBOS_WARNING("libunix", "not implemented");
  abort();
  return 0;
}
