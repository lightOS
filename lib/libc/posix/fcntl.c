/*
lightOS libunix
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <fcntl.h>
// TODO #include <termios.h>
#include <stdio.h>
#include <libOS/LIBUNIX_glue.h>

#include <libOS/LIBC_glue.h>

int fcntl(int fildes, int cmd, ...)
{
	va_list ap;
	va_start(ap, cmd);
	
	switch (cmd)
	{
/* TODO: libcurses
		case TCGETS:
		{
			struct termios *Term = va_arg(ap, struct termios*);
			return tcgetattr(fildes, Term);
		}break;
		case TCSETS:
		{
			const struct termios *Term = va_arg(ap, const struct termios*);
			return tcsetattr(fildes, TCSANOW, Term);
		}break;
		case TCSETSF:
		{
			const struct termios *Term = va_arg(ap, const struct termios*);
			return tcsetattr(fildes, TCSAFLUSH, Term);
		}break;
		case TCSETSW:
		{
			const struct termios *Term = va_arg(ap, const struct termios*);
			return tcsetattr(fildes, TCSADRAIN, Term);
		}break;
*/
    default:
    {
      _LIBOS_WARNING("libunix", "unknown command");
    }
	}
	
	va_end(ap);
	return -1;
}
int open(const char *path, int oflags, ...)
{
	// TODO
  _LIBOS_WARNING("libunix", "not fully implemented");
	
	FILE *File = fopen(path, "w+");
	if (File == NULL)return -1;
	return _LIBUNIX_get_fd(File);
}
