/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdlib.h>
#include <dirent.h>
#include <libOS/LIBUNIX_glue.h>

// TODO: Close all dirs on exit

/* TODO: More flexible for easier porting */
struct _LIBUNIX_DIR
{
  fpos_t            fpos;
  struct dirent     dir_ent;
  struct _LIBOS_FILE osdep;
};

int closedir(DIR *dir)
{
  // Close the file
  int result = _LIBUNIX_closedir(&dir->osdep);

  // Deallocate the DIR structure
  free(dir);

  return (result < 0) ? EOF : 0;
}

DIR *opendir(const char *name)
{
  // Allocate the DIR structure
  DIR *File = malloc(sizeof(DIR));
  if (File == NULL)return NULL;

  // Initialize the FILE structure
  File->fpos = 0;

  // Initialize the _LIBC_FILE structure
  if (_LIBOS_init_file(&File->osdep) != 0)
  {
    // Failed, free FILE structure
    free(File);
    return NULL;
  }

  // Open the directory
  if (_LIBUNIX_opendir(&File->osdep, name) != 0)
  {
    // Failed, uninitialize the _LIBC_FILE structure & free FILE structure
    _LIBOS_uninit_file(&File->osdep);
    free(File);
    return NULL;
  }
  return File;
}

struct dirent *readdir(DIR *dir)
{
  size_t result = _LIBUNIX_readdir(&dir->osdep,
                                   &dir->fpos,
                                   &dir->dir_ent);
  if (result == 0)
    return NULL;
  return &dir->dir_ent;
}

void rewinddir(DIR *dir)
{
  dir->fpos = 0;
}

void seekdir(DIR *dir, long off)
{
  dir->fpos = off;
}

long telldir(DIR *dir)
{
  return dir->fpos;
}
