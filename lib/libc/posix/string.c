/*
lightOS libunix
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <errno.h>
#include <stdlib.h>
#include <string.h>

void *memccpy(void * restrict s1,
              const void * restrict s2,
              int c,
              size_t n)
{
  unsigned char * restrict s = s1;
  const unsigned char * restrict d = s2;

  while (n != 0)
  {
    *s++ = *d;
    if (*d++ == (unsigned char)c)
      return s;
    n--;
  }
  return NULL;
}

char *strdup(const char *s1)
{
  char *s = malloc(strlen(s1));
  if (s != NULL)
    strcpy(s, s1);
  else
    errno = ENOMEM;
  return s;
}
