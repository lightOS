/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <assert.h>
#include <inttypes.h>

intmax_t imaxabs(intmax_t j)
{
  return (j < 0) ? -j : j;
}

imaxdiv_t imaxdiv(intmax_t numer, intmax_t denom)
{
  assert(denom != 0);

  imaxdiv_t result;
  result.quot = numer / denom;
  result.rem = numer % denom;
  return result;
}
