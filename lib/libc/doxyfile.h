
/** \defgroup libc lightlibc */

/** \defgroup libc_libOS Interface to the libOS
 *  \addtogroup libc */
/** \defgroup libc_internal Internal
 *  \addtogroup libc */

/** \defgroup libc_assert ISO/IEC 9899:1999 - 7.2 - assert.h
 *  \addtogroup libc */
/** \defgroup libc_ctype  ISO/IEC 9899:1999 - 7.4 - ctype.h
 *  \addtogroup libc */
/** \defgroup libc_errno ISO/IEC 9899:1999 - 7.5 - errno.h
 *  \addtogroup libc */
/** \defgroup libc_inttypes ISO/IEC 9899:1999 - 7.8 - inttypes.h
 *  \addtogroup libc */
/** \defgroup libc_locale ISO/IEC 9899:1999 - 7.11 - locale.h
 *  \addtogroup libc */
/** \defgroup libc_math ISO/IEC 9899:1999 - 7.12 - math.h
 *  \addtogroup libc */
/** \defgroup libc_setjmp ISO/IEC 9899:1999 - 7.13 - setjmp.h
 *  \addtogroup libc */
/** \defgroup libc_stdint ISO/IEC 9899:1999 - 7.18 - stdint.h
 *  \addtogroup libc */
/** \defgroup libc_stdio ISO/IEC 9899:1999 - 7.19 - stdio.h
 *  \addtogroup libc */
/** \defgroup libc_stdlib ISO/IEC 9899:1999 - 7.20 - stdlib.h
 *  \addtogroup libc */
/** \defgroup libc_string ISO/IEC 9899:1999 - 7.21 - string.h
 *  ISO/IEC 9899:1999 7.21 String handling
 *  \addtogroup libc */
/** \defgroup libc_time   ISO/IEC 9899:1999 - 7.23 - time.h
 *  ISO/IEC 9899:1999 7.23 Date and time
 *  \addtogroup libc */
/** \defgroup libc_wchar ISO/IEC 9899:1999 - 7.24 - wchar.h
 *  ISO/IEC 9899:1999 7.24 Extended multibyte and wide character utilities
 *  \addtogroup libc */

/** \defgroup libc_posix_ctype IEEE Std 1003.1 - ctype.h
 *  \addtogroup libc */
/** \defgroup libc_posix_dirent IEEE Std 1003.1 - dirent.h
 *  \addtogroup libc */
/** \defgroup libc_posix_getopt IEEE Std 1003.1 - getopt.h
 *  \addtogroup libc */
/** \defgroup libc_posix_stdio IEEE Std 1003.1 - stdio.h
 *  \addtogroup libc */
/** \defgroup libc_posix_string IEEE Std 1003.1 - string.h
 *  http://www.opengroup.org/onlinepubs/000095399/basedefs/string.h.html */
/** \defgroup libunix_time IEEE Std 1003.1 - time.h
 *  http://www.opengroup.org/onlinepubs/000095399/basedefs/time.h.html
 *  \addtogroup libc */
