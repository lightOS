/*
lightOS libc
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <string.h>
#include <errno.h>

// TODO: This is locale dependant

char *_LIBC_error[] =
{
  "No error",

  /*
   * Error definitions from the ISO/IEC 9899:1999 standard
   * ISO/IEC 9899:1999 - 7.5
   */

  "Mathematics argument out of domain of function",        //  1 EDOM
  "Result too large",                                      //  2 ERANGE
  "Illegal byte sequence",                                 //  3 EILSEQ

  /*
   * Error definitions from POSIX
   */

  "Argument list too long",                                //  4 E2BIG
  "Permission denied",                                     //  5 EACCES
  "Address in use",                                        //  6 EADDRINUSE
  "Address not available",                                 //  7 EADDRNOTAVAIL
  "Address family not supported",                          //  8 EAFNOSUPPORT
  "Resource unavailable",                                  //  9 EAGAIN
  "Connection already in progress",                        // 10 EALREADY
  "Bad file descriptor",                                   // 11 EBADF
  "Bad message",                                           // 12 EBADMSG
  "Device or resource busy",                               // 13 EBUSY
  "Operation canceled",                                    // 14 ECANCELED
  "No child processes",                                    // 15 ECHILD
  "Connection aborted",                                    // 16 ECONNABORTED
  "Connection refused",                                    // 17 ECONNREFUSED
  "Connection reset",                                      // 18 ECONNRESET
  "Resource deadlock would occur",                         // 19 EDEADLK
  "Destination address required",                          // 20 EDESTADDRREQ
  "Reserved",                                              // 21 EDQUOT
  "File exists",                                           // 22 EEXIST
  "Bad address",                                           // 23 EFAULT
  "File too large",                                        // 24 EFBIG
  "Host is unreachable",                                   // 25 EHOSTUNREACH
  "Identifier removed",                                    // 26 EIDRM
  "Operation in progress",                                 // 27 EINPROGRESS
  "Interrupted function",                                  // 28 EINTR
  "Invalid argument",                                      // 29 EINVAL
  "I/O error",                                             // 30 EIO
  "Socket is connected",                                   // 31 EISCONN
  "Is a directory",                                        // 32 EISDIR
  "Too many levels of symbolic links",                     // 33 ELOOP
  "Too many open files",                                   // 34 EMFILE
  "Too many links",                                        // 35 EMLINK
  "Message too large",                                     // 36 EMSGSIZE
  "Reserved",                                              // 37 EMULTIHOP
  "Filename too long",                                     // 38 ENAMETOOLONG
  "Network is down",                                       // 39 ENETDOWN
  "Connection aborted by network",                         // 40 ENETRESET
  "Network unreachable",                                   // 41 ENETUNREACH
  "Too many files open in system",                         // 42 ENFILE
  "No buffer space available",                             // 43 ENOBUFS
  "No message is available on the STREAM head read queue", // 44 ENODATA
  "No such device",                                        // 45 ENODEV
  "No such file or directory",                             // 46 ENOENT
  "Executable file format error",                          // 47 ENOEXEC
  "No locks available",                                    // 48 ENOLCK
  "Reserved",                                              // 49 ENOLINK
  "Not enough space",                                      // 50 ENOMEM
  "No message of the desired type",                        // 51 ENOMSG
  "Protocol not available",                                // 52 ENOPROTOOPT
  "No space left on device",                               // 53 ENOSPC
  "No STREAM resources",                                   // 54 ENOSR
  "Not a STREAM",                                          // 55 ENOSTR
  "Function not supported",                                // 56 ENOSYS
  "The socket is not connected",                           // 57 ENOTCONN
  "Not a directory",                                       // 58 ENOTDIR
  "Directory not empty",                                   // 59 ENOTEMPTY
  "Not a socket",                                          // 60 ENOTSOCK
  "Not supported",                                         // 61 ENOTSUP
  "Inappropriate I/O control operation",                   // 62 ENOTTY
  "No such device or address",                             // 63 ENXIO
  "Operation not supported on socket",                     // 64 EOPNOTSUPP
  "Value too large to be stored in data type",             // 65 EOVERFLOW
  "Operation not permitted",                               // 66 EPERM
  "Broken pipe",                                           // 67 EPIPE
  "Protocol error",                                        // 68 EPROTO
  "Protocol not supported",                                // 69 EPROTONOSUPPORT
  "Protocol wrong type for socket",                        // 70 EPROTOTYPE
  "Read-only file system",                                 // 71 EROFS
  "Invalid seek",                                          // 72 ESPIPE
  "No such process",                                       // 73 ESRCH
  "Reserved",                                              // 74 ESTALE
  "Stream ioctl() timeout",                                // 75 ETIME
  "Connection timed out",                                  // 76 ETIMEDOUT
  "Text file busy",                                        // 77 ETXTBSY
  "Operation would block",                                 // 78 EWOULDBLOCK
  "Cross-device link"                                      // 79 EXDEV
};

char *strerror(int errnum)
{
  if (errnum < 0 || errnum > _LIBC_ERROR_MAX)
  {
    errno = EINVAL;
    return "unknown error";
  }

  return _LIBC_error[errnum];
}
