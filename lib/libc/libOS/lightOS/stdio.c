/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <libOS/LIBC_glue.h>
#include <libkernel/kernel.h>
#include <libkernel/type_noprefix.h>
#include <libkernel/message_noprefix.h>
#include <libserver/fs_noprefix.h>

// TODO
#include <libc/internal/stdio.h>

static void init_std_port(FILE *File,
                          port_id_t stdPort)
{
  port_id_t Port, fsPort;
  _LIBKERNEL_get_standard_port(0, stdPort, &Port, &fsPort);
  if (Port == stdPort)
    File->osdep.port = _LIBKERNEL_create_port();
  else
    File->osdep.port = Port;
  File->osdep.fsport = fsPort;
}

void _LIBC_init_std_port()
{
  // Initialize the standard I/O
  init_std_port(stdin, PORT_STDIN);
  init_std_port(stdout, PORT_STDOUT);
  init_std_port(stderr, PORT_STDERR);
}

FILE _LIBOS_STDIN = 
{
  0,                                   // fpos
  _LIBOS_FFLAG_READ,                   // flags
  NULL,                                // buffer
  0,                                   // buffer size
  0,                                   // buffer position
  STDIN_FILENO,                        // UNIX file descriptor
  {PORT_STDIN, PORT_STDIN},            // osdep
  &_LIBOS_STDOUT,
};

FILE _LIBOS_STDOUT =
{
  0,                                   // fpos
  _LIBOS_FFLAG_WRITE | _IOLBF,         // flags
  NULL,                                // buffer
  0,                                   // buffer size
  0,                                   // buffer position
  STDOUT_FILENO,                       // UNIX file descriptor
  {PORT_STDOUT, PORT_STDOUT},          // osdep
  &_LIBOS_STDERR,
};

FILE _LIBOS_STDERR =
{
  0,                                   // fpos
  _LIBOS_FFLAG_WRITE | _IOLBF,         // flags
  NULL,                                // buffer
  0,                                   // buffer size
  0,                                   // buffer position
  STDERR_FILENO,                       // UNIX file descriptor
  {PORT_STDERR, PORT_STDERR},          // osdep
  NULL,
};

int _LIBOS_init_file(struct _LIBOS_FILE *osdep)
{
  osdep->fsport = 0;
  osdep->port = _LIBKERNEL_create_port();
  if (osdep->port == 0)
    return -1;
  return 0;
}

void _LIBOS_get_unique_filename(char *filename,
                                size_t length)
{
  port_id_t Port = _LIBKERNEL_create_port();
  ultostr(Port, filename, filename + length, 16);
  _LIBKERNEL_destroy_port(Port);
}

void _LIBOS_tmp_filename(char *filename)
{
  strcpy(filename, "tmp://");
  _LIBOS_get_unique_filename(&filename[6], _LIBOS_L_tmpnam - 6);
}

void _LIBOS_uninit_file(struct _LIBOS_FILE *osdep)
{
  _LIBKERNEL_destroy_port(osdep->port);
}

int _LIBOS_fopen(const char *filename,
                 struct _LIBOS_FILE *osdep,
                 size_t mode)
{
  // Get the filesystem port and the relative filename
  char *relname = 0;
  osdep->fsport = _LIBSERVER_get_fs_from_name(osdep->port, filename, &relname, malloc);
  if (osdep->fsport == 0)
  {
    free(relname);
    return -1;
  }

  // File exists
  size_t type = _LIBSERVER_file_exists(osdep->port, osdep->fsport, relname);

  // get the open/create flags
  size_t flags = FILE_ACCESS_INFO;
  if ((mode & _LIBOS_FFLAG_READ) != 0)flags |= FILE_ACCESS_READ;
  if ((mode & _LIBOS_FFLAG_WRITE) != 0)flags |= FILE_ACCESS_WRITE;

  if (type == FILE_TYPE_NONE &&
      (mode & _LIBOS_FFLAG_CREATE) != 0)
  {
    // create the file
    bool result = _LIBSERVER_create_file(osdep->port, osdep->fsport, relname, FILE_TYPE_BLOCK, flags, 1, 0);

    if (result == false)
    {
      free(relname);
      osdep->fsport = 0;
      return -1;
    }
  }
  else if (type == FILE_TYPE_BLOCK || type == FILE_TYPE_STREAM)
  {
    // open the file
    bool result = _LIBSERVER_open_file(osdep->port, osdep->fsport, relname, flags);

    if (result == false)
    {
      free(relname);
      osdep->fsport = 0;
      return -1;
    }

    // Truncate?
    if ((mode & _LIBOS_FFLAG_TRUNCATE) != 0)
    {
      //TODO truncate file
      printf("_LIBC_fopen(): truncate file \"%s\"\n", filename);
    }
  }
  else
  {
    free(relname);
    osdep->fsport = 0;
    return -1;
  }

  free(relname);
  return 0;
}

size_t _LIBOS_fsize(struct _LIBOS_FILE *osdep)
{
  message_t msg = _LIBKERNEL_create_message(osdep->fsport, MSG_FS_FILE_INFO, 0, 0, 0);
  _LIBKERNEL_send_message(osdep->port, &msg);
  _LIBKERNEL_add_wait_message(osdep->port, &msg);
  _LIBKERNEL_wait_message(&msg);
  return msg.param2 * msg.param3;
}

size_t _LIBOS_fread(struct _LIBOS_FILE *osdep,
                    size_t fpos,
                    size_t size,
                    void *ptr)
{
  message_t msg = _LIBKERNEL_create_message(osdep->fsport, MSG_FS_READ_FILE, fpos, size, 0);
  _LIBKERNEL_send_message(osdep->port, &msg);
  message_t reply = _LIBKERNEL_create_message(osdep->fsport, MSG_FS_READ_FILE, fpos, 0, 0);
  _LIBKERNEL_add_wait_message(osdep->port, &reply);
  _LIBKERNEL_wait_message(&reply);

  if (reply.param2 == (size_t)-1)
    return EOF;

  if (_LIBKERNEL_get_message_attribute(&reply) != SHM_TRANSFER_OWNERSHIP)
    return 0;

  shared_memory_t shm = _LIBKERNEL_get_shared_memory(&reply);
  size_t cpysize = shm.size;
  if (cpysize > size)cpysize = size;
  memcpy(ptr,
         shm.address,
         cpysize);
  _LIBKERNEL_destroy_shared_memory(&shm);
  return cpysize;
}

size_t _LIBOS_fwrite(struct _LIBOS_FILE *osdep,
                     size_t fpos,
                     size_t size,
                     const void *ptr)
{
  shared_memory_t shm = _LIBKERNEL_create_shared_memory(size);
  memcpy(shm.address, ptr, size);

  message_t msg = _LIBKERNEL_create_message_shm(osdep->fsport, MSG_FS_WRITE_FILE, fpos, &shm, SHM_TRANSFER_OWNERSHIP);
  _LIBKERNEL_send_message(osdep->port, &msg);
  message_t reply = _LIBKERNEL_create_message(osdep->fsport, MSG_FS_WRITE_FILE, fpos, 0, 0);
  _LIBKERNEL_add_wait_message(osdep->port, &reply);
  _LIBKERNEL_wait_message(&reply);
  return reply.param2;
}

int _LIBOS_fclose(struct _LIBOS_FILE *osdep)
{
  message_t msg = _LIBKERNEL_create_message(osdep->fsport, MSG_FS_CLOSE_FILE, 0, 0, 0);
  _LIBKERNEL_send_message(osdep->port, &msg);
  _LIBKERNEL_add_wait_message(osdep->port, &msg);
  _LIBKERNEL_wait_message(&msg);
  return (msg.param1 == 0) ? -1 : 0;
}
