/*
lightOS libc
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <string.h>
#include <stdlib.h>
#include <libkernel/kernel.h>

// TODO: rework

int _cmdline(char **cmdline,
             char **argv)
{
  size_t cmdlineLength = _LIBKERNEL_get_commandline_length();
  *cmdline = (char*)malloc(cmdlineLength + 1);
  _LIBKERNEL_get_commandline(*cmdline);
  (*cmdline)[cmdlineLength] = '\0';

  size_t argc = 0;
  // NOTE: Maximum of 50 arguments TODO really?
  {
    char *cur = *cmdline;
    char *end = *cmdline + cmdlineLength;
    while (1)
    {
      char *found = cur;
      while ( found != end &&
          *found != ' '){++found;}
      *found = '\0';
      argv[argc++] = cur;
      if (found == end)break;
      cur = found + 1;
    }
  }
  argv[argc] = NULL;
  return argc;
}
