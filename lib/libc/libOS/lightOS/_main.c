/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

extern int _cmdline(char **cmdline,
                    char **argv);
extern void _LIBC_init_std_port();
extern void _LIBC_init_environment();

// NOTE: defined in libc/env.c
extern char **environ;

int _main(int (*main)(int, char**, char**))
{
  // Initialize the standard I/O
  _LIBC_init_std_port();

  // Split the command line
  char *cmdline;
  char *argv[50];
  int argc = _cmdline(&cmdline, argv);

  // Initialize the environment variables
  _LIBC_init_environment();

  // Call the main() function
  int ret = main(argc, argv, environ);

  // Free the allocated command line
  free(cmdline);

  return ret;
}
