/*
lightOS libc
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <string.h>
#include <libOS/LIBC_glue.h>
#include <libkernel/kernel.h>
#include <libkernel/type_noprefix.h>
#include <libkernel/message_noprefix.h>
#include <libserver/fs_noprefix.h>

size_t _LIBOS_get_work_directory(char *buffer, size_t buffersize)
{
  // NOTE: Uses the thread port
  port_id_t Port = _LIBKERNEL_get_thread_id();

  // Send request
  message_t msg = _LIBKERNEL_create_message(_LIBKERNEL_PORT_VFS, MSG_VFS_GET_WORKING_DIR, _LIBKERNEL_get_process_id(), 0, 0);
  _LIBKERNEL_send_message(Port, &msg);

  // Wait for reply
  message_t reply = _LIBKERNEL_create_message(_LIBKERNEL_PORT_VFS, MSG_VFS_GET_WORKING_DIR, 0, 0, 0);
  _LIBKERNEL_add_wait_message(Port, &reply);
  _LIBKERNEL_wait_message(&reply);

  if (_LIBKERNEL_get_message_attribute(&reply) == SHM_TRANSFER_OWNERSHIP)
  {
    // Get the shared memory
    shared_memory_t shm = _LIBKERNEL_get_shared_memory(&reply);

    _LIBSERVER_file_name_info *name = shm.address;
    name->name[FILENAME_MAX] = '\0';

    size_t length = strlen(name->name);
    if (buffersize > length)
    {
      strcpy(buffer, name->name);
    }

    // Destroy the shared memory
    _LIBKERNEL_destroy_shared_memory(&shm);
    return length + 1;
  }
  return 0;
}

size_t _LIBOS_set_work_directory(const char *buffer)
{
  // NOTE: Uses the thread port
  port_id_t Port = _LIBKERNEL_get_thread_id();

  // Allocate shared memory
  shared_memory_t shm = _LIBKERNEL_create_shared_memory(sizeof(_LIBSERVER_file_name_info));
  _LIBSERVER_file_name_info *name = shm.address;

  // Copy the string
  strncpy(name->name,
          buffer,
          FILENAME_MAX);

  // Send request
  message_t msg = _LIBKERNEL_create_message_shm(_LIBKERNEL_PORT_VFS, MSG_VFS_SET_WORKING_DIR, _LIBKERNEL_get_process_id(), &shm, SHM_TRANSFER_OWNERSHIP);
  _LIBKERNEL_send_message(Port, &msg);

  // Wait for reply
  message_t reply = _LIBKERNEL_create_message(_LIBKERNEL_PORT_VFS, MSG_VFS_SET_WORKING_DIR, 0, 0, 0);
  _LIBKERNEL_add_wait_message(Port, &reply);
  _LIBKERNEL_wait_message(&reply);

  return (reply.param1 != 0);
}
