/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <libOS/LIBUNIX_glue.h>
#include <libkernel/kernel.h>
#include <libkernel/type_noprefix.h>
#include <libkernel/message_noprefix.h>
#include <libserver/fs_noprefix.h>

// TODO: Do not use errno here but in the calling function

int _LIBOS_stat(const char *path,
                struct stat *buffer)
{
  // TODO: set errno
  // TODO: use thread port
  // Create a new port
  port_id_t port = _LIBKERNEL_create_port();

  // Get the relative path
  char *newpath = 0;
  port_id_t fsport = _LIBSERVER_get_fs_from_name(port, path, &newpath, malloc);

  if (fsport == 0)
  {
    _LIBKERNEL_destroy_port(port);
    return -1;
  }

  // Create the shared memory
  shared_memory_t shm = _LIBKERNEL_create_shared_memory(sizeof(_LIBSERVER_file_name_info));

  // Copy the file name
  _LIBSERVER_file_name_info *Info = shm.address;
  strncpy(Info->name,
      newpath,
      FILENAME_MAX);

  // Send the request
  message_t msg = _LIBKERNEL_create_message_shm(fsport, MSG_FS_FILE_INFO, 0, &shm, SHM_TRANSFER_OWNERSHIP);
  _LIBKERNEL_send_message(port, &msg);

  // Wait for the reply
  message_t reply = _LIBKERNEL_create_message(fsport, MSG_FS_FILE_INFO, 0, 0, 0);
  _LIBKERNEL_add_wait_message(port, &reply);
  _LIBKERNEL_wait_message(&reply);

  if (reply.param1 != FILE_TYPE_BLOCK)
  {
    errno = ENOENT;
    return -1;
  }

  // Fill the stat structure
  buffer->st_dev = 0;
  buffer->st_ino = 0; // TODO
  buffer->st_mode = S_IFREG | S_IRWXU | S_IRWXG | S_IRWXO;
  buffer->st_nlink = 0;
  buffer->st_uid = 0; // TODO
  buffer->st_gid = 0; // TODO
  buffer->st_rdev = 0;
  buffer->st_size = reply.param2 * reply.param3;
  buffer->st_atime = 0; // TODO
  buffer->st_mtime = 0; // TODO
  buffer->st_ctime = 0; // TODO
  buffer->st_blksize = 512;
  buffer->st_blocks = (buffer->st_size + 511) / 512;

  // Deallocate the relative path
  free(newpath);

  // Destroy the port
  _LIBKERNEL_destroy_port(port);
  return 0;
}

// TODO
#include <libc/internal/stdio.h>

int _LIBOS_fstat(int fildes,
                 struct stat *buffer)
{
  FILE *File = _LIBUNIX_get_file(fildes);

  size_t size = _LIBOS_fsize(&File->osdep);

  // Fill the stat structure
  buffer->st_dev = 0;
  buffer->st_ino = 0; // TODO
  buffer->st_mode = S_IFREG | S_IRWXU | S_IRWXG | S_IRWXO;
  buffer->st_nlink = 0;
  buffer->st_uid = 0; // TODO
  buffer->st_gid = 0; // TODO
  buffer->st_rdev = 0;
  buffer->st_size = size;
  buffer->st_atime = 0;
  buffer->st_mtime = 0;
  buffer->st_ctime = 0;
  buffer->st_blksize = 512;
  buffer->st_blocks = (buffer->st_size + 511) / 512;

  return 0;
}
