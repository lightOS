/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <signal.h>
#include <unistd.h>
#include <libOS/LIBUNIX_glue.h>
#include <libkernel/kernel.h>

void _LIBUNIX_signal_handler(void (*func)(int))
{
  _LIBKERNEL_set_signal_handler(func);
}
void _LIBUNIX_signal_handler_end()
{
  _LIBKERNEL_signal_handler_end();
}
int _LIBUNIX_signal(pid_t pid, int sig)
{
  return _LIBKERNEL_signal(pid, sig);
}
