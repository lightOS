/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libOS/LIBUNIX_glue.h>
#include <libkernel/kernel.h>
#include <libkernel/message_noprefix.h>
#include <libserver/fs_noprefix.h>

// TODO
#include <libc/internal/stdio.h>

pid_t _LIBUNIX_getpid()
{
  return _LIBKERNEL_get_process_id();
}

int _LIBUNIX_isatty(FILE *File)
{
   message_t request = _LIBKERNEL_create_message(File->osdep.fsport, MSG_FS_GET_FILE_FLAGS, 0, 0, 0);
  _LIBKERNEL_send_message(File->osdep.port, &request);
  _LIBKERNEL_add_wait_message(File->osdep.port, &request);
  _LIBKERNEL_wait_message(&request);
  return ((request.param1 & FILE_ATTRIBUTE_TTY) != 0) ? 1 : 0;
}
