/*
lightOS libunix
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <libOS/LIBUNIX_glue.h>
#include <libserver/fs_noprefix.h>

int _LIBUNIX_opendir(struct _LIBOS_FILE *osdep, const char *name)
{
	// Get the filesystem port and the relative filename
	char *relname = 0;
	osdep->fsport = _LIBSERVER_get_fs_from_name(osdep->port, name, &relname, malloc);
	if (osdep->fsport == 0)
	{
		free(relname);
		return -1;
	}
	
	// File exists
  /* TODO: This did not work out well
	size_t type = _LIBSERVER_file_exists(osdep->port, osdep->fsport, relname);
	if (type != FILE_TYPE_DIRECTORY)
	{
    _LIBOS_WARNING("libOS", relname);
		free(relname);
		return -1;
	}*/
	
	// open the file
	bool result = _LIBSERVER_open_file(osdep->port, osdep->fsport, relname, FILE_ACCESS_INFO | FILE_ACCESS_READ);
	
	free(relname);
	
	if (result == false)
	{
		osdep->fsport = 0;
		return -1;
	}
	return 0;
}

size_t _LIBUNIX_readdir(struct _LIBOS_FILE *osdep,
						size_t *fpos,
						struct dirent *entry)
{
	entry->d_ino = 0;
  // TODO: Prefix???
	size_t result = _LIBOS_fread(osdep, *fpos, _LIBOS_FILENAME_MAX, entry->d_name);
	++*fpos;
	return result;
}
int _LIBUNIX_closedir(struct _LIBOS_FILE *osdep)
{
	return _LIBOS_fclose(osdep);
}
