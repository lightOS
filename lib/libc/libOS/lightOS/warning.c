/*
lightOS libc
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libOS/LIBC_glue.h>

#include <string.h>
#include <libkernel/kernel.h>
#include <libkernel/type_noprefix.h>
#include <libkernel/message_noprefix.h>

void _LIBOS_warning(const char *mod,
                    const char *file,
                    const char *func,
                    const char *msg)
{
  // NOTE TODO: Properly implement it when we've got a proper kernel log
  size_t len1 = strlen(mod), len2 = strlen(file), len3 = strlen(func), len4 = strlen(msg);

  shared_memory_t shm = _LIBKERNEL_create_shared_memory(len1 + len2 + len3 + len4 + 8);
  strcpy(shm.address, mod);
  strcat(shm.address, ":");
  strcat(shm.address, file);
  strcat(shm.address, ":");
  strcat(shm.address, func);
  strcat(shm.address, "(): ");
  strcat(shm.address, msg);
  strcat(shm.address, "\n");

  message_t message = _LIBKERNEL_create_message_shm(PORT_INIT,
                                                    _LIBKERNEL_MSG_LIBOS_WARNING,
                                                    0,
                                                    &shm,
                                                    SHM_TRANSFER_OWNERSHIP);
  _LIBKERNEL_send_message(_LIBKERNEL_get_thread_id(), &message);
}
