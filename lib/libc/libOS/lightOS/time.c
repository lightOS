/*
lightOS libc
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <time.h>
#include <libOS/LIBC_glue.h>
#include <libkernel/kernel.h>

// NOTE: Defined in libc/time.c
extern size_t _LIBC_calc_day_of_year(size_t year, size_t month);

void _LIBOS_get_time(struct tm *time)
{
  {
    size_t hour, min, sec;
    _LIBKERNEL_get_time(&hour, &min, &sec);
    time->tm_hour = hour;
    time->tm_min = min;
    time->tm_sec = sec;
  }

  {
    size_t wday, mday, mon, year, isdst;
    _LIBKERNEL_get_date(&wday,
                        &mday,
                        &mon,
                        &year,
                        &isdst);
    time->tm_wday = wday;
    time->tm_mday = mday;
    time->tm_mon = mon;
    time->tm_year = year;
    time->tm_isdst = isdst;
  }

  // Calculate day of year
  time->tm_yday = _LIBC_calc_day_of_year(time->tm_year, time->tm_mon - 1) + time->tm_mday;
}
