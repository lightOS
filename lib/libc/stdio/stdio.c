/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libc/internal.h>
#include <libc/internal/stdio.h>
#include <libc/LIBUNIX_glue.h>
#include <libOS/LIBC_glue.h>

#define FILE_FLAG_BUFFER_MASK                 0x03
//_LIBC_FFLAG_READ                              0x04
//_LIBC_FFLAG_WRITE                             0x08
#define FILE_FLAG_EOF                         0x10
#define FILE_FLAG_ERROR                       0x20
#define FILE_FLAG_BUFFER_CHANGED              0x40

static int last_fd = 3;
FILE *file_list = &_LIBOS_STDIN;

// TODO: Prefix???
static char _tmp_filename[_LIBOS_L_tmpnam];

char *tmpnam(char *s)
{
  if (s == NULL)s = _tmp_filename;
  _LIBOS_tmp_filename(s);
  return s;
}

FILE *tmpfile()
{
  char tmp[_LIBOS_L_tmpnam];
  tmpnam(tmp);
  return fopen(tmp, "wb+");
}

// NOTE: libc internal function to parse mode flags
static size_t _LIBC_get_mode(const char *mode)
{
  size_t flags = 0;
  if (strcmp(mode, "r") == 0 ||
      strcmp(mode, "rb") == 0 ||
      strcmp(mode, "rt") == 0)
    flags = _LIBOS_FFLAG_READ;
  else if (strcmp(mode, "w") == 0 ||
           strcmp(mode, "wb") == 0 ||
           strcmp(mode, "wt") == 0)
    flags = _LIBOS_FFLAG_CREATE | _LIBOS_FFLAG_TRUNCATE | _LIBOS_FFLAG_WRITE;
  else if (strcmp(mode, "ab") == 0)
    flags = _LIBOS_FFLAG_APPEND || _LIBOS_FFLAG_CREATE | _LIBOS_FFLAG_WRITE;
  else if (strcmp(mode, "r+") == 0 ||
           strcmp(mode, "r+b") == 0 ||
           strcmp(mode, "rb+") == 0)
    flags = _LIBOS_FFLAG_READ | _LIBOS_FFLAG_WRITE;
  else if (strcmp(mode, "w+") == 0 ||
           strcmp(mode, "w+b") == 0 ||
           strcmp(mode, "wb+") == 0)
    flags = _LIBOS_FFLAG_CREATE | _LIBOS_FFLAG_TRUNCATE | _LIBOS_FFLAG_WRITE | _LIBOS_FFLAG_READ;
  else if (strcmp(mode, "a+") == 0 ||
           strcmp(mode, "a+b") == 0 ||
           strcmp(mode, "ab+") == 0)
    flags = _LIBOS_FFLAG_APPEND | _LIBOS_FFLAG_CREATE | _LIBOS_FFLAG_WRITE | _LIBOS_FFLAG_READ;
  else
  {
    _LIBOS_WARNING("libc", "unknown open mode");
    return 0;
  }
  return flags;
}

// NOTE: internal function to remove a stream from the internal list of streams
static void _LIBC_remove_stream(FILE *stream)
{
  FILE *prev = NULL;
  FILE *cur = file_list;
  while (cur != NULL)
  {
    if (cur == stream)
    {
      if (prev == NULL)
        file_list = cur->next;
      else
        prev->next = cur->next;
    }
    prev = cur;
    cur = cur->next;
  }
}

FILE *fopen(const char *filename,
            const char *mode)
{
  size_t flags = _LIBC_get_mode(mode);
  if (flags == 0)
    return NULL;

  // Allocate the FILE structure
  FILE *File = malloc(sizeof(FILE));
  if (File == NULL)
    return NULL;

  // Initialize the FILE structure
  File->fpos = 0;
  File->flags = (flags & _LIBOS_FFLAG_MASK) | _IOBF;
  File->des = last_fd++;

  // Initialize the _LIBC_FILE structure
  if (_LIBOS_init_file(&File->osdep) != 0)
  {
    // Failed, free FILE structure
    free(File);
    return NULL;
  }

  // TODO: flag append
  if ((flags & _LIBOS_FFLAG_APPEND) != 0)
  {
    _LIBOS_WARNING("libc", "appending not implemented");
  }

  // Open the file
  if (_LIBOS_fopen(filename,
                   &File->osdep,
                   flags)
      != 0)
  {
    // Failed, uninitialize the _LIBC_FILE structure & free FILE structure
    _LIBOS_uninit_file(&File->osdep);
    free(File);
    return NULL;
  }

  // Allocate the buffer
  File->buf = malloc(BUFSIZ);
  File->bufsize = 0;
  File->bufpos = 0;

  // Add to list
  File->next = file_list;
  file_list = File;

  return File;
}

FILE *freopen(const char *path,
              const char *mode,
              FILE *stream)
{
  size_t flags = _LIBC_get_mode(mode);
  if (flags == 0)
    return NULL;

  int result = _LIBOS_fclose(&stream->osdep);
  if (result < 0)
    return NULL;

  // Initialize the FILE structure
  stream->fpos = 0;
  stream->flags = (flags & _LIBOS_FFLAG_MASK) | _IOBF;

  // Initialize the _LIBC_FILE structure
  if (_LIBOS_init_file(&stream->osdep) != 0)
    goto failed;

  // TODO: flag append
  if ((flags & _LIBOS_FFLAG_APPEND) != 0)
  {
    _LIBOS_WARNING("libc", "appending not implemented");
  }

  // Open the file
  if (_LIBOS_fopen(path,
                   &stream->osdep,
                   flags)
      != 0)
  {
    // uninitialize the _LIBC_FILE structure
    _LIBOS_uninit_file(&stream->osdep);
    goto failed;
  }

  return stream;

  failed:
    _LIBC_remove_stream(stream);
    free(stream->buf);
    if (stream->des != STDIN_FILENO &&
        stream->des != STDOUT_FILENO &&
        stream->des != STDERR_FILENO)
    {
      free(stream);
    }
    return NULL;
}

FILE *_LIBC_fdopen(int fildes,
                   const char *mode)
{
  // TODO: Is this correct?

  FILE *cur = file_list;
  while (cur != NULL)
  {
    if (cur->des == fildes)
      return cur;
    cur = cur->next;
  }
  _LIBOS_WARNING("libc", "failed");
  return NULL;
}

int fclose(FILE *file)
{
  // Flush the file
  fflush(file);

  // Close the file
  int result = _LIBOS_fclose(&file->osdep);

  // Deallocate the buffer
  free(file->buf);

  // Remove from the list
  _LIBC_remove_stream(file);

  // Deallocate the FILE structure
  if (file->des != STDIN_FILENO &&
      file->des != STDOUT_FILENO &&
      file->des != STDERR_FILENO)
  {
    free(file);
  }

  return (result < 0) ? EOF : 0;
}

int fflush(FILE *stream)
{
  if ((stream->flags & FILE_FLAG_BUFFER_MASK) != _IONBF)
  {
    if ((stream->flags & FILE_FLAG_BUFFER_CHANGED) != 0)
    {
      // Flush the buffer
      size_t result = _LIBOS_fwrite( &stream->osdep,
                      stream->fpos,
                      stream->bufsize,
                      stream->buf);
      
      // Error occured?
      if (result != stream->bufsize)
      {
        stream->flags |= FILE_FLAG_ERROR;
        return EOF;
      }
    }
    // Clear the buffer-changed flag
    stream->flags &= ~FILE_FLAG_BUFFER_CHANGED;
    
    // Set the new file position
    stream->fpos += stream->bufsize;
    
    // Adjust buffer position/size
    stream->bufpos = 0;
    stream->bufsize = 0;
  }
  return 0;
}

int fgetc(FILE *stream)
{
  // EOF?
  if ((stream->flags & FILE_FLAG_EOF) != 0)
    return EOF;
  
  // Buffered?
  if ((stream->flags & FILE_FLAG_BUFFER_MASK) != _IONBF)
  {
    // Need to read the file?
    if (stream->bufpos >= stream->bufsize)
    {
      // Flush the old buffer
      if (fflush(stream) == EOF)
        return EOF;
      
      // Fill the buffer
      stream->bufsize = _LIBOS_fread(  &stream->osdep,
                      stream->fpos,
                      BUFSIZ,
                      stream->buf);
      
      // New buffer position
      stream->bufpos = 0;
    }
    
    if (stream->bufpos >= stream->bufsize)
    {
      stream->flags |= FILE_FLAG_EOF;
      return EOF;
    }
    return (unsigned char)stream->buf[stream->bufpos++];
  }
  
  // Not buffered?
  char character;
  if (_LIBOS_fread(&stream->osdep,
          stream->fpos,
          1,
          &character)
    != 1)
  {
    stream->flags |= FILE_FLAG_EOF;
    return EOF;
  }
  return character;
}

char *fgets(char *s,
            int n,
            FILE *stream)
{
  int index = 0;
  while (index < (n - 1))
  {
    int character = fgetc(stream);
    
    // EOF or error and nothing read?
    if (character == EOF && index == 0)return NULL;
    // EOF or error?
    else if (character == EOF)goto success;
    
    s[index] = (char)character;
    ++index;
    
    // newline?
    if ((char)character == '\n')goto success;
  }
  
  success:
    s[index] = '\0';
    return s;
}

int fputc(int c, FILE *stream)
{
  // Need to flush?
  if (stream->bufpos >= BUFSIZ)
  {
    // Flush the old buffer
    if (fflush(stream) == EOF)
      return EOF;
  }
  
  if ((stream->flags & FILE_FLAG_BUFFER_MASK) != _IONBF)
  {
    // Allocate the buffer
    if (stream->buf == NULL)
    {
      stream->buf = malloc(BUFSIZ);
      stream->bufsize = 0;
      stream->bufpos = 0;
    }
    
    // Store the character in the buffer
    stream->buf[stream->bufpos++] = c;
    if (stream->bufpos >= stream->bufsize)
      ++stream->bufsize;
    stream->flags |= FILE_FLAG_BUFFER_CHANGED;
    
    if ((stream->flags & FILE_FLAG_BUFFER_MASK) == _IOLBF &&
      c == '\n')
    {
      if (fflush(stream) == EOF)
        return EOF;
    }
  }
  else
  {
    // Write the character direcly
    char character = (char)c;
    size_t result = _LIBOS_fwrite( &stream->osdep,
                    stream->fpos,
                    1,
                    &character);
    
    // Error?
    if (result != 1)
    {
      stream->flags |= FILE_FLAG_ERROR;
      return EOF;
    }
    ++stream->fpos;
  }
  return c;
}

int fputs(const char *s, FILE *stream)
{
  size_t index = 0;
  while (s[index] != '\0')
  {
    if (fputc(s[index], stream) == EOF)
      return EOF;
    ++index;
  }
  return 0;
}

int puts(const char *s)
{
  int result = fputs(s, stdout);
  if (result < 0)return result;
  return fputc('\n', stdout);
}

int ungetc(int c, FILE *stream)
{
  //TODO
  if (stream->fpos > 0 &&
    stream->bufpos > 0)
  {
    --stream->fpos;
    --stream->bufpos;
  }
  else return EOF;
  return c;
}

size_t fread(void *ptr,
             size_t size,
             size_t nmemb,
             FILE *stream)
{
  // TODO; assert size == 0 | nmemb == 0
  size_t i = 0;
  for (;i < (size * nmemb);i++)
  {
    // get the character
    int character = fgetc(stream);
    
    // Is EOF?
    if (character == EOF)
      break;
    
    // Store character
    ((char*)ptr)[i] = (char)character;
  }
  return i / size;
}

size_t fwrite(const void *ptr,
              size_t size,
              size_t nmemb,
              FILE *stream)
{
  // TODO; assert size == 0 | nmemb == 0
  size_t i = 0;
  for (;i < (size * nmemb);i++)
  {
    // Write the character
    if (fputc(((const unsigned char*)ptr)[i], stream) == EOF)
      break;
  }
  
  return i / size;
}

int fgetpos(FILE *stream, fpos_t *pos)
{
  // TODO: error? errno?
  *pos = stream->fpos + stream->bufpos;
  return 0;
}

int fseek(FILE *stream, long int offset, int whence)
{
  // flush the stream
  if (fflush(stream) == EOF)
    return -1;

  // Adjust the stream position
  size_t fsize = _LIBOS_fsize(&stream->osdep);
  if (whence == SEEK_SET &&
      ((size_t)offset < fsize || (stream->flags & _LIBOS_FFLAG_WRITE) != 0))
  {
    stream->fpos = offset;
  }
  else if (whence == SEEK_CUR &&
           (offset + stream->fpos) > 0 &&
           ((offset + stream->fpos) < fsize || (stream->flags & _LIBOS_FFLAG_WRITE) != 0))
  {
    stream->fpos += offset;
  }
  else if (whence == SEEK_END &&
           offset <= 0 &&
           (fsize > (size_t)(-offset) || (stream->flags & _LIBOS_FFLAG_WRITE) != 0))
  {
    stream->fpos = fsize - (size_t)(-offset);
  }
  else
    return -1;
  
  // Clear EOF flag
  stream->flags &= ~FILE_FLAG_EOF;
  return 0;
}

int fsetpos(FILE *stream, const fpos_t *pos)
{
  // TODO: errno?
  // flush the stream
  if (fflush(stream) == EOF)
    return -1;
  
  // Clear the EOF flag
  stream->flags &= ~FILE_FLAG_EOF;
  
  // Set the new position
  stream->fpos = *pos;
  
  return 0;
}

long int ftell(FILE *stream)
{
  // TODO: error (-1L)? errno?
  return stream->fpos + stream->bufpos;
}

void rewind(FILE *stream)
{
  fseek(stream, 0L, SEEK_SET);
  
  // Clear the error flag
  stream->flags &= ~FILE_FLAG_ERROR;
}

void clearerr(FILE *stream)
{
  stream->flags &= ~(FILE_FLAG_EOF | FILE_FLAG_ERROR);
}

int feof(FILE *stream)
{
  if ((stream->flags & FILE_FLAG_EOF) == 0)return 0;
  return -1;
}

int ferror(FILE *stream)
{
  if ((stream->flags & FILE_FLAG_ERROR) == 0)return 0;
  return -1;
}

void perror(const char *s)
{
  if (s != NULL && s[0] != '\0')
    fputs(s, stdout);
  fputs(": ", stdout);
  fputs(strerror(errno), stdout);
}


/*
 * NOTE: libc interal helper function
 */

void _LIBC_fclose_all()
{
  FILE *cur = file_list;
  while (cur != NULL)
  {
    // Flush the file
    fflush(cur);

    // Close the file
    _LIBOS_fclose(&cur->osdep);

    // Deallocate the buffer
    free(cur->buf);

    // Next element
    FILE *File = cur;
    cur = cur->next;

    // Deallocate the FILE structure
    if (File->des != STDIN_FILENO &&
        File->des != STDOUT_FILENO &&
        File->des != STDERR_FILENO)
    {
      free(File);
    }
  }
}
