/*
lightOS libc
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <ctype.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>

#include <stdlib.h>
#include <libOS/LIBC_glue.h>

static int _LIBC_scanf(void *handle,
                       int (*get_char)(void**),
                       void (*unget_char)(void**, char),
                       const char * restrict format,
                       va_list arg);


int fscanf(FILE * restrict stream,
           const char * restrict format,
           ...)
{
  va_list arg;
  va_start(arg, format);
  int result = vfscanf(stream, format, arg);
  va_end(arg);
  return result;
}

int scanf(const char * restrict format,
          ...)
{
  va_list arg;
  va_start(arg, format);
  int result = vscanf(format, arg);
  va_end(arg);
  return result;
}

int sscanf(const char * restrict s,
           const char * restrict format,
           ...)
{
  va_list arg;
  va_start(arg, format);
  int result = vsscanf(s, format, arg);
  va_end(arg);
  return result;
}

static int stream_getc(void **p)
{
  FILE * restrict stream = *(FILE * restrict *)p;
  return getc(stream);
}

static void stream_ungetc(void **p, char c)
{
  FILE * restrict stream = *(FILE * restrict *)p;
  ungetc(c, stream);
}

int vfscanf(FILE * restrict stream,
            const char * restrict format,
            va_list arg)
{
  return _LIBC_scanf(stream,
                     stream_getc,
                     stream_ungetc,
                     format,
                     arg);
}

int vscanf(const char * restrict format,
           va_list arg)
{
  return vfscanf(stdin, format, arg);
}

static int string_getc(void **p)
{
  const char * restrict s = *(const char * restrict *)p;
  if (*s == '\0')
    return EOF;
  int c = *s++;
  *p = (void*)s;
  return c;
}

static void string_ungetc(void **p, char c)
{
  const char * restrict s = *(const char * restrict *)p;
  *p = (void*)(s - 1);
}

int vsscanf(const char * restrict s,
            const char * restrict format,
            va_list arg)
{
  return _LIBC_scanf((void*)s,
                     string_getc,
                     string_ungetc,
                     format,
                     arg);
}


/*
 * Core functionality of the scanf family
 */

int _LIBC_scanf(void *handle,
                int (*get_char)(void**),
                void (*unget_char)(void**, char),
                const char * restrict format,
                va_list arg)
{
  int read = 0;

  while (1)
  {
    /* Process a directive composed of whitespace characters */
    {
      bool directive_whitespace = false;

      /* Skip the whitespace characters in format */
      while (*format != '\0' &&
            isspace(*format))
      {
        ++format;
        directive_whitespace = true;
      }

      /* Skip the whitespace characters in the input */
      if (directive_whitespace == true)
      {
        int c;
        do
        {
          c = get_char(&handle);
        } while (c != EOF && isspace(c) == true);

        if (c != EOF)
          unget_char(&handle, c);
      }
    }

    char fmt_c = *format++;

    /* Through with the format string */
    if (fmt_c == '\0')
      return read;

    /* Ordinary multibyte character directive */
    if (fmt_c != '%')
    {
      int c = get_char(&handle);

      if (c == EOF)
        goto failed;
      if (c != fmt_c)
      {
        unget_char(&handle, c);
        return read;
      }
    }
    /* Conversion specification directive */
    else
    {
      /* Format flags */
      bool assign_suppress = false;

      /* Optional assignment-suppressing character * */
      if (*format == '*')
      {
        assign_suppress = true;
        ++format;
      }

      /* Maximum field width */
      // TODO

      /* Length modifier */
      int integer_mod = 0; // NOTE: -2 -> char
                           //       -1 -> short
                           //        0 -> unchanged
                           //        1 -> long
                           //        2 -> long long
                           //        3 -> intmax_t
                           //        4 -> size_t
                           //        5 -> ptrdiff_t
      bool double_mod = false;
      if (*format == 'h')
      {
        ++format;
        if (*format == 'h')
        {
          ++format;
          integer_mod = -2;
        }
        else
          integer_mod = -1;
      }
      else if (*format == 'l')
      {
        ++format;
        if (*format == 'l')
        {
          ++format;
          integer_mod = 2;
        }
        else
          integer_mod = 1;
      }
      else if (*format == 'j')
      {
        ++format;
        integer_mod = 3;
      }
      else if (*format == 'z')
      {
        ++format;
        integer_mod = 4;
      }
      else if (*format == 't')
      {
        ++format;
        integer_mod = 5;
      }
      else if (*format == 'T')
      {
        double_mod = true;
        ++format;
      }

      /* Possibly clear whitespace characters from the input */
      // TODO

      /* Conversion specifier */
      switch (*format)
      {
/*
        case 'd':
          // TODO
          break;

        case 'i':
          // TODO
          break;

        case 'o':
          // TODO
          break;

        case 'u':
          // TODO
          break;

        case 'x':
          // TODO
          break;

        case 'a':
        case 'e':
        case 'f':
        case 'g':
          // TODO
          break;

        case 'c':
          // TODO
          break;

        case 's':
          // TODO
          break;

        case '[':
          // TODO
          break;

        case 'p':
          // TODO
          break;

        case 'n':
        {
          // TODO
        }break;
*/

        case '%':
        {
          int c = get_char(&handle);
          if (c == EOF)
            goto failed;
          if (c != '%')
          {
            unget_char(&handle, c);
            goto failed;
          }
        }break;

        default:
        {
          _LIBOS_WARNING("libc", "unknown conversion specifier");
          abort();
        }
      }
      ++format;
    }
  }

  return read;

failed:
  if (read == 0)
    return EOF;
  return read;
}
