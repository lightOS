/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <limits.h>

int printf(const char *format, ...)
{
  va_list arg;
  va_start(arg, format);
  int result = vprintf(format, arg);
  va_end(arg);
  return result;
}

int vprintf(const char *format,
            va_list arg)
{
  return vfprintf(stdout, format, arg);
}

int fprintf(FILE *stream,
            const char *format,
            ...)
{
  va_list arg;
  va_start(arg, format);
  int result = vfprintf(stream, format, arg);
  va_end(arg);
  return result;
}

int vfprintf(FILE *stream,
             const char *format,
             va_list arg)
{
  va_list argcopy;
  va_copy(argcopy, arg);
  int result = vsnprintf(0, 0, format, argcopy);
  va_end(argcopy);
  if (result < 0)return result;
  char *array = malloc(result + 1);
  result = vsnprintf(array, result + 1, format, arg);
  if (result < 0)
  {
    free(array);
    return result;
  }
  result = fwrite(array, result, 1, stream);
  free(array);
  return result;
}

int sprintf(char *s,
            const char *format,
            ...)
{
  va_list arg;
  va_start(arg, format);
  int result = vsprintf(s, format, arg);
  va_end(arg);
  return result;
}

int vsprintf(char *s,
             const char *format,
             va_list arg)
{
  return vsnprintf(s, UINT_MAX, format, arg);
}

int snprintf(char *s,
             size_t n,
             const char *format,
             ...)
{
  va_list arg;
  va_start(arg, format);
  int result = vsnprintf(s, n, format, arg);
  va_end(arg);
  return result;
}
