/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libOS/LIBC_glue.h>

/* Helper definitions */
#define FLAG_LEFT_JUSTIFIED                 0x0001
#define LENGTH_LONG                         0x0001
#define LENGTH_LONGLONG                     0x0002


int vsnprintf(char *s,
              size_t n,
              const char *format,
              va_list arg)
{
  const char *tmp = format;
  size_t written = 0;
  while (*tmp != '\0')
  {
    if (*tmp == '%')
    {
      ++tmp;

      /* Flags */
      size_t flags = 0;
      size_t bBreak = 1;
      while (bBreak)
      {
        switch (*tmp)
        {
          case '-':
          {
            flags |= FLAG_LEFT_JUSTIFIED;
          }break;
          case '+':
          {
          }break;
          case ' ':
          {
          }break;
          case '#':
          {
          }break;
          case '0':
          {
          }break;
          default:
          {
            bBreak = 0;
          }break;
        }
        if (bBreak == 1)++tmp;
      }

      /* Field width */
      unsigned long fieldwidth;
      if (*tmp == '*')
      {
        fieldwidth = va_arg(arg, int);
        ++tmp;
      }
      else
      {
        char *next = NULL;
        fieldwidth = strtoul(tmp, &next, 10);
        tmp = next;
      }

      // TODO: precision

      /* length modifier */
      size_t lengthmod = 0;
      bBreak = 1;
      while (bBreak)
      {
        switch (*tmp)
        {
          case 'l':
          {
            if ((lengthmod & LENGTH_LONG) != 0)
            {
              lengthmod = (lengthmod & (~LENGTH_LONG)) | LENGTH_LONGLONG;
            }
            else if ((lengthmod & LENGTH_LONGLONG) == 0)
            {
              lengthmod |= LENGTH_LONG;
            }
          }break;
          default:
          {
            bBreak = 0;
          }break;
        }
        if (bBreak == 1)++tmp;
      }

      /* Conversion specifier */
      switch (*tmp)
      {
        case '%':
        {
          if (s != NULL && written < (n - 1))
          {
            *s++ = '%';
          }
          ++written;
        }break;
        case 'i':
        case 'd':
        {
          long long value;
          if ((lengthmod & LENGTH_LONGLONG) != 0)value = va_arg(arg, long long);
          else if ((lengthmod & LENGTH_LONG) != 0)value = va_arg(arg, long);
          else value = va_arg(arg, int);
          int result = 0;

          if (s == NULL || written > (n - 1))result = lltostr(value, NULL, NULL, 10);
          else result = lltostr(value, s, s + n - 1 - written, 10);
          if (result > 0)
          {
            written += result;
            if (s != NULL)s += result;
          }
        }break;
        case 'u':
        {
          unsigned long long value;
          if ((lengthmod & LENGTH_LONGLONG) != 0)value = va_arg(arg, unsigned long long);
          else if ((lengthmod & LENGTH_LONG) != 0)value = va_arg(arg, unsigned long);
          else value = va_arg(arg, unsigned int);
          int result = 0;

          if (s == NULL || written > (n - 1))result = ulltostr(value, NULL, NULL, 10);
          else result = ulltostr(value, s, s + n - 1 - written, 10);
          if (result > 0)
          {
            written += result;
            if (s != NULL)s += result;
          }
        }break;
        case 'x':
        case 'X':
        {
          unsigned long long value;
          if ((lengthmod & LENGTH_LONGLONG) != 0)value = va_arg(arg, unsigned long long);
          else if ((lengthmod & LENGTH_LONG) != 0)value = va_arg(arg, unsigned long);
          else value = va_arg(arg, unsigned int);
          int result = 0;

          if (s == NULL || written > (n - 1))result = ulltostr(value, NULL, NULL, 16);
          else result = ulltostr(value, s, s + n - 1 - written, 16);
          if (result > 0)
          {
            written += result;
            if (s != NULL)s += result;
          }
        }break;
        case 'c':
        {
          char character = va_arg(arg, int);
          if (s != NULL && written < (n - 1))
            *s++ = character;
          ++written;
        }break;
        case 's':
        {
          char *string = va_arg(arg, char*);
          size_t length = strlen(string);

          if ((flags & FLAG_LEFT_JUSTIFIED) == 0 &&
            fieldwidth != 0)
          {
            size_t y = length;
            for (;y < fieldwidth;y++)
            {
              if (s != NULL && written < (n - 1))
                *s++ = ' ';
              ++written;
            }
          }

          while (*string != '\0')
          {
            if (s != NULL && written < (n - 1))
              *s++ = *string;
            ++string;
            ++written;
          }

          if ((flags & FLAG_LEFT_JUSTIFIED) != 0 &&
            fieldwidth != 0)
          {
            size_t y = length;
            for (;y < fieldwidth;y++)
            {
              if (s != NULL && written < (n - 1))
                *s++ = ' ';
              ++written;
            }
          }
        }break;
        default:
        {
          _LIBOS_WARNING("libc", "unimplemented conversion specifier in format");
          return -1;
        };
      }
    }
    else
    {
      if (s != NULL && written < (n - 1))
        *s++ = *tmp;
      ++written;
    }
    ++tmp;
  }
  if (s != NULL)
    *s = '\0';
  return written;
}
