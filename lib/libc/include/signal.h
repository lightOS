/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBUNIX_SIGNAL_H
#define LIGHTOS_LIBUNIX_SIGNAL_H

/*! \addtogroup libunix libunix */
/*@{*/

#include <lightlibc/compiler.h>
#include <sys/types.h>

/*! Request for default signal handling */
#define SIG_DFL _signal_default_handler
/*! Return value from signal() in case of error */
#define SIG_ERR 0
/*! Request that signal be held */
// TODO #define SIG_HOLD _signal_hold
/*! Request that signal be ignored */
#define SIG_IGN 0

/*
//! No asynchronous notification is delivered when the event of interest occurs
#define SIGEV_NONE                          0
//! A queued signal, with an application-defined value, is generated when the event of interest occurs
#define SIGEV_SIGNAL                        1
//! A notification function is called to perform notification
#define SIGEV_THREAD                        2
*/

// TODO

/*! Process abort signal */
#define SIGABRT                           1
/*! Continue executing, if stopped */
#define SIGCONT                           2
/*! Erroneous arithmetic operation */
#define SIGFPE                            3
/*! Hangup */
#define SIGHUP                            4
/*! Illegal instruction */
#define SIGILL                            5
/*! Terminal interrupt signal */
#define SIGINT                            6
/*! Kill (cannot be caught or ignored) */
#define SIGKILL                           7
/*! Terminal quit signal */
#define SIGQUIT                           8
/*! Invalid memory reference */
#define SIGSEGV                           9
/*! Stop executing (cannot be caught or ignored) */
#define SIGSTOP                           10
/*! Termination signal */
#define SIGTERM                           11
/*! Terminal stop signal */
#define SIGTSTP                           12

#define SIGTTOU                           13
#define SIGTTIN                           14
#define SIGPIPE                           15

#define SIGALRM                           16
#define SIGMAX                            16

// TODO: Not POSIX
#define NSIG                              (SIGMAX + 1)

// TODO: This is a fix for make
#define SIG_BLOCK 1
#define SIG_SETMASK 2

// TODO: define sig_atomic_t
typedef unsigned int sigset_t;

union sigval
{
  //! Integer signal value
  int sival_int;
  //! Pointer signal value
  void *sival_ptr;
};

typedef struct
{
  //! Notification type
  int sigev_notify;
  //! Signal number
  int sigev_signo;
  //! Signal value
  union sigval sigev_value;
  //! Notification function
  void (*sigev_notify_function)(union sigval);
  //! Notification attributes
  // TODO pthread_attr_t *sigev_notify_attributes
} sigevent;

typedef struct
{
  //! Signal number
  int si_signo;
  //! Signal code
  int si_code;
  //! If non-zero, an errno value associated with this signal, as defined in <errno.h>
  int si_errno;
  //! Sending process ID
  pid_t si_pid;
  //! Real user ID of sending process
  uid_t si_uid;
  //! Address of faulting instruction
  void *si_addr;
  //! Exit value or signal
  int si_status;
  //! Band event for SIGPOLL
  long si_band;
  //! Signal value
  union sigval si_value;
} siginfo_t;

struct sigaction
{
  //! Pointer to a signal-catching function or one of the macros SIG_IGN or SIG_DFL
  void (*sa_handler)(int);
  //! Set of signals to be blocked during execution of the signal handling function
  sigset_t sa_mask;
  //! Special flags
  int sa_flags;
  //! Pointer to a signal-catching function
  void (*sa_sigaction)(int, siginfo_t *, void *);
};

// TODO
typedef _LIBARCH_atomic_int_t sig_atomic_t;

#ifdef __cplusplus
  extern "C"
  {
#endif

  /*! Default signal handler */
  void _signal_default_handler(int sig);
  /*! Hold the signal */
  // TODO void _signal_hold(int sig);

  int sigaction(int sig,
                const struct sigaction *_LIGHTLIBC_RESTRICT act,
                struct sigaction *_LIGHTLIBC_RESTRICT oact);
  int sigaddset(sigset_t *, int);
  int sigemptyset(sigset_t *set);
  int sigfillset(sigset_t *set);
  void (*signal(int sig, void (*func)(int)))(int);
  int kill(pid_t pid, int sig);
  int raise(int sig);
  int sigprocmask(int, const sigset_t *, sigset_t *);

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
