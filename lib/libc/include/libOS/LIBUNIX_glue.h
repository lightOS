/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBUNIX_LIBUNIX_H
#define LIGHTOS_LIBUNIX_LIBUNIX_H

/*! \addtogroup libunix libunix */
/*@{*/

#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libOS/LIBC_glue.h>

/* Forward declaration */
// TODO struct _LIBOS_WINDOW;

// TODO
#define P_tmpdir                    "tmp://"

// Glue functions a libunix port has to implement
#ifdef __cplusplus
  extern "C"
  {
#endif

/* TODO Curses
    bool _LIBOS_init_window(struct _LIBOS_WINDOW *osdep,
                            size_t width,
                            size_t height,
                            bool curscr);
    char *_LIBOS_window_data(struct _LIBOS_WINDOW *osdep);
    void _LIBOS_screen_update(struct _LIBOS_WINDOW *screen,
                              FILE *outfile);
    void _LIBOS_deinit_window(struct _LIBOS_WINDOW *osdep);
*/

    // Directory management functions
    int _LIBUNIX_opendir(struct _LIBOS_FILE *osdep,
                         const char *name);
    size_t _LIBUNIX_readdir(struct _LIBOS_FILE *osdep,
                            size_t *fpos,
                            struct dirent *entry);
    int _LIBUNIX_closedir(struct _LIBOS_FILE *osdep);

    // Working directory functions
    size_t _LIBOS_get_work_directory(char *buffer,
                                     size_t buffersize);
    size_t _LIBOS_set_work_directory(const char *buffer);

    // Process management functions
    pid_t _LIBUNIX_getpid();

    // TTY
    int _LIBUNIX_isatty(FILE *File);

    // Signal management
    void _LIBUNIX_signal_handler(void (*func)(int));
    void _LIBUNIX_signal_handler_end();
    int _LIBUNIX_signal(pid_t pid,
                        int sig);

    int _LIBOS_stat(const char *path,
                    struct stat *buffer);
    int _LIBOS_fstat(int fildes,
                     struct stat *buffer);

    // Glue functions to libc
    FILE *_LIBUNIX_get_file(int fd);
    int _LIBUNIX_get_fd(FILE *File);

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
