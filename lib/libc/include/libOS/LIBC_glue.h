/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBC_LIBOS_LIBC_GLUE_H
#define LIGHTOS_LIBC_LIBOS_LIBC_GLUE_H

#include <time.h>
#include <stddef.h>
#include <libOS/this/LIBC_glue.h>

/*! \addtogroup libc libc */
/*@{*/
/*! \addtogroup libosinterface libOS - interface */
/*@{*/

// Defines for the general libc <-> system specific functions interface
#define _LIBOS_FFLAG_CREATE            0x01
#define _LIBOS_FFLAG_TRUNCATE          0x02
#define _LIBOS_FFLAG_READ              0x04
#define _LIBOS_FFLAG_WRITE             0x08
#define _LIBOS_FFLAG_APPEND            0x10
#define _LIBOS_FFLAG_MASK              0x0C

#define _LIBOS_WARNING(mod, str)       _LIBOS_warning(mod, __FILE__, __func__, str);

// Glue functions a libc port has to implement
#ifdef __cplusplus
  extern "C"
  {
#endif

    // Libc warning (about unimplemented functions)
    void _LIBOS_warning(const char *mod,
                        const char *file,
                        const char *func,
                        const char *msg);

    // Memory management functions
    void *_LIBOS_allocPage(size_t n);

    // Process management functions
    void _LIBOS_exit(int status);
    void _LIBOS_abort(void);

    // Environment variables
    size_t _LIBOS_env_count();
    size_t _LIBOS_get_env_length(size_t index);
    void _LIBOS_get_env(size_t index,
                        char *env);

    // File management functions
    int _LIBOS_init_file(struct _LIBOS_FILE *osdep);
    void _LIBOS_uninit_file(struct _LIBOS_FILE *osdep);

    void _LIBOS_get_unique_filename(char *filename,
                                    size_t length);
    void _LIBOS_tmp_filename(char *filename);
    int _LIBOS_fopen(const char *filename,
                     struct _LIBOS_FILE *osdep,
                     size_t mode);
    size_t _LIBOS_fsize(struct _LIBOS_FILE *osdep);
    size_t _LIBOS_fread(struct _LIBOS_FILE *osdep,
                        size_t fpos,
                        size_t size,
                        void *ptr);
    size_t _LIBOS_fwrite(struct _LIBOS_FILE *osdep,
                         size_t fpos,
                         size_t size,
                         const void *ptr);
    int _LIBOS_fclose(struct _LIBOS_FILE *osdep);

    // Time functions
    void _LIBOS_get_time(struct tm *time);

#ifdef __cplusplus
  }
#endif

/*@}*/
/*@}*/

#endif
