/*
lightOS libc
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBC_LIBOS_LIGHTOS_TYPES_H
#define LIGHTOS_LIBC_LIBOS_LIGHTOS_TYPES_H


/*
 * Libarch includes
 */
#include <libarch/type.h>


/*! \addtogroup libc libc */
/*@{*/
/*! \addtogroup liboslightos libOS - lightOS */
/*@{*/

typedef _LIBARCH_uint32_t _LIBOS_time_t;
typedef _LIBARCH_uint32_t _LIBOS_clock_t;

/*@}*/
/*@}*/

#endif
