/*
lightOS libc
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef _LIGHTLIBC_INTTYPES_H
#define _LIGHTLIBC_INTTYPES_H



/*! \addtogroup libc_inttypes */
/*@{*/

#include <stdint.h>

// TODO: libarch?

typedef struct
{
  intmax_t quot;
  intmax_t rem;
} imaxdiv_t;


// TODO 7.8.1 Macros for format speciﬁers

#define _scn8     "hh"
#define _pri8     ""

#define _scn16      "h"
#define _pri16      ""

#define _scn32      ""
#define _pri32      ""

#ifdef X86
  #define _scn64      "ll"
  #define _pri64      "ll"
#endif
#ifdef X86_64
  #define _scn64      "l"
  #define _pri64      "l"
#endif


#define PRId8   _pri8  "d"
#define PRId16    _pri16 "d"
#define PRId32    _pri32 "d"
#define PRId64    _pri64 "d"

#define PRIi8   _pri8  "i"
#define PRIi16    _pri16 "i"
#define PRIi32    _pri32 "i"
#define PRIi64    _pri64 "i"

#define PRIo8   _pri8  "o"
#define PRIo16    _pri16 "o"
#define PRIo32    _pri32 "o"
#define PRIo64    _pri64 "o"

#define PRIu8   _pri8  "u"
#define PRIu16    _pri16 "u"
#define PRIu32    _pri32 "u"
#define PRIu64    _pri64 "u"

#define PRIx8   _pri8  "x"
#define PRIx16    _pri16 "x"
#define PRIx32    _pri32 "x"
#define PRIx64    _pri64 "x"

#define PRIX8   _pri8  "X"
#define PRIX16    _pri16 "X"
#define PRIX32    _pri32 "X"
#define PRIX64    _pri64 "X"

#define SCNd8   _scn8  "d"
#define SCNd16    _scn16 "d"
#define SCNd32    _scn32 "d"
#define SCNd64    _scn64 "d"

#define SCNi8   _scn8  "i"
#define SCNi16    _scn16 "i"
#define SCNi32    _scn32 "i"
#define SCNi64    _scn64 "i"

#define SCNo8   _scn8  "o"
#define SCNo16    _scn16 "o"
#define SCNo32    _scn32 "o"
#define SCNo64    _scn64 "o"

#define SCNu8   _scn8  "u"
#define SCNu16    _scn16 "u"
#define SCNu32    _scn32 "u"
#define SCNu64    _scn64 "u"

#define SCNx8   _scn8  "x"
#define SCNx16    _scn16 "x"
#define SCNx32    _scn32 "x"
#define SCNx64    _scn64 "x"

/* 7.8.2 Functions for greatest-width integer types */

/* 7.8.2.1 The imaxabs function */
intmax_t imaxabs(intmax_t j);

/* 7.8.2.2 The imaxdiv function */
imaxdiv_t imaxdiv(intmax_t numer, intmax_t denom);

/* TODO 7.8.2.3 The strtoimax and strtoumax functions */
/* TODO 7.8.2.4 The wcstoimax and wcstoumax functions */

/*@}*/



#endif
