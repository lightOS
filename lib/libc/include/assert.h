/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/



/*! \addtogroup libc_assert */
/*@{*/

/* Quoting from the ISO/IEC 9899:1999 standard 7.2 Diagnostics <assert.h>:
   The assert macro is redeﬁned according to the current state of NDEBUG each time that
   <assert.h> is included.

   NOTE: The lack of inclusion guards is intentional
*/
#undef assert

#ifndef NDEBUG
  /** ISO/IEC 9899:1999 7.2.1.1
   *
   *  The assert macro puts diagnostic tests into programs; it expands to a void expression.
   *  When it is executed, if expression (which shall have a scalar type) is false (that is,
   *  compares equal to 0), the assert macro writes information about the particular call that
   *  failed (including the text of the argument, the name of the source ﬁle, the source line
   *  number, and the name of the enclosing function — the latter are respectively the values of
   *  the preprocessing macros __FILE__ and __LINE__ and of the identiﬁer
   *  __func__) on the standard error stream in an implementation-deﬁned format. It
   *  then calls the abort function.
   *
   *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/assert.html */
  #define assert(expression) (!(expression)) ? _LIBC_assert(#expression, __FILE__, __LINE__, __func__) : ((void)0)
#else
  #define assert(ignore) ((void)0)
#endif

/*@}*/



/*! \addtogroup libc_libOS */
/*@{*/

/*
 * Support functions
 */

#ifndef _LIGHTLIBC_ASSERT_H
#define _LIGHTLIBC_ASSERT_H

#ifdef __cplusplus
  extern "C"
  {
#endif

    /** Support function for the assert() macro. Prints the assertion output.
     *\param[in] file pointer to the file name
     *\param[in] line line
     *\param[in] func pointer to the function name */
    void _LIBC_assert(const char* expression,
		      const char* file,
                      int line,
                      const char* func);

#ifdef __cplusplus
  }
#endif

#endif

/*@}*/
