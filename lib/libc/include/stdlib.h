/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef _LIGHTLIBC_STDLIB_H
#define _LIGHTLIBC_STDLIB_H


#include <lightlibc/compiler.h>

/*
 * Standard includes
 */
#include <stddef.h>                    // NOTE: size_t, NULL
#include <limits.h>                    // NOTE: INT_MAX


/*
 * libc includes
 */
#include <libc/types.h>                // NOTE: _LIBC_div_t, _LIBC_ldiv_t, _LIBC_lldiv_t

/*
 * libOS includes
 */
#include <libOS/this/defines.h>        // NOTE: _LIBOS_EXIT_SUCCESS, _LIBOS_EXIT_FAILURE



/** \addtogroup libc_stdlib */
/*@{*/

// TODO: Rework and make standard compliant

//! \todo libunix
#define WNOHANG   1
#define WUNTRACED 2

/** Expand to integer constant expressions that can be used as the argument to the
 *  exit function to return successful termination status to the host environment */
#define EXIT_SUCCESS                   _LIBOS_EXIT_SUCCESS
/** Expand to integer constant expressions that can be used as the argument to the
 *  exit function to return unsuccessful termination status to the host environment */
#define EXIT_FAILURE                   _LIBOS_EXIT_FAILURE
/** Expands to an integer constant expression that is the maximum value returned by
 *  the rand function */
#define RAND_MAX                       INT_MAX

/** Expands to a positive integer expression with type size_t that is the maximum
 *  number of bytes in a multibyte character for the extended character set speciﬁed by the
 *  current locale (category LC_CTYPE), which is never greater than MB_LEN_MAX. */
#define MB_CUR_MAX                     (size_t)4

//! \todo wchar_t

typedef _LIBC_div_t div_t;
typedef _LIBC_ldiv_t ldiv_t;
typedef _LIBC_lldiv_t lldiv_t;

#ifdef __cplusplus
  extern "C"
  {
#endif

    /*
     * ISO/IEC 9899:1999 7.20.1 Numeric conversion functions
     */

    /** ISO/IEC 9899:1999 7.20.1.1
     *
     *  The atof function converts the initial portion of the string pointed to by nptr to
     *  double representation. Except for the behavior on error, it is equivalent to:
     *
     *  strtod(nptr, (char **)NULL)
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/atof.html
     *\param[in] nptr string that should be converted
     *\return the converted value */
    double atof(const char *nptr);

    /** ISO/IEC 9899:1999 7.20.1.2
     *
     *  The atoi function convert the initial portion of the string pointed to by nptr to int representation.
     *  Except for the behavior on error, this is equivalent to:
     *
     *  (int)strtol(nptr, (char **)NULL, 10)
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/atoi.html
     *\param[in] nptr string that should be converted
     *\return the converted value */
    int atoi(const char *nptr);

    /** ISO/IEC 9899:1999 7.20.1.2
     *
     *  The atol function convert the initial portion of the string pointed to by nptr to long representation.
     *  Except for the behavior on error, this is equivalent to:
     *
     *  strtol(nptr, (char **)NULL, 10)
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/atol.html
     *\param[in] nptr string that should be converted
     *\return the converted value */
    long int atol(const char *nptr);

    /** ISO/IEC 9899:1999 7.20.1.2
     *
     *  The atoll function convert the initial portion of the string pointed to by nptr to long long
     *  representation. Except for the behavior on error, this is equivalent to:
     *
     *   strtoll(nptr, (char **)NULL, 10)
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/atoll.html
     *\param[in] nptr string that should be converted
     *\return the converted value */
    long long int atoll(const char *nptr);

    /** ISO/IEC 9899:1999 7.20.1.3
      * */
    double strtod(const char * _LIGHTLIBC_RESTRICT nptr,
                  char ** _LIGHTLIBC_RESTRICT endptr);
    float strtof(const char * _LIGHTLIBC_RESTRICT nptr,
                 char ** _LIGHTLIBC_RESTRICT endptr);
    long double strtold(const char * _LIGHTLIBC_RESTRICT nptr,
                        char ** _LIGHTLIBC_RESTRICT endptr);


    // TODO
    long int strtol(const char *nptr,
                    char **endptr,
                    int base);
    long long int strtoll(const char *nptr,
                          char **endptr,
                          int base);
    unsigned long int strtoul(const char *nptr,
                              char **endptr,
                              int base);
    unsigned long long int strtoull(const char *nptr,
                                    char **endptr,
                                    int base);

    // TODO: these are own extensions
    int ltostr(long int val,
               char *nptr,
               char *endptr,
               int base);
    int lltostr(long int val,
                char *nptr,
                char *endptr,
                int base);
    int ultostr(unsigned long val,
                char *nptr,
                char *endptr,
                int base);
    int ulltostr(unsigned long long val,
                 char *nptr,
                 char *endptr,
                 int base);


    /*
     * ISO/IEC 9899:1999 7.20.2 Pseudo-random sequence generation functions
     */

    /** ISO/IEC 9899:1999 7.20.2.1
     *
     *  The rand function computes a sequence of pseudo-random integers in the range 0 to
     *  RAND_MAX.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/rand.html
     *\return The rand function returns a pseudo-random integer. */
    int rand();

    /** ISO/IEC 9899:1999 7.20.2.2
     *
     *  The srand function uses the argument as a seed for a new sequence of pseudo-random
     *  numbers to be returned by subsequent calls to rand. If srand is then called with the
     *  same seed value, the sequence of pseudo-random numbers shall be repeated. If rand is
     *  called before any calls to srand have been made, the same sequence shall be generated
     *  as when srand is ﬁrst called with a seed value of 1.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/srand.html
     *\param[in] seed the seed value */
    void srand(unsigned int seed);


    /*
     * ISO/IEC 9899:1999 7.20.3 Memory management functions
     */

    /** ISO/IEC 9899:1999 7.20.3.1
     *
     *  The calloc function allocates space for an array of nmemb objects, each of whose size
     *  is size. The space is initialized to all bits zero.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/calloc.html
     *\param[in] nmemb the number of objects to allocate memory for
     *\param[in] size  the size of one objects
     *\return The calloc function returns either a null pointer ([CX] errno is set to indicate the error,
     *        ENOMEM) or a pointer to the allocated space. */
    void *calloc(size_t nmemb,
                 size_t size);

    /** ISO/IEC 9899:1999 7.20.3.2
     *
     *  The free function causes the space pointed to by ptr to be deallocated, that is, made
     *  available for further allocation. If ptr is a null pointer, no action occurs. Otherwise, if
     *  the argument does not match a pointer earlier returned by the calloc, malloc, or
     *  realloc function, or if the space has been deallocated by a call to free or realloc,
     *  the behavior is undeﬁned.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/free.html
     *\param[in] ptr the pointer to the memory that should be deallocated */
    void free(void *ptr);

    /** ISO/IEC 9899:1999 7.20.3.3
     *
     *  The malloc function allocates space for an object whose size is speciﬁed by size and
     *  whose value is indeterminate.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/malloc.html
     *\param[in] size the size in bytes that should be allocated
     *\return The malloc function returns either a null pointer ([CX] errno is set to indicate the error,
     *        ENOMEM) or a pointer to the allocated space. */
    void *malloc(size_t size);

    /** ISO/IEC 9899:1999 7.20.3.4
     *
     *  The realloc function deallocates the old object pointed to by ptr and returns a
     *  pointer to a new object that has the size speciﬁed by size. The contents of the new
     *  object shall be the same as that of the old object prior to deallocation, up to the lesser of
     *  the new and old sizes. Any bytes in the new object beyond the size of the old object have
     *  indeterminate values.
     *  If ptr is a null pointer, the realloc function behaves like the malloc function for the
     *  speciﬁed size. Otherwise, if ptr does not match a pointer earlier returned by the
     *  calloc, malloc, or realloc function, or if the space has been deallocated by a call
     *  to the free or realloc function, the behavior is undeﬁned. If memory for the new
     *  object cannot be allocated, the old object is not deallocated and its value is unchanged.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/realloc.html
     *\param[in] ptr
     *\param[in] size
     *\return The realloc function returns a pointer to the new object (which may have the same
     *        value as a pointer to the old object), or a null pointer if the new object could not be
     *        allocated [CX] and sets errno to ENOMEM. */
    void *realloc(void *ptr,
                  size_t size);


    /*
     * ISO/IEC 9899:1999 7.20.4 Communication with the environment
     */

    /** ISO/IEC 9899:1999 7.20.4.1
     *
     *  The abort function causes abnormal program termination to occur, unless the signal
     *  SIGABRT is being caught and the signal handler does not return. Whether open streams
     *  with unwritten buffered data are ﬂushed, open streams are closed, or temporary ﬁles are
     *  removed is implementation-deﬁned. An implementation-deﬁned form of the status
     *  unsuccessful termination is returned to the host environment by means of the function
     *  call raise(SIGABRT).
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/abort.html
     *\todo verify that we conform to this stuff */
    void abort();

    /** ISO/IEC 9899:1999 7.20.4.2
     *
     *  The atexit function registers the function pointed to by func, to be called without
     *  arguments at normal program termination.
     *  [CX] After a successful call to any of the exec functions, any functions previously
     *       registered by atexit() shall no longer be registered.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/atexit.html
     *\param[in] func pointer to the function that should be registered
     *\return The atexit function returns zero if the registration succeeds, nonzero if it fails. */
    int atexit(void (*func)(void));

    /** ISO/IEC 9899:1999 7.20.4.3
     *
     *  The exit function causes normal program termination to occur. If more than one call to
     *  the exit function is executed by a program, the behavior is undeﬁned.
     *  First, all functions registered by the atexit function are called, in the reverse order of
     *  their registration, except that a function is called after any previously registered
     *  functions that had already been called at the time it was registered. If, during the call to
     *  any such function, a call to the longjmp function is made that would terminate the call
     *  to the registered function, the behavior is undeﬁned.
     *  Next, all open streams with unwritten buffered data are ﬂushed, all open streams are
     *  closed, and all ﬁles created by the tmpfile function are removed.
     *  Finally, control is returned to the host environment. If the value of status is zero or
     *  EXIT_SUCCESS, an implementation-deﬁned form of the status successful termination is
     *  returned. If the value of status is EXIT_FAILURE, an implementation-deﬁned form
     *  of the status unsuccessful termination is returned. Otherwise the status returned is
     *  implementation-deﬁned.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/exit.html
     *\todo Verfy that we conform to this stuff
     *\param[in] status return status */
    void exit(int status);

    /** ISO/IEC 9899:1999 7.20.4.4
     *
     *  The _Exit function causes normal program termination to occur and control to be
     *  returned to the host environment. No functions registered by the atexit function or
     *  signal handlers registered by the signal function are called. The status returned to the
     *  host environment is determined in the same way as for the exit function (7.20.4.3).
     *  Whether open streams with unwritten buffered data are ﬂushed, open streams are closed,
     *  or temporary ﬁles are removed is implementation-deﬁned.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/exit.html
     *\todo Verfy that we conform to this stuff
     *\param[in] status the return status */
    void _Exit(int status);

    /** ISO/IEC 9899:1999 7.20.4.5
     *
     *  The getenv function searches an environment list, provided by the host environment,
     *  for a string that matches the string pointed to by name. The set of environment names
     *  and the method for altering the environment list are implementation-deﬁned.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/getenv.html
     *\param[in] name name of the environment variable
     *\return The getenv function returns a pointer to a string associated with the matched list
     *        member. The string pointed to shall not be modiﬁed by the program, but may be
     *        overwritten by a subsequent call to the getenv function. If the speciﬁed name cannot
     *        be found, a null pointer is returned. */
    char *getenv(const char *name);

    /** ISO/IEC 9899:1999 7.20.4.6
     *
     *  If string is a null pointer, the system function determines whether the host
     *  environment has a command processor. If string is not a null pointer, the system
     *  function passes the string pointed to by string to that command processor to be
     *  executed in a manner which the implementation shall document; this might then cause the
     *  program calling system to behave in a non-conforming manner or to terminate.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/009695399/functions/system.html
     *\todo maybe implement
     *\return If the argument is a null pointer, the system function returns nonzero only if a
     *        command processor is available. If the argument is not a null pointer, and the system
     *        function does return, it returns an implementation-deﬁned value. */
    int system(const char *string);


    /*
     * ISO/IEC 9899:1999 7.20.5 Searching and sorting utilities
     */

    /** ISO/IEC 9899:1999 7.20.5.1
     *
     *  The bsearch function searches an array of nmemb objects, the initial element of which
     *  is pointed to by base, for an element that matches the object pointed to by key. The
     *  size of each element of the array is speciﬁed by size.
     *  The comparison function pointed to by compar is called with two arguments that point
     *  to the key object and to an array element, in that order. The function shall return an
     *  integer less than, equal to, or greater than zero if the key object is considered,
     *  respectively, to be less than, to match, or to be greater than the array element. The array
     *  shall consist of: all the elements that compare less than, all the elements that compare
     *  equal to, and all the elements that compare greater than the key object, in that order.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/bsearch.html
     *\param[in] key the key to search for
     *\param[in] base pointer to the sorted array to search in
     *\param[in] nmemb number of objects in the sorted array
     *\param[in] size size of one object in the sorted array in bytes
     *\param[in] compar pointer to the comparison function that provides the order
     *\return The bsearch function returns a pointer to a matching element of the array, or a null
     *        pointer if no match is found. If two elements compare as equal, which element is
     *        matched is unspeciﬁed. */
    void *bsearch(const void *key,
                  const void *base,
                  size_t nmemb,
                  size_t size,
                  int (*compar)(const void *, const void *));

    /** ISO/IEC 9899:1999 7.20.5.2
     *
     *  The qsort function sorts an array of nmemb objects, the initial element of which is
     *  pointed to by base. The size of each object is speciﬁed by size.
     *  The contents of the array are sorted into ascending order according to a comparison
     *  function pointed to by compar, which is called with two arguments that point to the
     *  objects being compared. The function shall return an integer less than, equal to, or
     *  greater than zero if the ﬁrst argument is considered to be respectively less than, equal to,
     *  or greater than the second.
     *  If two elements compare as equal, their order in the resulting sorted array is unspeciﬁed.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/qsort.html
     *\param[in,out] base pointer to the initial unsorted array
     *\param[in] nmemb number of objects in the array
     *\param[in] size size of one object in the array in bytes
     *\param[in] compar pointer to the comparison function that provides the order */
    void qsort(void *base,
               size_t nmemb,
               size_t size,
               int (*compar)(const void *, const void *));


    /*
     * ISO/IEC 9899:1999 7.20.6 Integer arithmetic functions
     */

    /** ISO/IEC 9899:1999 7.20.6.1
     *
     *  The abs function compute the absolute value of an integer j. If the
     *  result cannot be represented, the behavior is undeﬁned.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/abs.html
     *\param[in] j integer to compute absolute value for
     *\return the absolute value. */
    int abs(int j);

    /** ISO/IEC 9899:1999 7.20.6.1
     *
     *  The labs function compute the absolute value of an integer j. If the
     *  result cannot be represented, the behavior is undeﬁned.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/labs.html
     *\param[in] j integer to compute absolute value for
     *\return the absolute value. */
    long int labs(long int j);

    /** ISO/IEC 9899:1999 7.20.6.1
     *
     *  The llabs function compute the absolute value of an integer j. If the
     *  result cannot be represented, the behavior is undeﬁned.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/llabs.html
     *\param[in] j integer to compute absolute value for
     *\return the absolute value. */
    long long int llabs(long long int j);

    /** ISO/IEC 9899:1999 7.20.6.2
     *
     *  The div function computes numer / denom and numer % denom in a single operation.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/div.html
     *\param[in] numer numerator
     *\param[in] denom denominator
     *\return The div function returns a structure of type div_t, comprising both the
     *        quotient and the remainder. If either part of the result cannot be represented,
     *        the behavior is undeﬁned. */
    div_t div(int numer,
              int denom);

    /** ISO/IEC 9899:1999 7.20.6.2
     *
     *  The ldiv function computes numer / denom and numer % denom in a single operation.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/ldiv.html
     *\param[in] numer numerator
     *\param[in] denom denominator
     *\return The ldiv function returns a structure of type ldiv_t, comprising both the
     *        quotient and the remainder. If either part of the result cannot be represented,
     *        the behavior is undeﬁned. */
    ldiv_t ldiv(long int numer,
                long int denom);

    /** ISO/IEC 9899:1999 7.20.6.2
     *
     *  The lldiv function computes numer / denom and numer % denom in a single operation.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/ldiv.html
     *\param[in] numer numerator
     *\param[in] denom denominator
     *\return The lldiv function returns a structure of type lldiv_t, comprising both the
     *        quotient and the remainder. If either part of the result cannot be represented,
     *        the behavior is undeﬁned. */
    lldiv_t lldiv(long long int numer,
                  long long int denom);


    /*
     * ISO/IEC 9899:1999 7.20.7 Multibyte/wide character conversion functions
     */

    /** ISO/IEC 9899:1999 7.20.7.1
     *
     *  If s is not a null pointer, the mblen function determines the number of bytes contained
     *  in the multibyte character pointed to by s.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/mblen.html
     *\param[in] s the multibyte character string
     *\param[in] n maximum number of bytes to look at
     *\return If s is a null pointer, the mblen function returns a nonzero or zero value, if multibyte
     *        character encodings, respectively, do or do not have state-dependent encodings. If s is
     *        not a null pointer, the mblen function either returns 0 (if s points to the null character),
     *        or returns the number of bytes that are contained in the multibyte character (if the next n
     *        or fewer bytes form a valid multibyte character), or returns −1 (if they do not form a valid
     *        multibyte character) and [CX] may set errno to indicate the error (EILSEQ). */
    int mblen(const char *s,
              size_t n);

    /** ISO/IEC 9899:1999 7.20.7.2
     *
     *  If s is not a null pointer, the mbtowc function inspects at most n bytes beginning with
     *  the byte pointed to by s to determine the number of bytes needed to complete the next
     *  multibyte character (including any shift sequences). If the function determines that the
     *  next multibyte character is complete and valid, it determines the value of the
     *  corresponding wide character and then, if pwc is not a null pointer, stores that value in
     *  the object pointed to by pwc. If the corresponding wide character is the null wide
     *  character, the function is left in the initial conversion state.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/mbtowc.html
     *\param[in,out] pwc pointer to the output wide character
     *\param[in] s pointer to the multibyte character string
     *\param[in] n maximum number of bytes to look at
     *\return If s is a null pointer, the mbtowc function returns a nonzero or zero value, if multibyte
     *        character encodings, respectively, do or do not have state-dependent encodings. If s is
     *        not a null pointer, the mbtowc function either returns 0 (if s points to the null character),
     *        or returns the number of bytes that are contained in the converted multibyte character (if
     *        the next n or fewer bytes form a valid multibyte character), or returns −1 (if they do not
     *        form a valid multibyte character) and [CX] may set errno to indicate the error [-CX].
     *        In no case will the value returned be greater than n or the value of the MB_CUR_MAX
     *        macro.*/
    int mbtowc(wchar_t * _LIGHTLIBC_RESTRICT pwc,
               const char * _LIGHTLIBC_RESTRICT s,
               size_t n);

    /** ISO/IEC 9899:1999 7.20.7.3
     *
     *  The wctomb function determines the number of bytes needed to represent the multibyte
     *  character corresponding to the wide character given by wc (including any shift
     *  sequences), and stores the multibyte character representation in the array whose ﬁrst
     *  element is pointed to by s (if s is not a null pointer). At most MB_CUR_MAX characters
     *  are stored. If wc is a null wide character, a null byte is stored, preceded by any shift
     *  sequence needed to restore the initial shift state, and the function is left in the initial
     *  conversion state.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/wctomb.html
     *\param[in,out] s the resulting multibyte character
     *\param[in] wc the wide character to convert
     *\return If s is a null pointer, the wctomb function returns a nonzero or zero value, if multibyte
     *        character encodings, respectively, do or do not have state-dependent encodings. If s is
     *        not a null pointer, the wctomb function returns −1 if the value of wc does not correspond
     *        to a valid multibyte character, or returns the number of bytes that are contained in the
     *        multibyte character corresponding to the value of wc.
     *        In no case will the value returned be greater than the value of the MB_CUR_MAX macro. */
    int wctomb(char *s,
               wchar_t wc);


    /*
     * ISO/IEC 9899:1999 7.20.8 Multibyte/wide string conversion functions
     */

    /** ISO/IEC 9899:1999 7.20.8.1
     *
     *  The mbstowcs function converts a sequence of multibyte characters that begins in the
     *  initial shift state from the array pointed to by s into a sequence of corresponding wide
     *  characters and stores not more than n wide characters into the array pointed to by pwcs.
     *  No multibyte characters that follow a null character (which is converted into a null wide
     *  character) will be examined or converted. Each multibyte character is converted as if by
     *  a call to the mbtowc function, except that the conversion state of the mbtowc function is
     *  not affected.
     *  No more than n elements will be modiﬁed in the array pointed to by pwcs. If copying
     *  takes place between objects that overlap, the behavior is undeﬁned.
     *  [CX] If pwcs is a null pointer, mbstowcs() shall return the length required to convert
     *       the entire array regardless of the value of n, but no values are stored.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/mbstowcs.html
     *\param[in,out] pwcs pointer to the wide character string
     *\param[in] s pointer to the multibyte character string
     *\param[in] n maximum number of bytes to look at
     *\return If an invalid multibyte character is encountered, the mbstowcs function returns
     *        (size_t)(-1) and [CX] may set errno to indicate the error (EILSEQ) [-CX]. Otherwise,
     *        the mbstowcs function returns the number of array elements modiﬁed ([CX]
     *        or required if pwcs is null [-CX]) not including a terminating null wide
     *        character, if any. */
    size_t mbstowcs(wchar_t * _LIGHTLIBC_RESTRICT pwcs,
                    const char * _LIGHTLIBC_RESTRICT s,
                    size_t n);

    /** ISO/IEC 9899:1999 7.20.8.2
     *
     *  The wcstombs function converts a sequence of wide characters from the array pointed
     *  to by pwcs into a sequence of corresponding multibyte characters that begins in the
     *  initial shift state, and stores these multibyte characters into the array pointed to by s,
     *  stopping if a multibyte character would exceed the limit of n total bytes or if a null
     *  character is stored. Each wide character is converted as if by a call to the wctomb
     *  function, except that the conversion state of the wctomb function is not affected.
     *  No more than n bytes will be modiﬁed in the array pointed to by s. If copying takes place
     *  between objects that overlap, the behavior is undeﬁned.
     *  [CX] If s is a null pointer, wcstombs() shall return the length required to convert
     *       the entire array regardless of the value of n, but no values are stored.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/wcstombs.html
     *\param[in,out] s pointer to the multibyte character string
     *\param[in] pwcs pointer to the wide character string
     *\param[in] n maximum number of bytes to look at
     *\return If a wide character is encountered that does not correspond to a valid multibyte character,
     *        the wcstombs function returns (size_t)(-1). and [CX] may set errno to indicate the error
     *        (EILSEQ) [-CX]. Otherwise, the wcstombs function returns the number of bytes modiﬁed, not
     *        including a terminating null character, if any.*/
    size_t wcstombs(char * _LIGHTLIBC_RESTRICT s,
                    const wchar_t * _LIGHTLIBC_RESTRICT pwcs,
                    size_t n);

/*@}*/

    char *mktemp(char *templ);
    int mkstemp(char *templ);

    /* Environment variables */
    int setenv(const char *envname,
               const char *envval,
               int overwrite);
    int unsetenv(const char *name);

    /* Communication with the environment */
    void _exit(int status);

#ifdef __cplusplus
  }
#endif






#endif
