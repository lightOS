/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBUNIX_UNISTD_H
#define LIGHTOS_LIBUNIX_UNISTD_H

/*! \addtogroup libunix libunix */
/*@{*/

#include <stddef.h>
#include <sys/types.h>
#include <getopt.h>

// TODO
#include <stdio.h>

/* Version Test Macros */
#define _POSIX_VERSION                 200112L
#define _POSIX2_VERSION                200112L
#define _XOPEN_VERSION                 600


/* Constants for Options and Option Groups */
#define _POSIX_ADVISORY_INFO                     -1
#define _POSIX_ASYNCHRONOUS_IO                   -1
#define _POSIX_BARRIERS                          -1
/*TODO #define _POSIX_CHOWN_RESTRICTED*/
#define _POSIX_CLOCK_SELECTION                   -1
#define _POSIX_CPUTIME                           -1
#define _POSIX_FSYNC                             -1
#define _POSIX_IPV6                              -1
/*TODO #define _POSIX_JOB_CONTROL*/
#define _POSIX_MAPPED_FILES                      -1
#define _POSIX_MEMLOCK                           -1
#define _POSIX_MEMLOCK_RANGE                     -1
#define _POSIX_MEMORY_PROTECTION                 -1
#define _POSIX_MESSAGE_PASSING                   -1
#define _POSIX_MONOTONIC_CLOCK                   -1
/*_POSIX_NO_TRUNC NOTE: Not defined*/
#define _POSIX_PRIORITIZED_IO                    -1
#define _POSIX_PRIORITY_SCHEDULING               -1
#define _POSIX_RAW_SOCKETS                       -1
#define _POSIX_READER_WRITER_LOCKS               -1
#define _POSIX_REALTIME_SIGNALS                  -1
/*TODO #define _POSIX_REGEXP*/
#define _POSIX_SAVED_IDS                          1
#define _POSIX_SEMAPHORES                        -1
#define _POSIX_SHARED_MEMORY_OBJECTS             -1
#define _POSIX_SHELL                              1
#define _POSIX_SPAWN                             -1
#define _POSIX_SPIN_LOCKS                        -1
#define _POSIX_SPORADIC_SERVER                   -1
#define _POSIX_SYNCHRONIZED_IO                   -1
#define _POSIX_THREAD_ATTR_STACKADDR             -1
#define _POSIX_THREAD_ATTR_STACKSIZE             -1
#define _POSIX_THREAD_CPUTIME                    -1
#define _POSIX_THREAD_PRIO_INHERIT               -1
#define _POSIX_THREAD_PRIO_PROTECT               -1
#define _POSIX_THREAD_PRIORITY_SCHEDULING        -1
#define _POSIX_THREAD_PROCESS_SHARED             -1
#define _POSIX_THREAD_SAFE_FUNCTIONS             -1
#define _POSIX_THREAD_SPORADIC_SERVER            -1
#define _POSIX_THREADS                           -1
#define _POSIX_TIMEOUTS                          -1
#define _POSIX_TIMERS                            -1
#define _POSIX_TRACE                             -1
#define _POSIX_TRACE_EVENT_FILTER                -1
#define _POSIX_TRACE_INHERIT                     -1
#define _POSIX_TRACE_LOG                         -1
#define _POSIX_TYPED_MEMORY_OBJECTS              -1
/*TODO #define _POSIX_VDISABLE*/
#define _POSIX2_C_BIND                           200112L
#define _POSIX2_C_DEV                            -1
#define _POSIX2_CHAR_TERM                         1
#define _POSIX2_FORT_DEV                         -1
#define _POSIX2_FORT_RUN                         -1
#define _POSIX2_LOCALEDEF                        -1
#define _POSIX2_PBS                              -1
#define _POSIX2_PBS_ACCOUNTING                   -1
#define _POSIX2_PBS_CHECKPOINT                   -1
#define _POSIX2_PBS_LOCATE                       -1
#define _POSIX2_PBS_MESSAGE                      -1
#define _POSIX2_PBS_TRACK                        -1
#define _POSIX2_SW_DEV                           -1
#define _POSIX2_UPE                              -1
/* TODO
_POSIX_V6_ILP32_OFF32
    The implementation provides a C-language compilation environment with 32-bit int, long, pointer, and off_t types.
_POSIX_V6_ILP32_OFFBIG
    The implementation provides a C-language compilation environment with 32-bit int, long, and pointer types and an off_t type using at least 64 bits.
_POSIX_V6_LP64_OFF64
    The implementation provides a C-language compilation environment with 32-bit int and 64-bit long, pointer, and off_t types.
_POSIX_V6_LPBIG_OFFBIG
    The implementation provides a C-language compilation environment with an int type using at least 32 bits and long, pointer, and off_t types using at least 64 bits.
_XBS5_ILP32_OFF32 (LEGACY)
    [XSI] [Option Start]
    The implementation provides a C-language compilation environment with 32-bit int, long, pointer, and off_t types. [Option End]
_XBS5_ILP32_OFFBIG (LEGACY)
    [XSI] [Option Start]
    The implementation provides a C-language compilation environment with 32-bit int, long, and pointer types and an off_t type using at least 64 bits. [Option End]
_XBS5_LP64_OFF64 (LEGACY)
    [XSI] [Option Start]
    The implementation provides a C-language compilation environment with 32-bit int and 64-bit long, pointer, and off_t types. [Option End]
_XBS5_LPBIG_OFFBIG (LEGACY)
    [XSI] [Option Start]
    The implementation provides a C-language compilation environment with an int type using at least 32 bits and long, pointer, and off_t types using at least 64 bits. [Option End]
*/
/* TODO
_XOPEN_CRYPT
_XOPEN_ENH_I18N
_XOPEN_LEGACY
_XOPEN_REALTIME
_XOPEN_REALTIME_THREADS
_XOPEN_SHM
_XOPEN_STREAMS
_XOPEN_UNIX
*/

/* Execution-Time Symbolic Constants */
#define _POSIX_ASYNC_IO                          -1
#define _POSIX_PRIO_IO                           -1
#define _POSIX_SYNC_IO                           -1

/* Constants for Functions */

/* for access() */

/*! Test for read permission */
#define R_OK                      1
/*! Test for write permission */
#define W_OK                      2
/*! Test for execute permission */
#define X_OK                      4
/*! Test for file existance */
#define F_OK                      8

/* TODO The following symbolic constants shall be defined for the confstr() function: */

/*! File number of stdin; 0. */
#define STDIN_FILENO                  0
/*! File number of stdout; 1. */
#define STDOUT_FILENO                 1
/*! File number of stderr; 2. */
#define STDERR_FILENO                 2

/* Constants for pathconf and fpathconf */
#define _PC_FILESIZEBITS              1
#define _PC_LINK_MAX                  2
#define _PC_MAX_CANON                 3
#define _PC_MAX_INPUT                 4
#define _PC_NAME_MAX                  5
#define _PC_PATH_MAX                  6
#define _PC_PIPE_BUF                  7
#define _PC_CHOWN_RESTRICTED          8
#define _PC_NO_TRUNC                  9
#define _PC_VDISABLE                  10
#define _PC_ASYNC_IO                  11
#define _PC_PRIO_IO                   12
#define _PC_SYNC_IO                   13

/** The list of environment variables
 *\note declared here but defined in libc/env.c */
extern char **environ;

#ifdef __cplusplus
  extern "C"
  {
#endif

  /* Working directory */
  char *getcwd(char *buf, size_t size);
  int chdir(const char *path);
  int fchdir(int fd);
  
  /* Process management */
  pid_t getpid();
  
  /* TTY */
  int isatty(int fd);
  
  /* Temporary files mangement */
  char *mktemp(char *templ);
  int mkstemps(char *templ, int suffixlen);
  int mkstemp(char *templ);
  
  int access(const char *, int);
  
  int chown(const char *, uid_t, gid_t);
  
  int close(int);
  
  int link(const char *, const char *);
  
  ssize_t read(int, void *, size_t);
  
  int rmdir(const char *);
  
  int unlink(const char *);
  
  ssize_t write(int, const void *, size_t);
  

  // TODO:
  uid_t geteuid(void);
  unsigned alarm(unsigned seconds);
  int execve(const char *path, char *const argv[], char *const envp[]);
  int pipe(int fildes[2]);
  char *ttyname(int fildes);
  off_t lseek(int fildes, off_t offset, int whence);
  pid_t fork(void);
  unsigned sleep(unsigned seconds);
  int dup(int fildes);
  uid_t getuid(void);
  gid_t getgid(void);
  gid_t getegid(void);
  int setuid(uid_t uid);
  int setgid(gid_t gid);

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
