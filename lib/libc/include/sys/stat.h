/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBUNIX_SYS_STAT_H
#define LIGHTOS_LIBUNIX_SYS_STAT_H

/*! \addtogroup libunix libunix */
/*@{*/

#include <sys/types.h>

struct stat
{
  //! ID of device containing file
  dev_t   st_dev;
  //! file serial number
  ino_t   st_ino;
  //! mode of file
  mode_t    st_mode;
  //! number of links to the file
  nlink_t   st_nlink;
  //! user ID of file
  uid_t   st_uid;
  //! group ID of file
  gid_t   st_gid;
  //! device ID (if file is character or block special)
  dev_t   st_rdev;
  //! file size in bytes (if file is a regular file)
  off_t   st_size;
  //! time of last access
  time_t    st_atime;
  //! time of last data modification
  time_t    st_mtime;
  //! time of last status change
  time_t    st_ctime;
  //! a filesystem-specific preferred I/O block size for this object
  blksize_t st_blksize;
  //! number of blocks allocated for this object
  blkcnt_t  st_blocks;
};

struct stat64
{
  dev_t st_dev;
  ino64_t st_ino;
  mode_t st_mode;
  nlink_t st_nlink;
  uid_t st_uid;
  gid_t st_gid;
  dev_t st_rdev;
  off64_t st_size;
  time_t st_atime;
  time_t st_mtime;
  time_t st_ctime;
  blksize_t st_blksize;
  blkcnt64_t st_blocks;
  mode_t st_attr;
};

/*! File type */
#define S_IFMT                  0x000007
/*! block special */
#define S_IFBLK                 0x000001
/*! character special */
#define S_IFCHR                 0x000002
/*! FIFO special */
#define S_IFIFO                 0x000003
/*! regular */
#define S_IFREG                 0x000004
/*! directory */
#define S_IFDIR                 0x000005
/*! symbolic link */
#define S_IFLNK                 0x000006

/*! read permission, owner */
#define S_IRUSR                 0x000008
/*! write permission, owner */
#define S_IWUSR                 0x000010
/*! execute/search permission, owner */
#define S_IXUSR                 0x000020
/*! read, write, execute/search by owner */
#define S_IRWXU                 (S_IRUSR | S_IWUSR | S_IXUSR)
/*! read permission, group */
#define S_IRGRP                 0x000040
/*! write permission, group */
#define S_IWGRP                 0x000080
/*! execute/search permission, group */
#define S_IXGRP                 0x000100
/*! read, write, execute/search by group */
#define S_IRWXG                 (S_IRGRP | S_IWGRP | S_IXGRP)
/*! read permission, others */
#define S_IROTH                 0x000200
/*! write permission, others */
#define S_IWOTH                 0x000400
/*! execute/search permission, others */
#define S_IXOTH                 0x000800
/*! read, write, execute/search by others */
#define S_IRWXO                 (S_IROTH | S_IWOTH | S_IXOTH)

/*! set-user-ID on execution */
#define S_ISUID                 0x001000
/*! set-group-ID on execution */
#define S_ISGID                 0x002000
/*! on directories, restricted deletion flag */
#define S_ISVTX                 0x004000

/*! Test for a block special file */
#define S_ISBLK(m) ((m & S_IFMT) == S_IFBLK)
/*! Test for character special file */
#define S_ISCHR(m) ((m & S_IFMT) == S_IFCHR)
/*! Test for a directory */
#define S_ISDIR(m) ((m & S_IFMT) == S_IFDIR)
/*! Test for a pipe or a FIFO special file */
#define S_ISFIFO(m) ((m & S_IFMT) == S_IFIFO)
/*! Test for a regular file */
#define S_ISREG(m) ((m & S_IFMT) == S_IFREG)
/*! Test for a symbolic link */
#define S_ISLNK(m) ((m & S_IFMT) == S_IFLNK)

#ifdef __cplusplus
  extern "C"
  {
#endif

  int chmod(const char *, mode_t);
  int stat( const char *path,
        struct stat *buffer);
  int lstat(  const char *path,
        struct stat *buf);
  int fstat(  int fildes,
        struct stat *buf);
  int mkdir(const char *, mode_t);
  mode_t umask(mode_t);

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
