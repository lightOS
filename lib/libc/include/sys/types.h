/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBUNIX_SYS_TYPES_H
#define LIGHTOS_LIBUNIX_SYS_TYPES_H

/*! \addtogroup libunix libunix */
/*@{*/

#include <time.h>
#include <libc/types.h>

//! Used for file block counts
typedef _LIBUNIX_blkcnt_t blkcnt_t;
typedef _LIBUNIX_blkcnt64_t blkcnt64_t;
//! Used for block sizes
typedef _LIBUNIX_blksize_t blksize_t;

/*
TODO clock_t Used for system times in clock ticks or CLOCKS_PER_SEC (see <time.h>).
TODO clockid_t Used for clock ID type in the clock and timer functions.
*/

/*
TODO fsblkcnt_t Used for file system block counts
TODO fsfilcnt_t Used for file system file counts
*/

//! Used as a general identifier
typedef _LIBUNIX_id_t id_t;
//! Used for user IDs
typedef _LIBUNIX_uid_t uid_t;
//! Used for group IDs
typedef _LIBUNIX_gid_t gid_t;
//! Used for file serial numbers
#ifndef _LIBUNIX_DEFINED_ino_t
  #define _LIBUNIX_DEFINED_ino_t
  typedef _LIBUNIX_ino_t ino_t;
#endif
typedef _LIBUNIX_ino64_t ino64_t;
//! Used for device IDs
typedef _LIBUNIX_dev_t dev_t;
//! Used for process IDs and process group IDs
typedef _LIBUNIX_pid_t pid_t;

/*
TODO key_t Used for interprocess communication.
*/

//! Used for some file attributes
typedef _LIBUNIX_mode_t mode_t;
//! Used for link counts
typedef _LIBUNIX_nlink_t nlink_t;
//! Used for file sizes
typedef _LIBUNIX_off_t off_t;
typedef _LIBUNIX_off64_t off64_t;

/*
TODO pthread_attr_t Used to identify a thread attribute object.
TODO pthread_barrier_t [BAR] [Option Start] Used to identify a barrier. [Option End]
TODO pthread_barrierattr_t [BAR] [Option Start] Used to define a barrier attributes object. [Option End]
TODO pthread_cond_t Used for condition variables.
TODO pthread_condattr_t Used to identify a condition attribute object.
TODO pthread_key_t Used for thread-specific data keys.
TODO pthread_mutex_t Used for mutexes.
TODO pthread_mutexattr_t Used to identify a mutex attribute object.
TODO pthread_once_t Used for dynamic package initialisation.
TODO pthread_rwlock_t Used for read-write locks.
TODO pthread_rwlockattr_t Used for read-write lock attributes.
TODO pthread_spinlock_t [SPI] [Option Start] Used to identify a spin lock. [Option End]
TODO pthread_t Used to identify a thread.
*/

typedef _LIBUNIX_ssize_t ssize_t;
// NOTE: size_t from stddef.h

//! Used for time in microseconds
typedef _LIBUNIX_suseconds_t suseconds_t;
//! Used for time in microseconds
typedef _LIBUNIX_useconds_t useconds_t;

// NOTE: time_t from time.h

/*
TODO timer_t Used for timer ID returned by timer_create().
trace_attr_t
    [TRC] [Option Start] Used to identify a trace stream attributes object. [Option End]
trace_event_id_t
    [TRC] [Option Start] Used to identify a trace event type. [Option End]
trace_event_set_t
    [TRC TEF] [Option Start] Used to identify a trace event type set. [Option End]
trace_id_t
    [TRC] [Option Start] Used to identify a trace stream. [Option End]
*/

/*@}*/

#endif
