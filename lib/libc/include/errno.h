/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef _LIGHTLIBC_ERRNO_H
#define _LIGHTLIBC_ERRNO_H



/** \addtogroup libc_errno */
/*@{*/

/*
 * Error definitions from the ISO/IEC 9899:1999 standard
 * ISO/IEC 9899:1999 - 7.5
 */

/** Mathematics argument out of domain of function. */
#define EDOM                           1
/** Result too large. */
#define ERANGE                         2
/** Illegal byte sequence. */
#define EILSEQ                         3

/*
 * Error definitions from POSIX
 * http://www.opengroup.org/onlinepubs/000095399/basedefs/errno.h.html
 */

/** Argument list too long */
#define E2BIG                          4
/** Permission denied. */
#define EACCES                         5
/** Address in use. */
#define EADDRINUSE                     6
/** Address not available. */
#define EADDRNOTAVAIL                  7
/** Address family not supported. */
#define EAFNOSUPPORT                   8
/** Resource unavailable, try again (may be the same value as [EWOULDBLOCK]). */
#define EAGAIN                         9
/** Connection already in progress. */
#define EALREADY                       10
/** Bad file descriptor. */
#define EBADF                          11
/** Bad message. */
#define EBADMSG                        12
/** Device or resource busy. */
#define EBUSY                          13
/** Operation canceled. */
#define ECANCELED                      14
/** No child processes. */
#define ECHILD                         15
/** Connection aborted. */
#define ECONNABORTED                   16
/** Connection refused. */
#define ECONNREFUSED                   17
/** Connection reset. */
#define ECONNRESET                     18
/** Resource deadlock would occur. */
#define EDEADLK                        19
/** Destination address required. */
#define EDESTADDRREQ                   20
/** Reserved. */
#define EDQUOT                         21
/** File exists. */
#define EEXIST                         22
/** Bad address. */
#define EFAULT                         23
/** File too large. */
#define EFBIG                          24
/** Host is unreachable. */
#define EHOSTUNREACH                   25
/** Identifier removed. */
#define EIDRM                          26
/** Operation in progress. */
#define EINPROGRESS                    27
/** Interrupted function. */
#define EINTR                          28
/** Invalid argument. */
#define EINVAL                         29
/** I/O error. */
#define EIO                            30
/** Socket is connected. */
#define EISCONN                        31
/** Is a directory. */
#define EISDIR                         32
/** Too many levels of symbolic links. */
#define ELOOP                          33
/** Too many open files. */
#define EMFILE                         34
/** Too many links. */
#define EMLINK                         35
/** Message too large. */
#define EMSGSIZE                       36
/** Reserved. */
#define EMULTIHOP                      37
/** Filename too long. */
#define ENAMETOOLONG                   38
/** Network is down. */
#define ENETDOWN                       39
/** Connection aborted by network. */
#define ENETRESET                      40
/** Network unreachable. */
#define ENETUNREACH                    41
/** Too many files open in system. */
#define ENFILE                         42
/** No buffer space available. */
#define ENOBUFS                        43
/** No message is available on the STREAM head read queue. */
#define ENODATA                        44
/** No such device. */
#define ENODEV                         45
/** No such file or directory. */
#define ENOENT                         46
/** Executable file format error. */
#define ENOEXEC                        47
/** No locks available. */
#define ENOLCK                         48
/** Reserved. */
#define ENOLINK                        49
/** Not enough space. */
#define ENOMEM                         50
/** No message of the desired type. */
#define ENOMSG                         51
/** Protocol not available. */
#define ENOPROTOOPT                    52
/** No space left on device. */
#define ENOSPC                         53
/** No STREAM resources. */
#define ENOSR                          54
/** Not a STREAM. */
#define ENOSTR                         55
/** Function not supported. */
#define ENOSYS                         56
/** The socket is not connected. */
#define ENOTCONN                       57
/** Not a directory. */
#define ENOTDIR                        58
/** Directory not empty. */
#define ENOTEMPTY                      59
/** Not a socket. */
#define ENOTSOCK                       60
/** Not supported. */
#define ENOTSUP                        61
/** Inappropriate I/O control operation. */
#define ENOTTY                         62
/** No such device or address. */
#define ENXIO                          63
/** Operation not supported on socket. */
#define EOPNOTSUPP                     64
/** Value too large to be stored in data type. */
#define EOVERFLOW                      65
/** Operation not permitted. */
#define EPERM                          66
/** Broken pipe. */
#define EPIPE                          67
/** Protocol error. */
#define EPROTO                         68
/** Protocol not supported. */
#define EPROTONOSUPPORT                69
/** Protocol wrong type for socket. */
#define EPROTOTYPE                     70
/** Read-only file system. */
#define EROFS                          71
/** Invalid seek. */
#define ESPIPE                         72
/** No such process. */
#define ESRCH                          73
/** Reserved. */
#define ESTALE                         74
/** Stream ioctl() timeout. */
#define ETIME                          75
/** Connection timed out. */
#define ETIMEDOUT                      76
/** Text file busy. */
#define ETXTBSY                        77
/** Operation would block (may be the same value as [EAGAIN]). */
#define EWOULDBLOCK                    78
/** Cross-device link.  */
#define EXDEV                          79

#define _LIBC_ERROR_MAX                79

/*
 * errno
 */

#ifndef _LIGHTLIBC_MULTITHREADING
  /** Expands to a modiﬁable lvalue that has type int, the value of which is set to a
   *  positive error number by several library functions. It is unspeciﬁed whether errno is a
   *  macro or an identiﬁer declared with external linkage. */
  extern int errno;
#else
  #define errno (*_LIBC_errno())
#endif

/*@}*/




/** \addtogroup libc_internal */
/*@{*/

/*
 * Support functions
 */

#ifdef __cplusplus
  extern "C"
  {
#endif

  /** Support function for the errno macro.
   *\return Returns a pointer to the thread-local errno variable*/
  int *_LIBC_errno(void);

#ifdef __cplusplus
  }
#endif

/*@}*/



#endif
