/*
lightOS libc
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef _LIGHTLIBC_LOCALE_H
#define _LIGHTLIBC_LOCALE_H



/*! \addtogroup libc_locale */
/*@{*/

#define LC_COLLATE                    0x01
#define LC_CTYPE                    0x02
#define LC_MONETARY                   0x04
#define LC_NUMERIC                    0x08
#define LC_TIME                     0x10
#define LC_ALL                      (LC_COLLATE | LC_CTYPE | LC_MONETARY | LC_NUMERIC | LC_TIME)

struct lconv
{
  char *currency_symbol;
  char *decimal_point;
  char frac_digits;
  char *grouping;
  char *int_curr_symbol;
  char int_frac_digits;
  char int_n_cs_precedes;
  char int_n_sep_by_space;
  char int_n_sign_posn;
  char int_p_cs_precedes;
  char int_p_sep_by_space;
  char int_p_sign_posn;
  char *mon_decimal_point;
  char *mon_grouping;
  char *mon_thousands_sep;
  char *negative_sign;
  char n_cs_precedes;
  char n_sep_by_space;
  char n_sign_posn;
  char *positive_sign;
  char  p_cs_precedes;
  char p_sep_by_space;
  char p_sign_posn;
  char *thousands_sep;
};

#ifdef __cplusplus
  extern "C"
  {
#endif

  // TODO localeconv
  struct lconv *localeconv();
  // TODO setlocale
  char *setlocale(int, const char *);

#ifdef __cplusplus
  }
#endif

/*@}*/



#endif
