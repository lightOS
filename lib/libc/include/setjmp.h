/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef _LIGHTLIBC_SETJMP_H
#define _LIGHTLIBC_SETJMP_H



#include <libarch/setjmp.h>



/*! \addtogroup libc_setjmp */
/*@{*/

/** ISO/IEC 9899:1999 7.13
 *
 *  An array type suitable for holding the information needed to restore a calling
 *  environment. The environment of a call to the setjmp macro consists of information
 *  sufﬁcient for a call to the longjmp function to return execution to the correct block and
 *  invocation of that block, were it called recursively. It does not include the state of the
 *  ﬂoating-point status ﬂags, of open ﬁles, or of any other component of the abstract
 *  machine.
 *
 *  [CX] http://www.opengroup.org/onlinepubs/009695399/basedefs/setjmp.h.html */
typedef _LIBARCH_jmp_buf jmp_buf;

#ifdef __cplusplus
  extern "C"
  {
#endif

    /** ISO/IEC 9899:1999 7.13.1.1
     *
     *  The setjmp macro saves its calling environment in its jmp_buf argument for later use
     *  by the longjmp function.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/009695399/functions/setjmp.html
     *\param[in,out] env jmp_buf
     *\return If the return is from a direct invocation, the setjmp macro returns the value zero. If the
     *        return is from a call to the longjmp function, the setjmp macro returns a nonzero value. */
    #define setjmp(env)       _LIBARCH_setjmp(&env)

    /** ISO/IEC 9899:1999 7.13.2.1
     *
     *  The longjmp function restores the environment saved by the most recent invocation of
     *  the setjmp macro in the same invocation of the program with the corresponding
     *  jmp_buf argument. If there has been no such invocation, or if the function containing
     *  the invocation of the setjmp macro has terminated execution208) in the interim, or if the
     *  invocation of the setjmp macro was within the scope of an identiﬁer with variably
     *  modiﬁed type and execution has left that scope in the interim, the behavior is undeﬁned.
     *  All accessible objects have values, and all other components of the abstract machine209)
     *  have state, as of the time the longjmp function was called, except that the values of
     *  objects of automatic storage duration that are local to the function containing the
     *  invocation of the corresponding setjmp macro that do not have volatile-qualiﬁed type
     *  and have been changed between the setjmp invocation and longjmp call are
     *  indeterminate.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/009695399/functions/longjmp.html
     *\param[in] env jmp_buf
     *\param[in] val the return value */
    #define longjmp(env, val) _LIBARCH_longjmp(&env, val)

#ifdef __cplusplus
  }
#endif

/*@}*/



#endif
