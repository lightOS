/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef _LIGHTLIBC_GETOPT_H
#define _LIGHTLIBC_GETOPT_H



/*! \addtogroup libc_posix_dirent */
/*@{*/

// TODO: Rework && Make standard compliant

#define NO_ARG                         0
#define REQUIRED_ARG                   1
#define OPTIONAL_ARG                   2

struct option
{
  char *name;
  int has_arg;
  int *flag;
  int val;
};

extern char *optarg;
extern int optind;
extern int opterr;
extern int optopt;

#ifdef __cplusplus
  extern "C"
  {
#endif

  int getopt( int argc,
        char *const argv[],
        const char *optstring);
  int getopt_long ( int argc,
            char *const argv[],
            const char *shortopts,
            const struct option *longopts,
            int *longind);
  int getopt_long_only( int argc,
              char *const argv[],
              const char *shortopts,
              const struct option *longopts,
              int *longind);

#ifdef __cplusplus
  }
#endif

/*@}*/



#endif
