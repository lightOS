/*
lightOS libc
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBC_WCHAR_H
#define LIGHTOS_LIBC_WCHAR_H



/*
 * Standard includes
 */
#include <stddef.h>
#include <stdint.h>



/** \addtogroup libc_wchar */
/*@{*/


/*
 * Types
 * ISO/IEC 9899:1999 7.24.1
 */
#define WEOF                           (wint_t)-1


/*
 * Types
 * ISO/IEC 9899:1999 7.24.1
 */

// TODO: mbstate_t
// TODO: wint_t

/** Forward declaration of struct tm, see time.h */
struct tm;

#ifdef __cplusplus
  extern "C"
  {
#endif


  /*
   * ISO/IEC 9899:1999 7.24.2 Formatted wide character input/output functions
   */
  // TODO

#ifdef __cplusplus
  }
#endif

/*@}*/



#endif
