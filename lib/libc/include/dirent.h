/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef _LIGHTLIBC_DIRENT_H
#define _LIGHTLIBC_DIRENT_H



#include <lightlibc/compiler.h>

#include <stdio.h>
#include <libc/types.h>
#include <libOS/LIBC_glue.h>



/*! \addtogroup libc_posix_dirent */
/*@{*/

// TODO: Rework && Make standard compliant

/* Define ino_t */
#ifndef _LIBUNIX_DEFINED_ino_t
  #define _LIBUNIX_DEFINED_ino_t
  typedef _LIBUNIX_ino_t ino_t;
#endif

/*! Define dirent structure */
struct dirent
{
  /*! File serial number */
  ino_t d_ino;
  /*! Name of entry */
  // TODO: No Prefix
  char d_name[_LIBOS_FILENAME_MAX + 1]; // TODO: NAME_MAX?
};

/*! Helper structure to define DIR */
struct _LIBUNIX_DIR;

/*! Typedef DIR */
typedef struct _LIBUNIX_DIR DIR;



#ifdef __cplusplus
  extern "C"
  {
#endif

  int closedir(DIR *dir);
  DIR *opendir(const char *name);
  struct dirent *readdir(DIR *dir);
  // TODO readdir_r(DIR *_LIGHTLIBC_RESTRICT, struct dirent *_LIGHTLIBC_RESTRICT,
  //                struct dirent **_LIGHTLIBC_RESTRICT);
  void rewinddir(DIR *dir);
  void seekdir(DIR *dir, long off);
  long telldir(DIR *dir);

#ifdef __cplusplus
  }
#endif

/*@}*/



#endif
