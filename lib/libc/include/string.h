/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef _LIGHTLIBC_STRING_H
#define _LIGHTLIBC_STRING_H


#include <lightlibc/compiler.h>

/*
 * Standard includes
 */
#include <stddef.h>



#ifdef __cplusplus
  extern "C"
  {
#endif

    /** \addtogroup libc_string */
    /*@{*/

    /*
     * ISO/IEC 9899:1999 7.21.2 Copying functions
     */

    /** ISO/IEC 9899:1999 7.21.2.1
     *
     *  The memcpy function copies n characters from the object pointed to by s2 into the
     *  object pointed to by s1. If copying takes place between objects that overlap, the behavior
     *  is undeﬁned.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/memcpy.html
     *\param[in] s1 the destination
     *\param[in] s2 the source
     *\param[in] n the number of bytes to copy
     *\return The memcpy function returns the value of s1. */
    void *memcpy(void * _LIGHTLIBC_RESTRICT s1,
                 const void * _LIGHTLIBC_RESTRICT s2,
                 size_t n);

    /** ISO/IEC 9899:1999 7.21.2.2
     *
     *  The memmove function copies n characters from the object pointed to by s2 into the
     *  object pointed to by s1. Copying takes place as if the n characters from the object
     *  pointed to by s2 are ﬁrst copied into a temporary array of n characters that does not
     *  overlap the objects pointed to by s1 and s2, and then the n characters from the
     *  temporary array are copied into the object pointed to by s1.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/memmove.html
     *\param[in] s1 the destination
     *\param[in] s2 the source
     *\param[in] n the number of bytes to copy
     *\return The memmove function returns the value of s1. */
    void *memmove(void *s1,
                  const void *s2,
                  size_t n);

    /** ISO/IEC 9899:1999 7.21.2.3
     *
     *  The strcpy function copies the string pointed to by s2 (including the terminating null
     *  character) into the array pointed to by s1. If copying takes place between objects that
     *  overlap, the behavior is undeﬁned.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strcpy.html
     *\param[in] s1 the destination
     *\param[in] s2 the source
     *\return The strcpy function returns the value of s1. */
    char *strcpy(char * _LIGHTLIBC_RESTRICT s1,
                 const char * _LIGHTLIBC_RESTRICT s2);

    /** ISO/IEC 9899:1999 7.21.2.4
     *
     *  The strncpy function copies not more than n characters (characters that follow a null
     *  character are not copied) from the array pointed to by s2 to the array pointed to by
     *  s1. If copying takes place between objects that overlap, the behavior is undeﬁned.
     *  If the array pointed to by s2 is a string that is shorter than n characters, null characters
     *  are appended to the copy in the array pointed to by s1, until n characters in all have been
     *  written.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strncpy.html
     *\param[in] s1 the destination
     *\param[in] s2 the source
     *\param[in] n  maximum number of characters (including null-termination) to copy
     *\return The strncpy function returns the value of s1. */
    char *strncpy(char * _LIGHTLIBC_RESTRICT s1,
                  const char * _LIGHTLIBC_RESTRICT s2,
                  size_t n);


    /*
     * ISO/IEC 9899:1999 7.21.3 Concatenation functions
     */

    /** ISO/IEC 9899:1999 7.21.3.1
     *
     *  The strcat function appends a copy of the string pointed to by s2 (including the
     *  terminating null character) to the end of the string pointed to by s1. The initial character
     *  of s2 overwrites the null character at the end of s1. If copying takes place between
     *  objects that overlap, the behavior is undeﬁned.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strcat.html
     *\param[in] s1 the string that s2 is appended to
     *\param[in] s2 the string to append
     *\return The strcat function returns the value of s1. */
     char *strcat(char * _LIGHTLIBC_RESTRICT s1,
                  const char * _LIGHTLIBC_RESTRICT s2);

    /** ISO/IEC 9899:1999 7.21.3.2
     *
     *  The strncat function appends not more than n characters (a null character and
     *  characters that follow it are not appended) from the array pointed to by s2 to the end of
     *  the string pointed to by s1. The initial character of s2 overwrites the null character at the
     *  end of s1. A terminating null character is always appended to the result. If copying
     *  takes place between objects that overlap, the behavior is undeﬁned.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strncat.html
     *\param[in] s1 the string that s2 us appended to
     *\param[in] s2 the string to append
     *\param[in] n maximum number of bytes to append
     *\return The strncat function returns the value of s1. */
    char *strncat(char * _LIGHTLIBC_RESTRICT s1,
                  const char * _LIGHTLIBC_RESTRICT s2,
                  size_t n);


    /*
     * ISO/IEC 9899:1999 7.21.4 Comparison functions
     */

    /** ISO/IEC 9899:1999 7.21.4.1
     *
     *  The memcmp function compares the ﬁrst n characters of the object pointed to by s1 to
     *  the ﬁrst n characters of the object pointed to by s2.262)
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/memcmp.html
     *\param[in] s1 first memory region
     *\param[in] s2 second memory region
     *\param[in] n the number of bytes to compare
     *\return The memcmp function returns an integer greater than, equal to, or less than zero,
     *        accordingly as the object pointed to by s1 is greater than, equal to, or less than the object
     *        pointed to by s2. */
    int memcmp(const void *s1,
               const void *s2,
               size_t n);

    /** ISO/IEC 9899:1999 7.21.4.2
     *
     *  The strcmp function compares the string pointed to by s1 to the string pointed to by
     *  s2.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strcmp.html
     *\param[in] s1 first string
     *\param[in] s2 second string
     *\return The strcmp function returns an integer greater than, equal to, or less than zero,
     *        accordingly as the string pointed to by s1 is greater than, equal to, or less than the string
     *        pointed to by s2. */
    int strcmp(const char *s1,
               const char *s2);

    /** ISO/IEC 9899:1999 7.21.4.3
     *
     *  The strcoll function compares the string pointed to by s1 to the string pointed to by
     *  s2, both interpreted as appropriate to the LC_COLLATE category of the current locale.
     *  [CX] The strcoll() function shall not change the setting of errno if successful.
     *       Since no return value is reserved to indicate an error, an application wishing to
     *       check for error situations should set errno to 0, then call strcoll(), then check errno.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strcoll.html
     *\param[in] s1
     *\param[in] s2
     *\todo implement
     *\return The strcoll function returns an integer greater than, equal to, or less than zero,
     *        accordingly as the string pointed to by s1 is greater than, equal to, or less than the string
     *        pointed to by s2 when both are interpreted as appropriate to the current locale.
     *        [CX] On error, strcoll() may set errno, but no return value is reserved to indicate an error. */
    int strcoll(const char *s1,
                const char *s2);

    /** ISO/IEC 9899:1999 7.21.4.4
     *
     *  The strncmp function compares not more than n characters (characters that follow a
     *  null character are not compared) from the array pointed to by s1 to the array pointed to
     *  by s2.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strncmp.html
     *\param[in] s1 first string
     *\param[in] s2 second string
     *\param[in] n maximum number of bytes to compare
     *\return The strncmp function returns an integer greater than, equal to, or less than zero,
     *        accordingly as the possibly null-terminated array pointed to by s1 is greater than, equal
     *        to, or less than the possibly null-terminated array pointed to by s2. */
    int strncmp(const char *s1,
                const char *s2,
                size_t n);

    /** ISO/IEC 9899:1999 7.21.4.5
     *
     *  The strxfrm function transforms the string pointed to by s2 and places the resulting
     *  string into the array pointed to by s1. The transformation is such that if the strcmp
     *  function is applied to two transformed strings, it returns a value greater than, equal to, or
     *  less than zero, corresponding to the result of the strcoll function applied to the same
     *  two original strings. No more than n characters are placed into the resulting array
     *  pointed to by s1, including the terminating null character. If n is zero, s1 is permitted to
     *  be a null pointer. If copying takes place between objects that overlap, the behavior is
     *  undeﬁned.
     *  [CX] The strxfrm() function shall not change the setting of errno if successful.
     *       Since no return value is reserved to indicate an error, an application wishing to check
     *       for error situations should set errno to 0, then call strxfrm(), then check errno.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strxfrm.html
     *\param[in] s1
     *\param[in] s2
     *\param[in] n
     *\todo implement
     *\return The strxfrm function returns the length of the transformed string (not including the
     *        terminating null character). If the value returned is n or more, the contents of the array
     *        pointed to by s1 are indeterminate.
     *        [CX]  On error, strxfrm() may set errno but no return value is reserved to indicate an error. */
    size_t strxfrm(char *s1,
                   const char *s2,
                   size_t n);


    /*
     * ISO/IEC 9899:1999 7.21.5 Search functions
     */

    /** ISO/IEC 9899:1999 7.21.5.1
     *
     *  The memchr function locates the ﬁrst occurrence of c (converted to an unsigned
     *  char) in the initial n characters (each interpreted as unsigned char) of the object
     *  pointed to by s.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/memchr.html
     *\param[in] s the memory location
     *\param[in] c the character to look for in s
     *\param[in] n the size of the memory location s in bytes
     *\return The memchr function returns a pointer to the located character, or a null pointer if the
     *        character does not occur in the object. */
    void *memchr(const void * s,
                 int c,
                 size_t n);

    /** ISO/IEC 9899:1999 7.21.5.2
     *
     *  The strchr function locates the ﬁrst occurrence of c (converted to a char) in the
     *  string pointed to by s. The terminating null character is considered to be part of the
     *  string.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strchr.html
     *\param[in] s the string
     *\param[in] c the character to look for in s
     *\return The strchr function returns a pointer to the located character, or a null pointer if the
     *        character does not occur in the string. */
    char *strchr(const char *s,
                 int c);

    /** ISO/IEC 9899:1999 7.21.5.3
     *
     *  The strcspn function computes the length of the maximum initial segment of the string
     *  pointed to by s1 which consists entirely of characters not from the string pointed to by
     *  s2.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strcspn.html
     *\param[in] s1 the first string
     *\param[in] s2 the second string
     *\return The strcspn function returns the length of the segment. */
    size_t strcspn(const char *s1, const char *s2);

    /** ISO/IEC 9899:1999 7.21.5.4
     *
     *  The strpbrk function locates the ﬁrst occurrence in the string pointed to by s1 of any
     *  character from the string pointed to by s2.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strpbrk.html
     *\param[in] s1 the first string
     *\param[in] s2 the second string
     *\return The strpbrk function returns a pointer to the character, or a null pointer if no character
     *        from s2 occurs in s1. */
    char *strpbrk(const char *s1,
                  const char *s2);

    /** ISO/IEC 9899:1999 7.21.5.5
     *
     *  The strrchr function locates the last occurrence of c (converted to a char) in the
     *  string pointed to by s. The terminating null character is considered to be part of the
     *  string.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strrchr.html
     *\param[in] s the string
     *\param[in] c the character to look for in string (in reverse direction)
     *\return The strrchr function returns a pointer to the character, or a null pointer if c does not
     *        occur in the string. */
    char *strrchr(const char *s,
                  int c);

    /** ISO/IEC 9899:1999 7.21.5.6
     *
     *  The strspn function computes the length of the maximum initial segment of the string
     *  pointed to by s1 which consists entirely of characters from the string pointed to by s2.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strspn.html
     *\param[in] s1 the first string
     *\param[in] s2 the second string
     *\return The strspn function returns the length of the segment. */
    size_t strspn(const char *s1,
                  const char *s2);

    /** ISO/IEC 9899:1999 7.21.5.7
     *
     *  The strstr function locates the ﬁrst occurrence in the string pointed to by s1 of the
     *  sequence of characters (excluding the terminating null character) in the string pointed to
     *  by s2.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strstr.html
     *\param[in] s1 the first string
     *\param[in] s2 the second string
     *\return The strstr function returns a pointer to the located string, or a null pointer if the string
     *        is not found. If s2 points to a string with zero length, the function returns s1. */
    char *strstr(const char *s1,
                 const char *s2);

    /** ISO/IEC 9899:1999 7.21.5.8
     *
     *  A sequence of calls to the strtok function breaks the string pointed to by s1 into a
     *  sequence of tokens, each of which is delimited by a character from the string pointed to
     *  by s2. The ﬁrst call in the sequence has a non-null ﬁrst argument; subsequent calls in the
     *  sequence have a null ﬁrst argument. The separator string pointed to by s2 may be
     *  different from call to call.
     *  The ﬁrst call in the sequence searches the string pointed to by s1 for the ﬁrst character
     *  that is not contained in the current separator string pointed to by s2. If no such character
     *  is found, then there are no tokens in the string pointed to by s1 and the strtok function
     *  returns a null pointer. If such a character is found, it is the start of the ﬁrst token.
     *  The strtok function then searches from there for a character that is contained in the
     *  current separator string. If no such character is found, the current token extends to the
     *  end of the string pointed to by s1, and subsequent searches for a token will return a null
     *  pointer. If such a character is found, it is overwritten by a null character, which
     *  terminates the current token. The strtok function saves a pointer to the following
     *  character, from which the next search for a token will start.
     *  Each subsequent call, with a null pointer as the value of the ﬁrst argument, starts
     *  searching from the saved pointer and behaves as described above.
     *  The implementation shall behave as if no library function calls the strtok function.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strtok.html
     *\param[in] s1 the string to break up into tokens
     *\param[in] s2 delimiting characters
     *\return The strtok function returns a pointer to the ﬁrst character of a token, or a null pointer
     *        if there is no token. */
    char *strtok(char * _LIGHTLIBC_RESTRICT s1,
                 const char * _LIGHTLIBC_RESTRICT s2);


    /*
     * ISO/IEC 9899:1999 7.21.6 The memset function
     */

    /** ISO/IEC 9899:1999 7.21.6.1
     *
     *  The memset function copies the value of c (converted to an unsigned char) into
     *  each of the ﬁrst n characters of the object pointed to by s.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/memset.html
     *\param[in] s the memory location to set
     *\param[in] c the character to set every byte in s
     *\param[in] n the size of s in bytes
     *\return The memset function returns the value of s. */
    void *memset(void *s,
                 int c,
                 size_t n);

    /** ISO/IEC 9899:1999 7.21.6.2
     *
     *  The strerror function maps the number in errnum to a message string. Typically,
     *  the values for errnum come from errno, but strerror shall map any value of type
     *  int to a message.
     *  The implementation shall behave as if no library function calls the strerror function.
     *  [CX] The contents of the error message strings returned by strerror() should be determined
     *       by the setting of the LC_MESSAGES category in the current locale. The strerror()
     *       function shall not change the setting of errno if successful. Since no return value
     *       is reserved to indicate an error, an application wishing to check for error situations
     *       should set errno to 0, then call strerror(), then check errno.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strerror.html
     *\param[in] errnum the number of the error
     *\return The strerror function returns a pointer to the string, the contents of which are locale-
     *        speciﬁc. The array pointed to shall not be modiﬁed by the program, but may be
     *        overwritten by a subsequent call to the strerror function. */
    char *strerror(int errnum);

    /** ISO/IEC 9899:1999 7.21.6.3
     *
     *  The strlen function computes the length of the string pointed to by s.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strlen.html
     *\param[in] s the string
     *\return The strlen function returns the number of characters that precede the terminating null
     *        character. */
    size_t strlen(const char *s);

    /*@}*/

    /** \addtogroup libc_posix_string */
    /*@{*/

    /** IEEE Std 1003.1
    *
    *  The memccpy() function shall copy bytes from memory area s2 into s1, stopping
    *  after the first occurrence of byte c (converted to an unsigned char) is copied,
    *  or after n bytes are copied, whichever comes first. If copying takes place between
    *  objects that overlap, the behavior is undefined.
    *
    *  http://www.opengroup.org/onlinepubs/000095399/functions/memccpy.html
    *\param[in] s1
    *\param[in] s2
    *\param[in] c
    *\param[in] n
    *\return The memccpy() function shall return a pointer to the byte after the copy of c in
    *        s1, or a null pointer if c was not found in the first n bytes of s2. */
    void *memccpy(void *_LIGHTLIBC_RESTRICT s1,
		  const void *_LIGHTLIBC_RESTRICT s2,
		  int c,
		  size_t n);

    /** IEEE Std 1003.1
    *
    *  The strdup() function shall return a pointer to a new string, which is a duplicate of
    *  the string pointed to by s1. The returned pointer can be passed to free(). A null pointer
    *  is returned if the new string cannot be created.
    *
    *  http://www.opengroup.org/onlinepubs/000095399/functions/strdup.html
    *\param[in] s1
    *\return The strdup() function shall return a pointer to a new string on success. Otherwise,
    *        it shall return a null pointer and set errno to indicate the error. */
    char *strdup(const char *s1);

    /*@}*/

#ifdef __cplusplus
  }
#endif



#endif
