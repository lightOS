/*
lightOS libc
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBC_TIME_H
#define LIGHTOS_LIBC_TIME_H


#include <lightlibc/compiler.h>

/*
 * Standard includes
 */
#include <stddef.h>

/*
 * Internal includes
 */
#include <libc/types.h>                // NOTE: _LIBC_time_t, _LIBC_clock_t



/*! \addtogroup libc_time libc */
/*@{*/


// TODO: POSIX

/*
 * Defines
 * ISO/IEC 9899:1999 7.19.1
 */

/** Defines to 1000000 according to [XSI] */
#define CLOCKS_PER_SEC                 1000000

/*
 * Types
 * ISO/IEC 9899:1999 7.23.1
 */

/** Arithmetic types capable of representing times */
typedef _LIBC_time_t time_t;

/** Arithmetic types capable of representing times */
typedef _LIBC_clock_t clock_t;

/**  holds the components of a calendar time, called the broken-down time. */
struct tm
{
  /** seconds after the minute — [0, 60] */
  int tm_sec;
  /** minutes after the hour — [0, 59] */
  int tm_min;
  /** hours since midnight — [0, 23] */
  int tm_hour;
  /** hours since midnight — [0, day of the month — [1, 31] */
  int tm_mday;
  /** months since January — [0, 11] */
  int tm_mon;
  /** years since 1900 */
  int tm_year;
  /** days since Sunday — [0, 6] */
  int tm_wday;
  /** days since January 1 — [0, 365] */
  int tm_yday;
  /** Daylight Saving Time ﬂag */
  int tm_isdst;
};


/*
 * Functions
 */

#ifdef __cplusplus
  extern "C"
  {
#endif


  /*
   * ISO/IEC 9899:1999 7.23.2 Time manipulation functions
   */

  /** ISO/IEC 9899:1999 7.23.2.1
   *
   *  The clock function determines the processor time used.
   *
   *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/clock.html
   *\todo implement
   *\return The clock function returns the implementation’s best approximation to the processor
   *        time used by the program since the beginning of an implementation-deﬁned era related
   *        only to the program invocation. To determine the time in seconds, the value returned by
   *        the clock function should be divided by the value of the macro CLOCKS_PER_SEC. If
   *        the processor time used is not available or its value cannot be represented, the function
   *        returns the value (clock_t)(-1). */
  clock_t clock(void);

  /** ISO/IEC 9899:1999 7.23.2.2
   *
   *  The difftime function computes the difference between two calendar times: time1 -
   *  time0.
   *
   *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/difftime.html
   *\param[in] time1 first time
   *\param[in] time0 second time
   *\return The difftime function returns the difference expressed in seconds as a double. */
  double difftime(time_t time1, time_t time0);

  /** ISO/IEC 9899:1999 7.23.2.3
   *
   *  The mktime function converts the broken-down time, expressed as local time, in the
   *  structure pointed to by timeptr into a calendar time value with the same encoding as
   *  that of the values returned by the time function. The original values of the tm_wday
   *  and tm_yday components of the structure are ignored, and the original values of the
   *  other components are not restricted to the ranges indicated above. On successful
   *  completion, the values of the tm_wday and tm_yday components of the structure are
   *  set appropriately, and the other components are set to represent the speciﬁed calendar
   *  time, but with their values forced to the ranges indicated above; the ﬁnal value of
   *  tm_mday is not set until tm_mon and tm_year are determined.
   *
   *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/mktime.html and
   *       http://www.opengroup.org/onlinepubs/000095399/basedefs/xbd_chap04.html#tag_04_14
   *\param[in] timeptr
   *\todo implement fully
   *\return The mktime function returns the speciﬁed calendar time encoded as a value of type
   *        time_t. If the calendar time cannot be represented, the function returns the value
   *        (time_t)(-1) and [CX] and may set errno to indicate the error (EOVERFLOW). */
  time_t mktime(struct tm *timeptr);

  /** ISO/IEC 9899:1999 7.23.2.4
   *
   *  The time function determines the current calendar time. The encoding of the value is
   *  unspeciﬁed.
   *
   *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/time.html
   *\param[in,out] tloc
   *\return The time function returns the implementation’s best approximation to the current
   *        calendar time. The value (time_t)(-1) is returned if the calendar time is not
   *        available. If timer is not a null pointer, the return value is also assigned to the object it
   *        points to. */
  time_t time(time_t *tloc);


  /*
   * ISO/IEC 9899:1999 7.23.3 Time conversion functions
   */

  /** ISO/IEC 9899:1999 7.23.3.1
   *
   *  The asctime function converts the broken-down time in the structure pointed to by
   *  timeptr into a string in the form
   *
   *    Sun Sep 16 01:03:52 1973\n\0
   *
   *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/asctime.html
   *\param[in] timeptr the broken-down time
   *\return The asctime function returns a pointer to the string.
   *        [CX] If the function is unsuccessful, it shall return NULL. */
  char *asctime(const struct tm *timeptr);

  /** ISO/IEC 9899:1999 7.23.3.2
   *
   *  The ctime function converts the calendar time pointed to by timer to local time in the
   *  form of a string. It is equivalent to
   *  asctime(localtime(timer))
   *
   *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/ctime.html
   *\param[in] timer the time since epoch
   *\return The ctime function returns the pointer returned by the asctime function with that
   *        broken-down time as argument. */
  char *ctime(const time_t *timer);

  /** ISO/IEC 9899:1999 7.23.3.3
   *
   *  The gmtime function converts the calendar time pointed to by timer into a broken-
   *  down time, expressed as UTC.
   *
   *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/gmtime.html
   *\param[in] timer the time since epoch
   *\todo incomplete implementation
   *\return The gmtime function returns a pointer to the broken-down time, or a null pointer if the
   *        speciﬁed time cannot be converted to UTC and [CX] set errno to indicate the error (EOVERFLOW). */
  struct tm *gmtime(const time_t *timer);

  /** ISO/IEC 9899:1999 7.23.3.4
   *
   *  The localtime function converts the calendar time pointed to by timer into a
   *  broken-down time, expressed as local time.
   *  [CX] Local timezone information is used as though localtime() calls tzset().
   *
   *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/localtime.html
   *\param[in] timer time since epoch
   *\todo incomplete implementation
   *\return The localtime function returns a pointer to the broken-down time, or a null pointer if
   *        the speciﬁed time cannot be converted to local time and [CX] set errno to indicate the error
   *        (EOVERFLOW). */
  struct tm *localtime(const time_t *timer);

  /** ISO/IEC 9899:1999 7.23.3.5
   *
   *  The strftime function places characters into the array pointed to by s as controlled by
   *  the string pointed to by format. The format shall be a multibyte character sequence,
   *  beginning and ending in its initial shift state. The format string consists of zero or
   *  more conversion speciﬁers and ordinary multibyte characters. A conversion speciﬁer
   *  consists of a % character, possibly followed by an E or O modiﬁer character (described
   *  in the link below), followed by a character that determines the behavior of the conversion speciﬁer.
   *  All ordinary multibyte characters (including the terminating null character) are copied
   *  unchanged into the array. If copying takes place between objects that overlap, the
   *  behavior is undeﬁned. No more than maxsize characters are placed into the array.
   *
   *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/strftime.html
   *\param[in] s
   *\param[in] maxsize
   *\param[in] format
   *\param[in] timeptr
   *\todo incomplete implementation
   *\return If the total number of resulting characters including the terminating null character is not
   *        more than maxsize, the strftime function returns the number of characters placed
   *        into the array pointed to by s not including the terminating null character. Otherwise,
   *        zero is returned and the contents of the array are indeterminate. */
  size_t strftime(char * _LIGHTLIBC_RESTRICT s,
                  size_t maxsize,
                  const char * _LIGHTLIBC_RESTRICT format,
                  const struct tm * _LIGHTLIBC_RESTRICT timeptr);

#ifdef __cplusplus
  }
#endif

/*@}*/



#endif
