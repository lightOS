/*
lightOS libc
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBC_LIBC_TYPES_H
#define LIGHTOS_LIBC_LIBC_TYPES_H

/*
 * Standard includes
 */
#include <stddef.h>

/*
 * libOS includes
 */
#include <libOS/this/types.h>

/*
 * Libarch includes
 */
#include <libarch/type.h>


/*! \addtogroup libc libc */
/*@{*/

/*
 * stdint.h types
 */
typedef _LIBARCH_int8_t                _LIBC_int8_t;
typedef _LIBARCH_uint8_t               _LIBC_uint8_t;
typedef _LIBARCH_int16_t               _LIBC_int16_t;
typedef _LIBARCH_uint16_t              _LIBC_uint16_t;
typedef _LIBARCH_int32_t               _LIBC_int32_t;
typedef _LIBARCH_uint32_t              _LIBC_uint32_t;
typedef _LIBARCH_int64_t               _LIBC_int64_t;
typedef _LIBARCH_uint64_t              _LIBC_uint64_t;
typedef _LIBARCH_int_least8_t          _LIBC_int_least8_t;
typedef _LIBARCH_uint_least8_t         _LIBC_uint_least8_t;
typedef _LIBARCH_int_least16_t         _LIBC_int_least16_t;
typedef _LIBARCH_uint_least16_t        _LIBC_uint_least16_t;
typedef _LIBARCH_int_least32_t         _LIBC_int_least32_t;
typedef _LIBARCH_uint_least32_t        _LIBC_uint_least32_t;
typedef _LIBARCH_int_least64_t         _LIBC_int_least64_t;
typedef _LIBARCH_uint_least64_t        _LIBC_uint_least64_t;
typedef _LIBARCH_int_fast8_t           _LIBC_int_fast8_t;
typedef _LIBARCH_uint_fast8_t          _LIBC_uint_fast8_t;
typedef _LIBARCH_int_fast16_t          _LIBC_int_fast16_t;
typedef _LIBARCH_uint_fast16_t         _LIBC_uint_fast16_t;
typedef _LIBARCH_int_fast32_t          _LIBC_int_fast32_t;
typedef _LIBARCH_uint_fast32_t         _LIBC_uint_fast32_t;
typedef _LIBARCH_int_fast64_t          _LIBC_int_fast64_t;
typedef _LIBARCH_uint_fast64_t         _LIBC_uint_fast64_t;
typedef _LIBARCH_intptr_t              _LIBC_intptr_t;
typedef _LIBARCH_uintptr_t             _LIBC_uintptr_t;
typedef _LIBARCH_intmax_t              _LIBC_intmax_t;
typedef _LIBARCH_uintmax_t             _LIBC_uintmax_t;
#define _LIBC_INT8_MIN                 _LIBARCH_INT8_MIN
#define _LIBC_INT8_MAX                 _LIBARCH_INT8_MAX
#define _LIBC_UINT8_MAX                _LIBARCH_UINT8_MAX
#define _LIBC_INT16_MIN                _LIBARCH_INT16_MIN
#define _LIBC_INT16_MAX                _LIBARCH_INT16_MAX
#define _LIBC_UINT16_MAX               _LIBARCH_UINT16_MAX
#define _LIBC_INT32_MIN                _LIBARCH_INT32_MIN
#define _LIBC_INT32_MAX                _LIBARCH_INT32_MAX
#define _LIBC_UINT32_MAX               _LIBARCH_UINT32_MAX
#define _LIBC_INT64_MIN                _LIBARCH_INT64_MIN
#define _LIBC_INT64_MAX                _LIBARCH_INT64_MAX
#define _LIBC_UINT64_MAX               _LIBARCH_UINT64_MAX
#define _LIBC_INT_LEAST8_MIN           _LIBARCH_INT_LEAST8_MIN
#define _LIBC_INT_LEAST8_MAX           _LIBARCH_INT_LEAST8_MAX
#define _LIBC_UINT_LEAST8_MAX          _LIBARCH_UINT_LEAST8_MAX
#define _LIBC_INT_LEAST16_MIN          _LIBARCH_INT_LEAST16_MIN
#define _LIBC_INT_LEAST16_MAX          _LIBARCH_INT_LEAST16_MAX
#define _LIBC_UINT_LEAST16_MAX         _LIBARCH_UINT_LEAST16_MAX
#define _LIBC_INT_LEAST32_MIN          _LIBARCH_INT_LEAST32_MIN
#define _LIBC_INT_LEAST32_MAX          _LIBARCH_INT_LEAST32_MAX
#define _LIBC_UINT_LEAST32_MAX         _LIBARCH_UINT_LEAST32_MAX
#define _LIBC_INT_LEAST64_MIN          _LIBARCH_INT_LEAST64_MIN
#define _LIBC_INT_LEAST64_MAX          _LIBARCH_INT_LEAST64_MAX
#define _LIBC_UINT_LEAST64_MAX         _LIBARCH_UINT_LEAST64_MAX
#define _LIBC_INT_FAST8_MIN            _LIBARCH_INT_FAST8_MIN
#define _LIBC_INT_FAST8_MAX            _LIBARCH_INT_FAST8_MAX
#define _LIBC_UINT_FAST8_MAX           _LIBARCH_UINT_FAST8_MAX
#define _LIBC_INT_FAST16_MIN           _LIBARCH_INT_FAST16_MIN
#define _LIBC_INT_FAST16_MAX           _LIBARCH_INT_FAST16_MAX
#define _LIBC_UINT_FAST16_MAX          _LIBARCH_UINT_FAST16_MAX
#define _LIBC_INT_FAST32_MIN           _LIBARCH_INT_FAST32_MIN
#define _LIBC_INT_FAST32_MAX           _LIBARCH_INT_FAST32_MAX
#define _LIBC_UINT_FAST32_MAX          _LIBARCH_UINT_FAST32_MAX
#define _LIBC_INT_FAST64_MIN           _LIBARCH_INT_FAST64_MIN
#define _LIBC_INT_FAST64_MAX           _LIBARCH_INT_FAST64_MAX
#define _LIBC_UINT_FAST64_MAX          _LIBARCH_UINT_FAST64_MAX
#define _LIBC_INTPTR_MIN               _LIBARCH_INTPTR_MIN
#define _LIBC_INTPTR_MAX               _LIBARCH_INTPTR_MAX
#define _LIBC_UINTPTR_MAX              _LIBARCH_UINTPTR_MAX
#define _LIBC_INTMAX_MIN               _LIBARCH_INTMAX_MIN
#define _LIBC_INTMAX_MAX               _LIBARCH_INTMAX_MAX
#define _LIBC_UINTMAX_MAX              _LIBARCH_UINTMAX_MAX
#define _LIBC_PTRDIFF_MIN              _LIBARCH_PTRDIFF_MIN
#define _LIBC_PTRDIFF_MAX              _LIBARCH_PTRDIFF_MAX
/** \todo sig_atomic_t limits */
#define _LIBC_SIZE_MAX                 _LIBARCH_SIZE_MAX
/** \todo wchar_t limits */
/** \todo wint_t limits */
#define _LIBC_INT8_C(x)                _LIBARCH_INT8_C(x)
#define _LIBC_UINT8_C(x)               _LIBARCH_UINT8_C(x)
#define _LIBC_INT16_C(x)               _LIBARCH_INT16_C(x)
#define _LIBC_UINT16_C(x)              _LIBARCH_UINT16_C(x)
#define _LIBC_INT32_C(x)               _LIBARCH_INT32_C(x)
#define _LIBC_UINT32_C(x)              _LIBARCH_UINT32_C(x)
#define _LIBC_INT64_C(x)               _LIBARCH_INT64_C(x)
#define _LIBC_UINT64_C(x)              _LIBARCH_UINT64_C(x)
#define _LIBC_INTMAX_C(x)              _LIBARCH_INTMAX_C(x)
#define _LIBC_UINTMAX_C(x)             _LIBARCH_UINTMAX_C(x)


/*
 * inttypes.h types
 */



/*
 * stdio.h types
 */

/** File position type */
typedef size_t _LIBC_fpos_t;
/** Forward declaration of the _LIBC_INTERNAL_FILE structure
 *\note the _LIBC_INTERNAL_FILE structure is defined in libc/internal/stdio.h */
struct _LIBC_INTERNAL_FILE;
/** Typedef the FILE type */
typedef struct _LIBC_INTERNAL_FILE _LIBC_FILE;



/*
 * stdlib.h types
 */

typedef struct
{
  int quot;
  int rem;
} _LIBC_div_t;

typedef struct
{
  long quot;
  long rem;
} _LIBC_ldiv_t;

typedef struct
{
  long long quot;
  long long rem;
} _LIBC_lldiv_t;


/*
 * time.h types
 */
typedef _LIBOS_time_t _LIBC_time_t;
typedef _LIBOS_clock_t _LIBC_clock_t;

/*@}*/

//! Used for file block counts
typedef size_t _LIBUNIX_blkcnt_t;
typedef _LIBARCH_uint64_t _LIBUNIX_blkcnt64_t;
//! Used for block sizes
typedef size_t _LIBUNIX_blksize_t;

/*
TODO clock_t Used for system times in clock ticks or CLOCKS_PER_SEC (see <time.h>).
TODO clockid_t Used for clock ID type in the clock and timer functions.
*/

/*
TODO fsblkcnt_t Used for file system block counts
TODO fsfilcnt_t Used for file system file counts
*/

//! Used as a general identifier
typedef size_t _LIBUNIX_id_t;
//! Used for user IDs
typedef _LIBUNIX_id_t _LIBUNIX_uid_t;
//! Used for group IDs
typedef _LIBUNIX_id_t _LIBUNIX_gid_t;
//! Used for file serial numbers
typedef _LIBUNIX_id_t _LIBUNIX_ino_t;
typedef _LIBARCH_uint64_t _LIBUNIX_ino64_t;
//! Used for device IDs
typedef _LIBUNIX_id_t _LIBUNIX_dev_t;
//! Used for process IDs and process group IDs
typedef _LIBUNIX_id_t _LIBUNIX_pid_t;

/*
TODO key_t Used for interprocess communication.
*/

//! Used for some file attributes
typedef size_t _LIBUNIX_mode_t;
//! Used for link counts
typedef size_t _LIBUNIX_nlink_t;
//! Used for file sizes
typedef size_t _LIBUNIX_off_t;
typedef _LIBARCH_uint64_t _LIBUNIX_off64_t;

/*
TODO pthread_attr_t Used to identify a thread attribute object.
TODO pthread_barrier_t [BAR] [Option Start] Used to identify a barrier. [Option End]
TODO pthread_barrierattr_t [BAR] [Option Start] Used to define a barrier attributes object. [Option End]
TODO pthread_cond_t Used for condition variables.
TODO pthread_condattr_t Used to identify a condition attribute object.
TODO pthread_key_t Used for thread-specific data keys.
TODO pthread_mutex_t Used for mutexes.
TODO pthread_mutexattr_t Used to identify a mutex attribute object.
TODO pthread_once_t Used for dynamic package initialisation.
TODO pthread_rwlock_t Used for read-write locks.
TODO pthread_rwlockattr_t Used for read-write lock attributes.
TODO pthread_spinlock_t [SPI] [Option Start] Used to identify a spin lock. [Option End]
TODO pthread_t Used to identify a thread.
*/

typedef _LIBARCH_ssize_t _LIBUNIX_ssize_t;
// NOTE: size_t from stddef.h

//! Used for time in microseconds
typedef _LIBUNIX_ssize_t _LIBUNIX_suseconds_t;
//! Used for time in microseconds
typedef size_t _LIBUNIX_useconds_t;

// NOTE: time_t from time.h

/*
TODO timer_t Used for timer ID returned by timer_create().
trace_attr_t
    [TRC] [Option Start] Used to identify a trace stream attributes object. [Option End]
trace_event_id_t
    [TRC] [Option Start] Used to identify a trace event type. [Option End]
trace_event_set_t
    [TRC TEF] [Option Start] Used to identify a trace event type set. [Option End]
trace_id_t
    [TRC] [Option Start] Used to identify a trace stream. [Option End]
*/

#endif
