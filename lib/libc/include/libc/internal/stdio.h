/*
lightOS libc
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBC_LIBC_INTERNAL_STDIO_H
#define LIGHTOS_LIBC_LIBC_INTERNAL_STDIO_H

/*! \addtogroup libc libc */
/*@{*/
/*! \addtogroup libc_internal internal */
/*@{*/

#include <stddef.h>
#include <libc/types.h>
#include <libOS/LIBC_glue.h>

struct _LIBC_INTERNAL_FILE
{
  /*! File position */
  _LIBC_fpos_t                fpos;
  /*! Flags */
  size_t                      flags;
  /*! Pointer to the buffer */
  char*                       buf;
  /*! Size of the buffer */
  size_t                      bufsize;
  /*! Position within the buffer */
  size_t                      bufpos;
  /*! UNIX: File descriptor */
  int                         des;
  /*! OS-dependent structure */
  struct _LIBOS_FILE          osdep;
  /*! Pointer to the next FILE */
  struct _LIBC_INTERNAL_FILE* next;
};

#ifdef __cplusplus
  extern "C"
  {
#endif



#ifdef __cplusplus
  }
#endif

/*@}*/
/*@}*/

#endif
