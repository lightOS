/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBUNIX_FCNTL_H
#define LIGHTOS_LIBUNIX_FCNTL_H

/*! \addtogroup libunix libunix */
/*@{*/

// HACK
#define O_RDWR                1
#define O_CREAT               2
#define O_EXCL                4
#define O_WRONLY              8
#define O_TRUNC               16
#define O_RDONLY              32
#define O_APPEND              64
#define O_NONBLOCK            128

// HACK
#define F_DUPFD               1
#define F_GETFD               2
#define F_SETFD               3
#define F_GETFL               4
#define F_SETFL               5
#define F_GETLK               6
#define F_SETLK               7
#define F_SETLKW              8
#define F_GETOWN              9
#define F_SETOWN              10

#define FD_CLOEXEC            16

/*! Gets the current serial port settings */
#define TCGETS                0x0100
/*! Sets the serial port settings immediately */
#define TCSETS                0x0101
/*! Sets the serial port settings after flushing the input and output buffers */
#define TCSETSF               0x0102
/*! Sets the serial port settings after allowing the input and output buffers to drain/empty */
#define TCSETSW               0x0103

#ifdef __cplusplus
  extern "C"
  {
#endif

  int fcntl(int fildes, int cmd, ...);
  int open(const char *path, int oflags, ...);

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
