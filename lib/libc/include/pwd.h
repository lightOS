/*
lightOS libunix
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBUNIX_PWD_H
#define LIGHTOS_LIBUNIX_PWD_H

/*! \addtogroup libunix libunix */
/*@{*/

#include <sys/types.h>

struct passwd
{
  /*! User's login name */
  char *pw_name;
  /*! Numerical user ID */
  uid_t pw_uid;
  /*! Numerical group ID */
  gid_t pw_gid;
  /*! Initial working directory */
  char *pw_dir;
  /*! Program to use as shell */
  char *pw_shell;
};

#ifdef __cplusplus
  extern "C"
  {
#endif

  struct passwd *getpwuid(uid_t);
  void endpwent();
  struct passwd *getpwent();

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
