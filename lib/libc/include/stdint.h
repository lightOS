/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef _LIGHTLIBC_STDINT_H
#define _LIGHTLIBC_STDINT_H



#include <libc/types.h>



/** \addtogroup libc_stdint */
/*@{*/

/*
 * ISO/IEC 9899:1999 7.18.1.1 Exact-width integer types
 */

typedef _LIBC_int8_t                   int8_t;
typedef _LIBC_uint8_t                  uint8_t;
typedef _LIBC_int16_t                  int16_t;
typedef _LIBC_uint16_t                 uint16_t;
typedef _LIBC_int32_t                  int32_t;
typedef _LIBC_uint32_t                 uint32_t;
typedef _LIBC_int64_t                  int64_t;
typedef _LIBC_uint64_t                 uint64_t;


/*
 * ISO/IEC 9899:1999 7.18.1.2 Minimum-width integer types
 */

typedef _LIBC_int_least8_t             int_least8_t;
typedef _LIBC_uint_least8_t            uint_least8_t;
typedef _LIBC_int_least16_t            int_least16_t;
typedef _LIBC_uint_least16_t           uint_least16_t;
typedef _LIBC_int_least32_t            int_least32_t;
typedef _LIBC_uint_least32_t           uint_least32_t;
typedef _LIBC_int_least64_t            int_least64_t;
typedef _LIBC_uint_least64_t           uint_least64_t;


/*
 * ISO/IEC 9899:1999 7.18.1.3 Fastest minimum-width integer types
 */

typedef _LIBC_int_fast8_t              int_fast8_t;
typedef _LIBC_uint_fast8_t             uint_fast8_t;
typedef _LIBC_int_fast16_t             int_fast16_t;
typedef _LIBC_uint_fast16_t            uint_fast16_t;
typedef _LIBC_int_fast32_t             int_fast32_t;
typedef _LIBC_uint_fast32_t            uint_fast32_t;
typedef _LIBC_int_fast64_t             int_fast64_t;
typedef _LIBC_uint_fast64_t            uint_fast64_t;

/*
 * ISO/IEC 9899:1999 7.18.1.4 Integer types capable of holding object pointers
 */

typedef _LIBC_intptr_t                 intptr_t;
typedef _LIBC_uintptr_t                uintptr_t;

/*
 * ISO/IEC 9899:1999 7.18.1.5 Greatest-width integer types
 */

typedef _LIBC_intmax_t                 intmax_t;
typedef _LIBC_uintmax_t                uintmax_t;



/*
 * ISO/IEC 9899:1999 7.18.2 Limits of speciﬁed-width integer types
 */

/*
 * ISO/IEC 9899:1999 7.18.2.1 Limits of exact-width integer types
 */

#define INT8_MIN                       _LIBC_INT8_MIN
#define INT8_MAX                       _LIBC_INT8_MAX
#define UINT8_MAX                      _LIBC_UINT8_MAX
#define INT16_MIN                      _LIBC_INT16_MIN
#define INT16_MAX                      _LIBC_INT16_MAX
#define UINT16_MAX                     _LIBC_UINT16_MAX
#define INT32_MIN                      _LIBC_INT32_MIN
#define INT32_MAX                      _LIBC_INT32_MAX
#define UINT32_MAX                     _LIBC_UINT32_MAX
#define INT64_MIN                      _LIBC_INT64_MIN
#define INT64_MAX                      _LIBC_INT64_MAX
#define UINT64_MAX                     _LIBC_UINT64_MAX


/*
 * ISO/IEC 9899:1999 7.18.2.2 Limits of minimum-width integer types
 */

#define INT_LEAST8_MIN                 _LIBC_INT_LEAST8_MIN
#define INT_LEAST8_MAX                 _LIBC_INT_LEAST8_MAX
#define UINT_LEAST8_MAX                _LIBC_UINT_LEAST8_MAX
#define INT_LEAST16_MIN                _LIBC_INT_LEAST16_MIN
#define INT_LEAST16_MAX                _LIBC_INT_LEAST16_MAX
#define UINT_LEAST16_MAX               _LIBC_UINT_LEAST16_MAX
#define INT_LEAST32_MIN                _LIBC_INT_LEAST32_MIN
#define INT_LEAST32_MAX                _LIBC_INT_LEAST32_MAX
#define UINT_LEAST32_MAX               _LIBC_UINT_LEAST32_MAX
#define INT_LEAST64_MIN                _LIBC_INT_LEAST64_MIN
#define INT_LEAST64_MAX                _LIBC_INT_LEAST64_MAX
#define UINT_LEAST64_MAX               _LIBC_UINT_LEAST64_MAX


/*
 * ISO/IEC 9899:1999 7.18.2.3 Limits of fastest minimum-width integer types
 */

#define INT_FAST8_MIN                  _LIBC_INT_FAST8_MIN
#define INT_FAST8_MAX                  _LIBC_INT_FAST8_MAX
#define UINT_FAST8_MAX                 _LIBC_UINT_FAST8_MAX
#define INT_FAST16_MIN                 _LIBC_INT_FAST16_MIN
#define INT_FAST16_MAX                 _LIBC_INT_FAST16_MAX
#define UINT_FAST16_MAX                _LIBC_UINT_FAST16_MAX
#define INT_FAST32_MIN                 _LIBC_INT_FAST32_MIN
#define INT_FAST32_MAX                 _LIBC_INT_FAST32_MAX
#define UINT_FAST32_MAX                _LIBC_UINT_FAST32_MAX
#define INT_FAST64_MIN                 _LIBC_INT_FAST64_MIN
#define INT_FAST64_MAX                 _LIBC_INT_FAST64_MAX
#define UINT_FAST64_MAX                _LIBC_UINT_FAST64_MAX

/*
 * ISO/IEC 9899:1999 7.18.2.4 Limits of integer types capable of holding object pointers
 */

#define INTPTR_MIN                     _LIBC_INTPTR_MIN
#define INTPTR_MAX                     _LIBC_INTPTR_MAX
#define UINTPTR_MAX                    _LIBC_UINTPTR_MAX

/*
 * ISO/IEC 9899:1999 7.18.2.5 Limits of greatest-width integer types
 */

#define INTMAX_MIN                     _LIBC_INTMAX_MIN
#define INTMAX_MAX                     _LIBC_INTMAX_MAX
#define UINTMAX_MAX                    _LIBC_UINTMAX_MAX

/*
 * ISO/IEC 9899:1999 7.18.3 Limits of other integer types
 */

#define PTRDIFF_MIN                    _LIBC_PTRDIFF_MIN
#define PTRDIFF_MAX                    _LIBC_PTRDIFF_MAX
/** \todo sig_atomic_t limits */
#define SIZE_MAX                       _LIBC_SIZE_MAX
/** \todo wchar_t limits */
/** \todo wint_t limits */



/*
 * ISO/IEC 9899:1999 7.18.4 Macros for integer constants
 */

/*
 * ISO/IEC 9899:1999 7.18.4.1 Macros for minimum-width integer constants
 */
#define INT8_C(x)                      _LIBC_INT8_C(x)
#define UINT8_C(x)                     _LIBC_UINT8_C(x)
#define INT16_C(x)                     _LIBC_INT16_C(x)
#define UINT16_C(x)                    _LIBC_UINT16_C(x)
#define INT32_C(x)                     _LIBC_INT32_C(x)
#define UINT32_C(x)                    _LIBC_UINT32_C(x)
#define INT64_C(x)                     _LIBC_INT64_C(x)
#define UINT64_C(x)                    _LIBC_UINT64_C(x)

/*
 * ISO/IEC 9899:1999 7.18.4.2 Macros for greatest-width integer constants
 */
#define INTMAX_C(x)                    _LIBC_INTMAX_C(x)
#define UINTMAX_C(x)                   _LIBC_UINTMAX_C(x)

/*@}*/



#endif
