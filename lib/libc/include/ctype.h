/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef _LIGHTLIBC_CTYPE_H
#define _LIGHTLIBC_CTYPE_H



#ifndef _LIGHTLIBC_NO_POSIX
/** \addtogroup libc_posix_ctype */
/*@{*/

  /** IEEE Std 1003.1
  *
  *  The _tolower() macro shall be equivalent to tolower(c) except that the application shall
  *  ensure that the argument c is an uppercase letter.
  *
  *  http://www.opengroup.org/onlinepubs/000095399/functions/_tolower.html
  *\param[in] c the character that should be converted
  *\return Upon successful completion, _tolower() shall return the lowercase letter corresponding
  *        to the argument passed. */

  #define _tolower(c)                    tolower(c)

  /** IEEE Std 1003.1
  *
  *  The _toupper() macro shall be equivalent to toupper() except that the application shall ensure
  *  that the argument c is a lowercase letter.
  *
  *  http://www.opengroup.org/onlinepubs/000095399/functions/_toupper.html
  *\param[in] c the character that should be converted
  *\return Upon successful completion, _toupper() shall return the uppercase letter corresponding
  *        to the argument passed. */
  #define _toupper(c)                    toupper(c)

/*@}*/
#endif



#ifdef __cplusplus
  extern "C"
  {
#endif

    /** \addtogroup libc_ctype */
    /*@{*/

    /*
     * ISO/IEC 9899:1999 7.4.1 Character classiﬁcation functions
     */

    /** ISO/IEC 9899:1999 7.4.1.1
     *
     *  The isalnum function tests for any character for which isalpha or isdigit is true.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/isalnum.html
     *\param[in] c the character that should be tested
     *\return true, if alphanumeric character, false otherwise */
    int isalnum(int c);

    /** ISO/IEC 9899:1999 7.4.1.2
     *
     *  The isalpha function tests for any character for which isupper or islower is true,
     *  or any character that is one of a locale-speciﬁc set of alphabetic characters for which
     *  none of iscntrl, isdigit, ispunct, or isspace is true. In the "C" locale,
     *  isalpha returns true only for the characters for which isupper or islower is true.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/isalpha.html
     *\param[in] c the character that should be tested
     *\return true, if alphabetic character, false otherwise */
    int isalpha(int c);

    /** ISO/IEC 9899:1999 7.4.1.3
     *
     *  The isblank function tests for any character that is a standard blank character or is one
     *  of a locale-speciﬁc set of characters for which isspace is true and that is used to
     *  separate words within a line of text. The standard blank characters are the following:
     *  space (' '), and horizontal tab ('\t'). In the "C" locale, isblank returns true only
     *  for the standard blank characters.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/009695399/functions/isblank.html
     *\param[in] c the character that should be tested
     *\return true, if standard blank character, false otherwise */
    int isblank(int c);

    /** ISO/IEC 9899:1999 7.4.1.4
     *
     *  The iscntrl function tests for any control character.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/iscntrl.html
     *\param[in] c the character that should be tested
     *\return true, if control character, false otherwise */
    int iscntrl(int c);

    /** ISO/IEC 9899:1999 7.4.1.5
     *
     *  The isdigit function tests for any decimal-digit character (as deﬁned in 5.2.1).
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/isdigit.html
     *\param[in] c the character that should be tested
     *\return true, if digit character, false otherwise */
    int isdigit(int c);

    /** ISO/IEC 9899:1999 7.4.1.6
     *
     *  The isgraph function tests for any printing character except space (' ').
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/isgraph.html
     *\param[in] c the character that should be tested
     *\return true, if printing character, false otherwise */
    int isgraph(int c);

    /** ISO/IEC 9899:1999 7.4.1.7
     *
     *  The islower function tests for any character that is a lowercase letter or is one of a
     *  locale-speciﬁc set of characters for which none of iscntrl, isdigit, ispunct, or
     *  isspace is true. In the "C" locale, islower returns true only for the lowercase
     *  letters (as deﬁned in 5.2.1).
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/islower.html
     *\param[in] c the character that should be tested
     *\return true, if lowercase character, false otherwise */
    int islower(int c);

    /** ISO/IEC 9899:1999 7.4.1.8
     *
     *  The isprint function tests for any printing character including space (' ').
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/isprint.html
     *\param[in] c the character that should be tested
     *\return true, if printing character, false otherwise */
    int isprint(int c);

    /** ISO/IEC 9899:1999 7.4.1.9
     *
     *  The ispunct function tests for any printing character that is one of a locale-speciﬁc set
     *  of punctuation characters for which neither isspace nor isalnum is true. In the "C"
     *  locale, ispunct returns true for every printing character for which neither isspace
     *  nor isalnum is true.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/ispunct.html
     *\param[in] c the character that should be tested
     *\return true, if punctuation character, false otherwise */
    int ispunct(int c);

    /** ISO/IEC 9899:1999 7.4.1.10
     *
     *  The isspace function tests for any character that is a standard white-space character or
     *  is one of a locale-speciﬁc set of characters for which isalnum is false. The standard
     *  white-space characters are the following: space (' '), form feed ('\f'), new-line
     *  ('\n'), carriage return ('\r'), horizontal tab ('\t'), and vertical tab ('\v'). In the
     *  "C" locale, isspace returns true only for the standard white-space characters.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/isspace.html
     *\param[in] c the character that should be tested
     *\return true, if standard white-space character, false otherwise */
    int isspace(int c);

    /** ISO/IEC 9899:1999 7.4.1.11
     *
     *  The isupper function tests for any character that is an uppercase letter or is one of a
     *  locale-speciﬁc set of characters for which none of iscntrl, isdigit, ispunct, or
     *  isspace is true. In the "C" locale, isupper returns true only for the uppercase
     *  letters (as deﬁned in 5.2.1).
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/isupper.html
     *\param[in] c the character that should be tested
     *\return true, if uppercase character, false otherwise */
    int isupper(int c);

    /** ISO/IEC 9899:1999 7.4.1.12
     *
     *  The isxdigit function tests for any hexadecimal-digit character (as deﬁned in 6.4.4.2).
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/isxdigit.html
     *\param[in] c the character that should be tested
     *\return true, if hexadecimal-digit character, false otherwise */
    int isxdigit(int c);

    /*
     * ISO/IEC 9899:1999 7.4.2 Character case mapping functions
     */

    /** ISO/IEC 9899:1999 7.4.2.1
     *
     *  The tolower function converts an uppercase letter to a corresponding lowercase letter.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/tolower.html
     *\param[in] c the character that should be converted
     *\return If the argument is a character for which isupper is true and there are one or more
     *        corresponding characters, as speciﬁed by the current locale, for which islower is true,
     *        the tolower function returns one of the corresponding characters (always the same one
     *        for any given locale); otherwise, the argument is returned unchanged. */
    int tolower(int c);

    /** ISO/IEC 9899:1999 7.4.2.2
     *
     *  The toupper function converts a lowercase letter to a corresponding uppercase letter.
     *
     *  [CX] http://www.opengroup.org/onlinepubs/000095399/functions/toupper.html
     *\param[in] c the character that should be converted
     *\return If the argument is a character for which islower is true and there are one or more
     *        corresponding characters, as speciﬁed by the current locale, for which isupper is true,
     *        the toupper function returns one of the corresponding characters (always the same one
     *        for any given locale); otherwise, the argument is returned unchanged. */
    int toupper(int c);

    /*@}*/



    #ifndef _LIGHTLIBC_NO_POSIX
    /*! \addtogroup libc_posix_ctype */
    /*@{*/

    /** IEEE Std 1003.1
    *
    *  The isascii() function shall test whether c is a 7-bit US-ASCII character code.
    *  The isascii() function is defined on all integer values.
    *
    *  http://www.opengroup.org/onlinepubs/000095399/functions/isascii.html
    *\param[in] c the character that should be tested
    *\return The isascii() function shall return non-zero if c is a 7-bit US-ASCII character code
    *        between 0 and octal 0177 inclusive; otherwise, it shall return 0. */
    int isascii(int c);

    /** IEEE Std 1003.1
    *
    *  The toascii() function shall convert its argument into a 7-bit ASCII character.
    *
    *  http://www.opengroup.org/onlinepubs/000095399/functions/toascii.html
    *\param[in] c the character that should be converted
    *\return The toascii() function shall return the value (c & 0x7f).*/
    int toascii(int c);

    /*@}*/
    #endif

#ifdef __cplusplus
  }
#endif



#endif
