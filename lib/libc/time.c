/*
lightOS libc
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <time.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libOS/LIBC_glue.h>

/*
 * NOTE: Helper functions
 */

size_t _LIBC_month[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

static bool _LIBC_is_leap_year(size_t year)
{
  if ((year % 400) == 0)return true;
  if ((year % 100) == 0)return false;
  if ((year % 4) == 0)return true;
  return false;
}

size_t _LIBC_calc_day_of_year(size_t year, size_t month)
{
  size_t yday = 0;
  for (size_t i = 0;i < month;i++)
    yday += _LIBC_month[i];
  if (_LIBC_is_leap_year(year) == true && month > 1)++yday;
  return yday;
}

static void _LIBC_calc_month(size_t *days,
                             size_t *month)
{
  *month = 1;
  while (*days >= _LIBC_month[*month - 1])
  {
    *days -= _LIBC_month[*month - 1];
    ++*month;
  }
}

/*
 * libc functions
 */

clock_t clock(void)
{
  // TODO
  return (clock_t) -1;
}

double difftime(time_t time1, time_t time0)
{
  return (double)(time1 - time0);
}

time_t mktime(struct tm *timeptr)
{
  // NOTE: See http://www.opengroup.org/onlinepubs/000095399/basedefs/xbd_chap04.html#tag_04_14
  return timeptr->tm_sec + timeptr->tm_min * 60 + timeptr->tm_hour * 3600 + timeptr->tm_yday * 86400 +
         (timeptr->tm_year - 70) * 31536000 + ((timeptr->tm_year - 69) / 4) * 86400 -
         ((timeptr->tm_year - 1) / 100) * 86400 + ((timeptr->tm_year + 299) / 400) * 86400;
}

time_t time(time_t *tloc)
{
  struct tm time;
  _LIBOS_get_time(&time);
  time_t result = mktime(&time);
  if (tloc != NULL)
    *tloc = result;
  return result;
}

char *asctime(const struct tm *timeptr)
{
  static const char wday_name[7][3] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
  static const char mon_name[12][3] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                       "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
  static char result[26];
  sprintf(result,
          "%.3s %.3s%3d %.2d:%.2d:%.2d %d\n",
          wday_name[timeptr->tm_wday],
          mon_name[timeptr->tm_mon],
          timeptr->tm_mday, timeptr->tm_hour,
          timeptr->tm_min, timeptr->tm_sec,
          timeptr->tm_year);
  return result;
}

char *ctime(const time_t *timer)
{
  return asctime(localtime(timer));
}

struct tm *gmtime(const time_t *timer)
{
  // TODO?
  static struct tm result;

  if (timer == NULL)
    return NULL;

  size_t t = *timer;
  result.tm_sec = t % 60;
  t /= 60;
  result.tm_min = t % 60;
  t /= 60;
  result.tm_hour = t % 24;
  t /= 24;

  result.tm_year = 1970;
  while (1)
  {
    bool leapYear = _LIBC_is_leap_year(result.tm_year);
    if (leapYear == true && t >= 366)
    {
      result.tm_year++;
      t -= 366;
    }
    else if (leapYear == false && t >= 365)
    {
      result.tm_year++;
      t -= 365;
    }
    else
      break;
  }

  result.tm_yday = t;

  size_t mon;
  _LIBC_calc_month(&t, &mon);
  result.tm_mon = mon;
  result.tm_mday = t;

  // TODO int tm_wday;
  // TODO int tm_isdst;
  _LIBOS_WARNING("libc", "incomplete implementation");
  return &result;
}

struct tm *localtime(const time_t *timer)
{
  // TODO: probably not quite right, should consult the locale?
  _LIBOS_WARNING("libc", "incomplete implementation");
  return gmtime(timer);
}

size_t strftime(char * restrict s,
                size_t maxsize,
                const char * restrict format,
                const struct tm * restrict timeptr)
{
  size_t size = 0;
  while (*format != '\0')
  {
    if (*format != '%')
    {
      if (size == maxsize)
        return 0;
      *s++ = *format++;
      ++size;
      continue;
    }

    ++format;
    bool e_mod = false, o_mod = false;
    if (*format == 'E')
    {
      e_mod = true;
      ++format;
    }
    else if (*format == 'O')
    {
      o_mod = true;
      ++format;
    }

    // TODO: Use the E and O modifiers
    if (e_mod != 0 || o_mod != 0)
      _LIBOS_WARNING("libc", "O and E modifiers not implemented");

    switch (*format)
    {
      case 'H':
        size += 2;
        if (size > maxsize)
          return 0;
        *s++ = timeptr->tm_hour / 10 + '0';
        *s++ = timeptr->tm_hour % 10 + '0';
        break;

      case 'M':
        size += 2;
        if (size > maxsize)
          return 0;
        *s++ = timeptr->tm_min / 10 + '0';
        *s++ = timeptr->tm_min % 10 + '0';
        break;

      case 'S':
        size += 2;
        if (size > maxsize)
          return 0;
        *s++ = timeptr->tm_sec / 10 + '0';
        *s++ = timeptr->tm_sec % 10 + '0';
        break;

      case 'y':
        size += 2;
        if (size > maxsize)
          return 0;
        *s++ = (timeptr->tm_year / 10) % 10 + '0';
        *s++ = timeptr->tm_year % 10 + '0';
        break;
      case 'Y':
        size += 4;
        if (size > maxsize)
          return 0;
        *s++ = timeptr->tm_year / 1000 + '0';
        *s++ = (timeptr->tm_year / 100) % 10 + '0';
        *s++ = (timeptr->tm_year / 10) % 10 + '0';
        *s++ = timeptr->tm_year % 10 + '0';
        break;

      case 'C':
        size += 2;
        if (size > maxsize)
          return 0;
        *s++ = timeptr->tm_year / 1000 + '0';
        *s++ = (timeptr->tm_year / 100) % 10 + '0';
        break;

      case 'm':
        size += 2;
        if (size > maxsize)
          return 0;
        *s++ = timeptr->tm_mon / 10 + '0';
        *s++ = timeptr->tm_mon % 10 + '0';
        break;

      case 'd':
        size += 2;
        if (size > maxsize)
          return 0;
        *s++ = timeptr->tm_mday / 10 + '0';
        *s++ = timeptr->tm_mday % 10 + '0';
        break;

      default:
        _LIBOS_WARNING("libc", "unimplemented conversion specifier");
        abort();
    }
    ++format;
  }
  ++size;
  if (size > maxsize)
    return 0;
  *s = '\0';
  return size;
}
