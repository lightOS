/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <ctype.h>
#include <stdbool.h>

int isalnum(int c)
{
  if (isalpha(c) != false ||
      isdigit(c) != false)
    return true;
  return false;
}

/* TODO
  The isalpha function tests for any character for which isupper or islower is true,
  or any character that is one of a locale-speciﬁc set of alphabetic characters for which
  none of iscntrl, isdigit, ispunct, or isspace is true.168) In the "C" locale,
  isalpha returns true only for the characters for which isupper or islower is true.
*/
int isalpha(int c)
{
  if (isupper(c) != false ||
      islower(c) != false)
    return true;
  return false;
}

/* TODO
  The isblank function tests for any character that is a standard blank character or is one
  of a locale-speciﬁc set of characters for which isspace is true and that is used to
  separate words within a line of text. The standard blank characters are the following:
  space (' '), and horizontal tab ('\t'). In the "C" locale, isblank returns true only
  for the standard blank characters.
*/
int isblank(int c)
{
  if (c == ' ' || c == '\t')
    return true;
  return false;
}

int iscntrl(int c)
{
  if (c >= 0x00 && c <= 0x1F)
    return true;
  return false;
}

int isdigit(int c)
{
  if (c >= '0' && c <= '9')
    return true;
  return false;
}

int isgraph(int c)
{
  if (c == ' ')
    return false;
  return isprint(c);
}

/* TODO
  The islower function tests for any character that is a lowercase letter or is one of a
  locale-speciﬁc set of characters for which none of iscntrl, isdigit, ispunct, or
  isspace is true. In the "C" locale, islower returns true only for the lowercase
  letters (as deﬁned in 5.2.1).
*/
int islower(int c)
{
  if (c >= 'a' && c <= 'z')
    return true;
  return false;
}

int isprint(int c)
{
  if (c >= 0x20 && c <= 0x7E)
    return true;
  return false;
}

/* TODO
  The ispunct function tests for any printing character that is one of a locale-speciﬁc set
  of punctuation characters for which neither isspace nor isalnum is true. In the "C"
  locale, ispunct returns true for every printing character for which neither isspace
  nor isalnum is true.
*/
int ispunct(int c)
{
  if (isprint(c) != false &&
      isspace(c) == false &&
      isalnum(c) == false)
    return true;
  return false;
}

/* TODO
  The isspace function tests for any character that is a standard white-space character or
  is one of a locale-speciﬁc set of characters for which isalnum is false. The standard
  white-space characters are the following: space (' '), form feed ('\f'), new-line
  ('\n'), carriage return ('\r'), horizontal tab ('\t'), and vertical tab ('\v'). In the
  "C" locale, isspace returns true only for the standard white-space characters.
*/
int isspace(int c)
{
  if (c == ' ' || c == '\f' ||
    c == '\n' || c == '\r' ||
    c == '\t' || c == '\v')
    return true;
  return false;
}

/* TODO
  The isupper function tests for any character that is an uppercase letter or is one of a
  locale-speciﬁc set of characters for which none of iscntrl, isdigit, ispunct, or
  isspace is true. In the "C" locale, isupper returns true only for the uppercase
  letters (as deﬁned in 5.2.1).
*/
int isupper(int c)
{
  if (c >= 'A' && c <= 'Z')
    return true;
  return false;
}

int isxdigit(int c)
{
  if (isdigit(c) != false)return true;
  if ((c >= 'A' && c <= 'F') ||
      (c >= 'a' && c <= 'f'))
    return true;
  return false;
}

/* TODO
  If the argument is a character for which isupper is true and there are one or more
  corresponding characters, as speciﬁed by the current locale, for which islower is true,
  the tolower function returns one of the corresponding characters (always the same one
  for any given locale); otherwise, the argument is returned unchanged.
*/
int tolower(int c)
{
  if (isupper(c) != false)
    return (c + 0x20);
  return c;
}

/* TODO
  If the argument is a character for which islower is true and there are one or more
  corresponding characters, as speciﬁed by the current locale, for which isupper is true,
  the toupper function returns one of the corresponding characters (always the same one
  for any given locale); otherwise, the argument is returned unchanged.
*/
int toupper(int c)
{
  if (islower(c) != false)
    return (c - 0x20);
  return c;
}



#ifndef _LIGHTLIBC_NO_POSIX

  int isascii(int c)
  {
    if (c <= 0x80)
      return true;
    return false;
  }

  int toascii(int c)
  {
    return c & 0x7F;
  }

#endif
