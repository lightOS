/*
lightOS libunix
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <assert.h>
#include <stdio.h>
#include <libOS/LIBUNIX_glue.h>

// TODO
#include <libc/internal/stdio.h>

// Defined in libc/stdio.c
extern FILE *file_list;

FILE *_LIBUNIX_get_file(int fd)
{
	FILE *file = file_list;
	while (file != NULL)
	{
		if (file->des == fd)return file;
		file = file->next;
	}
	return NULL;
}
int _LIBUNIX_get_fd(FILE *File)
{
	return File->des;
}

int fileno(FILE *File)
{
	assert(File);
	
	return File->des;
}
