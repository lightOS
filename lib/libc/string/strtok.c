/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <string.h>

/* Taken from pdclib */

char *strtok(char * restrict s1,
             const char * restrict s2)
{
  static char *tmp = NULL;
  const char *p = s2;
  
  if (s1 != NULL)
  {
    /* new string */
    tmp = s1;
  }
  else
  {
    /* old string continued */
    if (tmp == NULL)
    {
      /* No old string, no new string, nothing to do */
      return NULL;
    }
    s1 = tmp;
    }
  
  /* skipping leading s2 characters */
  while (*p && *s1)
  {
    if (*s1 == *p)
    {
      /* found seperator; skip and start over */
      ++s1;
      p = s2;
      continue;
    }
    ++p;
  }
  
  if (! *s1)
  {
    /* no more to parse */
    return (tmp = NULL);
  }
  
  /* skipping non-s2 characters */
  tmp = s1;
  while (*tmp)
  {
    p = s2;
    while (*p)
    {
      if (*tmp == *p++)
      {
        /* found seperator; overwrite with '\0', position tmp, return */
        *tmp++ = '\0';
        return s1;
      }
    }
    ++tmp;
  }
  
  /* parsed to end of string */
  return (tmp = NULL);
}
