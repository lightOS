/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <limits.h>
#include <string.h>
#include <libc/internal.h>
#include <libOS/LIBC_glue.h>

int abs(int j)
{
  return (j < 0) ? -j : j;
}

long int labs(long int j)
{
  return (j < 0) ? -j : j;
}

long long int llabs(long long int j)
{
  return (j < 0) ? -j : j;
}

div_t div(int numer, int denom)
{
  div_t ret;
  ret.quot = numer / denom;
  ret.rem = numer % denom;
  return ret;
}

ldiv_t ldiv(long int numer, long int denom)
{
  ldiv_t ret;
  ret.quot = numer / denom;
  ret.rem = numer % denom;
  return ret;
}

lldiv_t lldiv(long long int numer, long long int denom)
{
  lldiv_t ret;
  ret.quot = numer / denom;
  ret.rem = numer % denom;
  return ret;
}

/*
 * 7.20.4 Communication with the environment
 */

void abort()
{
  _LIBC_fclose_all();
  _LIBOS_abort();
}

// NOTE: atexit in atexit.c

void exit(int status)
{
  _LIBC_do_atexit();
  _Exit(status);
}

void _Exit(int status)
{
  _LIBC_fclose_all();
  // TODO: Remove temporary files
  _LIBOS_exit(status);
}

// NOTE: getenv in env.c
