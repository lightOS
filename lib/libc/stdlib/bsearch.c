/*
lightOS libc
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdlib.h>

void *bsearch(const void *key,
              const void *base,
              size_t nmemb,
              size_t size,
              int (*compar)(const void *, const void *))
{
  size_t first = 0;
  size_t last  = size;

  while (last > (first + 1))
  {
    size_t mid = first + (last - first) / 2;

    void *elem = (void *)((const char *)base + mid * size);
    int res = compar(key, elem);
    if (res < 0)
      last = mid;
    else if (res > 0)
      first = mid;
    else
      return elem;
  }

  void *elem = (void *)((const char *)base + first * size);
  if (last != first && compar(key, elem) == 0)
    return elem;
  return NULL;
}
