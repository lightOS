/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <limits.h>
#include <string.h>
#include <libOS/LIBC_glue.h>

/* TODO
double atof(const char *nptr)
{
  return strtod(nptr, (char **)NULL);
}
*/

int atoi(const char *nptr)
{
  return (int)strtol(nptr, (char**)NULL, 10);
}

long int atol(const char *nptr)
{
  return strtol(nptr, (char**)NULL, 10);
}

long long int atoll(const char *nptr)
{
  return strtoll(nptr, (char**)NULL, 10);
}

long int strtol(const char *nptr,
                char **endptr,
                int base)
{
  const char *cur = nptr;

  // Skip whitespace
  while (isspace(*cur) != false)++cur;

  // Get sign
  bool sign = false;
  if (*cur == '-')
  {
    sign = true;
    ++cur;
  }
  else if (*cur == '+')
    ++cur;

  // Convert the string
  unsigned long int result = strtoul(cur, endptr, base);
  if (result >= LONG_MAX)
  {
    // TODO: Set errno to ERANGE
    if (sign == true)return LONG_MIN;
    else return LONG_MAX;
  }
  return (sign == true) ? -result : result;
}

long long int strtoll(const char *nptr,
                      char **endptr,
                      int base)
{
  const char *cur = nptr;

  // Skip whitespace
  while (isspace(*cur) != false)++cur;

  // Get sign
  bool sign = false;
  if (*cur == '-')
  {
    sign = true;
    ++cur;
  }
  else if (*cur == '+')
    ++cur;

  // Convert the string
  unsigned long long int result = strtoull(cur, endptr, base);
  if (result >= LLONG_MAX)
  {
    // TODO: Set errno to ERANGE
    if (sign == true)return LLONG_MIN;
    else return LLONG_MAX;
  }
  return (sign == true) ? -result : result;
}

unsigned long int strtoul(const char *nptr,
                          char **endptr,
                          int base)
{
  unsigned long long int result = strtoull(nptr, endptr, base);
  if (result >= LONG_MAX)
  {
    // TODO: Set errno to ERANGE
    return LONG_MAX;
  }
  return result;
}

unsigned long long int strtoull(const char *nptr,
                                char **endptr,
                                int base)
{
  const char *cur = nptr;

  // Skip whitespace
  while (isspace(*cur) != false)++cur;

  // Get the string length
  const char *end = cur;
  while (*end != '\0')
  {
    if ((*end >= '0' && *end <= '9' && (*end - '0') < base) ||
      (*end >= 'a' && *end <= 'z' && (*end - 'a') < (base - 10)) ||
      (*end >= 'A' && *end <= 'Z' && (*end - 'A') < (base - 10)))
      ++end;
    else break;
  }
  if (endptr != NULL)*endptr = (char*)end;
  if (end == cur)return 0;
  --end;

  // Convert to integer
  unsigned long long int multiply = 1;
  unsigned long long int result = 0;
  do
  {
    unsigned long long int c = 0;
    if (*end >= '0' && *end <= '9')c = *end - '0';
    else if (*end >= 'a' && *end <= 'z')c = *end - 'a' + 10;
    else if (*end >= 'A' && *end <= 'Z')c = *end - 'A' + 10;
    result += c * multiply;
    multiply *= base;
  } while (end-- != cur);

  return result;
}

int ltostr(long int val,
           char *nptr,
           char *endptr,
           int base)
{
  size_t add = 0;
  if (val < 0)
  {
    add = 1;
    if (nptr != endptr)
      *nptr++ = '-';
    val = -val;
  }
  return ultostr(val, nptr, endptr, base) + add;
}

int lltostr(long int val,
            char *nptr,
            char *endptr,
            int base)
{
  size_t add = 0;
  if (val < 0)
  {
    add = 1;
    if (nptr != endptr)
      *nptr++ = '-';
    val = -val;
  }
  return ulltostr(val, nptr, endptr, base) + add;
}

int ultostr(unsigned long val,
            char *nptr,
            char *endptr,
            int base)
{
  // TODO: How big is long?
  char tmp[33];
  size_t i = 0;

  do
  {
    size_t mod = val % base;
    if (mod < 10)tmp[i++] = mod + '0';
    else tmp[i++] = (mod - 10) + 'A';
  } while((val /= base) > 0);
  tmp[i] = '\0';

  size_t result = 0;
  for (;i > 0;i--)
  {
    if (nptr != endptr)
      *nptr++ = tmp[i-1];
    ++result;
  }
  if (nptr != endptr)
    *nptr = '\0';
  return result;
}

int ulltostr(unsigned long long val,
             char *nptr,
             char *endptr,
             int base)
{
  // TODO: How big is long long?
  char tmp[65];
  size_t i = 0;

  do
  {
    size_t mod = val % base;
    if (mod < 10)tmp[i++] = mod + '0';
    else tmp[i++] = (mod - 10) + 'A';
  } while((val /= base) > 0);
  tmp[i] = '\0';

  size_t result = 0;
  for (;i > 0;i--)
  {
    if (nptr != endptr)
      *nptr++ = tmp[i-1];
    ++result;
  }
  if (nptr != endptr)
    *nptr = '\0';
  return result;
}
