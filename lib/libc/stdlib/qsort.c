/*
lightOS libc
Copyright (C) 2007 Peter Stefanek

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdlib.h>
#include <string.h>

/* NOTE: For a more efficient implementation, the following optimizations are a
 * good thing (TM):
 *  -median-of-three algorithm to determine pivot element
 *  -using an alternate sorting algorithm for small partial arrays
 *   (insertionsort maybe)
 */
void quicksort( void *base, int l, int r, size_t esize,
                int ( *comp )( const void *, const void * ) )
{
  if( l < r )
  {
    int i = l;
    int j = r - 1;
    char *a = ( char * )base; /* bytes ftw! */
    char swapbuf[ esize ]; /* GNU C extension */

    while( 1 )
    {
      while( comp( a + i * esize, a + r * esize ) < 0 )          i ++;
      while( comp( a + j * esize, a + r * esize ) > 0 && j > i ) j --;
      if( j <= i ) break;

      memcpy( swapbuf, a + i * esize, esize );
      memcpy( a + i * esize, a + j * esize, esize );
      memcpy( a + j * esize, swapbuf, esize );
    }

    memcpy( swapbuf, a + i * esize, esize );
    memcpy( a + i * esize, a + r * esize, esize );
    memcpy( a + r * esize, swapbuf, esize );

    quicksort( a, l, i - 1, esize, comp );
    quicksort( a, i + 1, r, esize, comp );
  }
}

void qsort( void *base, size_t count, size_t esize,
            int ( *comp )( const void *, const void * ) )
{
  quicksort( base, 0, count - 1, esize, comp );
}
