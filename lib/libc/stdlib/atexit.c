/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stdlib.h>
#include <libc/internal.h>
#include <libOS/LIBC_glue.h>

/** Maximum number of functions to register with atexit */
#define MAX_ATEXIT 32

typedef void (*atexit_function)(void);

/** number of functions registered with atexit */
static size_t _LIBC_atexit_index = 0;
/** list of functions registered with atexit */
static atexit_function _LIBC_atexit_function[MAX_ATEXIT];

int atexit(void (*func)(void))
{
  if (_LIBC_atexit_index == MAX_ATEXIT)
  {
    _LIBOS_WARNING("libc", "limit exceeded");
    return -1;
  }

  _LIBC_atexit_function[_LIBC_atexit_index++] = func;
  return 0;
}

// NOTE: libc internal helper function

void _LIBC_do_atexit()
{
  while (_LIBC_atexit_index != 0)
    _LIBC_atexit_function[--_LIBC_atexit_index]();
}
