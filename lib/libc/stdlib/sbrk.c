/*
lightOS libc
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stddef.h>
#include <stdint.h>
#include <libOS/LIBC_glue.h>
#include <libarch/arch/type.h>

void *sbrk(ptrdiff_t increment)
{
  static uintptr_t heapend = 0;
  static uintptr_t allocend = 0;

  if ((heapend + increment) <= allocend)
  {
    void *tmp = (void*)heapend;
    heapend += increment;
    return tmp;
  }

  size_t size = increment - (allocend - heapend);
  size_t pages = size / _LIBARCH_PAGESIZE;
  if ((size % _LIBARCH_PAGESIZE) != 0)++pages;
  void *beg = _LIBOS_allocPage(pages);

  allocend = (uintptr_t)beg + pages * _LIBARCH_PAGESIZE;

  if (heapend == 0)
    heapend = (uintptr_t)beg;

  void *tmp = (void*)heapend;
  heapend = heapend + increment;
  return tmp;
}
