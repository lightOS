/*
lightOS libc
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <libOS/LIBC_glue.h>
#include <libc/LIBUNIX_glue.h>

// NOTE: internal helper function
static char **_LIBC_findenv(const char *envname);

char **environ = NULL;
size_t _LIBC_environ_count = 0;


char *getenv(const char *name)
{
  char **found = _LIBC_findenv(name);
  if (found == NULL)
    return NULL;

  return strchr(*found, '=') + 1;
}


// NOTE: setenv and unsetenv is Single UNIX Specification

int _LIBC_setenv(const char *envname,
                 const char *envval,
                 int overwrite)
{
  // Check for an occurance of '=' within the new environment
  // variable value
  // TODO: Should we set errno here?
  if (strchr(envval, '=') != NULL)
    return -1;

  size_t len1 = strlen(envname);
  size_t len2 = strlen(envval);

  // Go through all environment variables
  char **found = _LIBC_findenv(envname);
  if (found != NULL)
  {
    // Find the '='
    char *assign = strchr(*found, '=');
    ptrdiff_t dist = assign - *found;

    // Is it the environment variable name we search for?
    if (strncmp(*found, envname, dist) == 0)
    {
      // Are we supposed to change the value?
      if (overwrite == 0)
        return 0;

      // Allocate space
      char *newvar = realloc(*found, len1 + len2 + 2);
      if (newvar == NULL)
      {
        errno = ENOMEM;
        return -1;
      }
      *found = newvar;

      // Change the variable value
      strcpy(*found + dist + 1,
             envval);
      return 0;
    }
  }

  // The environment variable is not yet present
  // Allocate new space
  char **newenviron = malloc(sizeof(char*) * (_LIBC_environ_count + 2));
  if (newenviron == NULL)
  {
    errno = ENOMEM;
    return -1;
  }

  // Copy the old environ pointers
  memcpy(newenviron,
         environ,
         sizeof(char*) * (_LIBC_environ_count + 1));

  // Allocate space for the new environment variable
  char *newvar = malloc(len1 + len2 + 2);
  if (newvar == NULL)
  {
    free(newenviron);
    errno = ENOMEM;
    return -1;
  }

  // Copy the strings for the new environment variable
  strcpy(newvar, envname);
  newvar[len1] = '=';
  strcpy(newvar + len1 + 1, envval);

  // Set the new environment variable
  newenviron[_LIBC_environ_count++] = newvar;
  newenviron[_LIBC_environ_count] = NULL;

  free(environ);
  environ = newenviron;
  return 0;
}

int _LIBC_unsetenv(const char *name)
{
  char **found = _LIBC_findenv(name);
  if (found != NULL)
  {
    memcpy(found,
           found + 1,
           ((environ + _LIBC_environ_count) - found) * sizeof(char*));
    --_LIBC_environ_count;
  }
  return 0;
}


// NOTE: Internal helper functions

static char **_LIBC_findenv(const char *envname)
{
  int len = strlen(envname);

  char **curptr = environ;
  while (*curptr != NULL)
  {
    // Find the '='
    char *assign = strchr(*curptr, '=');
    ptrdiff_t dist = assign - *curptr;

    // Is it the environment variable name we search for?
    if (len == dist &&
        strncmp(*curptr, envname, dist) == 0)
      return curptr;

    ++curptr;
  }
  return NULL;
}

void _LIBC_init_environment()
{
  // Allocate enough space
  _LIBC_environ_count = _LIBOS_env_count();
  environ = malloc(sizeof(char*) * (_LIBC_environ_count + 1));

  if (environ != NULL)
  {
    for (size_t i = 0;i < _LIBC_environ_count;++i)
    {
      // Get the environment variable length
      size_t length = _LIBOS_get_env_length(i);
      environ[i] = malloc(length + 1);

      // Get the environment variable itself
      if (environ[i] != NULL)
        _LIBOS_get_env(i, environ[i]);
    }
    environ[_LIBC_environ_count] = NULL;
  }
}
