/*
lightOS libc
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>


#define UTF8_IS_1BYTE(c)               ((uint8_t)c & 0x80) == 0
#define UTF8_IS_2BYTE(c)               ((uint8_t)c & 0xE0) == 0xC0
#define UTF8_IS_3BYTE(c)               ((uint8_t)c & 0xF0) == 0xE0
#define UTF8_IS_4BYTE(c)               ((uint8_t)c & 0xF8) == 0xF0
#define UTF8_VALID_FOLLOWUP_BYTE(c)    ((uint8_t)c & 0xE0) == 0x80


int mblen(const char *s,
          size_t n)
{
  if (s == NULL ||
      n == 0 ||
      *s == '\0')
    return 0;

  if (UTF8_IS_1BYTE(*s))
    return 1;
  else if (UTF8_IS_2BYTE(*s) &&
           n >= 2)
  {
    if (UTF8_VALID_FOLLOWUP_BYTE(*(s + 1)))
      return 2;
  }
  else if (UTF8_IS_3BYTE(*s) &&
           n >= 3)
  {
    if (UTF8_VALID_FOLLOWUP_BYTE(*(s + 1)) &&
        UTF8_VALID_FOLLOWUP_BYTE(*(s + 2)))
      return 3;
  }
  else if (UTF8_IS_4BYTE(*s) &&
           n>= 4)
  {
    if (UTF8_VALID_FOLLOWUP_BYTE(*(s + 1)) &&
        UTF8_VALID_FOLLOWUP_BYTE(*(s + 2)) &&
        UTF8_VALID_FOLLOWUP_BYTE(*(s + 3)))
      return 4;
  }

  errno = EILSEQ;
  return -1;
}

int mbtowc(wchar_t * restrict pwc,
           const char * restrict s,
           size_t n)
{
  if (n >= 1 && *s == '\0' && pwc != NULL)
  {
    *pwc = L'\0';
    return 0;
  }

  int len = mblen(s, n);
  if (len == 0 ||
      len == -1)
    return len;

  if (pwc != NULL)
  {
    if (len == 1)
      *pwc = *s;
    else if (len == 2)
      *pwc = (((uint32_t)*s & 0x1F) << 6) |
              ((uint32_t)*(s + 1) & 0x3F);
    else if (len == 3)
      *pwc = (((uint32_t)*s & 0x0F) << 12) |
            (((uint32_t)*(s + 1) & 0x3F) << 6) |
              ((uint32_t)*(s + 2) & 0x3F);
    else if (len == 4)
      *pwc = (((uint32_t)*s & 0x0F) << 18) |
            (((uint32_t)*(s + 1) & 0x3F) << 12) |
            (((uint32_t)*(s + 2) & 0x3F) << 6) |
              ((uint32_t)*(s + 3) & 0x3F);
  }
  return len;
}

int wctomb(char *s,
           wchar_t wc)
{
  if (s == NULL)
    return 0;

  if (wc < 0x80)
  {
    *s = (char)wc;
    return 1;
  }
  else if (wc < 0x800)
  {
    *s = (((uint32_t)wc >> 6) & 0x1F) | 0xC0;
    *(s + 1) = ((uint32_t)wc & 0x3F) | 0x80;
    return 2;
  }
  else if (wc < 0x10000)
  {
    *s = (((uint32_t)wc >> 12) & 0x0F) | 0xE0;
    *(s + 1) = (((uint32_t)wc >> 6) & 0x3F) | 0x80;
    *(s + 2) = ((uint32_t)wc & 0x3F) | 0x80;
    return 3;
  }
  else if (wc < 0x200000)
  {
    *s = (((uint32_t)wc >> 18) & 0x07) | 0xF0;
    *(s + 2) = (((uint32_t)wc >> 12) & 0x3F) | 0x80;
    *(s + 2) = (((uint32_t)wc >> 6) & 0x3F) | 0x80;
    *(s + 3) = ((uint32_t)wc & 0x3F) | 0x80;
    return 4;
  }

  return -1;
}
