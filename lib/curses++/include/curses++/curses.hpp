/*
lightOS curses++
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_CURSESPP_CURSES_HPP
#define LIGHTOS_CURSESPP_CURSES_HPP


/*
 * Libarch includes
 */
#include <libarch/arch/console.h>

/*
 * Libkernel includes
 */
#include <libkernel/message.hpp>

/*
 * Libserver includes
 */
#include <libserver/keyboard.hpp>

/*
 * Standard C includes
 */
#include <cstdio>
#include <cstdint>

/*
 * Standard C++ includes
 */
#include <vector>



/** Namespace of the lightOS curses++ library */
namespace curses
{
  /** Colors */
  enum color
  {
    /** black */
    black         = _LIBARCH_CONSOLE_BLACK,
    /** blue */
    blue          = _LIBARCH_CONSOLE_BLUE,
    /** green */
    green         = _LIBARCH_CONSOLE_GREEN,
    /** cyan */
    cyan          = _LIBARCH_CONSOLE_CYAN,
    /** red */
    red           = _LIBARCH_CONSOLE_RED,
    /** magenta */
    magenta       = _LIBARCH_CONSOLE_MAGENTA,
    /** brown */
    brown         = _LIBARCH_CONSOLE_BROWN,
    /** light gray */
    light_gray    = _LIBARCH_CONSOLE_LIGHT_GRAY,
    /** dark gray */
    dark_gray     = _LIBARCH_CONSOLE_DARK_GRAY,
    /** light blue */
    light_blue    = _LIBARCH_CONSOLE_LIGHT_BLUE,
    /** light green */
    light_green   = _LIBARCH_CONSOLE_LIGHT_GREEN,
    /** light cyan */
    light_cyan    = _LIBARCH_CONSOLE_LIGHT_CYAN,
    /** light red */
    light_red     = _LIBARCH_CONSOLE_LIGHT_RED,
    /** light magenta */
    light_magenta = _LIBARCH_CONSOLE_LIGHT_MAGENTA,
    /** yellow */
    yellow        = _LIBARCH_CONSOLE_YELLOW,
    /** white */
    white         = _LIBARCH_CONSOLE_WHITE
  };

  /** Type for a key */
  typedef libserver::key_t key_t;
  /** Type for a keycode */
  typedef libserver::keycode keycode;

  /** A textmode window */
  class window
  {
    /** screen needs access to the constructor, destructor, m_data and m_changed */
    friend class screen;

    public:
      /** No copy-constructor */
      window(const window&) = delete;

      /** No assignment operator */
      window& operator = (const window&) = delete;

      /** Get the window width in characters
       *\return the window width in characters */
      inline size_t width() const;

      /** Get the window height in characters
       *\return the window height in characters */
      inline size_t height() const;

      /** Get the window x-coordinate
       *\return the window x coordinate */
      inline size_t x() const;

      /** Get the window y-coordinate
       *\return the window y-coordinate */
      inline size_t y() const;

      /** Change the window visibility
       *\param[in] bVisible true, if the window should be visible, false otherwise */
      inline void visible(bool bVisible);

      /** Get the window visibility
       *\return true, if the window is visible, false otherwise */
      inline bool visible() const;

      /** Move the window to the new x- and y-coordinate
       *\param[in] x the new x-coordinate
       *\param[in] y the new y-coordinate */
      void move(size_t x,
                size_t y);

      /** Resize the window
       *\note also sets a new background and foreground color and deletes
       *      all characters
       *\param[in] width      the new width
       *\param[in] height     the new height
       *\param[in] background the new background color
       *\param[in] foreground the new text color */
      void resize(size_t width,
                  size_t height,
                  color background = curses::color::black,
                  color foreground = curses::color::white);

      /** Print a character onto the window
       *\param[in] x          the x-coordinate
       *\param[in] y          the y-coordinate
       *\param[in] c          the ASCII character
       *\param[in] col        the text color
       *\param[in] background the background color */
      void character(size_t x,
                     size_t y,
                     char c,
                     color col,
                     color background);

      /** Print a character onto the window, leaving the background color
       *  unchanged
       *\param[in] x          the x-coordinate
       *\param[in] y          the y-coordinate
       *\param[in] c          the ASCII character
       *\param[in] col        the text color */
      void character(size_t x,
                     size_t y,
                     char c,
                     color col);

      /** Print a character onto the window, leaving the background and
       *  text color unchanged
       *\param[in] x          the x-coordinate
       *\param[in] y          the y-coordinate
       *\param[in] c          the ASCII character
       *\param[in] col        the foreground color */
      void character(size_t x,
                     size_t y,
                     char c);

      /** Print a character sequence onto the window
       *\param[in] x          the x-coordinate
       *\param[in] y          the y-coordinate
       *\param[in] s          the character sequence
       *\param[in] col        the text color
       *\param[in] background the background color */
      void string(size_t x,
                  size_t y,
                  const char* s,
                  color col,
                  color background);

      /** Print a character sequence onto the window, leaving the background
       *  color unchanged
       *\param[in] x          the x-coordinate
       *\param[in] y          the y-coordinate
       *\param[in] s          the character sequence
       *\param[in] col        the text color */
      void string(size_t x,
                  size_t y,
                  const char* s,
                  color col);

      /** Print a character sequence onto the window, leaving the background
       *  and the text color unchanged
       *\param[in] x          the x-coordinate
       *\param[in] y          the y-coordinate
       *\param[in] s          the character sequence */
      void string(size_t x,
                  size_t y,
                  const char* s);

      /** Print a character sequence onto the window
       *\param[in] x          the x-coordinate
       *\param[in] y          the y-coordinate
       *\param[in] max        the maximum number of characters to copy
       *\param[in] s          the character sequence
       *\param[in] col        the text color
       *\param[in] background the background color
       *\return the number of characters copied */
      size_t string(size_t x,
                    size_t y,
                    size_t max,
                    const char* s,
                    color col,
                    color background);

      /** Print a character sequence onto the window, leaving the background
       *  color unchanged
       *\param[in] x          the x-coordinate
       *\param[in] y          the y-coordinate
       *\param[in] max        the maximum number of characters to copy
       *\param[in] s          the character sequence
       *\param[in] col        the text color
       *\return the number of characters copied */
      size_t string(size_t x,
                    size_t y,
                    size_t max,
                    const char* s,
                    color col);

      /** Print a character sequence onto the window, leaving the background
       *  and the text color unchanged
       *\param[in] x          the x-coordinate
       *\param[in] y          the y-coordinate
       *\param[in] max        the maximum number of characters to copy
       *\param[in] s          the character sequence
       *\return the number of characters copied */
      size_t string(size_t x,
                    size_t y,
                    size_t max,
                    const char* s);

      /** Clear the window with a specific background and text color
       *\param[in] background the background color
       *\param[in] foreground the text color */
      void clear(color background = curses::color::black,
                 color foreground = curses::color::white);

      /** Clear a specific region of the window with a specific background
       *  and text color
       *\param[in] x          x-coordinate of the beginning of the region
       *\param[in] y          y-coordinate of the beginning of the region
       *\param[in] count      number of characters to clear
       *\param[in] background the background color
       *\param[in] foregorund the text color */
      void clear(size_t x,
                 size_t y,
                 size_t count,
                 color background = curses::color::black,
                 color foreground = curses::color::white);

    private:
      /** The constructor
       *\note only curses::screen is supposed to call this constructor
       *\param[in] x      the x-coordinate of the window
       *\param[in] y      the y-coordinate of the window
       *\param[in] width  the width of the window
       *\param[in] height the height of the window */
      window(size_t x,
             size_t y,
             size_t width,
             size_t height);

      /** The destructor
       *\note only curses::screen is supposed to call the destructor */
      ~window();


      /** The x-coordinate of the window */
      size_t   m_x;
      /** The y-coordinate of the window */
      size_t   m_y;
      /** The width of the window */
      size_t   m_width;
      /** The height of the window */
      size_t   m_height;

      /** Pointer to the window data */
      uint8_t* m_data;
      /** Did the window change? */
      bool     m_changed;
      /** Is the window visible? */
      bool     m_visible;
  };

  /** The textmode screen */
  class screen
  {
    public:
      /** The constructor
       *\param[in] bcursor enable/disable the cursor
       *\param[in] ofile   the output file handle
       *\param[in] ifile   the input file handle */
      screen(bool bcursor = true,
             FILE* ofile = stdout,
             FILE* ifile = stdin);

      /** No copy-constructor */
      screen(const screen&) = delete;
      /** No assignment operator */
      screen& operator = (const screen&) = delete;
      /** The destructor */
      ~screen();

      /** Get the width of the screen in characters
       *\return the width of the screen in characters */
      inline size_t width() const;

      /** Get the height of the screen in characters
       *\return the height of the screen in characters */
      inline size_t height() const;

      /** Create a window
       *\param[in] x the x-coordinate of the window in characters
       *\param[in] y the y-coordinate of the window in characters
       *\param[in] width the width of the window in characters
       *\param[in] height the height of the window in characters */
      window& create_window(size_t x,
                            size_t y,
                            size_t width,
                            size_t height);

      /** Enable/disable the cursor
       *\param[in] b_enable true for enabling, false for disabling */
      void enable_cursor(bool b_enable);

      /** Set the cursors position
       *\param[in] x x-coordinate of the cursor
       *\param[in] y y-coordinate of the cursor */
      void cursor(size_t x,
                  size_t y);

      /** Update the screen, writes all changes to the actual screen */
      void update();

      /** Get a key press
       *\return the pressed key */
      curses::key_t get_key();

    private:
      /** The output file handle */
      FILE*                    m_ofile;
      /** The input file handle */
      FILE*                    m_ifile;
      /** The width of the screen in characters */
      size_t                   m_width;
      /** The height of the screen in characters */
      size_t                   m_height;
      /** The current cursor position, x-coordinate */
      size_t                   m_cursor_x;
      /** The current cursor position, y-coordinate */
      size_t                   m_cursor_y;
      /** Is the cursor enabled? */
      bool                     m_bCursor;

      /** The screen data */
      libkernel::shared_memory m_data;

      /** The window list */
      std::vector<window*>     m_windows;
  };



  /*
   * Part of the implementation of window
   */

  size_t window::width() const
  {
    return m_width;
  }

  size_t window::height() const
  {
    return m_height;
  }

  size_t window::x() const
  {
    return m_x;
  }

  size_t window::y() const
  {
    return m_y;
  }

  void window::visible(bool bVisible)
  {
    m_visible = bVisible;
    m_changed = true;
  }

  bool window::visible() const
  {
    return m_visible;
  }



  /*
   * Part of the implementation of screen
   */

  size_t screen::width() const
  {
    return m_width;
  }

  size_t screen::height() const
  {
    return m_height;
  }
}

#endif
