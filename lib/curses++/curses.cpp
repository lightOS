/*
lightOS curses++
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstring>
#include <cassert>
#include <curses++/curses.hpp>
#include <libc/internal/stdio.h>       // NOTE: We have to peek into the FILE structure
#include <libserver/console.h>
using curses::window;
using curses::screen;


/*
 * Implementation of window
 */

window::window(size_t x,
               size_t y,
               size_t width,
               size_t height)
  : m_x(x), m_y(y), m_width(width), m_height(height), m_data(new uint8_t[2 * width * height]),
    m_changed(true), m_visible(true)
{
  clear();
}

window::~window()
{
  delete []m_data;
}

void window::move(size_t x,
                  size_t y)
{
  m_x = x;
  m_y = y;

  m_changed = true;
}

void window::resize(size_t width,
                    size_t height,
                    color background,
                    color foreground)
{
  m_width = width;
  m_height = height;
  m_changed = true;

  delete []m_data;
  m_data = new uint8_t[2 * width * height];

  clear(background, foreground);
}

void window::character(size_t x,
                       size_t y,
                       char c,
                       color col,
                       color background)
{
  assert(x < m_width);
  assert(y < m_height);

  size_t index = 2 * (y * m_width + x);
  m_data[index] = c;
  m_data[index + 1] = (background << 4) | col;
  m_changed = true;
}

void window::character(size_t x,
                       size_t y,
                       char c,
                       color col)
{
  assert(x < m_width);
  assert(y < m_height);

  size_t index = 2 * (y * m_width + x);
  m_data[index] = c;
  m_data[index + 1] = (m_data[index + 1] & 0xF0) | col;
  m_changed = true;
}

void window::character(size_t x,
                       size_t y,
                       char c)
{
  assert(x < m_width);
  assert(y < m_height);

  size_t index = 2 * (y * m_width + x);
  m_data[index] = c;
  m_changed = true;
}

void window::string(size_t x,
                    size_t y,
                    const char* s,
                    color col,
                    color background)
{
  size_t index = 2 * (y * m_width + x);
  while (*s != '\0')
  {
    m_data[index++] = *s++;
    m_data[index++] = (background << 4) | col;
  }
  m_changed = true;
}

void window::string(size_t x,
                    size_t y,
                    const char* s,
                    color col)
{
  size_t index = 2 * (y * m_width + x);
  while (*s != '\0')
  {
    m_data[index++] = *s++;
    m_data[index] = (m_data[index] & 0xF0) | col;
    ++index;
  }
  m_changed = true;
}

void window::string(size_t x,
                    size_t y,
                    const char* s)
{
  size_t index = 2 * (y * m_width + x);
  while (*s != '\0')
  {
    m_data[index++] = *s++;
    ++index;
  }
  m_changed = true;
}

size_t window::string(size_t x,
                      size_t y,
                      size_t max,
                      const char* s,
                      color col,
                      color background)
{
  size_t copied = 0;
  size_t index = 2 * (y * m_width + x);
  while (*s != '\0' && copied < max)
  {
    m_data[index++] = *s++;
    m_data[index++] = (background << 4) | col;
    ++copied;
  }
  m_changed = true;
  return copied;
}

size_t window::string(size_t x,
                      size_t y,
                      size_t max,
                      const char* s,
                      color col)
{
  size_t copied = 0;
  size_t index = 2 * (y * m_width + x);
  while (*s != '\0' && copied < max)
  {
    m_data[index++] = *s++;
    m_data[index] = (m_data[index] & 0xF0) | col;
    ++index;
    ++copied;
  }
  m_changed = true;
  return copied;
}

size_t window::string(size_t x,
                      size_t y,
                      size_t max,
                      const char* s)
{
  size_t copied = 0;
  size_t index = 2 * (y * m_width + x);
  while (*s != '\0' && copied < max)
  {
    m_data[index++] = *s++;
    ++index;
    ++copied;
  }
  m_changed = true;
  return copied;
}

void window::clear(color background,
                   color foreground)
{
  size_t index = 0;
  while (index < 2 * m_width * m_height)
  {
    m_data[index++] = '\0';
    m_data[index++] = (background << 4) | foreground;
  }
  m_changed = true;
}

void window::clear(size_t x,
                   size_t y,
                   size_t count,
                   color background,
                   color foreground)
{
  assert((x + count) <= m_width);
  assert(y < m_height);

  size_t base = 2 * (y * m_width + x);
  size_t index = base;
  while (index < (base  + 2 * count))
  {
    m_data[index++] = '\0';
    m_data[index++] = (background << 4) | foreground;
  }
  m_changed = true;
}



/*
 * Implemenation of screen
 */

// HACK: screen width and height hardcoded here
screen::screen(bool bcursor,
               FILE* ofile,
               FILE* ifile)
  : m_ofile(ofile), m_ifile(ifile), m_width(80), m_height(25), m_cursor_x(0), m_cursor_y(0),
    m_bCursor(bcursor), m_data(), m_windows()
{
  // Disable 'hardware' echoing
  _LIBSERVER_CONSOLE_HW_ECHO(m_ofile, false);

  // Clear the screen
  _LIBSERVER_CONSOLE_CLEAR(m_ofile);

  // allocate m_data
  libkernel::shared_memory tmp_shm(2 * m_width * m_height);
  m_data = tmp_shm;

  // Initialize
  memset(m_data.address<void>(),
         0,
         2 * m_width * m_height);

  enable_cursor(bcursor);
}

screen::~screen()
{
  // Free the windows
  while (m_windows.size() != 0)
  {
    window* Window = m_windows.back();
    m_windows.pop_back();
    delete Window;
  }

  enable_cursor(true);

  // Enable 'hardware' echoing
  _LIBSERVER_CONSOLE_HW_ECHO(m_ofile, true);

  // Clear the screen
  _LIBSERVER_CONSOLE_CLEAR(m_ofile);
}

window& screen::create_window(size_t x,
                              size_t y,
                              size_t width,
                              size_t height)
{
  assert((x + width) <= m_width);
  assert((y + height) <= m_height);

  window* new_window = new window(x, y, width, height);
  m_windows.push_back(new_window);
  return *new_window;
}

void screen::enable_cursor(bool b_enable)
{
  m_bCursor = b_enable;

  _LIBSERVER_CONSOLE_CURSOR(m_ofile, m_bCursor);
}

void screen::cursor(size_t x,
                    size_t y)
{
  assert(x < m_width);
  assert(y < m_height);

  m_cursor_x = x;
  m_cursor_y = y;

  _LIBSERVER_CONSOLE_SET_CURSOR(m_ofile, m_cursor_x, m_cursor_y);
}

void screen::update()
{
  bool need_update = false;

  // Go through all our windows and update the shared-memory region
  auto end = m_windows.end();
  for (auto i = m_windows.begin();i != end;++i)
    if ((*i)->m_changed && (*i)->m_visible)
    {
      // Copy the window to the screen's buffer
      for (size_t row = 0;row < (*i)->m_height;++row)
        memcpy(m_data.address<uint8_t>() + 2 * (((*i)->m_y + row) * m_width + (*i)->m_x),
               (*i)->m_data + 2 * row * (*i)->width(),
               2 * (*i)->width());

      (*i)->m_changed = false;
      need_update = true;
    }

  // If the shared-memory region got changed, send update!
  if (need_update)
  {
    // TODO: Not good, repetition of libunix/curses
    libkernel::message request(m_ofile->osdep.fsport,
                               libkernel::message::fs_write_file,
                               0,
                               m_data,
                               libkernel::shared_memory::read_only);
    libkernel::message_port Port(m_ofile->osdep.port, false);
    Port.send(request);
    libkernel::message reply(m_ofile->osdep.fsport,
                             libkernel::message::fs_write_file);
    Port.wait(reply);
    Port.release();
  }
}

curses::key_t screen::get_key()
{
  key_t Key;
  int result;
  char tmp[10];
  size_t length = 0;
  do
  {
    int character = fgetc(m_ifile);

    // TODO: throw if EOF

    tmp[length++] = (char)character;

    result = _LIBSERVER_convert_from_ecma48(tmp, length, &Key);

    // TODO: throw if _LIBSERVER_CONVERT_FAILED

    if (result == _LIBSERVER_CONVERT_FAILED)
      return get_key();
  } while (result == _LIBSERVER_CONVERT_NEED_INPUT);

  return Key;
}
