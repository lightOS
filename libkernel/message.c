/*
lightOS libkernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libkernel/message_noprefix.h>

message_t _LIBKERNEL_create_message(port_id_t port,
                                    message_param_t type,
                                    message_param_t param1,
                                    message_param_t param2,
                                    message_param_t param3)
{
  message_t msg;
  msg.port = port;
  msg.type = type;
  msg.param1 = param1;
  msg.param2 = param2;
  msg.param3 = param3;
  return msg;
}

message_t _LIBKERNEL_create_message_shm(port_id_t port,
                                        message_param_t type,
                                        message_param_t param1,
                                        shared_memory_t *shm,
                                        syscall_param_t flags)
{
  message_t msg;
  msg.port = port;
  msg.type = _LIBKERNEL_MAKE_MSG_TYPE(flags, type);
  msg.param1 = param1;
  msg.param2 = shm->id;
  msg.param3 = 0;
  return msg;
}

syscall_param_t _LIBKERNEL_get_message_type(const message_t *msg)
{
  return _LIBKERNEL_GET_MSG_TYPE(msg);
}

void _LIBKERNEL_set_message_type(message_t *msg,
                                 message_param_t type)
{
  msg->type = _LIBKERNEL_MAKE_MSG_TYPE(_LIBKERNEL_GET_MSG_FLAGS(msg), type);
}

syscall_param_t _LIBKERNEL_get_message_attribute(const message_t *msg)
{
  return _LIBKERNEL_GET_MSG_FLAGS(msg);
}

void _LIBKERNEL_set_message_attribute(message_t *msg,
                                      message_param_t attr){
  msg->type = _LIBKERNEL_MAKE_MSG_TYPE(attr, _LIBKERNEL_GET_MSG_TYPE(msg));
}

shared_memory_t _LIBKERNEL_get_shared_memory(const message_t *msg)
{
  shared_memory_t shm;
  if (_LIBKERNEL_get_message_attribute(msg) == SHM_NONE)
  {
    shm.id = 0;
    shm.address = 0;
    shm.size = 0;
  }
  else
  {
    shm.id = msg->param2;
    shm.size = _LIBKERNEL_GET_SHM_SIZE(msg);
    shm.address = _LIBKERNEL_GET_SHM_ADDRESS(msg);
  }
  return shm;
}
