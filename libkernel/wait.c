/*
lightOS libkernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libkernel/wait.h>
#include <libkernel/kernel.h>
#include <libkernel/event_noprefix.h>
#include <libkernel/message_noprefix.h>

void _LIBKERNEL_wait(syscall_param_t ms)
{
  // TODO: put events in a separate kernel header
  port_id_t port = _LIBKERNEL_get_thread_id();
  _LIBKERNEL_register_event(port,
                            EVENT_TIMER,
                            ms,
                            0);
  message_t msg = _LIBKERNEL_create_message(PORT_KERNEL,
                                            MSG_EVENT,
                                            EVENT_TIMER,
                                            0,
                                            0);
  _LIBKERNEL_add_wait_message(port, &msg);
  _LIBKERNEL_wait_message(&msg);
}
