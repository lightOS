/*
lightOS libkernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libkernel/kernel.h>
#include <libkernel/syscall.h>
#include <libkernel/arch/syscall.h>

#include <libkernel/type_noprefix.h>

/*! External functions
 *\note Defined in libkernel/$arch/syscall.S */
extern void _LIBKERNEL_thread_entry();

void _LIBKERNEL_kernel_version(syscall_param_t *major,
                               syscall_param_t *minor)
{
  _LIBKERNEL_SYSCALL0_2(_LIBKERNEL_SYSCALL_GET_KERNEL_VERSION,
                        *major,
                        *minor);
}

void *_LIBKERNEL_extend_heap(syscall_param_t size)
{
  void *address;
  _LIBKERNEL_SYSCALL1_1(_LIBKERNEL_SYSCALL_HEAPALLOC,
                        size,
                        address);
  return address;
}

void _LIBKERNEL_get_memory_info(syscall_param_t *total,
                                syscall_param_t *free)
{
  _LIBKERNEL_SYSCALL0_2(_LIBKERNEL_SYSCALL_GET_MEMORY_INFO,
                        *total,
                        *free);
}

_LIBKERNEL_syscall_param_t _LIBKERNEL_get_commandline_length()
{
  _LIBKERNEL_syscall_param_t length;
  _LIBKERNEL_SYSCALL1_1(_LIBKERNEL_SYSCALL_GET_COMMANDLINE,
                        0,
                        length);
  return length;
}

void _LIBKERNEL_get_commandline(char *cmdline)
{
  _LIBKERNEL_SYSCALL1_0(_LIBKERNEL_SYSCALL_GET_COMMANDLINE,
                        cmdline);
}

void _LIBKERNEL_get_time(syscall_param_t *hour,
                         syscall_param_t *minute,
                         syscall_param_t *second)
{
  _LIBKERNEL_SYSCALL0_3(_LIBKERNEL_SYSCALL_GET_TIME,
                        *second,
                        *minute,
                        *hour);
}

void _LIBKERNEL_get_date(syscall_param_t *dayofweek,
                         syscall_param_t *dayofmonth,
                         syscall_param_t *month,
                         syscall_param_t *year,
                         syscall_param_t *summertime)
{
  _LIBKERNEL_SYSCALL0_5(_LIBKERNEL_SYSCALL_GET_DATE,
                        *dayofweek,
                        *dayofmonth,
                        *month,
                        *year,
                        *summertime);
}

//
// IPC
//

port_id_t _LIBKERNEL_create_port()
{
  port_id_t id;
  _LIBKERNEL_SYSCALL0_1(_LIBKERNEL_SYSCALL_CREATE_PORT,
                        id);
  return id;
}

void _LIBKERNEL_destroy_port(port_id_t id)
{
  _LIBKERNEL_SYSCALL1_0(_LIBKERNEL_SYSCALL_DESTROY_PORT,
                        id);
}

port_id_t _LIBKERNEL_create_specific_port(port_id_t id)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL1_1(_LIBKERNEL_SYSCALL_CREATE_SPECIFIC_PORT,
                        id,
                        result);
  if (result == 0)
    return 0;
  return id;
}

syscall_param_t _LIBKERNEL_peek_message(port_id_t id,
                                        message_t *msg)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL1_6(_LIBKERNEL_SYSCALL_PEEK_MESSAGE,
                        id,
                        result,
                        msg->port,
                        msg->type,
                        msg->param1,
                        msg->param2,
                        msg->param3);
  return result;
}

syscall_param_t _LIBKERNEL_get_message(port_id_t id,
                                       message_t *msg)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL1_6(_LIBKERNEL_SYSCALL_GET_MESSAGE,
                        id,
                        result,
                        msg->port,
                        msg->type,
                        msg->param1,
                        msg->param2,
                        msg->param3);
  return (result == id);
}

port_id_t _LIBKERNEL_wait_message(message_t *msg)
{
  _LIBKERNEL_port_id_t result;
  #ifdef X86
    result = _LIBKERNEL_syscall_wait(_LIBKERNEL_SYSCALL_WAIT_MESSAGE, msg);
  #endif
  #ifdef X86_64
    _LIBKERNEL_SYSCALL0_6(_LIBKERNEL_SYSCALL_WAIT_MESSAGE,
                          result,
                          msg->port,
                          msg->type,
                          msg->param1,
                          msg->param2,
                          msg->param3);
  #endif
  return result;
}

syscall_param_t _LIBKERNEL_transfer_port(port_id_t port,
                                         process_id_t pid)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL2_1(_LIBKERNEL_SYSCALL_TRANSFER_PORT,
                        port,
                        pid,
                        result);
  return result;
}

syscall_param_t _LIBKERNEL_add_wait_message(port_id_t id,
                                            const message_t *msg)
{
  syscall_param_t result;
  #ifdef X86
    result = _LIBKERNEL_syscall_add_wait(_LIBKERNEL_SYSCALL_ADD_WAIT_MESSAGE,
                                         id,
                                         msg);
  #endif
  #ifdef X86_64
    _LIBKERNEL_SYSCALL6_1(_LIBKERNEL_SYSCALL_ADD_WAIT_MESSAGE,
                          id,
                          msg->port,
                          msg->type,
                          msg->param1,
                          msg->param2,
                          msg->param3,
                          result);
  #endif
  return result;
}

syscall_param_t _LIBKERNEL_send_message(port_id_t id,
                                        const message_t *msg)
{
  syscall_param_t result;
  #ifdef X86
    result = _LIBKERNEL_syscall_send(_LIBKERNEL_SYSCALL_SEND_MESSAGE,
                                     id,
                                     msg->port,
                                     msg->type,
                                     msg->param1,
                                     msg->param2,
                                     msg->param3);
  #endif
  #ifdef X86_64
    _LIBKERNEL_SYSCALL6_1(_LIBKERNEL_SYSCALL_SEND_MESSAGE,
                          id,
                          msg->port,
                          msg->type,
                          msg->param1,
                          msg->param2,
                          msg->param3,
                          result);
  #endif
  return result;
}

void _LIBKERNEL_get_port_info(port_id_t id,
                              process_id_t *owner,
                              thread_id_t *waiter)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL1_3(_LIBKERNEL_SYSCALL_PORT_INFO,
                        id,
                        result,
                        *owner,
                        *waiter);
}

syscall_param_t _LIBKERNEL_set_standard_port(process_id_t pid,
                                             port_id_t stdPort,
                                             port_id_t Port,
                                             port_id_t fsPort)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL5_1(_LIBKERNEL_SYSCALL_STANDARD_PORT,
                        pid,
                        0,
                        stdPort,
                        Port,
                        fsPort,
                        result);
  return result;
}

void _LIBKERNEL_get_standard_port(process_id_t pid,
                                  port_id_t stdPort,
                                  port_id_t *Port,
                                  port_id_t *fsPort)
{
  _LIBKERNEL_SYSCALL3_2(_LIBKERNEL_SYSCALL_STANDARD_PORT,
                        pid,
                        1,
                        stdPort,
                        *Port,
                        *fsPort);
}

syscall_param_t _LIBKERNEL_register_event(port_id_t id,
                                          syscall_param_t type,
                                          syscall_param_t param1,
                                          syscall_param_t param2)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL4_1(_LIBKERNEL_SYSCALL_REGISTER_EVENT,
                        id,
                        type,
                        param1,
                        param2,
                        result);
  return result;
}

void _LIBKERNEL_acknoledge_event(port_id_t id,
                                 syscall_param_t type,
                                 syscall_param_t param)
{
  _LIBKERNEL_SYSCALL3_0(_LIBKERNEL_SYSCALL_ACKNOLEDGE_EVENT,
                        id,
                        type,
                        param);
}

syscall_param_t _LIBKERNEL_unregister_event(port_id_t id,
                                            syscall_param_t type)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL2_1(_LIBKERNEL_SYSCALL_UNREGISTER_EVENT,
                        id,
                        type,
                        result);
  return result;
}

syscall_param_t _LIBKERNEL_get_tick_count()
{
  syscall_param_t count;
  _LIBKERNEL_SYSCALL0_1(_LIBKERNEL_SYSCALL_GET_TICK_COUNT,
                        count);
  return count;
}

shared_memory_t _LIBKERNEL_create_shared_memory(syscall_param_t size)
{
  shared_memory_t shm;
  shm.size = size;
  _LIBKERNEL_SYSCALL1_2(_LIBKERNEL_SYSCALL_CREATE_SHARED_MEMORY,
                        size,
                        shm.id,
                        shm.address);
  return shm;
}

void _LIBKERNEL_destroy_shared_memory(shared_memory_t *shm)
{
  if (shm->id != 0)
  {
    _LIBKERNEL_SYSCALL1_0(_LIBKERNEL_SYSCALL_DESTROY_SHARED_MEMORY,
                          shm->id);
  }
  shm->id = 0;
  shm->address = 0;
  shm->size = 0;
}

thread_id_t _LIBKERNEL_create_thread(void (*thread_entry)(_LIBKERNEL_syscall_param_t),
                                     syscall_param_t parameter,
                                     syscall_param_t flags)
{
  thread_id_t id;
  _LIBKERNEL_SYSCALL4_1(_LIBKERNEL_SYSCALL_CREATE_THREAD,
                        _LIBKERNEL_thread_entry,
                        thread_entry,
                        parameter,
                        flags,
                        id);
  return id;
}

syscall_param_t _LIBKERNEL_suspend_thread(thread_id_t id)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL1_1(_LIBKERNEL_SYSCALL_SUSPEND_THREAD,
                        id,
                        result);
  return result;
}

syscall_param_t _LIBKERNEL_resume_thread(thread_id_t id)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL1_1(_LIBKERNEL_SYSCALL_RESUME_THREAD,
                        id,
                        result);
  return result;
}

syscall_param_t _LIBKERNEL_destroy_thread(thread_id_t id)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL1_1(_LIBKERNEL_SYSCALL_DESTROY_THREAD,
                        id,
                        result);
  return result;
}

syscall_param_t _LIBKERNEL_destroy_process(process_id_t id)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL1_1(_LIBKERNEL_SYSCALL_DESTROY_PROCESS,
                        id,
                        result);
  return result;
}

process_id_t _LIBKERNEL_get_process_id()
{
  process_id_t id;
  _LIBKERNEL_SYSCALL0_1(_LIBKERNEL_SYSCALL_GET_PROCESSID,
                        id);
  return id;
}

thread_id_t _LIBKERNEL_get_thread_id()
{
  thread_id_t id;
  _LIBKERNEL_SYSCALL0_1(_LIBKERNEL_SYSCALL_GET_THREADID,
                        id);
  return id;
}

syscall_param_t _LIBKERNEL_get_process_count()
{
  syscall_param_t count;
  _LIBKERNEL_SYSCALL0_1(_LIBKERNEL_SYSCALL_GET_PROCESS_COUNT,
                        count);
  return count;
}

process_id_t _LIBKERNEL_enum_processes(syscall_param_t i)
{
  process_id_t id;
  _LIBKERNEL_SYSCALL1_1(_LIBKERNEL_SYSCALL_GET_PROCESS_LIST,
                        i,
                        id);
  return id;
}

syscall_param_t _LIBKERNEL_get_process_name_length(process_id_t id)
{
  syscall_param_t length;
  _LIBKERNEL_SYSCALL3_1(_LIBKERNEL_SYSCALL_GET_PROCESS_INFO,
                        id,
                        0,
                        0,
                        length);
  return length;
}

void _LIBKERNEL_get_process_name(process_id_t id,
                                 char *name)
{
  _LIBKERNEL_SYSCALL3_0(_LIBKERNEL_SYSCALL_GET_PROCESS_INFO,
                        id,
                        0,
                        name);
}

syscall_param_t _LIBKERNEL_get_process_memory_usage(process_id_t id)
{
  syscall_param_t mem_usage;
  _LIBKERNEL_SYSCALL2_1(_LIBKERNEL_SYSCALL_GET_PROCESS_INFO,
                        id,
                        1,
                        mem_usage);
  return mem_usage;
}

syscall_param_t _LIBKERNEL_range_allocator_count(syscall_param_t *capacity)
{
  syscall_param_t count;
  _LIBKERNEL_SYSCALL0_2(_LIBKERNEL_SYSCALL_RANGE_ALLOCATOR_COUNT,
                        count,
                        *capacity);
  return count;
}

void _LIBKERNEL_enum_range_allocator(syscall_param_t index,
                                     syscall_param_t *address,
                                     syscall_param_t *size)
{
  _LIBKERNEL_SYSCALL1_2(_LIBKERNEL_SYSCALL_ENUM_RANGE_ALLOCATOR,
                        index,
                        *address,
                        *size);
}

//
// VBE
//

void _LIBKERNEL_vbe_get_version(syscall_param_t *major,
                                syscall_param_t *minor)
{
  _LIBKERNEL_SYSCALL0_2(_LIBKERNEL_SYSCALL_VBE_GET_VERSION,
                        *major,
                        *minor);
}

syscall_param_t _LIBKERNEL_vbe_get_mode_count()
{
  syscall_param_t count;
  _LIBKERNEL_SYSCALL0_1(_LIBKERNEL_SYSCALL_VBE_GET_MODE_COUNT,
                        count);
  return count;
}

void _LIBKERNEL_vbe_enumerate_mode(syscall_param_t index,
                                   syscall_param_t *width,
                                   syscall_param_t *height,
                                   syscall_param_t *bpp)
{
  _LIBKERNEL_SYSCALL1_3(_LIBKERNEL_SYSCALL_VBE_GET_MODE,
                        index,
                        *width,
                        *height,
                        *bpp);
}

void *_LIBKERNEL_vbe_set_mode(syscall_param_t index)
{
  void *framebuffer;
  _LIBKERNEL_SYSCALL1_1(_LIBKERNEL_SYSCALL_VBE_SET_MODE,
                        index,
                        framebuffer);
  return framebuffer;
}

syscall_param_t _LIBKERNEL_allocate_io_port_range(syscall_param_t port,
                                                             syscall_param_t count)
{
  // NOTE TODO FIXME HACK
  syscall_param_t result;
  _LIBKERNEL_SYSCALL0_1( _LIBKERNEL_SYSCALL_REQUEST_IO,
                        result);
  return result;
}

void _LIBKERNEL_free_io_port_range(syscall_param_t port,
                                   syscall_param_t count)
{
  // NOTE TODO FIXME HACK
}

//
// ISA DMA
//

syscall_param_t _LIBKERNEL_setup_dma_channel(syscall_param_t channel,
                                                        void *address,
                                                        syscall_param_t count,
                                                        syscall_param_t mode)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL4_1(_LIBKERNEL_SYSCALL_DMA,
                        channel,
                        address,
                        count,
                        mode,
                        result);
  return result;
}

//
// Process creation
//

process_id_t _LIBKERNEL_execute(const void *data,
                                          syscall_param_t size,
                                          const char *cmdline,
                                          syscall_param_t flags,
                                          char *libName)
{
  process_id_t pid;
  _LIBKERNEL_SYSCALL5_1(_LIBKERNEL_SYSCALL_EXECUTE,
                        data,
                        size,
                        cmdline,
                        flags,
                        (syscall_param_t)libName,
                        pid);
  return pid;
}

//
// Physical memory-management
//

void *_LIBKERNEL_get_physical_address(const void *address)
{
  void *result;
  _LIBKERNEL_SYSCALL1_1(_LIBKERNEL_SYSCALL_GET_PHYSICAL_ADDRESS,
                        address,
                        result);
  return result;
}

void *_LIBKERNEL_allocate_memory_region(syscall_param_t size,
                                        syscall_param_t type,
                                        void *physical)
{
  void *result;
  _LIBKERNEL_SYSCALL3_1(_LIBKERNEL_SYSCALL_ALLOCATE_REGION,
                        size,
                        type,
                        physical,
                        result);
  return result;
}

void _LIBKERNEL_free_memory_region(void *address)
{
  _LIBKERNEL_SYSCALL1_0(_LIBKERNEL_SYSCALL_FREE_REGION,
                        address);
}

//
// System configuration
//

void _LIBKERNEL_get_system_configuration(system_config_t *config)
{
  syscall_param_t ret[3];
  _LIBKERNEL_SYSCALL0_3(_LIBKERNEL_SYSCALL_GET_SYSTEM_CONFIGURATION,
                        ret[0],
                        ret[1],
                        ret[2]);
  config->serial[0] = ret[0] & 0xFFFF;
  config->serial[1] = (ret[0] >> 16) & 0xFFFF;
  config->serial[2] = ret[1] & 0xFFFF;
  config->serial[3] = (ret[1] >> 16) & 0xFFFF;
  config->kernel_serial = ret[2] & 0xFFFF;
}

//
// Libraries
//

syscall_param_t _LIBKERNEL_get_library_count()
{
  syscall_param_t count;
  _LIBKERNEL_SYSCALL0_1(_LIBKERNEL_SYSCALL_GET_LIBRARY_COUNT,
                        count);
  return count;
}

syscall_param_t _LIBKERNEL_get_library_name_length(syscall_param_t index)
{
  syscall_param_t address, size, length;
  _LIBKERNEL_SYSCALL2_3(_LIBKERNEL_SYSCALL_GET_LIBRARY_INFO,
                        index,
                        0,
                        length,
                        address,
                        size);
  return length;
}

void _LIBKERNEL_get_library_info(syscall_param_t index,
                                 char *name,
                                 syscall_param_t length,
                                 syscall_param_t *address,
                                 syscall_param_t *size)
{
  _LIBKERNEL_SYSCALL2_3(_LIBKERNEL_SYSCALL_GET_LIBRARY_INFO,
                        index,
                        name,
                        length,
                        *address,
                        *size);
}

//
// Signal handler
//

void _LIBKERNEL_set_signal_handler(void (*func)(int))
{
  _LIBKERNEL_SYSCALL1_0(_LIBKERNEL_SYSCALL_SIGNAL_HANDLER,
                        func);
}

void _LIBKERNEL_signal_handler_end()
{
  _LIBKERNEL_SYSCALL0_0(_LIBKERNEL_SYSCALL_SIGNAL_END);
}

int _LIBKERNEL_signal(process_id_t pid,
                      int sig)
{
  syscall_param_t result;
  _LIBKERNEL_SYSCALL2_1(_LIBKERNEL_SYSCALL_SIGNAL,
                        pid,
                        sig,
                        result);
  if (result == 0)
    return 0;
  return -1;
}

syscall_param_t _LIBKERNEL_get_log_entry(syscall_param_t index,
                                                    syscall_param_t size,
                                                    char* address,
                                                    enum _LIBKERNEL_log_level* level)
{
  syscall_param_t real_size;
  _LIBKERNEL_SYSCALL3_2(_LIBKERNEL_SYSCALL_GET_LOG_ENTRY,
                        index,
                        size,
                        address,
                        real_size,
                        *level);
  return real_size;
}
