/*
lightOS libkernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_IO_PORT_HPP
#define LIGHTOS_LIBKERNEL_IO_PORT_HPP



/*
 * Libarch includes
 */
#include <libarch/ioport.hpp>

/*
 * Libkernel includes
 */
#include <libkernel/kernel.h>



/*! \addtogroup libkernel libkernel */
/*@{*/

#if defined(_LIBARCH_HAS_IOPORT)

namespace libkernel
{
  // TODO: range checking in debug build

  /** Class for port-based I/O
   *\param T   offset type
   *\param def default offset */
  template<typename T = size_t, T def = T()>
  class io_port
  {
    public:
      /** \note No copy-constructor */
      io_port(const io_port&) = delete;
      /** \note No assignment operator  */
      io_port& operator = (const io_port&) = delete;

      /** Default constructor */
      inline io_port();
      /** Destructor - frees allocated ressources */
      inline ~io_port();

      /** Allocate an I/O port range
       *\param[in] port the first I/O port in the range
       *\param[in] size the number of I/O ports
       *\return true, if the I/O port range has been allocated successfully,
       *        false otherwise */
      inline bool allocate(libarch::ioport_t port,
                           size_t size);

      /** Free the I/O port range, if any */
      inline void free();

      /** Get the size of the I/O port range */
      inline size_t size() const;

      /** Read a 8bit from an I/O port
       *\param[in] offset the offset to read from
       *\return the value read */
      inline uint8_t  read8(T offset = def);
      /** Read a 16bit from an I/O port
       *\param[in] offset the offset to read from
       *\return the value read */
      inline uint16_t read16(T offset = def);
      /** Read a 32bit from an I/O port
       *\param[in] offset the offset to read from
       *\return the value read */
      inline uint32_t read32(T offset = def);
    #if defined(_LIBARCH_HAS_IOPORT64)
      /** Read a 64bit from an I/O port
       *\param[in] offset the offset to read from
       *\return the value read */
      inline uint64_t read64(T offset = def);
    #endif

      /** Write a 8bit value to an I/O port
       *\param[in] value  the value to write
       *\param[in] offset the offset to write to */
      inline void write8(uint8_t value,
                         T offset = def);
      /** Write a 16bit value to an I/O port
       *\param[in] value  the value to write
       *\param[in] offset the offset to write to */
      inline void write16(uint16_t value,
                          T offset = def);
      /** Write a 32bit value to an I/O port
       *\param[in] value  the value to write
       *\param[in] offset the offset to write to */
      inline void write32(uint32_t value,
                          T offset = def);
    #if defined(_LIBARCH_HAS_IOPORT64)
      /** Write a 64bit value to an I/O port
       *\param[in] value  the value to write
       *\param[in] offset the offset to write to */
      inline void write64(uint64_t value,
                          T offset = def);
    #endif

    private:
      /*! The first port */
      libarch::ioport_t m_port;
      /*! The number of I/O ports */
      size_t            m_size;
  };



  /*
   * Part of the implementation
   */

  template<typename T, T def>
  io_port<T, def>::io_port()
    : m_port(), m_size()
  {
  }

  template<typename T, T def>
  io_port<T, def>::~io_port()
  {
    free();
  }

  template<typename T, T def>
  bool io_port<T, def>::allocate(libarch::ioport_t port,
                                 size_t size)
  {
    free();

    if (_LIBKERNEL_allocate_io_port_range(port, size) == 0)
      return false;

    m_port = port;
    m_size = size;
    return true;
  }

  template<typename T, T def>
  void io_port<T, def>::free()
  {
    _LIBKERNEL_free_io_port_range(m_port, m_size);
    m_port = 0;
    m_size = 0;
  }

  template<typename T, T def>
  size_t io_port<T, def>::size() const
  {
    return m_size;
  }

  template<typename T, T def>
  uint8_t io_port<T, def>::read8(T offset)
  {
    return _LIBARCH_in8(m_port + offset);
  }

  template<typename T, T def>
  uint16_t io_port<T, def>::read16(T offset)
  {
    return _LIBARCH_in16(m_port + offset);
  }

  template<typename T, T def>
  uint32_t io_port<T, def>::read32(T offset)
  {
    return _LIBARCH_in32(m_port + offset);
  }

#if defined(_LIBARCH_HAS_IOPORT64)
  template<typename T, T def>
  uint64_t io_port<T, def>::read64(T offset)
  {
    return _LIBARCH_in64(m_port + offset);
  }
#endif

  template<typename T, T def>
  void io_port<T, def>::write8(uint8_t value,
                               T offset)
  {
    _LIBARCH_out8(m_port + offset,
                  value);
  }

  template<typename T, T def>
  void io_port<T, def>::write16(uint16_t value,
                                T offset)
  {
    _LIBARCH_out16(m_port + offset,
                   value);
  }

  template<typename T, T def>
  void io_port<T, def>::write32(uint32_t value,
                                T offset)
  {
    _LIBARCH_out32(m_port + offset,
                   value);
  }

#if defined(_LIBARCH_HAS_IOPORT64)
  template<typename T, T def>
  void io_port<T, def>::write64(uint64_t value,
                                T offset)
  {
    _LIBARCH_out64(m_port + offset,
                   value);
  }
#endif
}

#endif

/*@}*/

#endif
