/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_COMPILER_HPP
#define LIGHTOS_LIBKERNEL_COMPILER_HPP

/*! \addtogroup libkernel libkernel */
/*@{*/

// NOTE: See http://gcc.gnu.org/onlinedocs/gcc/Function-Attributes.html
//           http://gcc.gnu.org/onlinedocs/gcc/Variable-Attributes.html
//           http://gcc.gnu.org/onlinedocs/gcc/Type-Attributes.html
//           http://www.imodulo.com/gnu/gcc/Other-Builtins.html
//       for more information.

/** Deprecated function/variable/type */
#define DEPRECATED                     __attribute__((deprecated))
/** Function does not return */
#define NORETURN                       __attribute__((noreturn))
/** Function does not have a side-effect (except for the return value) */
#define PURE                           __attribute__((pure))
/** Function does not have a side-effect and does only depend on its arguments */
#define CONST                          __attribute__((const))
/** Functions that should always be inlined */
#define ALWAYS_INLINE                  __attribute__((always_inline))
/** Specific alignment for a type/variable */
#define ALIGN(x)                       __attribute__((align(x)))
/** No padding for a type/variable */
#define PACKED                         __attribute__((packed))
/** The type may alias */
#define MAY_ALIAS                      __attribute__((may_alias))
/** The expression is very likely to be true */
#define LIKELY(exp)                    __builtin_expect(!!(exp), 1)
/** The expression is very unlikely to be true */
#define UNLIKELY(exp)                  __builtin_expect(!!(exp), 0)

/*@}*/

#endif
