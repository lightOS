/*
lightOS libkernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_LOG_H
#define LIGHTOS_LIBKERNEL_LOG_H

/*! \addtogroup libkernel libkernel */
/*@{*/

enum _LIBKERNEL_log_level
{
  _LIBKERNEL_log_notice  = 0,
  _LIBKERNEL_log_warning = 1,
  _LIBKERNEL_log_error   = 2,
  _LIBKERNEL_log_fatal   = 3
};

/*@}*/

#endif
