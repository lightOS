/*
lightOS libkernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_TYPE_HPP
#define LIGHTOS_LIBKERNEL_TYPE_HPP

#include <libkernel/type.h>

/*! \addtogroup libkernel libkernel */
/*@{*/

namespace libkernel
{
  typedef _LIBKERNEL_port_id_t          port_id_t;
  typedef _LIBKERNEL_thread_id_t        thread_id_t;
  typedef _LIBKERNEL_process_id_t       process_id_t;
  typedef _LIBKERNEL_shared_memory_id_t shared_memory_id_t;
  typedef _LIBKERNEL_message_param_t    message_param_t;
  typedef _LIBKERNEL_shared_memory_t    shared_memory_t;
  typedef _LIBKERNEL_system_config_t    system_config_t;
  typedef _LIBKERNEL_message_t          message_t;
}

/*@}*/

#endif
