/*
lightOS libkernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_SYSCALL_H
#define LIGHTOS_LIBKERNEL_SYSCALL_H

/*! \addtogroup libkernel libkernel */
/*@{*/

#define _LIBKERNEL_SYSCALL_GET_KERNEL_VERSION               0x0000
#define _LIBKERNEL_SYSCALL_HEAPALLOC                        0x0001
#define _LIBKERNEL_SYSCALL_GET_MEMORY_INFO                  0x0002

/* Command line syscalls */
#define _LIBKERNEL_SYSCALL_GET_COMMANDLINE                  0x0003

/* time/data syscalls */
#define _LIBKERNEL_SYSCALL_GET_TIME                         0x0004
#define _LIBKERNEL_SYSCALL_GET_DATE                         0x0005

/* port syscalls */
#define _LIBKERNEL_SYSCALL_CREATE_PORT                      0x0006
#define _LIBKERNEL_SYSCALL_DESTROY_PORT                     0x0007
#define _LIBKERNEL_SYSCALL_PEEK_MESSAGE                     0x0008
#define _LIBKERNEL_SYSCALL_GET_MESSAGE                      0x0009
#define _LIBKERNEL_SYSCALL_WAIT_MESSAGE                     0x000A
#define _LIBKERNEL_SYSCALL_SEND_MESSAGE                     0x000B
#define _LIBKERNEL_SYSCALL_PORT_INFO                        0x000C
#define _LIBKERNEL_SYSCALL_STANDARD_PORT                    0x000D
#define _LIBKERNEL_SYSCALL_TRANSFER_PORT                    0x000E
#define _LIBKERNEL_SYSCALL_ADD_WAIT_MESSAGE                 0x000F

/* process/thread syscalls */
#define _LIBKERNEL_SYSCALL_CREATE_THREAD                    0x0010
#define _LIBKERNEL_SYSCALL_SUSPEND_THREAD                   0x0011
#define _LIBKERNEL_SYSCALL_RESUME_THREAD                    0x0012
#define _LIBKERNEL_SYSCALL_DESTROY_THREAD                   0x0013
#define _LIBKERNEL_SYSCALL_DESTROY_PROCESS                  0x0014
#define _LIBKERNEL_SYSCALL_GET_PROCESSID                    0x0015
#define _LIBKERNEL_SYSCALL_GET_THREADID                     0x0016
#define _LIBKERNEL_SYSCALL_GET_PROCESS_COUNT                0x0017
#define _LIBKERNEL_SYSCALL_GET_PROCESS_LIST                 0x0018
#define _LIBKERNEL_SYSCALL_GET_PROCESS_INFO                 0x0019

/* Event syscalls */
#define _LIBKERNEL_SYSCALL_REGISTER_EVENT                   0x001A
#define _LIBKERNEL_SYSCALL_ACKNOLEDGE_EVENT                 0x001B
#define _LIBKERNEL_SYSCALL_UNREGISTER_EVENT                 0x001C

/* Get the number of ms since system boot */
#define _LIBKERNEL_SYSCALL_GET_TICK_COUNT                   0x001D

/* Shared memory syscalls */
#define _LIBKERNEL_SYSCALL_CREATE_SHARED_MEMORY             0x001E
#define _LIBKERNEL_SYSCALL_DESTROY_SHARED_MEMORY            0x001F

#define _LIBKERNEL_SYSCALL_RANGE_ALLOCATOR_COUNT            0x0020
#define _LIBKERNEL_SYSCALL_ENUM_RANGE_ALLOCATOR             0x0021

/* Server syscalls */
#define _LIBKERNEL_SYSCALL_CREATE_SPECIFIC_PORT             0x0025
// TODO #define _LIBKERNEL_SYSCALL_GET_KERNEL_LOG            0x0026

/* VBE (VESA BIOS Extensions) */
#define _LIBKERNEL_SYSCALL_VBE_GET_VERSION                  0x0027
#define _LIBKERNEL_SYSCALL_VBE_GET_MODE_COUNT               0x0028
#define _LIBKERNEL_SYSCALL_VBE_GET_MODE                     0x0029
#define _LIBKERNEL_SYSCALL_VBE_SET_MODE                     0x002A

#define _LIBKERNEL_SYSCALL_REQUEST_IO                       0x002B
#define _LIBKERNEL_SYSCALL_DMA                              0x002C
#define _LIBKERNEL_SYSCALL_EXECUTE                          0x002D
#define _LIBKERNEL_SYSCALL_GET_PHYSICAL_ADDRESS             0x002E
#define _LIBKERNEL_SYSCALL_ALLOCATE_REGION                  0x002F
#define _LIBKERNEL_SYSCALL_FREE_REGION                      0x0030
#define _LIBKERNEL_SYSCALL_GET_SYSTEM_CONFIGURATION         0x0031
#define _LIBKERNEL_SYSCALL_GET_LIBRARY_COUNT                0x0032
#define _LIBKERNEL_SYSCALL_GET_LIBRARY_INFO                 0x0033

/* Signal handler */
#define _LIBKERNEL_SYSCALL_SIGNAL_HANDLER                   0x0034
#define _LIBKERNEL_SYSCALL_SIGNAL                           0x0035
#define _LIBKERNEL_SYSCALL_SIGNAL_END                       0x0036

/* Kernel log */
#define _LIBKERNEL_SYSCALL_GET_LOG_ENTRY                    0x0037

/*@}*/

#endif
