/*
lightOS libkernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_MESSAGE_HPP
#define LIGHTOS_LIBKERNEL_MESSAGE_HPP

#include <libkernel/message.h>
#include <libkernel/event.hpp>
#include <libkernel/shared_memory.hpp>

// TODO: This should not be here
#include <vector>
#include <cstdint>

/** \addtogroup libkernel libkernel */
/*@{*/

namespace libkernel
{
  /* Forward declaration */
  class message_port;

  /** A inter-process communication message */
  class message
  {
    /** message_port needs access to m_msg */
    friend class libkernel::message_port;

    public:
      typedef libkernel::message_param_t param_t;

      /** The message types */
      enum type_t
      {
        /** Ping */
        ping                      = _LIBKERNEL_MSG_PING,
        /** Ping response */
        pong                      = _LIBKERNEL_MSG_PONG,
        /** Event */
        event                     = _LIBKERNEL_MSG_EVENT,

        /** vfs server: register a filesystem */
        vfs_register_fs           = _LIBKERNEL_MSG_VFS_REGISTER_FS,
        /** vfs server: wait for filesystem registration */
        vfs_wait_fs_registration  = _LIBKERNEL_MSG_VFS_WAIT_FS_REGISTRATION,
        /** vfs server: set a process's working directory */
        vfs_set_working_dir       = _LIBKERNEL_MSG_VFS_SET_WORKING_DIR,
        /** vfs server: get a process's working directory */
        vfs_get_working_dir       = _LIBKERNEL_MSG_VFS_GET_WORKING_DIR,
        /** vfs server: set a process's console filename */
        vfs_set_console           = _LIBKERNEL_MSG_VFS_SET_CONSOLE,
        /** vfs server: get a process's console filename */
        vfs_get_console           = _LIBKERNEL_MSG_VFS_GET_CONSOLE,
        /** vfs server: find filesystem */
        vfs_find_fs               = _LIBKERNEL_MSG_VFS_FIND_FS,
        /** vfs server: mount */
        vfs_mount                 = _LIBKERNEL_MSG_VFS_MOUNT,
        /** vfs server: unmount */
        vfs_unmount               = _LIBKERNEL_MSG_VFS_UNMOUNT,
        /** vfs server: number of mountpoints */
        vfs_get_mount_count       = _LIBKERNEL_MSG_VFS_GET_MOUNT_COUNT,
        /** vfs server: get information of a specific mountpoint */
        vfs_get_mount_info        = _LIBKERNEL_MSG_VFS_GET_MOUNT_INFO,

        /** pci server: enumerate pci devices */
        pci_enum_devices          = _LIBKERNEL_MSG_PCI_ENUM_DEVICES,
        /** pci server: get pci device info */
        pci_get_device_info       = _LIBKERNEL_MSG_PCI_GET_DEVICE_INFO,
        /** pci server: find pci device */
        pci_find_device           = _LIBKERNEL_MSG_PCI_FIND_DEVICE,
        /** pci server: enable busmastering for a device */
        pci_enable_busmaster      = _LIBKERNEL_MSG_PCI_ENABLE_BUSMASTER,

        /** fs server: get size & free */
        fs_get_info               = _LIBKERNEL_MSG_FS_GET_INFO,
        /** fs server: create a file */
        fs_create_file            = _LIBKERNEL_MSG_FS_CREATE_FILE,
        /** fs server: does a file exist? */
        fs_exists_file            = _LIBKERNEL_MSG_FS_EXISTS_FILE,
        /** fs server: open a file */
        fs_open_file              = _LIBKERNEL_MSG_FS_OPEN_FILE,
        /** fs server: get file information */
        fs_file_info              = _LIBKERNEL_MSG_FS_FILE_INFO,
        /** fs server: read a file */
        fs_read_file              = _LIBKERNEL_MSG_FS_READ_FILE,
        /** fs server: write a file */
        fs_write_file             = _LIBKERNEL_MSG_FS_WRITE_FILE,
        /** fs server: close a file */
        fs_close_file             = _LIBKERNEL_MSG_FS_CLOSE_FILE,
        /** fs server: delete a file */
        fs_delete_file            = _LIBKERNEL_MSG_FS_DELETE_FILE,
        /** fs server: rename a file */
        fs_rename_file            = _LIBKERNEL_MSG_FS_REANME_FILE,
        /** fs server: set the size of a file */
        fs_set_file_size          = _LIBKERNEL_MSG_FS_SET_FILE_SIZE,
        /** fs server: get the filename */
        fs_get_filename           = _LIBKERNEL_MSG_FS_GET_FILENAME,
        /** fs server: get the file flags */
        fs_get_file_flags         = _LIBKERNEL_MSG_FS_GET_FILE_FLAGS,
        /** fs server: get the number of ports that opened a file */
        fs_get_opener_count       = _LIBKERNEL_MSG_FS_GET_OPENER_COUNT,
        /** fs server: enumerate the ports that opened a file */
        fs_enum_opener            = _LIBKERNEL_MSG_FS_ENUM_OPENER,

        /** net server: register a NIC */
        net_register_nic          = _LIBKERNEL_MSG_NET_REGISTER_NIC,
        /** net server: unregister a NIC */
        net_unregister_nic        = _LIBKERNEL_MSG_NET_UNREGISTER_NIC,
        /** net server: enumerate the NICs */
        net_enum_nic              = _LIBKERNEL_MSG_NET_ENUM_NIC,
        /** net server: get information about one NIC */
        net_get_nic_info          = _LIBKERNEL_MSG_NET_GET_NIC_INFO,
        /** net server: the NIC receives a packet */
        net_nic_receive           = _LIBKERNEL_MSG_NET_NIC_RECEIVE,
        /** net server: the NIC transmits a packet */
        net_nic_transmit          = _LIBKERNEL_MSG_NET_NIC_TRANSMIT,
        /** net server: get the number of entries in the arp cache */
        net_arp_count             = _LIBKERNEL_MSG_NET_ARP_COUNT,
        /** net server: get one entry in the arp cache */
        net_arp_entry             = _LIBKERNEL_MSG_NET_ARP_ENTRY,
        /** net server: clear the arp cache */
        net_arp_clear             = _LIBKERNEL_MSG_NET_ARP_CLEAR,
        /** net server: get the number of entries in the dns cache */
        net_dns_count             = _LIBKERNEL_MSG_NET_DNS_COUNT,
        /** net server: get one entry in the dns cache */
        net_dns_entry             = _LIBKERNEL_MSG_NET_DNS_ENTRY,
        /** net server: clear the DNS cache */
        net_dns_clear             = _LIBKERNEL_MSG_NET_DNS_CLEAR,
        /** net server: set information about one NIC */
        net_set_nic_info          = _LIBKERNEL_MSG_NET_SET_NIC_INFO,
        /** net server: create a socket */
        net_socket_create         = _LIBKERNEL_MSG_NET_SOCKET_CREATE,
        /** net server: bind a socket */
        net_socket_bind           = _LIBKERNEL_MSG_NET_SOCKET_BIND,
        /** net server: connect socket */
        net_socket_connect        = _LIBKERNEL_MSG_NET_SOCKET_CONNECT,
        /** net server: send to another socket */
        net_socket_send           = _LIBKERNEL_MSG_NET_SOCKET_SEND,
        /** net server: receive from another socket */
        net_socket_receive        = _LIBKERNEL_MSG_NET_SOCKET_RECEIVE,
        /** net server: disconnect socket */
        net_socket_disconnect     = _LIBKERNEL_MSG_NET_SOCKET_DISCONNECT,
        /** net server: destroy a socket */
        net_socket_destroy        = _LIBKERNEL_MSG_NET_SOCKET_DESTROY,
        /** net server: resolve a domain name  */
        net_resolve               = _LIBKERNEL_MSG_NET_RESOLVE,

        /** init server: execute file */
        init_execute              = _LIBKERNEL_MSG_INIT_EXECUTE,
        /** init server: init done */
        init_done                 = _LIBKERNEL_MSG_INIT_DONE,

        /** gui server: get the number of video modes */
        gui_get_video_mode_count  = _LIBKERNEL_MSG_GUI_GET_VIDEO_MODE_COUNT,
        /** gui server: enumerate the available video modes */
        gui_enum_video_mode       = _LIBKERNEL_MSG_GUI_ENUM_VIDEO_MODES,
        /** gui server: get the current video mode */
        gui_get_video_mode        = _LIBKERNEL_MSG_GUI_GET_VIDEO_MODE,
        /** gui server: set the current video mode */
        gui_set_video_mode        = _LIBKERNEL_MSG_GUI_SET_VIDEO_MODE,
        /** gui server: get the background image */
        gui_get_background_image  = _LIBKERNEL_MSG_GUI_GET_BACKGROUND_IMAGE,
        /** gui server: set the background image */
        gui_set_background_image  = _LIBKERNEL_MSG_GUI_SET_BACKGROUND_IMAGE,
        /** gui server: create a window */
        gui_create_window         = _LIBKERNEL_MSG_GUI_CREATE_WINDOW,
        /** gui server: move a window */
        gui_move_window           = _LIBKERNEL_MSG_GUI_MOVE_WINDOW,
        /** gui server: resize a window */
        gui_resize_window         = _LIBKERNEL_MSG_GUI_RESIZE_WINDOW,
        /** gui server: destroy a window */
        gui_destroy_window        = _LIBKERNEL_MSG_GUI_DESTROY_WINDOW,
        /** gui server: draw a text */
        gui_draw_text             = _LIBKERNEL_MSG_GUI_DRAW_TEXT,
        /** gui server: keyboard event */
        gui_keyboard              = _LIBKERNEL_MSG_GUI_KEYBOARD
      };

      /** The constructor */
      message() = default;

      /** The constructor
       *\param[in] port the portId
       *\param[in] type the message id
       *\param[in] param1 the first parameter
       *\param[in] param2 the second parameter
       *\param[in] param3 the third parameter */
      inline message(port_id_t port,
                     size_t type,
                     size_t param1 = 0,
                     size_t param2 = 0,
                     size_t param3 = 0);

      /** The constructor
       *\param[in] port the portId
       *\param[in] type the message id
       *\param[in] param1 the first parameter
       *\param[in] shm reference to the shared-memory region
       *\param[in] flags the shared-memory transmission flags */
      inline message(port_id_t port,
                     size_t type,
                     size_t param1,
                     libkernel::shared_memory& shm,
                     size_t flags);

      /** Get the message id */
      inline type_t type() const;

      /** Set the message id */
      inline void type(type_t type);

      /** Get the message attribute */
      inline size_t attribute() const;

      /** Set the message attribute */
      inline void attribute(size_t attr);

      /** Get the first parameter */
      inline param_t param1() const;

      /** Set the first parameter */
      inline void param1(param_t param);

      /** Get the second parameter */
      inline param_t param2() const;

      /** Set the second parameter */
      inline void param2(param_t param);

      /** Get the third parameter */
      inline param_t param3() const;

      /** Set the third parameter */
      inline void param3(param_t param);

      /** Get the portId */
      inline port_id_t port() const;

      /** Set the portId */
      inline void port(port_id_t port);

      /** Get the shared-memory */
      inline libkernel::shared_memory get_shared_memory() const;

    private:
      /** The message */
      _LIBKERNEL_message_t m_msg;
  };



  /** A inter-process communication message port */
  class message_port
  {
    public:
      /** Specific message ports */
      enum type_t
      {
        none    = _LIBKERNEL_PORT_NONE,
        in      = _LIBKERNEL_PORT_STDIN,
        out     = _LIBKERNEL_PORT_STDOUT,
        err     = _LIBKERNEL_PORT_STDERR,
        kernel  = _LIBKERNEL_PORT_KERNEL,
        init    = _LIBKERNEL_PORT_INIT,
        pci     = _LIBKERNEL_PORT_PCI,
        vfs     = _LIBKERNEL_PORT_VFS,
        gui     = _LIBKERNEL_PORT_GUI,
        net     = _LIBKERNEL_PORT_NET,
        console = _LIBKERNEL_PORT_CONSOLE,

        /** User defined ports */
        user    = _LIBKERNEL_PORT_USER
      };

      /** Class for callbacks on a port
       *\TODO: Add optional default handler */
      class callback
      {
        private:
          /** Callback function with or without shared-memory
           *\param[in] port    the port the message was received on
           *\param[in] message the actual message that was received */
          typedef void (*function_pointer1)(libkernel::message_port& port,
                                            const libkernel::message& message);

          /** Callback function with shared-memory
           *\param[in] port          the port the message was received on
           *\param[in] shared_memory the shared-memory region that came with the message
           *\param[in] param         the additional parameter */
          typedef void (*function_pointer2)(libkernel::message_port& port,
                                            libkernel::shared_memory& shared_memory,
                                            libkernel::message::param_t param);

        public:
          /** Default constructor */
          inline callback() = default;

          /** No copy-constructor */
          callback(const callback&) = delete;

          /** No assignment operator */
          callback& operator = (const callback&) = delete;

          /** Destructor
           *\TODO deallocate the stuff */
          inline ~callback() = default;

          /** Register a handler for a specific message type without shared-memory
           *\param[in] msg_type the specific message type to register a handler for
           *\param[in] fun_ptr  the pointer to the handling function */
          inline bool register_handler(libkernel::message::type_t msg_type,
                                       function_pointer1 fun_ptr);

          /** Register a handler for a specific message type with shared-memory
           *\param[in] msg_type the specific message type to register a handler for
           *\param[in] fun_ptr  the pointer to the handling function */
          inline bool register_handler(libkernel::message::type_t msg_type,
                                       function_pointer2 fun_ptr);

          /** Register two handlers for a specific message type, one for messages
           *  with shared-memory, one for messages without shared-memory
           *\param[in] msg_type the specific message type to register handlers for
           *\param[in] fun_ptr1 the pointer to the handling function (without
           *                    shared-memory)
           *\param[in] fun_ptr2 the pointer to the handling function (with
           *                    shared-memory) */
          inline bool register_handler(libkernel::message::type_t msg_type,
                                       function_pointer1 fun_ptr1,
                                       function_pointer2 fun_ptr2);

          /** Wait for messages on the given port and call the appropriate
           *  callback function. Returns only if one of the handlers returns
           *  false or if getting a message from the port failed
           *\param[in] port the port */
          inline void operator () (libkernel::message_port& port);

        private:
          class handler1
          {
            public:
              virtual void operator () (libkernel::message_port& port,
                                        const libkernel::message& msg) = 0;

            protected:
              inline handler1() = default;
              handler1(const handler1&) = delete;
              handler1& operator = (const handler1&) = delete;
              inline ~handler1() = default;
          };

          class handler2
          {
            public:
              virtual void operator () (libkernel::message_port& port,
                                        libkernel::shared_memory& shared_memory,
                                        libkernel::message::param_t param) = 0;

            protected:
              inline handler2() = default;
              handler2(const handler2&) = delete;
              handler2& operator = (const handler2&) = delete;
              inline ~handler2() = default;
          };

          class function_handler1 : public handler1
          {
            public:
              inline function_handler1(function_pointer1 fun_ptr)
                : m_fun_ptr(fun_ptr)
              {
              }

              inline virtual void operator () (libkernel::message_port& port,
                                              const libkernel::message& message)
              {
                m_fun_ptr(port,
                          message);
              }

            private:
              function_pointer1 m_fun_ptr;
          };

          class function_handler2 : public handler2
          {
            public:
              inline function_handler2(function_pointer2 fun_ptr)
                : m_fun_ptr(fun_ptr)
              {
              }

              inline virtual void operator () (libkernel::message_port& port,
                                              libkernel::shared_memory& shared_memory,
                                              libkernel::message::param_t param)
              {
                m_fun_ptr(port,
                          shared_memory,
                          param);
              }

            private:
              function_pointer2 m_fun_ptr;
          };

          inline bool register_handler(libkernel::message::type_t msg_type,
                                       handler1* callback_handler1,
                                       handler2* callback_handler2);

          // TODO: Use a better datastructure
          std::vector<std::pair<libkernel::message::type_t, std::pair<handler1*,handler2*>>> m_handlers;
      };

      class wait_group
      {
        public:
          class handler
          {
            public:
              virtual void received(port_id_t id,
                                    message& msg) = 0;
              inline virtual ~handler() = default;
          };

          inline void register_handler(libkernel::port_id_t Port,
                                       const libkernel::message& message,
                                       handler* callback = 0);

          inline void register_handler(libkernel::message_port& Port,
                                       const libkernel::message& message,
                                       handler* callback = 0);

          inline std::pair<libkernel::port_id_t,libkernel::message> operator () ();

        private:
          std::vector<std::triple<libkernel::port_id_t,handler*,libkernel::message> > mTargets;
      };

      /** The constructor */
      inline message_port();

      /** The constructor */
      inline message_port(port_id_t id,
                          bool create = false);

      /** Get the message port's id */
      inline size_t id() const;

      /** Peek a message
       *\param[in,out] msg reference to the message
       *\return true, if a message is available, false otherwise */
      inline bool peek(message& msg) const;

      /** Get a message
       *\param[in,out] msg reference to the message
       *\return true, if successfull, false otherwise */
      inline bool get(message& msg) const;

      /** Wait for a specific message
       *\param[in,out] msg reference to the message */
      template<typename... Args>
      inline bool wait(message& msg,
                       const message& msg2,
                       const Args&... args) const;

      inline bool wait(message& msg) const;

      /** Send a message
      *\param[in] msg reference to the message
      *\return true, if successfull, false otherwise */
      inline bool send(const message& msg) const;

      /** Wait for a another message port
       *\param[in] id the other portId */
      inline void wait_for_other(port_id_t id);

      /** Register event
      *\param[in] type the event type
      *\param[in] param1 first event parameter
      *\param[in] param2 second event parameter
      *\return true, if successfull */
      inline bool register_event(event type,
                                 size_t param1 = 0,
                                 size_t param2 = 0);

      /** Unregister event
      *\param[in] type the event type
      *\return true, if successfull */
      inline bool unregister_event(event type);

      inline size_t register_isa_irq(uint8_t irq,
                                     uint8_t priority);
      inline size_t register_pci_irq(uint8_t irq,
                                     uint8_t intn,
                                     uint8_t bus,
                                     uint8_t device,
                                     uint8_t priority);

      inline void acknoledge_irq(size_t vector);

      inline port_id_t release();

      /** The destructor */
      inline ~message_port();

      /** The bool() operator */
      inline operator bool() const;

    private:
      /*! The port's id */
      port_id_t m_id;
  };



  /*
   * Implementation of message
   */

  message::message(libkernel::port_id_t id,
                   size_t type,
                   size_t param1,
                   size_t param2,
                   size_t param3)
    : m_msg(_LIBKERNEL_create_message(id, type, param1, param2, param3))
  {
  }

  message::message(libkernel::port_id_t id,
                   size_t type,
                   size_t param1,
                   libkernel::shared_memory& shm,
                   size_t flags)
    : m_msg(_LIBKERNEL_create_message_shm(id, type, param1, &shm.m_shm, flags))
  {
  }

  message::type_t message::type() const
  {
    return static_cast<message::type_t>(_LIBKERNEL_get_message_type(&m_msg));
  }

  void message::type(message::type_t type)
  {
    _LIBKERNEL_set_message_type(&m_msg, type);
  }

  size_t message::attribute() const
  {
    return _LIBKERNEL_get_message_attribute(&m_msg);
  }

  void message::attribute(size_t attr)
  {
    _LIBKERNEL_set_message_attribute(&m_msg, attr);
  }

  message::param_t message::param1() const
  {
    return m_msg.param1;
  }

  void message::param1(param_t param)
  {
    m_msg.param1 = param;
  }

  message::param_t message::param2() const
  {
    return m_msg.param2;
  }

  void message::param2(param_t param)
  {
    m_msg.param2 = param;
  }

  message::param_t message::param3() const
  {
    return m_msg.param3;
  }

  void message::param3(param_t param)
  {
    m_msg.param3 = param;
  }

  port_id_t message::port() const
  {
    return m_msg.port;
  }

  void message::port(port_id_t port)
  {
    m_msg.port = port;
  }

  libkernel::shared_memory message::get_shared_memory() const
  {
    return libkernel::shared_memory(_LIBKERNEL_get_shared_memory(&m_msg));
  }



  /*
   * Implementation of message_port
   */

  message_port::message_port()
    : m_id(_LIBKERNEL_create_port())
  {
  }

  message_port::message_port(port_id_t id,
                             bool create)
    : m_id(id)
  {
    if (create == true)
      m_id = _LIBKERNEL_create_specific_port(id);
  }

  size_t message_port::id() const
  {
    return m_id;
  }

  bool message_port::peek(message& msg) const
  {
    return (_LIBKERNEL_peek_message(m_id, &msg.m_msg) != 0);
  }

  bool message_port::get(message& msg) const
  {
    return (_LIBKERNEL_get_message(m_id, &msg.m_msg) != 0);
  }

  template<typename... Args>
  bool message_port::wait(message& msg,
                          const message& msg2,
                          const Args&... args) const
  {
    _LIBKERNEL_add_wait_message(m_id, &msg2.m_msg);
    return wait(msg, args...);
  }

  bool message_port::wait(message& msg) const
  {
    _LIBKERNEL_add_wait_message(m_id, &msg.m_msg);
    return (_LIBKERNEL_wait_message(&msg.m_msg) == m_id);
  }

  bool message_port::send(const message& msg) const
  {
    return (_LIBKERNEL_send_message(m_id, &msg.m_msg) != 0);
  }

  void message_port::wait_for_other(port_id_t id)
  {
    message msg(id, message::ping);
    while (send(msg) == false)
      ;

    msg.type(message::pong);
    wait(msg);
  }

  // TODO: Rework
  bool message_port::register_event(libkernel::event type,
                                    size_t param1,
                                    size_t param2)
  {
    return (_LIBKERNEL_register_event(m_id, type, param1, param2) != 0);
  }

  bool message_port::unregister_event(libkernel::event type)
  {
    return (_LIBKERNEL_unregister_event(m_id, type) != 0);
  }

  size_t message_port::register_isa_irq(uint8_t irq,
                                        uint8_t priority)
  {
    return _LIBKERNEL_register_event(m_id,
                                     event::irq,
                                     (irq << 8) | 1,
                                     priority << 24);
  }

  size_t message_port::register_pci_irq(uint8_t irq,
                                        uint8_t intn,
                                        uint8_t bus,
                                        uint8_t device,
                                        uint8_t priority)
  {
    return _LIBKERNEL_register_event(m_id,
                                     event::irq,
                                     (irq << 8) | 2,
                                     (priority << 24) | (intn << 16) | (device << 8) | bus);
  }

  void message_port::acknoledge_irq(size_t vector)
  {
    _LIBKERNEL_acknoledge_event(m_id,
                                event::irq,
                                vector);
  }

  port_id_t message_port::release()
  {
    port_id_t tmp = m_id;
    m_id = 0;
    return tmp;
  }

  message_port::~message_port()
  {
    _LIBKERNEL_destroy_port(m_id);
  }

  message_port::operator bool() const
  {
    return (m_id != 0);
  }



  /*
   * Implementation of message_port::callback
   */

  bool message_port::callback::register_handler(libkernel::message::type_t msg_type,
                                                function_pointer1 fun_ptr)
  {
    return register_handler(msg_type,
                            new function_handler1(fun_ptr),
                            0);
  }

  bool message_port::callback::register_handler(libkernel::message::type_t msg_type,
                                                function_pointer2 fun_ptr)
  {
    return register_handler(msg_type,
                            0,
                            new function_handler2(fun_ptr));
  }

  bool message_port::callback::register_handler(libkernel::message::type_t msg_type,
                                                function_pointer1 fun_ptr1,
                                                function_pointer2 fun_ptr2)
  {
    return register_handler(msg_type,
                            new function_handler1(fun_ptr1),
                            new function_handler2(fun_ptr2));
  }

  void message_port::callback::operator () (libkernel::message_port& port)
  {
    libkernel::message message;
    while (port.get(message))
    {
      auto i = m_handlers.begin();
      auto end = m_handlers.end();
      for (;i != end;++i)
        if ((*i).first == message.type())
        {
          if (message.attribute() == libkernel::shared_memory::none)
            (*i).second.first->operator () (port,
                                            message);
          else
          {
            libkernel::shared_memory shared_memory = message.get_shared_memory();
            (*i).second.second->operator () (port,
                                            shared_memory,
                                            message.param3());
          }
        }
    }
  }

  bool message_port::callback::register_handler(libkernel::message::type_t msg_type,
                                                handler1* callback_handler1,
                                                handler2* callback_handler2)
  {
    // TODO: Check that we are not screwing things up
    m_handlers.push_back(std::make_pair(msg_type,
                                        std::make_pair(callback_handler1, callback_handler2)));
    return true;
  }



  /*
   * Implementation of message_port::wait_group
   */

  void message_port::wait_group::register_handler(libkernel::port_id_t Port,
                                                  const libkernel::message& message,
                                                  handler* callback)
  {
    mTargets.push_back(std::triple<libkernel::port_id_t,handler*,libkernel::message>(Port, callback, message));
  }

  void message_port::wait_group::register_handler(libkernel::message_port& Port,
                                                  const libkernel::message& message,
                                                  handler* callback)
  {
    register_handler(Port.id(), message, callback);
  }

  std::pair<libkernel::port_id_t,libkernel::message> message_port::wait_group::operator () ()
  {
    for (size_t i = 0;i < mTargets.size();i++)
      _LIBKERNEL_add_wait_message(mTargets[i].first, &mTargets[i].third.m_msg);

    std::pair<libkernel::port_id_t,libkernel::message> result;
    result.first = _LIBKERNEL_wait_message(&result.second.m_msg);

    for (size_t i = 0;i < mTargets.size();i++)
      if (mTargets[i].first == result.first)
        if (mTargets[i].second != 0)
          {
            mTargets[i].second->received(result.first, result.second);
            result.first = 0;
          }
        return result;
  }
}

/*@}*/

#endif
