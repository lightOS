/*
lightOS libkernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_SHARED_MEMORY_HPP
#define LIGHTOS_LIBKERNEL_SHARED_MEMORY_HPP

#include <libkernel/type.hpp>
#include <libkernel/kernel.h>
#include <libkernel/arch/syscall.h>

/*! \addtogroup libkernel libkernel */
/*@{*/

namespace libkernel
{
  /* Forward declaration */
  class message;

  /** A shared-memory object */
  class shared_memory
  {
    friend class libkernel::message;

    public:
      /** Type of the shared-memory object */
      enum type_t
      {
        /*! No shared-memory */
        none               = _LIBKERNEL_SHM_NONE,
        /*! transfer the ownership from one process to another */
        transfer_ownership = _LIBKERNEL_SHM_TRANSFER_OWNERSHIP,
        /*! share the shared-memory with another process read-only */
        read_only          = _LIBKERNEL_SHM_READ_ONLY,
        /*! share the shared-memory with another process read/write */
        write              = _LIBKERNEL_SHM_WRITE,
        /*! share the shared-memory with another process mutual-exclusive write */
        mutual_write       = _LIBKERNEL_SHM_MUTUAL_WRITE
      };

      /*! The constructor */
      shared_memory() = default;
      /*! The constructor. Creates a shared-memory entity of a specific size
       *\param[in] size the size */
      inline shared_memory(size_t size);
      /*! The copy-constructor
       *\note the original shared-memory object becomes invalid*/
      inline shared_memory(const shared_memory& x);
      /*! The assignment operator */
      inline shared_memory& operator = (const shared_memory& x);
      /*! The destructor */
      inline ~shared_memory();

      /*! Get the address of the shared-memory region
       *\return pointer to the shared-memory region */
      template<class T>
      inline T* address() const;
      /*! Get the id */
      inline shared_memory_id_t id() const;
      /*! Get the size */
      inline size_t size() const;
      /*! Destroy the shared memory */
      inline void destroy();
      /*! The bool() operator */
      inline operator bool() const;

    protected:
      /*! The constructor */
      inline shared_memory(_LIBKERNEL_shared_memory_t shm);
      inline void assign(const shared_memory& x);

    private:
      /*! The shared-memory */
      _LIBKERNEL_shared_memory_t m_shm;
  };



  /*
   * Implementation of shared_memory
   */

  shared_memory::shared_memory(size_t size)
    : m_shm()
  {
    m_shm = _LIBKERNEL_create_shared_memory(size);
  }

  shared_memory::shared_memory(const shared_memory& x)
    : m_shm()
  {
    assign(x);
  }

  shared_memory& shared_memory::operator = (const shared_memory& x)
  {
    assign(x);
    return *this;
  }

  shared_memory::~shared_memory()
  {
    destroy();
  }

  template<class T>
  T* shared_memory::address() const
  {
    return reinterpret_cast<T*>(m_shm.address);
  }

  shared_memory_id_t shared_memory::id() const
  {
    return m_shm.id;
  }

  size_t shared_memory::size() const
  {
    return m_shm.size;
  }

  void shared_memory::destroy()
  {
    _LIBKERNEL_destroy_shared_memory(&m_shm);
  }

  shared_memory::operator bool() const
  {
    if (m_shm.id == 0)
      return false;
    return true;
  }

  shared_memory::shared_memory(_LIBKERNEL_shared_memory_t shm)
    : m_shm(shm)
  {
  }

  void shared_memory::assign(const shared_memory& x)
  {
    m_shm.address = x.m_shm.address;
    m_shm.size = x.m_shm.size;
    m_shm.id = x.m_shm.id;

    shared_memory& tmp = const_cast<shared_memory&>(x);
    tmp.m_shm.address = 0;
    tmp.m_shm.size = 0;
    tmp.m_shm.id = 0;
  }
}

/*@}*/

#endif
