/*
lightOS libkernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_TYPE_H
#define LIGHTOS_LIBKERNEL_TYPE_H

#include <libarch/type.h>
#include <libkernel/arch/syscall.h>

/*! \addtogroup libkernel libkernel */
/*@{*/

typedef _LIBKERNEL_syscall_param_t _LIBKERNEL_port_id_t;
typedef _LIBKERNEL_syscall_param_t _LIBKERNEL_thread_id_t;
typedef _LIBKERNEL_syscall_param_t _LIBKERNEL_process_id_t;
typedef _LIBKERNEL_syscall_param_t _LIBKERNEL_shared_memory_id_t;
typedef _LIBKERNEL_syscall_param_t _LIBKERNEL_message_param_t;

typedef struct
{
  /*! The ID */
  _LIBKERNEL_shared_memory_id_t id;
  /*! The address */
  void *address;
  /*! The size */
  _LIBKERNEL_syscall_param_t size;
} _LIBKERNEL_shared_memory_t;

typedef struct
{
  /*! Serial line I/O ports */
  _LIBKERNEL_syscall_param_t serial[4];
  /*! Serial line used by the kernel */
  _LIBKERNEL_syscall_param_t kernel_serial;
} _LIBKERNEL_system_config_t;

struct _LIBKERNEL_message_t
{
  #ifdef __cplusplus
    _LIBKERNEL_message_t() = default;

    inline _LIBKERNEL_message_t(_LIBKERNEL_port_id_t _port,
                                _LIBKERNEL_message_param_t _type,
                                _LIBKERNEL_message_param_t _param1,
                                _LIBKERNEL_message_param_t _param2,
                                _LIBKERNEL_message_param_t _param3)
      : type(_type), param1(_param1), param2(_param2), param3(_param3), port(_port){}
  #endif

  /*! The type */
  _LIBKERNEL_message_param_t type;
  /*! The first parameter */
  _LIBKERNEL_message_param_t param1;
  /*! The second parameter */
  _LIBKERNEL_message_param_t param2;
  /*! The third parameter */
  _LIBKERNEL_message_param_t param3;
  /*! The portId */
  _LIBKERNEL_port_id_t port;
} __attribute__((packed));

typedef struct _LIBKERNEL_message_t _LIBKERNEL_message_t;

/*@}*/

#endif
