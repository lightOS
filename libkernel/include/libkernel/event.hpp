/*
lightOS libkernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_EVENT_HPP
#define LIGHTOS_LIBKERNEL_EVENT_HPP

#include <libkernel/event.h>

/*! \addtogroup libkernel libkernel */
/*@{*/

namespace libkernel
{
  enum event
  {
    irq             = _LIBKERNEL_EVENT_IRQ,
    log_update      = _LIBKERNEL_EVENT_LOG_UPDATE,
    create_process  = _LIBKERNEL_EVENT_CREATE_PROCESS,
    destroy_process = _LIBKERNEL_EVENT_DESTROY_PROCESS,
    timer           = _LIBKERNEL_EVENT_TIMER
  };

  enum irq_priority
  {
    floppy        = 0x03,
    keyboard      = 0x04,
    serial        = 0x05,
    mouse         = 0x06,
    network       = 0x07
  };
}

/*@}*/

#endif
