/*
lightOS libkernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_X86_64_SYSCALL_H
#define LIGHTOS_LIBKERNEL_X86_64_SYSCALL_H

#include <libarch/type.h>

/*! \addtogroup libkernel libkernel */
/*@{*/

/*! Define the type of a syscall parameter */
typedef unsigned long _LIBKERNEL_syscall_param_t;

#define _LIBKERNEL_SYSCALL0_0(number) \
  __asm__ volatile("syscall":: \
                   "a" (number): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL0_1(number, return0) \
  __asm__ volatile("syscall": \
                   "=a" (return0): \
                   "a" (number): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL0_2(number, return0, return1) \
  __asm__ volatile("syscall": \
                   "=a" (return0), "=D" (return1): \
                   "a" (number): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL0_3(number, return0, return1, return2) \
  __asm__ volatile("syscall": \
                   "=a" (return0), "=D" (return1), "=S" (return2): \
                   "a" (number): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL0_4(number, return0, return1, return2, return3) \
  __asm__ volatile("syscall": \
                   "=a" (return0), "=D" (return1), "=S" (return2), "=d" (return3): \
                   "a" (number): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL0_5(number, return0, return1, return2, return3, return4) \
  register _LIBKERNEL_syscall_param_t __return4 __asm__("r8"); \
  __asm__ volatile("syscall": \
                   "=a" (return0), "=D" (return1), "=S" (return2), "=d" (return3), "=r" (__return4): \
                   "a" (number): \
                   "rcx", "r11"); \
  return4 = __return4
#define _LIBKERNEL_SYSCALL0_6(number, return0, return1, return2, return3, return4, return5) \
  register _LIBKERNEL_syscall_param_t __return4 __asm__("r8"); \
  register _LIBKERNEL_syscall_param_t __return5 __asm__("r9"); \
  __asm__ volatile("syscall": \
                   "=a" (return0), "=D" (return1), "=S" (return2), "=d" (return3), "=r" (__return4), "=r" (__return5): \
                   "a" (number): \
                   "rcx", "r11"); \
  return4 = __return4; \
  return5 = __return5

#define _LIBKERNEL_SYSCALL1_0(number, param0) \
  __asm__ volatile("syscall":: \
                   "a" (number), "D" (param0): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL1_1(number, param0, return0) \
  __asm__ volatile("syscall": \
                   "=a" (return0): \
                   "a" (number), "D" (param0) : \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL1_2(number, param0, return0, return1) \
  __asm__ volatile("syscall": \
                   "=a" (return0), "=D" (return1): \
                   "a" (number), "D" (param0): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL1_3(number, param0, return0, return1, return2) \
  __asm__ volatile("syscall": \
                   "=a" (return0), "=D" (return1), "=S" (return2): \
                   "a" (number), "D" (param0): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL1_4(number, param0, return0, return1, return2, return3) \
  __asm__ volatile("syscall": \
                   "=a" (return0), "=D" (return1), "=S" (return2), "=d" (return3): \
                   "a" (number), "D" (param0): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL1_5(number, param0, return0, return1, return2, return3, return4) \
  register uint64_t __return4 __asm__("r8"); \
  __asm__ volatile("syscall": \
                   "=a" (return0), "=D" (return1), "=S" (return2), "=d" (return3), "=r" (__return4): \
                   "a" (number), "D" (param0): \
                   "rcx", "r 11"); \
  return4 = __return4
#define _LIBKERNEL_SYSCALL1_6(number, param0, return0, return1, return2, return3, return4, return5) \
  register _LIBKERNEL_syscall_param_t __return4 __asm__("r8"); \
  register _LIBKERNEL_syscall_param_t __return5 __asm__("r9"); \
  __asm__ volatile("syscall": \
                   "=a" (return0), "=D" (return1), "=S" (return2), "=d" (return3), "=r" (__return4), "=r" (__return5): \
                   "a" (number), "D" (param0): \
                   "rcx", "r11"); \
  return4 = __return4; \
  return5 = __return5

#define _LIBKERNEL_SYSCALL2_0(number, param0, param1) \
  __asm__ volatile("syscall":: \
                   "a" (number), "D" (param0), "S" (param1): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL2_1(number, param0, param1, return0) \
  __asm__ volatile("syscall": \
                   "=a" (return0): \
                   "a" (number), "D" (param0), "S" (param1): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL2_3(number, param0, param1, return0, return1, return2) \
  __asm__ volatile("syscall": \
                   "=a" (return0), "=D" (return1), "=S" (return2): \
                   "a" (number), "D" (param0), "S" (param1): \
                   "rcx", "r11")

#define _LIBKERNEL_SYSCALL3_0(number, param0, param1, param2) \
  __asm__ volatile("syscall":: \
                   "a" (number), "D" (param0), "S" (param1), "d" (param2): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL3_1(number, param0, param1, param2, return0) \
  __asm__ volatile("syscall": \
                   "=a" (return0): \
                   "a" (number), "D" (param0), "S" (param1), "d" (param2): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL3_2(number, param0, param1, param2, return0, return1) \
  __asm__ volatile("syscall": \
                   "=a" (return0), "=D" (return1): \
                   "a" (number), "D" (param0), "S" (param1), "d" (param2): \
                   "rcx", "r11")

#define _LIBKERNEL_SYSCALL4_0(number, param0, param1, param2, param3) \
  register _LIBKERNEL_syscall_param_t __param3 __asm__("r8") = param3; \
  __asm__ volatile("syscall":: \
                   "a" (number), "D" (param0), "S" (param1), "d" (param2), "r" (__param3): \
                   "rcx", "r11")
#define _LIBKERNEL_SYSCALL4_1(number, param0, param1, param2, param3, return0) \
  register _LIBKERNEL_syscall_param_t __param3 __asm__("r8") = param3; \
  __asm__ volatile("syscall": \
                   "=a" (return0): \
                   "a" (number), "D" (param0), "S" (param1), "d" (param2), "r" (__param3): \
                   "rcx", "r11")

#define _LIBKERNEL_SYSCALL5_1(number, param0, param1, param2, param3, param4, return0) \
  register _LIBKERNEL_syscall_param_t __param3 __asm__("r8") = param3; \
  register _LIBKERNEL_syscall_param_t __param4 __asm__("r9") = param4; \
  __asm__ volatile("syscall": \
                   "=a" (return0): \
                   "a" (number), "D" (param0), "S" (param1), "d" (param2), "r" (__param3), "r" (__param4): \
                   "rcx", "r11")

#define _LIBKERNEL_SYSCALL6_1(number, param0, param1, param2, param3, param4, param5, return0) \
  register _LIBKERNEL_syscall_param_t __param3 __asm__("r8") = param3; \
  register _LIBKERNEL_syscall_param_t __param4 __asm__("r9") = param4; \
  __asm__ volatile("movq %7, %%r10\n" \
                   "syscall": \
                   "=a" (return0): \
                   "a" (number), "D" (param0), "S" (param1), "d" (param2), "r" (__param3), "r" (__param4), "g" (param5): \
                   "rcx", "r10", "r11")
#define _LIBKERNEL_SYSCALL6_6(number, param0, param1, param2, param3, param4, param5, return0, return1, return2, return3, return4, return5) \
  __asm__ volatile("movq %10, %%r8\n" \
                   "movq %11, %%r9\n" \
                   "movq %12, %%r10\n" \
                   "syscall\n" \
                   "movq %%r8, %4\n" \
                   "movq %%r9, %5\n": \
                   "=a" (return0), "=D" (return1), "=S" (return2), "=d" (return3), "=g" (return4), "=g" (return5): \
                   "a" (number), "D" (param0), "S" (param1), "d" (param2), "g" (param3), "g" (param4), "g" (param5): \
                   "rcx", "r8", "r9", "r10", "r11");

/*@}*/

#endif
