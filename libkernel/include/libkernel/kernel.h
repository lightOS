/*
lightOS libkernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_KERNEL_H
#define LIGHTOS_LIBKERNEL_KERNEL_H

#include <libarch/type.h>
#include <libkernel/arch/syscall.h>
#include <libkernel/type.h>
#include <libkernel/log.h>

/*! \addtogroup libkernel libkernel */
/*@{*/

#define _LIBKERNEL_THREAD_SUSPENDED              0x00000001
#define _LIBKERNEL_THREAD_MASK                   0x0000FFFF
#define _LIBKERNEL_PROCESS_SERVER                0x00010000
#define _LIBKERNEL_PROCESS_MASK                  0xFFFF0000
#define _LIBKERNEL_MEMORY_NONE                   0x00
#define _LIBKERNEL_MEMORY_BELOW_1MB              0x01
#define _LIBKERNEL_MEMORY_BELOW_16MB             0x02
#define _LIBKERNEL_MEMORY_BELOW_4GB              0x03
#define _LIBKERNEL_MEMORY_TYPE_MASK              0x0F
#define _LIBKERNEL_MEMORY_CONTINUOUS             0x10

#ifdef __cplusplus
  extern "C"
  {
#endif

    /*! Get the lightOS kernel version
     *\param[in,out] major major kernel version
     *\param[in,out] minor minor kernel version */
    void _LIBKERNEL_kernel_version(_LIBKERNEL_syscall_param_t *major,
                                   _LIBKERNEL_syscall_param_t *minor);

    /*! Extend the heap a specific amount of pages
     *\param[in] n the number of pages
     *\return pointer to the beginning of the allocated memory */
    void *_LIBKERNEL_extend_heap(_LIBKERNEL_syscall_param_t size);

    /*! Get information about the current memory usage
     *\param[in,out] total the total amount of memory
     *\param[in,out] free the free amount of memory */
    void _LIBKERNEL_get_memory_info(_LIBKERNEL_syscall_param_t *total,
                                    _LIBKERNEL_syscall_param_t *free);

    /*! Get the length of the command-line arguments
     *\return the length of the command-line arguments */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_get_commandline_length(void);

    /*! Get the command-line arguments
     *\param[in,out] cmdline memory where the command-line arguments should be written to */
    void _LIBKERNEL_get_commandline(char *cmdline);

    //
    // Time functions
    //

    /*! Get the current time
     *\param [in,out] hour the current hour
     *\param [in,out] minute the current minute
     *\param [in,out] second the current second */
    void _LIBKERNEL_get_time(_LIBKERNEL_syscall_param_t *hour,
                             _LIBKERNEL_syscall_param_t *minute,
                             _LIBKERNEL_syscall_param_t *second);

    /*! Get the current date
     *\param[in,out] dayofweek the day of week
     *\param[in,out] dayofmonth the day of month
     *\param[in,out] month the month
     *\param[in,out] year the year
     *\param[in,out] summertime summertime? */
    void _LIBKERNEL_get_date(_LIBKERNEL_syscall_param_t *dayofweek,
                             _LIBKERNEL_syscall_param_t *dayofmonth,
                             _LIBKERNEL_syscall_param_t *month,
                             _LIBKERNEL_syscall_param_t *year,
                             _LIBKERNEL_syscall_param_t *summertime);

    //
    // IPC
    //

    /*! Create port
     *\return the portId, if successfull, 0 otherwise */
    _LIBKERNEL_port_id_t _LIBKERNEL_create_port(void);

    /*! Destroy a port
     *\param[in] id the portId */
    void _LIBKERNEL_destroy_port(_LIBKERNEL_port_id_t id);

    /*! Create a specific port
     *\param[in] id the portId
     *\return the portId, if successfull, 0 otherwise */
    _LIBKERNEL_port_id_t _LIBKERNEL_create_specific_port(_LIBKERNEL_port_id_t id);

    /*! Peek a message
     *\param[in] id the portId
     *\param[in,out] msg pointer to the message
     *\return nonezero, if a message is available */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_peek_message(_LIBKERNEL_port_id_t id,
                                                       _LIBKERNEL_message_t *msg);

    /*! Get a message
     *\param[in] id the portId
     *\param[in,out] msg pointer to the message
     *\return nonezero, if successfull */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_get_message(_LIBKERNEL_port_id_t id,
                                                      _LIBKERNEL_message_t *msg);

    /*! Wait for a specific message
     *\param[in] id the portId
     *\param[in,out] msg pointer to the message */
    _LIBKERNEL_port_id_t _LIBKERNEL_wait_message(_LIBKERNEL_message_t *msg);

    /*! Transfer a port to another process
     *\param[in] port the port
     *\param[in] pid the process to transfer to
     *\return nonezero, if successfull */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_transfer_port(_LIBKERNEL_port_id_t port,
                                                        _LIBKERNEL_process_id_t pid);

    /*! Add a wait for a specfic message on a specific port
     *\param[in] id the port's id
     *\param[in] msg the message
     *\return nonezero, if successfull */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_add_wait_message(_LIBKERNEL_port_id_t id,
                                                           const _LIBKERNEL_message_t *msg);

    /*! Send a message
     *\param[in] id the portId
     *\param[in] msg reference to the message
     *\return nonezero, if successfull */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_send_message(_LIBKERNEL_port_id_t id,
                                                       const _LIBKERNEL_message_t *msg);

    /*! Get information about the port
     *\param[in] id the portId
     *\param[in,out] owner the owning process
     *\param[in,out] waiter the waiting thread, if any */
    void _LIBKERNEL_get_port_info(_LIBKERNEL_port_id_t id,
                                  _LIBKERNEL_process_id_t *owner,
                                  _LIBKERNEL_thread_id_t *waiter);

    /*! Set the standard port 
     *\param[in] pid the process id
     *\param[in] stdPort the standard port
     *\param[in] Port the port
     *\param[in] fsPort the filesystem port
     *\return 0 if failed */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_set_standard_port(_LIBKERNEL_process_id_t pid,
                                                            _LIBKERNEL_port_id_t stdPort,
                                                            _LIBKERNEL_port_id_t Port,
                                                            _LIBKERNEL_port_id_t fsPort);

    /*! Get the standard port 
     *\param[in] pid the process id
     *\param[in] stdPort the standard port
     *\param[out] Port the port
     *\param[out] fsPort the filesystem port */
    void _LIBKERNEL_get_standard_port(_LIBKERNEL_process_id_t pid,
                                      _LIBKERNEL_port_id_t stdPort,
                                      _LIBKERNEL_port_id_t *Port,
                                      _LIBKERNEL_port_id_t *fsPort);

    /*! Register event
     *\param[in] id the portId
     *\param[in] type the event type
     *\param[in] param1 first event parameter
     *\param[in] param2 second event parameter
     *\return nonezero, if successfull */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_register_event(_LIBKERNEL_port_id_t id,
                                                         _LIBKERNEL_syscall_param_t type,
                                                         _LIBKERNEL_syscall_param_t param1,
                                                         _LIBKERNEL_syscall_param_t param2);

    /*! Acknoledge event
     *\param[in] id the portId
     *\param[in] type the event type
     *\param[in] param1 first event parameter */
    void _LIBKERNEL_acknoledge_event(_LIBKERNEL_port_id_t id,
                                     _LIBKERNEL_syscall_param_t type,
                                     _LIBKERNEL_syscall_param_t param);

    /*! Unregister event
     *\param[in] id the portId
     *\param[in] type the event type
     *\return nonezero, if successfull */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_unregister_event(_LIBKERNEL_port_id_t id,
                                                           _LIBKERNEL_syscall_param_t type);

    //
    // Misc
    //

    /*! Get the number of ticks (10ms) since system bootup
     *\return number of ticks */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_get_tick_count(void);

    //
    // Shared-memory
    //

    /*! Create shared-memory
     *\param[in] size the size
     *\param[in,out] address the address
     *\return the shared-memory */
    _LIBKERNEL_shared_memory_t _LIBKERNEL_create_shared_memory(_LIBKERNEL_syscall_param_t size);

    /*! Destroy shared-memory
     *\param[in] id the shared-memory id */
    void _LIBKERNEL_destroy_shared_memory(_LIBKERNEL_shared_memory_t *shm);

    //
    // Process Management
    //

    /*! Create a new thread
     *\param[in] entryPoint the entry-point for the thread
     *\param[in] parameter the parameter
     *\param[in] flags thread creation flags
     *\return the thread's id or 0 */
    _LIBKERNEL_thread_id_t _LIBKERNEL_create_thread(void (*thread_entry)(_LIBKERNEL_syscall_param_t),
                                                    _LIBKERNEL_syscall_param_t parameter,
                                                    _LIBKERNEL_syscall_param_t flags);

    /*! Suspend a thread
     *\param[in] id the thread's id
     *\return nonezero, if successfull */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_suspend_thread(_LIBKERNEL_thread_id_t id);

    /*! Resume a thread
     *\param[in] id the thread's id
     *\return nonezero, if successfull */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_resume_thread(_LIBKERNEL_thread_id_t id);

    /*! Destroy an existing thread
     *\param[in] id the thread's id
     *\return nonezero, if successfull */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_destroy_thread(_LIBKERNEL_thread_id_t id);

    /*! Destroy a process
     *\param[in] id the process's id
     *\return nonezero, if successfull */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_destroy_process(_LIBKERNEL_process_id_t id);

    /*! Get the process's id
     *\return this process's id */
    _LIBKERNEL_process_id_t _LIBKERNEL_get_process_id(void);

    /*! Get the thread's id
     *\return this thread's id */
    _LIBKERNEL_thread_id_t _LIBKERNEL_get_thread_id(void);

    /*! Get the number of processes
     *\param[in] number of processes */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_get_process_count(void);

    /*! Enumerate the processes
     *\param[in] i index
     *\return the processId */
    _LIBKERNEL_process_id_t _LIBKERNEL_enum_processes(_LIBKERNEL_syscall_param_t i);

    /*! Get the length of the process name from the process's id
     *\param[in] id the process's id
     *\return the process name length */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_get_process_name_length(_LIBKERNEL_process_id_t id);

    /*! Get process name
     *\param[in] id the processId
     *\param[in,out] name the process name */
    void _LIBKERNEL_get_process_name(_LIBKERNEL_process_id_t id,
                                     char *name);

    /*! Get the process's memory usage
     *\param[in] id the process's id
     *\return the amount of memory used by this process */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_get_process_memory_usage(_LIBKERNEL_process_id_t id);

    /*! Get the number of free memory ranges within the range-allocator
     *\param[in,out] capacity ???
     *\todo ???
     *\return number of memory ranges within the range-allocator */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_range_allocator_count(_LIBKERNEL_syscall_param_t *capacity);

    /*! Enumerate the memory ranges within the range-allocator
     *\param[in] index the index
     *\param[in,out] address pointer to the physical beginning of the memory range
     *\param[in,out] size size of the region in byte */
    void _LIBKERNEL_enum_range_allocator(_LIBKERNEL_syscall_param_t index,
                                         _LIBKERNEL_syscall_param_t *address,
                                         _LIBKERNEL_syscall_param_t *size);

    //
    // VBE
    //

    /*! Get the VBE (= Video BIOS Extensions) version
     *\param[in,out] major the major version
     *\param[in,out] minor the minor version */
    void _LIBKERNEL_vbe_get_version(_LIBKERNEL_syscall_param_t *major,
                                    _LIBKERNEL_syscall_param_t *minor);

    /*! Get the number of supported modes
     *\return the number of supported modes */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_vbe_get_mode_count(void);

    /*! Enumerate the supported modes
     *\param[in] index
     *\param[in,out] width the mode's width
     *\param[in,out] height the mode's height
     *\param[in,out] bpp the mode's bits per pixel */
    void _LIBKERNEL_vbe_enumerate_mode(_LIBKERNEL_syscall_param_t index,
                                       _LIBKERNEL_syscall_param_t *width,
                                       _LIBKERNEL_syscall_param_t *height,
                                       _LIBKERNEL_syscall_param_t *bpp);

    /*! Set a specific video mode
     *\param[in] index the mode's index
     *\return the physical address of the framebuffer */
    void *_LIBKERNEL_vbe_set_mode(_LIBKERNEL_syscall_param_t index);

    //
    // I/O Ports
    //

    /*! Request access to an I/O port range
     *\param[in] port the first I/O port in the range
     *\param[in] count the number of ports after the first one
     *\return nonezero, if successfull */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_allocate_io_port_range(_LIBKERNEL_syscall_param_t port,
                                                                 _LIBKERNEL_syscall_param_t count);

    /*! Free a previously requested I/O port range
     *\param[in] port the first I/O port in the range
     *\param[in] count the number of ports after tge first one */
    void _LIBKERNEL_free_io_port_range(_LIBKERNEL_syscall_param_t port,
                                       _LIBKERNEL_syscall_param_t count);

    //
    // ISA DMA
    //

    /*! Setup a DMA channel
     *\param[in] channel the dma channel
     *\param[in] address physical address of the buffer
     *\param[in] count number of bytes
     *\param[in] mode the dma mode
     *\return nonezero, if successfull */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_setup_dma_channel(_LIBKERNEL_syscall_param_t channel,
                                                            void *address,
                                                            _LIBKERNEL_syscall_param_t count,
                                                            _LIBKERNEL_syscall_param_t mode);

    //
    // Process creation
    //

    /*! Execute a memory region
     *\param[in] data pointer to the memory region
     *\param[in] size size of the memory region
     *\param[in] cmdline the command-line
     *\param[in] flags process/thread creation flags
     *\return the processId, if successfull, 0 otherwise */
    _LIBKERNEL_process_id_t _LIBKERNEL_execute(const void *data,
                                               _LIBKERNEL_syscall_param_t size,
                                               const char *cmdline,
                                               _LIBKERNEL_syscall_param_t flags,
                                               char *libName);

    //
    // Physical memory-management
    //

    /*! Get the physical address from a virtual address
     *\param[in] address the virtual address
     *\return the physical address */
    void *_LIBKERNEL_get_physical_address(const void *address);

    /*! Allocate a memory region with special constraints
     *\param[in] size the size of the region in bytes
     *\param[in] type the constraints
     *\param[in] physical if nonezero the physical address of the beginning of the memory region
     *\return the virtual address of the memory region*/
    void *_LIBKERNEL_allocate_memory_region(_LIBKERNEL_syscall_param_t size,
                                            _LIBKERNEL_syscall_param_t type,
                                            void *physical);

    /*! Free a special memory region
     *\param[in] address pointer to the beginning of the memory region in virtual address space */
    void _LIBKERNEL_free_memory_region(void *address);

    //
    // System configuration
    //

    /*! Get this system's configuration
     *\param[in,out] config the system's configuration */
    void _LIBKERNEL_get_system_configuration(_LIBKERNEL_system_config_t *config);

    //
    // Libraries
    //

    /*! Get the number of loaded shared-libraries
     *\return the number of loaded shared-libraries */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_get_library_count(void);

    /*! Get the length of the name of a shared-library
     *\param[in] index the index
     *\return the length of the name */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_get_library_name_length(_LIBKERNEL_syscall_param_t index);

    /*! Get information regarding a shared-library
     *\param[in] index the index
     *\param[in,out] name the name
     *\param[in,out] address address of the shared-library beginning in virtual address space
     *\param[in,out] size the size of the shared-library */
    void _LIBKERNEL_get_library_info(_LIBKERNEL_syscall_param_t index,
                                     char *name,
                                     _LIBKERNEL_syscall_param_t length,
                                     _LIBKERNEL_syscall_param_t *address,
                                     _LIBKERNEL_syscall_param_t *size);

    //
    // Signal handler
    //

    /*! Set the signal handler
     *\param[in] func the new signal handler */
    void _LIBKERNEL_set_signal_handler(void (*func)(int));

    /*! Signal the end of the signal processing */
    void _LIBKERNEL_signal_handler_end(void);

    /*! Signal a process
     *\param[in] pid the process's id
     *\param[in] sig the signal number */
    int _LIBKERNEL_signal(_LIBKERNEL_process_id_t pid,
                          int sig);

    //
    // Kernel log
    //
    _LIBKERNEL_syscall_param_t _LIBKERNEL_get_log_entry(_LIBKERNEL_syscall_param_t index,
                                                        _LIBKERNEL_syscall_param_t size,
                                                        char* address,
                                                        enum _LIBKERNEL_log_level* level);

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
