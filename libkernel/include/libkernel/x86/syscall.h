/*
lightOS libkernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_X86_SYSCALL_H
#define LIGHTOS_LIBKERNEL_X86_SYSCALL_H

#include <libarch/type.h>

/*! \addtogroup libkernel libkernel */
/*@{*/

/*! Define the type of a syscall parameter */
typedef unsigned long _LIBKERNEL_syscall_param_t;

#define _LIBKERNEL_SYSCALL0_0(number) \
  __asm__ volatile("int $0xFF":: \
                   "a" (number));
#define _LIBKERNEL_SYSCALL0_1(number, return0) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0): \
                   "a" (number))
#define _LIBKERNEL_SYSCALL0_2(number, return0, return1) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0), "=b" (return1): \
                   "a" (number))
#define _LIBKERNEL_SYSCALL0_3(number, return0, return1, return2) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0), "=b" (return1), "=c" (return2): \
                   "a" (number))
#define _LIBKERNEL_SYSCALL0_4(number, return0, return1, return2, return3) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0), "=b" (return1), "=c" (return2), "=d" (return3): \
                   "a" (number))
#define _LIBKERNEL_SYSCALL0_5(number, return0, return1, return2, return3, return4) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0), "=b" (return1), "=c" (return2), "=d" (return3), "=S" (return4): \
                   "a" (number))
#define _LIBKERNEL_SYSCALL0_6(number, return0, return1, return2, return3, return4, return5) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0), "=b" (return1), "=c" (return2), "=d" (return3), "=S" (return4), "=D" (return5): \
                   "a" (number))

#define _LIBKERNEL_SYSCALL1_0(number, param0) \
  __asm__ volatile("int $0xFF":: \
                   "a" (number), "b" (param0))
#define _LIBKERNEL_SYSCALL1_1(number, param0, return0) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0): \
                   "a" (number), "b" (param0))
#define _LIBKERNEL_SYSCALL1_2(number, param0, return0, return1) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0), "=b" (return1): \
                   "a" (number), "b" (param0))
#define _LIBKERNEL_SYSCALL1_3(number, param0, return0, return1, return2) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0), "=b" (return1), "=c" (return2): \
                   "a" (number), "b" (param0))
#define _LIBKERNEL_SYSCALL1_4(number, param0, return0, return1, return2, return3) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0), "=b" (return1), "=c" (return2), "=d" (return3): \
                   "a" (number), "b" (param0))
#define _LIBKERNEL_SYSCALL1_5(number, param0, return0, return1, return2, return3, return4) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0), "=b" (return1), "=c" (return2), "=d" (return3), "=S" (return4): \
                   "a" (number), "b" (param0))
#define _LIBKERNEL_SYSCALL1_6(number, param0, return0, return1, return2, return3, return4, return5) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0), "=b" (return1), "=c" (return2), "=d" (return3), "=S" (return4), "=D" (return5): \
                   "a" (number), "b" (param0))

#define _LIBKERNEL_SYSCALL2_0(number, param0, param1) \
  __asm__ volatile("int $0xFF":: \
                   "a" (number), "b" (param0), "c" (param1))
#define _LIBKERNEL_SYSCALL2_1(number, param0, param1, return0) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0): \
                   "a" (number), "b" (param0), "c" (param1))
#define _LIBKERNEL_SYSCALL2_3(number, param0, param1, return0, return1, return2) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0), "=b" (return1), "=c" (return2): \
                   "a" (number), "b" (param0), "c" (param1))

#define _LIBKERNEL_SYSCALL3_0(number, param0, param1, param2) \
  __asm__ volatile("int $0xFF":: \
                   "a" (number), "b" (param0), "c" (param1), "d" (param2))
#define _LIBKERNEL_SYSCALL3_1(number, param0, param1, param2, return0) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0): \
                   "a" (number), "b" (param0), "c" (param1), "d" (param2))
#define _LIBKERNEL_SYSCALL3_2(number, param0, param1, param2, return0, return1) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0), "=b" (return1): \
                   "a" (number), "b" (param0), "c" (param1), "d" (param2))

#define _LIBKERNEL_SYSCALL4_0(number, param0, param1, param2, param3) \
  __asm__ volatile("int $0xFF":: \
                   "a" (number), "b" (param0), "c" (param1), "d" (param2), "S" (param3))
#define _LIBKERNEL_SYSCALL4_1(number, param0, param1, param2, param3, return0) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0): \
                   "a" (number), "b" (param0), "c" (param1), "d" (param2), "S" (param3))

#define _LIBKERNEL_SYSCALL5_1(number, param0, param1, param2, param3, param4, return0) \
  __asm__ volatile("int $0xFF": \
                   "=a" (return0): \
                   "a" (number), "b" (param0), "c" (param1), "d" (param2), "S" (param3), "D" (param4))

#ifdef __cplusplus
  extern "C"
  {
#endif

    _LIBKERNEL_syscall_param_t _LIBKERNEL_syscall_send(_LIBKERNEL_syscall_param_t,
                                                       _LIBKERNEL_syscall_param_t,
                                                       _LIBKERNEL_syscall_param_t,
                                                       _LIBKERNEL_syscall_param_t,
                                                       _LIBKERNEL_syscall_param_t,
                                                       _LIBKERNEL_syscall_param_t,
                                                       _LIBKERNEL_syscall_param_t);
    _LIBKERNEL_syscall_param_t _LIBKERNEL_syscall_add_wait(_LIBKERNEL_syscall_param_t,
                                                           _LIBKERNEL_syscall_param_t,
                                                           const void *);
    _LIBKERNEL_syscall_param_t _LIBKERNEL_syscall_wait(_LIBKERNEL_syscall_param_t,
                                                       void *);

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
