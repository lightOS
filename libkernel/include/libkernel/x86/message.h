/*
lightOS libkernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_X86_MESSAGE_H
#define LIGHTOS_LIBKERNEL_X86_MESSAGE_H

/*! \addtogroup libkernel libkernel */
/*@{*/

#define _LIBKERNEL_MAKE_MSG_TYPE(flags, type)    ((flags << 29) | (type & (~0xE0000000)))
#define _LIBKERNEL_GET_MSG_TYPE(msg)             (msg->type & (~0xE0000000))
#define _LIBKERNEL_GET_MSG_FLAGS(msg)            (msg->type >> 29)
#define _LIBKERNEL_GET_SHM_SIZE(msg)             (msg->param3 & 0x3FFFFF)
#define _LIBKERNEL_GET_SHM_ADDRESS(msg)          ((void*)(msg->param3 & 0xFFC00000))

/*@}*/

#endif
