/*
lightOS libkernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBKERNEL_MESSAGE_H
#define LIGHTOS_LIBKERNEL_MESSAGE_H

#include <libkernel/type.h>
#include <libkernel/arch/syscall.h>
#include <libkernel/arch/message.h>

/*! \addtogroup libkernel libkernel */
/*@{*/

/* Define shared-memory types */
#define _LIBKERNEL_SHM_NONE                      0
#define _LIBKERNEL_SHM_TRANSFER_OWNERSHIP        1
#define _LIBKERNEL_SHM_READ_ONLY                 2
#define _LIBKERNEL_SHM_WRITE                     3
#define _LIBKERNEL_SHM_MUTUAL_WRITE              4

/* Define port Id's */
#define _LIBKERNEL_PORT_NONE           0
#define _LIBKERNEL_PORT_STDIN          1
#define _LIBKERNEL_PORT_STDOUT         2
#define _LIBKERNEL_PORT_STDERR         3
#define _LIBKERNEL_PORT_KERNEL         4
#define _LIBKERNEL_PORT_INIT           5
#define _LIBKERNEL_PORT_PCI            6
#define _LIBKERNEL_PORT_VFS            7
#define _LIBKERNEL_PORT_GUI            8
#define _LIBKERNEL_PORT_NET            9
#define _LIBKERNEL_PORT_CONSOLE        10
#define _LIBKERNEL_PORT_USER           11

/* Define message ids */
#define _LIBKERNEL_MSG_PING                                0
#define _LIBKERNEL_MSG_PONG                                1
#define _LIBKERNEL_MSG_EVENT                               2
#define _LIBKERNEL_MSG_VFS_REGISTER_FS                     20
#define _LIBKERNEL_MSG_VFS_WAIT_FS_REGISTRATION            21
#define _LIBKERNEL_MSG_VFS_SET_WORKING_DIR                 22
#define _LIBKERNEL_MSG_VFS_GET_WORKING_DIR                 23
#define _LIBKERNEL_MSG_VFS_SET_CONSOLE                     24
#define _LIBKERNEL_MSG_VFS_GET_CONSOLE                     25
#define _LIBKERNEL_MSG_VFS_FIND_FS                         26
#define _LIBKERNEL_MSG_VFS_MOUNT                           27
#define _LIBKERNEL_MSG_VFS_UNMOUNT                         28
#define _LIBKERNEL_MSG_VFS_GET_MOUNT_COUNT                 29
#define _LIBKERNEL_MSG_VFS_GET_MOUNT_INFO                  30
#define _LIBKERNEL_MSG_FS_GET_INFO                         40
#define _LIBKERNEL_MSG_FS_CREATE_FILE                      41
#define _LIBKERNEL_MSG_FS_EXISTS_FILE                      42
#define _LIBKERNEL_MSG_FS_OPEN_FILE                        43
#define _LIBKERNEL_MSG_FS_FILE_INFO                        44
#define _LIBKERNEL_MSG_FS_READ_FILE                        45
#define _LIBKERNEL_MSG_FS_WRITE_FILE                       46
#define _LIBKERNEL_MSG_FS_CLOSE_FILE                       47
#define _LIBKERNEL_MSG_FS_DELETE_FILE                      48
#define _LIBKERNEL_MSG_FS_REANME_FILE                      49
#define _LIBKERNEL_MSG_FS_SET_FILE_SIZE                    50
#define _LIBKERNEL_MSG_FS_GET_FILENAME                     51
#define _LIBKERNEL_MSG_FS_GET_FILE_FLAGS                   52
#define _LIBKERNEL_MSG_FS_GET_OPENER_COUNT                 53
#define _LIBKERNEL_MSG_FS_ENUM_OPENER                      54
#define _LIBKERNEL_MSG_NET_REGISTER_NIC                    60
#define _LIBKERNEL_MSG_NET_UNREGISTER_NIC                  61
#define _LIBKERNEL_MSG_NET_ENUM_NIC                        62
#define _LIBKERNEL_MSG_NET_GET_NIC_INFO                    63
#define _LIBKERNEL_MSG_NET_NIC_RECEIVE                     64
#define _LIBKERNEL_MSG_NET_NIC_TRANSMIT                    65
#define _LIBKERNEL_MSG_NET_ARP_COUNT                       66
#define _LIBKERNEL_MSG_NET_ARP_ENTRY                       67
#define _LIBKERNEL_MSG_NET_ARP_CLEAR                       68
#define _LIBKERNEL_MSG_NET_DNS_COUNT                       69
#define _LIBKERNEL_MSG_NET_DNS_ENTRY                       70
#define _LIBKERNEL_MSG_NET_DNS_CLEAR                       71
#define _LIBKERNEL_MSG_NET_SET_NIC_INFO                    72
#define _LIBKERNEL_MSG_NET_SOCKET_CREATE                   73
#define _LIBKERNEL_MSG_NET_SOCKET_BIND                     74
#define _LIBKERNEL_MSG_NET_SOCKET_CONNECT                  75
#define _LIBKERNEL_MSG_NET_SOCKET_SEND                     76
#define _LIBKERNEL_MSG_NET_SOCKET_RECEIVE                  77
#define _LIBKERNEL_MSG_NET_SOCKET_DISCONNECT               78
#define _LIBKERNEL_MSG_NET_SOCKET_DESTROY                  79
#define _LIBKERNEL_MSG_NET_RESOLVE                         80
#define _LIBKERNEL_MSG_INIT_EXECUTE                        90
#define _LIBKERNEL_MSG_INIT_DONE                           91
#define _LIBKERNEL_MSG_PCI_ENUM_DEVICES                    100
#define _LIBKERNEL_MSG_PCI_GET_DEVICE_INFO                 101
#define _LIBKERNEL_MSG_PCI_FIND_DEVICE                     102
#define _LIBKERNEL_MSG_PCI_ENABLE_BUSMASTER                103
#define _LIBKERNEL_MSG_GUI_GET_VIDEO_MODE_COUNT            200
#define _LIBKERNEL_MSG_GUI_ENUM_VIDEO_MODES                201
#define _LIBKERNEL_MSG_GUI_GET_VIDEO_MODE                  202
#define _LIBKERNEL_MSG_GUI_SET_VIDEO_MODE                  203
#define _LIBKERNEL_MSG_GUI_GET_BACKGROUND_IMAGE            204
#define _LIBKERNEL_MSG_GUI_SET_BACKGROUND_IMAGE            205
#define _LIBKERNEL_MSG_GUI_CREATE_WINDOW                   206
#define _LIBKERNEL_MSG_GUI_MOVE_WINDOW                     207
#define _LIBKERNEL_MSG_GUI_RESIZE_WINDOW                   208
#define _LIBKERNEL_MSG_GUI_DESTROY_WINDOW                  209
#define _LIBKERNEL_MSG_GUI_DRAW_TEXT                       210
#define _LIBKERNEL_MSG_GUI_KEYBOARD                        211

// TODO: Used until we have a proper kernel log
#define _LIBKERNEL_MSG_LIBOS_WARNING                       1000

#ifdef __cplusplus
  extern "C"
  {
#endif

    /*! Create a message
     *\param[in] port the portId
     *\param[in] type the message id
     *\param[in] param1 the first parameter
     *\param[in] param2 the second parameter
     *\param[in] param3 the third parameter */
    _LIBKERNEL_message_t _LIBKERNEL_create_message(_LIBKERNEL_port_id_t port,
                                                   _LIBKERNEL_message_param_t type,
                                                   _LIBKERNEL_message_param_t param1,
                                                   _LIBKERNEL_message_param_t param2,
                                                   _LIBKERNEL_message_param_t param3);

    /*! Create a message with a shared-memory region
     *\param[in] port the portId
     *\param[in] type the message id
     *\param[in] param1 the first parameter
     *\param[in] shm the shared-memory region
     *\param[in] flags flags describing how to share the shared-memory region */
    _LIBKERNEL_message_t _LIBKERNEL_create_message_shm(_LIBKERNEL_port_id_t port,
                                                       _LIBKERNEL_message_param_t type,
                                                       _LIBKERNEL_message_param_t param1,
                                                       _LIBKERNEL_shared_memory_t *shm,
                                                       _LIBKERNEL_syscall_param_t flags);

    /*! Get the message type
     *\param[in] msg the message
     *\return the message type id */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_get_message_type(const _LIBKERNEL_message_t *msg);

    /*! Set the message type
     *\param[in] msg the message
     *\param[in] type the message type */
    void _LIBKERNEL_set_message_type(_LIBKERNEL_message_t *msg,
                                     _LIBKERNEL_message_param_t type);

    /*! Get the message attribute
     *\param[in] msg the message
     *\return the message attribute */
    _LIBKERNEL_syscall_param_t _LIBKERNEL_get_message_attribute(const _LIBKERNEL_message_t *msg);

    /*! Set the message attribute
     *\param[in] msg the message
     *\param[in] attr the attribute */
    void _LIBKERNEL_set_message_attribute(_LIBKERNEL_message_t *msg,
                                          _LIBKERNEL_message_param_t attr);

    /*! Get the shared-memory from a message
     *\param[in] msg the message
     *\return the shared-memory */
    _LIBKERNEL_shared_memory_t _LIBKERNEL_get_shared_memory(const _LIBKERNEL_message_t *msg);

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
