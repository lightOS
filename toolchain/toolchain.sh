#! /bin/bash

# Script parameters
# $1 architecture
# $2 grub_arch (if x86 or x86_64):
#    x86_64-unknown
#    i386-pc
# $3 --no-bochs

# Program versions
grub_version="0.97" # "0.97" or "svn" or "1.96"
nasm_version="2.06"
bochs_version="2.4.1" # "svn" or 2.4.1
qemu_version="0.10.5"
binutils_version="2.19.1"
gcc_version="4.4.0"        # or "snapshot-version"
gcc_version_clean="4.4.0"
gcc_is_snapshot="false"    # or "true"

curdir=`pwd`

if [ -z $1 ]
then
  echo "Error: No architecture selected"
  exit 1
fi

if [ $1 = "x86" ] ||
   [ $1 = "x86_64" ]
then
  if [ -z $2 ]
  then
    echo "Error: No grub architecture selected. Use \"x86_64-unknown\" or \"i386-pc\""
    exit 1
  fi
fi

# tool path
if [ ! -h bin ]
then
  echo "Enter the path where the cross compiled tools are stored: "
  read tool_path
  if [ ! -d $tool_path ]
  then
    echo "Error: Not a directory"
    exit 1
  fi
  ln -s $tool_path bin
fi

# grub
if [ $1 = "x86" ] ||
   [ $1 = "x86_64" ]
then
  if [ ! -e $curdir/bin/grub-$grub_version ]
  then
    if [ $grub_version = "svn" ]
    then
      echo "Downloading grub from svn"
      svn co svn://svn.savannah.gnu.org/grub/trunk/grub2
    else
      if [ ! -e grub-$grub_version.tar.gz ]
      then
        echo "Downloading grub-$grub_version"
        wget ftp://alpha.gnu.org/gnu/grub/grub-$grub_version.tar.gz
      fi
      echo "Unpacking grub-$grub_version"
      tar -xf grub-$grub_version.tar.gz
      if [ $grub_version = "0.97" ]
      then
        echo "Patching (x86-64 elf) grub-$grub_version"
        cp grub_x86_64.patch grub-$grub_version && cd grub-$grub_version && patch -p1 <grub_x86_64.patch && cd ..
      fi
    fi

    echo "Compiling/Installing grub-$grub_version"
    if [ $grub_version = "svn" ]
    then
      cd grub2 && ./configure --prefix=$curdir/bin && make && make install && make clean
    else
      cd grub-$grub_version && ./configure --prefix=$curdir/bin && make && make install && make clean
    fi

    status=$?
    cd ..

    if [ $grub_version = "svn" ]
    then
      rm -rf grub2
    else
      rm -rf grub-$grub_version
    fi

    if [ $status != 0 ]
    then
      echo "Error: Grub build failed"
      exit 1
    fi

    touch $curdir/bin/grub-$grub_version
  fi

  # Create links
  rm -f ../build/x86/boot/grub/stage1 ../build/x86/boot/grub/stage2_eltorito ../build/x86/boot/grub/stage2 ../build/x86_64/boot/grub/stage1 ../build/x86_64/boot/grub/stage2_eltorito ../build/x86_64/boot/grub/stage2
  ln -s $curdir/bin/lib/grub/$2/stage1 ../build/x86/boot/grub/stage1
  ln -s $curdir/bin/lib/grub/$2/stage2_eltorito ../build/x86/boot/grub/stage2_eltorito
  ln -s $curdir/bin/lib/grub/$2/stage2 ../build/x86/boot/grub/stage2
  ln -s $curdir/bin/lib/grub/$2/stage1 ../build/x86_64/boot/grub/stage1
  ln -s $curdir/bin/lib/grub/$2/stage2_eltorito ../build/x86_64/boot/grub/stage2_eltorito
  ln -s $curdir/bin/lib/grub/$2/stage2 ../build/x86_64/boot/grub/stage2
fi

# nasm
if [ $1 = "x86" ] ||
   [ $1 = "x86_64" ]
then
  if [ ! -e $curdir/bin/nasm-$nasm_version ]
  then
    if [ ! -e nasm-$nasm_version.tar.bz2 ]
    then
      echo "Downloading nasm-$nasm_version"
      wget http://www.nasm.us/pub/nasm/releasebuilds/$nasm_version/nasm-$nasm_version.tar.bz2
    fi

    echo "Unpacking nasm-$nasm_version"
    tar -xf nasm-$nasm_version.tar.bz2

    echo "Compiling/Installing nasm-$nasm_version"
    cd nasm-$nasm_version && ./configure --prefix=$curdir/bin && make && make install && make clean

    status=$?
    cd ..
    rm -rf nasm-$nasm_version

    if [ $status != 0 ]
    then
      echo "Error: Nasm build failed"
      exit 1
    fi

    touch $curdir/bin/nasm-$nasm_version
  fi

  # Create links
  rm -f nasm
  ln -s $curdir/bin/bin/nasm nasm
fi

# bochs
if [ -z $3 ] ||
   ! [ $3 = "--no-bochs" ]
then
  if [ $1 = "x86" ] ||
    [ $1 = "x86_64" ]
  then
    if [ ! -e bin/bochs-$bochs_version ]
    then
      if [ $bochs_version = "svn" ]
      then
        echo "Downloading bochs from svn"
        cvs -z3 -d:pserver:anonymous@bochs.cvs.sourceforge.net:/cvsroot/bochs co -P bochs
      else if [ ! -e bochs-$bochs_version.tar.gz ]
      then
        echo "Downloading bochs-$bochs_version"
        wget http://garr.dl.sourceforge.net/sourceforge/bochs/bochs-$bochs_version.tar.gz
      fi
      echo "Unpacking bochs-$bochs_version"
      tar -xf bochs-$bochs_version.tar.gz
    fi

    echo "Compile/Installing bochs-$bochs_version"
    flags="--enable-x86-64 --enable-apic --enable-ne2000 --enable-acpi --enable-pci --enable-usb --enable-mtrr --enable-debugger --enable-disasm --enable-all-optimizations --enable-vbe --enable-clgd54xx --enable-vme --enable-sse=4 --enable-sse-extension --enable-x86-debugger --enable-smp --enable-sb16=linux"

      if [ $bochs_version = "svn" ]
      then
        cd bochs && ./configure --prefix=$curdir/bin $flags && make && make install && make clean
      else
        cd bochs-$bochs_version && ./configure --prefix=$curdir/bin $flags && make && make install && make clean
      fi

      status=$?
      cd ..

      if [ $bochs_version = "svn" ]
      then
        rm -rf bochs
      else
        rm -rf bochs-$bochs_version
      fi

      if [ $status != 0 ]
      then
        echo "Error: Bochs build failed"
        exit 1
      fi
      touch $curdir/bin/bochs-$bochs_version
    fi
  fi
fi

rm -f x86-bochs x86_64-bochs
ln -s $curdir/bin/bin/bochs x86-bochs
ln -s $curdir/bin/bin/bochs x86_64-bochs

# qemu
if [ ! -e $curdir/bin/qemu-$qemu_version ]
then
  if [ ! -e qemu-$qemu_version.tar.gz ]
  then
    echo "Downloading qemu-$qemu_version"
    wget http://savannah.nongnu.org/download/qemu/qemu-$qemu_version.tar.gz
  fi
  echo "Unpacking qemu-$qemu_version"
  tar -xf qemu-$qemu_version.tar.gz
  echo "Compiling/Installing qemu-$qemu_version"
  cd qemu-$qemu_version && ./configure --target-list=x86_64-softmmu --prefix=$curdir/bin && make && make install && make clean
  status=$?
  cd ..
  rm -rf qemu-$qemu_version

  if [ $status != 0 ]
  then
    echo "Error: Qemu build failed"
    exit 1
  fi
  touch $curdir/bin/qemu-$qemu_version
fi

rm -f x86-qemu x86_64-qemu
ln -s $curdir/bin/bin/qemu-system-x86_64 x86-qemu
ln -s $curdir/bin/bin/qemu-system-x86_64 x86_64-qemu

# Architecture (for binutils and gcc)
if [ $1 = "x86" ]
then
  target="i686-pc-lightos"
  binutils_flags=
elif [ $1 = "x86_64" ]
then
  target="x86_64-pc-lightos"
  binutils_flags="--enable-64-bit-bfd"
else
  echo "Error: Invalid architecture selected: " $1
  exit 1
fi

# Binutils
if [ ! -e $curdir/bin/binutils-$1-$binutils_version ]
then
  if [ ! -e binutils-$binutils_version.tar.bz2 ]
  then
    echo "Downloading binutils-$binutils_version"
    wget http://ftp.gnu.org/gnu/binutils/binutils-$binutils_version.tar.bz2
  fi

  echo "Unpacking binutils-$binutils_version"
  tar -xf binutils-$binutils_version.tar.bz2
  echo "Patching binutils-$binutils_version"
  cd binutils-$binutils_version && patch -p0 < ../binutils.patch && cd ..
  echo "Compiling/Installing binutils-$binutils_version"
  cd binutils-$binutils_version && ./configure --prefix=$curdir/bin --with-sysroot=$curdir/bin --target=$target $binutils_flags --disable-nls && make -j3 all && make install && make clean
  status=$?
  cd ..
  rm -rf binutils-$binutils_version
  if [ $status != 0 ]
  then
    echo "Error: Binutils $1 build failed"
    exit 1
  fi

  touch $curdir/bin/binutils-$1-$binutils_version
fi

rm -f $1-ar $1-as $1-c++filt $1-ld $1-nm $1-objdump $1-readelf $1-strip
ln -s $curdir/bin/bin/$target-ar $1-ar
ln -s $curdir/bin/bin/$target-as $1-as
ln -s $curdir/bin/bin/$target-c++filt $1-c++filt
ln -s $curdir/bin/bin/$target-ld $1-ld
ln -s $curdir/bin/bin/$target-nm $1-nm
ln -s $curdir/bin/bin/$target-objdump $1-objdump
ln -s $curdir/bin/bin/$target-readelf $1-readelf
ln -s $curdir/bin/bin/$target-strip $1-strip

# Directories for gcc
if ! [ -e $curdir/bin/usr ]
then
  mkdir $curdir/bin/usr
fi

if ! [ -e $curdir/bin/usr/include ]
then
  mkdir $curdir/bin/usr/include
fi

# Install the libraries (for libgcc)
if [ $1 = "x86" ]
then
  cd .. && ./configure --arch x86 && make install-headers && cd $curdir
elif [ $1 = "x86_64" ]
then
  cd .. && ./configure --arch x86_64 && make install-headers && cd $curdir
fi

# gcc
if [ ! -e $curdir/bin/gcc-$1-$gcc_version ]
then
  if [ ! -e gcc-core-$gcc_version.tar.bz2 ]
  then
    echo "Downloading gcc-core-$gcc_version"
    if [ $gcc_is_snapshot = "true" ]
    then
      wget ftp://gcc.gnu.org/pub/gcc/snapshots/$gcc_version/gcc-core-$gcc_version.tar.bz2
    else
      wget ftp://ftp.fu-berlin.de/unix/languages/gcc/releases/gcc-$gcc_version/gcc-core-$gcc_version.tar.bz2
    fi
  fi
  if [ ! -e gcc-g++-$gcc_version.tar.bz2 ]
  then
    echo "Downloading gcc-g++-$gcc_version"
    if [ $gcc_is_snapshot = "true" ]
    then
      wget ftp://gcc.gnu.org/pub/gcc/snapshots/$gcc_version/gcc-g++-$gcc_version.tar.bz2
    else
      wget ftp://ftp.fu-berlin.de/unix/languages/gcc/releases/gcc-$gcc_version/gcc-g++-$gcc_version.tar.bz2
    fi
  fi
  echo "Unpacking gcc-core-$gcc_version"
  tar -xf gcc-core-$gcc_version.tar.bz2
  echo "Unpacking gcc-g++-$gcc_version"
  tar -xf gcc-g++-$gcc_version.tar.bz2
  echo "Patching gcc-$gcc_version"
  cd gcc-$gcc_version && patch -p0 < ../gcc.patch && cd ..
  echo "Compiling/Installing gcc-$gcc_version"
  mkdir build
  cd build
  ../gcc-$gcc_version/configure --prefix=$curdir/bin --with-sysroot=$curdir/bin --target=$target --disable-nls --enable-languages=c,c++ && make -j3 all-gcc all-target-libgcc && make install-gcc install-target-libgcc && make clean
  status=$?
  cd ..
  rm -rf build gcc-$gcc_version
  if [ $status != 0 ]
  then
    echo "Error: Gcc $1 build failed"
    exit 1
  fi
  touch $curdir/bin/gcc-$1-$gcc_version
fi

rm -f $1-libgcc.a $1-gcc $1-g++ $1-gcc-include $1-gcc-include-fixed
ln -s $curdir/bin/lib/gcc/$target/$gcc_version_clean/libgcc.a $1-libgcc.a
ln -s $curdir/bin/bin/$target-gcc $1-gcc
ln -s $curdir/bin/bin/$target-g++ $1-g++

exit 0
