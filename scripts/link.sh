#! /bin/sh

# script parameters
# $1 architecture

# libarch
rm -f libarch/include/libarch/arch libarch/include/libarch/x86_64/ioport.h
rm -f libarch/include/libarch/arch libarch/include/libarch/x86_64/console.h

# libkernel
rm -f libkernel/include/libkernel/arch

# kernel
rm -f kernel/libc/errno.c kernel/libc/memory.c kernel/libc/memory.h kernel/libc/conversion.c \
      kernel/libc/ctype.c kernel/libc/sbrk.c kernel/libc/string \
      kernel/libarch kernel/libsupc++ kernel/include/kernel/arch kernel/include/kernel/platform

# lib/lightOS
rm -f lib/lightOS/libarch
rm -f lib/lightOS/libkernel
rm -f lib/lightOS/libserver

# lib/libc
rm -f lib/libc/libarch
rm -f lib/libc/libOS/this
rm -f lib/libc/include/libOS/this

# lib/libc++
rm -f lib/libc++/libsupc++

if [ ! -z $1 ]
then
  # libarch
  ln -s $1 libarch/include/libarch/arch
  ln -s ../x86/ioport.h libarch/include/libarch/x86_64/ioport.h
  ln -s ../x86/console.h libarch/include/libarch/x86_64/console.h

  # libkernel
  ln -s $1 libkernel/include/libkernel/arch

  # kernel
  ln -s ../../lib/libc/errno.c kernel/libc/errno.c
  ln -s ../../lib/libc/ctype.c kernel/libc/ctype.c
  ln -s ../../lib/libc/stdlib/sbrk.c kernel/libc/sbrk.c
  ln -s ../../lib/libc/stdlib/conversion.c kernel/libc/conversion.c
  ln -s ../../lib/libc/3rdparty/memory.c kernel/libc/memory.c
  ln -s ../../lib/libc/3rdparty/memory.h kernel/libc/memory.h
  ln -s ../../lib/libc/string kernel/libc/string
  ln -s ../libarch/ kernel/libarch
  ln -s ../libsupc++ kernel/libsupc++
  ln -s $1 kernel/include/kernel/arch
  if [ $1 = "x86" ] ||
     [ $1 = "x86_64" ]
  then
    ln -s x86_shared kernel/include/kernel/platform
  fi

  # lib/lightOS
  ln -s ../../libarch lib/lightOS/libarch
  ln -s ../../libkernel lib/lightOS/libkernel
  ln -s ../../libserver lib/lightOS/libserver

  # lib/libc
  ln -s ../../libarch lib/libc/libarch
  ln -s lightOS lib/libc/libOS/this
  ln -s lightOS lib/libc/include/libOS/this

  # lib/libc++
  ln -s ../../libsupc++ lib/libc++/libsupc++
fi

exit 0
