#! /bin/sh

# script parameters
# $1 architecture
# $2 action: create or remove

if [ $2 = "remove" ]
then
  # Remove directories
  rm -rf build/tmp/$1
  rm -rf build/$1/bin build/$1/lib build/$1/system build/$1/system/server

  # Indicate removal
  rm -f .tmpdirs-$1
fi

if [ $2 = "create" ]
then
  if ! [ -e .tmpdirs-$1 ]
  then
    # Create directories
    if ! [ -e build/tmp ]
    then
      mkdir build/tmp
    fi
    mkdir build/tmp/$1

    # Build
    mkdir build/$1/bin
    mkdir build/$1/lib
    mkdir build/$1/system
    mkdir build/$1/system/server

    cd build/tmp/$1

    # Applications
    mkdir app
    mkdir app/shell
    mkdir app/bash
    mkdir app/bash/builtins
    mkdir app/bash/lib
    mkdir app/bash/lib/sh
    mkdir app/binutils
    mkdir app/binutils/bfd
    mkdir app/binutils/binutils
    mkdir app/binutils/gas
    mkdir app/binutils/ld
    mkdir app/binutils/libiberty
    mkdir app/binutils/opcodes
    mkdir app/dash
    mkdir app/make
    mkdir app/make/glob
    mkdir app/nano
    mkdir app/nasm
    mkdir app/nasm/output
    mkdir app/yasm
    mkdir app/yasm/frontends
    mkdir app/yasm/frontends/yasm
    mkdir app/yasm/libyasm
    mkdir app/yasm/modules
    mkdir app/yasm/modules/arch
    mkdir app/yasm/modules/arch/lc3b
    mkdir app/yasm/modules/arch/x86
    mkdir app/yasm/modules/dbgfmts
    mkdir app/yasm/modules/dbgfmts/codeview
    mkdir app/yasm/modules/dbgfmts/dwarf2
    mkdir app/yasm/modules/dbgfmts/null
    mkdir app/yasm/modules/dbgfmts/stabs
    mkdir app/yasm/modules/listfmts
    mkdir app/yasm/modules/listfmts/nasm
    mkdir app/yasm/modules/objfmts
    mkdir app/yasm/modules/objfmts/bin
    mkdir app/yasm/modules/objfmts/coff
    mkdir app/yasm/modules/objfmts/dbg
    mkdir app/yasm/modules/objfmts/elf
    mkdir app/yasm/modules/objfmts/macho
    mkdir app/yasm/modules/objfmts/rdf
    mkdir app/yasm/modules/objfmts/xdf
    mkdir app/yasm/modules/parsers
    mkdir app/yasm/modules/parsers/gas
    mkdir app/yasm/modules/parsers/nasm
    mkdir app/yasm/modules/preprocs
    mkdir app/yasm/modules/preprocs/cpp
    mkdir app/yasm/modules/preprocs/nasm
    mkdir app/yasm/modules/preprocs/raw

    # Kernel
    mkdir kernel
    mkdir kernel/libarch
    mkdir kernel/libarch/$1
    mkdir kernel/libc
    mkdir kernel/libc/string
    mkdir kernel/libc/stdlib
    mkdir kernel/libsupc++
    mkdir kernel/$1
    mkdir kernel/x86_shared

    # Libraries
    mkdir lib
    mkdir lib/libc
    mkdir lib/libc/3rdparty
    mkdir lib/libc/stdio
    mkdir lib/libc/stdlib
    mkdir lib/libc/string
    mkdir lib/libc/posix
    mkdir lib/libc/libarch
    mkdir lib/libc/libarch/$1
    mkdir lib/libc/libOS
    mkdir lib/libc/libOS/this
    mkdir lib/libc/libOS/this/posix
    mkdir lib/libc++
    mkdir lib/libc++/libsupc++
    mkdir lib/curses++
    mkdir lib/lightOS++
    mkdir lib/lightOS++/gui
    mkdir lib/libcdi
    mkdir lib/lightOS
    mkdir lib/lightOS/libkernel
    mkdir lib/lightOS/libkernel/$1
    mkdir lib/lightOS/libserver
    mkdir lib/ports
    mkdir lib/ports/bzip2
    mkdir lib/ports/freetype
    mkdir lib/ports/libpng
    mkdir lib/ports/zlib

    # Servers
    mkdir server
    mkdir server/console
    mkdir server/floppy
    mkdir server/fs
    mkdir server/fs/devfs
    mkdir server/fs/ext2
    mkdir server/fs/fat
    mkdir server/fs/iso9660
    mkdir server/fs/pipefs
    mkdir server/fs/tmpfs
    #mkdir server/gui
    mkdir server/ide
    mkdir server/init
    mkdir server/klog
    mkdir server/net
    mkdir server/net/pcnet
    mkdir server/net/rtl8139
    mkdir server/pci
    mkdir server/ps2
    mkdir server/serial
    mkdir server/vfs

    # Back to lightOS root directory
    cd ../../..

    # Indicate creation
    touch .tmpdirs-$1
  fi
fi
