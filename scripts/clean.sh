#! /bin/sh

if [ -e Makefile.config ]
then
  make --no-print-directory uninstall-headers
  make --no-print-directory -C apps clean
  make --no-print-directory -C server clean
  make --no-print-directory -C kernel clean
  make --no-print-directory -C lib/ports clean
  make --no-print-directory -C lib/curses++ clean
  make --no-print-directory -C lib/libc++ clean
  make --no-print-directory -C lib/lightOS++ clean
  make --no-print-directory -C lib/libc clean
  make --no-print-directory -C lib/lightOS clean
  ./scripts/link.sh
  rm -f Makefile.asm.config Makefile.config
fi

exit 0
