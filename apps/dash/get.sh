#!/bin/bash

if [ ! -e dash-$1.tar.gz ]
then
  wget http://gondor.apana.org.au/~herbert/dash/files/dash-$1.tar.gz
fi

if [ ! -e dash-$1/COPYING ]
then
  tar -xf dash-$1.tar.gz -C .
fi

if [ ! -d src ]
then
  ln -s dash-$1 src
  patch -p0 < dash.patch
fi
