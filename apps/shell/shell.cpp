/*
lightOS application
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <algorithm>
#include <lightOS/lightOS.hpp>
#include <lightOS/device.hpp>
using namespace std;
using namespace lightOS;

// TODO: Close pipes

void disable_echo_mode()
{
	cout.flush();
	cout << "\x1B]E";
	cout.flush();
}
void enable_echo_mode()
{
	cout.flush();
	cout << "\x1B[E";
	cout.flush();
}

vector<size_t> pidList;

// TODO: Add to lib/lightOS++?
string getFilename(string &currentPath, string &name)
{
	if (name == ".")return string(currentPath);
	else if (name == "..")
	{
		string::iterator prev = currentPath.begin() + 1;
		if (currentPath[0] != '/')prev = find<string::iterator, char>(currentPath.begin(), currentPath.end(), ':') + 3;
		if (prev == currentPath.end())return string(currentPath);
		while (true)
		{
			string::iterator cur = find<string::iterator, char>(prev + 1, currentPath.end(), '/');
			if (cur == currentPath.end())break;
			prev = cur;
		}
		return string(currentPath.begin(), prev);
	}
	if (name[0] == '/' ||
		find<string::iterator, char>(name.begin(), name.end(), ':') != name.end())
	{
		return string(name);
	}
	else
	{
		string tmp(currentPath);
		string::iterator i = find<string::iterator, char>(currentPath.begin(), currentPath.end(), ':');
		if (tmp.length() != 1 &&
			(i == currentPath.end() || (i + 3) != currentPath.end()))
			tmp.append(1, '/');
		tmp.append(name);
		return tmp;
	}
}

bool execute(string &cmdline, string &dir, libkernel::message_port &Port)
{
	// Split commandline
	vector<string> commandlist;
	string::iterator pipe = cmdline.begin();
	string::iterator _pipe = cmdline.begin();
	while (true)
	{
		pipe = find(pipe, cmdline.end(), '$');
		commandlist.push_back(string(_pipe, pipe).trim());
		if (pipe == cmdline.end())break;
		++pipe;
		_pipe = pipe;
	}
	
	// Get the name of the stdin pipe
	file stdIn(stdin);
	string consoleIn = stdIn.get_name();
	stdIn.release();
	
	// Last pipe name
	string lastPipe = consoleIn;
	
	bool bFailed = false;
	for (size_t i = 0;i < commandlist.size();i++)
	{
		// Check "/bin" first
		string path("/bin/");
		path.append(commandlist[i]);
		if (file::exists(path.c_str(), Port) != file::block)path = getFilename(dir, cmdline);
		
		// Execute the file
		size_t pid = file::execute(	path.c_str(),
									Port,
									lightOS::thread::suspended);
		
		// Process created?
		if (pid == 0)
		{
			cerr << "failed to execute \"" << path << "\"" << endl;
			bFailed = true;
			break;
		}
		else if (pid == static_cast<size_t>(-1))
		{
			return true;
		}
		else
		{
			// Add to the list of processes
			pidList.push_back(pid);
			
			// Register process event
			Port.register_event(libkernel::event::destroy_process, pid);	
			
			// Set process working directory
			setWorkingDirectory(Port, dir.c_str(), pid);
			
			// Set console file name
			setConsoleName(Port, consoleIn, pid);
			
			{
				// Open file
				file Pipe(lastPipe.c_str(), access::read);
				if (!Pipe)
				{
					cerr << "Failed to open pipe \"" << lastPipe << "\"" << endl;
					bFailed = true;
					break;
				}
				
				// Release the local file, transfer the port & set standard port
				pair<libkernel::port_id_t,libkernel::port_id_t> portInfo = Pipe.release();
				if (transfer_port(portInfo.first, pid) == false)
				{
					cerr << "Failed to transfer port" << endl;
					bFailed = true;
					break;
				}
				if (set_standard_port(pid, libkernel::message_port::in, portInfo.first, portInfo.second) == false)
				{
					cerr << "Failed to set standard input port" << endl;
					bFailed = true;
					break;
				}
			}
			
			// Not last process?
			if (i < (commandlist.size() - 1))
			{
				// Create Pipe
				pair<libkernel::port_id_t,libkernel::port_id_t> portInfo = create_pipe(lastPipe);
				
				// Transfer the port & set standard port
				if (transfer_port(portInfo.first, pid) == false)
				{
					cerr << "Failed to transfer port" << endl;
					bFailed = true;
					break;
				}
				if (set_standard_port(pid, libkernel::message_port::out, portInfo.first, portInfo.second) == false)
				{
					cerr << "Failed to set standard output port" << endl;
					bFailed = true;
					break;
				}
			}
			else
			{
				// Redirect stdout to the right console window
				pair<libkernel::port_id_t,libkernel::port_id_t> thisPortOut = get_standard_port(0, libkernel::message_port::out);
				
				if (set_standard_port(pid, libkernel::message_port::out, libkernel::message_port::out, thisPortOut.second) == false)
				{
					cerr << "Failed to set standard output port" << endl;
					bFailed = true;
					break;
				}
			}
			
			// Redirect stderr to the right console window
			pair<libkernel::port_id_t,libkernel::port_id_t> thisPortErr = get_standard_port(0, libkernel::message_port::err);
			if (set_standard_port(pid, libkernel::message_port::err, libkernel::message_port::err, thisPortErr.second) == false)
			{
				cerr << "Failed to set standard error port" << endl;
				bFailed = true;
				break;
			}
		}
	}
	
	if (bFailed == true)
	{
		for (size_t i = 0;i < pidList.size();i++)
		{
			cout << "kill pid " << pidList[i] << endl;
			destroy_process(pidList[i]);
		}
		return false;
	}
	
	// Resume all processes
	for (size_t i = 0;i < pidList.size();i++)
		resume_thread(pidList[i]);
	
	// Add keyboard events and process destruction events to the wait group
	libkernel::message_port::wait_group WaitGroup;
	WaitGroup.register_handler(Port.id(), libkernel::message(libkernel::message_port::kernel, libkernel::message::event, libkernel::event::destroy_process), 0);
	//WaitGroup.add(Keyboard.id().first, message(0, 0), &Keyboard);
	while (pidList.size() != 0)
	{
		// Wait
		pair<libkernel::port_id_t,libkernel::message> result = WaitGroup();
		
		if (result.first == Port.id())
		{
			// Remove pid from list
			for (vector<size_t>::iterator i = pidList.begin();i != pidList.end();i++)
				if (*i == result.second.param2())
				{
					pidList.erase(i);
					break;
				}
		}
	}
	
	return true;
}

int main(int argc, char *argv[])
{
	libkernel::message_port Port;
	
	// Disable 'echo' mode for this terminal
	disable_echo_mode();
	
	size_t iCmdIndex = 0;
	vector<string> cmdList;
	
	// TODO: Use the current working directory
	file dir("/", access::read);
	string sDir("/");
	string cmdline;
	bool running = true;
	bool newline = true;
	while (running)
	{
		if (newline)
		{
			cout << sDir << " >";
			cout << cmdline;
			cout.flush();
			newline = false;
		}
		
		int result = fgetc(stdin);
		if (result == EOF)
		{
			// TODO: Exit shell
		}
		
		// Escape sequence ^[
		if (static_cast<char>(result) == '\x1B')
		{
			int result1 = fgetc(stdin);
			int result2 = fgetc(stdin);
			
			if (static_cast<char>(result1) == '[' &&
				static_cast<char>(result2) == 'A')
			{
				if (cmdList.size() != 0 &&
					iCmdIndex < (cmdList.size() + 1))
				{
					if (iCmdIndex != 0)--iCmdIndex;
					
					cmdline = cmdList[iCmdIndex];
					cout << '\r';
					cout << "                                                                               ";
					cout << '\r';
					newline = true;
				}
				continue;
			}
			else if (	static_cast<char>(result1) == '[' &&
						static_cast<char>(result2) == 'B')
			{
				if (iCmdIndex < cmdList.size())
				{
					iCmdIndex++;
					if (iCmdIndex == cmdList.size())
						cmdline = "";
					else
						cmdline = cmdList[iCmdIndex];
					cout << '\r';
					cout << "                                                                               ";
					cout << '\r';
					newline = true;
				}
				continue;
			}
		}
		
		char character = static_cast<char>(result);
		
		// NOTE: Ignore ASCII control characters (except \n and \b)
		if (character != '\n' && character != '\b' &&
			character >= 0 && character < 0x20)
		{
			continue;
		}
		
		if (cmdline.length() != 0 || character != '\b')
		{
			cout << character;
			cout.flush();
		}
		if (character == '\n')
		{
			cmdList.push_back(cmdline);
			iCmdIndex = cmdList.size();
			
			cmdline.trim();
			string::iterator iterator = find<string::iterator, char>(cmdline.begin(), cmdline.end(), ' ');
			string cmd(cmdline.begin(), iterator);
			if (cmd.length() == 0);
			else if (cmd == "cd")
			{
				if (cmdline.length() >= 4)
				{
					// Get the full path
					string name(iterator + 1, cmdline.end());
					name.trim();
					string tmp = getFilename(sDir, name);
					
					// Check whether the directory exists
					size_t Type = file::exists(tmp.c_str(), Port);
					if (Type == file::none)cerr << "shell: file or directory does not exist" << endl;
					else if (Type != file::directory)cerr << "shell: not a directory" << endl;
					else
					{
						dir.close();
						dir.open(tmp.c_str(), access::read);
						sDir = dir.get_name();
					}
				}
			}
			else if (cmd == "clear")
			{
				cout.flush();
				cout << "\x1B[C";
				cout.flush();
			}
			else if (cmd == "exit")
			  return 0;
			else
			{
				// Enable 'echo' mode for this terminal
				enable_echo_mode();
				
				// Execute
				execute(cmdline, sDir, Port);
				
				// Disable 'echo' mode for this terminal
				disable_echo_mode();
			}
			newline = true;
			cmdline = "";
		}
		else if (character == '\b')
		{
			if (cmdline.length() != 0)
			{
				cmdline.erase(cmdline.end() - 1);
			}
		}
		else
		{
			cmdline.append(1, character);
		}
	}
}
