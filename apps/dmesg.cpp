/*
lightOS application
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstring>
#include <iostream>
#include <curses++/curses.hpp>
#include <lightOS/kernel_log.hpp>

void show_kernel_log(curses::window& Window,
                     std::vector<lightOS::kernel_log_entry>& kernel_log,
                     size_t first_entry);

int main(int argc, char** argv)
{
  bool use_curses = false;

  /* Commandline parameters */
  for (int i = 1;i < argc;++i)
  {
    if (strcmp(argv[i], "--help") == 0)
    {
      std::cout << "dmesg shows the kernel log" << std::endl << std::endl;
      std::cout << "commandline options:" << std::endl;
      std::cout << " --help   shows this help" << std::endl;
      std::cout << " -c       uses curses++ to show a colored kernel log" << std::endl;
      return 0;
    }
    else if (strcmp(argv[i], "-c") == 0)
      use_curses = true;
    else
    {
      std::cerr << "error: unknown commandline option '" << argv[i] << "'" << std::endl;
      return -1;
    }
  }

  /* Get kernel log */
  auto kernel_log = lightOS::get_kernel_log();

  /* Normal output of the log to the standard output */
  if (!use_curses)
  {
    for (auto i = kernel_log.begin();i != kernel_log.end();++i)
      std::cout << (*i).message << std::endl;
  }
  /* Curses output */
  else
  {
    curses::screen Screen(false); // NOTE: No cursor
    curses::window& Title = Screen.create_window(0, 0, Screen.width(), 1);
    curses::window& Log = Screen.create_window(0, 1, Screen.width(), Screen.height() - 2);
    curses::window& Status = Screen.create_window(0, Screen.height() - 1, Screen.width(), 1);
    Status.clear(curses::color::blue);
    Status.string(0, 0, " q - quit    u - update    arrow/page/pos1/end keys - scroll");

    bool run = true;
    size_t first_entry = 0;
    do
    {
      // Update title window
      Title.clear(curses::color::blue);
      Title.string((Screen.width() - 14) / 2,
                   0,
                   "- kernel log -");
      char lines[3 * sizeof(unsigned long) * CHAR_BIT + 3];
      int offset = ultostr((kernel_log.size() != 0) ? first_entry + 1 : 0, lines, NULL, 10);
      size_t max_entry = first_entry + Log.height();
      if (max_entry > kernel_log.size())
        max_entry = kernel_log.size();
      lines[offset++] = '-';
      offset += ultostr(max_entry, lines + offset, NULL, 10);
      lines[offset++] = ' ';
      lines[offset++] = '/';
      lines[offset++] = ' ';
      ultostr(kernel_log.size(), lines + offset, NULL, 10);
      Title.string(Screen.width() - strlen(lines),
                   0,
                   lines);

      // Update the log
      show_kernel_log(Log, kernel_log, first_entry);

      // Update the screen
      Screen.update();

      // Wait for input
      curses::key_t Key = Screen.get_key();
      if (Key.character != L'\0')
      {
        char c = Key.character;
        if (c == 'q')
          run = false;
        else if (c == 'u')
          kernel_log = lightOS::get_kernel_log();
      }
      else if (Key.code == curses::keycode::arrow_up)
      {
        if (first_entry != 0)
          --first_entry;
      }
      else if (Key.code == curses::keycode::arrow_down)
      {
        if ((first_entry + Log.height()) < kernel_log.size())
          ++first_entry;
      }
    else if (Key.code == curses::keycode::page_up)
    {
      if (first_entry >= Log.height())
        first_entry -= Log.height();
      else
        first_entry = 0;
    }
    else if (Key.code == curses::keycode::page_down)
    {
      if ((first_entry + 2 * Log.height() - 1) < kernel_log.size())
        first_entry += Log.height();
      else if (Log.height() <= kernel_log.size())
        first_entry = kernel_log.size() - Log.height();
    }
    else if (Key.code == curses::keycode::pos1)
    {
      first_entry = 0;
    }
    else if (Key.code == curses::keycode::end)
    {
      if (kernel_log.size() >= Log.height())
        first_entry = kernel_log.size() - Log.height();
      else
        first_entry = 0;
    }
    } while (run);
  }
}

void show_kernel_log(curses::window& Window,
                     std::vector<lightOS::kernel_log_entry>& kernel_log,
                     size_t first_entry)
{
  Window.clear();
  size_t entry_count = std::min(Window.height(), kernel_log.size() - first_entry);
  for (size_t i = first_entry;i < (first_entry + entry_count);++i)
  {
    curses::color entry_color = curses::color::green;
    if (kernel_log[i].level == libkernel::log_warning)
      entry_color = curses::color::white;
    else if (kernel_log[i].level == libkernel::log_error)
      entry_color = curses::color::red;
    Window.string(0, i - first_entry, kernel_log[i].message.c_str(), entry_color);
  }
}
