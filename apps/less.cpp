/*
lightOS application
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <vector>
#include <string>
#include <cstdio>
#include <iostream>
#include <curses++/curses.hpp>
#include <lightOS/filesystem.hpp>

void show_data(curses::window& Window,
               std::vector<std::string>& data,
               size_t first_line);
void read_from_stdin(std::vector<std::string>& data);
bool read_from_file(const char* filename,
                    std::vector<std::string>& data);

int main(int argc, char** argv)
{
  /* Check commandline argument count */
  if (argc > 2)
  {
    std::cerr << "error: invalid number of commandline arguments" << std::endl;
    return -1;
  }

  /* Open the standard input of the console */
  FILE *keyboard = NULL;
  {
    libkernel::message_port Port;
    std::string stdin_name = lightOS::getConsoleName(Port);

    keyboard = fopen(stdin_name.c_str(), "rb");
    if (keyboard == NULL)
    {
      std::cerr << "less: could not open the keyboard" << std::endl;
      return -1;
    }
  }

  /* Read the data */
  std::vector<std::string> data;
  if (argc == 2)
  {
    if (read_from_file(argv[1], data) == false)
    {
      fclose(keyboard);
      return -1;
    }
  }
  else
    read_from_stdin(data);

  /* Show */
  curses::screen Screen(false, stdout, keyboard); // NOTE: No cursor
  curses::window& Window = Screen.create_window(0, 0, Screen.width(), Screen.height() - 1);
  curses::window& Status = Screen.create_window(0, Screen.height() - 1, Screen.width(), 1);

  bool run = true;
  size_t first_line = 0;
  do
  {
    // Update status window
    Status.clear(curses::color::blue);
    Status.string(0, 0, " q - quit    arrow/page/pos1/end keys - scroll");
    char lines[3 * sizeof(unsigned long) * CHAR_BIT + 3];
    int offset = ultostr((data.size() != 0) ? first_line + 1 : 0, lines, NULL, 10);
    size_t max_entry = first_line + Window.height();
    if (max_entry > data.size())
      max_entry = data.size();
    lines[offset++] = '-';
    offset += ultostr(max_entry, lines + offset, NULL, 10);
    lines[offset++] = ' ';
    lines[offset++] = '/';
    lines[offset++] = ' ';
    ultostr(data.size(), lines + offset, NULL, 10);
    Status.string(Screen.width() - strlen(lines),
                  0,
                  lines);

    // Update the window
    show_data(Window, data, first_line);

    // Update the screen
    Screen.update();

    // Wait for input
    curses::key_t Key = Screen.get_key();
    if (Key.character != L'\0')
    {
      char c = static_cast<char>(Key.character);
      if (c == 'q')
        run = false;
    }
    else if (Key.code == curses::keycode::arrow_up)
    {
      if (first_line != 0)
        --first_line;
    }
    else if (Key.code == curses::keycode::arrow_down)
    {
      if ((first_line + Window.height()) < data.size())
        ++first_line;
    }
    else if (Key.code == curses::keycode::page_up)
    {
      if (first_line >= Window.height())
        first_line -= Window.height();
      else
        first_line = 0;
    }
    else if (Key.code == curses::keycode::page_down)
    {
      if ((first_line + 2 * Window.height() - 1) < data.size())
        first_line += Window.height();
      else if (Window.height() <= data.size())
        first_line = data.size() - Window.height();
    }
    else if (Key.code == curses::keycode::pos1)
    {
      first_line = 0;
    }
    else if (Key.code == curses::keycode::end)
    {
      if (data.size() >= Window.height())
        first_line = data.size() - Window.height();
      else
        first_line = 0;
    }
  } while (run);

  fclose(keyboard);
}

void show_data(curses::window& Window,
               std::vector<std::string> &data,
               size_t first_line)
{
  Window.clear();
  size_t entry_count = std::min(Window.height(), data.size() - first_line);
  for (size_t i = first_line;i < (first_line + entry_count);++i)
    if (data[i].c_str() != 0)
      Window.string(0, i - first_line, data[i].c_str());
}

void read_from_stdin(std::vector<std::string>& data)
{
  int character;
  size_t index = 0;
  while ((character = fgetc(stdin)) != EOF)
  {
    if (index >= data.size())
      data.push_back(std::string());
    char c = static_cast<char>(character);
    if (c == '\n')
      ++index;
    else
      data[index].append(1, c);
  }
}

bool read_from_file(const char* filename,
                    std::vector<std::string>& data)
{
  // Open the file
  lightOS::file File;
  if (File.open(filename, lightOS::access::read) == false)
  {
    std::cerr << "less: could not open file \"" << filename << "\"" << std::endl;
    return false;
  }

  // Read the file
  libkernel::shared_memory readData = File.read(0, File.blocksize() * File.blockcount());

  // Process the input
  std::string line;
  char* cur_data = readData.address<char>();
  for (size_t i = 0;i < readData.size();i++)
  {
    if (*cur_data != '\n')
      line.append(1, *cur_data);
    else
    {
      data.push_back(line);
      line = "";
    }
    ++cur_data;
  }
  return true;
}
