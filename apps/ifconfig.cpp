/*
lightOS application
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/net.hpp>
#include <lightOS/lightOS.hpp>
using namespace lightOS;
using namespace net;
using namespace std;

void print_nic_info(ethId id, libkernel::message_port &Port)
{
	cout << "eth" << dec << id << ": ";
	
	// Get the network information
	nic_info info;
	if (get_nic_info(info, id, Port) == false)
	{
		cout << "no information" << endl;
		return;
	}
	
	// Print the information
	cout << "hardware address " << info.ethernetAddress << endl;
	cout << "      ipv4: address " << info.ipv4Address << endl;
	cout << "            subnet " << info.ipv4SubnetMask << endl;
	cout << "            broadcast " << info.ipv4BroadcastAddress << endl;
	if (info.ipv4Dhcp.mAddress != 0)cout << "            dhcp " << info.ipv4Dhcp << endl;
	if (info.ipv4Dns.mAddress != 0)cout << "            dns " << info.ipv4Dns << endl;
	if (info.ipv4Router.mAddress != 0)cout << "            router " << info.ipv4Router << endl;
	cout << "      received " << dec << info.rx << " frames with a total size of " << info.rx_size << " bytes" << endl;
	cout << "      transmited " << dec << info.tx << " frames with a total size of " << info.tx_size << " bytes" << endl;
}
int main(int argc, char **argv)
{
	// Test whether the net server is running or not
	pair<processId,threadId> result = get_port_info(libkernel::message_port::net);
	if (result.first == 0)
	{
		cerr << "ifconfig: net server is not running" << endl;
		return -1;
	}
	
	libkernel::message_port Port;
	
	if (argc == 1)
	{
		// Get the mask from the net server
		size_t ethmask = enum_nic(Port);
		
		// Go through the bits to find registered networks
		for (size_t i = 0;i < (sizeof(size_t) * 8);i++)
		{
			if (((static_cast<size_t>(1) << i) & ethmask) != 0)
				print_nic_info(i, Port);
		}
	}
	else if (strcmp(argv[1], "--help") == 0)
	{
		// Print the help
		cout << "ifconfig                            show info for all ethernet connections" << endl;
		cout << "ifconfig ethX                       show info for the ethX ethernet connection" << endl;
		cout << "ifconfig ethX [-s <subnet>] <newip> set the IP address and subnet mask for ethX" << endl;
		cout << "ifconfig --help                     show this info" << endl;
	}
	else
	{
		char *eth = argv[1];
		char *ipAddress = 0;
		char *subnetMask = 0;
		for (int i = 2;i < argc;i++)
		{
			if (strcmp(argv[i], "-s") == 0)
			{
				if (argc <= i)
				{
					cerr << "ifconfig: no subnet mask" << endl;
					return -1;
				}
				++i;
				subnetMask = argv[i];
			}
			else
			{
				if (ipAddress != 0)
				{
					cerr << "ifconfig: too many arguments" << endl;
					return -1;
				}
				ipAddress = argv[i];
			}
		}
		
		if (strncmp(eth, "eth", 3) != 0)
		{
			cerr << "ifconfig: invalid ethernet device" << endl;
			return -1;
		}
		
		size_t num = strtoul(&eth[3], 0, 10);
		if (num > 31)
		{
			cerr << "ifconfig: invalid ethernet device" << endl;
			return -1;
		}
		
		// Get the mask from the net server
		size_t ethmask = enum_nic(Port);
		
		if ((ethmask & (1 << num)) == 0)
		{
			cerr << "ifconfig: ethernet device not present" << endl;
			return -1;
		}
		
		if (ipAddress == 0 &&
			subnetMask == 0)
		{
			print_nic_info(num, Port);
		}
		else
		{
			// Get the NIC information
			nic_info info;
			get_nic_info(info, num, Port);
			
			if (ipAddress != 0)
			{
				// Set the IPv4 address
				pair<bool, ipv4_address> result = net::parse_ipv4_address(ipAddress);
				if (result.first == false)
				{
					cerr << "ifconfig: invalid IPv4 address" << endl;
					return -1;
				}
				info.ipv4Address = result.second;
			}
			if (subnetMask != 0)
			{
				// Set the IPv4 subnet mask
				pair<bool, ipv4_address> result = net::parse_ipv4_address(subnetMask);
				if (result.first == false)
				{
					cerr << "ifconfig: invalid IPv4 subnet mask" << endl;
					return -1;
				}
				info.ipv4SubnetMask = result.second;
			}
			set_nic_info(info, num, Port);
		}
	}
	
	return 0;
}
