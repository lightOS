###############################################################################
######### lightOS Makfile                                            ##########
###############################################################################
include ../../Makefile.config
include ../../Makefile.rules

.PHONY : get

YASM_VERSION= 0.7.2

SRC_GENPERF= $(patsubst %,src/tools/genperf/%, genperf.c perfect.c phash.c xmalloc.c xstrdup.c)
OBJ_GENPERF= $(SRC_GENPERF:%.c=%.o)
SRC_RE2C= $(patsubst %,src/tools/re2c/%, main.c code.c dfa.c parser.c actions.c scanner.c mbo_getopt.c substr.c translate.c)
OBJ_RE2C= $(SRC_RE2C:%.c=%.o)
SRC_LIBYASM= src/module.c src/modules/listfmts/nasm/nasm-listfmt.c \
             $(patsubst %, src/modules/arch/x86/%, x86regtmod.c x86cpu.c x86arch.c x86bc.c x86expr.c x86id.c) \
             $(patsubst %, src/modules/arch/lc3b/%, lc3barch.c lc3bbc.c lc3bid.c) \
             $(patsubst %, src/modules/parsers/%, gas/gas-parser.c gas/gas-parse.c gas/gas-token.c nasm/nasm-parser.c nasm/nasm-parse.c nasm/nasm-token.c) \
             $(patsubst %, src/modules/preprocs/%, nasm/nasm-preproc.c nasm/nasm-pp.c nasm/nasmlib.c nasm/nasm-eval.c raw/raw-preproc.c cpp/cpp-preproc.c) \
             $(patsubst %, src/modules/dbgfmts/%, codeview/cv-dbgfmt.c codeview/cv-symline.c codeview/cv-type.c dwarf2/dwarf2-dbgfmt.c dwarf2/dwarf2-line.c dwarf2/dwarf2-aranges.c dwarf2/dwarf2-info.c null/null-dbgfmt.c stabs/stabs-dbgfmt.c) \
             $(patsubst %, src/modules/objfmts/%, dbg/dbg-objfmt.c bin/bin-objfmt.c elf/elf.c elf/elf-objfmt.c elf/elf-x86-x86.c elf/elf-x86-amd64.c coff/coff-objfmt.c coff/win64-except.c macho/macho-objfmt.c rdf/rdf-objfmt.c xdf/xdf-objfmt.c) \
             $(patsubst %, src/libyasm/%, assocdat.c bitvect.c bc-align.c bc-data.c bc-incbin.c bc-org.c bc-reserve.c bytecode.c errwarn.c expr.c file.c floatnum.c hamt.c insn.c intnum.c inttree.c linemap.c md5.c mergesort.c phash.c section.c strcasecmp.c strsep.c symrec.c valparam.c value.c xmalloc.c xstrdup.c)
OBJ_LIBYASM= $(SRC_LIBYASM:src/%.c=$(TMP_YASM)/%.o)
SRC= src/frontends/yasm/yasm.c src/frontends/yasm/yasm-options.c
OBJ= $(SRC:src/%.c=$(TMP_YASM)/%.o)
INCLUDE= -I yasm-$(YASM_VERSION) $(LIBC_INCLUDE) $(LIBUNIX_INCLUDE)
LIB= $(LIBGCC) $(LIBC) $(LIBLIGHTOS) $(LIBUNIX)
LIBS= --start-group $(LIB) src/libyasm.a --end-group

get:
	@./get.sh $(YASM_VERSION)

dep: src/x86insn_nasm.c src/x86insn_gas.c src/modules/arch/x86/x86cpu.c src/modules/arch/x86/x86regtmod.c src/modules/parsers/gas/gas-token.c src/modules/parsers/nasm/nasm-token.c src/modules/arch/lc3b/lc3bid.c src/version.mac src/modules/preprocs/nasm/standard.mac src/license.c
	@makedepend -f - -DHAVE_CONFIG_H -D_LIGHTOS_LIBUNIX $(INCLUDE_MAKEDEPEND) $(INCLUDE) $(SRC) $(SRC_LIBYASM) -p$(TMP_YASM)/ > $(DEP)

-include $(DEP)

all: dep $(BUILD)/$(ARCH)/bin/yasm

$(BUILD)/$(ARCH)/bin/yasm: src/libyasm.a $(OBJ)
	@echo " LD     yasm"
	@$(LD) $(LD_FLAGS) -T $(LDSCRIPT_C) -o $(@) $(CRT0_C) $(OBJ) $(LIBS)

src/libyasm.a: $(OBJ_LIBYASM)
	@echo " AR     libyasm.a"
	@$(AR) cru $(@) $(OBJ_LIBYASM)

$(TMP_YASM)/%.o: src/%.c
	@echo " CC     yasm/$(@:$(TMP_YASM)/%.o=src/%.c)"
	@$(CC) -DHAVE_CONFIG_H -D_LIGHTOS_LIBUNIX $(C_FLAGS) $(INCLUDE) -c $(@:$(TMP_YASM)/%.o=src/%.c) -o $(@)

src/version.mac: src/genversion
	@./src/genversion src/version.mac

src/modules/preprocs/nasm/standard.mac: src/genmacro src/version.mac
	@cd src && ./genmacro ./modules/preprocs/nasm/standard.mac version.mac

src/license.c: src/genstring
	@./src/genstring license_msg src/license.c ./src/COPYING

src/modules/parsers/gas/gas-token.c src/modules/parsers/nasm/nasm-token.c src/modules/arch/lc3b/lc3bid.c: src/re2c
	@./src/re2c -b -o $(@) $(@:%.c=%.re)

src/x86insn_nasm.c src/x86insn_gas.c src/modules/arch/x86/x86cpu.c src/modules/arch/x86/x86regtmod.c: src/genperf
	@./src/genperf $(@:%.c=%.gperf) $(@)

src/re2c: $(OBJ_RE2C)
	@echo " LD     yasm/re2c"
	@gcc -I . -o $(@) $(OBJ_RE2C)

src/genstring: src/genstring.o
	@echo " LD     yasm/genstring"
	@gcc -std=gnu99 -o $(@) src/genstring.o

src/genmodule: src/libyasm/genmodule.o
	@echo " LD     yasm/genmodule"
	@gcc -std=gnu99 -o $(@) src/libyasm/genmodule.o

src/genmacro: src/modules/preprocs/nasm/genmacro.o
	@echo " LD     yasm/genmacro"
	@gcc -std=gnu99 -o $(@) src/modules/preprocs/nasm/genmacro.o

src/genversion: src/modules/preprocs/nasm/genversion.o
	@echo " LD     yasm/genversion"
	@gcc -std=gnu99 -o $(@) src/modules/preprocs/nasm/genversion.o

src/genperf: $(OBJ_GENPERF)
	@echo " LD     yasm/genperf"
	@gcc -I src -o $(@) $(OBJ_GENPERF)

$(OBJ_GENPERF) $(OBJ_RE2C) src/modules/preprocs/nasm/genversion.o src/modules/preprocs/nasm/genmacro.o src/genstring.o:
	@echo " CC     yasm/$(@:%.o=%.c)"
	@gcc -DHAVE_CONFIG_H -I src -I src/libyasm -std=gnu99 -c $(@:%.o=%.c) -o $(@)

clean:
	@rm -f src $(DEP) $(OBJ_LIBYASM) $(OBJ) $(BUILD)/$(ARCH)/bin/yasm
	@rm -rf yasm-$(YASM_VERSION)
