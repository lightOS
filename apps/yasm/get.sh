#!/bin/bash

if !(test -e yasm-$1.tar.gz)
then
  wget http://www.tortall.net/projects/yasm/releases/yasm-$1.tar.gz
fi

if !(test -e yasm-$1/README)
then
  tar -xf yasm-$1.tar.gz
fi

if [ ! -d src ]
then
  ln -s yasm-$1 src
  patch -p0 < yasm.patch
fi

if [ ! -e yasm-$1/tools/genperf/phash.c ]
then
  ln -s ../../libyasm/phash.c yasm-$1/tools/genperf/phash.c
fi

if [ ! -e yasm-$1/tools/genperf/xmalloc.c ]
then
  ln -s ../../libyasm/xmalloc.c yasm-$1/tools/genperf/xmalloc.c
fi

if [ ! -e yasm-$1/tools/genperf/xstrdup.c ]
then
  ln -s ../../libyasm/xstrdup.c yasm-$1/tools/genperf/xstrdup.c
fi
