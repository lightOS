/*
lightOS application
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
using namespace std;

int main()
{
	cout << "arp                           see 'arp --help' for information" << endl;
	cout << "bzip2 [-d] <src> <dest>       (de)compress a file with bzip2" << endl;
	cout << "cat <file[s]>                 print the file(s)" << endl;
	cout << "cd <dir>                      change directory" << endl;
	cout << "clear                         clear screen" << endl;
	cout << "date                          print the date and time" << endl;
	cout << "df                            list the mountpoints" << endl;
	cout << "dns                           see 'dns --help' for information" << endl;
	cout << "echo <*>                      writes the parameters to the standard output" << endl;
	cout << "gzip [-d] <src> <dest>        (de)compress a file with gzip" << endl;
	cout << "ifconfig                      see 'ifconfig --help' for information" << endl;
	cout << "kill <pid>                    kill a process" << endl;
	cout << "ls                            list directory content" << endl;
	cout << "lspci                         enumerate all pci devices" << endl;
	cout << "lslib                         list all loaded libraries" << endl;
	cout << "mem                           memory information" << endl;
	cout << "mkdir <name>                  create a directory" << endl;
	cout << "ping <ip>                     send a ping to an IP adress and wait for a reply" << endl;
	cout << "ps                            list the current processes" << endl;
	cout << "server start <file>           start a server" << endl;
	cout << "sleep <ms>                    sleep a specific time (in ms)" << endl;
	cout << "uptime                        print the uptime" << endl;
	cout << "version                       print the version" << endl;
}
