/*
lightOS application
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <vector>
#include <iterator>
#include <lightOS/lightOS.hpp>
using namespace std;
using namespace lightOS;

void out_n(ostream &stream, size_t number, size_t radix, size_t size)
{
	char n[17];
	ultostr(number, n, n + 16, radix);
	stream << n;
	for (size_t i = strlen(n);i < size;i++)stream << ' ';
}

int main(int argc, char **argv)
{
	cout << "name:                         pid:      memory:" << endl;
	vector<triple<processId,string,size_t> > list = get_process_list();
	for (vector<triple<processId,string,size_t> >::iterator i=list.begin();i != list.end();i++)
	{
		cout << (*i).second;
		for (size_t y=(*i).second.length();y < 30;y++)cout << ' ';
		out_n(cout, (*i).first, 10, 10);
		cout << ((*i).third / 1024) << "kB" << endl;
	}
	cout << list.size() << " process(es)" << endl;
	return 0;
}
