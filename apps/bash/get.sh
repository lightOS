#!/bin/bash

if [ ! -e bash-$1.tar.gz ]
then
  wget http://ftp.gnu.org/gnu/bash/bash-$1.tar.gz
fi

if [ ! -e bash-$1/README ]
then
  tar -xf bash-$1.tar.gz -C .
fi

if [ ! -d src ]
then
  ln -s bash-$1 src
  patch -p0 < bash.patch
fi
