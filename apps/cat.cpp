/*
lightOS application
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <utility>
#include <lightOS/lightOS.hpp>
using namespace std;
using namespace lightOS;

#include <stdio.h>

void show_stdin()
{
	while (true)
	{
		char buf;
		if (fread(&buf, 1, 1, stdin) != 1)
			return;
		cout << buf;
	}
}
int main(int argc, char **argv)
{
	if (argc == 1)show_stdin();
	else
	{
		for (int i=1;i < argc;i++)
		{
			if (strcmp(argv[i], "-") == 0)
				show_stdin();
			else
			{
				file File(argv[i], access::read);
				if (File)
				{
					if (File.filetype() == file::block)
					{
						if (File.blockcount() != 0)
						{
							size_t size = File.blocksize() * File.blockcount();
							auto_array<char> data(new char[size+1]);
							if (File.read(data.get(), 0, size) != size)return -1;
							data.get()[size] = '\0';
							cout << data.get();
						}
					}
					else cerr << "cat: \"" << argv[i] << "\" is not a block file" << endl;
				}
				else cerr << "cat: could not open the file \"" << argv[i] << "\"" << endl;
			}
		}
	}
	return 0;
}
