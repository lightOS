/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/net.hpp>
#include <lightOS/lightOS.hpp>
using namespace std;
using namespace lightOS;

int main(int argc, char **argv)
{
	// Test whether the net server is running or not
	pair<processId,threadId> result = get_port_info(libkernel::message_port::net);
	if (result.first == 0)
	{
		cerr << "dns: net server is not running" << endl;
		return -1;
	}
	
	if (argc != 2 &&
		argc != 1)
	{
		cerr << "dns: invalid arguments" << endl;
		return -1;
	}
	
	if (argc == 1)
	{
		// Create a port
		libkernel::message_port Port;
		
		// Get the DNS cache
		vector<pair<string, lightOS::net::ipv4_address> > dnsCache;
		dnsCache = net::dns_cache(Port);
		
		// Print the DNS cache
		for (size_t i = 0;i < dnsCache.size();i++)
		{
			cout << "IP:  " << dnsCache[i].second << endl;
			cout << "     " << dnsCache[i].first << endl;
		}
	}
	else if (strcmp(argv[1], "clear") == 0)
	{
		// Create a port
		libkernel::message_port Port;
		
		// Clear the DNS cache
		if (net::dns_clear_cache(Port) == false)
		{
			cerr << "dns: could not clear the DNS cache" << endl;
			return -1;
		}
		cout << "dns: cleared the DNS cache" << endl;
	}
	else if (strcmp(argv[1], "--help") == 0)
	{
		// Print the help
		cout << "dns                           show the DNS cache" << endl;
		cout << "dns <domain name>             request the IP address for the domain name" << endl;
		cout << "dns clear                     clear the DNS cache" << endl;
		cout << "dns --help                    show this info" << endl;
	}
	else
	{
		// resolve a domain name
		socket Socket;
		net::ipv4_address Address;
		if (Socket.resolve(argv[1], Address) == false)
		{
			cerr << "dns: failed to resolve \"" << argv[1] << "\"" << endl;
			return -1;
		}
		
		cout << "dns: IP address for \"" << argv[1] << "\" is " << Address << endl;
	}
}
