/*
lightOS application
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <vector>
#include <utility>
#include <iterator>
#include <iostream>
#include <lightOS/lightOS.hpp>
#include <lightOS/pci.hpp>
using namespace std;
using namespace lightOS;

const char pciDeviceClass[][50] = 
{
	"Unknown",
	"Mass storage controller",
	"Network controller",
	"Display controller",
	"Multimedia device",
	"Memory Controller",
	"Bridge",
	"Simple communication controller",
	"Base system peripherals",
	"Input device",
	"Docking station",
	"Processor",
	"Serial bus controller",
	"Wireless controller",
	"Intelligent I/O controller",
	"Satellite communication controller",
	"Encryption/Decryption controller",
	"Data acquisition and signal processing controller"
};

void outputId(size_t id, size_t num)
{
	size_t size = 0;
	{
		size_t i = id;
		while (i != 0){i /= 16;size++;}
	}
	for (size_t i=size;i < num;i++)cout << '0';
	cout << hex << id;
}

int main(int argc, char **argv)
{
	// Test whether the pci server is running or not
	pair<processId,threadId> result = get_port_info(libkernel::message_port::pci);
	if (result.first == 0)
	{
		cerr << "lspci: pci server is not running" << endl;
		return -1;
	}
	
	libkernel::message_port Port;
	vector<pci::deviceAddr> list = pci::enumDevices(Port);
	cout << "          vendorID deviceID Class" << endl;
	for (vector<pci::deviceAddr>::iterator i=list.begin();i != list.end();i++)
	{
		cout << dec;
		if ((*i).bus < 10)cout << '0';
		cout << (*i).bus << ':';
		if ((*i).device < 10) cout << '0';
		cout << (*i).device << ':';
		if ((*i).function < 10)cout << '0';
		cout << (*i).function << "  ";
		
		pci::deviceInfo info = pci::getDeviceInfo(*i, Port);
		cout << "0x";
		outputId(info.vendorId, 4);
		cout << "   0x";
		outputId(info.deviceId, 4);
		cout << "   " << pciDeviceClass[info.baseclass] << endl;
		
		if (info.irq != 0)
			cout << " IRQ #" << dec << static_cast<size_t>(info.irq) << endl;
		
		for (size_t i = 0;i < 6;i++)
			if (info.mm_space[i] != 0)
				#ifdef X86
					cout << hex << " Memory range: 0x" << static_cast<size_t>(info.mm_space[i]) << " - 0x" << static_cast<size_t>(info.mm_space[i] + info.mm_space_size[i]) << endl;
				#endif
				#ifdef X86_64
					cout << hex << " Memory range: 0x" << info.mm_space[i] << " - 0x" << (info.mm_space[i] + info.mm_space_size[i]) << endl;
				#endif
		for (size_t i = 0;i < 6;i++)
			if (info.io_space[i] != 0)
				cout << hex << " I/O range: 0x" << info.io_space[i] << " - 0x" << (info.io_space[i] + info.io_space_size[i]) << endl;
	}
	return 0;
}
