#!/bin/bash

if [ ! -e binutils-$1.tar.bz2 ]
then
  wget http://ftp.gnu.org/gnu/binutils/binutils-$1.tar.bz2
fi

if [ ! -e binutils-$1/README ]
then
  tar -jxf binutils-$1.tar.bz2 -C .
fi

if [ ! -d src ]
then
  ln -s binutils-$1 src
  patch -p0 < binutils.patch
fi
