#!/bin/bash

if !(test -e nasm-$1.tar.bz2)
then
  wget http://garr.dl.sourceforge.net/sourceforge/nasm/nasm-$1.tar.bz2
fi

if !(test -e nasm-$1/README)
then
  tar -jxf nasm-$1.tar.bz2
fi

if [ ! -d src ]
then
  ln -s nasm-$1 src
  patch -p0 < nasm.patch
fi
