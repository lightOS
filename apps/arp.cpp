/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/net.hpp>
#include <lightOS/lightOS.hpp>
using namespace std;
using namespace lightOS;

int main(int argc, char **argv)
{
  // Test whether the net server is running or not
	pair<processId,threadId> result = get_port_info(libkernel::message_port::net);
	if (result.first == 0)
	{
		cerr << "arp: net server is not running" << endl;
		return -1;
	}
	
	if (argc != 1 &&
		argc != 2)
	{
		cerr << "arp: invalid arguments" << endl;
		return -1;
	}
	
	if (argc == 1)
	{
		// Create a port
		libkernel::message_port Port;
		
		// Get the ARP cache
		vector<pair<lightOS::net::ethernet_address, lightOS::net::ipv4_address> > arpCache;
		arpCache = net::arp_cache(Port);
		
		// Print the ARP cache
		cout << "Ethernet Address:     IP:" << endl;
		for (size_t i = 0;i < arpCache.size();i++)
		{
			cout << arpCache[i].first << "     " << arpCache[i].second << endl;
		}
	}
	else if (strcmp(argv[1], "clear") == 0)
	{
		// Create a port
		libkernel::message_port Port;
		
		// Clear the ARP cache
		if (net::arp_clear_cache(Port) == false)
		{
			cerr << "arp: could not clear the ARP cache" << endl;
			return -1;
		}
		cout << "arp: cleared the ARP cache" << endl;
	}
	else if (strcmp(argv[1], "--help") == 0)
	{
		// Print the help
		cout << "arp                           show the ARP cache" << endl;
		cout << "arp clear                     clear the ARP cache" << endl;
		cout << "arp --help                    show this info" << endl;
	}
	// TODO: Send ARP request
}
