/*
lightOS application
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/lightOS.hpp>
using namespace std;
using namespace lightOS;

int main(int argc, char **argv)
{
	file dir(".", access::read);
	if (!dir)
	{
		cerr << "failed to open directory" << endl;
		return -1;
	}
	for (file::iterator i = dir.begin();i != dir.end();i++)
	{
		string name(*i);
		
		cout << name;
		for (size_t i=name.length();i < 30;i++)cout << ' ';
		
		file File;
		if (File.open(name.c_str(), access::info) == true)
		{
			size_t Type = File.filetype();
			if (Type == file::block)cout << "block     ";
			else if (Type == file::stream)cout << "stream    ";
			else if (Type == file::directory)cout << "directory ";
			
			if (Type != file::directory)
			{
				cout << File.blocksize() << " ";
				if (Type != file::stream)cout << File.blockcount();
			}
		}
		cout << endl;
	}
	return 0;
}
