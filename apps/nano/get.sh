#!/bin/bash

if [ ! -e nano-$1.tar.gz ]
then
  wget http://www.nano-editor.org/dist/v2.0/nano-$1.tar.gz
fi

if [ ! -e nano-$1/README ]
then
  tar -xf nano-$1.tar.gz -C .
fi

if [ ! -d src ]
then
  ln -s nano-$1 src
  patch -p0 < nano.patch
fi
