#!/bin/bash

if !(test -e make-$1.tar.bz2)
then
  wget http://ftp.gnu.org/pub/gnu/make/make-$1.tar.bz2
fi

if !(test -e make-$1/README)
then
  tar -jxf make-$1.tar.bz2
fi

if [ ! -d src ]
then
  ln -s make-$1 src
  patch -p0 < make.patch
fi
