/*
lightOS application
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <vector>
#include <iterator>
#include <lightOS/lightOS.hpp>
using namespace std;
using namespace lightOS;

int main(int argc, char **argv)
{
	cout << "name:                         address:" << endl;
	vector<triple<string, void*, size_t> > list = get_library_list();
	for (vector<triple<string, void*, size_t> >::iterator i=list.begin();i != list.end();i++)
	{
		cout << (*i).first;
		for (size_t y=(*i).first.length();y < 30;y++)cout << ' ';
		cout << "0x" << hex << reinterpret_cast<uintptr_t>((*i).second) << " - 0x";
		cout << (reinterpret_cast<uintptr_t>((*i).second) + (*i).third) << endl;
	}
	cout << dec << list.size() << " libraries" << endl;
	return 0;
}
