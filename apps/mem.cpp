/*
lightOS application
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/lightOS.hpp>
using namespace std;
using namespace lightOS;

int main()
{
	pair<size_t,size_t> memory = get_memory_info();
	memory.first /= 1024;
	memory.second /= 1024;
	cout << "page-allocator:" << endl;
	cout << " total memory: " << dec << memory.first << "KiB" << endl;
	cout << " free memory:  " << dec << memory.second << "KiB (" << ((memory.second * 100) / memory.first) << "." << (((memory.second * 1000) / memory.first) % 10) << "%)" << endl;
	
	size_t capacity;
	vector<lightOS::range> RangeAllocator = lightOS::range_allocator(capacity);
	cout << "range-allocator:" << endl;
	size_t freeRange = 0;
	for (size_t i = 0;i < RangeAllocator.size();i++)
	{
		freeRange += RangeAllocator[i].size;
		cout << " 0x" << hex << RangeAllocator[i].address << " - 0x" << (RangeAllocator[i].address + RangeAllocator[i].size) << endl;
	}
	freeRange /= 1024;
	cout << " total memory: " << dec << (capacity / 1024) << "KiB" << endl;
	cout << " free memory:  " << dec << freeRange << "KiB (" << ((freeRange * 100) / (capacity / 1024)) << "." << (((freeRange * 1000) / (capacity / 1024)) % 10) << "%)" << endl;
}
