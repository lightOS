/*
lightOS application
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/lightOS.hpp>
using namespace std;
using namespace lightOS;

int main()
{
	pair<size_t,size_t> version = get_kernel_version();
	cout << "lightOS V" << version.first << "." << version.second;
	#ifdef X86
		cout << " x86";
	#endif
	#ifdef X86_64
		cout << " x86-64";
	#endif
	cout << endl;
	return 0;
}
