/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/net.hpp>
#include <lightOS/lightOS.hpp>
using namespace std;
using namespace lightOS;

int main(int argc, char **argv)
{
	// Test whether the net server is running or not
	pair<processId,threadId> res = get_port_info(libkernel::message_port::net);
	if (res.first == 0)
	{
		cerr << "ping: net server is not running" << endl;
		return -1;
	}
	
	if (argc != 2)
	{
		cerr << "ping: invalid arguments" << endl;
		return -1;
	}
	
	pair<bool, net::ipv4_address> result = net::parse_ipv4_address(argv[1]);
	if (result.first == false)
	{
		cerr << "ping: invalid IPv4 address" << endl;
		return -1;
	}
	
	// Create socket
	socket Socket;
	if (Socket.create(socket::icmp) == false)
	{
		cerr << "ping: failed to create socket" << endl;
		return -1;
	}
	
	// Bind socket
	if (Socket.bind(net::eth_all, net::port_any, result.second, net::port_any) == false)
	{
		cerr << "ping: failed to bind socket" << endl;
		return -1;
	}
	
	cout << "PING " << result.second << endl;
	
	// Send Ping
	const char *buffer = "Hello, World\n";
	if (Socket.send<char>(buffer, strlen(buffer)) == false)
	{
		cerr << "ping: failed to send" << endl;
		return -1;
	}
	
	pair<libkernel::shared_memory, lightOS::net::ipv4_address> received = Socket.receive();
	cout << "PONG " << received.second << endl;
}
