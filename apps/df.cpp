/*
lightOS application
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/lightOS.hpp>
using namespace std;
using namespace lightOS;

pair<size_t,string> convertSize(size_t size)
{
	size_t i = 0;
	while (size >= 10000)
	{
		i++;
		size /= 1024;
	}
	string unit;
	switch (i)
	{
		case 0:unit = "B";break;
		case 1:unit = "KiB";break;
		case 2:unit = "MiB";break;
		case 3:unit = "GiB";break;
		case 4:unit = "TiB";break;
	}
	return pair<size_t,string>(size, unit);
}

int main(int argc, char **argv)
{
	libkernel::message_port Port;
	vector<triple<string,string,string> > mountpoints = getMountpoints(Port);
	cout << "device:                       size:   free:  filesystem:         mounted on:" << endl;
	for(size_t i=0;i < mountpoints.size();i++)
	{
		cout << mountpoints[i].first;
		for (size_t y=mountpoints[i].first.length();y < 30;y++)cout << ' ';
		
		size_t size, free;
		getFilesystemInfo(Port, mountpoints[i].second.c_str(), size, free);
		
		if (size != 0)
		{
			size_t percent = (free * 100) / (size / 100);
			pair<size_t,string> Size = convertSize(size);
			if (Size.first < 1000)cout << ' ';
			if (Size.first < 100)cout << ' ';
			if (Size.first < 10)cout << ' ';
			cout << Size.first << Size.second;
			if (Size.second.length() == 1)cout << "  ";
			else if (Size.second.length() == 2)cout << ' ';
			cout << ' ';
			
			if ((percent / 100) < 10)cout << ' ';
			cout << (percent / 100) << '.';
			if ((percent % 100) < 10)cout << '0';
			cout << (percent % 100) << "% ";
		}
		else cout << "               ";
		
		cout << mountpoints[i].third;
		for (size_t y=mountpoints[i].third.length();y < 20;y++)cout << ' ';
		cout << mountpoints[i].second << endl;
	}
	return 0;
}
