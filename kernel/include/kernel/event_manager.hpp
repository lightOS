/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_EVENTMANAGER_HPP
#define LIGHTOS_KERNEL_EVENTMANAGER_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <cstddef>
#include <cstdint>
#include <vector>
#include <utility>
#include <kernel/spinlock.hpp>
#include <kernel/port_manager.hpp>
#include <kernel/utils/macros.hpp>

namespace kernel
{
  /*! The event-manager */
  class event_manager
  {
    SINGLETON(event_manager);

    public:
      /*! Registers an IRQ-event
       *\param[in] port the port the event should be sent to
       *\param[in] irq the IRQ number
       *\param[im] params additional information regarding the irq
       *\return an IRQ-id if successfull, 0 otherwise */
      size_t register_irq(libkernel::port_id_t port,
                          size_t param1,
                          size_t param2);
      /*! Acknoledge the IRQ-event
       *\param[in] port the port the event is registered with
       *\param[in] irq_id the IRQ-id (as returned by event_manager::register_irq */
      void acknoledge_irq(libkernel::port_id_t port,
                          size_t irq_id);
      /*! Unregister an IRQ-event
       *\param[in] port the port the event is registered with
       *\param[in] irq_id the IRQ-id (as returned by event_manager::register_irq */
      void unregister_irq(libkernel::port_id_t port,
                          size_t irq_id);
      /*! Handle an irq
       *\param[in] vector the interrupt vector */
      void handle_irq(uint8_t vector);

      /*! Register a timer event
       *\param[in] port the port the event is registered with
       *\param[in] ms microseconds
       *\param[in] periodic repeat the event or delete it afterwards? */
      void register_timer(libkernel::port_id_t port,
                          uint64_t microseconds,
                          bool periodic);
      /*! Unregister a timer event
       *\param[in] port the port the event is registered with */
      void unregister_timer(libkernel::port_id_t port);
      /*! Handle a tick count update
       *\param[in] microseconds microseconds elapsed since the last update */
      void tick(uint64_t microseconds);
      /*! Get the number of ticks (in ms)
       *\return the number of ticks (in ms) */
      size_t get_ticks();

    private:
      struct timer_event
      {
        libkernel::port_id_t   port;
        uint64_t remaining;
        uint64_t microseconds;
        bool     periodic;
      };

      /*! Tick count */
      uint64_t mTicks;
      /*! List of registered Timer-events */
      std::vector<timer_event> mTimerEvents;
      #ifdef _LIGHTOS_SMP
        /*! Spinlock to protect the event_manager in a multiprocessor environment */
        lightOS::spinlock mLock;
      #endif

      /*! List of registered IRQ-events */
      std::vector<std::pair<libkernel::port_id_t, size_t> > mIRQEvents;
      #ifdef _LIGHTOS_SMP
        /*! Spinlock to protect the event_manager's irq events in a multiprocessor environment */
        lightOS::spinlock mIRQLock;
      #endif
  };
}

/*@}*/

#endif
