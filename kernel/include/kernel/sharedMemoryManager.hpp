/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_SHAREDMEMORYMANAGER_HPP
#define LIGHTOS_KERNEL_SHAREDMEMORYMANAGER_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <vector>
#include <utility>
#include <kernel/id.hpp>
#include <kernel/process.hpp>
#include <kernel/spinlock.hpp>
#include <kernel/utils/macros.hpp>

namespace kernel
{
	class sharedMemoryManager
	{
    SINGLETON(sharedMemoryManager);

		public:
			/*! Create a shared-memory entity
			 *\param[in] Process the creating process
			 *\param[in] size the requested size of the shared-memory entity
			 *\return the Id and the address of the shared-memory entity if successfull, 0 otherwise */
			std::pair<libkernel::shared_memory_id_t, void*> create(process &Process, size_t size);
			/*! Transfer a shared-memory entity to another process
			 *\param[in] dstProcess the destination process
			 *\param[in] srcProcess the source process
			 *\param[in] id the Id of the shared-memory entity
			 *\param[in] flags the flags that describe the kind of mapping
			 *\return the Id and the address of the shared-memory entity if successfull, 0 otherwise */
			std::pair<void*, size_t> transfer(process &dstProcess, process &srcProcess, libkernel::shared_memory_id_t id, size_t flags);
			/*! Destroy a shared-memory entity (only within this process)
			 *\param[in] Process the destroying process
			 *\param[in] id the shared-memory entity Id
			 *\return true, if successfull, false otherwise */
			bool destroy(process &Process, libkernel::shared_memory_id_t id);
		
		private:
			/*! A shared-memory entity */
			struct entity
			{
				public:
					/*! The constructor
					 *\param[in] Id the id
					 *\param[in] Size the size */
					inline entity(libkernel::shared_memory_id_t Id, size_t Size)
						: id(Id), size(Size){}
					
					/*! The shared-memory entity Id */
					libkernel::shared_memory_id_t id;
					/*! The shared-memory entity size */
					size_t size;
					/*! The list of processes that have this share-memory entity mapped */
					std::vector<std::pair<libkernel::process_id_t, void*> > list;
			};
			/*! Get shared memory by the id
			 *\param[in] id the id
			 *\note The multiprocessor lock MUST be acquired BEFORE this function is called and is NOT released by
			 *      this function
			 *\return pointer to the memory region */
			entity *get_by_id(libkernel::shared_memory_id_t id);
			
			/*! List of shared-memory entities */
			std::vector<entity*> mList;
			#ifdef _LIGHTOS_SMP
				/*! Spinlock to protect the port_manager in a multiprocessor environment */
				lightOS::spinlock mLock;
			#endif
	};
}

/*@}*/

#endif
