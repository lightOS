/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_MULTIBOOT_HPP
#define LIGHTOS_KERNEL_MULTIBOOT_HPP

#include <cstdint>
#include <kernel/utils/macros.hpp>

/*! \addtogroup kernel kernel */
/*@{*/

namespace kernel
{
  namespace x86_shared
  {
    /*! Structure describing one multiboot module */
    struct module
    {
      /*! 32bit start address */
      uint32_t start;
      /*! 32bit end address */
      uint32_t end;
      /*! 32bit pointer to the name */
      uint32_t name;
      /*! reserved */
      uint32_t res;
    } PACKED;

    /*! Structure describing one memory map entry */
    struct memory_map
    {
      /*! Size of the entry */
      uint32_t size;
      /*! The base address of the memory region */
      uint64_t address;
      /*! The size of the memory region */
      uint64_t length;
      /*! The type of the memory region */
      uint32_t type;
    } PACKED;

    /*! The multiboot information structure */
    struct multiboot
    {
      /*! Flags */
      uint32_t flags;
      /*! RAM size under 1MB in kB */
      uint32_t mem_low;
      /*! RAM size in kB */
      uint32_t mem_high;
      /*! The number of the boot device */
      uint32_t boot_device;
      /*! 32bit pointer to the command line */
      uint32_t cmdline;
      /*! Number of modules loaded */
      uint32_t module_count;
      /*! 32bit pointer to an array of module structures */
      uint32_t mod;
      uint32_t res0;
      uint32_t res1;
      uint32_t res2;
      uint32_t res3;
      /*! Size of the memory map */
      uint32_t mmap_size;
      /*! 32bit pointer to the memory map */
      uint32_t mmap;

      uint32_t drives_length;
      uint32_t drives_addr;
      uint32_t config_table;
      uint32_t bootloader_name;
    } PACKED;

    // TODO: section type conflict weirdness
    extern "C" void parse_multiboot(const multiboot& boot) /* INIT_ONLY */;
  }
}

/*@}*/

#endif
