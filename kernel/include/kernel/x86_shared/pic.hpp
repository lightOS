/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_SHARED_PIC_HPP
#define LIGHTOS_KERNEL_X86_SHARED_PIC_HPP

/*
 * Standard C includes
 */
#include <cstdint>

/*
 * Kernel includes
 */
#include <kernel/io_port.hpp>
#include <kernel/utils/macros.hpp>
#include <kernel/x86_shared/interrupt_controller.hpp>



/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup kernel_x86_shared x86-shared */
/*@{*/

namespace kernel
{
  class processor;

  namespace x86_shared
  {
    class pic : public interrupt_controller
    {
      SINGLETON(pic);

      friend class interrupt_controller;
      friend class kernel::processor;

      public:
        virtual void init();
        virtual void handle_irq(uint8_t intnumber, interrupt_stackframe &stackframe);
        virtual uint8_t register_isa_irq(uint8_t irq, uint8_t priority);
        virtual uint8_t register_pci_irq(uint8_t irq,
                                         uint8_t intn,
                                         uint8_t bus,
                                         uint8_t device,
                                         uint8_t priority);
        virtual void acknoledge_irq(uint8_t vector);
        virtual void unregister_irq(uint8_t vector);

      private:
        void eoi(int irq);
        void mask();
        void mask(uint8_t number);
        void unmask();
        void unmask(uint8_t number);

        kernel::io_port m_pic1;
        kernel::io_port m_pic2;
    };
  }
}

/*@}*/
/*@}*/

#endif
