/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_SHARED_GDT_HPP
#define LIGHTOS_KERNEL_X86_SHARED_GDT_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <cstdint>
#include <cstddef>

namespace kernel
{
	class gdt
	{
		public:
			inline static gdt &instance(){return mInst;}
			/*! Initialize
			 *\note Called once */
			void initialize();
			/*! Load the GDT
			 *\note Called once per processor */
			void load();
			#ifdef _LIGHTOS_V86
				void clear_tss_busy_bit(size_t index);
			#endif
		private:
			inline gdt(){}
			inline ~gdt(){}
			
			size_t get_descriptor_count();
			size_t tss_descriptor_size();
			void set_tss_descriptor(size_t index,
									uintptr_t address);
			void set_segment_descriptor(size_t index,
										uintptr_t address,
										size_t limit,
										uint8_t flags,
										uint8_t flags2);
			
			static gdt mInst;
			
			size_t mSize;
			union descriptor
			{
				struct
				{
					uint16_t limit;
					uint16_t address0;
					uint8_t address1;
					uint8_t flags;
					uint8_t flags_limit;
					uint8_t address2;
				}__attribute__((packed)) x86;
				
				struct
				{
					uint32_t address3;
					uint32_t res0;
				}__attribute__((packed)) x86_64;
			} __attribute__((packed));
			descriptor *mGdt;
	};
}

/*@}*/

#endif
