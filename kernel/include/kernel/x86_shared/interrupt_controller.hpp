/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_SHARED_INTERRUPT_CONTROLLER_HPP
#define LIGHTOS_KERNEL_X86_SHARED_INTERRUPT_CONTROLLER_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <cstddef>
#include <cstdint>
#include <kernel/arch/arch.hpp>

#define BUS_ID_NONE														0
#define BUS_ID_ISA														1
#define BUS_ID_PCI														2

namespace kernel
{
	/*! Abstract base class for interrupt controllers */
	class interrupt_controller
	{
		public:
			/*! Get the currently used interrupt controller */
			static interrupt_controller &instance();
			
			/*! Initialize the interrupt controller */
			virtual void init() = 0;
			/*! Handle an incomming irq
			 *\param[in] intnumber the interrupt vector number */
			virtual void handle_irq(uint8_t intnumber,
									interrupt_stackframe &stackframe) = 0;
			/*! Register an ISA IRQ
			 *\param[in] irq the IRQ number
			 *\param[in] priority the IRQ priority (range: 2 - 14)
			 *\return the interrupt vector number */
			virtual uint8_t register_isa_irq(uint8_t irq, uint8_t priority) = 0;
			/*! Register a PCI IRQ
			 *\param[in] irq the IRQ number in the PCI Config Space
			 *\param[in] intn the interrupt line in the PCI Config Space
			 *\param[in] bus the PCI bus number the device is connected to
			 *\param[in] device the PCI device number
			 *\param[in] priority the IRQ priority (range: 2 - 14)
			 *\return the interrupt vector number */
			virtual uint8_t register_pci_irq(	uint8_t irq,
												uint8_t intn,
												uint8_t bus,
												uint8_t device,
												uint8_t priority) = 0;
			/*! Acknoledge IRQ
			 *\param[in] vector the interrupt vector number (as return by register_*_irq */
			virtual void acknoledge_irq(uint8_t vector) = 0;
			/*! Unregister an IRQ
			 *\param[in] vector the interrupt vector number (as return by register_*_irq */
			virtual void unregister_irq(uint8_t vector) = 0;
			
			#ifdef _LIGHTOS_SMP
				/*! Switch to the I/O and Local APIC */
				static void set_ioapic_mode();
			#endif
		private:
			#ifdef _LIGHTOS_SMP
				/*! Use the I/O and Local APIC? */
				static bool mUseIOApic;
			#endif
	};
}

/*@}*/

#endif
