/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_SHARED_SERIAL_HPP
#define LIGHTOS_KERNEL_X86_SHARED_SERIAL_HPP

/*
 * Standard C includes
 */
#include <cstdint>

/*
 * Kernel includes
 */
#include <kernel/spinlock.hpp>
#include <kernel/utils/macros.hpp>

/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup kernel_x86_shared x86-shared */
/*@{*/

#if !defined(NDEBUG)

#define KERNEL_HAS_SERIAL

namespace kernel
{
  namespace x86_shared
  {
    /*! The x86 and x86-64 serial device */
    class serial
    {
      SINGLETON(serial);

      public:
        /*! Set the COM Port
        *\param[in] port 0-3
        *\return true, if successfull, false otherwise */
        bool set_index(int port_index);
        /*! Get the used COMx
        *\return the I/O port or 0 if none */
        uint16_t get_used_port();
        /*! Initialize the COM Port */
        void init_used_port();

        /*! Get the I/O ports for COM0-3
        *\param[out] ser0 I/O address of COM0
        *\param[out] ser1 I/O address of COM1
        *\param[out] ser2 I/O address of COM2
        *\param[out] ser3 I/O address of COM3 */
        void get(uint16_t &ser0,
                 uint16_t &ser1,
                 uint16_t &ser2,
                 uint16_t &ser3);

        void put(char c);

      private:
        /*! The COM Ports */
        uint16_t m_ports[4];
        /*! The selected Port */
        int      m_index;

        /*! Spinlock to protect the serial in a multiprocessor environment */
        SPINLOCK_DECL(m_lock)
    };
  }
}

#endif

/*@}*/
/*@}*/

#endif
