/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_SHARED_IDT_HPP
#define LIGHTOS_KERNEL_X86_SHARED_IDT_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <cstdint>

namespace kernel
{
	class idt
	{
		public:
			typedef void (*interrupt_handler)();
			
			inline static idt &instance(){return mInst;}
			void init();
			void set_interrupt_gate(uint8_t number,
									interrupt_handler handler);
			interrupt_handler get_interrupt_gate(uint8_t number);
			#ifdef X86
				void set_userspace_interrupt_gate(	uint8_t number,
													interrupt_handler handler);
			#endif
			#ifdef _LIGHTOS_V86
				void set_task_gate(uint8_t number, uint8_t flags, uint16_t selector);
			#endif
			void load();
		private:
			inline idt(){}
			inline ~idt(){}
			
			static idt mInst;
			
			struct
			{
				uint16_t offset0;
				uint16_t selector;
				uint8_t reserved;
				uint8_t flags;
				uint16_t offset1;
				#ifdef X86_64
					uint32_t offset2;
					uint32_t reserved2;
				#endif
			} __attribute__((packed)) mIdt[256];
	};
	
	extern "C" void initialize_idt();
}

/*@}*/

#endif
