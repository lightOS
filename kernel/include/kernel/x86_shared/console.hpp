/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_SHARED_CONSOLE_HPP
#define LIGHTOS_KERNEL_X86_SHARED_CONSOLE_HPP


/*
 * Standard C includes
 */
#include <cstddef>

/*
 * Libarch includes
 */
#include <libarch/arch/console.h>
#include <libarch/exclusive_pointer.hpp>

/*
 * Kernel includes
 */
#include <kernel/spinlock.hpp>
#include <kernel/utils/macros.hpp>


/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup kernel_x86_shared x86-shared */
/*@{*/

#define KERNEL_CONSOLE_NOTICE          _LIBARCH_CONSOLE_GREEN
#define KERNEL_CONSOLE_WARNING         _LIBARCH_CONSOLE_WHITE
#define KERNEL_CONSOLE_ERROR           _LIBARCH_CONSOLE_RED
#define KERNEL_CONSOLE_FATAL           _LIBARCH_CONSOLE_RED

namespace kernel
{
  namespace x86_shared
  {
    class console_window;

    /*
     * Types
     */

    /** Console window identifier */
    typedef size_t                                                           console_id_t;
    /** Color type for the console */
    typedef uint8_t                                                          console_color_t;
    /** Console window pointer */
    #if defined(_LIGHTOS_MULTIPROCESSOR)
      typedef libarch::exclusive_pointer<console_window, lightOS::spinlock>  console_window_ptr;
    #else
      typedef libarch::exclusive_pointer<console_window, void>               console_window_ptr;
    #endif

    /*
     * Constants
     */

    /** Identifier of the kernel console window */
    const console_id_t kernel_console_id     = 0;
    /** Width of a console window */
    const size_t       console_window_width  = 80;
    /** Height of a console window */
    const size_t       console_window_height = 25;



    /*
     * Class types
     */

    /** Console window
     *\todo documentation */
    class console_window
    {
      NON_ASSIGNABLE(console_window);
      NON_COPY_CONSTRUCTABLE(console_window);

      friend class console;

      public:
        void copy_from(const void* raw_mem);
        void copy_to(void* raw_mem) const;

        inline console_id_t id() const;
        inline size_t cursor() const;
        inline void cursor(size_t cursor);
        inline void enable_cursor(bool enabled);
        inline bool is_cursor_enabled() const;

        inline void character(size_t index,
                              char character);
        inline void color(size_t index,
                          console_color_t color);

      private:
        console_window(console_id_t id,
                       console_window* prev_window,
                       console_window* next_window);
        ~console_window();

        inline void* address();
        inline const void* address() const;

        bool         m_bCursor;
        size_t       m_cursor;
        console_id_t m_id;

        /* For the console class */
        console_window* m_prev_window;
        console_window* m_next_window;

        SPINLOCK_DECL_MUTABLE(m_lock)
    };

    /** Textmode console */
    class console
    {
      SINGLETON(console);

      public:
        /** Create a new console window
         *\return 0, if the call failed, pointer to the console
         *        window otherwise. */
        console_window_ptr create();

        /** Get a console window by its identifier
         *\param[in] id the identifier
         *\return 0, if no console window with that identifier was found,
         *        pointer to the console window otherwise */
        console_window_ptr get(console_id_t id);

        /** Get the current console window
         *\return reference to the current console window */
        console_window_ptr get();

        /** Switch the console window
         *\param[in] cons reference to the new console window */
        void switch_to(console_window_ptr& cons);

      private:
        /* NOTE: We maintain a double-linked list of our console_windows's */
        /** First console window in our linked list */
        console_window* m_first_window;
        /** Last console window in our linked list */
        console_window* m_last_window;

        /** The currently displayed console window */
        console_window* m_current_window;

        /** Pointer to the video RAM */
        void*           m_video_ram;

        /*! Spinlock to protect the console and console_window in a multiprocessor environment */
        SPINLOCK_DECL_MUTABLE(m_lock)
    };



    /*
     * Part of the implementation of console_window
     */

    console_id_t console_window::id() const
    {
      return m_id;
    }

    void console_window::cursor(size_t cursor)
    {
      if (cursor < console_window_width * console_window_height)
        m_cursor = cursor;
    }

    size_t console_window::cursor() const
    {
      return m_cursor;
    }

    void console_window::enable_cursor(bool enabled)
    {
      m_bCursor = enabled;
    }

    bool console_window::is_cursor_enabled() const
    {
      return m_bCursor;
    }

    void console_window::character(size_t index,
                                   char character)
    {
      *adjust_pointer_cast<char>(address(), 2 * index) = character;
    }

    void console_window::color(size_t index,
                               console_color_t color)
    {
      *adjust_pointer_cast<console_color_t>(address(), 2 * index + 1) = color;
    }

    void* console_window::address()
    {
      return adjust_pointer_cast<void>(this, sizeof(console_window));
    }

    const void* console_window::address() const
    {
      return adjust_pointer_cast<const void>(this, sizeof(console_window));
    }
  }
}

/*@}*/
/*@}*/

#endif
