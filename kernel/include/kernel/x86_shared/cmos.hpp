/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_SHARED_CMOS_HPP
#define LIGHTOS_KERNEL_X86_SHARED_CMOS_HPP


/*
 * Standard C includes
 */
#include <cstdint>
#include <cstddef>

/*
 * Kernel includes
 */
#include <kernel/io_port.hpp>
#include <kernel/utils/macros.hpp>
#include <kernel/spinlock.hpp>


/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup device device */
/*@{*/

namespace kernel
{
  namespace x86_shared
  {
    /*! The CMOS device */
    class cmos
    {
      SINGLETON(cmos);

      public:
        /*! Get the current second
        *\return the current second */
        size_t second();
        /*! Get the current minute
        *\return the current minute */
        size_t minute();
        /*! Get the current hour
        *\return the current hour */
        size_t hour();
        /*! Get the day of the month
        *\return the day of the month */
        size_t dayofmonth();
        /*! Get the month
        *\return the month */
        size_t month();
        /*! Get the year
        *\return the year */
        size_t year();
        /*! Is summertime? */
        bool summertime();
        /*! Activates the periodic IRQ8 (1024Hz) */
        void activate_periodic_irq();
        /*! Ack the periodic IRQ8 */
        void ack_periodic_irq();

      private:
        /*! Read the cmos
        *\param[in] offset offset to read from
        *\return the read value */
        uint8_t read(uint8_t offset);
        /*! Write the cmos
        *\param[in] offset offset to write to
        *\param[in] value value to write */
        void write(uint8_t offset, uint8_t value);
        /*! Convert the date/time information if needed from packed-BCD to binary
        *\param[in] byte the byte that is to be converted
        *\return the converted byte */
        uint8_t convert(uint8_t byte);

        /** The CMOS I/O region */
        io_port m_io;

        #ifdef _LIGHTOS_SMP
          /*! Spinlock to protect the cmos in a multiprocessor environment */
          lightOS::spinlock mLock;
        #endif
    };
  }
}

/*@}*/
/*@}*/

#endif
