/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_SHARED_APIC_HPP
#define LIGHTOS_KERNEL_X86_SHARED_APIC_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <kernel/id.hpp>
#include <vector>
#include <cstdint>
#include <kernel/x86_shared/interrupt_controller.hpp>

#ifdef _LIGHTOS_SMP

#define APIC_BASE													0xFEE00000

namespace kernel
{
	typedef id apicId;
	
	class local_apic
	{
		public:
			local_apic();
			/*! Initialize
			 *\note Called once */
			static void initialize();
			static apicId id();
			static void eoi();
			inline apicId get_id() const{return mId;}
			void send_ipi(	apicId destination,
							uint8_t vector,
							size_t delivery_mode,
							bool bAssert,
							bool bLevelTriggered);
			inline ~local_apic(){}
			
			static bool check();
			void start_calibration();
			void end_calibration();
			
			void set_lint(	size_t n,
							size_t delivery_mode,
							size_t trigger_mode,
							size_t polarity);
			size_t get_lint_delivery_mode(size_t n);
			static void init_lint();
		protected:
			static void init_timer();
			
			static uint32_t read_register(uint32_t reg);
			static void write_register(uint32_t reg, uint32_t val);
		private:
			static volatile uint32_t *mBase;
			static uint32_t mTimerDelta;
			apicId mId;
			
			struct
			{
				bool masked;
				size_t delivery_mode;
				size_t trigger_mode;
				size_t polarity;
			} mLINT[2];
	};
	
	
	class io_apic
	{
		friend class smp;
		
		public:
			struct pci_irq
			{
				uint8_t bus;
				uint8_t device;
				uint8_t intn;
			};
			
			struct isa_irq
			{
				uint8_t irq;
			};
			
			static size_t count(){return mIoApic.size();}
			static io_apic *get_by_index(size_t index){return mIoApic[index];}
			static io_apic *get_by_id(apicId ID);
			inline apicId get_id() const{return mId;}
			inline size_t get_int_pins() const{return mIntPins;}
			uint8_t register_isa_irq(uint8_t irq, uint8_t vector);
			uint8_t register_pci_irq(	uint8_t irq,
										uint8_t intn,
										uint8_t bus,
										uint8_t device,
										uint8_t vector);
			size_t intn_from_vector(uint8_t vector);
			void unmask(size_t n);
			void mask(size_t n);
		
		protected:
			static apicId create(uint32_t *base, apicId id);
			void set_redirection_entry(	size_t n,
										size_t delivery_mode,
										size_t trigger_mode,
										size_t polarity,
										size_t bus_type,
										void *bus_info);
		
		private:
			io_apic(uint32_t *base, apicId id);
			inline ~io_apic(){}
			
			uint32_t read_register(uint32_t reg);
			void write_register(uint32_t reg, uint32_t val);
			
			struct redirection_entry
			{
				bool used;
				bool masked;
				size_t delivery_mode;
				size_t trigger_mode;
				size_t polarity;
				uint8_t vector;
				size_t bus_type;
				void *bus_info;
			};
			
			volatile uint32_t *mBase;
			apicId mId;
			size_t mIntPins;
			redirection_entry *mRedirectionTable;
			
			static std::vector<io_apic*> mIoApic;
	};
	
	class apic : public interrupt_controller
	{
		friend class interrupt_controller;
		
		public:
			enum
			{
				delivery_mode_fixed				= 0,
				delivery_mode_lowest_priority	= 1,
				delivery_mode_smi				= 2,
				delivery_mode_nmi				= 4,
				delivery_mode_init				= 5,
				delivery_mode_startup			= 6,
				delivery_mode_extint			= 7
			};
			
			enum
			{
				trigger_mode_edge				= 0,
				trigger_mode_level				= 1
			};
			
			enum
			{
				polarity_active_high			= 0,
				polarity_active_low				= 1
			};
			
			virtual void init();
			virtual void handle_irq(uint8_t intnumber, interrupt_stackframe &stackframe);
			virtual uint8_t register_isa_irq(	uint8_t irq,
												uint8_t priority);
			virtual uint8_t register_pci_irq(	uint8_t irq,
												uint8_t intn,
												uint8_t bus,
												uint8_t device,
												uint8_t priority);
			virtual void acknoledge_irq(uint8_t vector);
			virtual void unregister_irq(uint8_t vector);
		
		protected:
			static apic &instance(){return mInst;}
		
		private:
			inline apic(){}
			inline ~apic(){}
			
			static apic mInst;
	};
}

#endif

/*@}*/

#endif
