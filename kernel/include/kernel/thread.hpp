/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_THREAD_HPP
#define LIGHTOS_KERNEL_THREAD_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <utility>
#include <kernel/id.hpp>
#include <kernel/arch/arch.hpp>
#include <kernel/context.hpp>
#include <libkernel/message.hpp>

namespace kernel
{
  extern "C"
  {
    void executeThread(thread_state &state,
                       void *Context);
    void executeIdleThread();

    #ifdef X86
      void executeV8086Thread();
    #endif
  }

  class process;

  /*!\todo doc */
  class thread
  {
    friend class process;
    friend class processor;
    public:
      enum state
      {
        run,
        wait_for_port,
        wait_for_tick,
        suspended
      };
      /*! Get the thread id */
      inline libkernel::thread_id_t getId(){return tid;}
      /*! Get the process */
      inline process &getProcess() const{return Process;}
      /*! Grant I/O access */
      void grantIOAccess();
      /*! What's the thread's status? */
      inline state status(){return State;}
      /*! Block the thread
       *\param[in] reason the reason */
      void block(state reason);
      /*! Unblock the thread */
      void unblock();
      /*! Unblock the thread */
      void unblock(libkernel::port_id_t port,
              libkernel::port_id_t source,
              size_t param1,
              size_t param2,
              size_t param3,
              size_t param4);
      thread_state &get_cpu_state(){return mCpuState;}
      void set_cpu_state(interrupt_stackframe &frame);
      /*! Get the thread stack */
      inline std::pair<void*,size_t> get_stack(){return std::pair<void*,size_t>(threadStack, threadStackSize);}
      bool handle_page_fault(void *address);
    protected:
      /*! The constructor */
      thread( process &Process,
          uintptr_t entryPoint,
          void *stack,
          size_t stacksize,
          size_t param1,
          size_t param2,
          libkernel::thread_id_t id);
      /*! The destructor */
      ~thread();
    private:
      /*! The thread id */
      libkernel::thread_id_t tid;
      /*! The process */
      process &Process;
      /*! The thread stack */
      void *threadStack;
      size_t threadStackSize;
      /*! What is the thread's state? */
      state State;

      thread_state mCpuState;
      void *mFpuState;
      void *mFpuState2;
  };
}

/*@}*/

#endif
