/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_PAGEALLOCATOR_HPP
#define LIGHTOS_KERNEL_PAGEALLOCATOR_HPP


/*
 * Standard C includes
 */
#include <cstddef>

/*
 * Libarch includes
 */
#include <libarch/scoped_lock.hpp>

/*
 * libkernel includes
 */
#include <libkernel/compiler.hpp>

/*
 * Kernel includes
 */
#include <kernel/spinlock.hpp>
#include <kernel/utils/macros.hpp>


/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup kernel_pmm physical memory-management */
/*@{*/

namespace kernel
{
  /*! page_allocator class for the management of physical pages for normal usage */
  class page_allocator
  {
    SINGLETON(page_allocator);

    public:
      /*! Allocate a new page
       *\return physical address of the page or 0 if the allocation failed */
      void *allocate();
      /*! Free a previously allocated page
       *\param[in] physical address of the page */
      void free(void *page);

      /*! Number of free pages
       *\return the number of free pages */
      inline size_t size() const;
      /*! Total number of pages (userfriendly version)
       *\return the total number of pages (userfriendly version) */
      inline size_t capacity() const;
      /*! Set the total number of pages (userfriendly version)
       *\param[in] the number of pages (userfriendly version) */
      inline void capacity(size_t i) INIT_ONLY;

    private:
      /*! The number of free pages */
      size_t m_size;
      /*! The number of free pages on the stack itself */
      size_t m_stack_size;
      /*! The number of total pages (userfriendly version) */
      size_t m_page_count;
      /*! Pointer to the stack */
      void** m_stack;
      /** Pointer to the current (allocated) end of the stack */
      void** m_stack_top;

      /*! Spinlock to protect the page_allocator in a multiprocessor environment */
      SPINLOCK_DECL_MUTABLE(m_lock)
  };


  /*
   * Part of the implementation
   */

  size_t page_allocator::size() const
  {
    SCOPED_LOCK(m_lock, scope);
    return m_size;
  }

  size_t page_allocator::capacity() const
  {
    SCOPED_LOCK(m_lock, scope);
    return m_page_count;
  }

  void page_allocator::capacity(size_t i)
  {
    SCOPED_LOCK(m_lock, scope);
    m_page_count = i;
  }
}

/*@}*/
/*@}*/

#endif
