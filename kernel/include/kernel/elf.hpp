/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_ELF_HPP
#define LIGHTOS_KERNEL_ELF_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <memory>
#include <vector>
#include <utility>
#include <kernel/context.hpp>
#include <kernel/process.hpp>
#include <kernel/arch/arch_elf.hpp>
#include <kernel/spinlock.hpp>

namespace kernel
{
  /*! The elf class for loading elf executables
  *\note the elf class does not free the memory */
  class elf
  {
    public:
      /*! The constructor
      *\param[in] start address of the beginning of the elf file
      *\param[in] size the size of the memory location */
      elf(void *start, size_t size);
      /*! Is it a valid elf file? */
      bool valid();
      /*! Is it a shared library? */
      bool is_library() const{return mIsLibrary;}
      /*! Create the process image
       *\param[in,out] Context the context
       *\param[out] Pages List of allocated pages
       *\param[out] libName if the function called failed, this is the name of the needed library or ""
       *\return true if successfull, false otherwise */
      bool create_process_image(context &Context,
                                std::vector<void*> &Pages,
                                std::string &libName);
      /*! Returns the entry point
      *\return address of the entry point */
      inline uintptr_t entry_point(){if (mHeader == 0)return 0;return mHeader->entry;}
      /*! Create the library image
       *\param[in] name the library name */
      void create_library_image(const char *name);
      
      /*! Get the number of loaded libaries */
      static size_t library_count();
      /*! Get the name of a library */
      static size_t get_library_name(size_t index,
                                     char *result);
      /*! Get the start address and the size of the library */
      static std::pair<uintptr_t,size_t> get_library_address(size_t index);
    private:
      
      /*! Program Header entry flags */
      enum
      {
        PH_FLAG_EXECUTE     = 0x01,
        PH_FLAG_WRITE     = 0x02,
        PH_FLAG_READ      = 0x04
      };
      
      /*! Element types of the DYNAMIC segment */
      enum
      {
        DT_NULL         = 0x00,
        DT_NEEDED       = 0x01,
        DT_PLTRELSZ       = 0x02,
        DT_PLTGOT       = 0x03,
        DT_HASH         = 0x04,
        DT_STRTAB       = 0x05,
        DT_SYMTAB       = 0x06,
        DT_RELA         = 0x07,
        DT_RELASZ       = 0x08,
        DT_RELAENT        = 0x09,
        DT_SYMENT       = 0x0B,
        DT_REL          = 0x11,
        DT_RELSZ        = 0x12,
        DT_RELENT       = 0x13,
        DT_PLTREL       = 0x14,
        DT_JMPREL       = 0x17
      };
      
      struct library
      {
        std::string name;
        uintptr_t   base_address;
        size_t      size;
        std::vector<std::triple<void*,void*,size_t> > second;
        std::vector<std::pair<std::string,void*> > third;
      };
      
      void *get_file_offset(void *vAddress, size_t size);
      void parse_dynamic(elf_dynamic_entry *list,
                         size_t count,
                         size_t minAddr);
      void *lib_find_physical_page(uintptr_t address,
                                   library &lib);
      void lib_do_relocation_table(elf_relocation *table,
                                   size_t size,
                                   library &lib);
      void lib_do_relocation_a_table(elf_relocation_a *table,
                                     size_t size,
                                     library &lib);
      void process_do_relocation_table(elf_relocation *table,
                                       size_t size);
      void process_do_relocation_a_table(elf_relocation_a *table,
                                         size_t size);
      void do_relocation(elf_relocation *Relocation,
                         void *address1,
                         void *address2,
                         size_t base,
                         elf_symbol *Symbol);
      void do_relocation_a(elf_relocation_a *Relocation,
                           void *address1,
                           void *address2,
                           size_t base,
                           elf_symbol *Symbol);
      
      uint32_t get32(void *address1,
                     void *address2);
      void set32(void *address1,
                 void *address2,
                 uint32_t value);
      void set64(void *address1,
                 void *address2,
                 uint64_t value);
      
      /*! Pointer to the elf header */
      elf_header *mHeader;
      /*! Size of the elf file */
      size_t mSize;
      /*! Pointer to the elf program header table */
      elf_program_header *mProgHeader;
      /*! Is it a shared library instead of a normal executable? */
      bool mIsLibrary;
      
      char *mStringTable;
      size_t mSymbolCount;
      size_t mSymbolSize;
      elf_symbol *mSymbolTable;
      size_t mRelocationSize;
      size_t mRelocationTableSize;
      elf_relocation *mRelocationTable;
      size_t mRelocationASize;
      size_t mRelocationATableSize;
      elf_relocation_a *mRelocationATable;
      std::vector<size_t> mNeededLibraries;
      void *mPltGot;
      void *mPltGotRelocationTable;
      size_t mPltGotRelocationTableSize;
      size_t mPltGotRelocationType;
      
      /*! The libraries */
      static std::vector<library> m_library;
      /*! Base address */
      static uintptr_t m_library_base;
      #ifdef _LIGHTOS_SMP
        /*! Spinlock to protect the elf class in a multiprocessor environment */
        static lightOS::spinlock m_lock;
      #endif
  };
}

/*@}*/

#endif
