/*
lightOS kernel
Copyright (C) 2008-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_IO_PORT_HPP
#define LIGHTOS_KERNEL_IO_PORT_HPP



/*
 * Libarch includes
 */
#include <libarch/ioport.hpp>

/*
 * Kernel includes
 */
#include <kernel/utils/macros.hpp>



/*! \addtogroup kernel kernel */
/*@{*/

#if defined(_LIBARCH_HAS_IOPORT)

namespace kernel
{
  // TODO: range checking in debug build

  class io_port
  {
    NON_ASSIGNABLE(io_port);
    NON_COPY_CONSTRUCTABLE(io_port);

    friend class io_port_manager;
    public:
      /** Default constructor */
      inline io_port();
      /** Destructor - frees allocated ressources */
      inline ~io_port();

      /** Allocate an I/O port range
       *\param[in] port the first I/O port in the range
       *\param[in] size the number of I/O ports
       *\return true, if the I/O port range has been allocated successfully,
       *        false otherwise */
      inline bool allocate(libarch::ioport_t port,
                           size_t size);

      /** Free the I/O port range, if any */
      inline void free();

      /** Get the size of the I/O port range */
      inline size_t size() const;

      /** Read a 8bit from an I/O port
       *\param[in] offset the offset to read from
       *\return the value read */
      inline uint8_t  read8(size_t offset = 0);
      /** Read a 16bit from an I/O port
       *\param[in] offset the offset to read from
       *\return the value read */
      inline uint16_t read16(size_t offset = 0);
      /** Read a 32bit from an I/O port
       *\param[in] offset the offset to read from
       *\return the value read */
      inline uint32_t read32(size_t offset = 0);
    #if defined(_LIBARCH_HAS_IOPORT64)
      /** Read a 64bit from an I/O port
       *\param[in] offset the offset to read from
       *\return the value read */
      inline uint64_t read64(size_t offset = 0);
    #endif

      /** Write a 8bit value to an I/O port
       *\param[in] value  the value to write
       *\param[in] offset the offset to write to */
      inline void write8(uint8_t value,
                         size_t offset = 0);
      /** Write a 16bit value to an I/O port
       *\param[in] value  the value to write
       *\param[in] offset the offset to write to */
      inline void write16(uint16_t value,
                          size_t offset = 0);
      /** Write a 32bit value to an I/O port
       *\param[in] value  the value to write
       *\param[in] offset the offset to write to */
      inline void write32(uint32_t value,
                          size_t offset = 0);
    #if defined(_LIBARCH_HAS_IOPORT64)
      /** Write a 64bit value to an I/O port
       *\param[in] value  the value to write
       *\param[in] offset the offset to write to */
      inline void write64(uint64_t value,
                          size_t offset = 0);
    #endif

    private:
      /*! The first port */
      libarch::ioport_t m_port;
      /*! The number of I/O ports */
      size_t            m_size;
  };



  /*
  * Implementation
  */

  io_port::io_port()
    : m_port(), m_size(0)
  {
  }

  io_port::~io_port()
  {
    free();
  }

  bool io_port::allocate(libarch::ioport_t port,
                         size_t size)
  {
    // TODO: io_port_maanger
    m_port = port;
    m_size = size;
    return true;
  }

  void io_port::free()
  {
    // TODO: io_port_manager
    m_port = 0;
    m_size = 0;
  }

  size_t io_port::size() const
  {
    return m_size;
  }

  uint8_t io_port::read8(size_t offset)
  {
    return _LIBARCH_in8(m_port + offset);
  }

  uint16_t io_port::read16(size_t offset)
  {
    return _LIBARCH_in16(m_port + offset);
  }

  uint32_t io_port::read32(size_t offset)
  {
    return _LIBARCH_in32(m_port + offset);
  }

#if defined(_LIBARCH_HAS_IOPORT64)
  uint64_t io_port::read64(size_t offset)
  {
    return _LIBARCH_in64(m_port + offset);
  }
#endif

  void io_port::write8(uint8_t value,
                       size_t offset)
  {
    _LIBARCH_out8(m_port + offset,
                  value);
  }

  void io_port::write16(uint16_t value,
                        size_t offset)
  {
    _LIBARCH_out16(m_port + offset,
                   value);
  }

  void io_port::write32(uint32_t value,
                        size_t offset)
  {
    _LIBARCH_out32(m_port + offset,
                   value);
  }

#if defined(_LIBARCH_HAS_IOPORT64)
  void io_port::write64(uint64_t value,
                        size_t offset)
  {
    _LIBARCH_out64(m_port + offset,
                   value);
  }
#endif
}

#endif

/*@}*/

#endif
