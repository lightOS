/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_LOG_HPP
#define LIGHTOS_KERNEL_LOG_HPP

/*
 * Standard C includes
 */
#include <cstdint>

/*
 * Libkernel includes
 */
#include <libkernel/log.hpp>

/*
 * Kernel includes
 */
#include <kernel/spinlock.hpp>
#include <kernel/utils/macros.hpp>
#include <kernel/virtual_memory.hpp>

/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup kernel_log log */
/*@{*/


#ifndef NDEBUG
  /** Add a notice log entry */
  #define LOG(x)               kernel::helper::tmp_log_entry(libkernel::log_notice)  << x
  /** Add a warning log entry */
  #define WARNING(x)           kernel::helper::tmp_log_entry(libkernel::log_warning) << x

  /** Begin a notice log entry */
  #define LOG_BEGIN(name)      kernel::helper::tmp_log_entry name(libkernel::log_notice);
  /** Continue a notice log entry */
  #define LOG2(name, x)        name << x
  /** Begin a warning log entry */
  #define WARNING_BEGIN(name)  kernel::helper::tmp_log_entry name(libkernel::log_warning);
  /** Continue a warning log entry */
  #define WARNING2(name, x)    name << x
#else
  #define LOG(x)
  #define WARNING(x)
  #define LOG_BEGIN(name)
  #define LOG2(name, x)
  #define WARNING_BEGIN(name)
  #define WARNING2(name, x)
#endif

/** Add an error log entry */
#define ERROR(x)             kernel::helper::tmp_log_entry(libkernel::log_error)   << x
/** Add a fatal log entry and halt the kernel */
#define FATAL(x)             kernel::helper::tmp_log_entry(libkernel::log_fatal)   << x

/** Begin a error log entry */
#define ERROR_BEGIN(name)    kernel::helper::tmp_log_entry name(libkernel::log_error);
/** Continue a error log entry */
#define ERROR2(name, x)      name << x
/** Begin a fatal log entry */
#define FATAL_BEGIN(name)    kernel::helper::tmp_log_entry name(libkernel::log_fatal);
/** Continue a fatal log entry */
#define FATAL2(name, x)      name << x


namespace kernel
{
  namespace helper
  {
    /** Helper class for the log, stores a number for output */
    template<unsigned int base>
    struct integer_base
    {
      NON_ASSIGNABLE(integer_base);
      NON_COPY_CONSTRUCTABLE(integer_base);
      NON_DEFAULT_CONTRUCTABLE(integer_base);

      /** Construct the object with a given number and minimum digit number
       *\param[in] value the number
       *\param[in] min_digits the minimum amount of digits to be output */
      inline integer_base(uint64_t value,
                          size_t min_digits = 0);

      /** The number */
      uint64_t m_value;
      /** The minimum number of digits */
      size_t m_min_digits;
    };

    class tmp_log_entry;
  }

  /** The kernel log */
  class log
  {
    SINGLETON(log);

    /** Needs access to the log */
    friend class helper::tmp_log_entry;

    public:
      /** Commit a string to the log
       *\param[in] begin start address of the string
       *\param[in] end one past the end of the string
       *\param[in] lvl the log level */
      void commit(const char* begin,
                  const char* end,
                  libkernel::log_level lvl);

      /** Get a log entry
       *\param[in] index the index of the log entry
       *\param[in,out] lvl the log level
       *\param[in,out] begin pointer to the beginning of the resulting character array
       *\param[in] size the size of the begin character array
       *\return 0, if no log entry is present, number of bytes of the entry otherwise */
      size_t get_entry(size_t index,
                       libkernel::log_level& lvl,
                       char* begin,
                       size_t size);

    private:
      /** One log entry
       *\note the character sequence is stored immediatly after this structure in memory */
      struct entry
      {
        /** Pointer to the next entry in the log */
        entry*                next;
        /** Pointer to the previous entry in the log */
        entry*                prev;
        /** log level of the entry */
        libkernel::log_level  lvl;
      };

      /** Pointer to the first log entry */
      entry* m_beg;
      /** Pointer to the last log entry */
      entry* m_cur;
      /** Pointer for the next log entry */
      void*  m_cur_addr;
      /** Pointer to the end of the allocated memory region */
      void*  m_cur_max_addr;

      /*! Spinlock to protect the log in a multiprocessor environment */
      SPINLOCK_DECL(m_lock)
  };

  namespace helper
  {
    /** Helper class for the log, stores a log entry temporary */
    class tmp_log_entry
    {
      NON_ASSIGNABLE(tmp_log_entry);
      NON_COPY_CONSTRUCTABLE(tmp_log_entry);
      NON_DEFAULT_CONTRUCTABLE(tmp_log_entry);

      public:
        /** Constructor
         *\param[in] lvl the log level */
        tmp_log_entry(libkernel::log_level lvl);
        /** Destructor */
        ~tmp_log_entry();

        /** Add a character to the temporary log entry
         *\param[in] c the character to add
         *\return reference to the temporary log entry */
        tmp_log_entry& operator << (char c);

        /** Add a character sequence to the temporary log entry
         *\param[in] s pointer to the character sequence
         *\return reference to the temporary log entry */
        tmp_log_entry& operator << (const char* s);

        /** Add the hexadecimal representation of a void pointer to the temporary
         *  log entry
         *\param[in] p the void pointer
         *\return reference to the temporary log entry */
        tmp_log_entry& operator << (const void* p);

        /** Add the hexadecimal representation of an arbitrary pointer to the temporary
         *  log entry
         *\param[in] p the pointer
         *\return reference to the temporary log entry */
        template<typename T>
        tmp_log_entry& operator << (const T* p);

        /** Add the base-ary representation of an integer to the temporary log entry
         *\param[in] integer reference to the description class
         *\return reference to the temporary log entry */
        template<unsigned int base>
        tmp_log_entry& operator << (const integer_base<base>& integer);

      private:
        /** The log level of the entry */
        libkernel::log_level m_level;
        /** Pointer to the first character in the sequence */
        char*                m_first;
        /** Pointer to the last character in the sequence */
        char*                m_next;
        /** Pointer to the last possible character in the sequence */
        char*                m_last;
    };
  }



  /*
   * Implementation of integer_base
   */

  template<unsigned int base>
  helper::integer_base<base>::integer_base(uint64_t value,
                                           size_t min_digits)
    : m_value(value), m_min_digits(min_digits)
  {
  }



  /*
   * Part of the implementation of tmp_log_entry
   */
  template<unsigned int base>
  helper::tmp_log_entry& helper::tmp_log_entry::operator << (const helper::integer_base<base>& integer)
  {
    char digits[64];
    size_t digits_count = 0;

    uint64_t value = integer.m_value;
    while (value != 0)
    {
      uint8_t number = value % base;
      if (number < 10)
        digits[digits_count] = '0' + number;
      else
        digits[digits_count] = 'A' + (number - 10);
      value /= base;
      ++digits_count;
    }

    size_t min_digits = (integer.m_min_digits <= digits_count) ? 0 : (integer.m_min_digits - digits_count);
    if (digits_count == 0 && min_digits == 0)
      min_digits = 1;
    while (min_digits != 0)
    {
      tmp_log_entry::operator << ('0');
      --min_digits;
    }

    while (digits_count != 0)
      tmp_log_entry::operator << (digits[digits_count-- - 1]);

    return *this;
  }

  template<typename T>
  helper::tmp_log_entry& helper::tmp_log_entry::operator << (const T* p)
  {
    return operator << (reinterpret_cast<const void*>(p));
  }
}

/** Typedef the helper class for binary output of numbers */
typedef kernel::helper::integer_base<2>  bin;
/** Typedef the helper class for octal output of numbers */
typedef kernel::helper::integer_base<8>  oct;
/** Typedef the helper class for decimal output of numbers */
typedef kernel::helper::integer_base<10> dec;
/** Typedef the helper class for hexadecimal output of numbers */
typedef kernel::helper::integer_base<16> hex;

/*@}*/
/*@}*/

#endif
