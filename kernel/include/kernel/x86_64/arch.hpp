/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_64_ARCH_HPP
#define LIGHTOS_KERNEL_X86_64_ARCH_HPP

/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup x86_86 x86_64 */
/*@{*/

#include <cstddef>
#include <cstdint>

#define KERNEL_CONTEXT_START
#define KERNEL_CONTEXT_END

/*! Define the address of the first shared-library */
#define SHARED_LIBRARY_START 0x0000000070000000
/*! Define the address of the first memory-region */
#define MEMORY_REGION_START reinterpret_cast<void*>(0x0000400000000000)
/*! Define the address of the first shared-memory region  */
#define SHARED_MEMORY_START reinterpret_cast<void*>(0x0000600000000000)
/*! Define the address of the first thread stack */
#define THREAD_STACK_START reinterpret_cast<void*>(0x0000800000000000)
/*! Extract the message type */
#define MESSAGE_TYPE(x) ((x) & (~0xE000000000000000))

template<class T>
inline T physical_address(T a){return reinterpret_cast<T>(reinterpret_cast<uintptr_t>(a) | 0xFFFF800000000000);}

template<>
inline uintptr_t physical_address<uintptr_t>(uintptr_t a){return (a | 0xFFFF800000000000);}

namespace kernel
{
	/*! Define the type of an IPC message parameter */
	typedef uint64_t message_param_t;
	
	/*! Stack frame after an interrupt */
	struct interrupt_stackframe
	{
		uint64_t r15;
		uint64_t r14;
		uint64_t r13;
		uint64_t r12;
		uint64_t r11;
		uint64_t r10;
		uint64_t r9;
		uint64_t r8;
		uint64_t rbp;
		uint64_t rsi;
		uint64_t rdi;
		uint64_t rdx;
		uint64_t rcx;
		uint64_t rbx;
		uint64_t rax;
		uint64_t int_number;
		uint64_t errorcode;
		uint64_t rip;
		uint64_t cs;
		uint64_t rflags;
		uint64_t rsp;
		uint64_t ss;
		
		inline uint64_t &param0(){return rax;}
		inline uint64_t &param1(){return rdi;}
		inline uint64_t &param2(){return rsi;}
		inline uint64_t &param3(){return rdx;}
		inline uint64_t &param4(){return r8;}
		inline uint64_t &param5(){return r9;}
		inline uint64_t &param6(){return r10;}
		inline bool is_kernel_space(){return (cs == 0x08);}
		inline size_t syscall_number(){return rax;}
	};
	
	struct thread_state
	{
		uint64_t rax;		// 0x00
		uint64_t rbx;		// 0x08
		uint64_t rcx;		// 0x10
		uint64_t rdx;		// 0x18
		uint64_t rdi;		// 0x20
		uint64_t rsi;		// 0x28
		uint64_t rbp;		// 0x30
		uint64_t r8;		// 0x38
		uint64_t r9;		// 0x40
		uint64_t r10;		// 0x48
		uint64_t r11;		// 0x50
		uint64_t r12;		// 0x58
		uint64_t r13;		// 0x60
		uint64_t r14;		// 0x68
		uint64_t r15;		// 0x70
		uint64_t rsp;		// 0x78
		uint64_t rip;		// 0x80
		uint64_t rflags;	// 0x88
	};
	
	extern "C"
	{
		/*! interrupt handler
		 *\param[in] stackframe pointer to the stackframe */
		void interrupt(interrupt_stackframe &stackframe);
	}
	
	struct task_state_segment
	{
		uint32_t res0;
		uint64_t rsp0;
		uint64_t rsp1;
		uint64_t rsp2;
		uint64_t res1;
		uint64_t ist[7];
		uint64_t res2;
		uint16_t res3;
		uint16_t ioPermBitmap;
	}__attribute((packed));
}

/*@}*/
/*@}*/

#endif
