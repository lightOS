/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_64_ARCH_ELF_HPP
#define LIGHTOS_KERNEL_X86_64_ARCH_ELF_HPP

/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup x86-64 x86-64 */
/*@{*/

#include <cstddef>
#include <cstdint>

namespace kernel
{
	/*! The elf header on x86-64 */
	struct elf_header
	{
		uint8_t id[16];
		uint16_t type;
		uint16_t machine;
		uint32_t version;
			uint64_t entry;
			uint64_t programHeader;
			uint64_t sectionHeader;
		uint32_t flags;
		uint16_t headerSize;
		uint16_t programHeaderSize;
		uint16_t programHeaderCount;
		uint16_t sectionHeaderSize;
		uint16_t sectionHeaderCount;
		uint16_t iStringTable;
	}__attribute__((packed));
	
	/*! The elf programm header for x86-64 */
	struct elf_program_header
	{
		uint32_t type;
			uint32_t flags;
			uint64_t offset;
			uint64_t vAddress;
			uint64_t pAddress;
			uint64_t fileSize;
			uint64_t memorySize;
			uint64_t align;
	}__attribute__((packed));
	
	/*! Entry of the DYNAMIC segment for x86-64*/
	struct elf_dynamic_entry
	{
		uint64_t type;
		union
		{
			uint64_t value;
			uint64_t address;
		};
	}__attribute__((packed));
	
	/*! An elf symbol for x86-64 */
	struct elf_symbol
	{
		uint32_t name;
		uint8_t info;
		uint8_t other;
		uint16_t shndx;
			uint64_t value;
			uint64_t size;
	}__attribute__((packed));
	
	#define ELF_RELOCATION_SYM(i)((i)->info >> 32)
	#define ELF_RELOCATION_TYPE(i)((i)->info & 0xFFFFFFFF)
	
	/*! An elf relocation entry for x86-64 */
	struct elf_relocation
	{
		uint64_t address;
		uint64_t info;
	}__attribute__((packed));
	
	/*! An alternative elf relocation entry for x86-64 */
	struct elf_relocation_a
	{
		uint64_t address;
		uint64_t info;
		uint64_t addend;
	}__attribute__((packed));
	
	/*! The x86-64 relocation types */
	enum
	{
		R_X86_64_64				= 0x01,
		R_X86_64_PC32			= 0x02,
		R_X86_64_COPY			= 0x05,
		R_X86_64_GLOB_DAT		= 0x06,
		R_X86_64_JUMP_SLOT		= 0x07,
		R_X86_64_RELATIVE		= 0x08,
		R_X86_64_32				= 0x0A
	};
}

/*@}*/
/*@}*/

#endif
