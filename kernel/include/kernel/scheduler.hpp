/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_SCHEDULER_HPP
#define LIGHTOS_KERNEL_SCHEDULER_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <vector>
#include <iterator>
#include <kernel/arch/arch.hpp>
#include <kernel/process.hpp>
#include <kernel/spinlock.hpp>

namespace kernel
{
	/*! The scheduler class */
	class scheduler
	{
		public:
			/*! Add a thread to the scheduler's running queue
			 *\param[in] Thread the thread */
			static void add(thread *Thread);
			/*! Remove a thread from the scheduler's running queue
			 *\param[in] Thread the thread */
			static void remove(thread *Thread);
			/*! Get the on this processor currently executed process */
			static process *get_process();
			/*! Get the on this processor currently executed thread */
			static thread *get_thread();
			/*! Schedule! */
			static void schedule(interrupt_stackframe *stackframe);
		private:
			/*! List with non-blocking threads */
			static std::vector<thread*> mList;
			/*! List with threads that are currently running on a processor */
			static std::vector<thread*> mExecList;
			#ifdef _LIGHTOS_SMP
				/*! Spinlock to protect the scheduler in a multiprocessor environment */
				static lightOS::spinlock mLock;
			#endif
	};
}

/*@}*/

#endif
