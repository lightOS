/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_VIRTUAL_MEMORY_HPP
#define LIGHTOS_KERNEL_VIRTUAL_MEMORY_HPP


/*
 * Include the arch specific implementation of this header
 */
#include <kernel/arch/virtual_memory.hpp>


/*! \addtogroup kernel kernel */
/*@{*/

namespace kernel
{
  typedef ARCH_NAMESPACE::context_handle_t       context_handle_t;

  struct virtual_memory
  {
    static const uintptr_t page_size                = ARCH_NAMESPACE::virtual_memory::page_size;
    static const uintptr_t page_mask                = ARCH_NAMESPACE::virtual_memory::page_mask;

    static inline uintptr_t page_stack_begin();
    static inline uintptr_t kernel_memory_region_begin();
    static inline uintptr_t kernel_log_begin();
    static inline uintptr_t kernel_log_end();
    static inline uintptr_t kernel_binary_begin();
    static inline uintptr_t kernel_binary_end();
    static inline uintptr_t kernel_binary_physical_begin();
    static inline uintptr_t kernel_binary_physical_end();
    static inline uintptr_t kernel_log_tmp_first();
    static inline uintptr_t kernel_log_tmp_last();
    static inline context_handle_t kernel_context_handle();

    template<typename T>
    static inline T* page_stack_begin();
    template<typename T>
    static inline T* kernel_memory_region_begin();
    template<typename T>
    static inline T* kernel_log_begin();
    template<typename T>
    static inline T* kernel_log_end();
    template<typename T>
    static inline T* kernel_binary_begin();
    template<typename T>
    static inline T* kernel_binary_end();
    template<typename T>
    static inline T* kernel_binary_physical_begin();
    template<typename T>
    static inline T* kernel_binary_physical_end();
    template<typename T>
    static inline T* kernel_log_tmp_first();
    template<typename T>
    static inline T* kernel_log_tmp_last();
  };

  template<typename T>
  struct page_size_in
  {
    static const size_t value = virtual_memory::page_size / sizeof(T);
  };


  template<typename T>
  T* adjust_pointer(T* p, uintptr_t diff)
  {
    return reinterpret_cast<T*>(reinterpret_cast<uintptr_t>(p) + diff);
  }

  template<typename T2,
           typename T1>
  T2* adjust_pointer_cast(T1* p, uintptr_t diff)
  {
    return reinterpret_cast<T2*>(reinterpret_cast<uintptr_t>(p) + diff);
  }

  template<typename T>
  size_t pointer_difference(T* p, T* q)
  {
    return reinterpret_cast<uintptr_t>(p) - reinterpret_cast<uintptr_t>(q);
  }

  /*
   *  The implementation
   */

  uintptr_t virtual_memory::page_stack_begin()
  {
    return ARCH_NAMESPACE::virtual_memory::page_stack_begin;
  }

  uintptr_t virtual_memory::kernel_memory_region_begin()
  {
    return ARCH_NAMESPACE::virtual_memory::kernel_memory_region_begin;
  }

  uintptr_t virtual_memory::kernel_log_begin()
  {
    return ARCH_NAMESPACE::virtual_memory::kernel_log_begin;
  }

  uintptr_t virtual_memory::kernel_log_end()
  {
    return ARCH_NAMESPACE::virtual_memory::kernel_log_end;
  }

  uintptr_t virtual_memory::kernel_binary_begin()
  {
    return ARCH_NAMESPACE::virtual_memory::kernel_binary_begin();
  }

  uintptr_t virtual_memory::kernel_binary_end()
  {
    return ARCH_NAMESPACE::virtual_memory::kernel_binary_end();
  }

  uintptr_t virtual_memory::kernel_binary_physical_begin()
  {
    return ARCH_NAMESPACE::virtual_memory::kernel_binary_physical_begin();
  }

  uintptr_t virtual_memory::kernel_binary_physical_end()
  {
    return ARCH_NAMESPACE::virtual_memory::kernel_binary_physical_end();
  }

  uintptr_t virtual_memory::kernel_log_tmp_first()
  {
    return ARCH_NAMESPACE::virtual_memory::kernel_log_tmp_first();
  }

  uintptr_t virtual_memory::kernel_log_tmp_last()
  {
    return ARCH_NAMESPACE::virtual_memory::kernel_log_tmp_last();
  }

  context_handle_t virtual_memory::kernel_context_handle()
  {
    return ARCH_NAMESPACE::virtual_memory::kernel_context_handle();
  }

  template<typename T>
  T* virtual_memory::page_stack_begin()
  {
    return reinterpret_cast<T*>(virtual_memory::page_stack_begin());
  }

  template<typename T>
  T* virtual_memory::kernel_memory_region_begin()
  {
    return reinterpret_cast<T*>(virtual_memory::kernel_memory_region_begin());
  }

  template<typename T>
  T* virtual_memory::kernel_log_begin()
  {
    return reinterpret_cast<T*>(virtual_memory::kernel_log_begin());
  }

  template<typename T>
  T* virtual_memory::kernel_log_end()
  {
    return reinterpret_cast<T*>(virtual_memory::kernel_log_end());
  }

  template<typename T>
  T* virtual_memory::kernel_binary_begin()
  {
    return reinterpret_cast<T*>(virtual_memory::kernel_binary_begin());
  }

  template<typename T>
  T* virtual_memory::kernel_binary_end()
  {
    return reinterpret_cast<T*>(virtual_memory::kernel_binary_end());
  }

  template<typename T>
  T* virtual_memory::kernel_binary_physical_begin()
  {
    return reinterpret_cast<T*>(virtual_memory::kernel_binary_physical_begin());
  }

  template<typename T>
  T* virtual_memory::kernel_binary_physical_end()
  {
    return reinterpret_cast<T*>(virtual_memory::kernel_binary_physical_end());
  }

  template<typename T>
  T* virtual_memory::kernel_log_tmp_first()
  {
    return reinterpret_cast<T*>(virtual_memory::kernel_log_tmp_first());
  }

  template<typename T>
  T* virtual_memory::kernel_log_tmp_last()
  {
    return reinterpret_cast<T*>(virtual_memory::kernel_log_tmp_last());
  }
}

/*@}*/

#endif
