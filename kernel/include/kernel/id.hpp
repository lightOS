/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_ID_HPP
#define LIGHTOS_KERNEL_ID_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <cstddef>
#include <libkernel/type.hpp>

namespace kernel
{
	/*! Typedef an id type */
	typedef size_t id;
	
	/*! Typedef processorId */
	typedef id processorId;
	
	/*! Generate a new thread/process id number */
	id generateId();
	
	/*! Generate a new port id */
	libkernel::port_id_t generatePortId();
	
	/*! Generate a new shared memory id */
	libkernel::shared_memory_id_t generateSharedMemoryId();
}

/*@}*/

#endif
