/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_SPINLOCK_HPP
#define LIGHTOS_KERNEL_SPINLOCK_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <lightOS/atom.hpp>
#include <lightOS/spinlock.hpp>
#include <kernel/id.hpp>

namespace kernel
{
  class reentrant_spinlock
  {
    public:
      inline reentrant_spinlock(bool locked=false)
        : mAtom(locked ? 0 : 1){}
      void lock();
      void unlock();

    protected:
      lightOS::atom mAtom;
      processorId mProcessorId;
  };
}

#ifdef _LIGHTOS_SMP
  #define SPINLOCK_DECL(x)             lightOS::spinlock x;
  #define SPINLOCK_DECL_MUTABLE(x)     mutable lightOS::spinlock x;
  #define SPINLOCK_DECL_STATIC(x)      static lightOS::spinlock x;
  #define SPINLOCK_DEF(x)              lightOS::spinlock x;
  #define SPINLOCK_DEF_STATIC(x)       lightOS::spinlock x;
#else
  #define SPINLOCK_DECL(x)
  #define SPINLOCK_DECL_MUTABLE(x)
  #define SPINLOCK_DECL_STATIC(x)
  #define SPINLOCK_DEF(x)
  #define SPINLOCK_DEF_STATIC(x)
#endif

/*@}*/

#endif
