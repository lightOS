/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_SYSCALL_HPP
#define LIGHTOS_KERNEL_SYSCALL_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <kernel/arch/arch.hpp>

#define MAX_SYSCALL 56

namespace kernel
{
	typedef void (*systemcall)(interrupt_stackframe &);
	extern systemcall systemcalls[];
	
	namespace syscall
	{
		void get_kernel_version(interrupt_stackframe &frame);
		void heap_allocate(interrupt_stackframe &frame);
		void get_memory_info(interrupt_stackframe &frame);
		void get_commandline(interrupt_stackframe &frame);
		void get_time(interrupt_stackframe &frame);
		void get_date(interrupt_stackframe &frame);
		void create_port(interrupt_stackframe &frame);
		void destroy_port(interrupt_stackframe &frame);
		void peek_message(interrupt_stackframe &frame);
		void get_message(interrupt_stackframe &frame);
		void wait_message(interrupt_stackframe &frame);
		void send_message(interrupt_stackframe &frame);
		void port_info(interrupt_stackframe &frame);
		void standard_port(interrupt_stackframe &frame);
		void transfer_port(interrupt_stackframe &frame);
		void add_wait_message(interrupt_stackframe &frame);
		void create_thread(interrupt_stackframe &frame);
		void suspend_thread(interrupt_stackframe &frame);
		void resume_thread(interrupt_stackframe &frame);
		void destroy_thread(interrupt_stackframe &frame);
		void destroy_process(interrupt_stackframe &frame);
		void get_pid(interrupt_stackframe &frame);
		void get_tid(interrupt_stackframe &frame);
		void get_process_count(interrupt_stackframe &frame);
		void get_process_list(interrupt_stackframe &frame);
		void get_process_info(interrupt_stackframe &frame);
		void register_event(interrupt_stackframe &frame);
		void acknoledge_event(interrupt_stackframe &frame);
		void unregister_event(interrupt_stackframe &frame);
		void get_tick_count(interrupt_stackframe &frame);
		void create_shared_memory(interrupt_stackframe &frame);
		void destroy_shared_memory(interrupt_stackframe &frame);
		void range_allocator_count(interrupt_stackframe &frame);
		void enum_range_allocator(interrupt_stackframe &frame);
		//
		//
		//
		void create_specific_port(interrupt_stackframe &frame);
		void get_kernel_log(interrupt_stackframe &frame);
		void get_vbe_version(interrupt_stackframe &frame);
		void get_vbe_mode_count(interrupt_stackframe &frame);
		void get_vbe_mode(interrupt_stackframe &frame);
		void set_vbe_mode(interrupt_stackframe &frame);
		void request_io(interrupt_stackframe &frame);
		void dma(interrupt_stackframe &frame);
		void execute(interrupt_stackframe &frame);
		void get_physical_address(interrupt_stackframe &frame);
		void allocate_region(interrupt_stackframe &frame);
		void free_region(interrupt_stackframe &frame);
		void get_system_configuration(interrupt_stackframe &frame);
		void get_library_count(interrupt_stackframe &frame);
		void get_library_info(interrupt_stackframe &frame);
		void signal_handler(interrupt_stackframe &frame);
		void signal(interrupt_stackframe &frame);
		void signal_end(interrupt_stackframe &frame);

    void get_log_entry(interrupt_stackframe& frame);
	}
}

/*@}*/

#endif
