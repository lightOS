/*
lightOS kernel
Copyright (C) 2008-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_IO_PORT_MANAGER_HPP
#define LIGHTOS_KERNEL_IO_PORT_MANAGER_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <cstddef>
#include <cstdint>
#include <vector>
#include <utility>
#include <kernel/spinlock.hpp>
#include <kernel/io_port.hpp>
#include <kernel/utils/macros.hpp>

namespace kernel
{
  /*! The I/O port manager */
  class io_port_manager
  {
    SINGLETON(io_port_manager);

    public:
      /*! Initialize the I/O port manager */
      void init();

      // TODO: Enumerating I/O port allocations?

      /*! Allocate an I/O port range for usage within the kernel
       *\param[in,out] ioport reference to the I/O port interface
       *\param[in] port the first I/O port
       *\param[in] size the number of I/O ports
       *\return  */
      bool allocate(io_port& ioport,
                    libarch::ioport_t port,
                    size_t size);
      /** Allocate an I/O port range for usage within a process
       *\param[in] pid the process id of the calling process
       *\param[in] port the first I/O port
       *\param[in] size the number of I/O ports
       *\return  */
      bool allocate(libkernel::process_id_t pid,
                    libarch::ioport_t port,
                    size_t size);

      /*! Free an I/O port range
       *\param[in,out] reference to the I/O port interface */
      void free(io_port& port);
      /*! Free an I/O port range
       *\param[in] pid the process id of the calling process
       *\param[in] port the first I/O port */
      void free(libkernel::process_id_t pid,
                libarch::ioport_t port);

    private:
      /*! List of available I/O port ranges */
      std::vector<io_port*> m_list;

      /*! Spinlock to protect the io_port_manager in a multiprocessor environment
       *\todo spinlock
      SPINLOCK_DECL(m_lock) */
  };
}

/*@}*/

#endif
