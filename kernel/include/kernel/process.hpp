/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_PROCESS_HPP
#define LIGHTOS_KERNEL_PROCESS_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <vector>
#include <string>
#include <cstdint>
#include <iterator>
#include <algorithm>
#include <utility>
#include <kernel/id.hpp>
#include <kernel/thread.hpp>
#include <kernel/spinlock.hpp>

namespace kernel
{
	/*!\todo doc */
	class process
	{
		friend class thread;
		friend class context;
		public:
			/*! Create a new process
			 *\param[in] Context the context of the new process
			 *\param[in] pages list of allocated physical pages
			 *\param[in] name the process name
			 *\param[in] entryPoint the process's entry point
			 *\param[in] flags process creation flags
			 *\return process's id if successfull, 0 otherwise */
			static libkernel::process_id_t create(context &Context,
									std::vector<void*> pages,
									const std::string &name,
									uintptr_t entryPoint,
									size_t flags);
			/*! Destroy a process
			 *\param[in] pid the process's Id
			 *\return true, if successfull, false otherwise */
			static bool destroy(libkernel::process_id_t pid);
			/*! Get the number of processes
			 *\return the number of processes */
			static size_t size();
			/*! Get process by id 
			 *\param[in] id the process id
			 *\return pointer to the process class if successfull, 0 otherwise */
			static process *get_process(libkernel::process_id_t id);
			/*! Get the process by it's index
			 *\param[in] index the index
			 *\return pointer to the process class if successfull, 0 otherwise */
			static process *get_process_by_index(size_t index);
			/*! Get thread by id
			 *\param[in] id the thread id
			 *\return pointer to the thread class if successfull, 0 otherwise */
			static thread *get_thread(libkernel::thread_id_t id);
			
			// TODO
			/*! Is a server? */
			bool server() const{return mIsServer;}
			/*! Get the process id */
			libkernel::process_id_t getId() const{return mPid;}
			/*! Get the process's context */
			inline kernel::context &getContext(){return mContext;}
			/*! Get the process's name */
			inline std::string getName() const
			{
				std::string::const_iterator i = std::find<std::string::iterator, char>(mCmdLine.begin(), mCmdLine.end(), ' ');
				return std::string(mCmdLine.begin(), i);
			}
			/*! Get the process's command line */
			inline const std::string &getCommandLine() const{return mCmdLine;}
			/*! Create new thread */
			libkernel::thread_id_t createThread(	uintptr_t entryPoint,
									size_t param1,
									size_t param2,
									size_t flags,
									libkernel::thread_id_t tid=generateId());
			/*! Destroy an existing thread */
			bool destroyThread(libkernel::thread_id_t id);
			/*! Get the number of threads */
			inline size_t threads() const{return mList.size();}
			/*! Allocate some pages and map them into this context's heap */
			void *heapAlloc(size_t count);
			/*! Create a new memory region */
			void *create_memory_region(	size_t size,
										size_t flags,
										void *physical);
			/*! Free an existing memory region */
			void free_memory_region(void *address);
			
			static void registerEvent(	libkernel::port_id_t port,
										libkernel::event type,
										size_t param1,
										size_t param2);
			static void unregisterEvent(libkernel::port_id_t port,
										libkernel::event type);
			
			size_t used_page_count() const;
			void *signal_handler() const{return mSignalHandler;}
			void signal_handler(void *s){mSignalHandler = s;}
			bool signal(size_t sig);
			void signal_end();
			
			bool set_standard_port(libkernel::port_id_t stdPort, libkernel::port_id_t Port, libkernel::port_id_t fsPort);
			std::pair<libkernel::port_id_t,libkernel::port_id_t> get_standard_port(libkernel::port_id_t stdPort);
			
			void *allocate_shared_memory_region();
			void free_shared_memory_region(void *address);
		protected:
			/*! The constructor
			*\param[in] s the process's commandline
			*\param[in] cntxt the process's context */
			process(const std::string &cmdline,
					context &Context,
					std::vector<void*> pages,
					bool isServer);
			void destroyThread(thread *Thread);
			/*! The destructor */
			~process();
		private:
			/*! Is server? */
			bool mIsServer;
			/*! The thread list */
			std::vector<thread*> mList;
			/*! The process's command line */
			std::string mCmdLine;
			/*! The process's context */
			kernel::context mContext;
			/*! The process id */
			libkernel::process_id_t mPid;
			/*! The latest thread stack */
			void *mLatestThreadStack;
			/*! Allocated pages */
			std::vector<void*> mPages;
			/*! Free thread stacks */
			std::vector<std::pair<void*,size_t> > mFreeThreadStacks;
			/*! The latest shared memory address */
			void *mLatestSharedMemory;
			/*! List of free shared memory addresses */
			std::vector<void*> mFreeSharedMemory;
			/*! One memory region */
			struct region
			{
				void *vAddress;
				size_t size;
				bool continuous;
				bool physical;
				std::vector<void*> pages;
			};
			/*! List of memory regions */
			std::vector<region> mRegions;
			/*! Last memory region's address */
			void *mLatestMemoryRegion;
			
			
			/*! Broadcast an event */
			static void broadcastEvent(	libkernel::event type,
										size_t param1,
										size_t param2);
			/*! Event */
			struct event
			{
				libkernel::port_id_t port;
				libkernel::event type;
				size_t param1;
				size_t param2;
			};
			/*! List of registered events */
			static std::vector<event> mEvents;
			
			void *mSignalHandler;
			thread_state mSignalState;
			
			/*! Standard ports */
			libkernel::port_id_t mStandardPorts[5];
			libkernel::port_id_t mStandardFsPorts[5];
			
			
			
			/*! Typedef the iterator type */
			typedef std::vector<process*>::iterator process_iterator;
			/*! Typedef the const_iterator type */
			typedef std::vector<process*>::const_iterator const_process_iterator;
			/*! The process list */
			static std::vector<process*> mProcessList;
			#ifdef _LIGHTOS_SMP
				/*! Spinlock to protect the process manager in a multiprocessor environment */
				static lightOS::spinlock mListLock;
			#endif
	};
}

/*@}*/

#endif
