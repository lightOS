/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_UTILS_MACROS_HPP
#define LIGHTOS_KERNEL_UTILS_MACROS_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <libkernel/compiler.hpp>

#define SINGLETON(x)                   public: \
                                         inline static x& instance() ALWAYS_INLINE \
                                           {return m_instance;} \
                                       private: \
                                         static x m_instance; \
                                         x() INIT_ONLY; \
                                         x(const x&) = delete; \
                                         x& operator = (const x&) = delete; \
                                         ~x() INIT_ONLY
#define SINGLETON_INSTANCE(x)          x x::m_instance

#define NON_DEFAULT_CONTRUCTABLE(x)    x() = delete
#define NON_COPY_CONSTRUCTABLE(x)      x(const x&) = delete
#define NON_ASSIGNABLE(x)              x& operator = (const x&) = delete


#define CONCEPT_CHECK(concept)         extern _##concept##_concept<concept> _##concept##_concept_check

/** Pack initialisation functions into a special section that could be freed after
 *  the kernel initialisation is finished */
#define INIT_ONLY                      __attribute__((__section__(".init.text")))
#define INIT_ONLY_DATA                 __attribute__((__section__(".init.data")))

/*@}*/

#endif
