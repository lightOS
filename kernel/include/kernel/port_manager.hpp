/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_PORTMANAGER_HPP
#define LIGHTOS_KERNEL_PORTMANAGER_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <list>
#include <iterator>
#include <kernel/id.hpp>
#include <kernel/thread.hpp>
#include <kernel/spinlock.hpp>
#include <kernel/utils/macros.hpp>
#include <libkernel/type.hpp>
#include <libkernel/message.hpp>

// TODO: Special allocator for messages and ports

namespace kernel
{
  class port_manager
  {
    SINGLETON(port_manager);

    public:
      /*! Create a new port
       *\param[in] Process the process the port should belong to
       *\return the Id of the new port */
      libkernel::port_id_t create(process &Process);
      /*! Create a port with a specific id
       *\param[in] Process the process the port should belong to
       *\param[in] Id the Id the port should have
       *\return true, if the port was successfully created, false otherwise */
      bool create(process &Process,
            libkernel::port_id_t Id);
      /*! Send a message to a port
       *\param[in] port the port of the sending process
       *\param[in] msg the message to be send
       *\param[in] Process the sending process, 0 if the kernel sends the message
       *\return true, if successfully send, false otherwise */
      bool send(  libkernel::port_id_t port,
            const libkernel::message_t &msg,
            const process *Process = 0);
      /*! Peek a message from a port
       *\param[in] port the port
       *\param[in,out] msg the message, if any
       *\return true, if a message was there, false otherwise */
      bool peek(  libkernel::port_id_t port,
            libkernel::message_t &msg,
            const process &Process);
      /*! Get the next message or put the thread to sleep if none available
       *\param[in] port the port to look/wait for messages
       *\param[in,out] msg the received message
       *\param[in] Thread the thread
       *\return false, if thread is put to sleep, true otherwise */
      bool get( libkernel::port_id_t port,
            libkernel::message_t &msg,
            thread &Thread);
      /*! Wait for a specific message
       *\param[in,out] port the port the message was received (if at all)
       *\param[in,out] msg the received message (if any)
       *\param[in] Thread the thread
       *\return true, if the thread was put to sleep, false otherwise */
      bool wait(  libkernel::port_id_t &port,
            libkernel::message_t &msg,
            thread &Thread);
      /*! Add a waiter to a port
       *\param[in] port the port
       *\param[in] msg the message the waiter is waiting for
       *\param[in] Thread the waiter
       *\return true, if successfull, false otherwise */
      bool add_wait(  libkernel::port_id_t port,
              const libkernel::message_t &msg,
              thread &Thread);
      /*! Transfer a port to another process
       *\param[in] port the port that should be transfered
       *\param[in] Process the owning process
       *\param[in] receiver processId of the receiving process
       *\return true, if successfull, false otherwise */
      bool transfer(  libkernel::port_id_t port,
              const process &Process,
              libkernel::process_id_t receiver);

      /*! Get processId by port id
       *\param[in] id the libkernel::portid_t
       *\return the processId if successfull, 0 otherwise */
      libkernel::process_id_t get_processId(libkernel::port_id_t id);
      /*! Get threadId by port id of the waiting thread
       *\param[in] id the libkernel::portid_t
       *\return the threadId if successfull, 0 otherwise */
      libkernel::thread_id_t get_waiting_threadId(libkernel::port_id_t id);
      /*! Destroy a port
       *\param[in] port the Id of the port
       *\return true, if successfully destroyed, false otherwise */
      bool destroy(libkernel::port_id_t port);
      /*! Detroy all ports of a certain process
       *\param[in] Process pointer to the process */
      void destroy_process_ports(const process &Process);

    private:
      /*! Structure of one port */
      class port
      {
        public:
          /*! The constructor */
          inline port()
            : waiter(0){}
          /*! The constructor
           *\param[in] PortId the id of the port
           *\param[in] Owner the owning process */
          inline port(libkernel::port_id_t PortId, process *Owner)
            : id(PortId), owner(Owner), waiter(0){}
          /*! The destructor */
          inline ~port(){}

          /*! The libkernel::portid_t */
          libkernel::port_id_t id;
          /*! The owning process */
          process *owner;
          /*! The waiting thread */
          thread *waiter;
          /*! The message the thread is waiting for on this port */
          libkernel::message_t waiter_msg;
          /*! The list of messages */
          std::vector<libkernel::message_t*> msg;
      };

      /*! Get port by it's id
       *\param[in] id the Id of the port
       *\note The multiprocessor lock MUST be acquired BEFORE this function is called and is NOT released by
       *      this function
       *\return a pointer to the port, if successfull, 0 otherwise */
      port *get_port_by_id(libkernel::port_id_t id);
      /*! Destroy a port
       *\param[in] Port pointer to the port
       *\note The multiprocessor lock MUST be acquired BEFORE this function is called and is NOT released by
       *      this function */
      void destroy(port *Port);
      /*! Remove the thread from all ports it is waiting on
       *\param[in] Thread the thread to remove
       *\note The multiprocessor lock MUST be acquired BEFORE this function is called and is NOT released by
       *      this function */
      void remove_thread_wait(const thread &Thread);
      /*! Match two messages
       *\param[in] port the port that is used (instead of msg1.port)
       *\param[in] msg1 the first message
       *\param[in] msg2 the second message
       *\return true, if they match, false otherwise */
      bool match( libkernel::port_id_t port,
            const libkernel::message_t &msg1,
            const libkernel::message_t &msg2);

      /*! The list of ports */
      std::vector<port*> list;

      #ifdef _LIGHTOS_SMP
        /*! Spinlock to protect the port_manager in a multiprocessor environment */
        lightOS::spinlock mLock;
      #endif
  };
}

/*@}*/

#endif
