/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_PROCESSOR_HPP
#define LIGHTOS_KERNEL_PROCESSOR_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <cstddef>
#include <cstdint>
#include <vector>
#include <kernel/arch/arch.hpp>
#include <kernel/id.hpp>
#include <kernel/spinlock.hpp>
#include <kernel/x86_shared/apic.hpp>

namespace kernel
{
	class thread;
	
	/*! The machine specific registers */
	namespace msr
	{
		enum
		{
			/*! IA32_APIC_BASE */
			apic_base			= 0x1B
		};
	}
	
	/*! The processor class */
	class processor
	{
		friend class gdt;
		friend class smp;
		
		public:
			/*! Initialize
			 *\note Called once in kernel.cpp
			 *\note Calls initialize_this for this cpu */
			static void initialize();
			/*! Initialize THIS processor
			 *\note Called once for every processor */
			static void initialize_this();
			
			/*! Get a processor object by it's Id
			 *\param[in] Id the processor's Id
			 *\return pointer to the processor object or 0 */
			static processor *get_by_id(processorId Id);
			/*! Get a processor object by it's index
			 *\param[in] index the index
			 *\note Only access by the gdt
			 *\return pointer to the processor object or 0 */
			static processor *get_by_index(size_t i);
			/*! Get a this processor's object
			 *\return pointer to the processor object or 0 */
			inline static processor *get(){return processor::get_by_id(processor::id());}
			/*! Get the number of processors
			 *\return the number of processors */
			static size_t count();
			
			/*! Get the cpu Id
			 *\return the Id of this cpu */
			static processorId id();
			/*! Halt the processors */
			static void halt();
			
			/*! Execute a cpuid instruction
			 *\param[in] eax eax before the instruction
			 *\param[in] ecx ecx before the instruction
			 *\param[out] out_eax reference to eax after the instruction
			 *\param[out] out_ebx reference to ebx after the instruction
			 *\param[out] out_ecx reference to ecx after the instruction
			 *\param[out] out_edx reference to edx after the instruction */
			static void cpuid(	uint32_t eax,
								uint32_t ecx,
								uint32_t &out_eax,
								uint32_t &out_ebx,
								uint32_t &out_ecx,
								uint32_t &out_edx);
			/*! Read a machine specific register
			 *\param[in] index the index
			 *\return the value read */
			static uint64_t read_msr(size_t index);
			/*! Write a machine specific register
			 *\param[in] index the index
			 *\param[in] value the value to write */
			static void write_msr(size_t index, uint64_t value);
			/*! Return the address of a page fault
			 *\return the address of the page fault */
			static void *get_page_fault();
			/*! Handle an FPU exception */
			static bool handle_fpu_exception(interrupt_stackframe &stackframe);
			/*! Dump the interrupt frame content
			 *\param[in] stackframe the interrupt stackframe */
			static void dump(interrupt_stackframe &stackframe, bool stacktrace);
			/*! Get the current context
			 *\return physical address of the current context */
			static void *context();
			/*! Set the context
			 *\param[in] physical address of the current context */
			static void context(void *ctx);
			/*! Does this cpu have a local APIC (Advanced Programmable Interrupt Controller)?
			 *\return true, if a local APIC is present, false otherwise */
			static bool has_local_apic();
			/*! Does this cpu have the Execution-Disable/No-Execution Bit?
			 *\return true, if this bit is usable, false otherwise */
			static bool nx_bit();
			/*! Read the TSC (=Timestamp counter)
			 *\return value of the Timestamp counter */
			static uint64_t read_tsc();
			
			#ifdef _LIGHTOS_SMP
				/*! Delay for a number of milliseconds
				*\param[in] ms the number of milliseconds */
				static void msdelay(size_t ms);
				
				/*! Return this processor's local APIC instance */
				inline static local_apic &get_local_apic(){return *get()->mLocalApic;}
			#endif
			
			void set_tss(uint16_t selector, task_state_segment *tss);
			
			static void load_taskregister(uint16_t selector = 0);
			static uint16_t get_taskregister(){return processor::get()->mTSSSelector;}
			
			static void allocate_fpu_state(thread *Thread);
			static void free_fpu_state(thread *Thread);
			
			thread *get_thread() const{return mThread;}
			void set_thread(thread *Thread){mThread = Thread;}
		
		private:
			/*! The constructor */
			processor();
			/*! The copy-constructor
			 *\note NO implementation provided */
			processor(const processor &);
			/*! The = operator
			 *\note NO implementation provided */
			processor &operator = (const processor &);
			/*! The destructor */
			inline ~processor(){}
			
			#ifdef _LIGHTOS_SMP
				/*! Calibrates the time-stamp counter based msdelay() and the local APIC */
				static void calibrate();
			#endif
			
			/*! This processor's TSS selector */
			uint16_t mTSSSelector;
			/*! Pointer to this processor's TSS */
			task_state_segment *mTSS;
			/*! Pointer to this processor's kernel stack */
			uintptr_t mStack;
			/*! The currently executed thread or 0 if none */
			thread *mThread;
			
			#ifdef _LIGHTOS_SMP
				/*! The local APIC instance */
				local_apic *mLocalApic;
				/*! Timestamp count change within 1 second */
				static uint64_t mTscDelta;
			#endif
			
			// TODO: per cpu!!!
			static libkernel::thread_id_t lastFpuThread;
			static bool mFXSAVE;
			
			#ifdef _LIGHTOS_SMP
				/*! Spinlock to protect the processors in a multiprocessor environment */
				static lightOS::spinlock mLock;
			#endif
			/*! The list of processors */
			static std::vector<processor*> mProcessor;
	};
}

/*@}*/

#endif
