/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_SMP_HPP
#define LIGHTOS_KERNEL_SMP_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <cstddef>
#include <cstdint>
#include <utility>
#include <vector>
#include <kernel/spinlock.hpp>

namespace kernel
{
	#ifdef _LIGHTOS_SMP
	
	class smp
	{
		public:
			static bool compatible(){return mCompatible;}
			static void initialize();
			static void initialize2();
		
		private:
			static void ap_main();
			static void arch_ap_main();
			static void arch_pre_ap_bootup();
			static void arch_post_ap_bootup();
			
			struct mp_floating_pointer
			{
				uint32_t id;
				uint32_t config_table;
				uint8_t length;
				uint8_t revision;
				uint8_t checksum;
				uint8_t features[5];
			}__attribute__((packed));
			
			struct config_table_header
			{
				uint32_t id;
				uint16_t base_table_length;
				uint8_t revision;
				uint8_t checksum;
				char oem[8];
				char product[12];
				uint32_t oem_table;
				uint16_t oem_table_size;
				uint16_t entry_count;
				uint32_t local_apic_address;
				uint16_t extended_table_length;
				uint8_t extended_checksum;
				uint8_t reserved;
			}__attribute__((packed));
			
			struct processor
			{
				uint8_t entry_type;
				uint8_t lapic_id;
				uint8_t lapic_version;
				uint8_t flags;
				uint32_t signature;
				uint32_t feature_flags;
				uint32_t res0;
				uint32_t res1;
			}__attribute__((packed));
			
			struct bus
			{
				uint8_t entry_type;
				uint8_t busId;
				char name[6];
			}__attribute__((packed));
			
			struct io_apic
			{
				uint8_t entry_type;
				uint8_t id;
				uint8_t version;
				uint8_t flags;
				uint32_t address;
			}__attribute__((packed));
			
			struct io_interrupt_assignment
			{
				uint8_t entry_type;
				uint8_t type;
				uint16_t flags;
				uint8_t bus_id;
				uint8_t bus_irq;
				uint8_t io_apic_id;
				uint8_t io_apic_intn;
			}__attribute__((packed));
			
			struct local_interrupt_assignment
			{
				uint8_t entry_type;
				uint8_t type;
				uint16_t flags;
				uint8_t bus_id;
				uint8_t bus_irq;
				uint8_t lapic_id;
				uint8_t lapic_intn;
			}__attribute__((packed));
			
			static mp_floating_pointer *find(mp_floating_pointer *begin, size_t size);
			static bool checksum(const mp_floating_pointer *p);
			static bool checksum(const config_table_header *p);
			static size_t get_delivery_mode(size_t interrupt_type);
			static size_t get_polarity(size_t polarity, size_t busId);
			static size_t get_trigger_mode(size_t trigger_mode, size_t busId);
			
			struct bus_default
			{
				char name[6];
				size_t type;
				size_t trigger_mode;
				size_t polarity;
			};
			
			static bool mCompatible;
			static lightOS::spinlock mLock;
			static lightOS::spinlock mLock2;
			static uintptr_t mLastKernelStack;
			static bus_default bus_defaults[];
			static std::vector<std::pair<size_t, size_t> > mBusIdToBusDefaults;
	};
	#endif
}

/*@}*/

#endif
