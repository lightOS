/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_CONSOLE_HPP
#define LIGHTOS_KERNEL_CONSOLE_HPP


/*
 * Kernel platform includes
 */
#include <kernel/platform/console.hpp>


/*! \addtogroup kernel kernel */
/*@{*/

namespace kernel
{
  /*
   * Types lifted into the kernel namespace form the architecture namespace
   */

  /** Type for console window identifiers */
  typedef PLATFORM_NAMESPACE::console_id_t       console_id_t;
  /** Color type for the console */
  typedef PLATFORM_NAMESPACE::console_color_t    console_color_t;
  /** Console window type */
  typedef PLATFORM_NAMESPACE::console_window     console_window;
  /** Console window pointer type */
  typedef PLATFORM_NAMESPACE::console_window_ptr console_window_ptr;
  /** Console type */
  typedef PLATFORM_NAMESPACE::console            console;


  /*
   * Constants
   */

  /** The identifier of the kernel console */
  const console_id_t                          kernel_console_id     = PLATFORM_NAMESPACE::kernel_console_id;
  /** The width of a console window */
  const size_t                                console_window_width  = PLATFORM_NAMESPACE::console_window_width;
  /** The height of a console window */
  const size_t                                console_window_height = PLATFORM_NAMESPACE::console_window_height;

  // TODO: initialize the console class at some point later



  /*
   * Console window concept
   */

  template<typename T,
           void (T::*copy_from)(const void*)          = &T::copy_from,
           void (T::*copy_to)(void*) const            = &T::copy_to,
           console_id_t (T::*id)() const              = &T::id,
           size_t (T::*cursor1)() const               = &T::cursor,
           void (T::*cursor2)(size_t)                 = &T::cursor,
           void (T::*enable_cursor)(bool)             = &T::enable_cursor,
           bool (T::*is_cursor_enabled)() const       = &T::is_cursor_enabled,
           void (T::*character)(size_t, char)         = &T::character,
           void (T::*color)(size_t, console_color_t)  = &T::color>
  class _console_window_concept{};

  CONCEPT_CHECK(console_window);



  /*
   * Console concept
   */

  template<typename T,
           console& (*instance)()                           = &T::instance,
           console_window_ptr (T::*create)()                = &T::create,
           console_window_ptr (T::*get_by_id)(console_id_t) = &T::get,
           console_window_ptr (T::*get)()                   = &T::get,
           void (T::*switch_to)(console_window_ptr&)        = &T::switch_to>
  class _console_concept{};

  CONCEPT_CHECK(console);
}

/*@}*/

#endif
