/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_CONTEXT_HPP
#define LIGHTOS_KERNEL_CONTEXT_HPP


/*
 * Standard C includes
 */
#include <cstddef>

/*
 * Libarch includes
 */
#include <libarch/scoped_lock.hpp>

/*
 * Kernel includes
 */
#include <kernel/spinlock.hpp>
#include <kernel/utils/macros.hpp>
#include <kernel/virtual_memory.hpp>


/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup kernel_vmm virtual memory-management */
/*@{*/

namespace kernel
{
  // Forward declaration
  class process;


  /*! The context class */
  class context
  {
    friend class elf;

    NON_ASSIGNABLE(context);

    public:
      /*! The context constructor allocates a new context and will map the kernel into that context */
      context();

      /*! The copy-constructor */
      context(context& c);

      /*! The constructor
       *\param[in] handle pointer to the physical address of the page directory
       *\param[in] heapAddress address to the end of the heap */
      context(context_handle_t handle,
              void* heap);

      /*! The destructor deallocates the page tables */
      virtual ~context();

      /*! Calculate the machine specific flags needed for the page entry from the machine independent flags
       *\param[in] flags machine independent flags */
      size_t get_flags(size_t flags);

      /*!\todo document */
      inline void* physical_address(void* address);
      /*!\todo document */
      template<typename T>
      inline T* physical_address(T* address);
      /*!\todo document */
      size_t physical_address_flags(void* vaddress);

      /*! Unmap a page from this context
       *\param[in] vaddress virtual address of the beginning og the memory region
       *\return the physical address or 0 */
      void* unmap(void* vaddress);

      /*! Map some pages into this context
       *\param[in] paddress physical address of the beginning of the memory region
       *\param[in] vaddress virtual address of the beginning of the memory region
       *\param]in] pages the number of pages to map
       *\param[in] flags the machine independent flags for the mapping */
      void map(void* paddress,
               void* vaddress,
               size_t flags);

      /*! Map some pages into this context's heap
       *\param[in] paddress physical address of the beginning of the memory region
       *\param[in] pages the number of pages to map
       *\param[in] flags the machine independent flags for the mapping
       *\return pointer to the virtual address */
      void* heap_map(void* paddress,
                     size_t pages,
                     size_t flags);

      void* heap_map(size_t pages,
                     size_t flags,
                     process* Process);

      /*! Get the heap address
       *\return the address of the heap */
      inline void* heap() const;

      /*! Get the physical address of this context
       *\return the physical address of this context (the page directory or the page map Lvl4) */
      inline context_handle_t get_handle() const;

      /*! Copy a shared-memory region from one context to another
       *\param[in] vaddress_src virtual address of the beginning of the shared-memory region in the source process
       *\param[in] dest reference to the destination process context
       *\param[in] vaddress_dest virtual address of the beginning of the shared-memory region in the destination process
       *\param[in] flags machine independent flags */
      void copy_shared_memory(void* vaddress_src,
                              context& dest,
                              void* vaddress_dest,
                              size_t flags);

      /*! Free a shared-memory region, which includes deallocating the used space
       *\param[in] vaddress virtual address of the beginning of the shared-memory region
       *\param[in] size size of the shared-memory region */
      void free_shared_memory(void* vaddress,
                              size_t size);

      /*! Unmap a page table, which does not include deallocating the used space
       *\param[in] vaddress virtual address of the beginning of the shared-memory region */
      void unmap_shared_memory(void* vaddress);

      /*! Create the page tables for a specific address
       *\param[in] page the physical page to use as page table (if required)
       *\param[in] vaddress virtual address of the region the page tables should be created for
       *\param[in] flags machine independent flags for the mapping
       *\return true, if the page was used, false otherwise */
      bool create_page_tables(void* page,
                              void* vaddress,
                              size_t flags);

      bool copy_on_write_handler(void* vaddress,
                                 process& Process);

    protected:
      /*! Set the heap address */
      inline void heap(void* heap);

      /** \todo documentation
       *  \note does not lock */
      void* physical_address_nolock(void* vaddress);

      /*! Physical address of the page directory */
      context_handle_t m_handle;
      /*! Virtual address of the heap end */
      void*            m_heap;

      /*! Spinlock to protect the context in a multiprocessor environment */
      SPINLOCK_DECL_MUTABLE(m_lock)
  };

  /*! The kernel_context class */
  class kernel_context : protected context
  {
    friend class context;

    SINGLETON(kernel_context);

    public:
      using context::heap_map;
      using context::map;
      using context::get_handle;
      using context::create_page_tables;

      void *allocate_memory_region(size_t pages,
                                   size_t flags);
      void *map_memory_region(void *paddress,
                              size_t flags);
      void free_memory_region(void *address,
                              size_t pages);

    private:
      /*! Last kernel memory region */
      void *mLastKernelMemoryRegion;
  };



  /*
   * Part of the implementation
   */

  template<typename T>
  T* context::physical_address(T* address)
  {
    return reinterpret_cast<T*>(physical_address(reinterpret_cast<void*>(address)));
  }

  void* context::physical_address(void* address)
  {
    SCOPED_LOCK(m_lock, scope);
    return physical_address_nolock(address);
  }

  void* context::heap() const
  {
    SCOPED_LOCK(m_lock, scope);
    return m_heap;
  }

  void context::heap(void* heap)
  {
    SCOPED_LOCK(m_lock, scope);
    m_heap = heap;
  }

  context_handle_t context::get_handle() const
  {
    SCOPED_LOCK(m_lock, scope);
    return m_handle;
  }
}

/*@}*/
/*@}*/

#endif
