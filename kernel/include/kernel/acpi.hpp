/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_ACPI_HPP
#define LIGHTOS_KERNEL_ACPI_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <cstdint>

namespace kernel
{
	#ifdef ACPI
		class acpi
		{
			private:
				struct rsd_pointer
				{
					char signature[8];
					uint8_t checksum;
					char oemId[6];
					uint8_t revision;
					uint32_t rsdtAddress;
					uint32_t length;
					uint64_t xsdtAddress;
					uint8_t xchecksum;
					uint8_t reserved[3];
				}__attribute__((packed));
			public:
				static void initialize();
			private:
				static rsd_pointer *find_rsd_pointer(rsd_pointer *address, size_t size);
				static bool checksum(const rsd_pointer *address);
		};
	#endif
}

/*@}*/

#endif
