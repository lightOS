/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_TYPE_HPP
#define LIGHTOS_KERNEL_TYPE_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <stddef.h>
#include <libarch/type.h>

typedef _LIBARCH_int8_t   int8_t;
typedef _LIBARCH_int16_t  int16_t;
typedef _LIBARCH_int32_t  int32_t;
typedef _LIBARCH_int64_t  int64_t;
typedef _LIBARCH_uint8_t  uint8_t;
typedef _LIBARCH_uint16_t uint16_t;
typedef _LIBARCH_uint32_t uint32_t;

/*@}*/

#endif
