/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_BOOT_INFO_HPP
#define LIGHTOS_KERNEL_X86_BOOT_INFO_HPP

#include <kernel/x86_shared/multiboot.hpp>

/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup x86 x86 */
/*@{*/

namespace kernel
{
  namespace x86
  {
    typedef x86_shared::multiboot boot_info;
    typedef x86_shared::module    module_info;
  }
}

/*@}*/
/*@}*/

#endif
