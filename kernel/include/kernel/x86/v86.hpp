/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_V86_HPP
#define LIGHTOS_KERNEL_X86_V86_HPP

/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup x86 x86 */
/*@{*/

#ifdef _LIGHTOS_V86

#include <cstdint>
#include <kernel/processor.hpp>

namespace kernel
{
	/*! The x86 cpu state */
	struct state86
	{
		uint32_t eax;
		uint32_t ebx;
		uint32_t ecx;
		uint32_t edx;
		uint32_t ds;
		uint32_t es;
		uint32_t edi;
		uint32_t esi;
	};
	
	class v86
	{
		public:
			inline static v86 &instance(){return mInst;}
			bool int86(	uint32_t number,
						uint32_t eax,
						uint32_t ebx,
						uint32_t ecx,
						uint32_t edx,
						uint32_t ds,
						uint32_t es,
						uint32_t edi,
						uint32_t esi);
			state86 get_state();
			bool is_v86_mode(){return bInV86;}
			processorId processor_id(){return mProcessorId;}
		private:
			inline v86():bInV86(false){}
			inline ~v86(){}
			
			static v86 mInst;
			bool bInV86;
			volatile processorId mProcessorId;
	};
}

#endif

/*@}*/
/*@}*/

#endif
