/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_ARCH_ELF_HPP
#define LIGHTOS_KERNEL_X86_ARCH_ELF_HPP

/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup x86 x86 */
/*@{*/

#include <cstddef>
#include <cstdint>

namespace kernel
{
	/*! The elf header on x86 */
	struct elf_header
	{
		uint8_t id[16];
		uint16_t type;
		uint16_t machine;
		uint32_t version;
			uint32_t entry;
			uint32_t programHeader;
			uint32_t sectionHeader;
		uint32_t flags;
		uint16_t headerSize;
		uint16_t programHeaderSize;
		uint16_t programHeaderCount;
		uint16_t sectionHeaderSize;
		uint16_t sectionHeaderCount;
		uint16_t iStringTable;
	}__attribute__((packed));
	
	/*! The elf programm header for x86 */
	struct elf_program_header
	{
		uint32_t type;
			uint32_t offset;
			uint32_t vAddress;
			uint32_t pAddress;
			uint32_t fileSize;
			uint32_t memorySize;
			uint32_t flags;
			uint32_t align;
	}__attribute__((packed));
	
	/*! Entry of the DYNAMIC segment for x86 */
	struct elf_dynamic_entry
	{
		int32_t type;
		union
		{
			uint32_t value;
			uint32_t address;
		};
	}__attribute__((packed));
	
	/*! An elf symbol for x86 */
	struct elf_symbol
	{
		uint32_t name;
			uint32_t value;
			uint32_t size;
		uint8_t info;
		uint8_t other;
		uint16_t shndx;
	}__attribute__((packed));
	
	#define ELF_RELOCATION_SYM(i)  ((i)->info >> 8)
	#define ELF_RELOCATION_TYPE(i) ((i)->info & 0xFF)
	
	/*! An elf relocation entry for x86 */
	struct elf_relocation
	{
		uint32_t address;
		uint32_t info;
	}__attribute__((packed));
	
	/*! An alternative elf relocation entry for x86 */
	struct elf_relocation_a
	{
		uint32_t address;
		uint32_t info;
		uint32_t addend;
	}__attribute__((packed));
	
	/*! The x86 relocation types */
	enum
	{
		R_X86_NONE				= 0x00,
		R_X86_32				= 0x01,
		R_X86_PC32				= 0x02,
		R_X86_COPY				= 0x05,
		R_X86_JMP_SLOT			= 0x07,
		R_X86_RELATIVE			= 0x08
	};
}

/*@}*/
/*@}*/

#endif
