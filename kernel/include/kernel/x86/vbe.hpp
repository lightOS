/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_VBE_HPP
#define LIGHTOS_KERNEL_X86_VBE_HPP

/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup device device */
/*@{*/

#include <string>
#include <vector>

#ifdef _LIGHTOS_V86

	namespace kernel
	{
		/*! The VESA BIOS Extensions */
		class vbe
		{
			public:
				/*! Information about a video mode */
				struct videoMode
				{
					/*! The width in pixel */
					unsigned int width;
					/*! The height in pixel */
					unsigned int height;
					/*! The bits per pixel */
					unsigned int bpp;
					/*! The address of the frame buffer */
					void *framebuffer;
				};
				/*! Get the vbe class instance */
				inline static vbe &instance(){if (inst == 0)inst = new vbe();return *inst;}
				/*! Get the major version */
				unsigned int majorVersion() const{return major;}
				/*! Get the minor version */
				unsigned int minorVersion() const{return minor;}
				/*! Get the OEM */
				const std::string &oem() const{return oemName;}
				/*! Get the vendor name */
				const std::string &vendor() const{return vendorName;}
				/*! Get the product name */
				const std::string &product() const{return productName;}
				/*! Get the product revision */
				const std::string &revision() const{return productRevision;}
				/*! Get the number of available modes */
				unsigned int modeCount() const{return modes.size();}
				/*! Get information about an mode */
				bool getModeInfo(unsigned int index, videoMode &mode) const;
				/*! Set the video mode */
				bool setMode(unsigned int index);
			private:
				/*! The VBE info structure */
				struct infoBlock
				{
					char				signature[4];
					unsigned short		version;
					unsigned int		oemName;
					unsigned int		caps;
					unsigned int		videoMode;
					unsigned short		totalMemory;
					unsigned short		oemSoftwareRevision;
					unsigned int 		oemVendorName;
					unsigned int		oemProductName;
					unsigned int		oemProductRevision;
					unsigned char		res[222];
					unsigned char		oemData[256];
				}__attribute__((packed));
				/*! The VBE mode info structure */
				struct modeInfo
				{
					unsigned short		attributes;
					unsigned char		winAAttributes;
					unsigned char		winBAttributes;
					unsigned short		winGranularity;
					unsigned short		winSize;
					unsigned short		winASegment;
					unsigned short		winBSegment;
					unsigned int		winFunc;
					unsigned short		bytesPerScanline;
					unsigned short		xResolution;
					unsigned short		yResolution;
					unsigned char		xCharSize;
					unsigned char		yCharSize;
					unsigned char		numberOfPlanes;
					unsigned char		bitsPerPixel;
					unsigned char		numberOfBanks;
					unsigned char		memoryModel;
					unsigned char		BankSize;
					unsigned char		numberOfImagePages;
					unsigned char		res1;
					unsigned char		redMaskSize;
					unsigned char		refFieldPosition;
					unsigned char		greenMaskSize;
					unsigned char		greenFieldPosition;
					unsigned char		blueMaskSize;
					unsigned char		blueFieldPosition;
					unsigned char		rsvdMaskSize;
					unsigned char		rsvdFieldPosition;
					unsigned char		directColorModeInfo;
					unsigned int		physicalBase;
					unsigned int		offscreenMemOffset;
					unsigned short		offscreenMemSize;
					unsigned char		res2[206];
				}__attribute__((packed));
				/*! The constructor */
				vbe();
				/*! The destructor */
				~vbe();
				/*! The vbe instance */
				static vbe *inst;
				/*! The VBE major version */
				unsigned int major;
				/*! The VBE minor version */
				unsigned int minor;
				/*! The video memory size */
				size_t videoMemory;
				/*! OEM */
				std::string oemName;
				/*! Vendor name */
				std::string vendorName;
				/*! Product name */
				std::string productName;
				/*! The product revision */
				std::string productRevision;
				/*! List of available modes */
				std::vector<unsigned short> modes;
				/*! Information on the available modes */
				std::vector<modeInfo*> modesInfo;
		};
	}

#endif

/*@}*/
/*@}*/

#endif
