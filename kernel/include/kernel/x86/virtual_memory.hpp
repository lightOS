/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_VIRTUAL_MEMORY_HPP
#define LIGHTOS_KERNEL_X86_VIRTUAL_MEMORY_HPP


/*
 * Standard includes
 */
#include <cstddef>
#include <cstdint>


/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup kernel_x86 x86 */
/*@{*/

namespace kernel
{
  namespace x86
  {
    namespace assembler
    {
      extern "C" uintptr_t page_directory;
      extern "C" uintptr_t kernel_log_tmp;
      extern "C" uintptr_t kernel_console_window;
    }
    namespace linkerscript
    {
      extern "C" uintptr_t __kernel_start__;
      extern "C" uintptr_t __kernel_end__;
    }

    typedef void*            context_handle_t;

    struct virtual_memory
    {
      /*
       * virtual_memory interface constants
       */
      static const uintptr_t page_size                  = 4096;
      static const uintptr_t page_mask                  = 0xFFF;

      static const uintptr_t page_stack_begin           = 0xF0000000;
      static const uintptr_t kernel_memory_region_begin = 0xE0000000;
      // TODO static cosnt void*  page_stack_end         = ;
      static const uintptr_t kernel_log_begin           = 0xFF000000;
      static const uintptr_t kernel_log_end             = 0xFF400000;

      /*
       * Internal constants
       */
      static const uintptr_t _kernel_page_directory     = 0xFFBFE000;
      static const uintptr_t _phys_virt_shift           = 0xBFF00000;

      /*
       * virtual_memory interface functions
       */
      static inline uintptr_t kernel_binary_begin();
      static inline uintptr_t kernel_binary_end();
      static inline uintptr_t kernel_binary_physical_begin();
      static inline uintptr_t kernel_binary_physical_end();
      static inline uintptr_t kernel_log_tmp_first();
      static inline uintptr_t kernel_log_tmp_last();
      static inline context_handle_t kernel_context_handle();

      /*
       * Internal functions (x86)
       */
      template<typename T>
      static T* kernel_page_directory();

      /*
       * Internal functions (x86_shared)
       */
      template<typename T>
      static T* kernel_console_window();
    };



    /*
     * The implementation
     */

    uintptr_t virtual_memory::kernel_binary_begin()
    {
      return reinterpret_cast<uintptr_t>(&linkerscript::__kernel_start__);
    }

    uintptr_t virtual_memory::kernel_binary_end()
    {
      return reinterpret_cast<uintptr_t>(&linkerscript::__kernel_end__);
    }

    uintptr_t virtual_memory::kernel_binary_physical_begin()
    {
      return kernel_binary_begin()  - _phys_virt_shift;
    }

    uintptr_t virtual_memory::kernel_binary_physical_end()
    {
      return kernel_binary_end()  - _phys_virt_shift;
    }

    context_handle_t virtual_memory::kernel_context_handle()
    {
      return reinterpret_cast<context_handle_t>(reinterpret_cast<uintptr_t>(&assembler::page_directory) - _phys_virt_shift);
    }

    uintptr_t virtual_memory::kernel_log_tmp_first()
    {
      return reinterpret_cast<uintptr_t>(&assembler::kernel_log_tmp);
    }

    uintptr_t virtual_memory::kernel_log_tmp_last()
    {
      return kernel_log_tmp_first() + page_size;
    }



    /*
     * Internal functions (x86)
     */

    template<typename T>
    T* virtual_memory::kernel_page_directory()
    {
      return reinterpret_cast<T*>(_kernel_page_directory);
    }



    /*
     * Internal functions (x86_shared)
     */

    template<typename T>
    T* virtual_memory::kernel_console_window()
    {
      return reinterpret_cast<T*>(&assembler::kernel_console_window);
    }
  }
}

/*@}*/
/*@}*/

#endif
