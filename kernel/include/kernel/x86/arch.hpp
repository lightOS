/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_X86_ARCH_HPP
#define LIGHTOS_KERNEL_X86_ARCH_HPP

/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup x86 x86 */
/*@{*/

#include <cstddef>
#include <cstdint>

#define KERNEL_CONTEXT_START \
	kernel_context &KernelContext = kernel_context::instance();\
	context_handle_t tmpContext = processor::context();\
	if (tmpContext != KernelContext.get_handle())processor::context(KernelContext.get_handle());
#define KERNEL_CONTEXT_END if(tmpContext != KernelContext.get_handle())processor::context(tmpContext);

/*! Define the address of the first shared-library */
#define SHARED_LIBRARY_START 0x90000000
/*! Define the address of the first memory-region */
#define MEMORY_REGION_START reinterpret_cast<void*>(0xA0000000)
/*! Define the address of the first shared-memory region  */
#define SHARED_MEMORY_START reinterpret_cast<void*>(0xB0000000)
/*! Define the address of the first thread stack */
#define THREAD_STACK_START reinterpret_cast<void*>(0xC0000000)
/*! Extract the message type */
#define MESSAGE_TYPE(x) ((x) & (~0xE0000000))

template<class T>
T physical_address(T a){return a;}

namespace kernel
{
	/*! Define the type of an IPC message parameter */
	typedef uint32_t message_param_t;
	
	/*! Stack frame after an interrupt */
	struct interrupt_stackframe
	{
		uint32_t edi;
		uint32_t esi;
		uint32_t ebp;
		uint32_t res;
		uint32_t ebx;
		uint32_t edx;
		uint32_t ecx;
		uint32_t eax;
		uint32_t int_number;
		uint32_t errorcode;
		uint32_t eip;
		uint32_t cs;
		uint32_t eflags;
		uint32_t esp;
		uint32_t ss;
		
		inline uint32_t &param0(){return eax;}
		inline uint32_t &param1(){return ebx;}
		inline uint32_t &param2(){return ecx;}
		inline uint32_t &param3(){return edx;}
		inline uint32_t &param4(){return esi;}
		inline uint32_t &param5(){return edi;}
		inline uint32_t &param6(){return ebp;}
		inline bool is_kernel_space(){return (cs == 0x08);}
		inline size_t syscall_number(){return eax;}
	};
	
	struct thread_state
	{
		uint32_t eax;
		uint32_t ebx;
		uint32_t ecx;
		uint32_t edx;
		uint32_t edi;
		uint32_t esi;
		uint32_t ebp;
		uint32_t esp;
		uint32_t eip;
		uint32_t eflags;
	};
	
	/*! interrupt handler
	 *\param[in] stackframe pointer to the stackframe */
	extern "C" void interrupt(interrupt_stackframe &stackframe);
	
	struct task_state_segment
	{
		uint16_t link;
		uint16_t res0;
		uint32_t esp0;
		uint16_t ss0;
		uint16_t res1;
		uint32_t esp1;
		uint16_t ss1;
		uint16_t res2;
		uint32_t esp2;
		uint16_t ss2;
		uint16_t res3;
		uint32_t cr3;
		uint32_t eip;
		uint32_t eflags;
		uint32_t eax;
		uint32_t ecx;
		uint32_t edx;
		uint32_t ebx;
		uint32_t esp;
		uint32_t ebp;
		uint32_t esi;
		uint32_t edi;
		uint16_t es;
		uint16_t res4;
		uint16_t cs;
		uint16_t res5;
		uint16_t ss;
		uint16_t res6;
		uint16_t ds;
		uint16_t res7;
		uint16_t fs;
		uint16_t res8;
		uint16_t gs;
		uint16_t res9;
		uint16_t ldt;
		uint16_t res10;
		uint16_t res11;
		uint16_t ioPermBitmap;
	} __attribute__((packed));
}

/*@}*/
/*@}*/

#endif
