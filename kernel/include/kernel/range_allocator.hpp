/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_RANGEALLOCATOR_HPP
#define LIGHTOS_KERNEL_RANGEALLOCATOR_HPP

#include <cstddef>
#include <cstdint>
#include <list>

/*
 * Libarch includes
 */
#include <libarch/scoped_lock.hpp>

#include <kernel/spinlock.hpp>
#include <kernel/utils/macros.hpp>

/*! \addtogroup kernel kernel */
/*@{*/
/*! \addtogroup kernel_pmm physical memory-management */
/*@{*/

namespace kernel
{
  /*! range_allocator class for the management of pages below the 16mb barrier */
  class range_allocator
  {
    SINGLETON(range_allocator);

    public:
      /*! A memory-range */
      struct range
      {
        /*! physical start address */
        uintptr_t address;
        /*! size (in pages) of the range */
        size_t size;
      };

      /*! Get the capacity */
      inline size_t capacity() const;
      /*! Set the capacity (userfriendly version)
       *\param[in] cap the new capacity */
      inline void capacity(size_t capacity) INIT_ONLY;
      /*! Allocate a new range
       *\param[in] size size of the memory region in pages
       *\param[in] Type special requirements regarding the address size
       *\return physical address of the beginning of the range */
      void *allocate_range(size_t size,
                           size_t Type);
      /*! Free a range
       *\param[in] page physical address of the beginning of the range
       *\param[in] size the size of the memory region in pages */
      void free_range(uintptr_t page,
                      size_t size);
      /*! Remove a range from the allocator
       *\param[in] page physical address of the beginning of the range
       *\param[in] size the size of the memory region in pages */
      void remove_range(uintptr_t page,
                        size_t size);

      /*! Get the number of ranges */
      inline size_t count() const;
      /*! Return the n'th range */
      range get_range(size_t n);

    private:
      /*! Remove a page from the allocator
       *\param[in] page physical address of the beginning of the range */
      void remove_page(uintptr_t page,
                       std::list<range> *Container);

      /*! The capacity (userfriendly version) */
      size_t m_capacity;
      /*! free ranges below 1MB */
      std::list<range> m_below1Mb;
      /*! free ranges below 16MB */
      std::list<range> m_below16Mb;

      /*! Spinlock to protect the range_allocator in a multiprocessor environment */
      SPINLOCK_DECL_MUTABLE(m_lock)
  };



  /*
   * Part of the implementation
   */

  size_t range_allocator::capacity() const
  {
    SCOPED_LOCK(m_lock, scope);
    return m_capacity;
  }

  void range_allocator::capacity(size_t capacity)
  {
    SCOPED_LOCK(m_lock, scope);
    m_capacity = capacity;
  }

  size_t range_allocator::count() const
  {
    SCOPED_LOCK(m_lock, scope);
    return m_below1Mb.size() + m_below16Mb.size();
  }
}

/*@}*/
/*@}*/

#endif
