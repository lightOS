/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_KERNEL_HPP
#define LIGHTOS_KERNEL_KERNEL_HPP

/*! \addtogroup kernel kernel */
/*@{*/

#include <cstddef>
#include <kernel/arch/boot_info.hpp>

namespace kernel
{
  /** The kernel's major version number */
  const size_t version_major = 0;
  /** The kernel's minor version number */
  const size_t version_minor = 2;

  /** Define the type that contains information passed from the bootloader
   *  to the kernel */
  typedef ARCH_NAMESPACE::boot_info   boot_info;
  /** Define the type that contains information about one boot module */
  typedef ARCH_NAMESPACE::module_info module_info;
}

/** Kernel C++ entry point
 *\param[in] info information passed from the bootloader to the kernel */
extern "C" void kernel_main(const kernel::boot_info &info);

/*@}*/

#endif
