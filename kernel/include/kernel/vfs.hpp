/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_KERNEL_VFS_HPP
#define LIGHTOS_KERNEL_VFS_HPP

#include <cstddef>
#include <string>
#include <kernel/port_manager.hpp>

/*! \addtogroup kernel kernel */
/*@{*/

namespace kernel
{
  struct mountpoint
  {
    portId      m_port;
    std::string m_device;
    std::string m_path;
    std::string m_fsname;
  };

  class vfs
  {
    public:
      // TODO: move to libkernel
      enum error
      {
        E_INVALID_FS_NAME,
        E_INVALID_DEVICE_PATH,
        E_INVALID_MOUNT_PATH,
        E_NOT_MOUNTED
      };

      error mount(const char *device,
                  const char *path,
                  portId fs_port);
      error unmount(const char *path);

      const mountpoint *find_filesystem(const char *path);

      size_t get_mointpoint_count();
      const mountpoint *get_mointpoint(size_t index);

      /** \todo move to process */
      void set_working_dir();
      /** \todo move to process */
      void get_working_dir();
      /** \todo move to process */
      void set_console();
      /** \todo move to process */
      void get_console();

    private:
      struct pending_mount
      {
        portId      mounter;
        portId      fsport;
        std::string device;
        std::string path;
        std::string fsname;
      };
  };
}

/*@}*/

#endif
