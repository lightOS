/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstddef>
#include <kernel/thread.hpp>
#include <kernel/scheduler.hpp>
#include <lightOS/c++utils.hpp>
#include <kernel/processor.hpp>
#include <kernel/page_allocator.hpp>

#include <kernel/virtual_memory.hpp>
using namespace std;
using kernel::thread;
using kernel::virtual_memory;

bool thread::handle_page_fault(void *address)
{
	page_allocator &PageAllocator = page_allocator::instance();
	uintptr_t pfAddress = reinterpret_cast<uintptr_t>(address);
	if (pfAddress < (reinterpret_cast<uintptr_t>(threadStack) - threadStackSize) &&
		pfAddress > (reinterpret_cast<uintptr_t>(threadStack) - 1024 * 1024))
	{
		size_t pages = (reinterpret_cast<uintptr_t>(threadStack) + threadStackSize - pfAddress + virtual_memory::page_size - 1) / virtual_memory::page_size;
		
		for (size_t i = 0;i < pages;i++)
		{
			context &Context = Process.getContext();
			void *page = PageAllocator.allocate();
			Process.mPages.push_back(page);
			Context.map(page,
						adjust_pointer(threadStack, - threadStackSize - virtual_memory::page_size),
						lightOS::context::write | lightOS::context::user);
			threadStackSize += virtual_memory::page_size;
		}
		return true;
	}
	return false;
}
void thread::block(state reason)
{
	if (reason == run)return;
	if (State == run)
	{
		scheduler::remove(this);
	}
	State = reason;
}
void thread::unblock()
{
	if (State == run)return;
	State = run;
	
	scheduler::add(this);
}
thread::~thread()
{
	processor::free_fpu_state(this);
	
	scheduler::remove(this);
}
