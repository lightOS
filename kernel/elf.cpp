/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstring>
#include <climits>
#include <kernel/log.hpp>
#include <kernel/elf.hpp>
#include <kernel/arch/arch.hpp>
#include <kernel/page_allocator.hpp>
#include <lightOS/c++utils.hpp>
#include <kernel/processor.hpp>

#include <kernel/virtual_memory.hpp>
using namespace std;
using namespace kernel;
using kernel::virtual_memory;

vector<kernel::elf::library> kernel::elf::m_library;
uintptr_t elf::m_library_base = SHARED_LIBRARY_START;
#ifdef _LIGHTOS_SMP
  lightOS::spinlock elf::m_lock;
#endif

size_t elf::library_count()
{
  LOCK(m_lock);
    size_t size = m_library.size();
  UNLOCK(m_lock);
  return size;
}

size_t elf::get_library_name(size_t index,
                             char *result)
{
  if (index >= m_library.size())return 0;
  
  LOCK(m_lock);
  
    size_t length = m_library[index].name.length();
    if (result == 0)
    {
      UNLOCK(m_lock);
      return length;
    }
    strcpy(result,
           m_library[index].name.c_str());
  
  UNLOCK(m_lock);
  return length;
}

pair<uintptr_t,size_t> elf::get_library_address(size_t index)
{
  pair<uintptr_t,size_t> result(0, 0);
  
  LOCK(m_lock);
    if (index < m_library.size())
    {
      result.first = m_library[index].base_address;
      result.second = m_library[index].size;
    }
  UNLOCK(m_lock);
  
  return result;
}

elf::elf(void *start, size_t s)
  : mHeader(reinterpret_cast<elf_header*>(start)), mSize(s), mStringTable(0), mSymbolCount(0), mSymbolSize(0), mSymbolTable(0),
    mRelocationSize(0), mRelocationTableSize(0), mRelocationTable(0), mRelocationASize(0), mRelocationATableSize(0),
    mRelocationATable(0), mPltGot(0), mPltGotRelocationTable(0), mPltGotRelocationTableSize(0), mPltGotRelocationType(0)
{
  mProgHeader = adjust_pointer(reinterpret_cast<elf_program_header*>(start), mHeader->programHeader);
}

void *elf::get_file_offset(void *vAddress, size_t size)
{
  for (size_t i = 0;i < mHeader->programHeaderCount;i++)
  {
    elf_program_header *p = adjust_pointer(reinterpret_cast<elf_program_header*>(mProgHeader), i * mHeader->programHeaderSize);
    if (p->vAddress <= reinterpret_cast<uintptr_t>(vAddress) &&
      (p->vAddress + p->memorySize) >= reinterpret_cast<uintptr_t>(adjust_pointer(vAddress, size)))
    {
      return adjust_pointer(vAddress, reinterpret_cast<uintptr_t>(mHeader) + p->offset - p->vAddress);
    }
  }
  return 0;
}

void elf::parse_dynamic(elf_dynamic_entry *list,
            size_t count,
            size_t minAddr)
{
  for (size_t i = 0;i < count;i++)
  {
    if (list[i].type == DT_NULL)break;
    switch (list[i].type)
    {
      case DT_NEEDED:
      {
        // Save the index in the string table
        mNeededLibraries.push_back(list[i].value);
      }break;
      case DT_HASH:
      {
        // Save the number of symbols
        if (mIsLibrary == true)
          mSymbolCount = *adjust_pointer(reinterpret_cast<uint32_t*>(mHeader), minAddr + list[i].address + 4);
        else
          mSymbolCount = *adjust_pointer(reinterpret_cast<uint32_t*>(mHeader), list[i].address - 0x400000 + 4);
      }break;
      case DT_STRTAB:
      {
        // Save a pointer to the string table
        if (mIsLibrary == true)
          mStringTable = adjust_pointer(reinterpret_cast<char*>(mHeader), minAddr + list[i].address);
        else
          mStringTable = reinterpret_cast<char*>(list[i].address);
      }break;
      case DT_SYMTAB:
      {
        // Save a pointer to the symbol table
        if (mIsLibrary == true)
          mSymbolTable = adjust_pointer(reinterpret_cast<elf_symbol*>(mHeader), minAddr + list[i].address);
        else
          mSymbolTable = reinterpret_cast<elf_symbol*>(list[i].address);
      }break;
      case DT_SYMENT:
      {
        // Save the size of one symbol table entry
        mSymbolSize = list[i].value;
      }break;
      case DT_PLTGOT:
      {
        // Save a pointer to the Procedure Linkage/Global Offset Table
        mPltGot = reinterpret_cast<void*>(list[i].address);
      }break;
      case DT_PLTRELSZ:
      {
        // Save the size of the relocation table
        mPltGotRelocationTableSize = list[i].value;
      }break;
      case DT_PLTREL:
      {
        // Save the type of relocation entries
        mPltGotRelocationType = list[i].value;
      }break;
      case DT_JMPREL:
      {
        mPltGotRelocationTable = reinterpret_cast<void*>(list[i].address);
      }break;
      case DT_RELA:
      {
        // Save a pointer to the relocation table
        mRelocationATable = reinterpret_cast<elf_relocation_a*>(list[i].address);
      }break;
      case DT_RELASZ:
      {
        // Save the size of the relocation table
        mRelocationATableSize = list[i].value;
      }break;
      case DT_RELAENT:
      {
        // Save the size of one relocation table entry
        mRelocationASize = list[i].value;
      }break;
      case DT_REL:
      {
        // Save a pointer to the relocation table
        mRelocationTable = reinterpret_cast<elf_relocation*>(list[i].address);
      }break;
      case DT_RELSZ:
      {
        // Save the size of the relocation table
        mRelocationTableSize = list[i].value;
      }break;
      case DT_RELENT:
      {
        // Save the size of one relocation table entry
        mRelocationSize = list[i].value;
      }break;
      default:
      {
        //LOG("elf::create_library_image(): entry #" << dec << i << " = 0x" << hex << list[i].type << endl);
      }break;
    }
  }
}

void *elf::lib_find_physical_page(uintptr_t address,
                                  library &lib)
{
  for (size_t y = 0;y < lib.second.size();y++)
    if (lib.second[y].second == reinterpret_cast<void*>((address + m_library_base) & (~0xFFF)))
      return physical_address<void*>(lib.second[y].first);
  return 0;
}

void elf::lib_do_relocation_table(elf_relocation *table,
                                  size_t size,
                                  library &lib)
{
  table = reinterpret_cast<elf_relocation*>(get_file_offset(table, size));
  for (size_t i = 0;i < (size / sizeof(elf_relocation));i++)
  {
    elf_relocation *rel = &table[i];
    elf_symbol *Symbol = adjust_pointer(reinterpret_cast<elf_symbol*>(mSymbolTable), ELF_RELOCATION_SYM(rel) * mSymbolSize);
    
    // Find the right page
    void *page1 = lib_find_physical_page(rel->address, lib);
    void *page2 = lib_find_physical_page(rel->address + virtual_memory::page_size, lib);
    
    // Process the relocation
    do_relocation(rel,
                  adjust_pointer(page1, rel->address & 0xFFF),
                  page2,
                  m_library_base,
                  Symbol);
  }
}

void elf::lib_do_relocation_a_table(elf_relocation_a *table,
                                    size_t size,
                                    library &lib)
{
  table = reinterpret_cast<elf_relocation_a*>(get_file_offset(table, size));
  for (size_t i = 0;i < (size / sizeof(elf_relocation_a));i++)
  {
    elf_relocation_a *rel = &table[i];
    elf_symbol *Symbol = adjust_pointer(reinterpret_cast<elf_symbol*>(mSymbolTable), ELF_RELOCATION_SYM(rel) * mSymbolSize);
    
    // Find the right page
    void *page1 = lib_find_physical_page(rel->address, lib);
    void *page2 = lib_find_physical_page(rel->address + virtual_memory::page_size, lib);
    
    // Process the relocation
    do_relocation_a(rel,
                    adjust_pointer(page1, rel->address & 0xFFF),
                    page2,
                    m_library_base,
                    Symbol);
  }
}

void elf::create_library_image(const char *name)
{
  LOCK(m_lock);
  
    // Is the library already loaded
    for (size_t i = 0;i < m_library.size();i++)
      if (m_library[i].name == name)
      {
        UNLOCK(m_lock);
        return;
      }
    
    // Library information
    library lib;
    lib.name = name;
    
    // Go through the segments
    size_t maxAddr = 0;
    size_t minAddr = ULONG_MAX;
    page_allocator &PageAllocator = page_allocator::instance();
    for (size_t i = 0;i < mHeader->programHeaderCount;i++)
    {
      elf_program_header *p = reinterpret_cast<elf_program_header*>(reinterpret_cast<uintptr_t>(mProgHeader) + i * mHeader->programHeaderSize);
      if (p->type == 1)
      {
        size_t fileSize = p->fileSize;
        for (size_t i = 0;i < p->memorySize;)
        {
          size_t y = 0;
          void *page = 0;
          bool found = false;
          for (;y < lib.second.size();y++)
            if (lib.second[y].second == reinterpret_cast<void*>((p->vAddress + i + m_library_base) & (~0xFFF)))
            {
              page = lib.second[y].first;
              found = true;
            }
          if (found == false)
          {
            page = PageAllocator.allocate();
            memset( physical_address<void*>(page),
                0,
                virtual_memory::page_size);
          }
          
          // Copy the section content to memory
          if (fileSize != 0)
          {
            size_t cpysize = virtual_memory::page_size;
            size_t offset = 0;
            if (i == 0 && (p->vAddress & 0xFFF) != 0)
              offset = p->vAddress & 0xFFF;
            if (fileSize < virtual_memory::page_size)cpysize = fileSize;
            if ((cpysize + offset) > virtual_memory::page_size)cpysize = virtual_memory::page_size - offset;
            memcpy( physical_address<void*>(adjust_pointer(page, offset)),
                &reinterpret_cast<char*>(mHeader)[p->offset + i],
                cpysize);
          }
          
          // Save the page
          size_t flags = lightOS::context::user;
          if ((p->flags & 0x01) != 0)flags |= lightOS::context::execute;
          if ((p->flags & 0x02) != 0)flags |= lightOS::context::write;
          if (found == false)
          {
            lib.second.push_back(triple<void*,void*,size_t>(page, reinterpret_cast<void*>((p->vAddress + i + m_library_base) & (~0xFFF)), flags));
          }
          else if (flags != lib.second[y].third)
          {
            lib.second[y].third |= flags;
          }
          
          if ((p->vAddress + i + virtual_memory::page_size) > maxAddr)
            maxAddr = p->vAddress + i + virtual_memory::page_size;
          if ((p->offset + i) < minAddr)
            minAddr = p->offset + i;
          
          if (i == 0 && (p->vAddress & 0xFFF) != 0)fileSize += p->vAddress & 0xFFF;
          if (fileSize > virtual_memory::page_size)fileSize -= virtual_memory::page_size;
          else fileSize = 0;
          i += virtual_memory::page_size;
        }
      }
      else if (p->type == 2)
      {
        // DYNAMIC segment
        parse_dynamic(  adjust_pointer(reinterpret_cast<elf_dynamic_entry*>(mHeader), p->offset),
                p->fileSize / sizeof(elf_dynamic_entry),
                minAddr);
      }
    }
    
    // correct maxAddr
    if ((maxAddr % virtual_memory::page_size) != 0)maxAddr -= maxAddr % virtual_memory::page_size;
    
    // Go through the symbol table
    for (size_t i = 1;i < mSymbolCount;i++)
    {
      elf_symbol *Symbol = adjust_pointer(mSymbolTable, i * mSymbolSize);
      char *SymbolName = &mStringTable[Symbol->name];
      if (Symbol->shndx != 0)
      {
        Symbol->value += m_library_base;
        lib.third.push_back(pair<string,void*>(SymbolName, reinterpret_cast<void*>(Symbol->value)));
      }
      else if (Symbol->shndx == 0 && (Symbol->info & 0x0F) == 2 && Symbol->value != 0)
      {
        LOG("elf: special symbol \"" << SymbolName << "\"");
      }
      else
      {
        // Search the symbol in the other shared libraries
        bool found = false;
        for (size_t y = 0;y < m_library.size();y++)
          for (size_t z = 0;z < m_library[y].third.size();z++)
          {
            if (m_library[y].third[z].first == SymbolName)
            {
              Symbol->value = reinterpret_cast<uintptr_t>(m_library[y].third[z].second);
              found = true;
            }
          }
        if (found == false)
        {
          WARNING("elf: unknown external symbol \"" << SymbolName << "\"");
        }
      }
    }
    
    // Process the relocations (REL)
    if (mRelocationTable != 0)
    {
      lib_do_relocation_table(mRelocationTable,
                  mRelocationTableSize,
                  lib);
    }
    
    // Process the relocations (RELA)
    if (mRelocationATable != 0)
    {
      lib_do_relocation_a_table(  mRelocationATable,
                    mRelocationATableSize,
                    lib);
    }
    
    // Process PLT/GOT relocation entries
    if (mPltGotRelocationTable != 0)
    {
      if (mPltGotRelocationType == DT_REL)
      {
        lib_do_relocation_table(reinterpret_cast<elf_relocation*>(mPltGotRelocationTable),
                    mPltGotRelocationTableSize,
                    lib);
      }
      else if (mPltGotRelocationType == DT_RELA)
      {
        lib_do_relocation_a_table(  reinterpret_cast<elf_relocation_a*>(mPltGotRelocationTable),
                      mPltGotRelocationTableSize,
                      lib);
      }
      else
      {
        ERROR("elf::create_library_image(): unknown PLT/GOT relocation entries");
      }
    }
    
    LOG("elf: \"" << name << "\" loaded (address 0x" << hex(m_library_base) << " - 0x" <<
        hex(m_library_base + maxAddr) << ")");
    
    lib.base_address = m_library_base;
    lib.size = maxAddr;
    m_library.push_back(lib);
    m_library_base += maxAddr;
  
  UNLOCK(m_lock);
}

void elf::process_do_relocation_table(  elf_relocation *table,
                    size_t size)
{
  for (size_t i = 0;i < (size / sizeof(elf_relocation));i++)
  {
    elf_relocation *rel = &table[i];
    if (ELF_RELOCATION_SYM(rel) == 0)continue;
    
    bool found = false;
    elf_symbol *Symbol = adjust_pointer(mSymbolTable, ELF_RELOCATION_SYM(rel) * mSymbolSize);
    const char *name = &mStringTable[Symbol->name];
    uintptr_t targetAddr = 0;
    if (Symbol->shndx == 0xFFF1)
    {
      targetAddr = Symbol->value;
      found = true;
    }
    else
    {
      for (size_t y = 0;y < m_library.size();y++)
        for (size_t z = 0;z < m_library[y].third.size();z++)
          if (m_library[y].third[z].first == name)
          {
            targetAddr = reinterpret_cast<uintptr_t>(m_library[y].third[z].second);
            found = true;
          }
    }

    if (found == true)
    {
      do_relocation(  rel,
              reinterpret_cast<void*>(rel->address),
              #ifdef X86
                reinterpret_cast<void*>((rel->address + virtual_memory::page_size) & 0xFFFFF000),
              #else
                reinterpret_cast<void*>((rel->address + virtual_memory::page_size) & 0xFFFFFFFFFFFFF000),
              #endif
              targetAddr,
              Symbol);
    }
    else
    {
      ERROR("elf::process_do_relocation_table(): could not find symbol \"" << name << "\"");
    }
  }
}

void elf::process_do_relocation_a_table(elf_relocation_a *table,
                    size_t size)
{
  for (size_t i = 0;i < (size / sizeof(elf_relocation_a));i++)
  {
    elf_relocation_a *rel = &table[i];
    if (ELF_RELOCATION_SYM(rel) == 0)continue;
    
    bool found = false;
    elf_symbol *Symbol = adjust_pointer(mSymbolTable, ELF_RELOCATION_SYM(rel) * mSymbolSize);
    const char *name = &mStringTable[Symbol->name];
    uintptr_t targetAddr = 0;
    if (Symbol->shndx != 0)
    {
      targetAddr = Symbol->value;
      found = true;
    }
    else
    {
      for (size_t y = 0;y < m_library.size();y++)
        for (size_t z = 0;z < m_library[y].third.size();z++)
          if (m_library[y].third[z].first == name)
          {
            targetAddr = reinterpret_cast<uintptr_t>(m_library[y].third[z].second);
            found = true;
          }
    }
    
    if (found == true)
    {
      do_relocation_a(rel,
              reinterpret_cast<void*>(rel->address),
              #ifdef X86
                reinterpret_cast<void*>((rel->address + virtual_memory::page_size) & 0xFFFFF000),
              #else
                reinterpret_cast<void*>((rel->address + virtual_memory::page_size) & 0xFFFFFFFFFFFFF000),
              #endif
              targetAddr,
              Symbol);
    }
    else
    {
      ERROR("elf::process_do_relocation_a_table(): could not find symbol \"" << name << "\"");
    }
  }
}

bool elf::create_process_image( context &Context,
                std::vector<void*> &Pages,
                string &libName)
{
  LOCK(m_lock);
  
    KERNEL_CONTEXT_START;
    
    page_allocator &PageAllocator = page_allocator::instance();
    
    // Go through the sections
    bool phdr = false;
    uintptr_t maxAddr = 0x400000;
    size_t headerSize = mHeader->programHeaderCount;
    for (size_t i = 0;i < headerSize;i++)
    {
      elf_program_header *p = adjust_pointer(mProgHeader, i * mHeader->programHeaderSize);
      
      if (p->type == 0)continue;
      else if (p->type == 1)
      {
        size_t vAddr = p->vAddress;
        size_t fileSize = p->fileSize;
        for (size_t i = 0;i < p->memorySize;)
        {
          if (vAddr >= SHARED_LIBRARY_START)
          {
            ERROR("elf: invalid userspace elf file");
            
            // Clean up
            for (size_t i = 0;i < Pages.size();i++)
              PageAllocator.free(Pages[i]);
            
            KERNEL_CONTEXT_END;
            
            UNLOCK(m_lock);
            return false;
          }
          
          bool alreadyMapped = true;
          void *page = Context.physical_address(reinterpret_cast<void*>(vAddr & (~0xFFF)));
          if (page == 0)
          {
            page = PageAllocator.allocate();
            memset( physical_address<void*>(page),
                0,
                virtual_memory::page_size);
            alreadyMapped = false;
            Pages.push_back(page);
          }
          
          // Copy the section content to memory
          if (fileSize != 0)
          {
            size_t cpysize = virtual_memory::page_size;
            size_t offset = 0;
            if (i == 0 && (vAddr & 0xFFF) != 0)
              offset = vAddr & 0xFFF;
            if (fileSize < virtual_memory::page_size)cpysize = fileSize;
            if ((cpysize + offset) > virtual_memory::page_size)cpysize = virtual_memory::page_size - offset;
            memcpy( physical_address<void*>(adjust_pointer(page, offset)),
                &reinterpret_cast<char*>(mHeader)[p->offset + i],
                cpysize);
          }
          
          // Map the page
          size_t flags = lightOS::context::user;
          size_t flags_old;
          if ((p->flags & 0x01) != 0)flags |= lightOS::context::execute;
          if ((p->flags & 0x02) != 0)flags |= lightOS::context::write;
          if (alreadyMapped == false)
          {
            Context.map(page,
                  reinterpret_cast<void*>(vAddr),
                  flags);
          }
          // FIXME: We might need to adjust this for x86 (because there it is always executeable)
          else if ((flags_old = Context.physical_address_flags(reinterpret_cast<void*>(vAddr))) != flags)
          {
            ERROR("elf: two section with different flags are overlapping @ 0x" << hex(vAddr) << ", flags = 0x" <<
                  hex(flags) << ", flags_old = 0x" << hex(flags_old));
            processor::halt();
          }
          
          if (((vAddr + virtual_memory::page_size) & (~0xFFF)) > maxAddr)
            maxAddr = (vAddr + virtual_memory::page_size) & (~0xFFF);
          
          vAddr += virtual_memory::page_size;
          if (i == 0 && (vAddr & 0xFFF) != 0)fileSize += p->vAddress & 0xFFF;
          if (fileSize > virtual_memory::page_size)fileSize -= virtual_memory::page_size;
          else fileSize = 0;
          if (i == 0 && (p->vAddress & 0xFFF) != 0)
            i = virtual_memory::page_size - (p->vAddress & 0xFFF);
          else i += virtual_memory::page_size;
        }
      }
      else if (p->type == 2)
      {
        // DYNAMIC segment
        parse_dynamic(  reinterpret_cast<elf_dynamic_entry*>(reinterpret_cast<uintptr_t>(mHeader) + p->offset),
                p->fileSize / sizeof(elf_dynamic_entry),
                0);
      }
      else if (p->type == 3)continue;
      else if (p->type == 6)
      {
        if (phdr == false)
        {
          mProgHeader = adjust_pointer(reinterpret_cast<elf_program_header*>(mHeader), p->offset);
          headerSize = p->fileSize / sizeof(elf_program_header);
          phdr = true;
          --i;
        }
      }
      else if (p->type == 4 || p->type == 5)
      {
        ERROR("elf: unsupported program header type (0x" << hex(p->type) << ")");
        break;
      }
      else
      {
        LOG("elf: unknown program header type (0x" << hex(p->type) << ")");
        continue;
      }
    }
    
    Context.heap(reinterpret_cast<void*>(maxAddr));
    KERNEL_CONTEXT_END;
    
    // Switch to process's context
    context_handle_t oldCtx = processor::context();
    processor::context(Context.get_handle());
    
    // Map the needed libraries
    for (size_t i = 0;i < mNeededLibraries.size();i++)
    {
      const char *name = &mStringTable[mNeededLibraries[i]];
      bool found = false;
      for (size_t i = 0;i < m_library.size();i++)
        if (m_library[i].name == name)
        {
          for (size_t y = 0;y < m_library[i].second.size();y++)
          {
            void *page = m_library[i].second[y].first;
            size_t flags = m_library[i].second[y].third & (~lightOS::context::write);
            if ((m_library[i].second[y].third & lightOS::context::write) != 0)
              flags |= lightOS::context::copy_on_write;
            Context.map(page,
                  m_library[i].second[y].second,
                  flags);
          }
          found = true;
        }
      if (found == false)
      {
        libName = name;
        
        // Clean up
        for (size_t i = 0;i < Pages.size();i++)
          PageAllocator.free(Pages[i]);
        
        // Switch back to the original context
        processor::context(oldCtx);
        
        UNLOCK(m_lock);
        return false;
      }
    }
    
    // Process the relocations (REL)
    if (mRelocationTable != 0)
    {
      process_do_relocation_table(mRelocationTable,
                    mRelocationTableSize);
    }
    
    // Process the relocations (RELA)
    if (mRelocationATable != 0)
    {
      process_do_relocation_a_table(  mRelocationATable,
                      mRelocationATableSize);
    }
    
    // Process PLT/GOT relocation entries
    if (mPltGotRelocationTable != 0)
    {
      if (mPltGotRelocationType == DT_REL)
      {
        process_do_relocation_table(reinterpret_cast<elf_relocation*>(mPltGotRelocationTable),
                      mPltGotRelocationTableSize);
      }
      else if (mPltGotRelocationType == DT_RELA)
      {
        process_do_relocation_a_table(  reinterpret_cast<elf_relocation_a*>(mPltGotRelocationTable),
                        mPltGotRelocationTableSize);
      }
      else
      {
        ERROR("elf::create_process_image(): unknown PLT/GOT relocation entries");
      }
    }
    
    // Switch back to the original context
    processor::context(oldCtx);
  
  UNLOCK(m_lock);
  return true;
}
