/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/kernel.hpp>
#include <cstring>
#include <algorithm>
#include <iterator>
#include <memory>
#include <kernel/smp.hpp>
#include <kernel/acpi.hpp>
#include <kernel/elf.hpp>
#include <kernel/log.hpp>
#include <kernel/scheduler.hpp>
#include <kernel/io_port_manager.hpp>
#include <kernel/x86_shared/pic.hpp>
#include <kernel/x86_shared/apic.hpp>
#include <kernel/x86_shared/serial.hpp>
#include <kernel/x86_shared/gdt.hpp>
#include <kernel/x86_shared/interrupt_controller.hpp>
#include <kernel/range_allocator.hpp>
#include <kernel/virtual_memory.hpp>
#include <kernel/processor.hpp>
using namespace std;
using namespace kernel;
using kernel::virtual_memory;

void kernel_main(const kernel::boot_info &info)
{
  // Go through the module list
  kernel::module_info *Module = physical_address<kernel::module_info*>(reinterpret_cast<kernel::module_info*>(info.mod));
  for (size_t i = 0;i < info.module_count;i++)
  {
    elf Elf(physical_address<void*>(reinterpret_cast<void*>(Module[i].start)),
        Module[i].end - Module[i].start);
    if (Elf.valid() == false)
    {
      FATAL(reinterpret_cast<char*>(Module[i].name) << ": Not a valid elf file.");
      processor::halt();
    }

    if (Elf.is_library() == true)
    {
      /* library */
      const char *name = physical_address<char*>(reinterpret_cast<char*>(Module[i].name));
      Elf.create_library_image(name);
    }
    else
    {
      // Create the process image
      string libName;
      context Context;
      vector<void*> Pages;
      bool result = Elf.create_process_image(Context, Pages, libName);

      string path(physical_address<char*>(reinterpret_cast<char*>(Module[i].name)));
      if (result == true)
      {
        // Create the process
        process::create(Context,
                Pages,
                path,
                Elf.entry_point(),
                lightOS::process::server);
      }
      else
      {
        if (libName.length() != 0)
        {
          ERROR("kernel: shared-library \"" << libName.c_str() << "\" for \"" << path.c_str() << "\" not loaded");
        }
        else
        {
          ERROR("kernel: invalid elf file \"" << path.c_str() << "\"");
        }
      }
    }

    // Free module space
    range_allocator &RangeAllocator = range_allocator::instance();
    size_t size = Module[i].end - Module[i].start + virtual_memory::page_size - 1;
    RangeAllocator.free_range(Module[i].start, size / virtual_memory::page_size);
  }

  // Initialize the I/O Port Manager
  io_port_manager &IOPortManager = io_port_manager::instance();
  IOPortManager.init();

  // Has this processor a local APIC?
  if (processor::has_local_apic() == true)
  {
    #ifdef _LIGHTOS_SMP
      // Initialize the APIC class
      local_apic::initialize();
    #endif
  }
  else
  {
    LOG("No local APIC found");
  }

  // Initialize the processor class
  // NOTE Also calls initialize_this() for the BSP
  processor::initialize();

  // SMP compiled in?
  #ifdef _LIGHTOS_SMP
    if (processor::has_local_apic() == true)
    {
      // Initialize SMP
      smp::initialize();
    }
  #endif

  // ACPI compiled in?
  #ifdef ACPI
    // Initialize ACPI
    acpi::initialize();
  #endif

  // Initialize the GDT
  gdt &Gdt = gdt::instance();
  Gdt.initialize();
  Gdt.load();

  // Load the Task Register
  processor::load_taskregister();

  // SMP compiled in?
  #ifdef _LIGHTOS_SMP
    if (smp::compatible() == true)
    {
      // Complete initialization & schedule!
      smp::initialize2();
    }
  #endif

  // Initialize the interrupt controller
  interrupt_controller &InterruptController = interrupt_controller::instance();
  InterruptController.init();

  scheduler::schedule(0);
}
