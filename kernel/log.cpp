/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libarch/scoped_lock.hpp>
#include <kernel/log.hpp>
#include <kernel/serial.hpp>
#include <kernel/console.hpp>
#include <kernel/processor.hpp>
using kernel::log;
using kernel::helper::tmp_log_entry;


SINGLETON_INSTANCE(kernel::log);



/*
 * Implementation of kernel::log
 */

log::log()
  : m_beg(0), m_cur(0), m_cur_addr(virtual_memory::kernel_log_begin<void>()),
    m_cur_max_addr(adjust_pointer(virtual_memory::kernel_log_begin<void>(), virtual_memory::page_size))

{
}

log::~log()
{
}

void log::commit(const char* begin,
                 const char* end,
                 libkernel::log_level lvl)
{
  #ifdef KERNEL_HAS_SERIAL
    // Pump through the serial line before we do anything else
    {
      const char* tmp = begin;
      serial& Serial = serial::instance();
      while (LIKELY(tmp != end))
        Serial.put(*tmp++);
      Serial.put('\n');
    }
  #endif

  if (LIKELY(lvl != libkernel::log_fatal))
  {
    // Add the entry to the log

    const size_t needed_size = sizeof(entry) + (end - begin) + 1;

    while (UNLIKELY(pointer_difference(m_cur_max_addr, m_cur_addr) < needed_size))
    {
      // TODO: Allocate space & map stuff (the fault tolerant way, perhaps tell serial if we
      //       swallowed a log entry because we ran out of memory)
    }

    entry* new_entry = reinterpret_cast<entry*>(m_cur_addr);
    new_entry->next = 0;
    new_entry->prev = m_cur;
    new_entry->lvl = lvl;
    if (UNLIKELY(m_cur == 0))
      m_beg = new_entry;
    else
      m_cur->next = new_entry;
    m_cur = new_entry;
    m_cur_addr = adjust_pointer(m_cur_addr, needed_size);

    char* entry_string = adjust_pointer_cast<char>(new_entry, sizeof(entry));
    while (LIKELY(begin != end))
      *entry_string++ = *begin++;
    *entry_string = '\0';
  }
  else
  {
    size_t cur_index = 0;
    console& Console = console::instance();
    console_window_ptr kernel_console = Console.get(kernel_console_id);

    // Write the log before the fatal message
    entry *cur = m_cur;

    if (cur != 0)
    {
      size_t i = console_window_height - 2;
      while (i > 0 && cur->prev != 0)
      {
        cur = cur->prev;
        --i;
      }
    }

    while (cur != 0)
    {
      console_color_t color = KERNEL_CONSOLE_NOTICE;
      if (cur->lvl == libkernel::log_warning)
        color = KERNEL_CONSOLE_WARNING;
      else if (cur->lvl == libkernel::log_error)
        color = KERNEL_CONSOLE_ERROR;

      const char* entry_string = adjust_pointer_cast<char>(cur, sizeof(entry));
      while (*entry_string != '\0')
      {
        kernel_console->character(cur_index, *entry_string++);
        kernel_console->color(cur_index, color);
        ++cur_index;
      }
      cur_index += (console_window_width - (cur_index % console_window_width)) % console_window_width;
      cur = cur->next;
    }

    // Write the fatal error message
    const char* fatal = "FATAL: ";
    while (*fatal != '\0')
    {
      kernel_console->character(cur_index, *fatal++);
      kernel_console->color(cur_index, KERNEL_CONSOLE_FATAL);
      ++cur_index;
    }

    while (begin != end)
    {
      kernel_console->character(cur_index, *begin++);
      kernel_console->color(cur_index, KERNEL_CONSOLE_FATAL);
      ++cur_index;
    }
    Console.switch_to(kernel_console);

    // TODO: Fix this for multiprocessor environments
    processor::halt();
  }
}

size_t log::get_entry(size_t index,
                      libkernel::log_level& lvl,
                      char* begin,
                      size_t size)
{
  SCOPED_LOCK(m_lock, scope);

  entry* cur = m_beg;
  while (cur != 0 && index > 0)
  {
    cur = cur->next;
    --index;
  }

  if (cur == 0)
    return 0;

  size_t real_size = 0;
  char* entry_string = adjust_pointer_cast<char>(cur, sizeof(entry));
  while (*entry_string != '\0' && size >= 1)
  {
    *begin++ = *entry_string++;
    --size;
    ++real_size;
  }
  do
  {
    ++real_size;
  } while (*entry_string++ != '\0');
  if (size != 0)
    *begin = '\0';
  lvl = cur->lvl;

  return real_size;
}



/*
 * Implementation of kernel::tmp_log_entry
 */

tmp_log_entry::tmp_log_entry(libkernel::log_level lvl)
  : m_level(lvl),  m_first(virtual_memory::kernel_log_tmp_first<char>()),
    m_next(virtual_memory::kernel_log_tmp_first<char>()), m_last(virtual_memory::kernel_log_tmp_last<char>())
{
  #if defined(_LIGHTOS_MULTIPROCESSOR)
    log::instance().m_lock.lock();
  #endif
}

tmp_log_entry::~tmp_log_entry()
{
  log::instance().commit(m_first,
                         m_next,
                         m_level);

  #if defined(_LIGHTOS_MULTIPROCESSOR)
    log::instance().m_lock.unlock();
  #endif
}

tmp_log_entry& tmp_log_entry::operator << (char c)
{
  if (m_next != m_last)
    *m_next++ = c;
  return *this;
}

tmp_log_entry& tmp_log_entry::operator << (const char* s)
{
  while (*s != '\0')
    tmp_log_entry::operator << (*s++);
  return *this;
}

tmp_log_entry& tmp_log_entry::operator << (const void* p)
{
  return *this << "0x" << hex(reinterpret_cast<uintptr_t>(p), 2 * _LIBARCH_POINTER_SIZE);
}
