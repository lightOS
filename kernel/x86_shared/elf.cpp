/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/elf.hpp>
using namespace std;
using namespace kernel;

uint32_t elf::get32(void *address1,
					void *address2)
{
	switch (reinterpret_cast<uintptr_t>(address1) & 0xFFF)
	{
		case 0xFFF:
		{
			return (*reinterpret_cast<uint8_t*>(address1)) | ((*reinterpret_cast<uint32_t*>(address2) & 0x00FFFFFF) << 8);
		}break;
		case 0xFFE:
		{
			return (*reinterpret_cast<uint16_t*>(address1)) | (*reinterpret_cast<uint16_t*>(address2) << 16);
		}break;
		case 0xFFD:
		{
			return ((*reinterpret_cast<uint32_t*>(reinterpret_cast<uintptr_t>(address1) - 1) & 0xFFFFFF00) >> 8) | (*reinterpret_cast<uint8_t*>(address2) << 24);
		}break;
	}
	return *reinterpret_cast<uint32_t*>(address1);
}

void elf::set32(void *address1,
				void *address2,
				uint32_t value)
{
	switch (reinterpret_cast<uintptr_t>(address1) & 0xFFF)
	{
		case 0xFFF:
		{
			*reinterpret_cast<uint8_t*>(address1) = value & 0xFF;
			*reinterpret_cast<uint32_t*>(address2) &= 0xFF000000;
			*reinterpret_cast<uint32_t*>(address2) |= (value & 0xFFFFFF00) >> 8;
		}break;
		case 0xFFE:
		{
			*reinterpret_cast<uint16_t*>(address1) = value & 0xFFFF;
			*reinterpret_cast<uint16_t*>(address2) = (value & 0xFFFF0000) >> 16;
		}break;
		case 0xFFD:
		{
			*reinterpret_cast<uint32_t*>(reinterpret_cast<uintptr_t>(address1) - 1) &= 0x000000FF;
			*reinterpret_cast<uint32_t*>(reinterpret_cast<uintptr_t>(address1) - 1) |= (value & 0x00FFFFFF) << 8;
			*reinterpret_cast<uint8_t*>(address2) = (value & 0xFF000000) >> 24;
		}break;
		default:
		{
			*reinterpret_cast<uint32_t*>(address1) = value;
		}break;
	}
}
