/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libarch/ioport.hpp>
#include <kernel/x86_shared/dma.hpp>
using kernel::x86_shared::dma;

// TODO: io_port usage?

SINGLETON_INSTANCE(kernel::x86_shared::dma);

uint16_t pagePort[]   = {0x87, 0x83, 0x81, 0x82, 0x8F, 0x8B, 0x89, 0x8A};
uint16_t addressPort[]  = {0x00, 0x02, 0x04, 0x06, 0xC0, 0xC4, 0xC8, 0xCC};
uint16_t countPort[]  = {0x01, 0x03, 0x05, 0x07, 0xC2, 0xC6, 0xCA, 0xCE};

dma::dma()
{
}

dma::~dma()
{
}

bool dma::setup(uint8_t channel,
                uintptr_t address,
                size_t count,
                uint8_t mode)
{
  if (channel >= 8)return false;
  
  LOCK(mLock);
  
    // disable channel, flip-flop & mode
    if (channel < 4)
    {
      _LIBARCH_out8(0x0A, channel|0x4);
      _LIBARCH_out8(0x0C, 0x00);
      _LIBARCH_out8(0x0B, mode | channel);
    }
    else
    {
      _LIBARCH_out8(0xD4, (channel - 4)|0x04);
      _LIBARCH_out8(0xD8, 0x00);
      _LIBARCH_out8(0xD6, mode | (channel - 4));
    }
    
    // Address
    _LIBARCH_out8(addressPort[channel], address & 0xFF);
    _LIBARCH_out8(addressPort[channel], (address >> 8) & 0xFF);
    _LIBARCH_out8(pagePort[channel], (address >> 16) & 0xFF);
    
    // flip-flop
    if (channel < 4)
      _LIBARCH_out8(0x0C, 0x00);
    else
      _LIBARCH_out8(0xD8, 0x00);
    
    // Byte count
    --count;
    _LIBARCH_out8(countPort[channel], count & 0xFF);
    _LIBARCH_out8(countPort[channel], (count >> 8) & 0xFF);
    
    // Enable channel
    if (channel < 4)
      _LIBARCH_out8(0x0A, channel);
    else
      _LIBARCH_out8(0xD4, channel - 4);
  
  UNLOCK(mLock);
  return true;
}
