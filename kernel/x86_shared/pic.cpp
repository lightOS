/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libarch/ioport.hpp>
#include <kernel/log.hpp>
#include <kernel/scheduler.hpp>
#include <kernel/event_manager.hpp>
#include <kernel/x86_shared/pic.hpp>
#include <kernel/x86_shared/cmos.hpp>
using kernel::x86_shared::pic;

SINGLETON_INSTANCE(kernel::x86_shared::pic);

pic::pic()
  : m_pic1(), m_pic2()
{
  if (m_pic1.allocate(0x20, 0x02) == false ||
      m_pic2.allocate(0xA0, 0x02) == false)
  {
    // TODO: Report error
  }

  // Reprogram PIC
  m_pic1.write8(0x11, 0x00);
  m_pic2.write8(0x11, 0x00);
  m_pic1.write8(0x20, 0x01);
  m_pic2.write8(0x28, 0x01);
  m_pic1.write8(0x04, 0x01);
  m_pic2.write8(0x02, 0x01);
  m_pic1.write8(0x01, 0x01);
  m_pic2.write8(0x01, 0x01);

  // Mask all IRQs
  mask();
}

pic::~pic()
{
}

void pic::init()
{
  // Activate the IRQ8 RTC periodic interrupt
  cmos::instance().activate_periodic_irq();

  // Unmask the IRQ8
  unmask(0x08);

  // Unmask the IRQ0
  unmask(0x00);
}

void pic::handle_irq(uint8_t intnumber,
                     interrupt_stackframe &stackframe)
{
  if (intnumber < 32 ||
      intnumber >= 48)
    return;

  /* Is Spurios IRQ7? */
  size_t irq = (intnumber - 32);
  if (irq == 7)
  {
    m_pic1.write8(0x0B, 0x00);
    if ((m_pic1.read8(0x00) & 0x80) == 0)
    {
      LOG("pic: spurious IRQ7");
      return;
    }
  }
  /* Is spurious IRQ15? */
  else if (irq == 15)
  {
    m_pic2.write8(0x0B, 0x00);
    if ((m_pic2.read8(0x00) & 0x80) == 0)
    {
      LOG("pic: spurious IRQ15");
      return;
    }
  }

  /* Is periodic RTC IRQ8 */
  if (irq == 8)
  {
    // Acknoledge the IRQ8
    cmos::instance().ack_periodic_irq();

    // Handle the ticks
    static bool bIsFirst = false;
    event_manager &eventManager = event_manager::instance();
    if (bIsFirst == false)
      eventManager.tick(976);
    else
      eventManager.tick(977);
    bIsFirst = !bIsFirst;
  }
  else if(irq != 0)
  {
    // Mask the irq
    mask(irq);

    // Handle the IRQ
    event_manager &eventManager = event_manager::instance();
    eventManager.handle_irq(intnumber);
  }

  // Send end-of-interrupt
  eoi(irq);

  if (irq == 0)
    scheduler::schedule(&stackframe);
}

uint8_t pic::register_isa_irq(uint8_t irq,
                              uint8_t priority)
{
  if (irq == 0 || irq >= 16)
    return 0;

  LOG("pic: registering ISA-IRQ" << dec(irq));

  unmask(irq);
  return irq + 32;
}

uint8_t pic::register_pci_irq(uint8_t irq,
                              uint8_t intn,
                              uint8_t bus,
                              uint8_t device,
                              uint8_t priority)
{
  if (irq == 0 || irq >= 16)
    return 0;

  LOG("pic: registering PCI-IRQ" << dec(irq));

  unmask(irq);
  return irq + 32;
}

void pic::acknoledge_irq(uint8_t vector)
{
  uint8_t irq = vector - 32;
  unmask(irq);
}

void pic::unregister_irq(uint8_t vector)
{
  uint8_t irq = vector - 32;
  mask(irq);
}

void pic::eoi(int irq)
{
  if (irq >= 16)
    return;

  m_pic1.write8(0x20, 0x00);
  if (irq > 7)
    m_pic2.write8(0x20, 0x00);
}

void pic::mask()
{
  m_pic1.write8(0xFF, 0x01);
  m_pic2.write8(0xFF, 0x01);
}

void pic::mask(uint8_t number)
{
  if (number <= 7)
  {
    uint8_t mask = m_pic1.read8(0x01);
    m_pic1.write8((mask | (1 << number)), 0x01);
  }
  else
  {
    uint8_t mask = m_pic2.read8(0x01);
    m_pic2.write8((mask | (1 << (number - 8))), 0x01);
  }
}

void pic::unmask()
{
  m_pic1.write8(0x00, 0x01);
  m_pic2.write8(0x00, 0x01);
}

void pic::unmask(uint8_t number)
{
  if (number <= 7)
  {
    uint8_t mask = m_pic1.read8(0x01);
    m_pic1.write8((mask & ~(1 << number)), 0x01);
  }
  else
  {
    unmask(2);

    uint8_t mask = m_pic2.read8(0x01);
    m_pic2.write8((mask & ~(1 << (number - 8))), 0x01);
  }
}
