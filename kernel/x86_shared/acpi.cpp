/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <lightOS/c++utils.hpp>
#include <kernel/log.hpp>
#include <kernel/context.hpp>
#include <kernel/processor.hpp>
#include <kernel/acpi.hpp>
using namespace kernel;
using namespace std;

#ifdef ACPI

	void acpi::initialize()
	{
		rsd_pointer *RSDPointer = find_rsd_pointer(physical_address(reinterpret_cast<rsd_pointer*>(0x9FC00)), 0x400);
		if (RSDPointer == 0)RSDPointer = find_rsd_pointer(physical_address(reinterpret_cast<rsd_pointer*>(0xE0000)), 0x20000);
		
		if (RSDPointer->revision == 0){LOG("acpi: ACPI 1.0" << endl);}
		else if (RSDPointer->revision == 1){LOG("acpi: ACPI 2.0" << endl);}
		else if (RSDPointer->revision == 2){LOG("acpi: ACPI 3.0" << endl);}
		else {LOG("acpi: unkown ACPI version" << endl);}
		processor::halt();
		
		// TODO: Map the tables somewhere safe...
	}
	
	acpi::rsd_pointer *acpi::find_rsd_pointer(rsd_pointer *address, size_t size)
	{
		rsd_pointer *cur = address;
		while (reinterpret_cast<uintptr_t>(cur) < (reinterpret_cast<uintptr_t>(address) + size))
		{
			if (cur->signature[0] == 'R' && cur->signature[1] == 'S' && cur->signature[2] == 'D' && cur->signature[3] == ' ' &&
				cur->signature[4] == 'P' && cur->signature[5] == 'T' && cur->signature[6] == 'R' && cur->signature[7] == ' ' &&
				checksum(cur) == true)
				return cur;
			cur = adjust_pointer(cur, 16);
		}
		return 0;
	}
	
	bool acpi::checksum(const rsd_pointer *address)
	{
		// ACPI 1.0 checksum
		uint8_t checksum = 0;
		const uint8_t *Pointer = reinterpret_cast<const uint8_t*>(address);
		for (size_t i = 0;i < 20;i++)
			checksum += Pointer[i];
		if (checksum != 0)return false;
		
		// ACPI 2.0+ checksum
		checksum = 0;
		for (size_t i = 0;i < address->length;i++)
			checksum += Pointer[i];
		if (checksum != 0)return false;
		
		return true;
	}

#endif
