/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libarch/ioport.hpp>
#include <libarch/scoped_lock.hpp>
#include <kernel/x86_shared/serial.hpp>

#ifndef NDEBUG

SINGLETON_INSTANCE(kernel::x86_shared::serial);

// TODO: io_port usage?
#define REG_INTERRUPT_ENABLE                        1


kernel::x86_shared::serial::serial()
  : m_index(-1)
{
  uint16_t *bios_data_area = reinterpret_cast<uint16_t*>(0x400);
  for (size_t i = 0;i < 4;i++)
    m_ports[i] = bios_data_area[i];
}

kernel::x86_shared::serial::~serial()
{
}

bool kernel::x86_shared::serial::set_index(int port_index)
{
  SCOPED_LOCK(m_lock, scope);

  if (port_index > 3)
    return false;
  if (m_ports[port_index] == 0)
    return false;
  m_index = port_index;
  return true;
}

uint16_t kernel::x86_shared::serial::get_used_port()
{
  SCOPED_LOCK(m_lock, scope);

  if (m_index == -1)
    return 0;
  return m_ports[m_index];
}

void kernel::x86_shared::serial::init_used_port()
{
  SCOPED_LOCK(m_lock, scope);

  if (m_index == -1)
    return;

  // Disable all interrupts
  _LIBARCH_out8(m_ports[m_index] + REG_INTERRUPT_ENABLE, 0x00);

  // Set DLAB
  _LIBARCH_out8(m_ports[m_index] + 3, 0x80);

  // Set baud rate first low than high (38400 BPS)
  _LIBARCH_out8(m_ports[m_index] + 0, 0x03);
  _LIBARCH_out8(m_ports[m_index] + 1, 0x00);

  // 8bits, no parity, 1 stop bit, clear DLAB
  _LIBARCH_out8(m_ports[m_index] + 3 , 0x03);

  // FIFO Control Register
  _LIBARCH_out8(m_ports[m_index] + 2 , 0xC7);

  // Turn on DTR, RTS, and OUT2
  _LIBARCH_out8(m_ports[m_index] + 4 , 0x0B);
}

void kernel::x86_shared::serial::put(char c)
{
  SCOPED_LOCK(m_lock, scope);

  if (m_index != -1)
  {
    // Send the character through the wire
    _LIBARCH_out8(m_ports[m_index], c);
    while ((_LIBARCH_in8(m_ports[m_index] + 5) & 0x40) == 0)
      ;

    // NOTE: This is only to force the qemu serial console to do newlines
    if (c == '\n')
    {
      _LIBARCH_out8(m_ports[m_index], 13);
      while ((_LIBARCH_in8(m_ports[m_index] + 5) & 0x40) == 0)
        ;
    }
  }
}

void kernel::x86_shared::serial::get(uint16_t& ser0,
                                     uint16_t& ser1,
                                     uint16_t& ser2,
                                     uint16_t& ser3)
{
  SCOPED_LOCK(m_lock, scope);

  ser0 = m_ports[0];
  ser1 = m_ports[1];
  ser2 = m_ports[2];
  ser3 = m_ports[3];
}

#endif
