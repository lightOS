/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/smp.hpp>
#include <kernel/log.hpp>
#include <kernel/context.hpp>
#include <kernel/scheduler.hpp>
#include <kernel/page_allocator.hpp>
#include <kernel/processor.hpp>
#include <lightOS/spinlock.hpp>
#include <lightOS/c++utils.hpp>
#include <libarch/ioport.hpp>
#include <kernel/x86_shared/apic.hpp>
#include <kernel/x86_shared/idt.hpp>
#include <kernel/x86_shared/gdt.hpp>
#include <kernel/x86_shared/interrupt_controller.hpp>
using namespace std;
using namespace kernel;

#ifdef _LIGHTOS_SMP

static apicId mHighestApicId = 0;
bool smp::mCompatible = false;
lightOS::spinlock smp::mLock;
lightOS::spinlock smp::mLock2(true);
smp::bus_default smp::bus_defaults[] =
{
	{{'I', 'S', 'A', ' ', ' ', ' '}, BUS_ID_ISA, apic::trigger_mode_edge, apic::polarity_active_high},
	{{'P', 'C', 'I', ' ', ' ', ' '}, BUS_ID_PCI, apic::trigger_mode_level, apic::polarity_active_low},
};
vector<pair<size_t,size_t> > smp::mBusIdToBusDefaults;

void smp::ap_main()
{
	// Call architecture specific application processor bootup routine
	smp::arch_ap_main();
	
	// Create a processor object for this processor
	::processor::initialize_this();
	
	LOG("smp: processor #" << dec(::processor::id()) << " bootstrapped");
	
	// Unlock SMP Spinlock
	mLock.unlock();
	
	// Load the IDT
	idt &Idt = idt::instance();
	Idt.load();
	
	// Wait for smp::initialize2()
	mLock2.lock();
	mLock2.unlock();
	
	// Initialize the local APIC (2)
	local_apic::init_lint();
	
	// Load the GDT
	gdt &Gdt = gdt::instance();
	Gdt.load();
	
	// Load the Task Register
	::processor::load_taskregister();
	
	// call the scheduler
	scheduler::schedule(0);
}

void smp::initialize()
{
	/* Search for the multiprocessor floating pointer structure */
	mp_floating_pointer *p;
	/* Search in the first kilobyte of the ebda */
	uint16_t *ebdaSegment = physical_address(reinterpret_cast<uint16_t*>(0x40E));
	p = find(physical_address<mp_floating_pointer*>(reinterpret_cast<mp_floating_pointer*>((*ebdaSegment) * 16)), 0x400);
	if (p == 0)
	{
		/* Search in the last kilobyte of the base memory */
		uint16_t *baseSize = physical_address(reinterpret_cast<uint16_t*>(0x413));
		p = find(physical_address<mp_floating_pointer*>(reinterpret_cast<mp_floating_pointer*>((*baseSize - 1) * 1024)), 0x400);
	}
	if (p == 0)
	{
		/* Search in the BIOS ROM address space */
		p = find(physical_address<mp_floating_pointer*>(reinterpret_cast<mp_floating_pointer*>(0xF0000)), 0x10000);
	}
	
	if (p == 0)
	{
		WARNING("smp: not compliant to the multiprocessor specification");
		return;
	}
	
	LOG("smp: multiprocessor specification v1." << dec(static_cast<size_t>(p->revision)) << " detected");
	
	// Is the configuration table below 1MB?
	if (p->config_table >= 0x100000)
	{
		WARNING("smp: the configuration table is above 1MB");
		return;
	}
	
	// Check whether config table is present? What about standard configuration(s)?
	if (p->features[0] != 0 || p->config_table == 0)
	{
		WARNING("smp: lightOS does not support the default configurations");
		return;
	}
	
	config_table_header *cfgTable = physical_address<config_table_header*>(reinterpret_cast<config_table_header*>(p->config_table));
	
	// Is the MP configuration table header valid?
	if (cfgTable->id != 0x504D4350 ||
		checksum(cfgTable) != true ||
		cfgTable->revision != p->revision)
	{
		WARNING("smp: invalid configuration table");
		return;
	}
	
	// Get the local APIC of this processor, which is of course the bootstrap processor
	local_apic &BSPApic = ::processor::get_local_apic();
	
	// Architecture specific function before the AP bootup
	arch_pre_ap_bootup();
	
	// Go through the base MP configuration table
	size_t iProcessor = 0;
	uint8_t *type = reinterpret_cast<uint8_t*>(physical_address(adjust_pointer(cfgTable, sizeof(config_table_header))));
	for (size_t i = 0;i < cfgTable->entry_count;i++)
	{
		size_t length = 8;
		// NOTE: The table is ordered by the entry type in ascending order
		switch (*type)
		{
			case 0:
			{
				// Processor
				processor *Processor = reinterpret_cast<processor*>(type);
				
				if (Processor->lapic_id > mHighestApicId)
					mHighestApicId = Processor->lapic_id;
				
				LOG("smp: processor #" << dec((size_t)Processor->lapic_id));
				if ((Processor->flags & 0x02) != 0)
				{
					LOG(" (BSP)");
				}
				else
				{
					LOG(" (AP)");
				}
				
				// Is application Processor and usable?
				if ((Processor->flags & 0x02) == 0 &&
					(Processor->flags & 0x01) == 1)
				{
					// Map a kernel stack for the processor
					page_allocator &PageAllocator = page_allocator::instance();
					mLastKernelStack -= 2 * virtual_memory::page_size;
					void *stack_page = PageAllocator.allocate();
					kernel_context::instance().map(	stack_page,
													adjust_pointer(reinterpret_cast<void*>(mLastKernelStack), - virtual_memory::page_size),
													lightOS::context::write | lightOS::context::global);
					
					// Lock SMP Lock
					mLock.lock();
					
					// Assert Init IPI
					BSPApic.send_ipi(	Processor->lapic_id,
										0,
										apic::delivery_mode_init,
										true,
										true);
					
					// Deassert Init IPI
					BSPApic.send_ipi(	Processor->lapic_id,
										0,
										apic::delivery_mode_init,
										false,
										true);
					
					// Delay 10ms
					kernel::processor::msdelay(10);
					
					// Send Startup IPI
					BSPApic.send_ipi(	Processor->lapic_id,
										0x07,
										apic::delivery_mode_startup,
										true,
										false);
					
					// Wait until AP bootup
					mLock.lock();
					mLock.unlock();
				}
				++iProcessor;
				length = 20;
			}break;
			case 1:
			{
				// Bus
				bus *Bus = reinterpret_cast<bus*>(type);
				
				// Add the bus, if possible
				bool found = false;
				for (size_t i = 0;i < (sizeof(bus_defaults) / sizeof(bus_default));i++)
					if (strncmp(Bus->name, bus_defaults[i].name, 6) == 0)
					{
						mBusIdToBusDefaults.push_back(pair<size_t,size_t>(Bus->busId, i));
						found = true;
					}
				
				// Save the name
				char name[7];
				strncpy(name, Bus->name, 6);
				name[6] = '\0';
				
				// An unknown Bus
				if (found == false)
				{
					FATAL("smp: unknown bus: id = 0x" << hex(Bus->busId) << ", name = \"" << name << "\"");
					::processor::halt();
				}
				
				LOG("Bus: id = 0x" << hex(Bus->busId) << ", name = \"" << name << "\"");
			}break;
			case 2:
			{
				// I/O APIC
				io_apic *IOApic = reinterpret_cast<io_apic*>(type);
				
				if (IOApic->id <= mHighestApicId)
				{
					WARNING("smp: Fixing I/O APIC Id (#" << dec(IOApic->id) << ") clash");
					IOApic->id = mHighestApicId++;
				}
				
				// Is I/O APIC usable?
				if ((IOApic->flags & 0x01) == 0x01)
				{
					apicId ApicId = kernel::io_apic::create(reinterpret_cast<uint32_t*>(IOApic->address), IOApic->id);
					kernel::io_apic *apic = kernel::io_apic::get_by_id(ApicId);
					LOG("smp: I/O APIC ID = 0x" << hex(ApicId) << ", Int pins = " << dec(apic->get_int_pins()));
				}
			}break;
			case 3:
			{
				// I/O Interrupt Assignment
				io_interrupt_assignment *IOInt = reinterpret_cast<io_interrupt_assignment*>(type);
				
				// Get the bus type
				size_t bus_type = BUS_ID_NONE;
				for (size_t i = 0;i < mBusIdToBusDefaults.size();i++)
					if (mBusIdToBusDefaults[i].first == IOInt->bus_id)
					{
						bus_type = bus_defaults[mBusIdToBusDefaults[i].second].type;
						break;
					}
				
				// create the bus specific bus_info structure
				void *bus_info = 0;
				if (bus_type == BUS_ID_ISA)
				{
					::io_apic::isa_irq *isa = new ::io_apic::isa_irq;
					isa->irq = IOInt->bus_irq;
					bus_info = isa;
				}
				if (bus_type == BUS_ID_PCI)
				{
					::io_apic::pci_irq *pci = new ::io_apic::pci_irq;
					pci->bus = IOInt->bus_id;
					pci->device = (IOInt->bus_irq >> 2) & 0x1F;
					pci->intn = IOInt->bus_irq & 0x03;
					bus_info = pci;
				}
				
				// Extract polarity/trigger-mode/delivery-mode information
				size_t polarity = get_polarity(	IOInt->flags & 0x03,
												IOInt->bus_id);
				size_t trigger_mode = get_trigger_mode(	(IOInt->flags >> 2) & 0x03,
														IOInt->bus_id);
				size_t delivery_mode = get_delivery_mode(IOInt->type);
				
				// Find the I/O APIC
				::io_apic &ioApic = *::io_apic::get_by_id(IOInt->io_apic_id);
				
				// Write the I/O redirection entry
				ioApic.set_redirection_entry(	IOInt->io_apic_intn,
												delivery_mode,
												trigger_mode,
												polarity,
												bus_type,
												bus_info);
				
				LOG("I/O Interrupt Assignment: type = 0x" << hex(IOInt->type) << ", flags = 0x" << dec(IOInt->flags) <<
            ", busId = 0x" << dec(IOInt->bus_id) << ", busIRQ = 0x" << dec(IOInt->bus_irq));
        LOG("   ioApicId = 0x" << dec(IOInt->io_apic_id) << ", ioApicIntn = 0x" << dec(IOInt->io_apic_intn));
			}break;
			case 4:
			{
				// Local Interrupt Assignment
				local_interrupt_assignment *LocalInt = reinterpret_cast<local_interrupt_assignment*>(type);
				
				size_t polarity = get_polarity(	LocalInt->flags & 0x03,
												LocalInt->bus_id);
				size_t trigger_mode = get_trigger_mode(	(LocalInt->flags >> 2) & 0x03,
														LocalInt->bus_id);
				size_t delivery_mode = get_delivery_mode(LocalInt->type);
				
				// All Local APICs affected?
				if (LocalInt->lapic_id == 0xFF)
					for (size_t i = 0;i < ::processor::count();i++)
					{
						local_apic &lApic = ::processor::get_by_index(i)->get_local_apic();
						lApic.set_lint(	LocalInt->lapic_intn,
										delivery_mode,
										trigger_mode,
										polarity);
					}
				// Only one APIC
				else
				{
					local_apic &lApic = ::processor::get_by_id(LocalInt->lapic_id)->get_local_apic();
					lApic.set_lint(	LocalInt->lapic_intn,
									delivery_mode,
									trigger_mode,
									polarity);
				}
				
				LOG("Local Interrupt Assignment: type = 0x" << hex(LocalInt->type) << ", flags = 0x" <<
            hex(LocalInt->flags) << ", busId = 0x" << hex(LocalInt->bus_id) << ", busIRQ = 0x" <<
            hex(LocalInt->bus_irq) << "  lApicId = 0x" << hex(LocalInt->lapic_id) << ", lApicIntn = 0x" <<
            hex(LocalInt->lapic_intn));
			}break;
		};
		type = adjust_pointer(type, length);
	}
	
	// Architecture specific function after the AP bootup
	arch_post_ap_bootup();
	
	// TODO: Process the extended entries
	
	// Is PIC-Mode implemented and we have to write the IMCR?
	if ((p->features[1] & 0x80) == 0x80)
	{
		LOG("smp: IMCR implemented");
		libarch::out8(0x22, 0x70);
		libarch::out8(0x23, 0x01);
	}
	
	// Switch to I/O APIC mode
	interrupt_controller::set_ioapic_mode();
	
	mCompatible = true;

}
void smp::initialize2()
{
	// Initialize the other processors (2)
	mLock2.unlock();
	
	// Initialize the local APIC (2)
	local_apic::init_lint();
}
smp::mp_floating_pointer *smp::find(mp_floating_pointer *begin, size_t size)
{
	mp_floating_pointer *cur = begin;
	while (reinterpret_cast<uintptr_t>(cur) < (reinterpret_cast<uintptr_t>(begin) + size))
	{
		if (cur->id == 0x5F504D5F && checksum(cur) == true)
			return cur;
		cur = adjust_pointer(cur, 16);
	}
	return 0;
}
bool smp::checksum(const mp_floating_pointer *p)
{
	uint8_t sum = 0;
	for (size_t i = 0;i < (size_t)(p->length * 16);i++)
		sum += reinterpret_cast<const uint8_t*>(p)[i];
	return (sum == 0);
}
bool smp::checksum(const config_table_header *p)
{
	uint8_t sum = 0;
	for (size_t i = 0;i < (size_t)p->base_table_length;i++)
		sum += reinterpret_cast<const uint8_t*>(p)[i];
	if (sum != 0)return false;
	
	sum = p->extended_checksum;
	for (size_t i = 0;i < (size_t)p->extended_table_length;i++)
		sum += reinterpret_cast<const uint8_t*>(p)[p->base_table_length + i];
	if (sum != 0)return false;
	
	return true;
}

#define TRIGGER_MODE_BUS												0
#define TRIGGER_MODE_EDGE												1
#define TRIGGER_MODE_LEVEL												3
#define POLARITY_BUS													0
#define POLARITY_ACTIVE_HIGH											1
#define POLARITY_ACTIVE_LOW												3
#define INTERRUPT_TYPE_INT												0
#define INTERRUPT_TYPE_NMI												1
#define INTERRUPT_TYPE_SMI												2
#define INTERRUPT_TYPE_EXTINT											3

size_t smp::get_delivery_mode(size_t interrupt_type)
{
	if (interrupt_type == INTERRUPT_TYPE_INT)
		return apic::delivery_mode_fixed;
	if (interrupt_type == INTERRUPT_TYPE_NMI)
		return apic::delivery_mode_nmi;
	if (interrupt_type == INTERRUPT_TYPE_SMI)
		return apic::delivery_mode_smi;
	if (interrupt_type == INTERRUPT_TYPE_EXTINT)
		return apic::delivery_mode_extint;
	
	FATAL("smp: unknown interrupt type 0x" << hex(interrupt_type));
	::processor::halt();
	return 0;
}
size_t smp::get_polarity(size_t polarity, size_t busId)
{
	if (polarity == POLARITY_BUS)
	{
		for (size_t i = 0;i < mBusIdToBusDefaults.size();i++)
			if (mBusIdToBusDefaults[i].first == busId)
				return bus_defaults[mBusIdToBusDefaults[i].second].polarity;
	}
	if (polarity == POLARITY_ACTIVE_HIGH)
		return apic::polarity_active_high;
	if (polarity == POLARITY_ACTIVE_LOW)
		return apic::polarity_active_low;
	
	FATAL("smp: unknown interrupt polarity 0x" << hex(polarity));
	::processor::halt();
	return 0;
}

size_t smp::get_trigger_mode(size_t trigger_mode, size_t busId)
{
	if (trigger_mode == TRIGGER_MODE_BUS)
	{
		for (size_t i = 0;i < mBusIdToBusDefaults.size();i++)
			if (mBusIdToBusDefaults[i].first == busId)
				return bus_defaults[mBusIdToBusDefaults[i].second].trigger_mode;
	}
	if (trigger_mode == TRIGGER_MODE_EDGE)
		return apic::trigger_mode_edge;
	if (trigger_mode == TRIGGER_MODE_LEVEL)
		return apic::trigger_mode_level;
	
	FATAL("smp: unknown interrupt trigger mode 0x" << hex(trigger_mode));
	::processor::halt();
	return 0;
}

#endif
