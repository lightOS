/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/log.hpp>
#include <kernel/arch/arch.hpp>
#include <kernel/syscall.hpp>
#include <kernel/scheduler.hpp>
#include <kernel/processor.hpp>
#include <kernel/x86_shared/interrupt_controller.hpp>
using namespace std;
using namespace kernel;

#ifdef _LIGHTOS_V86
  #include <kernel/x86/v86m.hpp>
  #include <kernel/x86/v86.hpp>
#endif

size_t kernelExceptionNr = 0;

void kernel::interrupt(interrupt_stackframe &stackframe)
{
  process *Process = scheduler::get_process();

  if (stackframe.int_number < 32)
  {
    #ifdef _LIGHTOS_V86
      /* Is exception 13 & we are in virtual mode? */
      if (stackframe.int_number == 13 &&
        v86::instance().is_v86_mode() == true)
      {
        virtual_8086_monitor(stackframe);
        return;
      }
    #endif

    /* Get cr2 */
    void *page_fault = processor::get_page_fault();

    /* Did the exception happen while executing kernel code? */
    bool inKernel = stackframe.is_kernel_space();

    /* Increase kernelExceptionNr */
    if (inKernel == true)++kernelExceptionNr;

    /* Is the page fault */
    if (inKernel != true && stackframe.int_number == 14)
    {
      // TODO; Is the pagefault triggered by a nonepresent page?
      /*  Cause by a too small stack? */
      thread *Thread = scheduler::get_thread();
      if (Thread != 0)
      {
        if (Thread->handle_page_fault(page_fault) == true)
          scheduler::schedule(&stackframe);
      }

      // TODO; Is the pagefault triggered by a write access to a readonly page?
      /* Caused by a Copy-on-Write page? */
      process *Process = scheduler::get_process();
      if (Process != 0)
      {
        context &Context = Process->getContext();
        if (Context.copy_on_write_handler(page_fault, *Process) == true)
          scheduler::schedule(&stackframe);
      }
    }

    /* Device not available exception? */
    if (inKernel != true)
    {
      if (processor::handle_fpu_exception(stackframe) == true)
        return;
    }

    /* Output the exception number */
    {
      ERROR_BEGIN(log);
      ERROR2(log, "Exception #" << dec(stackframe.int_number) << " in ");

      /* Output the current process to the screen */
      if (inKernel == true)
      {
        ERROR2(log, "kernel while executing \"");
        #ifdef _LIGHTOS_V86
          if (v86::instance().is_v86_mode() == true)
            ERROR2(log, "virtual-8086-mode");
          else
          {
        #endif
            if (Process == 0)
              ERROR2(log, "idle");
            else
              ERROR2(log, Process->getName().c_str());
        #ifdef _LIGHTOS_V86
          }
        #endif
        ERROR2(log, "\"");
      }
      else
      {
        ERROR2(log, "process \"");
        #ifdef _LIGHTOS_V86
          if (v86::instance().is_v86_mode() == true)
            ERROR2(log, "virtual-8086-mode");
          else
          {
        #endif
            if (Process == 0)
              ERROR2(log, "idle");
            else
              ERROR2(log, Process->getName().c_str());
        #ifdef _LIGHTOS_V86
          }
        #endif
        ERROR2(log, "\"");
      }
      ERROR2(log, " with error code " << bin(stackframe.errorcode, 8) << "b");
    }

    #ifdef _LIGHTOS_V86
      processor::dump(stackframe, (kernelExceptionNr <= 1) ? !v86::instance().is_v86_mode() : false);
    #else
      processor::dump(stackframe, (kernelExceptionNr <= 1) ? true : false);
    #endif

    kernelExceptionNr = 0;

    /* Halt CPU when in kernel mode */
    if (inKernel ||
      #ifdef _LIGHTOS_V86
        v86::instance().is_v86_mode() == true ||
      #endif
      Process == 0)
    {
      FATAL("Halting cpu");
      processor::halt();
    }
    /* Destroy the process when in user mode */
    else
    {
      process::destroy(Process->getId());
      scheduler::schedule(0);
    }
  }
  else if (stackframe.int_number == 0xFF)
  {
    if (stackframe.syscall_number() < MAX_SYSCALL)
      systemcalls[stackframe.syscall_number()](stackframe);
    else
    {
      ERROR("Invalid syscall #" << dec(stackframe.syscall_number()) << " in \"" << Process->getName().c_str() << "\"");
      process::destroy(Process->getId());
      scheduler::schedule(0);
    }
  }
  else
  {
    // Handle the interrupt
    interrupt_controller &InterruptController = interrupt_controller::instance();
    InterruptController.handle_irq(stackframe.int_number, stackframe);
  }
}
