/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <new>
#include <cstring>
#include <libarch/scoped_lock.hpp>
#include <kernel/virtual_memory.hpp>
#include <kernel/x86_shared/console.hpp>
using kernel::x86_shared::console;
using kernel::x86_shared::console_window;
using kernel::x86_shared::console_window_ptr;


SINGLETON_INSTANCE(kernel::x86_shared::console);


static_assert((sizeof(kernel::x86_shared::console_window) +
               kernel::x86_shared::console_window_width * kernel::x86_shared::console_window_height * 2)
              < kernel::virtual_memory::page_size, "kernel::x86_shared::console_window to big for current allocator");


/*
 * Implementation of console_window
 */

void console_window::copy_from(const void* raw_mem)
{
  memcpy(address(),
         raw_mem,
         console_window_width * console_window_height * 2);
}

void console_window::copy_to(void* raw_mem) const
{
  memcpy(raw_mem,
         address(),
         console_window_width * console_window_height * 2);
}

console_window::console_window(console_id_t id,
                               console_window* prev_window,
                               console_window* next_window)
  : m_bCursor(true), m_cursor(0), m_id(id), m_prev_window(prev_window), m_next_window(next_window)
{
  memset(address(),
         0,
         console_window_width * console_window_height * 2);
}

console_window::~console_window()
{
}



/*
 * Implementation of console
 */

console::console()
  : m_first_window(ARCH_NAMESPACE::virtual_memory::kernel_console_window<console_window>()),
    m_last_window(ARCH_NAMESPACE::virtual_memory::kernel_console_window<console_window>()),
    m_current_window(ARCH_NAMESPACE::virtual_memory::kernel_console_window<console_window>()),
    m_video_ram(reinterpret_cast<void*>(0xB8000))
{
  new (m_first_window) console_window(kernel_console_id, 0, 0);
}

console::~console()
{
}

console_window_ptr console::create()
{
  SCOPED_LOCK(m_lock, scope);

  // TODO
}

console_window_ptr console::get(console_id_t id)
{
  SCOPED_LOCK(m_lock, scope);

  console_window* cur = m_first_window;
  while (cur != 0)
  {
    if (cur->id() == id)
      return EXCLUSIVE_POINTER(cur, cur->m_lock);
    cur = cur->m_next_window;
  }
  return console_window_ptr();
}

console_window_ptr console::get()
{
  SCOPED_LOCK(m_lock, scope);

  return EXCLUSIVE_POINTER(m_current_window, m_current_window->m_lock);
}

void console::switch_to(console_window_ptr& cons)
{
  SCOPED_LOCK(m_lock, scope);

  m_current_window = *cons;
  m_current_window->copy_to(m_video_ram);
  // TODO: treat cursor
}
