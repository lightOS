/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/processor.hpp>
#include <kernel/x86_shared/gdt.hpp>
using namespace kernel;

gdt gdt::mInst;

#ifdef _LIGHTOS_V86
	extern void *kernelTSS;
	extern void *v8086TSS;
#endif

void gdt::initialize()
{
	// Calculate the number of descriptors
	mSize = get_descriptor_count();
	
	// Allocate memory for the GDT
	mGdt = new descriptor[mSize];
	
	// Initialize the descriptors
	set_segment_descriptor(0, 0, 0, 0, 0);
	set_segment_descriptor(1, 0, 0xFFFFF, 0x98, 0xC);	// Kernel code
	set_segment_descriptor(2, 0, 0xFFFFF, 0x92, 0xC);	// Kernel data
	set_segment_descriptor(3, 0, 0xFFFFF, 0xF8, 0xC);	// User code
	set_segment_descriptor(4, 0, 0xFFFFF, 0xF2, 0xC);	// User data
	#ifdef _LIGHTOS_V86
		set_segment_descriptor(5, reinterpret_cast<uintptr_t>(&v8086TSS), 0x02088, 0x89, 0x0);
		set_segment_descriptor(6, reinterpret_cast<uintptr_t>(&kernelTSS), 0x00068, 0x89, 0x0);
	#endif
	
	#ifdef _LIGHTOS_V86
		size_t start = 7;
	#else
		size_t start = 5;
	#endif
	for (size_t i = start;i < mSize;i += tss_descriptor_size())
	{
		task_state_segment *tss = new task_state_segment;
		processor *Processor = processor::get_by_index((i - start) / tss_descriptor_size());
		Processor->set_tss(i << 3, tss);
		set_tss_descriptor(i, reinterpret_cast<uintptr_t>(tss));
	}
}
