/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/x86_shared/pic.hpp>
#include <kernel/x86_shared/apic.hpp>
#include <kernel/x86_shared/cmos.hpp>
#include <kernel/x86_shared/interrupt_controller.hpp>
using namespace kernel;

#ifdef _LIGHTOS_SMP
  bool interrupt_controller::mUseIOApic = false;
#endif

interrupt_controller &interrupt_controller::instance()
{
  #ifdef _LIGHTOS_SMP
    if (mUseIOApic == true)
      return apic::instance();
  #endif
  return x86_shared::pic::instance();
}

#ifdef _LIGHTOS_SMP
  void interrupt_controller::set_ioapic_mode()
  {
    x86_shared::pic::instance().mask();
    mUseIOApic = true;
  }
#endif
