/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/log.hpp>
#include <kernel/x86_shared/cmdline.hpp>
#include <kernel/x86_shared/serial.hpp>

bool kernel::x86_shared::cmdline_args(const char* arg)
{
  #ifdef KERNEL_HAS_SERIAL
  if (strncmp(arg, "--serial", 8) == 0)
  {
    // Get the line number
    int line = arg[8] - '0';

    x86_shared::serial& Serial = x86_shared::serial::instance();
    if (line < 0 ||
        line > 3 ||
        !Serial.set_index(line))
    {
      FATAL("Invalid or unavailable serial line number");
    }
    Serial.init_used_port();
  }
  else
  #endif
    return false;

  return true;
}
