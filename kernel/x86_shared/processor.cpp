/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/smp.hpp>
#include <kernel/log.hpp>
#include <kernel/thread.hpp>
#include <kernel/scheduler.hpp>
#include <kernel/processor.hpp>
using namespace kernel;
using namespace std;

bool processor::mFXSAVE = false;
libkernel::thread_id_t processor::lastFpuThread = 0;
#ifdef _LIGHTOS_SMP
	uint64_t processor::mTscDelta = 0;
	lightOS::spinlock processor::mLock;
#endif
vector<processor*> processor::mProcessor;

void processor::initialize()
{
	// FXSAVE/FXSTOR supported?
	uint32_t eax, ebx, ecx, edx;
	cpuid(1, 0, eax, ebx, ecx, edx);
	if (((edx >> 24) & 1) == 1)
		mFXSAVE = true;
	
	// Initialize SSE
	if (mFXSAVE == true)
	{
		// TODO: arch specific function
		// Set Cr4.OSFXSR & Cr4.OSXMMEXCPT
		#ifdef X86
			uint32_t cr4;
			asm volatile("mov %%cr4, %%eax":"=a" (cr4));
			cr4 |= 0x600;
			asm volatile("mov %%eax, %%cr4"::"a" (cr4));
		#endif
		#ifdef X86_64
			uint64_t cr4;
			asm volatile("mov %%cr4, %%rax":"=a" (cr4));
			cr4 |= 0x600;
			asm volatile("mov %%rax, %%cr4"::"a" (cr4));
		#endif
	}
	
	// Create an processor object for this processor
	processor::initialize_this();
	
	#ifdef _LIGHTOS_SMP
		if (has_local_apic() == true)
			// Calibrate the timers
			calibrate();
	#endif
}

void processor::initialize_this()
{
	LOCK(mLock);
	
		processor *Processor = new processor();
		mProcessor.push_back(Processor);
	
	UNLOCK(mLock);
}

processor *processor::get_by_id(processorId Id)
{
	#ifdef _LIGHTOS_SMP
		if (mProcessor.size() == 1)
			return mProcessor[0];
		
		LOCK(mLock);
		
		for (size_t i = 0;i < mProcessor.size();i++)
			if (mProcessor[i]->mLocalApic->get_id() == Id)
			{
				processor *Processor = mProcessor[i];
				UNLOCK(mLock);
				return Processor;
			}
		
		UNLOCK(mLock);
	#else
		return mProcessor[0];
	#endif
	return 0;
}

processor *processor::get_by_index(size_t i)
{
	LOCK(mLock);
	
		if (i >= mProcessor.size())
		{
			UNLOCK(mLock);
			return 0;
		}
		
		processor *Processor = mProcessor[i];
	
	UNLOCK(mLock);
	return Processor;
}

size_t processor::count()
{
	LOCK(mLock);
	
		size_t count = mProcessor.size();
	
	UNLOCK(mLock);
	return count;;
}

processorId processor::id()
{
	#ifdef _LIGHTOS_SMP
		if (mProcessor.size() == 0)
			return 0;
		
		if (has_local_apic() == false)return 0;
			return local_apic::id();
	#else
		return 0;
	#endif
}

void processor::cpuid(	uint32_t eax,
						uint32_t ecx,
						uint32_t &out_eax,
						uint32_t &out_ebx,
						uint32_t &out_ecx,
						uint32_t &out_edx)
{
	asm volatile(	"cpuid":
					"=a" (out_eax), "=b" (out_ebx), "=c" (out_ecx), "=d" (out_edx):
					"a" (eax), "c" (ecx));
}

bool processor::has_local_apic()
{
	#ifdef _LIGHTOS_SMP
		if (smp::compatible() == true)return true;
	#endif
	
	// Execute a cpuid instruction
	uint32_t eax, ebx, ecx, edx;
	cpuid(1, 0, eax, ebx, ecx, edx);
	if (((edx >> 9) & 0x01) != 0x01)return false;
	
	#ifdef _LIGHTOS_SMP
		// Checks
		if (local_apic::check() == false)return false;
	#endif
	
	return true;
}

uint64_t processor::read_tsc()
{
	uint32_t eax, edx;
	asm volatile(	"rdtsc":
					"=a" (eax), "=d" (edx));
	return (static_cast<uint64_t>(edx) << 32) | eax;
}

uint64_t processor::read_msr(size_t index)
{
	uint32_t eax, edx;
	asm volatile("rdmsr" : "=a" (eax), "=d" (edx) : "c" (index));
	return static_cast<uint64_t>(eax) | (static_cast<uint64_t>(edx) << 32);
}

void processor::write_msr(size_t index, uint64_t value)
{
	uint32_t eax = value, edx = value >> 32;
	asm volatile("wrmsr" :: "a" (eax), "d" (edx), "c" (index));
}

#ifdef _LIGHTOS_SMP

	#include <kernel/x86_shared/pic.hpp>
	#include <kernel/x86_shared/idt.hpp>

	extern "C"
	{
		void calibration();
		void calibration_handler();
		// NOTE: Only used for the spurious interrupt (Occurred on bochs 2.3.5)
		void calibration_handler7();
	}

	void processor::calibrate()
	{
		local_apic &lApic = get()->get_local_apic();
		
		// Set the new irq0 handler
		idt &Idt = idt::instance();
		idt::interrupt_handler tmpHandler = Idt.get_interrupt_gate(0x20);
		idt::interrupt_handler tmpHandler7 = Idt.get_interrupt_gate(0x27);
		Idt.set_interrupt_gate(0x20, calibration_handler);
		Idt.set_interrupt_gate(0x27, calibration_handler7);
		
		// Mask everything except irq0
		x86_shared::pic &Pic = x86_shared::pic::instance();
		Pic.mask();
		Pic.unmask(0);
		
		// Wait for interrupt
		calibration();
		
		// Read the time-stamp counter
		uint64_t tmptsc = processor::read_tsc();
		
		// Start the Local APIC Timer calibration
		lApic.start_calibration();
		
		// End-of-Interrupt
		Pic.eoi(0);
		
		// Wait for interrupt
		calibration();
		
		// Read the time-stamp counter again
		tmptsc = read_tsc() - tmptsc;
		mTscDelta = tmptsc * 100;
		
		// End the Local APIC Timer calibration
		lApic.end_calibration();
		
		// End-of-Interrupt
		Pic.eoi(0);
		
		// Unmask all Interrupts
		Pic.unmask();
		
		// Restore the old irq0/7 handler
		Idt.set_interrupt_gate(0x20, tmpHandler);
		Idt.set_interrupt_gate(0x27, tmpHandler7);
	}

	void processor::msdelay(size_t ms)
	{
		uint64_t newVal = ((mTscDelta * ms) / 1000) + read_tsc();
		while (read_tsc() < newVal);
	}

#endif

bool processor::handle_fpu_exception(interrupt_stackframe &stackframe)
{
	#ifdef X86
		uint32_t cr0 = 0;
		asm volatile("mov %%cr0, %%eax":"=a" (cr0));
	#endif
	#ifdef X86_64
		uint64_t cr0 = 0;
		asm volatile("mov %%cr0, %%rax":"=a" (cr0));
	#endif
	if (stackframe.int_number == 7 &&
		(cr0 & 0x08) == 0x08)
	{
		asm volatile("clts");
		
		thread *Thread = scheduler::get_thread();
		if (Thread->getId() != lastFpuThread)
		{
			if (lastFpuThread != 0)
			{
				thread *lastThread = process::get_thread(lastFpuThread);
				if (lastThread != 0)
				{
					if (mFXSAVE == true)
					{
						#ifdef X86
							asm volatile("fxsave (%%eax)"::"a"(lastThread->mFpuState));
						#endif
						#ifdef X86_64
							asm volatile("fxsave (%%rax)"::"a"(lastThread->mFpuState));
						#endif
					}
					else
					{
						#ifdef X86
							asm volatile("fsave (%%eax)"::"a"(lastThread->mFpuState));
						#endif
						#ifdef X86_64
							asm volatile("fsave (%%rax)"::"a"(lastThread->mFpuState));
						#endif
					}
				}
			}
			
			if (Thread->mFpuState == 0)
			{
				// Allocate space for the state
				allocate_fpu_state(Thread);
				
				asm volatile("finit");
				
				if (mFXSAVE == true)
				{
					// Init MXCSR
					uint32_t mxcsr = 0x3F000;
					#ifdef X86
						asm volatile("ldmxcsr (%%eax)"::"a" (&mxcsr));
					#endif
					#ifdef X86_64
						asm volatile("ldmxcsr (%%rax)"::"a" (&mxcsr));
					#endif
				}
			}
			else
			{
				if (mFXSAVE == true)
				{
					#ifdef X86
						asm volatile("fxrstor (%%eax)"::"a"(Thread->mFpuState));
					#endif
					#ifdef X86_64
						asm volatile("fxrstor (%%rax)"::"a"(Thread->mFpuState));
					#endif
				}
				else
				{
					#ifdef X86
						asm volatile("frstor (%%eax)"::"a"(Thread->mFpuState));
					#endif
					#ifdef X86_64
						asm volatile("frstor (%%rax)"::"a"(Thread->mFpuState));
					#endif
				}
			}
			lastFpuThread = Thread->getId();
		}
		return true;
	}
	return false;
}

void processor::load_taskregister(uint16_t selector)
{
	processor *Processor = processor::get();
	if (selector == 0)selector = Processor->mTSSSelector;
	asm volatile("ltr %%ax"::"a" (selector));
}

void processor::allocate_fpu_state(thread *Thread)
{
	if (mFXSAVE == true)
	{
		// NOTE: Must be aligned to a 16byte boundary
		Thread->mFpuState2 = ::operator new(512 + 16);
		Thread->mFpuState = reinterpret_cast<void*>((reinterpret_cast<uintptr_t>(Thread->mFpuState2) & (~0x0F)) + 0x10);
	}
	else
	{
		Thread->mFpuState = ::operator new(108);
	}
}

void processor::free_fpu_state(thread *Thread)
{
	if (mFXSAVE == true)::operator delete(Thread->mFpuState2);
	else ::operator delete(Thread->mFpuState);
}

void processor::halt()
{
	asm volatile("hlt");
}
