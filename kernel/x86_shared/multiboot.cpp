/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/log.hpp>
#include <kernel/page_allocator.hpp>
#include <kernel/range_allocator.hpp>
#include <kernel/virtual_memory.hpp>
#include <kernel/x86_shared/multiboot.hpp>
#include <kernel/cmdline.hpp>
#include <kernel/x86_shared/cmdline.hpp>
using kernel::virtual_memory;

#define MEMORY_USABLE                            1
#define MEMORY_RESERVED                          2
#define MEMORY_ACPI                              3
#define MEMORY_NVS                               4
#define MEMORY_UNUSABLE                          5

// NOTE: This code assumes that we have more than 16MB (Checked by the kernel.S)
// NOTE: THis code assumes that all modules are below 16MB

void kernel::x86_shared::parse_multiboot(const multiboot& boot)
{
  // Check the multiboot flags
  if ((boot.flags & 0x0D) != 0x0D)
    FATAL("Bootloader is not fully multiboot compliant.");

  // Parse the multiboot commandline
  if ((boot.flags & 0x04) == 0x04)
    kernel::parse_cmdline(reinterpret_cast<char*>(boot.cmdline),
                          kernel::x86_shared::cmdline_args);

  // Bootloader name
  if ((boot.flags & 0x200) != 0)
  {
    LOG("Bootloader: " << reinterpret_cast<const char*>(boot.bootloader_name));
  }

  // Print the memory map to the log
  memory_map* MemoryMap = reinterpret_cast<memory_map*>(boot.mmap);
  #ifndef NDEBUG
    LOG("Memory map:");
    while (reinterpret_cast<uintptr_t>(MemoryMap) < (boot.mmap + boot.mmap_size))
    {
      LOG_BEGIN(log);

      LOG2(log, "  0x" << hex(MemoryMap->address, 16) << " - 0x" << hex(MemoryMap->address  + MemoryMap->length, 16) << " (");
      if (MemoryMap->type == MEMORY_USABLE)
        LOG2(log, "usable");
      else if (MemoryMap->type == MEMORY_RESERVED)
        LOG2(log, "reserved");
      else if (MemoryMap->type == MEMORY_ACPI)
        LOG2(log, "ACPI");
      else if (MemoryMap->type == MEMORY_NVS)
        LOG2(log, "APCI NVS");
      else if (MemoryMap->type == MEMORY_UNUSABLE)
        LOG2(log, "unusable");
      else
        LOG2(log, "unknown: " << dec(MemoryMap->type));
      LOG2(log, ")");

      MemoryMap = adjust_pointer(MemoryMap, MemoryMap->size + 4);
    }
  #endif

  // Fill the page_allocator and the range_allocator
  page_allocator &PageAllocator = page_allocator::instance();
  range_allocator &RangeAllocator = range_allocator::instance();

  // Go through the memory-map and fill the page_allocator
  // NOTE: The page_allocator must be filled _before_ the range_allocator
  size_t capacity = 0;
  MemoryMap = reinterpret_cast<memory_map*>(boot.mmap);
  while (reinterpret_cast<uintptr_t>(MemoryMap) < (boot.mmap + boot.mmap_size))
  {
    if (MemoryMap->type == MEMORY_USABLE)
      for (uintptr_t i = MemoryMap->address;i < (MemoryMap->address + MemoryMap->length);i += virtual_memory::page_size)
        if (i >= (16 * 1024 * 1024)) // Above 16MB
        {
          PageAllocator.free(reinterpret_cast<void*>(i));
          ++capacity;
        }
    MemoryMap = adjust_pointer(MemoryMap, MemoryMap->size + 4);
  }
  PageAllocator.capacity(capacity);

  // Go through the memory-map and fill the range_allocator
  capacity = 0;
  MemoryMap = reinterpret_cast<memory_map*>(boot.mmap);
  while (reinterpret_cast<uintptr_t>(MemoryMap) < (boot.mmap + boot.mmap_size))
  {
    if (MemoryMap->type == 1)
    {
      if (MemoryMap->address < 0x100000)
      {
        // NOTE: assumes that if the start address is below 1Mb the rest is also below 1Mb
        size_t length = MemoryMap->length;
        uintptr_t start = MemoryMap->address;

        // NOTE: The first 4kb are reserved for Virtual-8086-mode and the BIOS data area
        if (start < 0x1000)
        {
          length -= 0x1000 - start;
          start = 0x1000;
        }

        RangeAllocator.free_range(start, length / virtual_memory::page_size);
        capacity += length / virtual_memory::page_size;
      }
      else if (MemoryMap->address < (16 * 1024 * 1024)) // start address below 16MB
      {
        size_t size = MemoryMap->length;
        if ((MemoryMap->address + MemoryMap->length) >= (16 * 1024 * 1024)) // only partially below 16MB
          size -= (MemoryMap->address + MemoryMap->length) - 16 * 1024 * 1024;
        RangeAllocator.free_range(MemoryMap->address, size / virtual_memory::page_size);
        capacity += size / virtual_memory::page_size;
      }
    }
    MemoryMap = adjust_pointer(MemoryMap, MemoryMap->size + 4);
  }

  // Remove the kernel range from the range_allocator
  size_t size = (virtual_memory::kernel_binary_physical_end() - virtual_memory::kernel_binary_physical_begin()) /
                virtual_memory::page_size;
  RangeAllocator.remove_range(virtual_memory::kernel_binary_physical_begin(),
                              size);

  RangeAllocator.capacity(capacity - size);

  // Remove the boot modules from the range_allocator
  kernel::x86_shared::module* Module = reinterpret_cast<kernel::x86_shared::module*>(boot.mod);
  for (size_t i = 0;i < boot.module_count;i++)
  {
    size_t size = (Module[i].end - Module[i].start + virtual_memory::page_size - 1) /
                  virtual_memory::page_size;
    RangeAllocator.remove_range(Module[i].start,
                                size);
  }
}
