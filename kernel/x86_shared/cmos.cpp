/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libarch/ioport.hpp>
#include <kernel/x86_shared/cmos.hpp>
using kernel::x86_shared::cmos;

#define CMOS_ADDRESS                             0x00
#define CMOS_DATA                                0x01

SINGLETON_INSTANCE(kernel::x86_shared::cmos);

cmos::cmos()
  : m_io()
{
  if (m_io.allocate(0x70, 0x02) == false)
  {
    // TODO: Report error
  }
}

cmos::~cmos()
{
}

size_t cmos::second()
{
  return convert(read(0));
}

size_t cmos::minute()
{
  return convert(read(2));
}

size_t cmos::hour()
{
  size_t hour = convert(read(4));
  if (hour > 24)
    hour = 12 + hour - 128;
  return hour;
}

size_t cmos::dayofmonth()
{
  return convert(read(7));
}

size_t cmos::month()
{
  return convert(read(8));
}

size_t cmos::year()
{
  return convert(read(50)) * 100 + convert(read(9));
}

bool cmos::summertime()
{
  return ((read(11) & 1) == 1);
}

void cmos::activate_periodic_irq()
{
  write(0x0A, 0x26);
  uint8_t statusb = read(0x0B);
  write(0x0B, statusb | 0x40);
}

void cmos::ack_periodic_irq()
{
  read(0x0C);
}

uint8_t cmos::read(uint8_t offset)
{
  LOCK(mLock);
  
    if (offset <= 9 ||
        offset == 50)
    {
      m_io.write8(10, CMOS_ADDRESS);
      while ((m_io.read8(CMOS_DATA) & 0x80) == 0x80)
        ;
    }
    m_io.write8(offset, CMOS_ADDRESS);
    uint8_t data = m_io.read8(CMOS_DATA);
  
  UNLOCK(mLock);
  return data;
}

void cmos::write(uint8_t offset, uint8_t value)
{
  LOCK(mLock);
  
    if (offset <= 9 ||
        offset == 50)
    {
      m_io.write8(10, CMOS_ADDRESS);
      while ((m_io.read8(CMOS_DATA) & 0x80) == 0x80)
        ;
    }
    m_io.write8(offset, CMOS_ADDRESS);
    m_io.write8(value, CMOS_DATA);
  
  UNLOCK(mLock);
}

uint8_t cmos::convert(uint8_t byte)
{
  if ((read(11) & 0x04) == 0)
    return ((byte >> 4) * 10 + (byte & 0x0F));
  return byte;
}
