/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <lightOS/c++utils.hpp>
#include <kernel/smp.hpp>
#include <kernel/log.hpp>
#include <kernel/context.hpp>
#include <kernel/scheduler.hpp>
#include <kernel/processor.hpp>
#include <kernel/event_manager.hpp>
#include <kernel/x86_shared/cmos.hpp>
#include <kernel/x86_shared/apic.hpp>
using namespace kernel;
using namespace std;

#ifdef _LIGHTOS_SMP

#define SPURIOUS_VECTOR					0xEF
#define LVT_ERROR_VECTOR				0xFD
#define LVT_LINT0_VECTOR				0xFC
#define LVT_LINT1_VECTOR				0xFB
#define RTC_VECTOR						0xFA
#define LVT_TIMER_VECTOR				0x20

apic apic::mInst;
vector<io_apic*> io_apic::mIoApic;
uint32_t local_apic::mTimerDelta = 0;
volatile uint32_t *local_apic::mBase = 0;

void local_apic::initialize()
{
	// Map the local APIC registers into memory
	kernel_context &KernelContext = kernel_context::instance();
	mBase = reinterpret_cast<volatile uint32_t*>(KernelContext.map_memory_region(reinterpret_cast<void*>(APIC_BASE), lightOS::context::write | lightOS::context::global | lightOS::context::cache_disable));
}

apicId local_apic::id()
{
	return ((read_register(0x20) >> 24) & 0xFF);
}

void local_apic::eoi()
{
	write_register(0xB0, 0x00);
}

void local_apic::send_ipi(	apicId destination,
							uint8_t vector,
							size_t delivery_mode,
							bool bAssert,
							bool bLevelTriggered)
{
	while ((read_register(0x300) & 0x1000) != 0);
	
	write_register(0x310, (destination << 24));
	write_register(0x300, vector | (delivery_mode << 8) | (bAssert ? (1 << 14) : 0) | (bLevelTriggered ? (1 << 15) : 0));
}

bool local_apic::check()
{
	// Get the APIC Base
	uint64_t apic_base_msr = processor::read_msr(msr::apic_base);
	if ((apic_base_msr & 0x800) == 0)
	{
		ERROR("apic: local apic disabled");
		return false;
	}
	if ((apic_base_msr & 0xFFFFFFFFFFFFF000ULL) != APIC_BASE)
	{
		ERROR("apic: local apic at wrong adress (0x" << hex(apic_base_msr & 0xFFFFFFFFFFFFF000ULL) << ")");
		return false;
	}
	return true;
}

void local_apic::start_calibration()
{
	// Set the Initial Count Register
	write_register(0x0380, 0xFFFFFFFF);
	
	// Set the Current Count Register
	write_register(0x0390, 0x00000000);
	
	// Set Divide Configuration Register
	uint32_t tmp = read_register(0x03E0);
	write_register(0x03E0, (tmp & 0xFFFFFFF4) | 0x0B);
	
	// Set the LVT Timer Register (Enable)
	tmp = read_register(0x0320);
	write_register(0x0320, (tmp & 0xFFFCFF00) | 0x20000 | LVT_TIMER_VECTOR);
}

void local_apic::end_calibration()
{
	// Set the LVT Timer Register (Disable)
	uint32_t tmp = read_register(0x0320);
	write_register(0x0320, (tmp & 0xFFFCFF00) | 0x30000 | LVT_TIMER_VECTOR);
	
	// Read the Current Count Register
	mTimerDelta = (0xFFFFFFFF - read_register(0x0390)) * 100;
	
	// Initialize this timer
	init_timer();
}

void local_apic::set_lint(	size_t n,
							size_t delivery_mode,
							size_t trigger_mode,
							size_t polarity)
{
	if (n > 1)
	{
		WARNING("local_apic: LINT" << dec(n) << " does not exist");
		return;
	}
	
	mLINT[n].masked = false;
	mLINT[n].delivery_mode = delivery_mode;
	mLINT[n].trigger_mode = trigger_mode;
	mLINT[n].polarity = polarity;
}

size_t local_apic::get_lint_delivery_mode(size_t n)
{
	if (n > 1)
	{
		WARNING("local_apic: LINT" << dec(n) << " does not exist");
		return 0;
	}
	
	return mLINT[n].delivery_mode;
}

void local_apic::init_lint()
{
	local_apic &lApic = processor::get_local_apic();
	
	// Set LINT0 up
	uint32_t value = 0;
	if (lApic.mLINT[0].masked == true)
		value = 1 << 16;
	else
	{
		if (lApic.mLINT[0].delivery_mode == apic::delivery_mode_fixed)
			value |= LVT_LINT0_VECTOR;
		
		value |= (lApic.mLINT[0].delivery_mode << 8) | (lApic.mLINT[0].polarity << 13) | (lApic.mLINT[0].trigger_mode << 15);
	}
	write_register(0x0350, value);
	
	// Set LINT1 up
	value = 0;
	if (lApic.mLINT[1].masked == true)
		value = 1 << 16;
	else
	{
		if (lApic.mLINT[1].delivery_mode == apic::delivery_mode_fixed)
			value |= LVT_LINT1_VECTOR;
		
		value |= (lApic.mLINT[1].delivery_mode << 8) | (lApic.mLINT[1].polarity << 13) | (lApic.mLINT[1].trigger_mode << 15);
	}
	write_register(0x0360, value);
}

void local_apic::init_timer()
{
	// Set the Initial Count Register (10ms)
	write_register(0x0380, mTimerDelta / 100);
	
	// Set Divide Configuration Register
	uint32_t tmp = read_register(0x03E0);
	write_register(0x03E0, (tmp & 0xFFFFFFF4) | 0x0B);
	
	// Set the LVT Timer Register
	tmp = read_register(0x0320);
	write_register(0x0320, (tmp & 0xFFFCFF00) | 0x20000 | LVT_TIMER_VECTOR);
}

uint32_t local_apic::read_register(uint32_t reg)
{
	return *reinterpret_cast<volatile uint32_t*>(reinterpret_cast<uintptr_t>(mBase) + reg);
}

void local_apic::write_register(uint32_t reg, uint32_t val)
{
	*reinterpret_cast<volatile uint32_t*>(reinterpret_cast<uintptr_t>(mBase) + reg) = val;
}

local_apic::local_apic()
{
	// Enable the APIC and set the Spurious Interrupt Vector (Interrupt 0xEF)
	uint32_t tmp = read_register(0x00F0);
	write_register(0x00F0, (tmp & 0xFFFFFE00) | 0x100 | SPURIOUS_VECTOR);
	
	// Set the Task Priority to 0x00
	tmp = read_register(0x0080);
	write_register(0x0080, (tmp & 0xFFFFFF00));
	
	// Set the LVT Error Register (Interrupt 0xFD)
	tmp = read_register(0x0370);
	write_register(0x0370, (tmp & 0xFFFEEF00) | LVT_ERROR_VECTOR);
	
	// Enable the APIC timer
	// NOTE Only on APs, BSP will activate the Timer within end_calibration
	if (mTimerDelta != 0)
		init_timer();
	
	// Disable LVT LINT0 and LINT1
	mLINT[0].masked = true;
	mLINT[1].masked = true;
	
	// Get APIC ID
	mId = id();
}



void io_apic::set_redirection_entry(size_t n,
									size_t delivery_mode,
									size_t trigger_mode,
									size_t polarity,
									size_t bus_type,
									void *bus_info)
{
	if (n >= mIntPins)
	{
		FATAL("io_apic: no I/O redirection table entry " << dec(n));
		processor::halt();
	}
	
	// Save the data
	mRedirectionTable[n].used = false;
	if (delivery_mode != apic::delivery_mode_fixed)
		mRedirectionTable[n].masked = false;
	mRedirectionTable[n].delivery_mode = delivery_mode;
	mRedirectionTable[n].trigger_mode = trigger_mode;
	mRedirectionTable[n].polarity = polarity;
	mRedirectionTable[n].bus_type = bus_type;
	mRedirectionTable[n].bus_info = bus_info;
	
	// Set the entry
	uint8_t index = 0x10 + 2 * n;
	write_register(index, ((mRedirectionTable[n].masked ? 1 : 0) << 16) | ((mRedirectionTable[n].trigger_mode & 0x01) << 15) | ((mRedirectionTable[n].polarity & 0x01) << 13) | ((mRedirectionTable[n].delivery_mode & 0x07) << 8));
	write_register(index + 1, processor::id() << 24);
}

apicId io_apic::create(uint32_t *base, apicId id)
{
	io_apic *ioapic = new io_apic(base, id);
	mIoApic.push_back(ioapic);
	return ioapic->get_id();
}

io_apic *io_apic::get_by_id(apicId ID)
{
	for (size_t i = 0;i < mIoApic.size();i++)
		if (mIoApic[i]->get_id() == ID)
			return mIoApic[i];
	return 0;
}

uint8_t io_apic::register_isa_irq(uint8_t irq, uint8_t vector)
{
	for (size_t i = 0;i < mIntPins;i++)
		if (mRedirectionTable[i].used == false &&
			mRedirectionTable[i].bus_type == BUS_ID_ISA)
		{
			isa_irq *isa = reinterpret_cast<isa_irq*>(mRedirectionTable[i].bus_info);
			if (isa->irq == irq)
			{
				mRedirectionTable[i].used = true;
				mRedirectionTable[i].vector = vector;
				unmask(i);
				return vector;
			}
		}
	return 0;
}

uint8_t io_apic::register_pci_irq(	uint8_t irq,
									uint8_t intn,
									uint8_t bus,
									uint8_t device,
									uint8_t vector)
{
	// TODO
	return 0;
}

uint32_t io_apic::read_register(uint32_t reg)
{
	*mBase = reg;
	return *adjust_pointer(mBase, 0x10);
}

void io_apic::write_register(uint32_t reg, uint32_t val)
{
	*mBase = reg;
	*adjust_pointer(mBase, 0x10) = val;
}

size_t io_apic::intn_from_vector(uint8_t vector)
{
	for (size_t i = 0;i < mIntPins;i++)
		if (mRedirectionTable[i].vector == vector)
			return i;
	return -1;
}

void io_apic::unmask(size_t n)
{
	if (mRedirectionTable[n].masked == false)return;
	
	// Unmask the entry
	uint8_t index = 0x10 + 2 * n;
	uint32_t value = read_register(index);
	write_register(index, (value & ~(0x000100FF)) | mRedirectionTable[n].vector);
	
	mRedirectionTable[n].masked = false;
}

void io_apic::mask(size_t n)
{
	if (mRedirectionTable[n].masked == true)return;
	
	// Set the entry
	uint8_t index = 0x10 + 2 * n;
	write_register(index, (1 << 16) | read_register(index));
	
	mRedirectionTable[n].masked = true;
}

io_apic::io_apic(uint32_t *base, apicId id)
	: mId(id)
{
	// Map the I/O APIC registers into memory
	kernel_context &KernelContext = kernel_context::instance();
	mBase = reinterpret_cast<uint32_t*>(KernelContext.map_memory_region(reinterpret_cast<void*>(base), lightOS::context::write | lightOS::context::global | lightOS::context::cache_disable));
	
	// Get the APIC ID
	if (((read_register(0) >> 24) & 0x0F) != id)
	{
		write_register(0, (read_register(0) & (~0x0F000000)) | (id << 24));
		
		WARNING("io_apic: setting new I/O APIC Id to #" << dec(id));
	}
	
	// Get the number of interrupt pins
	mIntPins = ((read_register(1) >> 16) & 0xFF) + 1;
	
	// Allocate memory for the redirection table
	mRedirectionTable = new redirection_entry[mIntPins];
	for (size_t i = 0;i < mIntPins;i++)
	{
		mRedirectionTable[i].used = false;
		mRedirectionTable[i].masked = true;
		mRedirectionTable[i].vector = 0x00;
		mRedirectionTable[i].bus_type = BUS_ID_NONE;
		mRedirectionTable[i].bus_info = 0;
	}
}



void apic::init()
{
	// Activate the IRQ8 RTC periodic interrupt
	cmos::instance().activate_periodic_irq();
	
	// Unmask the IRQ8
	// Search through the I/O APICs
	for (size_t i = 0;i < io_apic::count();i++)
	{
		io_apic *IoApic = io_apic::get_by_index(i);
		
		// Does this I/O APIC have an entry for this ISA IRQ?
		if (IoApic->register_isa_irq(8, RTC_VECTOR) != 0)
			return;
	}
}

void apic::handle_irq(uint8_t intnumber, interrupt_stackframe &stackframe)
{
	switch (intnumber)
	{
		// Is it the spurious interrupt vector?
		case SPURIOUS_VECTOR:
		{
			LOG("apic: spurious interrupt");
		}break;
		// Is it the error interrupt vector?
		case LVT_ERROR_VECTOR:
		{
			LOG("apic: error interrupt");
		}break;
		// Is it the local apic timer interrupt vector?
		case LVT_TIMER_VECTOR:
		{
			// Send an End-of-Interrupt
			local_apic::eoi();
			
			// Schedule!
			scheduler::schedule(&stackframe);
		}break;
		case LVT_LINT0_VECTOR:
		{
			LOG("apic[" << dec(processor::id()) << "]: LINT0");
			
			local_apic &lApic = processor::get_local_apic();
			if (lApic.get_lint_delivery_mode(0) == delivery_mode_fixed)
				local_apic::eoi();
		}break;
		case LVT_LINT1_VECTOR:
		{
			LOG("apic[" << dec(processor::id()) << "]: LINT1");
			
			local_apic &lApic = processor::get_local_apic();
			if (lApic.get_lint_delivery_mode(1) == delivery_mode_fixed)
				local_apic::eoi();
		}break;
		case RTC_VECTOR:
		{
			// Handle the ticks
			static bool bIsFirst = false;
			event_manager &eventManager = event_manager::instance();
			if (bIsFirst == false)
				eventManager.tick(976);
			else
				eventManager.tick(977);
			bIsFirst = !bIsFirst;
			
			// Acknoledge the IRQ8
			cmos::instance().ack_periodic_irq();
			
			// Send an End-of-Interrupt
			local_apic::eoi();
		}break;
		default:
		{
			if (intnumber == 0x27)
			{
				LOG("apic[" << dec(processor::id()) << "]: spurious PIC IRQ7");
			}
			else if (intnumber == 0x2F)
			{
				LOG("apic[" << dec(processor::id()) << "]: spurious PIC IRQ15");
			}
			else
			{
				// Mask the IRQ
				// Search through the I/O APICs
				for (size_t i = 0;i < io_apic::count();i++)
				{
					io_apic *IoApic = io_apic::get_by_index(i);
					
					// Get the Intn from the vector
					size_t intn = IoApic->intn_from_vector(intnumber);
					if (intn != static_cast<size_t>(-1))
					{
						IoApic->mask(intn);
						break;
					}
				}
				
				// Handle the interrupt
				event_manager &eventManager = event_manager::instance();
				eventManager.handle_irq(intnumber);
				
				// TODO: remove
				static int count = 0;
				LOG("apic[" << dec(processor::id()) << "]: interrupt vector 0x" << hex(intnumber) << "(" << dec(count) << "), cpu = " << dec(processor::id()));
        ++count;
				
				local_apic::eoi();
			}
		}break;
	}
}

uint8_t apic::register_isa_irq(uint8_t irq, uint8_t priority)
{
	LOG("apic: registering ISA-IRQ" << dec(irq));
	
	// TODO: Calculate an interrupt vector from the priority
	uint8_t vector = priority << 4;
	
	// Search through the I/O APICs
	for (size_t i = 0;i < io_apic::count();i++)
	{
		io_apic *IoApic = io_apic::get_by_index(i);
		
		// Does this I/O APIC have an entry for this ISA IRQ?
		if (IoApic->register_isa_irq(irq, vector) != 0)
			return vector;
	}
	
	return 0;
}

uint8_t apic::register_pci_irq(	uint8_t irq,
								uint8_t intn,
								uint8_t bus,
								uint8_t device,
								uint8_t priority)
{
	// TODO: Calculate an interrupt vector from the priority
	uint8_t vector = priority << 4;
	
	// Search through the I/O APICs
	for (size_t i = 0;i < io_apic::count();i++)
	{
		io_apic *IoApic = io_apic::get_by_index(i);
		
		// Does this I/O APIC have an entry for this PCI device?
		if (IoApic->register_pci_irq(irq, intn, bus, device, vector) != 0)
			return vector;
	}
	
	// Just use the ISA IRQ if no PCI IRQ found
	// NOTE: This is a fix for qemu & bochs
	return register_isa_irq(irq, priority);
}

void apic::acknoledge_irq(uint8_t vector)
{
	static int count = 0;
	LOG("apic: ack irq 0x" << hex(vector) << " (" << dec(count) << ")");
  ++count;
	
	// Unask the IRQ
	// Search through the I/O APICs
	for (size_t i = 0;i < io_apic::count();i++)
	{
		io_apic *IoApic = io_apic::get_by_index(i);
		
		// Get the Intn from the vector
		size_t intn = IoApic->intn_from_vector(vector);
		if (intn != static_cast<size_t>(-1))
		{
			IoApic->unmask(intn);
			break;
		}
	}
}

void apic::unregister_irq(uint8_t vector)
{
	// TODO
}

#endif
