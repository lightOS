/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/log.hpp>
#include <kernel/sharedMemoryManager.hpp>
#include <lightOS/c++utils.hpp>
#include <kernel/processor.hpp>
#include <kernel/page_allocator.hpp>

#include <kernel/virtual_memory.hpp>
using namespace kernel;
using namespace std;
using kernel::virtual_memory;

SINGLETON_INSTANCE(kernel::sharedMemoryManager);

kernel::sharedMemoryManager::sharedMemoryManager()
{
}

kernel::sharedMemoryManager::~sharedMemoryManager()
{
}

pair<libkernel::shared_memory_id_t, void*> sharedMemoryManager::create(process &Process, size_t size)
{
	if (size == 0)
	{
		LOG("sharedMemoryManager: shared-memory region of size 0");
		return pair<libkernel::shared_memory_id_t, void*>(0, 0);
	}
	if (size > virtual_memory::page_size * 1024)
	{
		ERROR("sharedMemoryManager: shared-memory region too big");
		return pair<libkernel::shared_memory_id_t, void*>(0, 0);
	}
	
	void *address = Process.allocate_shared_memory_region();
	if (address == 0)
	{
		ERROR("sharedMemoryManager: no shared-memory region available");
		return pair<libkernel::shared_memory_id_t, void*>(0, 0);
	}
	
	size_t pageCount = size / virtual_memory::page_size;
	if ((size % virtual_memory::page_size) != 0)++pageCount;
	
	LOCK(mLock);
	
		// Create a shared-memory entity
		entity *Memory = new entity(generateSharedMemoryId(), size);
		Memory->list.push_back(make_pair(Process.getId(), address));
		mList.push_back(Memory);
		
		// Allocate the pages and map them into the address space
		KERNEL_CONTEXT_START;
		
			context &Context = Process.getContext();
			page_allocator &PageAllocator = page_allocator::instance();
			for (size_t i = 0;i < pageCount;i++)
			{
				void *page = PageAllocator.allocate();
				Context.map(page,
							adjust_pointer(address, i * virtual_memory::page_size),
							lightOS::context::write | lightOS::context::user);
			}
		
		KERNEL_CONTEXT_END;
	
	UNLOCK(mLock);
	return pair<libkernel::shared_memory_id_t, void*>(Memory->id, address);
}

pair<void*, size_t> sharedMemoryManager::transfer(	process &dstProcess,
													process &srcProcess,
													libkernel::shared_memory_id_t id,
													size_t flags)
{
	if (dstProcess.getId() == srcProcess.getId())
		return pair<void*, size_t>(0, 0);
	
	LOCK(mLock);
	
		// Find the shared-memory entity
		entity *Memory = get_by_id(id);
		if (Memory == 0)
			return pair<void*, size_t>(0, 0);
		
		// Check, whether the src process has this shared memory entity
		vector<pair<libkernel::process_id_t, void*> >::const_iterator i = Memory->list.begin();
		for (;i != Memory->list.end();i++)
			if ((*i).first == srcProcess.getId())
				break;
		if (i == Memory->list.end())
		{
			ERROR("sharedMemoryManager: source process does not own the shared-memory entity");
			return pair<void*, size_t>(0, 0);
		}
		
		// Is it already mapped in the destination process?
		void *address = 0;
		for (vector<pair<libkernel::process_id_t, void*> >::iterator y = Memory->list.begin();y != Memory->list.end();y++)
			if ((*y).first == dstProcess.getId()){address = (*y).second;break;}
		
		if (address == 0)
			// Allocate a shared-memory region
			address = dstProcess.allocate_shared_memory_region();
		
		// Map the shared-memory entity in the destination process
		srcProcess.getContext().copy_shared_memory((*i).second, dstProcess.getContext(), address, flags);
		
		// Add the destination process to the list
		Memory->list.push_back(pair<libkernel::process_id_t, void*>(dstProcess.getId(), address));
		
		pair<void*, size_t> result(address, Memory->size);
	
	UNLOCK(mLock);
	
	// Remove the source process, if the ownership should be transfered
	if (flags == libkernel::shared_memory::transfer_ownership)
		destroy(srcProcess, id);
	
	return result;
}

bool sharedMemoryManager::destroy(process &Process, libkernel::shared_memory_id_t id)
{
	LOCK(mLock);
	
	// Search for the shared-memory entity
	for (vector<entity*>::iterator i = mList.begin();i != mList.end();i++)
		if (i->id == id)
		{
			// Search for the process
			for(vector<pair<libkernel::process_id_t, void*> >::iterator y=i->list.begin();y != i->list.end();y++)
				if ((*y).first == Process.getId())
				{
					// Free or just unmap the shared-memory entity?
					if (i->list.size() == 1)
						Process.getContext().free_shared_memory((*y).second, i->size);
					else
						Process.getContext().unmap_shared_memory((*y).second);
					
					// Free the shared-memory region
					Process.free_shared_memory_region((*y).second);
					
					// Remove the process from the list
					i->list.erase(y);
					break;
				}
			
			// Can we delete the shared-memory entity?
			if (i->list.size() == 0)
			{
				delete *i;
				mList.erase(i);
			}
			
			UNLOCK(mLock);
			return true;
		}
	
	UNLOCK(mLock);
	return false;
}

sharedMemoryManager::entity *sharedMemoryManager::get_by_id(libkernel::shared_memory_id_t id)
{
	for (vector<entity*>::iterator i = mList.begin();i != mList.end();i++)
		if (i->id == id)return *i;
	return 0;
}
