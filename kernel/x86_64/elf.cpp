/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstring>
#include <kernel/elf.hpp>
#include <kernel/log.hpp>
using namespace std;
using namespace kernel;

bool elf::valid()
{
	if (mHeader == 0)return false;
	
	// Check elf header id
	// NOTE: bochs 2.3 fails here
	if (mHeader->id[0] != 0x7F ||
		mHeader->id[1] != 'E' ||
		mHeader->id[2] != 'L' ||
		mHeader->id[3] != 'F')
		return false;
	// Is 64bit elf?
	if (mHeader->id[4] != 2)return false;
	// LSB byte encoding?
	if (mHeader->id[5] != 1)return false;
	// version?
	if (mHeader->id[6] != 1)return false;
	// executable elf?
	if (mHeader->type == 2)
		mIsLibrary = false;
	// shared elf?
	else if (mHeader->type == 3)
		mIsLibrary = true;
	else return false;
	// x86-64 machine?
	if (mHeader->machine != 62)return false;
	// version?
	if (mHeader->version != 1)return false;
	return true;
}

void elf::set64(void *address1,
				void *address2,
				uint64_t value)
{
	if ((reinterpret_cast<uintptr_t>(address1) & 0xFFF) < 0xFF9)
	{
		*(uint64_t*)address1 = value;
		return;
	}
	
	size_t i = 0x1000 - (reinterpret_cast<uintptr_t>(address1) & 0xFFF);
	size_t y = 0;
	for (;y < i;y++)
		*(uint8_t*)(reinterpret_cast<uintptr_t>(address1) + y) = (value >> (8 * y)) & 0xFF;
	for (;y <= 8;y++)
		*(uint8_t*)(reinterpret_cast<uintptr_t>(address2) + y - i) = (value >> (8 * y)) & 0xFF;
}

void elf::do_relocation(elf_relocation *Relocation,
						void *address1,
						void *address2,
						size_t base,
						elf_symbol *Symbol)
{
	ERROR("elf::do_relocation(): unknown relocation entry (0x" << hex(ELF_RELOCATION_TYPE(Relocation)) << ")");
}

void elf::do_relocation_a(	elf_relocation_a *Relocation,
							void *address1,
							void *address2,
							size_t base,
							elf_symbol *Symbol)
{
	switch (ELF_RELOCATION_TYPE(Relocation))
	{
		case R_X86_64_64:
		{
			set64(address1, address2, Symbol->value + Relocation->addend);
		}break;
		case R_X86_64_PC32:
		{
			if (mIsLibrary == false)
			{
				uint64_t res = base + Relocation->addend - reinterpret_cast<uintptr_t>(address1);
				if (res >= 0x80000000)
				{
					ERROR("elf::do_relocation_a(): R_X86_64_PC32: result does not fit into 32bits");
				}
				else set32(address1, address2, res);
			}
			else
			{
				ERROR("elf::do_relocation_a(): R_X86_64_PC32");
			}
		}break;
		case R_X86_64_GLOB_DAT:
		case R_X86_64_JUMP_SLOT:
		{
			if (mIsLibrary == false)
				set64(address1, address2, base);
			else
				set64(address1, address2, Symbol->value);
		}break;
		case R_X86_64_RELATIVE:
		{
			set64(address1, address2, base + Relocation->addend);
		}break;
		case R_X86_64_32:
		{
			if (mIsLibrary == false)
			{
				uint64_t res = base + Relocation->addend;
				if (res >= 0x80000000)
				{
					ERROR("elf::do_relocation_a(): R_X86_64_32: result does not fit into 32bits");
				}
				else set32(address1, address2, res);
			}
			else
			{
				ERROR("elf::do_relocation_a(): R_X86_64_32");
			}
		}break;
		default:
		{
			ERROR("elf::do_relocation_a(): unknown relocation entry (0x" << hex(ELF_RELOCATION_TYPE(Relocation)) << ")");
		}break;
	}
}
