/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/log.hpp>
#include <kernel/context.hpp>
#include <kernel/process.hpp>
#include <kernel/processor.hpp>
#include <kernel/page_allocator.hpp>
#include <kernel/virtual_memory.hpp>
using namespace std;
using namespace kernel;
using kernel::virtual_memory;

// TODO: This could be allot clearer without all those hard-coded values and with more defines, macros,...

#define PAGE_PRESENT                   0x0001
#define PAGE_WRITE                     0x0002
#define PAGE_USER                      0x0004
#define PAGE_WRITE_THROUGH             0x0008
#define PAGE_CACHE_DISABLE             0x0010
#define PAGE_ACCESSED                  0x0020
#define PAGE_DIRTY                     0x0040
#define PAGE_4MB                       0x0080
#define PAGE_GLOBAL                    0x0100
#define PAGE_COPY_ON_WRITE             0x0200
#define PAGE_NX                        0x8000000000000000

inline void invlpg(void *address)
{
  asm volatile("invlpg (%%rax)"::"a"(address));
}
inline void invlall()
{
  asm volatile( "mov %cr3, %rax\n\t"
          "mov %rax, %cr3");
}

context::context()
  : m_handle(), m_heap(reinterpret_cast<void*>(0x400000))
{
  page_allocator &PageAllocator = page_allocator::instance();
  m_handle = PageAllocator.allocate();

  memcpy(::physical_address<void*>(m_handle),
         ::physical_address<void*>(kernel_context::instance().get_handle()),
         0x1000);
  memset(::physical_address<void*>(m_handle),
         0,
         0x800);
}

size_t context::get_flags(size_t flags)
{
  size_t fl = PAGE_PRESENT;
  if ((flags & lightOS::context::write) != 0)fl |= PAGE_WRITE;
  if ((flags & lightOS::context::user) != 0)fl |= PAGE_USER;
  if ((flags & lightOS::context::global) != 0)fl |= PAGE_GLOBAL;
  if ((flags & lightOS::context::write_through) != 0)fl |= PAGE_WRITE_THROUGH;
  if ((flags & lightOS::context::cache_disable) != 0)fl |= PAGE_CACHE_DISABLE;
  if ((flags & lightOS::context::copy_on_write) != 0)fl |= PAGE_COPY_ON_WRITE;
  if ((flags & lightOS::context::execute) == 0)
    if (processor::nx_bit() == true)
      fl |= PAGE_NX;
  return fl;
}

bool context::copy_on_write_handler(void *vaddress, process &Process)
{
  size_t flags = physical_address_flags(vaddress);
  if ((flags & lightOS::context::copy_on_write) != 0)
  {
    // Allocate a new page
    page_allocator &PageAllocator = page_allocator::instance();
    void *newPage = PageAllocator.allocate();
    Process.mPages.push_back(newPage);
    
    // Copy the page
    void *vaddr = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(vaddress) & 0xFFFFFFFFFFFFF000);
    void *toCopy = physical_address(vaddr);
    memcpy( ::physical_address<void*>(newPage),
        ::physical_address<void*>(toCopy),
        virtual_memory::page_size);
    
    // Map the new page
    size_t newFlags = (flags & (~lightOS::context::copy_on_write)) | lightOS::context::write;
    map(newPage, vaddr, newFlags);
    
    return true;
  }
  return false;
}

void *context::physical_address_nolock(void *vaddress)
{
  size_t pageMapEntry = (reinterpret_cast<size_t>(vaddress) >> 39) & 0x1FF;
  size_t pageDirPointerEntry = (reinterpret_cast<size_t>(vaddress) >> 30) & 0x1FF;
  size_t pageDirEntry = (reinterpret_cast<size_t>(vaddress) >> 21) & 0x1FF;
  size_t pageTableEntry = (reinterpret_cast<size_t>(vaddress) >> 12) & 0x1FF;
  
  uint64_t *pageMap = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(m_handle));
  if ((pageMap[pageMapEntry] & PAGE_PRESENT) != PAGE_PRESENT)
    return 0;
  uint64_t *pageDirPointer = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageMap[pageMapEntry] & 0x7FFFFFFFF000));
  if ((pageDirPointer[pageDirPointerEntry] & PAGE_PRESENT) != PAGE_PRESENT)
    return 0;
  uint64_t *pageDir = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirPointer[pageDirPointerEntry] & 0x7FFFFFFFF000));
  if ((pageDir[pageDirEntry] & PAGE_PRESENT) != PAGE_PRESENT)
    return 0;
  uint64_t *pageTable = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDir[pageDirEntry] & 0x7FFFFFFFF000));
  if ((pageTable[pageTableEntry] & PAGE_PRESENT) != PAGE_PRESENT)
    return 0;
  return reinterpret_cast<void*>((pageTable[pageTableEntry] & 0x7FFFFFFFF000) | (reinterpret_cast<uint64_t>(vaddress) & 0xFFF));
}

size_t context::physical_address_flags(void *vaddress)
{
  size_t pageMapEntry = (reinterpret_cast<size_t>(vaddress) >> 39) & 0x1FF;
  size_t pageDirPointerEntry = (reinterpret_cast<size_t>(vaddress) >> 30) & 0x1FF;
  size_t pageDirEntry = (reinterpret_cast<size_t>(vaddress) >> 21) & 0x1FF;
  size_t pageTableEntry = (reinterpret_cast<size_t>(vaddress) >> 12) & 0x1FF;
  
  uint64_t *pageMap = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(m_handle));
  if ((pageMap[pageMapEntry] & PAGE_PRESENT) != PAGE_PRESENT)
    return 0;
  uint64_t *pageDirPointer = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageMap[pageMapEntry] & 0x7FFFFFFFF000));
  if ((pageDirPointer[pageDirPointerEntry] & PAGE_PRESENT) != PAGE_PRESENT)
    return 0;
  uint64_t *pageDir = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirPointer[pageDirPointerEntry] & 0x7FFFFFFFF000));
  if ((pageDir[pageDirEntry] & PAGE_PRESENT) != PAGE_PRESENT)
    return 0;
  uint64_t *pageTable = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDir[pageDirEntry] & 0x7FFFFFFFF000));
  if ((pageTable[pageTableEntry] & PAGE_PRESENT) != PAGE_PRESENT)
    return 0;
  
  size_t flags = pageTable[pageTableEntry] & 0x800000000FFF;
  
  size_t fl = 0;
  if ((flags & PAGE_WRITE) != 0)fl |= lightOS::context::write;
  if ((flags & PAGE_USER) != 0)fl |= lightOS::context::user;
  if ((flags & PAGE_GLOBAL) != 0)fl |= lightOS::context::global;
  if ((flags & PAGE_WRITE_THROUGH) != 0)fl |= lightOS::context::write_through;
  if ((flags & PAGE_CACHE_DISABLE) != 0)fl |= lightOS::context::cache_disable;
  if ((flags & PAGE_COPY_ON_WRITE) != 0)fl |= lightOS::context::copy_on_write;
  if ((flags & PAGE_NX) == 0 || processor::nx_bit() == false)
      fl |= lightOS::context::execute;
  return fl;
}

void *context::unmap(void *vaddress)
{
  size_t pageMapEntry = (reinterpret_cast<size_t>(vaddress) >> 39) & 0x1FF;
  size_t pageDirPointerEntry = (reinterpret_cast<size_t>(vaddress) >> 30) & 0x1FF;
  size_t pageDirEntry = (reinterpret_cast<size_t>(vaddress) >> 21) & 0x1FF;
  size_t pageTableEntry = (reinterpret_cast<size_t>(vaddress) >> 12) & 0x1FF;
  uint64_t pageDirPointer = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(m_handle))[pageMapEntry];
  if ((pageDirPointer & PAGE_PRESENT) == PAGE_PRESENT)
  {
    pageDirPointer &= 0x7FFFFFFFF000;
    uint64_t pageDir = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirPointer))[pageDirPointerEntry];
    if ((pageDir & PAGE_PRESENT) == PAGE_PRESENT)
    {
      pageDir &= 0x7FFFFFFFF000;
      uint64_t pageTable = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDir))[pageDirEntry];
      if ((pageTable & PAGE_PRESENT) == PAGE_PRESENT)
      {
        pageTable &= 0x7FFFFFFFF000;
        uint64_t page = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageTable))[pageTableEntry];
        ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageTable))[pageTableEntry] = 0;
        invlpg(vaddress);
        return reinterpret_cast<void*>(page & 0x7FFFFFFFF000);
      }
    }
  }
  return 0;
}

void context::map(  void *paddress,
          void *vaddress,
          size_t flags)
{
  size_t fl = get_flags(flags);
  size_t pageMapEntry = (reinterpret_cast<size_t>(vaddress) >> 39) & 0x1FF;
  size_t pageDirPointerEntry = (reinterpret_cast<size_t>(vaddress) >> 30) & 0x1FF;
  size_t pageDirEntry = (reinterpret_cast<size_t>(vaddress) >> 21) & 0x1FF;
  size_t pageTableEntry = (reinterpret_cast<size_t>(vaddress) >> 12) & 0x1FF;
  
  page_allocator &PageAllocator = page_allocator::instance();
  uint64_t *pageMap = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(m_handle));
  if ((pageMap[pageMapEntry] & PAGE_PRESENT) != PAGE_PRESENT)
  {
    // TODO: Write to every process context
    if (m_handle == kernel_context::instance().m_handle)
    {
      FATAL("TODO: Write kernel context changes to every context");
      processor::halt();
    }
    void *page = PageAllocator.allocate();
    memset(::physical_address<void*>(page), 0, 4096);
    pageMap[pageMapEntry] = reinterpret_cast<uint64_t>(page) | fl | PAGE_WRITE;
  }
  uint64_t *pageDirPointer = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageMap[pageMapEntry] & 0x7FFFFFFFF000));
  if ((pageDirPointer[pageDirPointerEntry] & PAGE_PRESENT) != PAGE_PRESENT)
  {
    void *page = PageAllocator.allocate();
    memset(::physical_address<void*>(page), 0, 4096);
    pageDirPointer[pageDirPointerEntry] = reinterpret_cast<uint64_t>(page) | fl | PAGE_WRITE;
  }
  uint64_t *pageDir = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirPointer[pageDirPointerEntry] & 0x7FFFFFFFF000));
  if ((pageDir[pageDirEntry] & PAGE_PRESENT) != PAGE_PRESENT)
  {
    void *page = PageAllocator.allocate();
    memset(::physical_address<void*>(page), 0, 4096);
    pageDir[pageDirEntry] = reinterpret_cast<size_t>(page) | fl | PAGE_WRITE;
  }
  uint64_t *pageTable = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDir[pageDirEntry] & 0x7FFFFFFFF000));
  pageTable[pageTableEntry] = reinterpret_cast<uint64_t>(paddress) | fl;
  invlpg(vaddress);
}

void context::copy_shared_memory(void *vaddress_src, context &dest, void *vaddress_dest, size_t flags)
{
  uint64_t flold = PAGE_USER;
  if (flags != libkernel::shared_memory::transfer_ownership)flold |= PAGE_PRESENT;
  if (flags != libkernel::shared_memory::mutual_write)flold |= PAGE_WRITE;
  uint64_t flnew = PAGE_PRESENT | PAGE_USER;
  if (flags != libkernel::shared_memory::read_only)flnew |= PAGE_WRITE;
  
  page_allocator &PageAllocator = page_allocator::instance();
  size_t pageMapEntry = (reinterpret_cast<size_t>(vaddress_src) >> 39) & 0x1FF;
  size_t pageDirPointerEntry = (reinterpret_cast<size_t>(vaddress_src) >> 30) & 0x1FF;
  size_t pageDirEntry = (reinterpret_cast<size_t>(vaddress_src) >> 21) & 0x1FF;
  uint64_t pageDirPointerSrc = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(m_handle))[pageMapEntry];
  if ((pageDirPointerSrc & PAGE_PRESENT) == PAGE_PRESENT)
  {
    pageDirPointerSrc &= 0x7FFFFFFFF000;
    uint64_t pageDirSrc = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirPointerSrc))[pageDirPointerEntry];
    if ((pageDirSrc & PAGE_PRESENT) == PAGE_PRESENT)
    {
      pageDirSrc &= 0x7FFFFFFFF000;
      uint64_t pageTableSrc = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirSrc))[pageDirEntry];
      uint64_t pageTableSrc1 = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirSrc))[pageDirEntry + 1];
      if ((pageTableSrc & PAGE_PRESENT) == PAGE_PRESENT)
      {
        // Map the destination page tables
        size_t pageMapEntryDst = (reinterpret_cast<size_t>(vaddress_dest) >> 39) & 0x1FF;
        size_t pageDirPointerEntryDst = (reinterpret_cast<size_t>(vaddress_dest) >> 30) & 0x1FF;
        size_t pageDirEntryDst = (reinterpret_cast<size_t>(vaddress_dest) >> 21) & 0x1FF;
        uint64_t pageDirPointerDst = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(dest.m_handle))[pageMapEntryDst];
        if ((pageDirPointerDst & PAGE_PRESENT) != PAGE_PRESENT)
        {
          void *page = PageAllocator.allocate();
          memset(::physical_address<void*>(page), 0, 4096);
          pageDirPointerDst = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(dest.m_handle))[pageMapEntryDst] = reinterpret_cast<uint64_t>(page) | PAGE_WRITE | PAGE_USER | PAGE_PRESENT;
        }
        pageDirPointerDst &= 0x7FFFFFFFF000;
        uint64_t pageDirDst = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirPointerDst))[pageDirPointerEntryDst];
        if ((pageDirDst & PAGE_PRESENT) != PAGE_PRESENT)
        {
          void *page = PageAllocator.allocate();
          memset(::physical_address<void*>(page), 0, 4096);
          pageDirDst = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirPointerDst))[pageDirPointerEntryDst] = reinterpret_cast<uint64_t>(page) | PAGE_WRITE | PAGE_USER | PAGE_PRESENT;
        }
        pageDirDst &= 0x7FFFFFFFF000;
        
        ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirDst))[pageDirEntryDst] = (pageTableSrc & 0x7FFFFFFFF000) | flnew;
        ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirSrc))[pageDirEntry] = (pageTableSrc & 0x7FFFFFFFF000) | flold;
        if ((pageTableSrc1 & PAGE_PRESENT) == PAGE_PRESENT)
        {
          ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(dest.m_handle))[pageDirEntryDst + 1] = (pageTableSrc1 & 0x7FFFFFFFF000) | flnew;
          ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(m_handle))[pageDirEntry + 1] = (pageTableSrc1 & 0x7FFFFFFFF000) | flold;
        }
        invlall();
      }
    }
  }
}

void context::free_shared_memory(void *vaddress, size_t size)
{
  page_allocator &PageAllocator = page_allocator::instance();
  
  size_t pageCount = size / virtual_memory::page_size;
  if ((size % virtual_memory::page_size) != 0)++pageCount;
  
  size_t pageMapEntry = (reinterpret_cast<size_t>(vaddress) >> 39) & 0x1FF;
  size_t pageDirPointerEntry = (reinterpret_cast<size_t>(vaddress) >> 30) & 0x1FF;
  size_t pageDirEntry = (reinterpret_cast<size_t>(vaddress) >> 21) & 0x1FF;
  uint64_t pageDirPointer = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(m_handle))[pageMapEntry];
  if ((pageDirPointer & PAGE_PRESENT) == PAGE_PRESENT)
  {
    pageDirPointer &= 0x7FFFFFFFF000;
    uint64_t pageDir = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirPointer))[pageDirPointerEntry];
    if ((pageDir & PAGE_PRESENT) == PAGE_PRESENT)
    {
      pageDir &= 0x7FFFFFFFF000;
      uint64_t pageTable = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDir))[pageDirEntry];
      uint64_t pageTable1 = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDir))[pageDirEntry + 1];
      if ((pageTable & PAGE_PRESENT) == PAGE_PRESENT)
      {
        pageTable &= 0x7FFFFFFFF000;
        pageTable1 &= 0x7FFFFFFFF000;
        for (size_t i=0;i < pageCount;i++)
        {
          if (i < 512)
            PageAllocator.free(reinterpret_cast<void*>(::physical_address<uintptr_t*>(reinterpret_cast<uintptr_t*>(pageTable))[i] & 0x7FFFFFFFF000));
          else if (i < 1024)
            PageAllocator.free(reinterpret_cast<void*>(::physical_address<uintptr_t*>(reinterpret_cast<uintptr_t*>(pageTable1))[i] & 0x7FFFFFFFF000));
        }
        PageAllocator.free(reinterpret_cast<void*>(pageTable));
        if (size >= (2 * 1024 * 1024))
          PageAllocator.free(reinterpret_cast<void*>(pageTable1));
        ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDir))[pageDirEntry] = 0;
        ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDir))[pageDirEntry + 1] = 0;
        invlall();
      }
    }
  }
}

void context::unmap_shared_memory(void *vaddress)
{
  size_t pageMapEntry = (reinterpret_cast<size_t>(vaddress) >> 39) & 0x1FF;
  size_t pageDirPointerEntry = (reinterpret_cast<size_t>(vaddress) >> 30) & 0x1FF;
  size_t pageDirEntry = (reinterpret_cast<size_t>(vaddress) >> 21) & 0x1FF;
  uint64_t pageDirPointer = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(m_handle))[pageMapEntry];
  if ((pageDirPointer & PAGE_PRESENT) == PAGE_PRESENT)
  {
    pageDirPointer &= 0x7FFFFFFFF000;
    uint64_t pageDir = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirPointer))[pageDirPointerEntry];
    if ((pageDir & PAGE_PRESENT) == PAGE_PRESENT)
    {
      pageDir &= 0x7FFFFFFFF000;
      ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDir))[pageDirEntry] = 0;
      ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDir))[pageDirEntry + 1] = 0;
      invlall();
    }
  }
}

bool context::create_page_tables(void *page, void *vaddress, size_t flags)
{
  size_t fl = get_flags(flags);
  size_t pageMapEntry = (reinterpret_cast<size_t>(vaddress) >> 39) & 0x1FF;
  size_t pageDirPointerEntry = (reinterpret_cast<size_t>(vaddress) >> 30) & 0x1FF;
  size_t pageDirEntry = (reinterpret_cast<size_t>(vaddress) >> 21) & 0x1FF;
  size_t pageTableEntry = (reinterpret_cast<size_t>(vaddress) >> 12) & 0x1FF;
  
  uint64_t *pageMap = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(m_handle));
  if ((pageMap[pageMapEntry] & PAGE_PRESENT) != PAGE_PRESENT)
  {
    /* TODO: Write to every process context */
    if (m_handle == kernel_context::instance().m_handle)
    {
      FATAL("TODO: Write kernel context changes to every context");
      processor::halt();
    }
    memset(::physical_address<void*>(page), 0, 4096);
    pageMap[pageMapEntry] = reinterpret_cast<uint64_t>(page) | fl;
    return true;
  }
  uint64_t *pageDirPointer = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageMap[pageMapEntry] & 0x7FFFFFFFF000));
  if ((pageDirPointer[pageDirPointerEntry] & PAGE_PRESENT) != PAGE_PRESENT)
  {
    memset(::physical_address<void*>(page), 0, 4096);
    pageDirPointer[pageDirPointerEntry] = reinterpret_cast<uint64_t>(page) | fl;
    return true;
  }
  uint64_t *pageDir = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirPointer[pageDirPointerEntry] & 0x7FFFFFFFF000));
  if ((pageDir[pageDirEntry] & PAGE_PRESENT) != PAGE_PRESENT)
  {
    memset(::physical_address<void*>(page), 0, 4096);
    pageDir[pageDirEntry] = reinterpret_cast<uint64_t>(page) | fl;
    return true;
  }
  uint64_t *pageTable = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDir[pageDirEntry] & 0x7FFFFFFFF000));
  if ((pageTable[pageTableEntry] & PAGE_PRESENT) != PAGE_PRESENT)
  {
    pageTable[pageTableEntry] = reinterpret_cast<uint64_t>(page) | fl;
    invlpg(vaddress);
    return true;
  }
  return false;
}

context::~context()
{
  page_allocator &PageAllocator = page_allocator::instance();
  if (m_handle != 0)
  {
    for (size_t i = 0;i < 256;i++)
    {
      uint64_t pageDirPointer = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(m_handle))[i];
      if ((pageDirPointer & PAGE_PRESENT) == PAGE_PRESENT)
      {
        pageDirPointer &= 0x7FFFFFFFF000;
        for (size_t i0 = 0;i0 < 512;i0++)
        {
          uint64_t pageDir = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDirPointer))[i0];
          if ((pageDir & PAGE_PRESENT) == PAGE_PRESENT)
          {
            pageDir &= 0x7FFFFFFFF000;
            for (size_t i1 = 0;i1 < 512;i1++)
            {
              uint64_t pageTable = ::physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(pageDir))[i1];
              if ((pageTable & PAGE_PRESENT) == PAGE_PRESENT)
                PageAllocator.free(reinterpret_cast<void*>(pageTable & 0x7FFFFFFFF000));
            }
            PageAllocator.free(reinterpret_cast<void*>(pageDir));
          }
        }
        PageAllocator.free(reinterpret_cast<void*>(pageDirPointer));
      }
    }
    PageAllocator.free(m_handle);
  }
}
