/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstdlib>
#include <kernel/log.hpp>
#include <kernel/scheduler.hpp>
#include <kernel/processor.hpp>
using namespace std;
using namespace kernel;

processor::processor()
  : mThread(0)
{
  asm volatile("mov %%rsp, %0":"=m"(mStack));
  mStack = (mStack & 0xFFFFFFFFFFFFF000) + 0x1000;

  #ifdef _LIGHTOS_SMP
    if (has_local_apic() == true)
      mLocalApic = new local_apic();
    else mLocalApic = 0;
  #endif
}

void processor::set_tss(uint16_t selector, task_state_segment *tss)
{
  // Save the TSS selector and the pointer to the TSS
  mTSSSelector = selector;
  mTSS = tss;

  // Setup the TSS
  memset(tss, 0, sizeof(task_state_segment));
  mTSS->rsp0 = mStack;
}

void *processor::get_page_fault()
{
  void *cr2;
  asm volatile("mov %%cr2, %%rax":"=a" (cr2));
  return cr2;
}

void processor::dump(interrupt_stackframe &stackframe, bool stacktrace)
{
  /* Output cs:rip and ss:rsp */
  ERROR("  cs:rip 0x" << hex(stackframe.cs, 4) << ":0x" << hex(stackframe.rip, 16) <<
        "  ss:rsp 0x" << hex(stackframe.ss, 4) << ":0x" << hex(stackframe.rsp, 16));

  /* Page fault? */
  if (stackframe.int_number == 14)
  {
    ERROR("  cr2: " << get_page_fault() <<
          "  cr3: " << context());
  }

  /* Dump the CPU state to the screen */
  ERROR("  rax: 0x" << hex(stackframe.rax, 16) << 
        "  rbx: 0x" << hex(stackframe.rbx, 16) <<
        "  rcx: 0x" << hex(stackframe.rcx, 16));
  ERROR("  rdx: 0x" << hex(stackframe.rdx, 16) <<
        "  rdi: 0x" << hex(stackframe.rdi, 16) <<
        "  rsi: 0x" << hex(stackframe.rsi, 16));
  ERROR("  r8:  0x" << hex(stackframe.r8, 16) <<
        "  r9:  0x" << hex(stackframe.r9, 16) <<
        "  r10: 0x" << hex(stackframe.r10, 16));
  ERROR("  r11: 0x" << hex(stackframe.r11, 16) <<
        "  r12: 0x" << hex(stackframe.r12, 16) <<
        "  r13: 0x" << hex(stackframe.r13, 16));
  ERROR("  r14: 0x" << hex(stackframe.r14, 16) <<
        "  r15: 0x" << hex(stackframe.r15, 16) <<
        "  rbp: 0x" << hex(stackframe.rbp, 16));
  ERROR("  rflags: 0x" << hex(stackframe.rflags, 16));

  if (stacktrace)
  {
    ERROR("stack trace:");
    uint64_t *stack = reinterpret_cast<uint64_t*>(stackframe.rbp);
    while (*stack != 0)
    {
      ERROR("  rip 0x" << hex(*(stack + 1), 16) << " with rsp 0x" << hex(*stack, 16));
      stack = reinterpret_cast<uint64_t*>(*stack);
    }
  }
  else
    ERROR("no stack trace");
}

void *processor::context()
{
  void *ctx;
  asm volatile("mov %%cr3, %%rax":"=a" (ctx));
  return ctx;
}

void processor::context(void *ctx)
{
  asm volatile("mov %%rax, %%cr3"::"a" (ctx));
}

bool processor::nx_bit()
{
  // Execute a cpuid instruction
  uint32_t eax, ebx, ecx, edx;
  cpuid(0x80000001, 0, eax, ebx, ecx, edx);
  return (((edx >> 20) & 0x01) == 0x01);
}
