/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/x86_shared/idt.hpp>
using namespace kernel;

void idt::set_interrupt_gate(	uint8_t number,
								interrupt_handler handler)
{
	mIdt[number].offset0 = reinterpret_cast<uintptr_t>(handler) & 0xFFFF;
	mIdt[number].selector = 0x08;
	mIdt[number].reserved = 0;
	mIdt[number].flags = 0x8E;
	mIdt[number].offset1 = (reinterpret_cast<uintptr_t>(handler) >> 16) & 0xFFFF;
	mIdt[number].offset2 = (reinterpret_cast<uintptr_t>(handler) >> 32) & 0xFFFFFFFF;
	mIdt[number].reserved2 = 0;
}
idt::interrupt_handler idt::get_interrupt_gate(uint8_t number)
{
	uint64_t address = mIdt[number].offset0 | (mIdt[number].offset1 << 16) | (static_cast<uint64_t>(mIdt[number].offset2) << 32);
	return reinterpret_cast<interrupt_handler>(address);
}
void idt::load()
{
	struct
	{
		uint16_t size;
		uint64_t idt;
	} __attribute__((packed)) idtr = {4095, reinterpret_cast<uintptr_t>(&mIdt)};
	
	asm volatile("lidt %0" :: "m"(idtr));
}
