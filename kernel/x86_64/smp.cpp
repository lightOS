/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/smp.hpp>
#include <kernel/context.hpp>
#include <kernel/processor.hpp>
using namespace kernel;

#ifdef _LIGHTOS_SMP

	context *smpContext = 0;
	uintptr_t smp::mLastKernelStack = 0;
	
	void smp::arch_ap_main()
	{
		// Switch to kernel context
		::processor::context(kernel_context::instance().get_handle());
	}
	void smp::arch_pre_ap_bootup()
	{
		smpContext = new context();
		
		// Identity map the trampoline code
		smpContext->map(reinterpret_cast<void*>(0x7000),
						reinterpret_cast<void*>(0x7000),
						lightOS::context::write | lightOS::context::execute);
		
		*physical_address<uint64_t*>(reinterpret_cast<uint64_t*>(0x7FF0)) = reinterpret_cast<uint64_t>(smpContext->get_handle());
	}
	void smp::arch_post_ap_bootup()
	{
		delete smpContext;
	}

#endif
