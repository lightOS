/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/x86_shared/gdt.hpp>
#include <kernel/processor.hpp>
using namespace kernel;

void gdt::load()
{
	struct
	{
		uint16_t size;
		uint64_t gdt;
	} __attribute__((packed)) gdtr = {static_cast<uint16_t>(mSize * 8 - 1), reinterpret_cast<uintptr_t>(mGdt)};
	
	asm volatile("lgdt %0" :: "m"(gdtr));
}
size_t gdt::get_descriptor_count()
{
	return	1 +						// Zero descriptor
			4 +						// Kernel Data & Code, User Data & Code
			2 *	processor::count();	// Thread TSS
}
size_t gdt::tss_descriptor_size()
{
	return 2;
}
void gdt::set_tss_descriptor(	size_t index,
								uintptr_t address)
{
	mGdt[index].x86.limit = sizeof(task_state_segment);
	mGdt[index].x86.address0 = address & 0xFFFF;
	mGdt[index].x86.address1 = (address >> 16) & 0xFF;
	mGdt[index].x86.flags = 0x89;
	mGdt[index].x86.flags_limit = 0x00;
	mGdt[index].x86.address2 = (address >> 24) & 0xFF;
	mGdt[index + 1].x86_64.address3 = (address >> 32) & 0xFFFFFFFF;
	mGdt[index + 1].x86_64.res0 = 0;
}
void gdt::set_segment_descriptor(	size_t index,
									uintptr_t address,
									size_t limit,
									uint8_t flags,
									uint8_t flags2)
{
	mGdt[index].x86.limit = 0;
	mGdt[index].x86.address0 = 0;
	mGdt[index].x86.address1 = 0;
	mGdt[index].x86.flags = flags;
	mGdt[index].x86.flags_limit = 0x20;
	mGdt[index].x86.address2 = 0;
}
