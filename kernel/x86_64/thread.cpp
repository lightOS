/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/thread.hpp>
#include <kernel/scheduler.hpp>
#include <cstring>
using namespace std;
using namespace kernel;

thread::thread(	process &Process,
				uintptr_t entryPoint,
				void *stack,
				size_t stacksize,
				size_t param1,
				size_t param2,
				libkernel::thread_id_t id)
	: tid(id), Process(Process), threadStack(stack), threadStackSize(stacksize), State(run), mFpuState(0), mFpuState2(0)
{
	mCpuState.rax = param1;
	mCpuState.rbx = param2;
	mCpuState.rcx = 0;
	mCpuState.rdx = 0;
	mCpuState.rdi = 0;
	mCpuState.rsi = 0;
	mCpuState.rbp = 0;
	mCpuState.r8 = 0;
	mCpuState.r9 = 0;
	mCpuState.r10 = 0;
	mCpuState.r11 = 0;
	mCpuState.r12 = 0;
	mCpuState.r13 = 0;
	mCpuState.r14 = 0;
	mCpuState.r15 = 0;
	mCpuState.rsp = reinterpret_cast<uint64_t>(stack);
	mCpuState.rflags = 0x202;
	mCpuState.rip = entryPoint;
}
void thread::grantIOAccess()
{
	mCpuState.rflags |= 0x3000;
}
void thread::unblock(libkernel::port_id_t port,
						libkernel::port_id_t source,
						size_t param1,
						size_t param2,
						size_t param3,
						size_t param4)
{
	mCpuState.rax = port;
	mCpuState.rdi = source;
	mCpuState.rsi = param1;
	mCpuState.rdx = param2;
	mCpuState.r8 = param3;
	mCpuState.r9 = param4;
	
	unblock();
}
void thread::set_cpu_state(interrupt_stackframe &frame)
{
	mCpuState.rax = frame.rax;
	mCpuState.rbx = frame.rbx;
	mCpuState.rcx = frame.rcx;
	mCpuState.rdx = frame.rdx;
	mCpuState.rdi = frame.rdi;
	mCpuState.rsi = frame.rsi;
	mCpuState.rbp = frame.rbp;
	mCpuState.r8 = frame.r8;
	mCpuState.r9 = frame.r9;
	mCpuState.r10 = frame.r10;
	mCpuState.r11 = frame.r11;
	mCpuState.r12 = frame.r12;
	mCpuState.r13 = frame.r13;
	mCpuState.r14 = frame.r14;
	mCpuState.r15 = frame.r15;
	mCpuState.rsp = frame.rsp;
	mCpuState.rip = frame.rip;
	mCpuState.rflags = frame.rflags;
}
