/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/context.hpp>
#include <kernel/page_allocator.hpp>
#include <kernel/virtual_memory.hpp>
using kernel::page_allocator;
using kernel::virtual_memory;

// TODO: Remove
#include <kernel/log.hpp>

SINGLETON_INSTANCE(kernel::page_allocator);

page_allocator::page_allocator()
  : m_size(0), m_stack_size(0), m_page_count(0), m_stack(virtual_memory::page_stack_begin<void*>()),
    m_stack_top(virtual_memory::page_stack_begin<void*>())
{
}

page_allocator::~page_allocator()
{
}

void *page_allocator::allocate()
{
  SCOPED_LOCK(m_lock, scope);

  if (UNLIKELY(m_stack_size == 0))
  {
    // TODO: We might want to unmap the pages that are used as the stack itself, we might want to do that at
    //       once
    // TODO: Remove when other parts of the kernel honor this error
    FATAL("page_allocator: No free pages");
    return 0;
  }

  --m_size;
  --m_stack_size;
  return *m_stack++;
}

void page_allocator::free(void *page)
{
  SCOPED_LOCK(m_lock, scope);

  if (UNLIKELY(m_stack == m_stack_top))
  {
    kernel_context &Context = kernel_context::instance();
    if (Context.create_page_tables(page,
                                   adjust_pointer(m_stack, - virtual_memory::page_size),
                                   lightOS::context::write | lightOS::context::global) == true)
    {
      ++m_size;
      return;
    }
    m_stack_top -= page_size_in<void*>::value;
  }

  ++m_size;
  ++m_stack_size;
  *--m_stack = page;
}
