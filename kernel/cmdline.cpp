/*
lightOS kernel
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/cmdline.hpp>
#include <kernel/log.hpp>

static const char* skip_param(const char* cmdline)
{
  // Skip a parameter
  while (*cmdline != '\0' && *cmdline != ' ')
    ++cmdline;
  while (*cmdline != '\0' && *cmdline == ' ')
    ++cmdline;
  return cmdline;
}

void kernel::parse_cmdline(const char* cmdline,
                           bool (*arch_callback)(const char*))
{
  // Skip the kernel name
  cmdline = skip_param(cmdline);

  while (*cmdline != '\0')
  {
    if (!arch_callback(cmdline) &&
        !kernel::cmdline_args(cmdline))
    {
      // Unknown command line option
      FATAL("Unkown commandline option \"" << cmdline << "\".");
    }

    // Skip the processed parameter
    cmdline = skip_param(cmdline);
  }
}

bool kernel::cmdline_args(const char* arg)
{
  // NOTE: architecture independant arguments here
  return false;
}
