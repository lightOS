/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/log.hpp>
#include <kernel/event_manager.hpp>
#include <kernel/x86_shared/interrupt_controller.hpp>
using namespace std;
using namespace kernel;

SINGLETON_INSTANCE(kernel::event_manager);


kernel::event_manager::event_manager()
  : mTicks(0)
{
}

kernel::event_manager::~event_manager()
{
}

size_t event_manager::register_irq(libkernel::port_id_t port,
                                   size_t param1,
                                   size_t param2)
{
  // TODO: We should check whether this irq/params combination is really supposed to be used by this process

  uint8_t bus = param1 & 0xFF;
  uint8_t irq = (param1 >> 8) & 0xFF;
  uint8_t busnr = param2 & 0xFF;
  uint8_t dev = (param2 >> 8) & 0xFF;
  uint8_t intn = (param1 >> 16) & 0xFF;
  uint8_t priority = (param2 >> 24) & 0xFF;

  uint8_t vector = 0;
  interrupt_controller &InterruptController = interrupt_controller::instance();
  if (bus == BUS_ID_ISA)
    vector = InterruptController.register_isa_irq(irq, priority);
  else if (bus == BUS_ID_PCI)
    vector = InterruptController.register_pci_irq(irq, intn, busnr, dev, priority);

  if (vector != 0)
  {
    LOCK(mIRQLock);

      mIRQEvents.push_back(pair<libkernel::port_id_t, size_t>(port, vector));

    UNLOCK(mIRQLock);
  }

  return vector;
}

void event_manager::acknoledge_irq(libkernel::port_id_t port,
                                   size_t irq_id)
{
  LOCK(mIRQLock);

    auto end = mIRQEvents.end();
    for (auto i = mIRQEvents.begin();i != end;i++)
      if ((*i).first == port &&
        (*i).second == irq_id)
      {
        interrupt_controller &InterruptController = interrupt_controller::instance();
        InterruptController.acknoledge_irq(irq_id);
        UNLOCK(mIRQLock);
        return;
      }

  UNLOCK(mIRQLock);
}

void event_manager::unregister_irq(libkernel::port_id_t port,
                                   size_t irq_id)
{
  LOCK(mIRQLock);

    auto end = mIRQEvents.end();
    for (auto i = mIRQEvents.begin();i != end;i++)
      if ((*i).first == port &&
        (*i).second == irq_id)
      {
        interrupt_controller &InterruptController = interrupt_controller::instance();
        InterruptController.unregister_irq((*i).second);
        mIRQEvents.erase(i);
        UNLOCK(mIRQLock);
        return;
      }

  UNLOCK(mIRQLock);
}

void event_manager::handle_irq(uint8_t vector)
{
  LOCK(mIRQLock);

    auto end = mIRQEvents.end();
    for (auto i = mIRQEvents.begin();i != end;i++)
      if ((*i).second == vector)
      {
        libkernel::message_t msg((*i).first, libkernel::message::event, libkernel::event::irq, vector, 0);
        port_manager &PortManager = port_manager::instance();
        PortManager.send(libkernel::message_port::kernel, msg);
      }

  UNLOCK(mIRQLock);
}

void event_manager::register_timer(libkernel::port_id_t port,
                                   uint64_t microseconds,
                                   bool periodic)
{
  LOCK(mLock);

    timer_event event;
    event.port = port;
    event.remaining = microseconds * 1000;
    event.microseconds = microseconds * 1000;
    event.periodic = periodic;
    mTimerEvents.push_back(event);

  UNLOCK(mLock);
}

void event_manager::unregister_timer(libkernel::port_id_t port)
{
  LOCK(mLock);

    std::vector<timer_event>::iterator end = mTimerEvents.end();
    for (std::vector<timer_event>::iterator i = mTimerEvents.begin();i != end;i++)
      if ((*i).port == port)
      {
        mTimerEvents.erase(i);
        UNLOCK(mLock);
        return;
      }

  UNLOCK(mLock);
}

void event_manager::tick(uint64_t microseconds)
{
  LOCK(mLock);

    mTicks += microseconds;

    for (size_t i = 0;i < mTimerEvents.size();)
    {
      if (mTimerEvents[i].remaining < microseconds)
      {
        // Send the message
        libkernel::message_t msg(mTimerEvents[i].port,
                                 libkernel::message::event,
                                 libkernel::event::timer,
                                 0,
                                 0);
        port_manager &PortManager = port_manager::instance();
        PortManager.send(libkernel::message_port::kernel, msg);

        // Is periodic?
        if (mTimerEvents[i].periodic == true)
        {
          mTimerEvents[i].remaining = mTimerEvents[i].microseconds;
          i++;
        }
        else
          mTimerEvents.erase(mTimerEvents.begin() + i);
      }
      else
      {
        mTimerEvents[i].remaining -= microseconds;
        i++;
      }
    }

  UNLOCK(mLock);
}

size_t event_manager::get_ticks()
{
  LOCK(mLock);

    size_t result = mTicks / 1000;

  UNLOCK(mLock);
  return result;
}
