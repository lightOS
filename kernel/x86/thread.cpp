/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstring>
#include <kernel/thread.hpp>
#include <kernel/scheduler.hpp>
using namespace std;
using namespace kernel;

thread::thread(	process &Process,
				uintptr_t entryPoint,
				void *stack,
				size_t stacksize,
				size_t param1,
				size_t param2,
				libkernel::thread_id_t id)
	: tid(id), Process(Process), threadStack(stack), threadStackSize(stacksize), State(run), mFpuState(0), mFpuState2(0)
{
	mCpuState.eax = param1;
	mCpuState.ebx = param2;
	mCpuState.ecx = 0;
	mCpuState.edx = 0;
	mCpuState.edi = 0;
	mCpuState.esi = 0;
	mCpuState.ebp = 0;
	mCpuState.esp = reinterpret_cast<uint32_t>(stack);
	mCpuState.eflags = 0x202;
	mCpuState.eip = entryPoint;
}
void thread::grantIOAccess()
{
	mCpuState.eflags |= 0x3000;
}
void thread::unblock(	libkernel::port_id_t port,
						libkernel::port_id_t source,
						size_t param1,
						size_t param2,
						size_t param3,
						size_t param4)
{
	mCpuState.eax = port;
	mCpuState.ebx = source;
	mCpuState.ecx = param1;
	mCpuState.edx = param2;
	mCpuState.esi = param3;
	mCpuState.edi = param4;
	
	unblock();
}
void thread::set_cpu_state(interrupt_stackframe &frame)
{
	mCpuState.eax = frame.eax;
	mCpuState.ebx = frame.ebx;
	mCpuState.ecx = frame.ecx;
	mCpuState.edx = frame.edx;
	mCpuState.edi = frame.edi;
	mCpuState.esi = frame.esi;
	mCpuState.ebp = frame.ebp;
	mCpuState.esp = frame.esp;
	mCpuState.eip = frame.eip;
	mCpuState.eflags = frame.eflags;
}
