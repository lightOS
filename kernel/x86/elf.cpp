/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/elf.hpp>
#include <cstring>
#include <kernel/log.hpp>
using namespace std;
using namespace kernel;

bool elf::valid()
{
	if (mHeader == 0)return false;
	
	// Check elf header id
	if (mHeader->id[0] != 0x7F ||
		mHeader->id[1] != 'E' ||
		mHeader->id[2] != 'L' ||
		mHeader->id[3] != 'F')
		return false;
	// Is 32bit elf?
	if (mHeader->id[4] != 1)return false;
	// LSB byte encoding?
	if (mHeader->id[5] != 1)return false;
	// version?
	if (mHeader->id[6] != 1)return false;
	// executable elf?
	if (mHeader->type == 2)
		mIsLibrary = false;
	// shared elf?
	else if (mHeader->type == 3)
		mIsLibrary = true;
	else return false;
	// i386 machine?
	if (mHeader->machine != 3)return false;
	// version?
	if (mHeader->version != 1)return false;
	return true;
}

void elf::do_relocation(elf_relocation *Relocation,
						void *address1,
						void *address2,
						size_t base,
						elf_symbol *Symbol)
{
	switch (ELF_RELOCATION_TYPE(Relocation))
	{
		case R_X86_32:
		{
			if (mIsLibrary == false)
				set32(address1, address2, get32(address1, address2) + base);
			else
				set32(address1, address2, get32(address1, address2) + Symbol->value);
		}break;
		case R_X86_PC32:
		{
			set32(address1, address2, get32(address1, address2) + Symbol->value - base - Relocation->address);
		}break;
		case R_X86_JMP_SLOT:
		{
			if (mIsLibrary == false)
				set32(address1, address2, base);
			else
				set32(address1, address2, Symbol->value);
		}break;
		case R_X86_RELATIVE:
		{
			set32(address1, address2, get32(address1, address2) + base);
		}break;
		default:
		{
			ERROR("elf::do_relocation(): unknown relocation entry (0x" << hex(ELF_RELOCATION_TYPE(Relocation)) << ")");
		}break;
	}
}

void elf::do_relocation_a(	elf_relocation_a *Relocation,
							void *address1,
							void *address2,
							size_t base,
							elf_symbol *Symbol)
{
	ERROR("elf::do_relocation_a(): unknown relocation entry (0x" << hex(ELF_RELOCATION_TYPE(Relocation)) << ")");
}
