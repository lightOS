/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstdlib>
#include <kernel/log.hpp>
#include <kernel/thread.hpp>
#include <kernel/process.hpp>
#include <kernel/scheduler.hpp>
#include <kernel/processor.hpp>
using namespace std;
using namespace kernel;

processor::processor()
  : mThread(0)
{
  asm volatile("mov %%esp, %0":"=m"(mStack));
  mStack = (mStack & 0xFFFFF000) + 0x1000;

  #ifdef _LIGHTOS_SMP
    if (has_local_apic() == true)
      mLocalApic = new local_apic();
    else mLocalApic = 0;
  #endif
}

void processor::set_tss(uint16_t selector, task_state_segment *tss)
{
  // Save the TSS selector and the pointer to the TSS
  mTSSSelector = selector;
  mTSS = tss;

  // Setup the TSS
  memset(tss, 0, sizeof(task_state_segment));
  mTSS->ss0 = 0x10;
  mTSS->esp0 = mStack;
}

void *processor::get_page_fault()
{
  void *cr2;
  asm volatile("mov %%cr2, %%eax":"=a" (cr2));
  return cr2;
}

void processor::dump(interrupt_stackframe &stackframe,
                     bool stacktrace)
{
  /* Output cs:eip and ss:esp */
  {
    ERROR_BEGIN(log);
    ERROR2(log, "  cs:eip 0x" << hex(stackframe.cs, 4) << ":0x" << hex(stackframe.eip, 8));
    if (stackframe.cs != 0x08)
      ERROR2(log, "  ss:esp 0x" << hex(stackframe.ss, 4) << ":0x" << hex(stackframe.esp, 8));
  }

  /* Page fault? */
  if (stackframe.int_number == 14)
  {
    ERROR_BEGIN(log);
    ERROR2(log, "  cr2: " << get_page_fault());
    ERROR2(log, "  cr3: " << context());
  }

  /* Dump the CPU state to the screen */
  ERROR("  eax: 0x" << hex(stackframe.eax, 8) << "  ebx: 0x" << hex(stackframe.ebx, 8) <<
        "  ecx: 0x" << hex(stackframe.ecx, 8) << "  edx: 0x" << hex(stackframe.edx, 8));
  ERROR("  edi: 0x" << hex(stackframe.edi, 8) << "  esi: 0x" << hex(stackframe.esi, 8) <<
        "  ebp: 0x" << hex(stackframe.ebp, 8) << "  eflags: 0x" << hex(stackframe.eflags, 8));

  if (stacktrace)
  {
    ERROR("stack trace:");
    uint32_t *stack = reinterpret_cast<uint32_t*>(stackframe.ebp);
    while (*stack != 0)
    {
      ERROR("  eip 0x" << hex(*(stack + 1), 8) << " with esp 0x" << hex(*stack, 8));
      stack = reinterpret_cast<uint32_t*>(*stack);
    }
  }
  else
    ERROR("no stack trace");
}

void *processor::context()
{
  void *ctx;
  asm volatile("mov %%cr3, %%eax":"=a" (ctx));
  return ctx;
}

void processor::context(void *ctx)
{
  asm volatile("mov %%eax, %%cr3"::"a" (ctx));
}

bool processor::nx_bit()
{
  return false;
}
