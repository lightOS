/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstdint>
#include <kernel/log.hpp>
#include <kernel/x86/v86m.hpp>
#include <kernel/processor.hpp>
using namespace std;
using namespace kernel;

#ifdef _LIGHTOS_V86
	void kernel::push16(uint32_t **Esp, uint16_t value)
	{
		uint16_t *stack = reinterpret_cast<uint16_t*>(*Esp);
		*--stack = value;
		*Esp = reinterpret_cast<uint32_t*>(stack);
	}
	uint16_t kernel::pop16(uint32_t **Esp)
	{
		uint16_t *stack = reinterpret_cast<uint16_t*>(*Esp);
		uint16_t result = *stack;
		*Esp = reinterpret_cast<uint32_t*>(stack);
		return result;
	}
	void kernel::virtual_8086_monitor(interrupt_stackframe &frame)
	{
		uint8_t *Eip = reinterpret_cast<uint8_t*>((frame.cs << 4) + frame.eip);
		uint32_t *Esp = reinterpret_cast<uint32_t*>((frame.ss << 4) + frame.esp);
		uint16_t *Ivt = reinterpret_cast<uint16_t*>(0);
		
		switch (*Eip)
		{
			case 0xCD:	// INT n
			{
				++Eip;
				uint8_t Number = *Eip++;
				
				// Stack large enough?
				if (frame.esp < 6)
				{
					FATAL("virtual-8086-monitor: Int n: Stack not large enough\n");
					FATAL("Halting System" << endl);
					processor::halt();
				}
				
				if (Number == 0xEF)
				{
					asm volatile("int $0xEF");
				}
				
				// Push the EFLAGS
				push16(&Esp, frame.eflags & 0xFFFF);
				// Push CS
				push16(&Esp, frame.cs);
				// Push IP
				push16(&Esp, reinterpret_cast<uintptr_t>(Eip));
				
				// Get new CS:EIP from IVT
				frame.cs = *(Ivt + 2 * Number + 1);
				frame.eip = *(Ivt + 2 * Number);
			}break;
			case 0xCF:	// IRET
			{
				// Stack large enough?
				if (frame.esp > 0xFFFA)
				{
					FATAL("virtual-8086-monitor: IRET: Stack not large enough\n");
					FATAL("Halting System" << endl);
					processor::halt();
				}
				
				// Pop IP
				frame.eip = pop16(&Esp);
				// Pop CS
				frame.cs = pop16(&Esp);
				// Pop EFLAGS
				frame.eflags = (frame.eflags & 0xFFFF0000) | pop16(&Esp) | 0x3202;
			}break;
			default:
			{
				FATAL("virtual-8086-monitor: Unknown instruction 0x" << hex << *Eip << '\n');
				FATAL("Halting System" << endl);
				processor::halt();
			}break;
		}
		
		// Set ESP
		frame.esp = reinterpret_cast<uintptr_t>(Esp) - (frame.ss << 4);
	}
#endif
