/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/x86_shared/idt.hpp>
using namespace kernel;

void idt::set_interrupt_gate(	uint8_t number,
								interrupt_handler handler)
{
	mIdt[number].offset0 = reinterpret_cast<uintptr_t>(handler) & 0xFFFF;
	mIdt[number].selector = 0x08;
	mIdt[number].reserved = 0;
	mIdt[number].flags = 0x8E;
	mIdt[number].offset1 = (reinterpret_cast<uintptr_t>(handler) >> 16) & 0xFFFF;
}
idt::interrupt_handler idt::get_interrupt_gate(uint8_t number)
{
	uint32_t address = mIdt[number].offset0 | (mIdt[number].offset1 << 16);
	return reinterpret_cast<interrupt_handler>(address);
}
void idt::set_userspace_interrupt_gate(	uint8_t number,
										interrupt_handler handler)
{
	set_interrupt_gate(number, handler);
	mIdt[number].flags = 0xEE;
}
#ifdef _LIGHTOS_V86
	void idt::set_task_gate(uint8_t number, uint8_t flags, uint16_t selector)
	{
		mIdt[number].selector = selector;
		mIdt[number].flags = flags;
	}
#endif
void idt::load()
{
	struct
	{
		uint16_t size;
		uint32_t idt;
	} __attribute__((packed)) idtr = {2047, reinterpret_cast<uintptr_t>(&mIdt)};
	
	asm volatile("lidt %0" :: "m"(idtr));
}
