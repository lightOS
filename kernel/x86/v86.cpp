/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/x86/v86.hpp>
#include <kernel/x86_shared/idt.hpp>
#include <kernel/x86_shared/gdt.hpp>
using namespace kernel;

#ifdef _LIGHTOS_V86

v86 v86::mInst;

extern "C"
{
	/*! Execute an 8086 interrupt in virtual-8086-mode  */
	bool int86(	uint32_t number,
				uint32_t eax,
				uint32_t ebx,
				uint32_t ecx,
				uint32_t edx,
				uint32_t ds,
				uint32_t es,
				uint32_t edi,
				uint32_t esi);
	
	/*! Get the state of the virtual-8086-mode */
	void getState86(state86 *state);
}

bool v86::int86(uint32_t number,
				uint32_t eax,
				uint32_t ebx,
				uint32_t ecx,
				uint32_t edx,
				uint32_t ds,
				uint32_t es,
				uint32_t edi,
				uint32_t esi)
{
	gdt &Gdt = gdt::instance();
	idt &Idt = idt::instance();
	
	/* Load the virtual-8086-mode TSS */
	processor::load_taskregister(0x28);
	
	/* Clear the busy-bit in this processors TSS deskriptor */
	Gdt.clear_tss_busy_bit(processor::get_taskregister() >> 3);
	
	/* Add a task gate to the kernel task @ int 0xFE */
	Idt.set_task_gate(254, 0xE5, 0x30);
	
	mProcessorId = processor::id();
	bInV86 = true;
	
	/* 8086 Interrupt! */
	bool res = ::int86(number, eax, ebx, ecx, edx, ds, es, edi, esi);
	
	bInV86 = false;
	
	/* Remove the task gate @ int 0xFE */
	Idt.set_task_gate(254, 0x00, 0x00);
	
	/* Clear the busy-bit in the virtual-8086-mode/kernel TSS descriptor */
	Gdt.clear_tss_busy_bit(5);
	Gdt.clear_tss_busy_bit(6);
	
	/* Load the original taskregister */
	processor::load_taskregister();
	
	return res;
}
state86 v86::get_state()
{
	state86 State;
	getState86(&State);
	return State;
}

#endif
