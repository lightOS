/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstring>
#include <cstdint>
#include <kernel/context.hpp>
#include <kernel/process.hpp>
#include <kernel/page_allocator.hpp>
#include <kernel/processor.hpp>

#include <kernel/virtual_memory.hpp>
using namespace std;
using namespace kernel;
using kernel::virtual_memory;

#define PAGE_PRESENT                   0x0001
#define PAGE_WRITE                     0x0002
#define PAGE_USER                      0x0004
#define PAGE_WRITE_THROUGH             0x0008
#define PAGE_CACHE_DISABLE             0x0010
#define PAGE_ACCESSED                  0x0020
#define PAGE_DIRTY                     0x0040
#define PAGE_4MB                       0x0080
#define PAGE_GLOBAL                    0x0100
#define PAGE_COPY_ON_WRITE             0x0200

// TODO: Heap address?
context::context()
  : m_handle(), m_heap(reinterpret_cast<void*>(0x400000))
{
  page_allocator& PageAllocator = page_allocator::instance();
  m_handle = PageAllocator.allocate();

  // TODO: kernel context
  KERNEL_CONTEXT_START;

  // TODO: Delegate this to the kernel_context, we need to lock the kernel_context
  memcpy(m_handle,
         kernel::x86::virtual_memory::kernel_page_directory<void>(),
         virtual_memory::page_size);
  memset(m_handle,
         0,
         0xC00);

  KERNEL_CONTEXT_END;
}

size_t context::get_flags(size_t flags)
{
  size_t fl = PAGE_PRESENT;
  if ((flags & lightOS::context::write) != 0)fl |= PAGE_WRITE;
  if ((flags & lightOS::context::user) != 0)fl |= PAGE_USER;
  if ((flags & lightOS::context::global) != 0)fl |= PAGE_GLOBAL;
  if ((flags & lightOS::context::write_through) != 0)fl |= PAGE_WRITE_THROUGH;
  if ((flags & lightOS::context::cache_disable) != 0)fl |= PAGE_CACHE_DISABLE;
  if ((flags & lightOS::context::copy_on_write) != 0)fl |= PAGE_COPY_ON_WRITE;
  return fl;
}

bool context::copy_on_write_handler(void* vaddress,
                                    process& Process)
{
  size_t flags = physical_address_flags(vaddress);
  if ((flags & lightOS::context::copy_on_write) != 0)
  {
    // Allocate a new page
    page_allocator &PageAllocator = page_allocator::instance();
    void *newPage = PageAllocator.allocate();
    Process.mPages.push_back(newPage);

    KERNEL_CONTEXT_START;

    // Copy the page
    void *vaddr = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(vaddress) & 0xFFFFF000);
    void *toCopy = physical_address(vaddr);
    memcpy( newPage,
        toCopy,
        virtual_memory::page_size);

    // Map the new page
    size_t newFlags = (flags & (~lightOS::context::copy_on_write)) | lightOS::context::write;
    map(newPage, vaddr, newFlags);

    KERNEL_CONTEXT_END;

    return true;
  }
  return false;
}

void* context::physical_address_nolock(void* vaddress)
{
  KERNEL_CONTEXT_START;

  uint32_t physical = 0;
  size_t iDir = reinterpret_cast<size_t>(vaddress) >> 22;
  size_t pageTable = reinterpret_cast<uint32_t*>(m_handle)[iDir];
  if ((pageTable & PAGE_PRESENT) == PAGE_PRESENT)
  {
    size_t iPageTable = (reinterpret_cast<uint32_t>(vaddress) >> 12) & 0x3FF;
    physical = reinterpret_cast<uint32_t*>(pageTable & 0xFFFFF000)[iPageTable] & 0xFFFFF000;
  }

  KERNEL_CONTEXT_END;
  return reinterpret_cast<void*>(physical | (reinterpret_cast<uint32_t>(vaddress) & 0xFFF));
}

size_t context::physical_address_flags(void* vaddress)
{
  KERNEL_CONTEXT_START;

  uint32_t flags = 0;
  size_t iDir = reinterpret_cast<size_t>(vaddress) >> 22;
  size_t pageTable = reinterpret_cast<uint32_t*>(m_handle)[iDir];
  if ((pageTable & PAGE_PRESENT) == PAGE_PRESENT)
  {
    size_t iPageTable = (reinterpret_cast<uint32_t>(vaddress) >> 12) & 0x3FF;
    flags = reinterpret_cast<uint32_t*>(pageTable & 0xFFFFF000)[iPageTable] & 0xFFF;
  }

  KERNEL_CONTEXT_END;

  size_t fl = lightOS::context::execute;
  if ((flags & PAGE_WRITE) != 0)fl |= lightOS::context::write;
  if ((flags & PAGE_USER) != 0)fl |= lightOS::context::user;
  if ((flags & PAGE_GLOBAL) != 0)fl |= lightOS::context::global;
  if ((flags & PAGE_WRITE_THROUGH) != 0)fl |= lightOS::context::write_through;
  if ((flags & PAGE_CACHE_DISABLE) != 0)fl |= lightOS::context::cache_disable;
  if ((flags & PAGE_COPY_ON_WRITE) != 0)fl |= lightOS::context::copy_on_write;
  return fl;
}

void* context::unmap(void* vaddress)
{
  KERNEL_CONTEXT_START;

  size_t iDir = reinterpret_cast<size_t>(vaddress) >> 22;
  size_t pageTable = reinterpret_cast<uint32_t*>(m_handle)[iDir];
  if ((pageTable & PAGE_PRESENT) == PAGE_PRESENT)
  {
    size_t iPageTable = (reinterpret_cast<uint32_t>(vaddress) >> 12) & 0x3FF;
    pageTable &= 0xFFFFF000;
    uint32_t page = reinterpret_cast<uint32_t*>(pageTable)[iPageTable];
    reinterpret_cast<uint32_t*>(pageTable)[iPageTable] = 0;
    return reinterpret_cast<void*>(page & 0xFFFFF000);
  }

  KERNEL_CONTEXT_END;
  return 0;
}

void context::map(void* paddress,
                  void* vaddress,
                  size_t flags)
{
  KERNEL_CONTEXT_START;

  size_t fl = get_flags(flags);

  size_t iDir = reinterpret_cast<size_t>(vaddress) >> 22;
  size_t pageTable = reinterpret_cast<uint32_t*>(m_handle)[iDir];
  if ((pageTable & PAGE_PRESENT) != PAGE_PRESENT)
  {
    // Allocate page table
    page_allocator &PageAllocator = page_allocator::instance();
    void *page = PageAllocator.allocate();
    memset(page, 0, 4096);
    pageTable = reinterpret_cast<uint32_t*>(m_handle)[iDir] = reinterpret_cast<uint32_t>(page) | fl | PAGE_WRITE;

    // If kernel address space, patch all process address spaces
    if (m_handle == kernel_context::instance().m_handle)
    {
      for (size_t i=0;i < process::size();i++)
      {
        process *Process = process::get_process_by_index(i);
        context &Context = Process->getContext();
        reinterpret_cast<uint32_t*>(Context.m_handle)[iDir] = reinterpret_cast<uint32_t>(page) | fl | PAGE_WRITE;
      }
    }
  }
  size_t iPageTable = (reinterpret_cast<uint32_t>(vaddress) >> 12) & 0x3FF;
  reinterpret_cast<uint32_t*>(pageTable & 0xFFFFF000)[iPageTable] = (reinterpret_cast<uint32_t>(paddress) & 0xFFFFF000) | fl;
  vaddress = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(vaddress) + 4096);
  paddress = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(paddress) + 4096);

  KERNEL_CONTEXT_END;
}

void context::copy_shared_memory(void* vaddress_src,
                                 context& dest,
                                 void* vaddress_dest,
                                 size_t flags)
{
  KERNEL_CONTEXT_START;

  size_t flold = PAGE_USER;
  if (flags != libkernel::shared_memory::transfer_ownership)flold |= PAGE_PRESENT;
  if (flags != libkernel::shared_memory::mutual_write)flold |= PAGE_WRITE;
  size_t flnew = PAGE_PRESENT | PAGE_USER;
  if (flags != libkernel::shared_memory::read_only)flnew |= PAGE_WRITE;

  size_t iDirSrc = reinterpret_cast<uint32_t>(vaddress_src) >> 22;
  uint32_t pageTableSrc = reinterpret_cast<uint32_t*>(m_handle)[iDirSrc];
  if ((pageTableSrc & PAGE_PRESENT) == PAGE_PRESENT)
  {
    size_t iDirDest = reinterpret_cast<uint32_t>(vaddress_dest) >> 22;
    reinterpret_cast<uint32_t*>(dest.m_handle)[iDirDest] = (pageTableSrc & 0xFFFFF000) | flnew;
    reinterpret_cast<uint32_t*>(m_handle)[iDirSrc] = (pageTableSrc & 0xFFFFF000) | flold;
  }

  KERNEL_CONTEXT_END;
}

void context::free_shared_memory(void* vaddress,
                                 size_t size)
{
  size_t pageCount = size / 4096;
  if ((size % 4096) != 0)++pageCount;

  KERNEL_CONTEXT_START;

  size_t iDir = reinterpret_cast<size_t>(vaddress) >> 22;
  uint32_t pageTable = reinterpret_cast<uint32_t*>(m_handle)[iDir];
  if ((pageTable & PAGE_PRESENT) == PAGE_PRESENT)
  {
    page_allocator &PageAllocator = page_allocator::instance();
    for (size_t i=0;i < pageCount;i++)
      PageAllocator.free(reinterpret_cast<void*>(reinterpret_cast<uint32_t*>(pageTable & 0xFFFFF000)[i] & 0xFFFFF000));
    PageAllocator.free(reinterpret_cast<void*>(pageTable & 0xFFFFF000));
    reinterpret_cast<uint32_t*>(m_handle)[iDir] = 0;
  }

  KERNEL_CONTEXT_END;
}

bool context::create_page_tables(void* page,
                                 void* vaddress,
                                 size_t flags)
{
  bool result = false;
  KERNEL_CONTEXT_START;

  size_t fl = get_flags(flags);
  uint32_t iDir = reinterpret_cast<uint32_t>(vaddress) >> 22;
  uint32_t pageTable = reinterpret_cast<uint32_t*>(m_handle)[iDir];
  if ((pageTable & PAGE_PRESENT) != PAGE_PRESENT)
  {
    memset(page, 0, 4096);
    reinterpret_cast<uint32_t*>(m_handle)[iDir] = reinterpret_cast<uint32_t>(page) | fl;
    result = true;
  }
  else
  {
    uint32_t iPageTable = (reinterpret_cast<uint32_t>(vaddress) >> 12) & 0x3FF;
    uint32_t pageTableEntry = reinterpret_cast<uint32_t*>(pageTable & 0xFFFFF000)[iPageTable];
    if ((pageTableEntry & PAGE_PRESENT) != PAGE_PRESENT)
    {
      memset(page, 0, 4096);
      reinterpret_cast<uint32_t*>(pageTable & 0xFFFFF000)[iPageTable] = reinterpret_cast<uint32_t>(page) | fl;
      result = true;
    }
  }

  KERNEL_CONTEXT_END;
  return result;
}

void context::unmap_shared_memory(void* vaddress)
{
  KERNEL_CONTEXT_START;

  size_t iDir = reinterpret_cast<uint32_t>(vaddress) >> 22;
  reinterpret_cast<uint32_t*>(m_handle)[iDir] = 0;

  KERNEL_CONTEXT_END;
}

context::~context()
{
  KERNEL_CONTEXT_START;

  page_allocator &PageAllocator = page_allocator::instance();
  if (m_handle != 0)
  {
    for (size_t i = 0;i < 768;i++)
    {
      uint32_t pageTable = reinterpret_cast<uint32_t*>(m_handle)[i];
      if ((pageTable & PAGE_PRESENT) == PAGE_PRESENT)
        PageAllocator.free(reinterpret_cast<void*>(pageTable & 0xFFFFF000));
    }
    PageAllocator.free(m_handle);
  }
}
