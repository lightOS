/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstring>
#include <kernel/log.hpp>
#include <kernel/x86/vbe.hpp>
#include <kernel/context.hpp>
#include <kernel/x86/arch.hpp>
#include <kernel/x86/v86.hpp>
using namespace std;
using namespace kernel;

#ifdef _LIGHTOS_V86

	vbe *vbe::inst = 0;
	
	vbe::vbe()
	{
		KERNEL_CONTEXT_START;
		
		major = 0;
		minor = 0;
		
		infoBlock *info = (infoBlock*)0x200;
		info->signature[0] = 'V';
		info->signature[1] = 'B';
		info->signature[2] = 'E';
		info->signature[3] = '2';
		
		v86 &v = v86::instance();
		
		if (v.int86(0x10, 0x4F00, 0, 0, 0, 0, 0, 0x200, 0) == false)
		{
			ERROR("VBE: Virtual-8086-mode call failed" << endl);
			return;
		}
		
		state86 State = v.get_state();
		if (State.eax != 0x4F)
		{
			ERROR("VBE: not supported (" << hex << "0x" << State.eax << ")" << endl);
			return;
		}
		
		// Save the information
		major = info->version >> 8;
		minor = info->version & 0xFF;
		videoMemory = info->totalMemory * 64 * 1024;
		
		oemName = (char*)(((info->oemName >> 12) & 0xFFFF0) + (info->oemName & 0xFFFF));
		vendorName = (char*)(((info->oemVendorName >> 12) & 0xFFFF0) + (info->oemVendorName & 0xFFFF));
		productName = (char*)(((info->oemProductName >> 12) & 0xFFFF0) + (info->oemProductName & 0xFFFF));
		productRevision = (char*)(((info->oemProductRevision >> 12) & 0xFFFF0) + (info->oemProductRevision & 0xFFFF));
		
		modeInfo *ModeInfo = (modeInfo*)0x500;
		unsigned short *videoMode = (unsigned short*)(((info->videoMode >> 12) & 0xFFFF0) + (info->videoMode & 0xFFFF));
		
		// Save the list of video modes
		for (;*videoMode != 0xFFFF;*videoMode++)
		{
			if (v.int86(0x10, 0x4F01, 0, *videoMode, 0, 0, 0, 0x500, 0) == false)
			{
				ERROR("VBE: Virtual-8086-mode call failed" << endl);
				return;
			}
			
			// Skip not supported modes
			if ((ModeInfo->attributes & 0x80) == 0 ||
				ModeInfo->xResolution < 800 ||
				ModeInfo->yResolution < 600)continue;
			if (ModeInfo->bitsPerPixel != 16 &&
				ModeInfo->bitsPerPixel != 32)continue;
			
			// Save in the list of video modes
			modeInfo *p = new modeInfo;
			memcpy(p, ModeInfo, sizeof(modeInfo));
			modes.push_back(*videoMode);
			modesInfo.push_back(p);
		}
		KERNEL_CONTEXT_END;
	}

	bool vbe::getModeInfo(unsigned int index, videoMode &mode) const
	{
		if (index >= modes.size())return false;
		
		modeInfo *Info = modesInfo[index];
		mode.width = Info->xResolution;
		mode.height = Info->yResolution;
		mode.bpp = Info->bitsPerPixel;
		mode.framebuffer = (void*)Info->physicalBase;
		return true;
	}

	bool vbe::setMode(unsigned int index)
	{
		if (index >= modes.size())return false;
		
		unsigned short mode = modes[index];
		v86 &v = v86::instance();
		if (v.int86(0x10, 0x4F02, (mode & 0x1FF) | 0x4000, 0, 0, 0, 0, 0x200, 0) == false)return false;
		
		return true;
	}

	vbe::~vbe()
	{
	}

#endif
