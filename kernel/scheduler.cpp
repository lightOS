/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/scheduler.hpp>
#include <kernel/processor.hpp>
using namespace std;
using namespace kernel;

#ifdef _LIGHTOS_V86
	#include <kernel/x86/v86.hpp>
#endif

// TODO: Use a list for the threads
#include <kernel/log.hpp>

vector<thread*> scheduler::mList;
vector<thread*> scheduler::mExecList;
#ifdef _LIGHTOS_SMP
	lightOS::spinlock scheduler::mLock;
#endif

void scheduler::add(thread *Thread)
{
	LOCK(mLock);
	mList.push_back(Thread);
	UNLOCK(mLock);
}
void scheduler::remove(thread *Thread)
{
	LOCK(mLock);
	for (std::vector<thread*>::iterator i=mList.begin();i != mList.end();)
		if (*i == Thread){i = mList.erase(i);}
		else ++i;
	for (std::vector<thread*>::iterator i=mExecList.begin();i != mExecList.end();)
		if (*i == Thread){i = mExecList.erase(i);}
		else ++i;
	UNLOCK(mLock);
}
process *scheduler::get_process()
{
	thread *Thread = get_thread();
	
	if (Thread == 0)return 0;
	return &Thread->getProcess();
}
thread *scheduler::get_thread()
{
  // TODO: Bad if we have not yet initialized processor::
	processor &Processor = *processor::get();
	return Processor.get_thread();
}
void scheduler::schedule(interrupt_stackframe *stackframe)
{
	/* Get the processor object for the processor we are currently running on */
	processor &Processor = *processor::get();
	
	/* Save the thread state and add the thread to the queue again */
	thread *thisThread = Processor.get_thread();
	if (stackframe != 0 && thisThread != 0)
		thisThread->set_cpu_state(*stackframe);
	
	/* Erase the thread from the exec list and add it to the normal non-blocking list */
	LOCK(mLock);
	if (thisThread != 0)
		for (std::vector<thread*>::iterator i=mExecList.begin();i != mExecList.end();i++)
			if (*i == thisThread)
			{
				mList.push_back(thisThread);
				mExecList.erase(i);
				break;
			}
	UNLOCK(mLock);
	
	#ifdef _LIGHTOS_V86
		if (v86::instance().is_v86_mode() == true)
		{
			#ifdef _LIGHTOS_SMP
				if (v86::instance().processor_id() != processor::id())
					while (v86::instance().is_v86_mode() == true);
				else
			#endif
			
				executeV8086Thread();
		}
	#endif
	
	/* Get the next thread */
	LOCK(mLock);
	thread *Thread = 0;
	if (mList.size() != 0)
	{
		Thread = mList.front();
		mList.erase(mList.begin());
		mExecList.push_back(Thread);
	}
	UNLOCK(mLock);
	
	/* Set the thread this processor is currently executing */
	Processor.set_thread(Thread);
	
	/* Execute the thread or idle */
	if (Thread != 0)
	{
		//LOG("#" << dec << processor::id() << ": " << Thread->getProcess().getName().c_str() << endl);
		executeThread(	Thread->get_cpu_state(),
						Thread->getProcess().getContext().get_handle());
	}
	else executeIdleThread();
}
