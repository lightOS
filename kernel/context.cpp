/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/context.hpp>
#include <kernel/process.hpp>
#include <kernel/page_allocator.hpp>
#include <kernel/virtual_memory.hpp>
using kernel::context;
using kernel::kernel_context;
using kernel::virtual_memory;


SINGLETON_INSTANCE(kernel::kernel_context);


context::context(context& c)
  : m_handle(), m_heap()
{
  SCOPED_LOCK(c.m_lock, scope);
  m_heap = c.m_heap;
  m_handle = c.m_handle;
  c.m_heap = 0;
  c.m_handle = 0;
}

context::context(kernel::context_handle_t handle,
                 void* heap)
  : m_handle(handle), m_heap(heap)
{
}

// TODO: Locking

void *context::heap_map(void *paddress,
                        size_t pages,
                        size_t flags)
{
  for (size_t i = 0;i < pages;i++)
  {
    map(adjust_pointer(paddress, i * virtual_memory::page_size),
        adjust_pointer(m_heap, i * virtual_memory::page_size),
        flags);
  }

  // Set new heap address
  void *tmp = m_heap;
  m_heap = adjust_pointer(m_heap, pages * virtual_memory::page_size);
  return tmp;
}

void *context::heap_map(size_t pages,
                        size_t flags,
                        process *Process)
{
  page_allocator &PageAllocator = page_allocator::instance();
  for (size_t i = 0;i < pages;i++)
  {
    void *page = PageAllocator.allocate();
    if (Process != 0)Process->mPages.push_back(page);
    map(page,
        adjust_pointer(m_heap, i * virtual_memory::page_size),
        flags);
  }

  // Set new heap address
  void *tmp = m_heap;
  m_heap = adjust_pointer(m_heap, pages * virtual_memory::page_size);
  return tmp;
}

void *kernel_context::map_memory_region(void *paddress, size_t flags)
{
  mLastKernelMemoryRegion = adjust_pointer(mLastKernelMemoryRegion, - virtual_memory::page_size);
  map(paddress,
      mLastKernelMemoryRegion,
      flags);
  return mLastKernelMemoryRegion;
}

void *kernel_context::allocate_memory_region(size_t pages, size_t flags)
{
  page_allocator &PageAllocator = page_allocator::instance();
  mLastKernelMemoryRegion = adjust_pointer(mLastKernelMemoryRegion, - pages * virtual_memory::page_size);
  for (size_t i = 0;i < pages;i++)
  {
    void *page = PageAllocator.allocate();
    map(page,
      adjust_pointer(mLastKernelMemoryRegion, i * virtual_memory::page_size),
      flags);
  }
  return mLastKernelMemoryRegion;
}

void kernel_context::free_memory_region(void *address, size_t pages)
{
  page_allocator &PageAllocator = page_allocator::instance();
  for (size_t i = 0;i < pages;i++)
  {
    void *page = physical_address(adjust_pointer(address, i * virtual_memory::page_size));
    PageAllocator.free(page);
  }

  if (mLastKernelMemoryRegion == address)
    mLastKernelMemoryRegion = adjust_pointer(address, pages * virtual_memory::page_size);
}

kernel_context::kernel_context()
  : context(virtual_memory::kernel_context_handle(), virtual_memory::kernel_binary_end<void>()),
    mLastKernelMemoryRegion(virtual_memory::kernel_memory_region_begin<void>())
{
}

kernel_context::~kernel_context()
{
  m_handle = 0;
}
