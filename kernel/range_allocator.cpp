/*
lightOS kernel
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstdint>
#include <kernel/log.hpp>
#include <kernel/range_allocator.hpp>
#include <kernel/virtual_memory.hpp>
using namespace std;
using kernel::range_allocator;
using kernel::virtual_memory;

SINGLETON_INSTANCE(kernel::range_allocator);

range_allocator::range_allocator()
{
}

range_allocator::~range_allocator()
{
}

// TODO: Locks

void *range_allocator::allocate_range(size_t size,
                                      size_t Type)
{
  list<range> *Container = 0;

  // Which container?
  if (Type == lightOS::memory::below_1mb)Container = &m_below1Mb;
  else if (Type == lightOS::memory::below_16mb)Container = &m_below16Mb;
  else
  {
    ERROR("range_allocator: invalid memory range type");
    return 0;
  }

  // LOCK(mLock);

    // Exact match
    list<range>::iterator i = Container->begin();
    list<range>::iterator best_fit = Container->end();
    for (;i != Container->end();i++)
      if ((*i).size == size)
      {
        void *page = reinterpret_cast<void*>((*i).address);
        Container->erase(i);
        //UNLOCK(mLock);
        return page;
      }
      else if ((*i).size > size &&
          (best_fit == Container->end() || (*i).size < (*best_fit).size))
      {
        best_fit = i;
      }

    // Best fit?
    if (best_fit != Container->end())
    {
      void *page = reinterpret_cast<void*>((*best_fit).address);
      (*best_fit).size -= size;
      (*best_fit).address += size * virtual_memory::page_size;
      //UNLOCK(mLock);
      return page;
    }

  //UNLOCK(mLock);

  ERROR("range_allocator: no memory range found");
  return 0;
}
void range_allocator::free_range(uintptr_t page,
                size_t size)
{
  list<range> *Container = 0;

  // Which container?
  if (page < 0x100000)Container = &m_below1Mb;
  else if (page < 0x1000000)Container = &m_below16Mb;
  else
  {
    ERROR("range_allocator: freeing range 0x" << hex(page) << " " << dec(size) << " pages");
    return;
  }

  //LOCK(mLock);

    // Can we add this somewhere?
    list<range>::iterator i = Container->begin();
    list<range>::iterator before = Container->end();
    list<range>::iterator after = Container->end();
    for (;i != Container->end();i++)
    {
      if ((*i).address == (page + size * virtual_memory::page_size))
        after = i;
      else if (((*i).address + (*i).size * virtual_memory::page_size) == page)
        before = i;
    }

    if (before != Container->end())
    {
      (*before).size += size;
      if (after != Container->end())
      {
        (*before).size += (*after).size;
        Container->erase(after);
      }
    }
    else if (after != Container->end())
    {
      (*after).address = page;
      (*after).size += size;
    }
    else
    {
      // Add it to the end of the list
      range Range;
      Range.size = size;
      Range.address = page;
      Container->push_front(Range);
    }

  //UNLOCK(mLock);
}
void range_allocator::remove_range( uintptr_t page,
                  size_t size)
{
  list<range> *Container = 0;

  // Which container?
  if (page < 0x100000)Container = &m_below1Mb;
  else if (page < 0x1000000)Container = &m_below16Mb;
  else
  {
    ERROR("range_allocator: removing range 0x" << hex(page) << " " << dec(size) << " pages");
    return;
  }

  for (size_t i = 0;i < size;i++)
    remove_page(page + i * virtual_memory::page_size, Container);
}
void range_allocator::remove_page(uintptr_t page, list<range> *Container)
{
  //LOCK(mLock);

    list<range>::iterator i = Container->begin();
    for (;i != Container->end();i++)
    {
      if ((*i).address == page)
      {
        (*i).address = page + virtual_memory::page_size;
        --(*i).size;
        //UNLOCK(mLock);
        return;
      }
      else if (((*i).address + (*i).size * virtual_memory::page_size) == page)
      {
        --(*i).size;
        //UNLOCK(mLock);
        return;
      }
      else if ((*i).address < page && ((*i).address + (*i).size * virtual_memory::page_size) > page)
      {
        size_t newsize = (page - (*i).address) / virtual_memory::page_size;
        range Range;
        Range.address = page + virtual_memory::page_size;
        Range.size = (*i).size - newsize - 1;
        Container->push_back(Range);
        (*i).size = newsize;
        //UNLOCK(mLock);
        return;
      }
    }

  //UNLOCK(mLock);
}
range_allocator::range range_allocator::get_range(size_t n)
{
  //LOCK(mLock);

    list<range>::iterator i;
    if (n < m_below1Mb.size())
      i = m_below1Mb.begin();
    else if ((n -= m_below1Mb.size()) < m_below16Mb.size())
      i = m_below16Mb.begin();

    while (n-- != 0)++i;

    range Range(*i);

  //UNLOCK(mLock);

  return Range;
}
