/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/syscall.hpp>
#include <kernel/scheduler.hpp>
#include <kernel/kernel.hpp>
#include <kernel/page_allocator.hpp>
#include <kernel/range_allocator.hpp>
#include <kernel/process.hpp>
#include <kernel/port_manager.hpp>
#include <kernel/sharedMemoryManager.hpp>
#include <kernel/elf.hpp>
#include <kernel/event_manager.hpp>
#include <kernel/x86_shared/dma.hpp>
#include <kernel/x86_shared/cmos.hpp>
#include <kernel/x86_shared/serial.hpp>
#include <kernel/processor.hpp>
#include <lightOS/lightOS.hpp>
#include <string>
#include <kernel/log.hpp>
#ifdef X86
	#include <kernel/x86/vbe.hpp>
#endif
#include <kernel/virtual_memory.hpp>
using namespace std;
using namespace kernel;

size_t monthnumbers[] = {0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5};

systemcall kernel::systemcalls[] = 
{
	syscall::get_kernel_version,		// 0
	syscall::heap_allocate,				// 1
	syscall::get_memory_info,			// 2
	syscall::get_commandline,			// 3
	syscall::get_time,					// 4
	syscall::get_date,					// 5
	syscall::create_port,				// 6
	syscall::destroy_port,				// 7
	syscall::peek_message,				// 8
	syscall::get_message,				// 9
	syscall::wait_message,				// 10
	syscall::send_message,				// 11
	syscall::port_info,					// 12
	syscall::standard_port,				// 13
	syscall::transfer_port,				// 14
	syscall::add_wait_message,			// 15
	syscall::create_thread,				// 16
	syscall::suspend_thread,			// 17
	syscall::resume_thread,				// 18
	syscall::destroy_thread,			// 19
	syscall::destroy_process,			// 20
	syscall::get_pid,					// 21
	syscall::get_tid,					// 22
	syscall::get_process_count,			// 23
	syscall::get_process_list,			// 24
	syscall::get_process_info,			// 25
	syscall::register_event,			// 26
	syscall::acknoledge_event,			// 27
	syscall::unregister_event,			// 28
	syscall::get_tick_count,			// 29
	syscall::create_shared_memory,		// 30
	syscall::destroy_shared_memory,		// 31
	syscall::range_allocator_count,		// 32
	syscall::enum_range_allocator,		// 33
	0,									// 34
	0,									// 35
	0,									// 36
	syscall::create_specific_port,		// 37
	syscall::get_kernel_log,			// 38
	syscall::get_vbe_version,			// 39
	syscall::get_vbe_mode_count,		// 40
	syscall::get_vbe_mode,				// 41
	syscall::set_vbe_mode,				// 42
	syscall::request_io,				// 43
	syscall::dma,						// 44
	syscall::execute,					// 45
	syscall::get_physical_address,		// 46
	syscall::allocate_region,			// 47
	syscall::free_region,				// 48
	syscall::get_system_configuration,	// 49
	syscall::get_library_count,			// 50
	syscall::get_library_info,			// 51
	syscall::signal_handler,			// 52
	syscall::signal,					// 53
	syscall::signal_end,				// 54

  syscall::get_log_entry,     // 55
};

static void server_check(interrupt_stackframe &frame)
{
	process &Process = *scheduler::get_process();
	
	if (Process.server() == false)
	{
		ERROR(Process.getName().c_str() << ": not a server (syscall: 0x" << hex(frame.syscall_number()) << ")");
		process::destroy(Process.getId());
		scheduler::schedule(0);
	}
}

void syscall::get_kernel_version(interrupt_stackframe &frame)
{
	frame.param0() = version_major;
	frame.param1() = version_minor;
}

void syscall::heap_allocate(interrupt_stackframe &frame)
{
	process &Process = *scheduler::get_process();
	frame.param0() = reinterpret_cast<uintptr_t>(Process.heapAlloc(frame.param1()));
}

void syscall::get_memory_info(interrupt_stackframe &frame)
{
	page_allocator &PageAllocator = page_allocator::instance();
	frame.param0() = PageAllocator.capacity() * virtual_memory::page_size;
	frame.param1() = PageAllocator.size() * virtual_memory::page_size;
}

void syscall::get_commandline(interrupt_stackframe &frame)
{
	process &Process = *scheduler::get_process();
	frame.param0() = Process.getCommandLine().length();
	if (frame.param1() != 0)
	{
		strcpy(reinterpret_cast<char*>(frame.param1()), Process.getCommandLine().c_str());
	}
}

void syscall::get_time(interrupt_stackframe &frame)
{
	x86_shared::cmos &Cmos = x86_shared::cmos::instance();
	frame.param0() = Cmos.second();
	frame.param1() = Cmos.minute();
	frame.param2() = Cmos.hour();
}

void syscall::get_date(interrupt_stackframe &frame)
{
	x86_shared::cmos &Cmos = x86_shared::cmos::instance();
	frame.param1() = Cmos.dayofmonth();
	frame.param2() = Cmos.month();
	frame.param3() = Cmos.year();
	frame.param4() = static_cast<size_t>(Cmos.summertime());
	
	// Calculate day of week
	frame.param0() = frame.param1() % 7;
	frame.param0() += monthnumbers[frame.param2() - 1];
	frame.param0() += ((frame.param3() % 100) + ((frame.param3() % 100) / 4)) % 7;
	frame.param0() -= ((frame.param3() / 100) % 4 - 3) * 2;
	if (frame.param2() < 3)frame.param0()--;
	frame.param0() %= 7;
}

void syscall::create_port(interrupt_stackframe &frame)
{
	port_manager &PortManager = port_manager::instance();
	frame.param0() = PortManager.create(*scheduler::get_process());
}

void syscall::destroy_port(interrupt_stackframe &frame)
{
	port_manager &PortManager = port_manager::instance();
	frame.param0() = static_cast<size_t>(PortManager.destroy(frame.param1()));
}

void syscall::peek_message(interrupt_stackframe &frame)
{
	libkernel::message_t msg;
	port_manager &PortManager = port_manager::instance();
	frame.param0() = static_cast<size_t>(PortManager.peek(	static_cast<libkernel::port_id_t>(frame.param1()),
															msg,
															*scheduler::get_process()));
	frame.param1() = msg.port;
	frame.param2() = msg.type;
	frame.param3() = msg.param1;
	frame.param4() = msg.param2;
	frame.param5() = msg.param3;
}

void syscall::get_message(interrupt_stackframe &frame)
{
	libkernel::message_t msg;
	port_manager &PortManager = port_manager::instance();
	frame.param0() = frame.param1();
	if (PortManager.get(static_cast<libkernel::port_id_t>(frame.param1()),
						msg,
						*scheduler::get_thread())
		== false)
	{
		scheduler::schedule(&frame);
	}

  frame.param1() = msg.port;
  frame.param2() = msg.type;
  frame.param3() = msg.param1;
  frame.param4() = msg.param2;
  frame.param5() = msg.param3;
}

void syscall::wait_message(interrupt_stackframe &frame)
{
	libkernel::message_t msg;
	port_manager &PortManager = port_manager::instance();
	if (PortManager.wait(	reinterpret_cast<libkernel::port_id_t&>(frame.param0()),
							msg,
							*scheduler::get_thread())
		== true)
	{
		scheduler::schedule(&frame);
	}

  frame.param1() = msg.port;
  frame.param2() = msg.type;
  frame.param3() = msg.param1;
  frame.param4() = msg.param2;
  frame.param5() = msg.param3;
}

void syscall::send_message(interrupt_stackframe &frame)
{
	process &Process = *scheduler::get_process();
	port_manager &PortManager = port_manager::instance();
	sharedMemoryManager &SharedMemoryManager = sharedMemoryManager::instance();
	
	if (frame.param2() == libkernel::message_port::kernel &&
		frame.param3() == libkernel::message::event &&
		frame.param4() == libkernel::event::irq)
	{
		libkernel::process_id_t pid = PortManager.get_processId(frame.param1());
		if (Process.getId() == pid)
		{
			// TODO HACK reenableIRQ(frame.param5(), frame.param1());
		}
		else
		{
			ERROR(Process.getName().c_str() << ": not the port owner");
			Process.destroyThread(scheduler::get_thread()->getId());
			scheduler::schedule(0);
		}
	}
	
	#ifdef X86
		size_t flags = frame.param3() >> 29;
	#endif
	#ifdef X86_64
		size_t flags = frame.param3() >> 61;
	#endif
	if (flags != 0)
	{
		process *dest = process::get_process(PortManager.get_processId(frame.param2()));
		if (dest != 0)
		{
			pair<void*, size_t> result = SharedMemoryManager.transfer(*dest, Process, frame.param5(), flags);
			if (result.first == 0)
			{
				ERROR("syscall: Unable to transfer shared memory #" << dec(frame.param5()) << " (sender: \"" <<
              Process.getName().c_str() << "\" on port " << dec(frame.param1()) << " to port " <<
              dec(frame.param2()) << ", receiver: \"" << dest->getName().c_str() << "\")");
				frame.param0() = static_cast<size_t>(false);
				return;
			}
			frame.param6() = reinterpret_cast<uintptr_t>(result.first) | result.second;
		}
		else
		{
			ERROR("syscall: Cannot find receiving process (sender: \"" << Process.getName().c_str() <<
            "\" on port " << dec(frame.param1()) << " to port " << dec(frame.param2()) << ")");
			frame.param0() = static_cast<size_t>(false);
			return;
		}
	}
	
	libkernel::message_t msg;
	msg.port = frame.param2();
	msg.type = frame.param3();
	msg.param1 = frame.param4();
	msg.param2 = frame.param5();
	msg.param3 = frame.param6();
	frame.param0() = static_cast<size_t>(PortManager.send(frame.param1(), msg, scheduler::get_process()));
}

void syscall::port_info(interrupt_stackframe &frame)
{
	port_manager &PortManager = port_manager::instance();
	libkernel::process_id_t pid = PortManager.get_processId(frame.param1());
	libkernel::thread_id_t tid = PortManager.get_waiting_threadId(frame.param1());
	
	frame.param0() = static_cast<size_t>(pid != 0);
	frame.param1() = pid;
	frame.param2() = tid;
}

void syscall::standard_port(interrupt_stackframe &frame)
{
	// Get the process
	process *Process;
	if (frame.param1() == 0)Process = scheduler::get_process();
	else Process = process::get_process(frame.param1());
	
	// set_standard_port
	if (frame.param2() == 0)
	{
		frame.param0() = static_cast<size_t>(false);
		if (Process != 0)
		{
			frame.param0() = static_cast<size_t>(Process->set_standard_port(frame.param3(), frame.param4(), frame.param5()));
		}
	}
	// get_standard_port
	else if (frame.param2() == 1)
	{
		frame.param0() = 0;
		frame.param1() = 0;
		if (Process != 0)
		{
			std::pair<libkernel::port_id_t,libkernel::port_id_t> ports = Process->get_standard_port(frame.param3());
			frame.param0() = ports.first;
			frame.param1() = ports.second;
		}
	}
}

void syscall::transfer_port(interrupt_stackframe & frame)
{
	port_manager &PortManager = port_manager::instance();
	frame.param0() = static_cast<size_t>(PortManager.transfer(	frame.param1(),
																*scheduler::get_process(),
																frame.param2()));
}

void syscall::add_wait_message(interrupt_stackframe &frame)
{
	libkernel::message_t msg(frame.param2(), frame.param3(), frame.param4(), frame.param5(), frame.param6());
	port_manager &PortManager = port_manager::instance();
	frame.param0() = static_cast<size_t>(PortManager.add_wait(frame.param1(), msg, *scheduler::get_thread()));
}

void syscall::create_thread(interrupt_stackframe &frame)
{
	frame.param0() = static_cast<size_t>(scheduler::get_process()->createThread(frame.param1(), frame.param2(), frame.param3(), frame.param4()));
}

void syscall::suspend_thread(interrupt_stackframe &frame)
{
	if (frame.param1() == 0)frame.param1() = scheduler::get_thread()->getId();
	thread *Thread = process::get_thread(frame.param1());
	frame.param0() = static_cast<size_t>(false);
	if (Thread != 0)
		if (Thread->status() == thread::run)
		{
			Thread->block(thread::suspended);
			frame.param0() = static_cast<size_t>(true);
		}
	scheduler::schedule(&frame);
}

void syscall::resume_thread(interrupt_stackframe &frame)
{
	thread *Thread = process::get_thread(frame.param1());
	frame.param0() = static_cast<size_t>(false);
	if (Thread != 0)
		if (Thread->status() == thread::suspended)
		{
			Thread->unblock();
			frame.param0() = static_cast<size_t>(true);
		}
}

void syscall::destroy_thread(interrupt_stackframe &frame)
{
	libkernel::thread_id_t tid = scheduler::get_thread()->getId();
	if (frame.param1() == 0)frame.param1() = tid;
	frame.param0() = static_cast<size_t>(scheduler::get_process()->destroyThread(frame.param1()));
	if (tid == frame.param1())scheduler::schedule(0);
}

void syscall::destroy_process(interrupt_stackframe &frame)
{
	libkernel::process_id_t thispid = scheduler::get_process()->getId();
	if (frame.param1() == 0)frame.param1() = thispid;
	frame.param0() = static_cast<size_t>(process::destroy(frame.param1()));
	if (frame.param1() == thispid)scheduler::schedule(0);
	scheduler::schedule(&frame);
}

void syscall::get_pid(interrupt_stackframe &frame)
{
	frame.param0() = scheduler::get_process()->getId();
}

void syscall::get_tid(interrupt_stackframe &frame)
{
	frame.param0() = scheduler::get_thread()->getId();
}

void syscall::get_process_count(interrupt_stackframe &frame)
{
	frame.param0() = process::size();
}

void syscall::get_process_list(interrupt_stackframe &frame)
{
	if (frame.param1() >= process::size())frame.param0() = 0;
	else
	{
		process *Process = process::get_process_by_index(frame.param1());
		frame.param0() = Process->getId();
	}
}

void syscall::get_process_info(interrupt_stackframe &frame)
{
	process *Process = process::get_process(frame.param1());
	if (Process != 0)
	{
		if (frame.param2() == 0) // Get process name (length)
		{
			string name = Process->getName();
			if (frame.param3() == 0)frame.param0() = name.length();
			else strcpy(reinterpret_cast<char*>(frame.param3()), name.c_str());
		}
		else if (frame.param2() == 1) // Get process used memory
		{
			frame.param0() = Process->used_page_count() * virtual_memory::page_size;
		}
		//TODO: more process information
	}
}

void syscall::register_event(interrupt_stackframe &frame)
{
	event_manager &EventManager = event_manager::instance();
	
	if (frame.param2() == libkernel::event::irq ||
		frame.param2() == libkernel::event::log_update)
	{
		server_check(frame);
		
		if (frame.param2() == libkernel::event::irq)
			frame.param0() = EventManager.register_irq(	frame.param1(),
														frame.param3(),
														frame.param4());
		else if (frame.param2() == libkernel::event::log_update)
		{
			// TODO NOTE HACK frame.param0() = static_cast<size_t>(video::instance().registerEvent(frame.param1()));
			FATAL("No log_update event");
			processor::halt();
		}
	}
	else if (	frame.param2() == libkernel::event::create_process ||
				frame.param2() == libkernel::event::destroy_process)
	{
		process::registerEvent(	frame.param1(),
								static_cast<libkernel::event>(frame.param2()),
								frame.param3(),
								frame.param4());
		frame.param0() = static_cast<size_t>(true);
	}
	else if (frame.param2() == libkernel::event::timer)
		EventManager.register_timer(frame.param1(),
									static_cast<uint64_t>(frame.param3()),
									static_cast<bool>(frame.param4()));
	else
	{
		ERROR("error: trying to register unknown event");
		process &Process = *scheduler::get_process();
		process::destroy(Process.getId());
		scheduler::schedule(0);
	}
}

void syscall::acknoledge_event(interrupt_stackframe &frame)
{
	event_manager &EventManager = event_manager::instance();
	if (frame.param2() == libkernel::event::irq)
		EventManager.acknoledge_irq(frame.param1(), frame.param3());
	else
	{
		ERROR("error: trying to acknoledge unknown event");
		process &Process = *scheduler::get_process();
		process::destroy(Process.getId());
		scheduler::schedule(0);
	}
}

void syscall::unregister_event(interrupt_stackframe &frame)
{
  event_manager &EventManager = event_manager::instance();
  
/* TODO
  if (frame.param2() == lightOS::event::irq ||
    frame.param2() == lightOS::event::log_update)
  {
    server_check(frame);
    
    if (frame.param2() == lightOS::event::irq)
      frame.param0() = EventManager.register_irq( frame.param1(),
                            frame.param3(),
                            frame.param4());
    else if (frame.param2() == lightOS::event::log_update)
    {
      // TODO NOTE HACK frame.param0() = static_cast<size_t>(video::instance().registerEvent(frame.param1()));
      FATAL("No log_update event");
      processor::halt();
    }
  }
  else if ( frame.param2() == lightOS::event::create_process ||
        frame.param2() == lightOS::event::destroy_process)
  {
    process::registerEvent( frame.param1(),
                static_cast<lightOS::event::type>(frame.param2()),
                frame.param3(),
                frame.param4());
    frame.param0() = static_cast<size_t>(true);
  }
  else*/ if (frame.param2() == libkernel::event::timer)
    EventManager.unregister_timer(frame.param1());
  else
  {
    ERROR("error: trying to unregister unknown event");
    process &Process = *scheduler::get_process();
    process::destroy(Process.getId());
    scheduler::schedule(0);
  }
}

void syscall::get_tick_count(interrupt_stackframe &frame)
{
	event_manager &EventManager = event_manager::instance();
	frame.param0() = EventManager.get_ticks();
}

void syscall::create_shared_memory(interrupt_stackframe &frame)
{
	sharedMemoryManager &SharedMemoryManager = sharedMemoryManager::instance();
	pair<libkernel::shared_memory_id_t, void*> result = SharedMemoryManager.create(*scheduler::get_process(), frame.param1());
	frame.param0() = result.first;
	frame.param1() = reinterpret_cast<uintptr_t>(result.second);
}

void syscall::destroy_shared_memory(interrupt_stackframe &frame)
{
	sharedMemoryManager &SharedMemoryManager = sharedMemoryManager::instance();
	frame.param0() = static_cast<size_t>(SharedMemoryManager.destroy(*scheduler::get_process(), frame.param1()));
}

void syscall::range_allocator_count(interrupt_stackframe &frame)
{
	range_allocator &RangeAllocator = range_allocator::instance();
	frame.param0() = RangeAllocator.count();
	frame.param1() = RangeAllocator.capacity() * virtual_memory::page_size;
}

void syscall::enum_range_allocator(interrupt_stackframe &frame)
{
	range_allocator &RangeAllocator = range_allocator::instance();
	range_allocator::range Range = RangeAllocator.get_range(frame.param1());
	frame.param0() = Range.address;
	frame.param1() = Range.size * virtual_memory::page_size;
}

void syscall::create_specific_port(interrupt_stackframe &frame)
{
	server_check(frame);
	
	process &Process = *scheduler::get_process();
	port_manager &PortManager = port_manager::instance();
	frame.param0() = static_cast<size_t>(PortManager.create(Process, frame.param1()));
}

void syscall::get_kernel_log(interrupt_stackframe &frame)
{
	server_check(frame);
	
	FATAL("not get_kernel_log()");
	processor::halt();
	/*
	std::string &log = video::instance().log();
	if (frame.param1() == 0)
	{
		frame.param0() = log.length();
	}
	else
	{
		if (log.length() != 0)
		{
			strncpy(reinterpret_cast<char*>(frame.param1()), log.c_str(), frame.param2() + 1);
			log.erase(log.begin(), log.begin() + frame.param2());
		}
	}*/
}

void syscall::get_vbe_version(interrupt_stackframe &frame)
{
	server_check(frame);
	
	#ifdef _LIGHTOS_V86
		vbe &Vbe = vbe::instance();
		frame.param0() = Vbe.majorVersion();
		frame.param1() = Vbe.minorVersion();
	#else
		frame.param0() = 0;
		frame.param1() = 0;
	#endif
	scheduler::schedule(&frame);
}

void syscall::get_vbe_mode_count(interrupt_stackframe &frame)
{
	server_check(frame);
	
	#ifdef _LIGHTOS_V86
		vbe &Vbe = vbe::instance();
		frame.param0() = Vbe.modeCount();
	#else
		frame.param0() = 0;
	#endif
	scheduler::schedule(&frame);
}

void syscall::get_vbe_mode(interrupt_stackframe &frame)
{
	server_check(frame);
	
	#ifdef _LIGHTOS_V86
		vbe &Vbe = vbe::instance();
		vbe::videoMode VideoMode;
		if (Vbe.getModeInfo(frame.param1(), VideoMode) == true)
		{
			frame.param0() = VideoMode.width;
			frame.param1() = VideoMode.height;
			frame.param2() = VideoMode.bpp;
		}
		else
		{
			frame.param0() = frame.param1() = frame.param2() = 0;
		}
	#else
		frame.param0() = frame.param1() = frame.param2() = 0;
	#endif
	scheduler::schedule(&frame);
}

void syscall::set_vbe_mode(interrupt_stackframe &frame)
{
	server_check(frame);
	
	frame.param0() = 0;
	#ifdef _LIGHTOS_V86
		vbe &Vbe = vbe::instance();
		if (Vbe.setMode(frame.param1()) == true)
		{
			vbe::videoMode VideoMode;
			if (Vbe.getModeInfo(frame.param1(), VideoMode) == true)
				frame.param0() = reinterpret_cast<size_t>(VideoMode.framebuffer);
		}
	#endif
	scheduler::schedule(&frame);
}

void syscall::request_io(interrupt_stackframe &frame)
{
	server_check(frame);
	
	thread &Thread = *scheduler::get_thread();
	frame.param0() = static_cast<size_t>(true);
	Thread.set_cpu_state(frame);
	Thread.grantIOAccess();
	scheduler::schedule(0);
}

void syscall::dma(interrupt_stackframe &frame)
{
	server_check(frame);
	
	x86_shared::dma &Dma = x86_shared::dma::instance();
	frame.param0() = static_cast<size_t>(Dma.setup(	frame.param1(),
													frame.param2(),
													frame.param3(),
													frame.param4()));
}

void syscall::execute(interrupt_stackframe &frame)
{
	server_check(frame);
	
	frame.param0() = 0;
	string libName;
	string name(reinterpret_cast<char*>(frame.param3()));
	
	#ifdef X86
		// NOTE: We have to copy the elf image into the kernel space
		size_t pages = frame.param2() / virtual_memory::page_size;
		if ((frame.param2() % virtual_memory::page_size) != 0)++pages;
		void *image = kernel_context::instance().allocate_memory_region(pages, lightOS::context::write);
		memcpy(image, reinterpret_cast<void*>(frame.param1()), frame.param2());
		
		KERNEL_CONTEXT_START;
		elf Elf(image, frame.param2());
	#else
		elf Elf(reinterpret_cast<void*>(frame.param1()), frame.param2());
	#endif
	
	frame.param0() = 0;
	if (Elf.valid() == true)
	{
		if (Elf.is_library() == true)
		{
			Elf.create_library_image(name.c_str());
			frame.param0() = static_cast<size_t>(-1);
		}
		else
		{
			context Context;
			vector<void*> Pages;
			bool result = Elf.create_process_image(Context, Pages, libName);
			
			if (result == true)
			{
				frame.param0() = process::create(	Context,
													Pages,
													name,
													Elf.entry_point(),
													frame.param4());
			}
		}
	}
	
	#ifdef X86
		KERNEL_CONTEXT_END;
		KernelContext.free_memory_region(image, pages);
	#endif
	
	if (frame.param0() == 0 &&
		libName.length() != 0)
		strcpy(reinterpret_cast<char*>(frame.param5()), libName.c_str());
}

void syscall::get_physical_address(interrupt_stackframe &frame)
{
	server_check(frame);
	
	context &Context = scheduler::get_process()->getContext();
	frame.param0() = reinterpret_cast<size_t>(Context.physical_address(reinterpret_cast<void*>(frame.param1())));
}

void syscall::allocate_region(interrupt_stackframe &frame)
{
	server_check(frame);
	
	process &Process = *scheduler::get_process();
	frame.param0() = reinterpret_cast<size_t>(Process.create_memory_region(	frame.param1(),
																			frame.param2(),
																			reinterpret_cast<void*>(frame.param3())));
}

void syscall::free_region(interrupt_stackframe &frame)
{
	server_check(frame);
	
	scheduler::get_process()->free_memory_region(reinterpret_cast<void*>(frame.param1()));
}

void syscall::get_system_configuration(interrupt_stackframe &frame)
{
	server_check(frame);
	
	// Get the information from the serial driver
	uint16_t ser0, ser1, ser2, ser3, usedSer;
  /* TODO FIXME HACK
	serial &Serial = serial::instance();
	usedSer = Serial.get();
	Serial.get(ser0, ser1, ser2, ser3);
  */
	
	// Pack this info into the registers
	frame.param0() = (ser1 << 16) | ser0;
	frame.param1() = (ser3 << 16) | ser2;
	frame.param2() = usedSer;
}

void syscall::get_library_count(interrupt_stackframe &frame)
{
	frame.param0() = elf::library_count();
}

void syscall::get_library_info(interrupt_stackframe &frame)
{
	size_t index = frame.param1();
	frame.param0() = elf::get_library_name(index, reinterpret_cast<char*>(frame.param2()));
	pair<uintptr_t,size_t> result = elf::get_library_address(index);
	frame.param1() = result.first;
	frame.param2() = result.second;
}

void syscall::signal_handler(interrupt_stackframe &frame)
{
	process *Process = scheduler::get_process();
	if (frame.param1() == 0)
		frame.param1() = reinterpret_cast<size_t>(Process->signal_handler());
	else
		Process->signal_handler(reinterpret_cast<void*>(frame.param1()));
}

void syscall::signal(interrupt_stackframe &frame)
{
	libkernel::process_id_t pid = scheduler::get_process()->getId();
	if (frame.param1() == 0)frame.param1() = pid;
	if (frame.param1() == pid)
		scheduler::get_thread()->set_cpu_state(frame);
	
	process *Dest = process::get_process(frame.param1());
	frame.param0() = Dest->signal(frame.param2());
	
	if (frame.param1() == pid)
		scheduler::schedule(0);
}

void syscall::signal_end(interrupt_stackframe &frame)
{
	process *Process = scheduler::get_process();
	thread *Thread = scheduler::get_thread();
	Thread->set_cpu_state(frame);
	Process->signal_end();
	scheduler::schedule(0);
}

/*
 * in:
 *  param1 - index
 *  param2 - size of param3's array
 *  param3 - pointer to the array
 * out:
 *  param1 - real size
 *  param2 - log level
 */
void syscall::get_log_entry(interrupt_stackframe& frame)
{
  log& Log = log::instance();
  libkernel::log_level lvl;
  frame.param0() = Log.get_entry(frame.param1(),
                                 lvl,
                                 reinterpret_cast<char*>(frame.param3()),
                                 frame.param2());
  frame.param1() = lvl;
}
