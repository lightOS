/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/id.hpp>
#include <kernel/spinlock.hpp>
#include <lightOS/lightOS.hpp>
using namespace kernel;

libkernel::port_id_t lastPortId = libkernel::message_port::user;
libkernel::shared_memory_id_t lastSharedMemoryId = 0;
#ifdef _LIGHTOS_SMP
	lightOS::spinlock mIdLock;
#endif

id kernel::generateId()
{
	LOCK(mIdLock);
		id Id = ++lastPortId;
	UNLOCK(mIdLock);
	return Id;
}
libkernel::port_id_t kernel::generatePortId()
{
	LOCK(mIdLock);
		libkernel::port_id_t Id = ++lastPortId;
	UNLOCK(mIdLock);
	return Id;
}
libkernel::shared_memory_id_t kernel::generateSharedMemoryId()
{
	LOCK(mIdLock);
		libkernel::shared_memory_id_t Id = ++lastSharedMemoryId;
	UNLOCK(mIdLock);
	return Id;
}
