/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/arch/arch.hpp>
#include <kernel/log.hpp>
#include <kernel/process.hpp>
#include <kernel/port_manager.hpp>
#include <libarch/scoped_lock.hpp>
using namespace kernel;
using namespace std;

//FIXME: Use lists to speed things up

SINGLETON_INSTANCE(kernel::port_manager);

kernel::port_manager::port_manager()
{
}

kernel::port_manager::~port_manager()
{
}

libkernel::port_id_t port_manager::create(process &Process)
{
  port *Port = new port(generatePortId(), &Process);
  SCOPED_LOCK(mLock, scope);
  list.push_back(Port);
  return Port->id;
}

bool port_manager::create(  process &Process,
              libkernel::port_id_t Id)
{
  SCOPED_LOCK(mLock, scope);

  // Does the port already exist?
  if (get_port_by_id(Id) != 0)
    return false;

  // Create the port
  port *Port = new port(Id, &Process);
  list.push_back(Port);
  return true;
}

bool port_manager::send(libkernel::port_id_t port,
            const libkernel::message_t &msg,
            const process *Process)
{
  LOCK(mLock);
  
    // Does the destination port exist at all?
    port_manager::port *Port = get_port_by_id(msg.port);
    if (Port == 0)
    {
      UNLOCK(mLock);
      return false;
    }
    
    // Is it the kernel that is sending a message?
    if (Process != 0 && port != libkernel::message_port::kernel)
    {
      // Does the source port really exist and does the process own that port?
      port_manager::port *srcPort = get_port_by_id(port);
      if (srcPort == 0 || (Process != 0 && srcPort->owner != Process))
      {
        UNLOCK(mLock);
        
        ERROR("port_manager::send(): source port #" << dec(port) << " non-existent or not owned by the process");
        return false;
      }
    }
    
    // Is the thread already waiting for this message?
    // TODO: multiple wait messages not supported
    if (Port->waiter != 0 &&
      Port->waiter->status() == thread::wait_for_port &&
      match(port, msg, Port->waiter_msg) == true)
    {
      // Unblock the thread
      Port->waiter->unblock(msg.port, port, msg.type, msg.param1, msg.param2, msg.param3);
      remove_thread_wait(*Port->waiter);
      
      UNLOCK(mLock);
      return true;
    }
    
    // Enqueue the message
    libkernel::message_t *Msg = new libkernel::message_t(port, msg.type, msg.param1, msg.param2, msg.param3);
    Port->msg.push_back(Msg);
  
  UNLOCK(mLock);
  return true;
}

bool port_manager::peek(libkernel::port_id_t port,
            libkernel::message_t &msg,
            const process &Process)
{
  LOCK(mLock);
  
    // Does the port exist and does it belong to this process?
    port_manager::port *Port = get_port_by_id(port);
    if (Port == 0 || Port->owner != &Process)
    {
      UNLOCK(mLock);
      
      ERROR("port_manager::peek(): port #" << dec(port) << " non-existent or not owned by the process");
      return false;
    }
    
    // Any messages in the queue?
    if (Port->msg.size() == 0)
    {
      UNLOCK(mLock);
      return false;
    }
    
    libkernel::message_t *Message = Port->msg.front();
    msg = *Message;
    Port->msg.erase(Port->msg.begin());
    delete Message;
  
  UNLOCK(mLock);
  return true;
}

bool port_manager::get( libkernel::port_id_t port,
            libkernel::message_t &msg,
            thread &Thread)
{
  if (peek(port, msg, Thread.getProcess()) == true)return true;
  
  LOCK(mLock);
  
    // Does the port exist and does it belong to this process?
    port_manager::port *Port = get_port_by_id(port);
    if (Port == 0 || Port->owner != &Thread.getProcess())
    {
      UNLOCK(mLock);
      
      ERROR("port_manager::get(): port #" << dec(port) << " non-existent or not owned by the process");
      return false;
    }
  
    Port->waiter = &Thread;
    Port->waiter_msg.port = 0;
    Port->waiter_msg.type = 0;
    Port->waiter_msg.param1 = 0;
    Port->waiter_msg.param2 = 0;
    Port->waiter_msg.param3 = 0;
    Port->waiter->block(thread::wait_for_port);
  
  UNLOCK(mLock);
  return false;
}

bool port_manager::wait(libkernel::port_id_t &port,
            libkernel::message_t &msg,
            thread &Thread)
{
  LOCK(mLock);
  
    size_t ports = 0;
    
    // Go through the ports
    for (vector<port_manager::port*>::iterator i=list.begin();i != list.end();i++)
      if (i->waiter == &Thread)
      {
        // Go through the messages of that port
        for (vector<libkernel::message_t*>::iterator y = i->msg.begin();y != i->msg.end();y++)
        {
          if (match(y->port, **y, i->waiter_msg) == true)
          {
            // Copy the message
            port = i->id;
            msg = **y;
            
            // Delete the message
            delete *y;
            i->msg.erase(y);
            
            // Remove this thread from all the other port waits
            remove_thread_wait(*i->waiter);
            
            UNLOCK(mLock);
            return false;
          }
        }
        ++ports;
      }
    
    // Are there any port the thread is waiting for?
    if (ports == 0)
    {
      port = 0;
      
      UNLOCK(mLock);
      return false;
    }
  
    // Block the thread
    Thread.block(thread::wait_for_port);
  
  UNLOCK(mLock);
  return true;
}

bool port_manager::add_wait(libkernel::port_id_t port,
              const libkernel::message_t &msg,
              thread &Thread)
{
  LOCK(mLock);
  
    // Does the port exist and does it belong to this process?
    port_manager::port *Port = get_port_by_id(port);
    if (Port == 0 || Port->owner != &Thread.getProcess())
    {
      UNLOCK(mLock);
      
      ERROR("port_manager::add_wait(): port #" << dec(port) << " non-existent or not owned by the process");
      return false;
    }
    
    // Set the waiter
    Port->waiter = &Thread;
    
    // Copy the message
    Port->waiter_msg = msg;
  
  UNLOCK(mLock);
  return true;
}

bool port_manager::transfer(libkernel::port_id_t port,
              const process &Process,
              libkernel::process_id_t receiver)
{
  LOCK(mLock);
  
    // Does the port exist and dies it belong to this process
    port_manager::port *Port = get_port_by_id(port);
    if (Port == 0 || Port->owner != &Process)
    {
      UNLOCK(mLock);
      
      ERROR("port_manager::transfer(): port #" << dec(port) << " non-existent or not owned by the process");
      return false;
    }
    
    // Get the receiving process
    process *recProcess = process::get_process(receiver);
    if (recProcess == 0)
    {
      UNLOCK(mLock);
      return false;
    }
    
    // Set the port owner
    Port->owner = recProcess;
  UNLOCK(mLock);
  return true;
}

libkernel::process_id_t port_manager::get_processId(libkernel::port_id_t id)
{
  LOCK(mLock);
  
    // Find the port
    port *Port = get_port_by_id(id);
    if (Port == 0)
    {
      UNLOCK(mLock);
      return 0;
    }
    
    // Save the pid
    libkernel::process_id_t pid = Port->owner->getId();
  
  UNLOCK(mLock);
  return pid;
}

libkernel::thread_id_t port_manager::get_waiting_threadId(libkernel::port_id_t id)
{
  LOCK(mLock);
  
    // Find the port
    port *Port = get_port_by_id(id);
    if (Port == 0 ||
      Port->waiter == 0)
    {
      UNLOCK(mLock);
      return 0;
    }
    
    // Save the threadId
    libkernel::thread_id_t tid = Port->waiter->getId();
  
  UNLOCK(mLock);
  return tid;
}

bool port_manager::destroy(libkernel::port_id_t port)
{
  LOCK(mLock);
  
    for (vector<port_manager::port*>::iterator i = list.begin();i != list.end();i++)
      if (i->id == port)
      {
        destroy(*i);
        list.erase(i);
        
        UNLOCK(mLock);
        return true;
      }
  
  UNLOCK(mLock);
  return false;
}

void port_manager::destroy_process_ports(const process &Process)
{
  LOCK(mLock);
  
  for (vector<port_manager::port*>::iterator i=list.begin();i != list.end();)
    if (i->owner == &Process)
    {
      destroy(*i);
      i = list.erase(i);
    }
    else ++i;
  
  UNLOCK(mLock);
}

port_manager::port *port_manager::get_port_by_id(libkernel::port_id_t id)
{
  for (vector<port_manager::port*>::iterator i=list.begin();i != list.end();i++)
    if (i->id == id)return *i;
  return 0;
}

void port_manager::destroy(port *Port)
{
  for (vector<libkernel::message_t*>::iterator y = Port->msg.begin();y != Port->msg.end();y++)
    delete *y;
  delete Port;
}

void port_manager::remove_thread_wait(const thread &Thread)
{
  for (vector<port_manager::port*>::iterator i=list.begin();i != list.end();i++)
    if (i->waiter == &Thread)
      i->waiter = 0;
}

bool port_manager::match(libkernel::port_id_t port,
            const libkernel::message_t &msg1,
            const libkernel::message_t &msg2)
{
  if (msg2.port != 0)
    if (msg2.port != port)
      return false;
  if (msg2.type != 0)
    if (MESSAGE_TYPE(msg2.type) != MESSAGE_TYPE(msg1.type))
      return false;
  if (msg2.param1 != 0)
    if (msg2.param1 != msg1.param1)
      return false;
  if (msg2.param2 != 0)
    if (msg2.param2 != msg1.param2)
      return false;
  if (msg2.param3 != 0)
    if (msg2.param3 != msg1.param3)
      return false;
  return true;
}
