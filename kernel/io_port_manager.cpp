/*
lightOS kernel
Copyright (C) 2008-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <kernel/log.hpp>
#include <kernel/io_port_manager.hpp>

SINGLETON_INSTANCE(kernel::io_port_manager);

kernel::io_port_manager::io_port_manager()
{
}

kernel::io_port_manager::~io_port_manager()
{
}

void kernel::io_port_manager::init()
{
/*
  io_port_range range;
  range.port = 0x0000;
  range.size = 0x10000;
  mList.push_back(range);
*/
}

bool kernel::io_port_manager::allocate(io_port& ioport,
                                       libarch::ioport_t port,
                                       size_t size)
{
/*
  if (range.size == 0)return false;

  LOCK(mLock);

    std::vector<io_port_range>::iterator end = mList.end();
    for (std::vector<io_port_range>::iterator i = mList.begin();i != end;i++)
      if ((*i).port <= range.port &&
        ((*i).port + (*i).size) >= (range.port + range.size))
      {
        if ((*i).port == range.port)
        {
          (*i).port += range.size;
          (*i).size -= range.size;
        }
        else if (((*i).port + (*i).size) == (range.port + range.size))
        {
          (*i).size -= range.size;
        }
        else
        {
          io_port_range range2;
          range2.port = range.port + range.size;
          range2.size = ((*i).port + (*i).size) - range2.port;
          mList.push_back(range2);
          (*i).size = range.port - (*i).port;
        }
        UNLOCK(mLock);
        return true;
      }

  UNLOCK(mLock);
*/

  return false;
}

void kernel::io_port_manager::free(io_port& port)
{
/*
  LOCK(mLock);

    std::vector<io_port_range>::iterator end = mList.end();
    std::vector<io_port_range>::iterator i = mList.begin();
    for (;i != end;i++)
      if (range.port == ((*i).port + (*i).size))
      {
        (*i).size += range.size;
        break;
      }

    std::vector<io_port_range>::iterator i2 = mList.begin();
    for (;i2 != end;i2++)
      if ((*i2).port == (range.port + range.size))
      {
        if (i == end)
        {
          (*i2).port = range.port;
          (*i2).size += range.size;
        }
        else
        {
          (*i).size += (*i2).size;
          mList.erase(i2);
        }
        UNLOCK(mLock);
        return;
      }

    if (i == end)
      mList.push_back(range);

  UNLOCK(mLock);
*/
}
