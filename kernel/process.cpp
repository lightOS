/*
lightOS kernel
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <algorithm>
#include <iterator>
#include <kernel/arch/arch.hpp>
#include <kernel/log.hpp>
#include <kernel/process.hpp>
#include <kernel/scheduler.hpp>
#include <kernel/port_manager.hpp>
#include <kernel/page_allocator.hpp>
#include <kernel/range_allocator.hpp>
#include <lightOS/c++utils.hpp>
#include <kernel/processor.hpp>

#include <kernel/virtual_memory.hpp>
using namespace std;
using namespace kernel;
using kernel::virtual_memory;

vector<process*> process::mProcessList;
#ifdef _LIGHTOS_SMP
	lightOS::spinlock process::mListLock;
#endif

// TODO: Move to event_manager
vector<process::event> process::mEvents;

libkernel::process_id_t process::create(	context &Context,
							std::vector<void*> pages,
							const std::string &name,
							uintptr_t entryPoint,
							size_t flags)
{
	// Create the process
	process *Process = new process(	name,
									Context,
									pages,
									((flags & lightOS::process::server) != 0) ? true : false);
	
	
	// Add the process to the list
	LOCK(mListLock);
	
		mProcessList.push_back(Process);
	
	UNLOCK(mListLock);
	
	
	// Create the initial thread
	Process->createThread(	entryPoint,
							0,
							0,
							flags,
							Process->getId());
	
	// TODO: use the event_manager
	broadcastEvent(	libkernel::event::create_process,
					Process->getId(),
					0);
	
	return Process->getId();
}

bool process::destroy(libkernel::process_id_t pid)
{
	//LOCK(mListLock);
	
		process_iterator iEnd = mProcessList.end();
		for (process_iterator i = mProcessList.begin();i != iEnd;i++)
			if ((*i)->getId() == pid)
			{
				// Remove the process from the list
				process *Process = *i;
				mProcessList.erase(i);
				
				//UNLOCK(mLock);
				
				// Destroy the process's ports
				port_manager &PortManager = port_manager::instance();
				PortManager.destroy_process_ports(*Process);
				
				// Delete the process
				KERNEL_CONTEXT_START;
					delete Process;
				KERNEL_CONTEXT_END;
				
				// TODO: Use the event_manager
				broadcastEvent(	libkernel::event::destroy_process,
								pid,
								0);
				return true;
			}
	
	//UNLOCK(mListLock);
	return false;
}

size_t process::size()
{
	//LOCK(mListLock);
	
		size_t size = mProcessList.size();
	
	//UNLOCK(mListLock);
	return size;
}

process *process::get_process(libkernel::process_id_t id)
{
	//LOCK(mListLock);
	
		process_iterator iEnd = mProcessList.end();
		for (process_iterator i = mProcessList.begin();i != iEnd;i++)
			if ((*i)->getId() == id)
			{
				//UNLOCK(mLock);
				return *i;
			}
	
	//UNLOCK(mListLock);
	return 0;
}

process *process::get_process_by_index(size_t index)
{
	//LOCK(mListLock);
	
		process *Process = 0;
		if (index < mProcessList.size())
			Process = mProcessList[index];
	
	//UNLOCK(mListLock);
	return Process;
}

thread *process::get_thread(libkernel::thread_id_t id)
{
	//LOCK(mListLock);
	
		process_iterator iEnd = mProcessList.end();
		for (process_iterator i = mProcessList.begin();i != iEnd;i++)
			for (vector<thread*>::iterator y=(*i)->mList.begin();y != (*i)->mList.end();y++)
				if ((*y)->getId() == id)
				{
					UNLOCK(mListLock);
					return *y;
				}
	
	//UNLOCK(mListLock);
	return 0;
}

//
// TODO TODO TODO TODO TODO TODO TODO TODO
//

size_t process::used_page_count() const
{
	// text/data/heap size in pages
	size_t pagecount = mPages.size();
	
	// Add thread stack sizes
	for (vector<thread*>::iterator i = mList.begin();i != mList.end();i++)
	{
		pair<void*,size_t> threadStack = (*i)->get_stack();
		pagecount += threadStack.second / virtual_memory::page_size;
	}
	
	// TODO: Add memory regions
	return pagecount;
}
void process::registerEvent(libkernel::port_id_t port,
							libkernel::event type,
							size_t param1,
							size_t param2)
{
	event Event = {port, type, param1, param2};
	mEvents.push_back(Event);
}
void process::unregisterEvent(	libkernel::port_id_t port,
								libkernel::event type)
{
	for (vector<event>::iterator i=mEvents.begin();i != mEvents.end();i++)
		if ((*i).port == port && (*i).type == type)
		{
			mEvents.erase(i);
			return;
		}
}
void process::broadcastEvent(	libkernel::event type,
								size_t param1,
								size_t param2)
{
	for (vector<event>::iterator i=mEvents.begin();i != mEvents.end();i++)
		if ((*i).type == type &&
			((*i).param1 == 0 || (*i).param1 == param1) &&
			((*i).param2 == 0 || (*i).param2 == param2))
		{
			port_manager &PortManager = port_manager::instance();
      libkernel::message_t msg((*i).port,
                               libkernel::message::event,
                               type,
                               param1,
                               param2);
      PortManager.send(libkernel::message_port::kernel, msg);
    }
}
process::process(	const string &cmdline,
					context &Context,
					std::vector<void*> pages,
					bool isServer)
	:	mIsServer(isServer), mCmdLine(cmdline), mContext(Context), mPid(generateId()),
		mLatestThreadStack(THREAD_STACK_START), mPages(pages), mLatestSharedMemory(SHARED_MEMORY_START),
		mLatestMemoryRegion(MEMORY_REGION_START), mSignalHandler(0)
{
	for (size_t i = 1;i < 5;i++)
	{
		mStandardPorts[i] = i;
		mStandardFsPorts[i] = i;
	}
}
libkernel::thread_id_t process::createThread(	uintptr_t entryPoint,
								size_t param1,
								size_t param2,
								size_t flags,
								libkernel::thread_id_t tid)
{
	void *stack;
	size_t stacksize;
	if (mFreeThreadStacks.size() != 0)
	{
		stack = mFreeThreadStacks[mFreeThreadStacks.size() - 1].first;
		stacksize = mFreeThreadStacks[mFreeThreadStacks.size() - 1].second;
		mFreeThreadStacks.pop_back();
	}
	else
	{
		stack = mLatestThreadStack;
		mLatestThreadStack = adjust_pointer(mLatestThreadStack, - 1024 * 1024);
		
		page_allocator &PageAllocator = page_allocator::instance();
		void *page = PageAllocator.allocate();
		mPages.push_back(page);
		mContext.map(	page,
						adjust_pointer(stack, - virtual_memory::page_size),
						lightOS::context::write | lightOS::context::user);
		stacksize = virtual_memory::page_size;
	}
	
	thread *Thread = new thread(*this, entryPoint, stack, stacksize, param1, param2, tid);
	mList.push_back(Thread);
	if ((flags & lightOS::thread::suspended) != 0)
	{
		Thread->block(thread::suspended);
	}
	else
	{
		scheduler::add(Thread);
	}
	
	libkernel::thread_id_t ThreadID = Thread->getId();
	if (port_manager::instance().create(*this, ThreadID) == false)
	{
		ERROR("process::create(): Could not create the thread port");
	}
	return ThreadID;
}
bool process::destroyThread(libkernel::thread_id_t id)
{
	if (mList.size() == 1)
	{
		destroy(mPid);
		return true;
	}
	
	for (vector<thread*>::iterator i=mList.begin();i != mList.end();i++)
	{
		if ((*i)->getId() == id)
		{
			destroyThread(*i);
			mList.erase(i);
			return true;
		}
	}
	return false;
}
void process::destroyThread(thread *Thread)
{
	mFreeThreadStacks.push_back(Thread->get_stack());
	delete Thread;
}
void *process::heapAlloc(size_t count)
{
	return mContext.heap_map(count, lightOS::context::write | lightOS::context::user, this);
}
bool process::set_standard_port(libkernel::port_id_t stdPort, libkernel::port_id_t Port, libkernel::port_id_t fsPort)
{
	if (stdPort >= 5 || stdPort == 0)return false;
	mStandardPorts[stdPort] = Port;
	mStandardFsPorts[stdPort] = fsPort;
	return true;
}
pair<libkernel::port_id_t,libkernel::port_id_t> process::get_standard_port(libkernel::port_id_t stdPort)
{
	if (stdPort >= 5 || stdPort == 0)return pair<libkernel::port_id_t,libkernel::port_id_t>(0, 0);
	return pair<libkernel::port_id_t, libkernel::port_id_t>(mStandardPorts[stdPort], mStandardFsPorts[stdPort]);
}
void *process::create_memory_region(size_t size,
									size_t flags,
									void *physical)
{
	if (size == 0)return 0;
	
	void *result = 0;
	page_allocator &PageAllocator = page_allocator::instance();
	
	size_t pageCount = size / virtual_memory::page_size;
	
	if (physical == 0)
	{
		if ((size % virtual_memory::page_size) != 0)++pageCount;
		if ((flags & lightOS::memory::type_mask) == lightOS::memory::below_1mb ||
			(flags & lightOS::memory::type_mask) == lightOS::memory::below_16mb ||
			((flags & lightOS::memory::type_mask) == lightOS::memory::below_4gb && pageCount == 1) ||
			((flags & lightOS::memory::type_mask) == lightOS::memory::below_4gb && (flags & lightOS::memory::continuous) != lightOS::memory::continuous))
		{
			region Region;
			Region.size = pageCount;
			Region.vAddress = mLatestMemoryRegion;
			Region.physical = false;
			mLatestMemoryRegion = adjust_pointer(mLatestMemoryRegion, virtual_memory::page_size * pageCount);
			if ((flags & lightOS::memory::type_mask) == lightOS::memory::below_1mb ||
				(flags & lightOS::memory::type_mask) == lightOS::memory::below_16mb)
			{
				// Allocate memory below 1/16MB (NOTE: always continuous)
				range_allocator &RangeAllocator = range_allocator::instance();
				Region.pages.push_back(RangeAllocator.allocate_range(pageCount, flags & lightOS::memory::type_mask));
				Region.continuous = true;
			}
			else
			{
				// Allocate memory below 4GB (NOTE: never continuous)
				for (size_t i=0;i < pageCount;i++)
					Region.pages.push_back(PageAllocator.allocate());
				Region.continuous = false;
			}
			
			// Map the pages
			KERNEL_CONTEXT_START;
			for (size_t i = 0;i < Region.size;i++)
			{
				void *address;
				if (Region.continuous == false)
					address = Region.pages[i];
				else
					address = adjust_pointer(Region.pages[0], virtual_memory::page_size * i);
				
				mContext.map(	address,
								adjust_pointer(Region.vAddress, virtual_memory::page_size * i),
								lightOS::context::write | lightOS::context::user);
			}
			KERNEL_CONTEXT_END;
			
			mRegions.push_back(Region);
			result = Region.vAddress;
		}
		else
		{
			LOG("process::create_memory_region(): Unknown memory region type");
		}
	}
	else
	{
		region Region;
		Region.size = pageCount;
		Region.vAddress = mLatestMemoryRegion;
		Region.physical = true;
		mLatestMemoryRegion = adjust_pointer(mLatestMemoryRegion, virtual_memory::page_size * pageCount);
		
		// Map the pages
		KERNEL_CONTEXT_START;
		for (size_t i = 0;i < Region.size;i++)
		{
			mContext.map(	adjust_pointer(physical, virtual_memory::page_size * i),
							adjust_pointer(Region.vAddress, virtual_memory::page_size * i),
							flags);
		}
		KERNEL_CONTEXT_END;
		
		mRegions.push_back(Region);
		result = Region.vAddress;
	}
	return result;
}
void process::free_memory_region(void *address)
{
	vector<region>::iterator i = mRegions.begin();
	for (;i != mRegions.end();i++)
		if ((*i).vAddress == address)
		{
			// Unmap the region
			KERNEL_CONTEXT_START;
			for (size_t y = 0;y < (*i).size;y++)
			{
				void *address = adjust_pointer((*i).vAddress, y * virtual_memory::page_size);
				mContext.unmap(address);
			}
			KERNEL_CONTEXT_END;
			
			if ((*i).physical != true)
			{
				// Free the region
				if (reinterpret_cast<uintptr_t>((*i).pages[0]) < 0x1000000)
				{
					// NOTE: This is always continuous
					range_allocator &RangeAllocator = range_allocator::instance();
					RangeAllocator.free_range(reinterpret_cast<uintptr_t>((*i).pages[0]), (*i).size);
				}
				else
				{
					// NOTE: This is never continuous
					page_allocator &PageAllocator = page_allocator::instance();
					for (size_t y = 0;y < (*i).size;y++)
						PageAllocator.free((*i).pages[y]);
				}
			}
			
			mRegions.erase(i);
			return;
		}
	ERROR("process::free_memory_region(): region starting at 0x" << hex(reinterpret_cast<uintptr_t>(address)) <<
        " not found");
}
void *process::allocate_shared_memory_region()
{
	void *address = 0;
	if (mFreeSharedMemory.size() != 0)
	{
		address = mFreeSharedMemory.back();
		mFreeSharedMemory.pop_back();
	}
	else
	{
		address = adjust_pointer(mLatestSharedMemory, - virtual_memory::page_size * 1024);
		mLatestSharedMemory = address;
	}
	return address;
}
void process::free_shared_memory_region(void *address)
{
	mFreeSharedMemory.push_back(address);
}
bool process::signal(size_t sig)
{
	thread *Thread = mList[0];
	thread_state &State = Thread->get_cpu_state();
	mSignalState = State;
	
	#ifdef X86
		State.eip = reinterpret_cast<uintptr_t>(mSignalHandler);
		
		context_handle_t ctx = processor::context();
		processor::context(mContext.get_handle());
		
		State.esp -= 4;
		int *stack = reinterpret_cast<int*>(State.esp);
		*stack = sig;
		State.esp -= 4;
		
		processor::context(ctx);
	#endif
	#ifdef X86_64
		State.rip = reinterpret_cast<uintptr_t>(mSignalHandler);
		State.rdi = sig;
	#endif
	
	return true;
}
void process::signal_end()
{
	thread *Thread = mList[0];
	thread_state &State = Thread->get_cpu_state();
	State = mSignalState;
}
process::~process()
{
	// Delete all threads
	for (vector<thread*>::iterator i = mList.begin();i != mList.end();i++)
	{
		destroyThread(*i);
	}
	
	// Free the process regions
	page_allocator &PageAllocator = page_allocator::instance();
	while (mRegions.size() != 0)
	{
		free_memory_region(mRegions[0].vAddress);
	}
	
	// Free all pages
	for (vector<void*>::iterator i = mPages.begin();i != mPages.end();i++)
		PageAllocator.free(*i);
}
