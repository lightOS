/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <cstdlib>
#include <libarch/ioport.hpp>
#include <lightOS/lightOS.hpp>
#include "serial.hpp"
using namespace lightOS;
using namespace std;
using namespace server;

using libkernel::message;

#define REG_TRANSMIT_BUFFER													0		// DLAB = 0, (w)
#define REG_RECEIVE_BUFFER													0		// DLAB = 0, (r)
#define REG_DIVISOR_LATCH_LOW												0		// DLAB = 1
#define REG_INTERRUPT_ENABLE												1		// DLAB = 0
#define REG_DIVISOR_LATCH_HIGH												1		// DLAB = 1
#define REG_FIFO_CONTROL													2		// (w)
#define REG_LINE_CONTROL													3
#define REG_LINE_STATUS														5

int main()
{
	serial Serial;
	
	Serial.loop();
	
	return 0;
}

serial::serial()
{
	// Request I/O Access
	// TODO FIXME HACK
	allocate_io_port_range(0, 0);
	
	// Request IRQ 3 (for serial1 and serial3) and IRQ 4 (for serial0 and serial2)
  m_interrupt_vector[0] = mPort.register_isa_irq(3, libkernel::irq_priority::serial);
  m_interrupt_vector[1] = mPort.register_isa_irq(4, libkernel::irq_priority::serial);
	
	// Get the serial I/O ports
	system_config sysCfg;
	get_system_configuration(sysCfg);
	
	// Save the serial I/O ports
	for (size_t i = 0;i < 4;i++)
	{
		mIOPort[i].first = sysCfg.serial[i];
		if (mIOPort[i].first != sysCfg.kernel_serial &&
			mIOPort[i].first != 0)
		{
			mIOPort[i].second = true;
			init(i);
		}
		else mIOPort[i].second = false;
	}
}
void serial::loop()
{
	message msg;
	while (mPort.get(msg))
	{
		if (msg.port() == libkernel::message_port::kernel &&
			msg.type() == libkernel::message::event &&
			msg.param1() == libkernel::event::irq)
		{
			if (msg.param2() == m_interrupt_vector[0])
				received(1, 3);
			else if (msg.param2() == m_interrupt_vector[1])
				received(0, 2);
		}
	}
}
void serial::init(size_t i)
{
	if (mIOPort[i].second == true)
	{
		// Disable all interrupts
		libarch::out8(mIOPort[i].first + REG_INTERRUPT_ENABLE, 0x00);
		
		// Set DLAB
		libarch::out8(mIOPort[i].first + REG_LINE_CONTROL, 0x80);
		
		// Set baud rate first low than high (38400 BPS)
		libarch::out8(mIOPort[i].first + REG_DIVISOR_LATCH_LOW, 0x03);
		libarch::out8(mIOPort[i].first + REG_DIVISOR_LATCH_HIGH, 0x00);
		
		// 8bits, no parity, 1 stop bit, clear DLAB
		libarch::out8(mIOPort[i].first + REG_LINE_CONTROL , 0x03);
		
		// FIFO Control Register
		libarch::out8(mIOPort[i].first + REG_FIFO_CONTROL , 0xC7);
		
		// Turn on DTR, RTS, and OUT2
		libarch::out8(mIOPort[i].first + 4 , 0x0B);
		
		// Enable interrupt when data arrived
		libarch::out8(mIOPort[i].first + REG_INTERRUPT_ENABLE, 0x01);
	}
}
bool serial::check_available(size_t i, uint8_t status)
{
	if ((status & 0x01) == 0x01)
	{
		cout << (char)libarch::in8(mIOPort[i].first + REG_RECEIVE_BUFFER);
		cout.flush();
		return true;
	}
	return false;
}
void serial::check_error(size_t i, uint8_t status)
{
	if ((status & 0x02) != 0)
		cerr << "serial#" << i << ": overrun error" << endl;
	if ((status & 0x04) != 0)
		cerr << "serial#" << i << ": parity error" << endl;
	if ((status & 0x08) != 0)
		cerr << "serial#" << i << ": framing error" << endl;
	if ((status & 0x10) != 0)
		cerr << "serial#" << i << ": break interrupt" << endl;
	if ((status & 0x80) != 0)
		cerr << "serial#" << i << ": error in received fifo" << endl;
}
void serial::received(size_t i, size_t y)
{
	uint8_t status1 = 0, status2 = 0;
	
	if (mIOPort[i].second == true)
	{
		status1 = libarch::in8(mIOPort[i].first + REG_LINE_STATUS);
		if (check_available(i, status1) == true)
			return;
	}
	
	if (mIOPort[y].second == true)
	{
		status2 = libarch::in8(mIOPort[y].first + REG_LINE_STATUS);
		if (check_available(y, status2) == true)
			return;
	}
	
	if (mIOPort[i].second == true)
		check_error(i, status1);
	
	if (mIOPort[y].second == true)
		check_error(y, status2);
}
