/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SERVER_SERIAL_SERIAL_HPP
#define SERVER_SERIAL_SERIAL_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup serial serial */
/*@{*/

#include <lightOS/lightOS.hpp>

namespace server
{
	/*! Driver for the serial line */
	class serial
	{
		public:
			/*! The constructor */
			serial();
			/*! The main loop */
			void loop();
			/*! The destructor */
			inline ~serial(){}
		private:
			/*! The copy-constructor
			 *\note NO implementation provided */
			serial(const serial &);
			/*! The = operator
			 *\note NO implementation provided */
			serial &operator = (const serial &);
			
			/*! Initialize a serial line
			 *\param[in] i the index of the serial line */
			void init(size_t i);
			/*! An IRQ occurred
			 *\param[in] i the first serial line on this irq
			 *\param[in] y the second serial line on this irq */
			void received(size_t i, size_t y);
			/*! Character available?
			 *\param[in] i the index of the serial line
			 *\param[in] status the status register
			 *\return true, if character available and processed, false otherwise */
			bool check_available(size_t i, uint8_t status);
			/*! Did an error occurr?
			 *\param[in] i the index of the serial line
			 *\param[in] status the status register */
			void check_error(size_t i, uint8_t status);
			
			/*! A lightOS IPC port */
			libkernel::message_port mPort;
			/*! The I/O ports of the serial lines */
			std::pair<uint16_t, bool> mIOPort[4];
      size_t m_interrupt_vector[2];
	};
}

/*@}*/
/*@}*/

#endif
