/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <string>
#include <iostream>
#include <lightOS/filesystem.hpp>
using namespace std;
using namespace lightOS;

using libkernel::message;

size_t index = 0;
std::string cache;

void commit_log(file &File)
{
	if (cache.length() != 0)
	{
		File.write(cache.c_str(), cache.length(), index);
		index += cache.length();
		cache.erase(cache.begin(), cache.end());
	}
}
bool log(file &File)
{
	// TODO FIXME HACK
	string log;// = getKernelLog();
	if (log.length() != 0)
	{
		bool i = false;
		for (size_t offset = 0;offset < log.length();offset++)
		{
			if (log[offset] == '\b')
			{
				if (i == false)
					--index;
				else
					cache.erase(cache.end() - 1, cache.end());
			}
			else
			{
				cache.append(1, log[offset]);
				i = true;
			}
		}
		if (cache.length() != 0 &&
			((index % 512) + cache.length()) >= 512)
		{
			commit_log(File);
		}
	}
	return true;
}

int main(int argc, char **argv)
{
	if (argc < 2)
	{
		cerr << "No log filename" << endl;
		return -1;
	}
	
	libkernel::message_port Port;
	if (Port.register_event(libkernel::event::log_update) == false ||
		Port.register_event(libkernel::event::timer, 10000) == false)
	{
		cerr << "klog: unable to register klog" << endl;
		return -1;
	}
	
	// Create the logfile
	file logFile;
	if (logFile.create(argv[1], file::block, access::write) == false)
	{
		cerr << "klog: failed to create logfile \"" << argv[1] << "\"" << endl;
		return -1;
	}
	log(logFile);
	
	message msg;
	while (Port.get(msg))
	{
		libkernel::shared_memory shm = msg.get_shared_memory();
		switch (msg.type())
		{
			case libkernel::message::event:
			{
				if (msg.param1() == libkernel::event::log_update)
				{
					log(logFile);
				}
				else if (msg.param1() == libkernel::event::timer)
				{
					commit_log(logFile);
					Port.register_event(libkernel::event::timer, 10000);
				}
			}break;
		}
	}
	
	return 0;
}
