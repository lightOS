/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/net.hpp>
#include <lightOS/debug.hpp>
#include "udp.hpp"
#include "dhcp.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

dhcp dhcp::mInstance;

bool dhcp::process_packet(	void *p,
							size_t size,
							lightOS::server::net::info &info,
							lightOS::net::ipv4_address source,
							uint16_t sourceport,
							lightOS::net::ipv4_address destination,
							uint16_t destport)
{
	packet *Packet = reinterpret_cast<packet*>(p);
	
	// Is BOOTP/DHCP client port?
	if (destport == lightOS::net::hton16(68) &&
		sourceport == lightOS::net::hton16(67))
	{
		if (Packet->operation != 2 ||
			Packet->hardware_type != 1 ||
			Packet->length != 6 ||
			Packet->id != info.dhcp_id() ||
			info.nic_info().ethernetAddress.compare_to(Packet->client_hw_address) != true)
		{
			DEBUG("dhcp: received an invalid packet");
			return false;
		}
		
		if (check_magic(Packet) == false)
		{
			DEBUG("dhcp: DHCP magic not found");
			return false;
		}
		
		size_t type = 0;
		if (get_message_type(Packet, type) == false)
		{
			DEBUG("dhcp: DHCP message type not found");
			return false;
		}
		
		// DHCP Offer & Waiting for DHCP Offer
		if (type == 0x02 &&
			info.dhcp_status() == lightOS::server::net::info::dhcp_wait_for_offer)
		{
			udp &Udp = udp::instance();
			
			// Get the server IP
			if (get_server_identifier(Packet, info.nic_info().ipv4Dhcp) == false)
			{
				if (Packet->server_address != 0)
					info.nic_info().ipv4Dhcp.mAddress = Packet->server_address;
				else
				{
					DEBUG("dhcp: DHCP Offer does not contain a server IP address");
					return false;
				}
			}
			
			// Get the client IP
			info.dhcp_ip(Packet->client_address);
			if (info.dhcp_ip() == 0)
			{
				DEBUG("dhcp: DHCP Offer does not contain a client IP address");
				return false;
			}
			
			DEBUG("dhcp: DHCP Offer received from " << info.nic_info().ipv4Dhcp);
			
			// Allocate & initialize the BOOTP packet
			packet *PacketReq = reinterpret_cast<packet*>(Udp.allocate_packet(sizeof(packet), info));
			memset(	PacketReq,
					0,
					sizeof(packet));
			
			PacketReq->operation = 1;							// BOOT Request
			PacketReq->hardware_type = 1;						// Ethernet
			PacketReq->length = 6;								// Length of the hardware address
			PacketReq->hops = 0;
			PacketReq->id = info.dhcp_id();
			PacketReq->secs = 0;
			PacketReq->flags = lightOS::net::hton16(0x8000);	// Hint to the DHCP-Server (SHOULD use broadcast)
			info.nic_info().ethernetAddress.to(PacketReq->client_hw_address);
			
			// Initialize the DHCP packet
			size_t offset;
			offset = add_magic(PacketReq);										// DHCP Magic
			offset = add_message_type(PacketReq, 3, offset);					// DHCP Request
			// DHCP Server Identifier
			offset = add_server_identifier(PacketReq, info.nic_info().ipv4Dhcp, offset);
			offset = add_requested_ip(PacketReq, info.dhcp_ip(), offset);				// DHCP Requested IP
			uint8_t parms[] = {0x01, 0x03, 0x1C, 0x06};
			offset = add_parameter_request_list(PacketReq, parms, 4, offset);	// DHCP Parameter Request list
			add_end(PacketReq, offset);											// DHCP End of Options
			
			// Set the NIC status
			info.dhcp_status(lightOS::server::net::info::dhcp_wait_for_ack);
			
			DEBUG("dhcp: Sending DHCP Request");
			
			// Send the DHCP request
			Udp.transmit_packet(PacketReq,
								sizeof(packet),
								info,
								68,
								lightOS::net::ipv4_address(0xFFFFFFFF),
								67);
		}
		// DHCP Ack & Waiting for DHCP Ack
		else if (	type == 0x05 &&
					info.dhcp_status() == lightOS::server::net::info::dhcp_wait_for_ack &&
					info.nic_info().ipv4Dhcp == source)
		{
			DEBUG("dhcp: DHCP Ack received");
			
			// Set the IPv4 address
			info.nic_info().ipv4Address = info.dhcp_ip();
			
			// Get the Subnet mask
			get_subnet_mask(Packet, info.nic_info().ipv4SubnetMask);
			
			// Get the Broadcast address
			if (get_broadcast_address(Packet, info.nic_info().ipv4BroadcastAddress) == false)
			{
				// Default Broadcast address
				info.nic_info().ipv4BroadcastAddress = (info.nic_info().ipv4Address & info.nic_info().ipv4SubnetMask) | (lightOS::net::ipv4_address(0xFFFFFFFF) & (~info.nic_info().ipv4SubnetMask));
			}
			
			// Get the DNS Server address
			get_dns_server(Packet, info.nic_info().ipv4Dns);
			
			// Get the Router
			get_router(Packet, info.nic_info().ipv4Router);
			
			// Set the NIC status
			info.dhcp_status(lightOS::server::net::info::dhcp_none);
		}
		else
		{
			DEBUG("dhcp: unexpected DHCP packet");
			return false;
		}
	}
	// Is BOOTP/DHCP server port?
	else if (	destport == lightOS::net::hton16(67) &&
				sourceport == lightOS::net::hton16(68))
	{
		DEBUG("dhcp: DHCP Request received");
	}
	else
	{
		DEBUG("dhcp: invalid dhcp packet");
		return false;
	}
	
	return true;
}
void dhcp::send_request(lightOS::server::net::info &info)
{
	udp &Udp = udp::instance();
	
	// Allocate & initialize the BOOTP packet
	packet *Packet = reinterpret_cast<packet*>(Udp.allocate_packet(sizeof(packet), info));
	memset(	Packet,
			0,
			sizeof(packet));
	
	Packet->operation = 1;							// BOOT Request
	Packet->hardware_type = 1;						// Ethernet
	Packet->length = 6;								// Length of the hardware address
	Packet->hops = 0;
	Packet->id = lightOS::net::hton32(rand());
	Packet->secs = 0;
	Packet->flags = lightOS::net::hton16(0x8000);	// Hint to the DHCP-Server (SHOULD use broadcast)
	info.nic_info().ethernetAddress.to(Packet->client_hw_address);
	
	// Initialize the DHCP packet
	size_t offset;
	offset = add_magic(Packet);											// DHCP Magic
	offset = add_message_type(Packet, 1, offset);						// DHCP Discover
	uint8_t parms[] = {0x01, 0x03, 0x1C, 0x06};
	offset = add_parameter_request_list(Packet, parms, 4, offset);		// DHCP Parameter Request list
	add_end(Packet, offset);											// DHCP End of Options
	
	// Set the NIC status
	info.dhcp_status(lightOS::server::net::info::dhcp_wait_for_offer);
	info.dhcp_id(Packet->id);
	
	DEBUG("dhcp: Sending DHCP Discover");
	
	// Send the DHCP request
	Udp.transmit_packet(Packet,
						sizeof(packet),
						info,
						68,
						lightOS::net::ipv4_address(0xFFFFFFFF),
						67);
}
size_t dhcp::add_magic(packet *Packet)
{
	Packet->options[0] = 0x63;
	Packet->options[1] = 0x82;
	Packet->options[2] = 0x53;
	Packet->options[3] = 0x63;
	return 4;
}
bool dhcp::check_magic(packet *Packet)
{
	if (Packet->options[0] != 0x63)return false;
	if (Packet->options[1] != 0x82)return false;
	if (Packet->options[2] != 0x53)return false;
	if (Packet->options[3] != 0x63)return false;
	return true;
}
size_t dhcp::add_message_type(packet *Packet, size_t type, size_t offset)
{
	Packet->options[offset++] = 53;		// DHCP Option type
	Packet->options[offset++] = 1;		// DHCP Option length
	Packet->options[offset++] = type;	// DHCP Message type
	return offset;
}
bool dhcp::get_message_type(packet *Packet, size_t &type)
{
	size_t offset = 4;
	for (;offset < 64;)
	{
		if (Packet->options[offset] == 53)
		{
			type = Packet->options[offset + 2];
			return true;
		}
		offset += Packet->options[offset + 1];
	}
	return false;
}
size_t dhcp::add_server_identifier(packet *Packet, lightOS::net::ipv4_address ip, size_t offset)
{
	Packet->options[offset++] = 54;
	Packet->options[offset++] = 4;
	Packet->options[offset++] = ip.mAddress & 0xFF;
	Packet->options[offset++] = (ip.mAddress >> 8) & 0xFF;
	Packet->options[offset++] = (ip.mAddress >> 16) & 0xFF;
	Packet->options[offset++] = (ip.mAddress >> 24) & 0xFF;
	return offset;
}
bool dhcp::get_server_identifier(packet *Packet, lightOS::net::ipv4_address &ip)
{
	size_t offset = 4;
	for (;offset < 64;)
	{
		if (Packet->options[offset] == 54)
		{
			ip.mAddress =	Packet->options[offset + 2] |
							(Packet->options[offset + 3] << 8) |
							(Packet->options[offset + 4] << 16) |
							(Packet->options[offset + 5] << 24);
			return true;
		}
		else if (Packet->options[offset] == 0xFF)return false;
		offset += Packet->options[offset + 1] + 2;
	}
	return false;
}
size_t dhcp::add_requested_ip(	packet *Packet,
								lightOS::net::ipv4_address ip,
								size_t offset)
{
	Packet->options[offset++] = 50;
	Packet->options[offset++] = 4;
	Packet->options[offset++] = ip.mAddress & 0xFF;
	Packet->options[offset++] = (ip.mAddress >> 8) & 0xFF;
	Packet->options[offset++] = (ip.mAddress >> 16) & 0xFF;
	Packet->options[offset++] = (ip.mAddress >> 24) & 0xFF;
	return offset;
}
size_t dhcp::add_parameter_request_list(packet *Packet,
										uint8_t *list,
										size_t size,
										size_t offset)
{
	Packet->options[offset++] = 55;
	Packet->options[offset++] = size;
	for (size_t i = 0;i < size;i++)
		Packet->options[offset++] = list[i];
	return offset;
}
bool dhcp::get_subnet_mask(packet *Packet, lightOS::net::ipv4_address &ip)
{
	size_t offset = 4;
	for (;offset < 64;)
	{
		if (Packet->options[offset] == 1)
		{
			ip.mAddress =	Packet->options[offset + 2] |
							(Packet->options[offset + 3] << 8) |
							(Packet->options[offset + 4] << 16) |
							(Packet->options[offset + 5] << 24);
			return true;
		}
		else if (Packet->options[offset] == 0xFF)return false;
		offset += Packet->options[offset + 1] + 2;
	}
	return false;
}
bool dhcp::get_broadcast_address(packet *Packet, lightOS::net::ipv4_address &ip)
{
	size_t offset = 4;
	for (;offset < 64;)
	{
		if (Packet->options[offset] == 28)
		{
			ip.mAddress =	Packet->options[offset + 2] |
							(Packet->options[offset + 3] << 8) |
							(Packet->options[offset + 4] << 16) |
							(Packet->options[offset + 5] << 24);
			return true;
		}
		else if (Packet->options[offset] == 0xFF)return false;
		offset += Packet->options[offset + 1] + 2;
	}
	return false;
}
bool dhcp::get_dns_server(packet *Packet, lightOS::net::ipv4_address &ip)
{
	size_t offset = 4;
	for (;offset < 64;)
	{
		if (Packet->options[offset] == 6)
		{
			ip.mAddress =	Packet->options[offset + 2] |
							(Packet->options[offset + 3] << 8) |
							(Packet->options[offset + 4] << 16) |
							(Packet->options[offset + 5] << 24);
			return true;
		}
		else if (Packet->options[offset] == 0xFF)return false;
		offset += Packet->options[offset + 1] + 2;
	}
	return false;
}
bool dhcp::get_router(packet *Packet, lightOS::net::ipv4_address &ip)
{
	size_t offset = 4;
	for (;offset < 64;)
	{
		if (Packet->options[offset] == 3)
		{
			ip.mAddress =	Packet->options[offset + 2] |
							(Packet->options[offset + 3] << 8) |
							(Packet->options[offset + 4] << 16) |
							(Packet->options[offset + 5] << 24);
			return true;
		}
		else if (Packet->options[offset] == 0xFF)return false;
		offset += Packet->options[offset + 1] + 2;
	}
	return false;
}
void dhcp::add_end(packet *Packet, size_t offset)
{
	Packet->options[offset] = 0xFF;
}
