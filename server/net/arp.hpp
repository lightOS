/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_SERVER_NET_ARP_HPP
#define LIGHTOS_SERVER_NET_ARP_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstdint>
#include <cstddef>
#include <vector>
#include <utility>
#include <lightOS/net.hpp>
#include "net.hpp"

namespace lightOS
{
	namespace server
	{
		class arp
		{
			public:
				static arp &instance(){return mInstance;}
				bool process_packet(void *p,
									size_t size,
									lightOS::server::net::info &info,
									lightOS::net::ethernet_address source,
									lightOS::net::ethernet_address destination);
				bool get_ethernet_address(	lightOS::net::ipv4_address addr,
											lightOS::net::ethernet_address &ethAddr,
											lightOS::server::net::info &info,
											lightOS::server::net::callback *callback);
				inline size_t cache_size() const{return mIpv4Cache.size();}
				inline std::pair<lightOS::net::ethernet_address, lightOS::net::ipv4_address> cache_entry(size_t i) const
				{
					if (i > mIpv4Cache.size())return std::pair<lightOS::net::ethernet_address, lightOS::net::ipv4_address>(lightOS::net::ethernet_address(), 0);
					return mIpv4Cache[i];
				}
				inline void clear_cache(){mIpv4Cache.clear();}
			private:
				static arp mInstance;
				
				std::vector<std::pair<lightOS::net::ethernet_address, lightOS::net::ipv4_address> > mIpv4Cache;
				std::vector<std::pair<lightOS::net::ipv4_address, lightOS::server::net::callback*> > mIpv4RequestCache;
				
				struct packet_v4
				{
					uint16_t hardware_type;
					uint16_t protocol_type;
					uint8_t hardware_length;
					uint8_t protocol_length;
					uint16_t operation;
					uint8_t sender_address[6];
					uint32_t sender_protocol_address;
					uint8_t receiver_address[6];
					uint32_t receiver_protocol_address;
				}__attribute__((packed));
		};
	}
}

/*@}*/

#endif
