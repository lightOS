/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/net.hpp>
#include "ip.hpp"
#include "arp.hpp"
#include "ethernet.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

ethernet ethernet::mInstance;

bool ethernet::process_frame(	void *pframe,
								size_t size,
								lightOS::server::net::info &info)
{
	if (size > 1518)return false;
	
	frame *Frame = reinterpret_cast<frame*>(pframe);
	void *data = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(pframe) + sizeof(frame));
	switch (lightOS::net::ntoh16(Frame->type))
	{
		case lightOS::net::ethernet_type::ipv4:
		{
			ipv4 &Ipv4 = ipv4::instance();
			return Ipv4.process_packet(	data,
										size - sizeof(frame),
										info,
										lightOS::net::ethernet_address(Frame->source),
										lightOS::net::ethernet_address(Frame->destination));
		}break;
		case lightOS::net::ethernet_type::arp:
		{
			arp &Arp = arp::instance();
			return Arp.process_packet(	data,
										size - sizeof(frame),
										info,
										lightOS::net::ethernet_address(Frame->source),
										lightOS::net::ethernet_address(Frame->destination));
		}break;
		case lightOS::net::ethernet_type::ipv6:
		{
			cout << "net: ipv6 packet" << endl;
		}break;
		default:
		{
			cout << "net: unknown ethernet packet type = 0x" << hex << Frame->type << endl;
		}break;
	}
	
	return true;
}
void *ethernet::allocate_frame(size_t size, lightOS::server::net::info &info)
{
	size += sizeof(frame);
	if (size > 1518)return 0;
	
	char *Frame = new char[size];
	return reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(Frame) + sizeof(frame));
}
void ethernet::transmit_frame(	void *packet,
								size_t size,
								lightOS::server::net::info &info,
								lightOS::net::ethernet_address &dest,
								uint16_t type)
{
	size += sizeof(frame);
	if (size > 1518)return;
	
	frame *Frame = reinterpret_cast<frame*>(reinterpret_cast<uintptr_t>(packet) - sizeof(frame));
	dest.to(Frame->destination);
	info.nic_info().ethernetAddress.to(Frame->source);
	Frame->type = lightOS::net::hton16(type);
	
	net::instance().transmit_frame(info.id(), Frame, size);
}
