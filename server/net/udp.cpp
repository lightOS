/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/net.hpp>
#include <lightOS/debug.hpp>
#include "udp.hpp"
#include "ip.hpp"
#include "dns.hpp"
#include "dhcp.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

udp udp::mInstance;

bool udp::process_packet(	void *p,
							size_t size,
							lightOS::server::net::info &info,
							lightOS::net::ipv4_address source,
							lightOS::net::ipv4_address destination)
{
	packet *Packet = reinterpret_cast<packet*>(p);
	
	// Calculate the checksum
	uint32_t pseudoheader[3];
	pseudoheader[0] = source.mAddress;
	pseudoheader[1] = destination.mAddress;
	pseudoheader[2] = (lightOS::net::hton16(size) << 16) | (17 << 8);
	size_t checksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(&pseudoheader[0]), 3 * 4);
	checksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(Packet), size, checksum, 3);
	checksum = lightOS::net::ones_complement_checksum_end(checksum);
	if (checksum != Packet->checksum)
	{
		DEBUG("udp: invalid checksum (calculated 0x" << hex << checksum << ", received 0x" << Packet->checksum << ")");
	}
	
	size_t dataSize = size - sizeof(packet);
	if (Packet->length < dataSize)dataSize = Packet->length;
	void *data = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(Packet) + sizeof(packet));
	
	// Is it a BOOTP/DHCP packet?
	if (Packet->destport == lightOS::net::ntoh16(67) ||
		Packet->destport == lightOS::net::ntoh16(68))
	{
		dhcp &Dhcp = dhcp::instance();
		Dhcp.process_packet(	data,
								dataSize,
								info,
								source,
								Packet->sourceport,
								destination,
								Packet->destport);
	}
	else if (Packet->destport == lightOS::net::ntoh16(53))
	{
		dns &Dns = dns::instance();
		Dns.process_packet(	data,
							dataSize,
							info,
							source,
							Packet->sourceport,
							destination,
							Packet->destport);
	}
	else
	{
		// Is there any socket handling this packet?
		for (size_t i = 0;i < mSockets.size();i++)
			if (mSockets[i]->mSrcPort == lightOS::net::ntoh16(Packet->destport))
			{
				libkernel::shared_memory shm(dataSize);
				memcpy(	shm.address<void>(),
						reinterpret_cast<void*>(data),
						shm.size());
				message reply(mSockets[i]->id(), libkernel::message::net_socket_receive, source.mAddress, shm, libkernel::shared_memory::transfer_ownership);
				lightOS::server::net &Net = lightOS::server::net::instance();
				Net.port().send(reply);
				return true;
			}
		
		DEBUG("udp: udp packet from ip " << source << " port " << dec << lightOS::net::ntoh16(Packet->sourceport) << " to ip " << destination << " port " << dec << lightOS::net::ntoh16(Packet->destport));
		return false;
	}
	
	return true;
}
void *udp::allocate_packet(size_t size, lightOS::server::net::info &info)
{
	size += sizeof(packet);
	void *p = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(ipv4::instance().allocate_packet(size, info)) + sizeof(packet));
	return p;
}
void udp::transmit_packet(	void *p,
							size_t size,
							lightOS::server::net::info &info,
							uint16_t srcport,
							lightOS::net::ipv4_address dest,
							uint16_t destport)
{
	// Initialize the UDP Header
	packet *Packet = reinterpret_cast<packet*>(reinterpret_cast<uintptr_t>(p) - sizeof(packet));
	Packet->sourceport = lightOS::net::hton16(srcport);
	Packet->destport = lightOS::net::hton16(destport);
	Packet->length = lightOS::net::hton16(size + sizeof(packet));
	
	// Calculate the checksum
	uint32_t pseudoheader[3];
	pseudoheader[0] = info.nic_info().ipv4Address.mAddress;
	pseudoheader[1] = dest.mAddress;
	pseudoheader[2] = (lightOS::net::hton16(size + sizeof(packet)) << 16) | (17 << 8);
	size_t checksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(&pseudoheader[0]), 3 * 4);
	checksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(Packet), size + sizeof(packet), checksum, 3);
	Packet->checksum = lightOS::net::ones_complement_checksum_end(checksum);
	
	// Send the packet
	ipv4 &Ipv4 = ipv4::instance();
	Ipv4.transmit_packet(	Packet,
							size + sizeof(packet),
							info,
							dest,
							lightOS::net::ip_type::udp);
}


udp::socket::socket(lightOS::socket::type Type, libkernel::port_id_t Port)
	: lightOS::server::net::socket(Type, Port)
{
	udp::instance().mSockets.push_back(this);
}
bool udp::socket::send(	libkernel::shared_memory &shm,
						lightOS::net::ipv4_address ip)
{
	bool result = false;
	if (mType == lightOS::socket::udp)
	{
		lightOS::server::net &Net = lightOS::server::net::instance();
		lightOS::server::net::info *Info = Net.get_network_info(mEth, ip);
		if (Info != 0)
		{
			udp &Udp = udp::instance();
			
			// Allocate and initialize the packet
			void *packet = reinterpret_cast<void*>(Udp.allocate_packet(shm.size(), *Info));
			memcpy(	packet,
					shm.address<void>(),
					shm.size());
			
			// Set IP address
			if (ip == 0)ip = mDstIp;
			
			if (ip != 0)
			{
				// Send the packet
				// TODO: Dest port?????
				Udp.transmit_packet(	packet,
										shm.size(),
										*Info,
										mSrcPort,
										ip,
										mDstPort);
				result = true;
			}
		}
	}
	else if (mType == lightOS::socket::raw_udp)
	{
		cerr << "udp::socket::send(): raw_udp not yet supported" << endl;
	}
	return result;
}
udp::socket::~socket()
{
	udp &Udp = udp::instance();
	std::vector<socket*>::iterator i = Udp.mSockets.begin();
	for (;i != Udp.mSockets.end();i++)
		if ((*i) == this)
			Udp.mSockets.erase(i);
}
