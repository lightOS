/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_SERVER_NET_ETHERNET_HPP
#define LIGHTOS_SERVER_NET_ETHERNET_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstdint>
#include <cstddef>
#include <lightOS/net.hpp>
#include "net.hpp"

namespace lightOS
{
	namespace server
	{
		class ethernet
		{
			public:
				static ethernet &instance(){return mInstance;}
				bool process_frame(	void *frame,
									size_t size,
									lightOS::server::net::info &info);
				void *allocate_frame(size_t size, lightOS::server::net::info &info);
				void transmit_frame(	void *packet,
										size_t size,
										lightOS::server::net::info &info,
										lightOS::net::ethernet_address &dest,
										uint16_t type);
			private:
				static ethernet mInstance;
				
				struct frame
				{
					uint8_t destination[6];
					uint8_t source[6];
					uint16_t type;
				}__attribute__((packed));
		};
	}
}

/*@}*/

#endif
