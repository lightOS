/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/

#include <cstdint>
#include <lightOS/pci.hpp>
#include <lightOS/net.hpp>
#include <lightOS/lightOS.hpp>

namespace lightOS
{
	namespace server
	{
		class pcnet
		{
			public:
				bool init();
				void handle();
				void deinit();
			private:
				void reset();
				void stop();
				void start();
				void dev_init(bool promiscuous);
				void send(void *data, size_t length);
				void wait_irq(size_t bitmask);
				uint16_t read_csr(size_t csr);
				void write_csr(size_t csr, uint16_t value);
				uint16_t read_bcr(size_t bcr);
				void write_bcr(size_t bcr, uint16_t value);
				
				enum
				{
					vendor_id	= 0x1022,
					device_id	= 0x2000
				};
				
				/* I/O registers */
				enum
				{
					APROM0				= 0x00,
					APROM2				= 0x02,
					APROM4				= 0x04,
					APROM6				= 0x06,
					APROM8				= 0x08,
					APROM10				= 0x0A,
					APROM12				= 0x0C,
					APROM14				= 0x0E,
					RDP					= 0x10,
					RAP					= 0x12,
					RESET				= 0x14,
					BDP					= 0x16
				};
				
				/* CSR registers */
				enum
				{
					csr_status			= 0,
					csr_interrupt_mask	= 3,
					csr_feature			= 4
				};
				
				/* CSR0 bits */
				enum
				{
					status_init					= 0x01,
					status_start				= 0x02,
					status_stop					= 0x04,
					status_transmit_demand		= 0x08,
					status_interrupt_enable		= 0x40,
					status_init_done			= 0x100,
					status_transmit_interrupt	= 0x200,
					status_receive_interrupt	= 0x400,
					status_error				= 0x8000
				};
				
				/* CSR4 bits */
				enum
				{
					feature_autostrip_receive	= 0x400,
					feature_autopad_transmit	= 0x800
				};
				
				/* CSR15 bits */
				enum
				{
					mode_promiscuous	= 0x8000
				};
				
				/* BCR registers */
				enum	
				{
					bcr_software_style	= 20
				};
				
				/* flags in the receive/transmit descriptors */
				enum
				{
					descriptor_own				= 0x80000000,
					descriptor_error			= 0x40000000,
					descriptor_packet_start		= 0x02000000,
					descriptor_packet_end		= 0x01000000
				};
				
				struct initialization_block
				{
					uint16_t	mode;
					uint8_t		receiveLength;
					uint8_t		transferLength;
					uint64_t	physicalAddress;
					uint64_t	logicalAddress;
					uint32_t	receiveDescriptor;
					uint32_t	transmitDescriptor;
				} __attribute__((packed));
				
				struct receive_descriptor
				{
					uint32_t	address;
					uint32_t	flags;
					uint32_t	flags2;
					uint32_t	res;
				} __attribute__((packed));
				
				struct transmit_descriptor
				{
					uint32_t	address;
					uint32_t	flags;
					uint32_t	flags2;
					uint32_t	res;
				} __attribute__((packed));
				
				libkernel::message_port mPort;
				lightOS::net::ethId mEthernetId;
				lightOS::pci::deviceAddr mDevAddr;
				lightOS::pci::deviceInfo mDevInfo;
				lightOS::net::ethernet_address mEthernetAddress;
				static initialization_block mInitBlock;
				receive_descriptor *mReceiveDescriptor;
				transmit_descriptor *mTransmitDescriptor;
				// NOTE: the ring buffer contain 128 descriptors
				void *mVReceiveDescriptor[128];
				void *mVTransmitDescriptor[128];
				size_t mLastReceiveDescriptor;
				size_t mLastTransmitDescriptor;
				size_t mLastTransmitDescriptorEvaluated;
        size_t m_interrupt_vector;
		};
	}
}
