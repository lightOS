/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <libarch/ioport.hpp>
#include <lightOS/debug.hpp>
#include "pcnet.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

// NOTE: static to enforce a reasonable alignment
// FIXME: Might be above 4GB in physical address space!
pcnet::initialization_block pcnet::mInitBlock;

int main()
{
	/* Initialize the NIC */
	pcnet Pcnet;
	if (Pcnet.init() == false)return -1;
	
	/* Handle everything */
	Pcnet.handle();
	
	/* Deinitialize the NIC */
	Pcnet.deinit();
}

bool pcnet::init()
{
	mPort.wait_for_other(libkernel::message_port::pci);
	mPort.wait_for_other(libkernel::message_port::net);
	
	// Find device
	mDevAddr = pci::findDevice(vendor_id, device_id, mPort);
	if (!mDevAddr)
	{
		cerr << "pcnet: no device found" << endl;
		return false;
	}
	DEBUG("pcnet: found device " << dec << mDevAddr.bus << ":" << mDevAddr.device << ":" << mDevAddr.function);
	
	// Enable Busmastering
	if (pci::enableBusmaster(mDevAddr, mPort) == false)
		return false;
	
	// Get device info
	mDevInfo = pci::getDeviceInfo(mDevAddr, mPort);
	DEBUG("pcnet: I/O 0x" << hex << mDevInfo.io_space[0]);
	DEBUG("pcnet: IRQ #" << dec << static_cast<size_t>(mDevInfo.irq));
	
	// Enable I/O Access
	// TODO FIXME HACK
	allocate_io_port_range(0, 0);
	
	// Request the irq
	m_interrupt_vector = mPort.register_pci_irq(mDevInfo.irq,
                                              mDevInfo.irq,
                                              mDevAddr.bus,
                                              mDevAddr.device,
                                              libkernel::irq_priority::network);
	
	// Read MAC address
	uint16_t word0 = libarch::in16(mDevInfo.io_space[0] + APROM0);
	uint16_t word1 = libarch::in16(mDevInfo.io_space[0] + APROM2);
	uint16_t word2 = libarch::in16(mDevInfo.io_space[0] + APROM4);
	mEthernetAddress = lightOS::net::ethernet_address(word0, word0 >> 8, word1, word1 >> 8, word2, word2 >> 8);
	DEBUG("pcnet: Ethernet address = " << mEthernetAddress);
	
	reset();
	stop();
	dev_init(false);
	start();
	
	// Register with the net server
	net::nic_info info;
	info.ethernetAddress = mEthernetAddress;
	mEthernetId = net::register_nic(info, mPort);
	DEBUG("pcnet: Registered with the net server, id = " << dec << mEthernetId);
	
	return true;
}
void pcnet::handle()
{
	message msg;
	while (mPort.get(msg))
	{
		libkernel::shared_memory shm = msg.get_shared_memory();
		switch (msg.type())
		{
			case libkernel::message::event:
			{
				if (msg.port() == libkernel::message_port::kernel &&
					msg.param1() == libkernel::event::irq)
				{
					size_t csr0 = read_csr(csr_status);
					if ((csr0 & status_error) != 0)
					{
						cerr << "pcnet: error (csr0 = " << read_csr(csr_status) << ")" << endl;
					}
					else if ((csr0 & status_transmit_interrupt) != 0)
					{
						// loop until all frames are captured
						while (	mLastTransmitDescriptorEvaluated != mLastTransmitDescriptor &&
								(mTransmitDescriptor[mLastTransmitDescriptorEvaluated].flags & descriptor_own) == 0)
						{
							if ((mTransmitDescriptor[mLastTransmitDescriptorEvaluated].flags & descriptor_error) != 0)
							{
								cerr << "pcnet: transmit error (descriptor flags 0x" << hex << mTransmitDescriptor[mLastTransmitDescriptorEvaluated].flags << endl;
							}
							//TODO: Send a reply to the net server
							mTransmitDescriptor[mLastTransmitDescriptorEvaluated].flags = 0;
							mTransmitDescriptor[mLastTransmitDescriptorEvaluated].flags2 = 0;
							++mLastTransmitDescriptorEvaluated;
							if (mLastTransmitDescriptorEvaluated == 128)mLastTransmitDescriptorEvaluated = 0;
						}
						// Clear the interrupt request
						write_csr(csr_status, status_interrupt_enable | status_transmit_interrupt);
					}
					else if ((csr0 & status_receive_interrupt) != 0)
					{
						// loop until all frames are captured
						while ((mReceiveDescriptor[mLastReceiveDescriptor].flags & descriptor_own) == 0)
						{
							if ((mReceiveDescriptor[mLastReceiveDescriptor].flags & descriptor_error) != 0 ||
								(mReceiveDescriptor[mLastReceiveDescriptor].flags & (descriptor_packet_start | descriptor_packet_end)) != (descriptor_packet_start | descriptor_packet_end))
							{
								cerr << "pcnet: receive error (descriptor flags 0x" << hex << mReceiveDescriptor[mLastReceiveDescriptor].flags << endl;
								destroy_process(0);
							}
							else
							{
								size_t size = mReceiveDescriptor[mLastReceiveDescriptor].flags2 & 0xFFFF;
								if (size > 64)size -= 4;
								net::nic_received_frame(mVReceiveDescriptor[mLastReceiveDescriptor],
														size,
														mPort);
							}
							mReceiveDescriptor[mLastReceiveDescriptor].flags = descriptor_own | 0xF7FF;
							mReceiveDescriptor[mLastReceiveDescriptor].flags2 = 0;
							mLastReceiveDescriptor++;
							if (mLastReceiveDescriptor == 128)mLastReceiveDescriptor = 0;
						}
						// Clear the interrupt request
						write_csr(csr_status, status_interrupt_enable | status_receive_interrupt);
					}
					else
					{
						DEBUG("pcnet: unknown interrupt source (csr0 = " << read_csr(csr_status) << ")");
					}
					
          mPort.acknoledge_irq(m_interrupt_vector);
				}
			}break;
			case libkernel::message::net_nic_transmit:
			{
				if (msg.port() == libkernel::message_port::net &&
					msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					send(shm.address<void>(), shm.size());
				}
			}break;
		}
	}
}
void pcnet::deinit()
{
	// Free the allocated receive & transmit buffer
	DEBUG("pcnet: Deallocate receive, transmit and descriptor buffer");
	free_region(&mVTransmitDescriptor[0]);
	free_region(&mVReceiveDescriptor[0]);
	free_region(mReceiveDescriptor);
	
	// TODO Stop device, unrequest irq, free the pci device
}
void pcnet::dev_init(bool promiscuous)
{
	// Allocate the receive & transmit descriptor buffer
	void *descriptor_region = allocate_region(4096, memory::below_4gb | memory::continuous);
	DEBUG("pcnet: Allocated descriptor buffer @ virtual 0x" << reinterpret_cast<uintptr_t>(descriptor_region) << " and @ physical 0x" << reinterpret_cast<uintptr_t>(get_physical_address(descriptor_region)));
	mReceiveDescriptor = reinterpret_cast<receive_descriptor*>(descriptor_region);
	mTransmitDescriptor = reinterpret_cast<transmit_descriptor*>(reinterpret_cast<uintptr_t>(descriptor_region) + 2 * 1024);
	
	// Fill the initialization block
	// NOTE: Transmit and receive buffer contain 128 entries
	memset(&mInitBlock, 0, sizeof(initialization_block));
	mInitBlock.mode = promiscuous ? mode_promiscuous : 0;
	mInitBlock.receiveLength = 0x70;
	mInitBlock.transferLength = 0x70;
	mInitBlock.receiveDescriptor = reinterpret_cast<uintptr_t>(get_physical_address(mReceiveDescriptor));
	mInitBlock.transmitDescriptor = reinterpret_cast<uintptr_t>(get_physical_address(mTransmitDescriptor));
	memcpy(&mInitBlock.physicalAddress, &mEthernetAddress.mAddress, 6);
	
	// Allocate the buffers
	void *p1 = allocate_region(64 * 4096, memory::below_4gb);
	void *p2 = allocate_region(64 * 4096, memory::below_4gb);
	DEBUG("pcnet: Allocated receive buffer @ virtual 0x" << reinterpret_cast<uintptr_t>(p1) << " and @ physical 0x" << reinterpret_cast<uintptr_t>(get_physical_address(p1)));
	DEBUG("pcnet: Allocated transmit buffer @ virtual 0x" << reinterpret_cast<uintptr_t>(p2) << " and @ physical 0x" << reinterpret_cast<uintptr_t>(get_physical_address(p2)));
	for (size_t i = 0;i < 128;i++)
	{
		mVReceiveDescriptor[i] = p1;
		mReceiveDescriptor[i].address = reinterpret_cast<size_t>(get_physical_address(p1));
		mReceiveDescriptor[i].flags = descriptor_own | 0xF7FF;
		mReceiveDescriptor[i].flags2 = 0;
		mReceiveDescriptor[i].res = 0;
		p1 = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(p1) + 2048);
		
		mVTransmitDescriptor[i] = p2;
		memset(&mTransmitDescriptor[i], 0, sizeof(transmit_descriptor));
		mTransmitDescriptor[i].address = reinterpret_cast<size_t>(get_physical_address(p2));
		p2 = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(p2) + 2048);
	}
	mLastTransmitDescriptor = 0;
	mLastTransmitDescriptorEvaluated = 0;
	mLastReceiveDescriptor = 0;
	
	// Get the physical address of the initialization block
	void *pmInitBlock = get_physical_address(&mInitBlock);
	// Set the address for the initialization block
	write_csr(1, reinterpret_cast<uintptr_t>(pmInitBlock));
	write_csr(2, reinterpret_cast<uintptr_t>(pmInitBlock) >> 16);
	
	// TODO BCR18.BREADE / BWRITE
	
	// CSR0.INIT, Initialize
	write_csr(csr_status, status_init | status_interrupt_enable);
	
	// Wait until initialization completed
	wait_irq(status_init_done);
	
	/* CSR4 Enable transmit auto-padding and receive automatic padding removal
	   TODO DMAPLUS? */
	write_csr(csr_feature, read_csr(csr_feature) | feature_autostrip_receive | feature_autopad_transmit);
	
	/* CSR46/47 Polling? */
	/* CSR80/82 ? */
	
	DEBUG("pcnet: Initialization done");
}
void pcnet::send(void *data, size_t length)
{
	if (mLastTransmitDescriptor > 127)mLastTransmitDescriptor = 0;
	
	// Copy data
	memcpy(	mVTransmitDescriptor[mLastTransmitDescriptor],
			data,
			length);
	
	// Setup the descriptor
	mTransmitDescriptor[mLastTransmitDescriptor].res = 0;
	mTransmitDescriptor[mLastTransmitDescriptor].flags2 = 0;
	size_t flags = descriptor_own | descriptor_packet_start | descriptor_packet_end | ((-length) & 0xFFFF);
	mTransmitDescriptor[mLastTransmitDescriptor].flags = flags;
	
	// Do the transfer
	write_csr(csr_status, status_interrupt_enable | status_transmit_demand);
	
	mLastTransmitDescriptor++;
	if (mLastTransmitDescriptor == 128)mLastTransmitDescriptor = 0;
}
void pcnet::wait_irq(size_t bitmask)
{
	message msg(0, libkernel::message::event, libkernel::event::irq, m_interrupt_vector);
	mPort.wait(msg);
	
	size_t csr0 = read_csr(csr_status);
	write_csr(csr_status, csr0);
	
  mPort.acknoledge_irq(m_interrupt_vector);
}
void pcnet::reset()
{
	// reset device
	libarch::in16(mDevInfo.io_space[0] + RESET);
	libarch::out16(mDevInfo.io_space[0] + RESET, 0);
	wait(10);
	
	// Set software style to PCnet-PCI 32bit
	write_bcr(bcr_software_style, 0x02);
}
void pcnet::start()
{
	write_csr(csr_status, status_start | status_interrupt_enable);
}
void pcnet::stop()
{
	write_csr(csr_status, status_stop);
}
uint16_t pcnet::read_csr(size_t csr)
{
	libarch::out16(mDevInfo.io_space[0] + RAP, csr);
	return libarch::in16(mDevInfo.io_space[0] + RDP);
}
void pcnet::write_csr(size_t csr, uint16_t value)
{
	libarch::out16(mDevInfo.io_space[0] + RAP, csr);
	libarch::out16(mDevInfo.io_space[0] + RDP, value);
}
uint16_t pcnet::read_bcr(size_t bcr)
{
	libarch::out16(mDevInfo.io_space[0] + RAP, bcr);
	return libarch::in16(mDevInfo.io_space[0] + BDP);
}
void pcnet::write_bcr(size_t bcr, uint16_t value)
{
	libarch::out16(mDevInfo.io_space[0] + RAP, bcr);
	libarch::out16(mDevInfo.io_space[0] + BDP, value);
}
