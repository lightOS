/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_SERVER_NET_UDP_HPP
#define LIGHTOS_SERVER_NET_UDP_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstdint>
#include <cstddef>
#include <lightOS/net.hpp>
#include "net.hpp"

namespace lightOS
{
	namespace server
	{
		class udp
		{
			public:
				class socket : public lightOS::server::net::socket
				{
					friend class udp;
					public:
						socket(lightOS::socket::type Type, libkernel::port_id_t Port);
						virtual bool send(	libkernel::shared_memory &shm,
											lightOS::net::ipv4_address ip);
						virtual ~socket();
					private:
				};
				
				friend class socket;
				
				static udp &instance(){return mInstance;}
				bool process_packet(void *p,
									size_t size,
									lightOS::server::net::info &info,
									lightOS::net::ipv4_address source,
									lightOS::net::ipv4_address destination);
				void *allocate_packet(size_t size, lightOS::server::net::info &info);
				void transmit_packet(	void *p,
										size_t size,
										lightOS::server::net::info &info,
										uint16_t srcport,
										lightOS::net::ipv4_address dest,
										uint16_t destport);
			private:
				static udp mInstance;
				
				std::vector<socket*> mSockets;
				
				struct packet
				{
					uint16_t sourceport;
					uint16_t destport;
					uint16_t length;
					uint16_t checksum;
				}__attribute__((packed));
		};
	}
}

/*@}*/

#endif
