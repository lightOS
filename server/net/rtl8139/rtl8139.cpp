/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <libarch/ioport.hpp>
#include <lightOS/debug.hpp>
#include "rtl8139.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

int main()
{
	/* Initialize the NIC */
	rtl8139 Rtl8139;
	if (Rtl8139.init() == false)return -1;
	
	/* Handle everything */
	Rtl8139.handle();
	
	/* Deinitialize the NIC */
	Rtl8139.deinit();
	
	return 0;
}

bool rtl8139::init()
{
	iTransmitBuffer = 0;
	iTransmitBufferEval = 0;
	iReceiveBuffer = 0;
	
	mPort.wait_for_other(libkernel::message_port::pci);
	mPort.wait_for_other(libkernel::message_port::net);
	
	// Find device
	mDevAddr = pci::findDevice(vendor_id, device_id, mPort);
	if (!mDevAddr)
	{
		cerr << "rtl8139: no device found" << endl;
		return false;
	}
	DEBUG("rtl8139: found device " << dec << mDevAddr.bus << ":" << mDevAddr.device << ":" << mDevAddr.function);
	
	// Enable Busmastering
	if (pci::enableBusmaster(mDevAddr, mPort) == false)
		return false;
	
	// Get device info
	mDevInfo = pci::getDeviceInfo(mDevAddr, mPort);
	DEBUG("rtl8139: I/O 0x" << hex << mDevInfo.io_space[0]);
	DEBUG("rtl8139: irq 0x" << static_cast<size_t>(mDevInfo.irq));
	
	// Enable I/O Access
	// TODO FIXME HACK
	allocate_io_port_range(0, 0);
	
	// Request the irq
  m_interrupt_vector = mPort.register_pci_irq(mDevInfo.irq,
                                              mDevInfo.irq,
                                              mDevAddr.bus,
                                              mDevAddr.device,
                                              libkernel::irq_priority::network);
	
	// Read MAC address
	uint8_t byte0 = libarch::in8(mDevInfo.io_space[0] + IDR0);
	uint8_t byte1 = libarch::in8(mDevInfo.io_space[0] + IDR1);
	uint8_t byte2 = libarch::in8(mDevInfo.io_space[0] + IDR2);
	uint8_t byte3 = libarch::in8(mDevInfo.io_space[0] + IDR3);
	uint8_t byte4 = libarch::in8(mDevInfo.io_space[0] + IDR4);
	uint8_t byte5 = libarch::in8(mDevInfo.io_space[0] + IDR5);
	mEthernetAddress = lightOS::net::ethernet_address(byte0, byte1, byte2, byte3, byte4, byte5);
	DEBUG("rtl8139: Ethernet address = " << mEthernetAddress);
	
	reset();
	dev_init(false);
	
	// Register with the net server
	net::nic_info info;
	info.ethernetAddress = mEthernetAddress;
	mEthernetId = net::register_nic(info, mPort);
	DEBUG("rtl8139: Registered with the net server, id = " << dec << mEthernetId);
	
	return true;
}
void rtl8139::handle()
{
	message msg;
	while (mPort.get(msg))
	{
		libkernel::shared_memory shm = msg.get_shared_memory();
		switch (msg.type())
		{
			case libkernel::message::event:
			{
				if (msg.port() == libkernel::message_port::kernel &&
					msg.param1() == libkernel::event::irq)
				{
					uint16_t intStatus = libarch::in16(mDevInfo.io_space[0] + ISR);
					if ((intStatus & ~(isr_rx_ok | isr_tx_ok)) != 0)
					{
						cerr << "rtl8139: error: interrupt status = 0x" << hex << intStatus << endl;
						destroy_process(0);
					}
					
					if ((intStatus & isr_tx_ok) == isr_tx_ok)
					{
						uint32_t txStatus;
						while (	iTransmitBufferEval != iTransmitBuffer  &&
								((txStatus = libarch::in32(mDevInfo.io_space[0] + 0x10 + iTransmitBufferEval * 4)) & tsr_own) == tsr_own)
						{
							if ((txStatus & tsr_tx_ok) != tsr_tx_ok)
							{
								cerr << "rtl8139: error: transmit failed" << endl;
								destroy_process(0);
							}
							++iTransmitBufferEval;
							if (iTransmitBufferEval == 4)iTransmitBuffer = 0;
						}
						libarch::out16(mDevInfo.io_space[0] + ISR, isr_tx_ok);
					}
					if ((intStatus & isr_rx_ok) == isr_rx_ok)
					{
						while ((libarch::in8(mDevInfo.io_space[0] + COMMAND) & cmd_rx_empty) == 0)
						{
							struct header
							{
								uint16_t flags;
								uint16_t length;
							}__attribute__((packed));
							
							// Receive successfull?
							header *Header = reinterpret_cast<header*>(reinterpret_cast<uintptr_t>(receiveBuffer) + iReceiveBuffer);
							if ((Header->flags & 0x01) != 0x01)
							{
								cerr << "rtl8139: error: receive failed (header = 0x" << hex << Header->flags << ", length = " << dec << Header->length << ")" << endl;
								destroy_process(0);
							}
							
							// Copy the packet to a temporary buffer
							uint8_t *frame = new uint8_t[Header->length];
							size_t cpysize = Header->length - 4;
							// FIXME: the 8Kb receive buffer size is hardcoded here
							if ((iReceiveBuffer + cpysize) > 0x2000)
							{
								size_t cpysize2 = (iReceiveBuffer + cpysize) % 0x2000;
								memcpy(	&frame[cpysize - cpysize2],
										receiveBuffer,
										cpysize2);
								cpysize -= cpysize2;
							}
							memcpy(	frame,
									reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(Header) + sizeof(header)),
									cpysize);
							
							// Send the packet to the net server
							net::nic_received_frame(frame,
													Header->length - 4,
													mPort);
							
							// Set the new CAPR
							iReceiveBuffer += 4 + Header->length + 3;
							iReceiveBuffer &= ~0x03;
							iReceiveBuffer %= 0x2000;
							// FIXME: wtf?
							libarch::out16(mDevInfo.io_space[0] + CAPR, iReceiveBuffer - 0x10);
							delete []frame;
						}
						libarch::out16(mDevInfo.io_space[0] + ISR, isr_rx_ok);
					}
					
          mPort.acknoledge_irq(m_interrupt_vector);
				}
			}break;
			case libkernel::message::net_nic_transmit:
			{
				if (msg.port() == libkernel::message_port::net &&
					msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					send(shm.address<void>(), shm.size());
				}
			}break;
		}
	}
}
void rtl8139::deinit()
{
	// Free the allocated receive & transmit buffer
	DEBUG("rtl8139: Deallocate receive and transmit buffer");
	free_region(receiveBuffer);
	free_region(transmitBuffer[0]);
	
	// TODO Stop device, unrequest irq, free the pci device
}
void rtl8139::reset()
{
	// reset device
	libarch::out8(mDevInfo.io_space[0] + COMMAND, cmd_reset);
	
	// Wait until reset is done
	while ((libarch::in8(mDevInfo.io_space[0] + COMMAND) & cmd_reset) == cmd_reset){}
}
void rtl8139::dev_init(bool promiscuous)
{
	// Enable the RX & TX
	libarch::out8(mDevInfo.io_space[0] + COMMAND, cmd_rx_enable | cmd_tx_enable);
	
	// Set the TCR (Transmit Configuration Register)
	libarch::out32(mDevInfo.io_space[0] + TCR, 0x03000700);
	
	// Set the RCR (Receive Configuration Register)
	libarch::out32(mDevInfo.io_space[0] + RCR, 0x0000070a);
	
	// Allocate the receive & transmit buffer
	// NOTE: receive buffer is 8KB + 16byte
	// NOTE: transmit buffer is 4 * 2KB
	receiveBuffer = allocate_region(12 * 1024, memory::below_16mb | memory::continuous);
	DEBUG("rtl8139: Allocated receive buffer @ virtual 0x" << reinterpret_cast<uintptr_t>(receiveBuffer) << " and @ physical 0x" << reinterpret_cast<uintptr_t>(get_physical_address(receiveBuffer)));
	transmitBuffer[0] = allocate_region(8 * 1024, memory::below_4gb);
	transmitBuffer[1] = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(transmitBuffer[0]) + 2048);
	transmitBuffer[2] = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(transmitBuffer[1]) + 2048);
	transmitBuffer[3] = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(transmitBuffer[2]) + 2048);
	DEBUG("rtl8139: Allocated transmit buffer @ virtual 0x" << reinterpret_cast<uintptr_t>(transmitBuffer[0]) << " and @ physical 0x" << reinterpret_cast<uintptr_t>(get_physical_address(transmitBuffer[0])));
	
	// Set the receive buffer
	libarch::out32(mDevInfo.io_space[0] + RBSTART, reinterpret_cast<size_t>(get_physical_address(receiveBuffer)));
	
	// Set the transmit buffer
	libarch::out32(mDevInfo.io_space[0] + TSAD0, reinterpret_cast<uintptr_t>(get_physical_address(transmitBuffer[0])));
	libarch::out32(mDevInfo.io_space[0] + TSAD1, reinterpret_cast<uintptr_t>(get_physical_address(transmitBuffer[1])));
	libarch::out32(mDevInfo.io_space[0] + TSAD2, reinterpret_cast<uintptr_t>(get_physical_address(transmitBuffer[2])));
	libarch::out32(mDevInfo.io_space[0] + TSAD3, reinterpret_cast<uintptr_t>(get_physical_address(transmitBuffer[3])));
	
	// Set the IMR (Interrupt Mask Register)
	libarch::out16(mDevInfo.io_space[0] + IMR, 0xFFFF);
	
	DEBUG("rtl8139: Initialization done");
}
void rtl8139::send(void *data, size_t length)
{
	if (iTransmitBuffer >= 4)iTransmitBuffer = 0;
	
	// Copy the packet
	memcpy(	transmitBuffer[iTransmitBuffer],
			data,
			length);
	
	// Set the descriptor status
	uint32_t status = length;
	libarch::out32(mDevInfo.io_space[0] + 0x10 + iTransmitBuffer * 4, status);
	
	++iTransmitBuffer;
}
