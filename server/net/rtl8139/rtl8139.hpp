/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/

#include <cstdint>
#include <lightOS/pci.hpp>
#include <lightOS/net.hpp>
#include <lightOS/lightOS.hpp>

namespace lightOS
{
	namespace server
	{
		class rtl8139
		{
			public:
				bool init();
				void handle();
				void deinit();
			private:
				void reset();
				void dev_init(bool promiscuous);
				void send(void *data, size_t length);
				
				enum
				{
					vendor_id	= 0x10EC,
					device_id	= 0x8139
				};
				
				/* I/O registers */
				enum
				{
					IDR0				= 0x00,
					IDR1				= 0x01,
					IDR2				= 0x02,
					IDR3				= 0x03,
					IDR4				= 0x04,
					IDR5				= 0x05,
					TSAD0				= 0x20,
					TSAD1				= 0x24,
					TSAD2				= 0x28,
					TSAD3				= 0x2C,
					RBSTART				= 0x30,
					COMMAND				= 0x37,
					CAPR				= 0x38,
					IMR					= 0x3C,
					ISR					= 0x3E,
					TCR					= 0x40,
					RCR					= 0x44
				};
				
				/* Command register */
				enum
				{
					cmd_rx_empty		= 0x01,
					cmd_tx_enable		= 0x04,
					cmd_rx_enable		= 0x08,
					cmd_reset			= 0x10
				};
				
				/* Interrupt status register */
				enum
				{
					isr_rx_ok			= 0x01,
					isr_tx_ok			= 0x04
				};
				
				/* Transmit status register */
				enum
				{
					tsr_own				= 0x2000,
					tsr_tx_ok			= 0x8000
				};
				
				libkernel::message_port mPort;
				lightOS::net::ethId mEthernetId;
				lightOS::pci::deviceAddr mDevAddr;
				lightOS::pci::deviceInfo mDevInfo;
				lightOS::net::ethernet_address mEthernetAddress;
				size_t iTransmitBuffer;
				size_t iTransmitBufferEval;
				void *transmitBuffer[4];
				size_t iReceiveBuffer;
				void *receiveBuffer;
        size_t m_interrupt_vector;
		};
	}
}
