/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstdlib>
#include <iostream>
#include <lightOS/net.hpp>
#include <lightOS/debug.hpp>
#include "udp.hpp"
#include "dns.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

dns dns::mInstance;

bool dns::process_packet(	void *p,
							size_t size,
							lightOS::server::net::info &info,
							lightOS::net::ipv4_address source,
							uint16_t soureport,
							lightOS::net::ipv4_address destination,
							uint16_t destport)
{
	packet *Packet = reinterpret_cast<packet*>(p);
	
	// TODO: Check transferId
	if ((lightOS::net::ntoh16(Packet->flags) & 0x8000) == 0)
	{
		DEBUG("dns: received a DNS request");
		return false;
	}
	if ((lightOS::net::ntoh16(Packet->flags) & 0x7800) != 0)
	{
		DEBUG("dns: receive a non-standard query");
		return false;
	}
	
	bool error = false;
	if ((lightOS::net::ntoh16(Packet->flags) & 0x0F) != 0)
		error = true;
	
	// Skip the questions
	char *question = reinterpret_cast<char*>(reinterpret_cast<uintptr_t>(Packet) + sizeof(packet));
	for (size_t i = 0;i < lightOS::net::ntoh16(Packet->qcount);i++)
	{
		if (error == true)
		{
			// Get domain name
			string domain = get_domain_name(question);
			
			DEBUG("dns: received reply for \"" << domain << "\", no IP found (error = 0x" << hex << (lightOS::net::ntoh16(Packet->flags) & 0x0F) << ")");
			
			// Broadcast DNS reply
			broadcast_result(domain, 0, false);
		}
		size_t length = strlen(question);
		question = reinterpret_cast<char*>(reinterpret_cast<uintptr_t>(question) + length + 4 + 1);
	}
	
	// FIXME: Somehow crashes on replies with more than one answer
	if (lightOS::net::ntoh16(Packet->acount) > 1)
	{
		DEBUG("dns: DNS reply with more than one answer, only processing first answer");
		Packet->acount = lightOS::net::hton16(1);
	}
	
	// Process the answers
	char *answer = question;
	for (size_t i = 0;i < lightOS::net::ntoh16(Packet->acount);i++)
	{
		// Get domain name
		char *this_answer = reinterpret_cast<char*>(reinterpret_cast<uintptr_t>(Packet) + (lightOS::net::ntoh16(*reinterpret_cast<uint16_t*>(answer)) & (~0xC000)));
		string domain = get_domain_name(this_answer);
		
		// Get IP
		ressource_header *resHeader = reinterpret_cast<ressource_header*>(reinterpret_cast<uintptr_t>(answer) + 2);
		lightOS::net::ipv4_address Addr(*reinterpret_cast<uint32_t*>(reinterpret_cast<uintptr_t>(resHeader) + sizeof(ressource_header)));
		
		DEBUG("dns: received reply for \"" << domain << "\", IP is " << Addr);
		
		// Add to Cache
		// TODO: what about the TTL ;)
		mCache.push_back(pair<string, lightOS::net::ipv4_address>(domain, Addr));
		
		// Broadcast DNS reply
		broadcast_result(domain, Addr, true);
		
		// Next answer
		answer = reinterpret_cast<char*>(reinterpret_cast<uintptr_t>(this_answer) + sizeof(ressource_header) + lightOS::net::ntoh16(resHeader->size));
	}
	return true;
}
bool dns::get_ip(	const std::string &name,
					lightOS::net::ipv4_address &addr,
					lightOS::server::net::info &info,
					libkernel::port_id_t port)
{
	// Go through the Cache
	for (size_t i = 0;i < mCache.size();i++)
		if (mCache[i].first == name)
		{
			addr = mCache[i].second;
			return true;
		}
	
	size_t size = sizeof(packet) + name.length() + 6;
	udp &Udp = udp::instance();
	
	// Allocate & initialize the DNS packet
	packet *Packet = reinterpret_cast<packet*>(Udp.allocate_packet(size, info));
	memset(	Packet,
			0,
			size);
	
	Packet->id = lightOS::net::hton16(rand());
	Packet->flags = lightOS::net::hton16(0x100);
	Packet->qcount = lightOS::net::hton16(1);
	
	// Add the question to the DNS packet
	char *question = reinterpret_cast<char*>(reinterpret_cast<uintptr_t>(Packet) + sizeof(packet));
	strcpy(	question + 1,
			name.c_str());
	char *count = question;
	++question;
	while (*question != '\0')
	{
		if (*question == '.')
		{
			*question = 0;
			count = question;
		}
		else ++*count;
		++question;
	}
	++question;
	*reinterpret_cast<uint16_t*>(question) = lightOS::net::hton16(1);
	*reinterpret_cast<uint16_t*>(question + 2) = lightOS::net::hton16(1);
	
	DEBUG("dns: sending DNS request for \"" << name << "\"");
	
	// Send the DNS request
	Udp.transmit_packet(Packet,
						size,
						info,
						53,
						info.nic_info().ipv4Dns,
						53);
	
	// Add to DNS reply queue
	mRequestCache.push_back(pair<string, libkernel::port_id_t>(name, port));
	
	return false;
}
string dns::get_domain_name(const char *enc)
{
	string result;
	while (*enc != 0)
	{
		if (result.length() != 0)result.append(".");
		result.append(enc + 1, static_cast<size_t>(static_cast<unsigned char>(*enc)));
		enc += static_cast<unsigned char>(*enc) + 1;
	}
	return result;
}
void dns::broadcast_result(	std::string &domain,
							lightOS::net::ipv4_address addr,
							bool result)
{
	// Any matching DNS requests?
	vector<pair<string, libkernel::port_id_t> >::iterator i = mRequestCache.begin();
	for (;i != mRequestCache.end();i++)
		if ((*i).first == domain)
		{
			if ((*i).second != 0)
			{
				message reply((*i).second, libkernel::message::net_resolve, static_cast<size_t>(result), addr.mAddress);
				net::instance().port().send(reply);
			}
			mRequestCache.erase(i);
			break;
		}
}
