/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_SERVER_NET_DHCP_HPP
#define LIGHTOS_SERVER_NET_DHCP_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstdint>
#include <cstddef>
#include <lightOS/net.hpp>
#include "net.hpp"

namespace lightOS
{
	namespace server
	{
		class dhcp
		{
			public:
				static dhcp &instance(){return mInstance;}
				bool process_packet(void *p,
									size_t size,
									lightOS::server::net::info &info,
									lightOS::net::ipv4_address source,
									uint16_t soureport,
									lightOS::net::ipv4_address destination,
									uint16_t destport);
				void send_request(lightOS::server::net::info &info);
			private:
				static dhcp mInstance;
				
				struct packet
				{
					uint8_t operation;
					uint8_t hardware_type;
					uint8_t length;
					uint8_t hops;
					uint32_t id;
					uint16_t secs;
					uint16_t flags;
					uint32_t old_client_address;
					uint32_t client_address;
					uint32_t server_address;
					uint32_t gateway_address;
					uint8_t client_hw_address[16];
					uint8_t name[64];
					uint8_t file[128];
					uint8_t options[64];
				}__attribute__((packed));
				
				size_t add_magic(packet *Packet);
				size_t add_message_type(packet *Packet, size_t type, size_t offset);
				size_t add_server_identifier(	packet *Packet,
												lightOS::net::ipv4_address ip,
												size_t offset);
				size_t add_requested_ip(packet *Packet,
										lightOS::net::ipv4_address ip,
										size_t offset);
				size_t add_parameter_request_list(	packet *Packet,
													uint8_t *list,
													size_t size,
													size_t offset);
				void add_end(packet *Packet, size_t offset);
				
				bool check_magic(packet *Packet);
				bool get_message_type(packet *Packet, size_t &type);
				bool get_server_identifier(packet *Packet, lightOS::net::ipv4_address &ip);
				bool get_subnet_mask(packet *Packet, lightOS::net::ipv4_address &ip);
				bool get_broadcast_address(packet *Packet, lightOS::net::ipv4_address &ip);
				bool get_dns_server(packet *Packet, lightOS::net::ipv4_address &ip);
				bool get_router(packet *Packet, lightOS::net::ipv4_address &ip);
		};
	}
}

/*@}*/

#endif
