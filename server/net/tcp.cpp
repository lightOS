/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/net.hpp>
#include <lightOS/debug.hpp>
#include "tcp.hpp"
#include "ip.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

tcp tcp::mInstance;

bool tcp::process_packet(	void *p,
							size_t size,
							lightOS::server::net::info &info,
							lightOS::net::ipv4_address source,
							lightOS::net::ipv4_address destination)
{
	packet *Packet = reinterpret_cast<packet*>(p);
	
	// Check the checksum
	uint16_t chksum = checksum(Packet, size, source, destination);
	if (chksum != Packet->checksum)
	{
		DEBUG("tcp: invalid checksum (calculated 0x" << hex << chksum << ", received 0x" << Packet->checksum << ")");
	}
	
	// Find the socket
	socket *Socket = 0;
	for (size_t i = 0;i < mSockets.size();i++)
		if (mSockets[i]->mSrcPort == lightOS::net::ntoh16(Packet->destport))
			if (mSockets[i]->mDstPort == lightOS::net::ntoh16(Packet->sourceport))
				Socket = mSockets[i];
	
	// Socket found?
	if (Socket != 0)
	{
		if (Socket->mStatus == socket::handshake)
		{
			// Is it a SYN-ACK Packet?
			if ((Packet->flags & (syn | ack)) != (syn | ack) ||
				(Packet->flags & (~(syn | ack))) != 0)
			{
				DEBUG("tcp: not a SYN/ACK packet");
				return false;
			}
			
			// Correct Ack number?
			if (lightOS::net::ntoh32(Packet->ack_number) != Socket->mSequenceNumber)
			{
				DEBUG("tcp: wrong sequence number (expected 0x" << hex << Socket->mSequenceNumber << ", received 0x" << hex << lightOS::net::ntoh32(Packet->ack_number));
				return false;
			}
			
			// Save Sequence number
			Socket->mAckNumber = lightOS::net::ntoh32(Packet->sequence_number) + 1;
			
			// Initialize TCP ACK Packet
			size_t size2 = sizeof(packet);
			packet *Packet2 = reinterpret_cast<packet*>(ipv4::instance().allocate_packet(size2, info));
			memset(Packet2, 0, size);
			Packet2->sourceport = lightOS::net::hton16(Socket->mSrcPort);
			Packet2->destport = lightOS::net::hton16(Socket->mDstPort);
			Packet2->sequence_number = lightOS::net::hton32(Socket->mSequenceNumber);
			Packet2->ack_number = lightOS::net::hton32(Socket->mAckNumber);
			Packet2->offset = (size2 / 4) << 4;
			Packet2->flags = ack;
			Packet2->window = lightOS::net::hton16(Socket->mWindow);
			Packet2->checksum = checksum(Packet2, size2, info.nic_info().ipv4Address , source);
			
			// Connection established
			Socket->mStatus = socket::established;
			
			DEBUG("tcp: connection established");
			
			// Send the Packet
			transmit_raw_packet(Packet2,
								size2,
								info,
								source);
			
			// Report to the socket owner
			message event(Socket->id(), libkernel::message::net_socket_connect, static_cast<size_t>(true));
			net::instance().port().send(event);
		}
		else
		{
			DEBUG("tcp: out of context packet, flags = 0x" << hex << (size_t)Packet->flags);
		}
	}
	// No socket found
	else
	{
		DEBUG("tcp: no socket found");
		return false;
	}
	
	return true;
}
void *tcp::allocate_packet(size_t size, lightOS::server::net::info &info)
{
	size += sizeof(packet);
	void *p = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(ipv4::instance().allocate_packet(size, info)) + sizeof(packet));
	return p;
}
void tcp::transmit_raw_packet(	packet *Packet,
								size_t size,
								lightOS::server::net::info &info,
								lightOS::net::ipv4_address dst)
{
	ipv4 &Ipv4 = ipv4::instance();
	Ipv4.transmit_packet(	Packet,
							size,
							info,
							dst,
							lightOS::net::ip_type::tcp);
}
void tcp::transmit_packet(	void *p,
							size_t size,
							lightOS::server::net::info &info,
							uint16_t srcport,
							lightOS::net::ipv4_address dest,
							uint16_t destport)
{
/*	// Initialize the UDP Header
	packet *Packet = reinterpret_cast<packet*>(reinterpret_cast<uintptr_t>(p) - sizeof(packet));
	Packet->sourceport = lightOS::net::hton16(srcport);
	Packet->destport = lightOS::net::hton16(destport);
	Packet->length = lightOS::net::hton16(size + sizeof(packet));
	
	// Calculate the checksum
	uint32_t pseudoheader[3];
	pseudoheader[0] = info.nic_info().ipv4Address.mAddress;
	pseudoheader[1] = dest.mAddress;
	pseudoheader[2] = (lightOS::net::hton16(size + sizeof(packet)) << 16) | (17 << 8);
	size_t checksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(&pseudoheader[0]), 3 * 4);
	checksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(Packet), size + sizeof(packet), checksum, 3);
	Packet->checksum = lightOS::net::ones_complement_checksum_end(checksum);
	
	// Send the packet
	ipv4 &Ipv4 = ipv4::instance();
	Ipv4.transmit_packet(	Packet,
							size + sizeof(packet),
							info,
							dest,
							lightOS::net::ip_type::udp);*/
}
uint16_t tcp::checksum(	packet *Packet,
						size_t size,
						lightOS::net::ipv4_address &src,
						lightOS::net::ipv4_address &dst)
{
	uint32_t pseudoheader[3];
	pseudoheader[0] = src.mAddress;
	pseudoheader[1] = dst.mAddress;
	pseudoheader[2] = (lightOS::net::hton16(size) << 16) | (6 << 8);
	size_t checksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(&pseudoheader[0]), 3 * 4);
	checksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(Packet), size, checksum, 8);
	return lightOS::net::ones_complement_checksum_end(checksum);
}
tcp::socket::socket(lightOS::socket::type Type, libkernel::port_id_t Port)
	:	lightOS::server::net::socket(Type, Port), mWindow(5840), mMaxSegmentSize(0), mStatus(socket::none), mSequenceNumber(0),
		mAckNumber(0)
{
	tcp::instance().mSockets.push_back(this);
}
bool tcp::socket::bind(	lightOS::net::ethId eth,
						uint16_t srcPort,
						lightOS::net::ipv4_address dstIp,
						uint16_t dstPort)
{
	if (srcPort == lightOS::net::port_random)
	{
		tcp &Tcp = tcp::instance();
		srcPort = Tcp.mLastPort++;
	}
	return lightOS::server::net::socket::bind(eth, srcPort, dstIp, dstPort);
}
bool tcp::socket::connect(	lightOS::net::ipv4_address dstIp,
							uint16_t dstPort)
{
	if (mStatus != socket::none)return false;
	
	if (dstIp.mAddress != lightOS::net::ip_any)
		mDstIp = dstIp;
	if (dstPort != lightOS::net::port_any)
		mDstPort = dstPort;
	
	if (mDstIp.mAddress == lightOS::net::ip_any)
	{
		DEBUG("tcp: socket::connect(): no ip");
		return false;
	}
	if (mDstPort == lightOS::net::port_any)
	{
		DEBUG("tcp: socket::connect(): no port");
		return false;
	}
	
	// Get network info
	lightOS::server::net::info *Info = net::instance().get_network_info(mEth, mDstIp);
	if (Info == 0)
	{
		DEBUG("tcp: Target IP unreachable");
		return false;
	}
	
	// Initialize TCP SYN Packet
	tcp &Tcp = tcp::instance();
	size_t size = sizeof(packet);
	packet *Packet = reinterpret_cast<packet*>(ipv4::instance().allocate_packet(size, *Info));
	memset(Packet, 0, size);
	Packet->sourceport = lightOS::net::hton16(mSrcPort);
	Packet->destport = lightOS::net::hton16(mDstPort);
	mSequenceNumber = rand();
	Packet->sequence_number = lightOS::net::hton32(mSequenceNumber);
	++mSequenceNumber;
	Packet->offset = (size / 4) << 4;
	Packet->flags = syn;
	Packet->window = lightOS::net::hton16(mWindow);
	Packet->checksum = Tcp.checksum(Packet, size, Info->nic_info().ipv4Address , mDstIp);
	
	// Set the socket status
	mStatus = socket::handshake;
	
	// Send the Packet
	Tcp.transmit_raw_packet(Packet,
							size,
							*Info,
							mDstIp);
	
	return true;
}
bool tcp::socket::send(	libkernel::shared_memory &shm,
						lightOS::net::ipv4_address ip)
{
	bool result = false;
	if (mType == lightOS::socket::tcp)
	{
		DEBUG("tcp::socket::send");
		/*lightOS::server::net &Net = lightOS::server::net::instance();
		lightOS::server::net::info *Info = Net.get_network_info(mEth, ip);
		if (Info != 0)
		{
			udp &Udp = udp::instance();
			
			// Allocate and initialize the packet
			void *packet = reinterpret_cast<void*>(Udp.allocate_packet(shm.size(), *Info));
			memcpy(	packet,
					shm.address<void>(),
					shm.size());
			
			// Set IP address
			if (ip == 0)ip = mDstIp;
			
			if (ip != 0)
			{
				// Send the packet
				// TODO: Dest port?????
				Udp.transmit_packet(	packet,
										shm.size(),
										*Info,
										mSrcPort,
										ip,
										mDstPort);
				result = true;
			}
		}*/
	}
	else if (mType == lightOS::socket::raw_tcp)
	{
		cerr << "tcp::socket::send(): raw_tcp not yet supported" << endl;
	}
	return result;
}
bool tcp::socket::disconnect()
{
	// Is connection established?
	if (mStatus != socket::established)return false;
	
	// Get network info
	lightOS::server::net::info *Info = net::instance().get_network_info(mEth, mDstIp);
	if (Info == 0)
	{
		DEBUG("tcp: Target IP unreachable");
		return false;
	}
	
	// Initialize TCP FIN Packet
	tcp &Tcp = tcp::instance();
	size_t size = sizeof(packet);
	packet *Packet = reinterpret_cast<packet*>(ipv4::instance().allocate_packet(size, *Info));
	memset(Packet, 0, size);
	Packet->sourceport = lightOS::net::hton16(mSrcPort);
	Packet->destport = lightOS::net::hton16(mDstPort);
	Packet->sequence_number = lightOS::net::hton32(mSequenceNumber);
	++mSequenceNumber;
	Packet->offset = (size / 4) << 4;
	Packet->flags = fin;
	Packet->window = lightOS::net::hton16(mWindow);
	Packet->checksum = Tcp.checksum(Packet, size, Info->nic_info().ipv4Address , mDstIp);
	
	// Set the socket status
	mStatus = socket::disconnecting;
	DEBUG("tcp: disconnecting");
	
	// Send the Packet
	Tcp.transmit_raw_packet(Packet,
							size,
							*Info,
							mDstIp);
	
	return true;
}
tcp::socket::~socket()
{
	tcp &Tcp = tcp::instance();
	std::vector<socket*>::iterator i = Tcp.mSockets.begin();
	for (;i != Tcp.mSockets.end();i++)
		if ((*i) == this)
			Tcp.mSockets.erase(i);
}
