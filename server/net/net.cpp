/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/config.hpp>
#include <lightOS/lightOS.hpp>
#include "net.hpp"
#include "arp.hpp"
#include "ip.hpp"
#include "udp.hpp"
#include "tcp.hpp"
#include "dns.hpp"
#include "dhcp.hpp"
#include "ethernet.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

int main(int argc, char **argv)
{
	server::net &Net = server::net::instance();
	
	// Initialize the random number generator
	srand(get_tick_count());
	
	// Parse the config file
	config::section<config::key_value_pair> *configFile = config::parse<config::key_value_pair>("/system/config/net.cfg");
	if (configFile == 0)
	{
		cerr << "net: No config file" << endl;
		return -1;
	}
	
	message msg;
	while (Net.port().get(msg))
	{
		libkernel::shared_memory shm = msg.get_shared_memory();
		switch (msg.type())
		{
			case libkernel::message::ping:
			{
				message reply(msg.port(), libkernel::message::pong);
				Net.port().send(reply);
			}break;
			case libkernel::message::net_register_nic:
			{
				if (msg.attribute() == libkernel::shared_memory::transfer_ownership &&
					shm.size() == sizeof(lightOS::net::nic_info))
				{
					// Initialize the NIC info
					lightOS::net::nic_info *info = shm.address<lightOS::net::nic_info>();
					info->tx = 0;
					info->rx = 0;
					info->tx_size = 0;
					info->rx_size = 0;
					
					// Register the NIC
					size_t Id = Net.register_nic(msg.port(), *info);
					
					// Go through the configuration file and decide what should be done:
					// * DHCP
					// * static IP
					string name("eth");
					char num[2];
					ultostr(Id, num, num + 2, 10);
					name.append(num);
					config::section<config::key_value_pair> *Section = configFile->get_section(name);
					if (Section != 0)
					{
						lightOS::net::nic_info &nic_info = Net.get_network_info(Id)->nic_info();
						const config::key_value_pair *cfg_ipv4 = (*Section)["ipv4::ip"];
						if (cfg_ipv4 != 0)
						{
							// DHCP should be used?
							if (cfg_ipv4->get_value() == "dhcp")
							{
								dhcp &Dhcp = dhcp::instance();
								Dhcp.send_request(*Net.get_network_info(Id));
							}
							else
							{
								// Set IP address
								pair<bool, lightOS::net::ipv4_address> result = lightOS::net::parse_ipv4_address(cfg_ipv4->get_value());
								if (result.first == false)
								{
									cerr << "net: config file entry \"" << name << "::ipv4::ip\" invalid" << endl;
								}
								nic_info.ipv4Address = result.second;
								
								// Set Subnet mask
								const config::key_value_pair *cfg_subnetmaskv4 = (*Section)["ipv4::subnet_mask"];
								if (cfg_subnetmaskv4 != 0)
								{
									pair<bool, lightOS::net::ipv4_address> result = lightOS::net::parse_ipv4_address(cfg_subnetmaskv4->get_value());
									if (result.first == false)
									{
										cerr << "net: config file entry \"" << name << "::ipv4::subnet_mask\" invalid" << endl;
									}
									nic_info.ipv4SubnetMask = result.second;
								}
								else nic_info.ipv4SubnetMask.mAddress = 0xFFFFFF;
								
								// Set Broadcast address
								const config::key_value_pair *cfg_broadcastv4 = (*Section)["ipv4::broadcast_address"];
								if (cfg_broadcastv4 != 0)
								{
									pair<bool, lightOS::net::ipv4_address> result = lightOS::net::parse_ipv4_address(cfg_broadcastv4->get_value());
									if (result.first == false)
									{
										cerr << "net: config file entry \"" << name << "::ipv4::broadcast_address\" invalid" << endl;
									}
									nic_info.ipv4BroadcastAddress = result.second;
								}
								else nic_info.ipv4BroadcastAddress = (nic_info.ipv4Address & nic_info.ipv4SubnetMask) | (lightOS::net::ipv4_address(0xFFFFFFFF) & (~nic_info.ipv4SubnetMask));
								
								// Set DNS server address
								const config::key_value_pair *cfg_dnsv4 = (*Section)["ipv4::dns"];
								if (cfg_dnsv4 != 0)
								{
									pair<bool, lightOS::net::ipv4_address> result = lightOS::net::parse_ipv4_address(cfg_dnsv4->get_value());
									if (result.first == false)
									{
										cerr << "net: config file entry \"" << name << "::ipv4::dns\" invalid" << endl;
									}
									nic_info.ipv4Dns = result.second;
								}
								else nic_info.ipv4Dns.mAddress = 0;
								
								// Set Router address
								const config::key_value_pair *cfg_routerv4 = (*Section)["ipv4::router"];
								if (cfg_routerv4 != 0)
								{
									pair<bool, lightOS::net::ipv4_address> result = lightOS::net::parse_ipv4_address(cfg_routerv4->get_value());
									if (result.first == false)
									{
										cerr << "net: config file entry \"" << name << "::ipv4::router\" invalid" << endl;
									}
									nic_info.ipv4Router = result.second;
								}
								else nic_info.ipv4Router.mAddress = 0;

								// Set DHCP server
								nic_info.ipv4Dhcp.mAddress = 0;
							}
						}
						else nic_info.ipv4Address.mAddress = 0;
					}
					message reply(msg.port(), libkernel::message::net_register_nic, Id);
					Net.port().send(reply);
				}
			}break;
			case libkernel::message::net_unregister_nic:
			{
				//TODO
			}break;
			case libkernel::message::net_enum_nic:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					size_t nicmask = Net.enum_nic();
					message reply(msg.port(), libkernel::message::net_enum_nic, nicmask);
					Net.port().send(reply);
				}
			}break;
			case libkernel::message::net_get_nic_info:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					lightOS::server::net::info *info = Net.get_network_info(static_cast<lightOS::net::ethId>(msg.param1()));
					if (info != 0)
					{
						libkernel::shared_memory shared(sizeof(lightOS::net::nic_info));
						memcpy(	shared.address<void>(),
								&info->nic_info(),
								sizeof(lightOS::net::nic_info));
						message reply(msg.port(), libkernel::message::net_get_nic_info, msg.param1(), shared, libkernel::shared_memory::transfer_ownership);
						Net.port().send(reply);
					}
					else
					{
						message reply(msg.port(), libkernel::message::net_get_nic_info, msg.param1());
						Net.port().send(reply);
					}
				}
			}break;
			case libkernel::message::net_nic_receive:
			{
				if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					Net.received_frame(msg.port(), shm);
				}
			}break;
			case libkernel::message::net_nic_transmit:
			{
				// TODO
			}break;
			case libkernel::message::net_arp_count:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					arp &Arp = arp::instance();
					size_t count = Arp.cache_size();
					message reply(msg.port(), libkernel::message::net_arp_count, count);
					Net.port().send(reply);
				}
			}break;
			case libkernel::message::net_arp_entry:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					arp &Arp = arp::instance();
					pair<lightOS::net::ethernet_address, lightOS::net::ipv4_address> result = Arp.cache_entry(msg.param1());
					uint64_t ethernet;
					result.first.to(ethernet);
					message reply(	msg.port(),
									libkernel::message::net_arp_entry,
									ethernet & 0xFFFFFFFF,
									(ethernet >> 32) & 0xFFFFFFFF,
									result.second.mAddress);
					Net.port().send(reply);
				}
			}break;
			case libkernel::message::net_arp_clear:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					arp &Arp = arp::instance();
					Arp.clear_cache();
					message reply(msg.port(), libkernel::message::net_arp_clear, static_cast<size_t>(true));
					Net.port().send(reply);
				}
			}break;
			case libkernel::message::net_dns_count:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					dns &Dns = dns::instance();
					size_t count = Dns.cache_size();
					message reply(msg.port(), libkernel::message::net_dns_count, count);
					Net.port().send(reply);
				}
			}break;
			case libkernel::message::net_dns_entry:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					dns &Dns = dns::instance();
					pair<string, lightOS::net::ipv4_address> result = Dns.cache_entry(msg.param1());
					libkernel::shared_memory shared(result.first.length() + 1);
					strcpy(	shared.address<char>(),
							result.first.c_str());
					message reply(msg.port(), libkernel::message::net_dns_entry, result.second.mAddress, shared, libkernel::shared_memory::transfer_ownership);
					Net.port().send(reply);
				}
			}break;
			case libkernel::message::net_dns_clear:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					dns &Dns = dns::instance();
					Dns.clear_cache();
					message reply(msg.port(), libkernel::message::net_dns_clear, static_cast<size_t>(true));
					Net.port().send(reply);
				}
			}break;
			case libkernel::message::net_set_nic_info:
			{
				if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					lightOS::server::net::info *tmp = Net.get_network_info(msg.param1());
					bool success = false;
					if (tmp != 0)
					{
						lightOS::net::nic_info *NewInfo = shm.address<lightOS::net::nic_info>();
						if (tmp->nic_info().ethernetAddress != NewInfo->ethernetAddress)
						{
							clog << "net: TODO: change ethernet address" << endl;
						}
						if (tmp->nic_info().ipv4Address != NewInfo->ipv4Address)
						{
							tmp->nic_info().ipv4Address = NewInfo->ipv4Address;
						}
						if (tmp->nic_info().ipv4SubnetMask != NewInfo->ipv4SubnetMask)
						{
							tmp->nic_info().ipv4SubnetMask = NewInfo->ipv4SubnetMask;
						}
						tmp->nic_info().ipv4BroadcastAddress = (tmp->nic_info().ipv4Address & tmp->nic_info().ipv4SubnetMask) | (lightOS::net::ipv4_address(0xFFFFFFFF) & (~tmp->nic_info().ipv4SubnetMask));
						success = true;
					}
					message reply(msg.port(), libkernel::message::net_set_nic_info, msg.param1(), static_cast<size_t>(success));
					Net.port().send(reply);
				}
			}break;
			case libkernel::message::net_socket_create:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					bool result = false;
					
					// Create the socket
					lightOS::server::net::socket *Socket = 0;
					lightOS::socket::type Type = static_cast<lightOS::socket::type>(msg.param1());
					switch (Type)
					{
						case lightOS::socket::icmp:
						case lightOS::socket::raw_icmp:
						{
							Socket = new ipv4::icmp_socket(Type, msg.port());
						}break;
						case lightOS::socket::tcp:
						case lightOS::socket::raw_tcp:
						{
							Socket = new tcp::socket(Type, msg.port());
						}break;
						case lightOS::socket::udp:
						case lightOS::socket::raw_udp:
						{
							Socket = new udp::socket(Type, msg.port());
						}break;
					}
					
					// Add to socket list
					if (Socket != 0)
					{
						Net.add_socket(Socket);
						result = true;
					}
					
					// Send reply
					message reply(msg.port(), libkernel::message::net_socket_create, static_cast<size_t>(result));
					Net.port().send(reply);
				}
			}break;
			case libkernel::message::net_socket_bind:
			{
				if (msg.attribute() == libkernel::shared_memory::transfer_ownership &&
					shm.size() == sizeof(lightOS::net::bind_socket_info))
				{
					bool result = false;
					lightOS::net::bind_socket_info *BindInfo = shm.address<lightOS::net::bind_socket_info>();
					
					// Find the socket
					server::net::socket *Socket = Net.find_socket(msg.port());
					if (Socket != 0)
					{
						result = Socket->bind(	BindInfo->ethID,
												BindInfo->srcPort,
												BindInfo->dstIP,
												BindInfo->dstPort);
					}
					
					// Send reply
					message reply(msg.port(), libkernel::message::net_socket_bind, static_cast<size_t>(result));
					Net.port().send(reply);
				}
			}break;
			case libkernel::message::net_socket_connect:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					// Find the socket
					server::net::socket *Socket = Net.find_socket(msg.port());
					
					// Do the connect
					if (Socket == 0 ||
						Socket->connect(msg.param1(), msg.param2()) == false)
					{
						// Send reply
						message reply(msg.port(), libkernel::message::net_socket_connect, static_cast<size_t>(false));
						Net.port().send(reply);
					}
				}
			}break;
			case libkernel::message::net_socket_send:
			{
				if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					bool result = false;
					
					// Find the socket
					server::net::socket *Socket = Net.find_socket(msg.port());
					if (Socket != 0)
					{
						result = Socket->send(shm, msg.param1());
					}
					
					// Send reply
					message reply(msg.port(), libkernel::message::net_socket_send, static_cast<size_t>(result));
					Net.port().send(reply);
				}
			}break;
			case libkernel::message::net_socket_disconnect:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					// Find the socket
					server::net::socket *Socket = Net.find_socket(msg.port());
					
					// Do the disconnect
					if (Socket == 0 ||
						Socket->disconnect() == false)
					{
						// Send reply
						message reply(msg.port(), libkernel::message::net_socket_disconnect, static_cast<size_t>(false));
						Net.port().send(reply);
					}
				}
			}break;
			case libkernel::message::net_socket_destroy:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					//TODO
				}
			}break;
			case libkernel::message::net_resolve:
			{
				if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					bool result = false;
					dns &Dns = dns::instance();
					lightOS::net::ipv4_address Address(0);
					
					// Find a DNS server
					lightOS::server::net::info *Info = Net.find_dns_server();
					if (Info != 0)
					{
						string domain(shm.address<char>(), shm.size());
						if (Dns.get_ip(domain, Address, *Info, msg.port()) == false)
							break;
						result = true;
					}
					
					message reply(msg.port(), libkernel::message::net_resolve, static_cast<size_t>(result), Address.mAddress);
					Net.port().send(reply);
				}
			}break;
		}
	}
	
	return 0;
}

server::net server::net::mInstance;

size_t server::net::register_nic(libkernel::port_id_t port, lightOS::net::nic_info &info)
{
	// Find a free NIC id
	size_t id = 0;
	for (;id < (sizeof(size_t) * 8);id++)
	{
		if (((1 << id) & mNICIds) == 0)
		{
			mNICIds |= (1 << id);
			break;
		}
	}
	
	mNIC.push_back(new net::info(port, id, info));
	return id;
}
void server::net::unregister_nic(libkernel::port_id_t port)
{
	// TODO: Find, free id, erase data
}
size_t server::net::enum_nic()
{
	return mNICIds;
}
server::net::info *server::net::get_network_info(lightOS::net::ethId net)
{
	for (size_t i = 0;i < mNIC.size();i++)
		if (mNIC[i]->id() == net)
			return mNIC[i];
	return 0;
}
server::net::info *server::net::get_network_info(	lightOS::net::ethId eth,
													lightOS::net::ipv4_address targetIp)
{
	for (size_t i = 0;i < mNIC.size();i++)
		if ((eth & (1 << mNIC[i]->id())) == 1)
			if ((targetIp & mNIC[i]->nic_info().ipv4SubnetMask) != (mNIC[i]->nic_info().ipv4Address & mNIC[i]->nic_info().ipv4SubnetMask) ||
				mNIC[i]->nic_info().ipv4Router.mAddress != 0)
				return mNIC[i];
	return 0;
}
server::net::info *server::net::find_dns_server()
{
	for (size_t i = 0;i < mNIC.size();i++)
		if (mNIC[i]->nic_info().ipv4Dns.mAddress != 0)
			return mNIC[i];
	return 0;
}
void server::net::received_frame(libkernel::port_id_t port, const libkernel::shared_memory &shm)
{
	net::info *Info = 0;
	for (size_t i = 0;i < mNIC.size();i++)
		if (mNIC[i]->port() == port)
		{
			Info = mNIC[i];
			break;
		}
	if (Info == 0)return;
	++Info->nic_info().rx;
	Info->nic_info().rx_size += shm.size();
	
	ethernet &Ethernet = ethernet::instance();
	Ethernet.process_frame(	shm.address<void>(),
							shm.size(),
							*Info);
}
void server::net::transmit_frame(lightOS::net::ethId net, void *data, size_t size)
{
	net::info *Info = get_network_info(net);
	if (Info == 0)return;
	
	libkernel::shared_memory shm(size);
	memcpy(	shm.address<void>(),
			data,
			size);
	message msg(Info->port(), libkernel::message::net_nic_transmit, net, shm, libkernel::shared_memory::transfer_ownership);
	mPort.send(msg);
	
	++Info->nic_info().tx;
	Info->nic_info().tx_size += size;
}
server::net::~net()
{
	//TODO
}
void server::net::add_socket(socket *Socket)
{
	mSockets.push_back(Socket);
}
void server::net::remove_socket(socket *Socket)
{
	vector<server::net::socket*>::iterator i = mSockets.begin();
	for (;i != mSockets.end();i++)
		if ((*i) == Socket)
			mSockets.erase(i);
}
lightOS::server::net::socket *server::net::find_socket(libkernel::port_id_t port)
{
	for (size_t i = 0;i < mSockets.size();i++)
		if (mSockets[i]->id() == port)
			return mSockets[i];
	return 0;
}

bool lightOS::server::net::socket::bind(lightOS::net::ethId eth,
										uint16_t srcPort,
										lightOS::net::ipv4_address dstIp,
										uint16_t dstPort)
{
	mEth = eth;
	mSrcPort = srcPort;
	mDstIp = dstIp;
	mDstPort = dstPort;
	return true;
}
