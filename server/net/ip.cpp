/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/net.hpp>
#include <lightOS/debug.hpp>
#include "ip.hpp"
#include "arp.hpp"
#include "ethernet.hpp"
#include "udp.hpp"
#include "tcp.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

ipv4 ipv4::mInstance;

bool ipv4::process_packet(	void *p,
							size_t size,
							lightOS::server::net::info &info,
							lightOS::net::ethernet_address source,
							lightOS::net::ethernet_address destination)
{
	packet *Packet = reinterpret_cast<packet*>(p);
	
	// Check ip packet version
	if (((Packet->version_ihl >> 4) & 0x0F) != 4)return false;
	
	// Check packet size
	if ((Packet->version_ihl & 0x0F) < 5)return false;
	
	// Initializing the TCP/IP stack via DHCP?
	if (info.dhcp_status() == lightOS::server::net::info::dhcp_none)
	{
		// Is it for us?
		if (lightOS::net::ipv4_address(Packet->destination) != info.nic_info().ipv4Address &&
			lightOS::net::ipv4_address(Packet->destination) != info.nic_info().ipv4BroadcastAddress &&
			Packet->destination != 0xFFFFFFFF)return false;
	}
	
	// NOTE TODO: The packet could be fragmented
	if (lightOS::net::ntoh16(Packet->length) > size)
	{
		DEBUG("ip: Packet->length = " << dec << lightOS::net::ntoh16(Packet->length) << " > size = " << size);
		return false;
	}
	
	// Checksum
	size_t checksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(Packet), (Packet->version_ihl & 0x0F) * 4, 0, 5);
	checksum = lightOS::net::ones_complement_checksum_end(checksum);
	if (checksum != Packet->header_checksum)
	{
		cerr << "ipv4: invalid checksum (calculated 0x" << hex << checksum << ", received 0x" << Packet->header_checksum << ")" << endl;
		return false;
	}
	
	size_t dataSize = lightOS::net::ntoh16(Packet->length) - 4 * (Packet->version_ihl & 0xF);
	void *data = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(Packet) + 4 * (Packet->version_ihl & 0xF));
	switch (Packet->protocol)
	{
		case lightOS::net::ip_type::icmp:
		{
			icmp_packet *IcmpPacket = reinterpret_cast<icmp_packet*>(data);
			size_t icmpChecksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(data), dataSize, 0, 1);
			icmpChecksum = lightOS::net::ones_complement_checksum_end(icmpChecksum);
			if (icmpChecksum != IcmpPacket->checksum)
			{
				cerr << "icmpv4: invalid checksum (calculated 0x" << hex << icmpChecksum << ", received 0x" << IcmpPacket->checksum << ")" << endl;
				return false;
			}
			switch (IcmpPacket->type)
			{
				case lightOS::net::icmp_type::echo:
				{
					icmp_packet *IcmpReply = reinterpret_cast<icmp_packet*>(allocate_packet(dataSize, info));
					IcmpReply->type = lightOS::net::icmp_type::echo_reply;
					IcmpReply->code = 0;
					memcpy(	reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(IcmpReply) + sizeof(icmp_packet)),
							reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(IcmpPacket) + sizeof(icmp_packet)),
							dataSize - sizeof(icmp_packet));
					size_t replyChecksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(IcmpReply), dataSize, 0, 1);
					replyChecksum = lightOS::net::ones_complement_checksum_end(replyChecksum);
					IcmpReply->checksum = replyChecksum;
					transmit_packet(IcmpReply, dataSize, info, lightOS::net::ipv4_address(Packet->source), lightOS::net::ip_type::icmp);
				}break;
				case lightOS::net::icmp_type::echo_reply:
				{
					icmp_packet_echo *IcmpPacket2 = reinterpret_cast<icmp_packet_echo*>(reinterpret_cast<uintptr_t>(IcmpPacket) + sizeof(icmp_packet));
					for (size_t i = 0;i < mIcmpSockets.size();i++)
						if (mIcmpSockets[i]->mIdentifier == lightOS::net::hton16(IcmpPacket2->identifier))
						{
							libkernel::shared_memory shm(dataSize - sizeof(icmp_packet) - sizeof(icmp_packet_echo));
							memcpy(	shm.address<void>(),
									reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(IcmpPacket2) + sizeof(icmp_packet_echo)),
									shm.size());
							message reply(mIcmpSockets[i]->id(), libkernel::message::net_socket_receive, Packet->source, shm, libkernel::shared_memory::transfer_ownership);
							lightOS::server::net &Net = lightOS::server::net::instance();
							Net.port().send(reply);
						}
				}break;
			}
		}break;
		case lightOS::net::ip_type::udp:
		{
			udp &udp = udp::instance();
			return udp.process_packet(	data,
										dataSize,
										info,
										lightOS::net::ipv4_address(Packet->source),
										lightOS::net::ipv4_address(Packet->destination));

		}break;
		case lightOS::net::ip_type::tcp:
		{
			tcp &Tcp = tcp::instance();
			return Tcp.process_packet(	data,
										dataSize,
										info,
										lightOS::net::ipv4_address(Packet->source),
										lightOS::net::ipv4_address(Packet->destination));
		}break;
	};
	
	return true;
}
void *ipv4::allocate_packet(size_t size, lightOS::server::net::info &info)
{
	size += sizeof(packet);
	void *p = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(ethernet::instance().allocate_frame(size, info)) + sizeof(packet));
	return p;
}
void ipv4::transmit_packet(	void *p,
							size_t size,
							lightOS::server::net::info &info,
							lightOS::net::ipv4_address dest,
							uint16_t protocol)
{
	size += sizeof(packet);
	packet *Packet = reinterpret_cast<packet*>(reinterpret_cast<uintptr_t>(p) - sizeof(packet));
	
	Packet->version_ihl = 0x45;
	Packet->type_of_service = 0;
	Packet->length = lightOS::net::hton16(size);
	Packet->identification = 0;
	Packet->flags_frag = 0x40;
	Packet->ttl = 64;
	Packet->protocol = protocol;
	Packet->source = info.nic_info().ipv4Address.mAddress;
	Packet->destination = dest.mAddress;
	size_t checksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(Packet), (Packet->version_ihl & 0x0F) * 4, 0, 5);
	Packet->header_checksum = lightOS::net::ones_complement_checksum_end(checksum);
	
	
	// Not in this subnet?
	lightOS::net::ipv4_address arpAddr = dest;
	if ((dest & info.nic_info().ipv4SubnetMask) != (info.nic_info().ipv4Address & info.nic_info().ipv4SubnetMask))
	{
		// Set to router IP
		arpAddr = info.nic_info().ipv4Router;
	}
	
	// Get ethernet address
	lightOS::net::ethernet_address destEthAddr;
	if (arp::instance().get_ethernet_address(	arpAddr,
												destEthAddr,
												info,
												this)
		== true)
	{
		ethernet &Ethernet = ethernet::instance();
		Ethernet.transmit_frame(Packet,
								size,
								info,
								destEthAddr,
								lightOS::net::ethernet_type::ipv4);
	}
	else
	{
		mWaitingforArp.push_back(triple<packet*,size_t, lightOS::net::ipv4_address>(Packet, size, arpAddr));
	}
}
void ipv4::arp_received(lightOS::net::ethernet_address ethAddr,
						lightOS::net::ipv4_address ipv4Addr,
						lightOS::server::net::info &info)
{
	for (size_t i = 0;i != mWaitingforArp.size();i++)
		if (mWaitingforArp[i].third.mAddress == ipv4Addr.mAddress)
		{
			ethernet &Ethernet = ethernet::instance();
			Ethernet.transmit_frame(mWaitingforArp[i].first,
									mWaitingforArp[i].second,
									info,
									ethAddr,
									lightOS::net::ethernet_type::ipv4);
			mWaitingforArp.erase(mWaitingforArp.begin() + i);
			i--;
		}
}

ipv4::icmp_socket::icmp_socket(lightOS::socket::type Type, libkernel::port_id_t Port)
	: lightOS::server::net::socket(Type, Port), mSequence(0), mIdentifier(rand())
{
	ipv4::instance().mIcmpSockets.push_back(this);
}
bool ipv4::icmp_socket::send(	libkernel::shared_memory &shm,
								lightOS::net::ipv4_address ip)
{
	bool result = false;
	if (mType == lightOS::socket::icmp)
	{
		lightOS::server::net &Net = lightOS::server::net::instance();
		lightOS::server::net::info *Info = Net.get_network_info(mEth, ip);
		if (Info != 0)
		{
			ipv4 &Ipv4 = ipv4::instance();
			icmp_packet *IcmpRequest = reinterpret_cast<icmp_packet*>(Ipv4.allocate_packet(shm.size() + sizeof(icmp_packet) + sizeof(icmp_packet_echo), *Info));
			IcmpRequest->type = lightOS::net::icmp_type::echo;
			IcmpRequest->code = 0;
			icmp_packet_echo *IcmpRequest2 = reinterpret_cast<icmp_packet_echo*>(reinterpret_cast<uintptr_t>(IcmpRequest) + sizeof(icmp_packet));
			IcmpRequest2->sequence_number = lightOS::net::hton16(++mSequence);
			IcmpRequest2->identifier = lightOS::net::hton16(mIdentifier);
			memcpy(	reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(IcmpRequest) + sizeof(icmp_packet) + sizeof(icmp_packet_echo)),
					shm.address<void>(),
					shm.size());
			size_t chksum = lightOS::net::ones_complement_checksum(reinterpret_cast<uint16_t*>(IcmpRequest), shm.size() + sizeof(icmp_packet) + sizeof(icmp_packet_echo), 0, 1);
			IcmpRequest->checksum = lightOS::net::ones_complement_checksum_end(chksum);
			if (ip == 0)ip = mDstIp;
			if (ip != 0)
			{
				Ipv4.transmit_packet(	IcmpRequest,
										shm.size() + sizeof(icmp_packet) + sizeof(icmp_packet_echo),
										*Info,
										ip,
										lightOS::net::ip_type::icmp);
				result = true;
			}
		}
	}
	else if (mType == lightOS::socket::raw_icmp)
	{
		cerr << "ipv4::icmp_socket::send(): raw_icmp not yet supported" << endl;
	}
	return result;
}
ipv4::icmp_socket::~icmp_socket()
{
	ipv4 &Ipv4 = ipv4::instance();
	std::vector<icmp_socket*>::iterator i = Ipv4.mIcmpSockets.begin();
	for (;i != Ipv4.mIcmpSockets.end();i++)
		if ((*i) == this)
			Ipv4.mIcmpSockets.erase(i);
}
