/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_SERVER_NET_DNS_HPP
#define LIGHTOS_SERVER_NET_DNS_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstdint>
#include <cstddef>
#include <string>
#include <vector>
#include <lightOS/net.hpp>
#include "net.hpp"

namespace lightOS
{
	namespace server
	{
		class dns
		{
			public:
				static dns &instance(){return mInstance;}
				bool process_packet(void *p,
									size_t size,
									lightOS::server::net::info &info,
									lightOS::net::ipv4_address source,
									uint16_t soureport,
									lightOS::net::ipv4_address destination,
									uint16_t destport);
				bool get_ip(	const std::string &name,
								lightOS::net::ipv4_address &addr,
								lightOS::server::net::info &info,
								libkernel::port_id_t Port);
				std::string get_domain_name(const char *enc);
				void broadcast_result(	std::string &domain,
										lightOS::net::ipv4_address addr,
										bool result);
				inline size_t cache_size() const{return mCache.size();}
				inline std::pair<std::string, lightOS::net::ipv4_address> cache_entry(size_t i) const
				{
					if (i > mCache.size())return std::pair<std::string, lightOS::net::ipv4_address>(std::string(), 0);
					return mCache[i];
				}
				inline void clear_cache(){mCache.clear();}
			private:
				static dns mInstance;
				
				struct packet
				{
					uint16_t id;
					uint16_t flags;
					uint16_t qcount;
					uint16_t acount;
					uint16_t nscount;
					uint16_t arcount;
				}__attribute__((packed));
				
				struct ressource_header
				{
					uint16_t type;
					uint16_t net_class;
					uint32_t ttl;
					uint16_t size;
				}__attribute__((packed));
				
				std::vector<std::pair<std::string, libkernel::port_id_t> > mRequestCache;
				std::vector<std::pair<std::string, lightOS::net::ipv4_address> > mCache;
		};
	}
}

/*@}*/

#endif
