/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/net.hpp>
//#define NDEBUG
#include <lightOS/debug.hpp>
#include "arp.hpp"
#include "ethernet.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

arp arp::mInstance;

bool arp::process_packet(	void *p,
							size_t size,
							lightOS::server::net::info &info,
							lightOS::net::ethernet_address source,
							lightOS::net::ethernet_address destination)
{
	packet_v4 *Packet = reinterpret_cast<packet_v4*>(p);
	
	// Is it an ethernet arp packet?
	if (lightOS::net::ntoh16(Packet->hardware_type) != 1)return false;
	if (Packet->hardware_length != 6)return false;
	
	// Are the ethernet source addresses identical?
	if (source != lightOS::net::ethernet_address(Packet->sender_address))return false;
	
	// Is IPv4 ARP packet?
	if (lightOS::net::ntoh16(Packet->protocol_type) == lightOS::net::ethernet_type::ipv4 &&
		Packet->protocol_length == 4)
	{
		switch (lightOS::net::ntoh16(Packet->operation))
		{
			case 1:
			{
				// ARP request
				lightOS::net::ipv4_address destIp(Packet->receiver_protocol_address);
				lightOS::net::ipv4_address sourceIp(Packet->sender_protocol_address);
				DEBUG("arp: from " << sourceIp << ", " << source);
				DEBUG("arp: seeking " << destIp);
				
				if (info.nic_info().ipv4Address == destIp)
				{
					DEBUG("arp: sending reply");
					ethernet &Ethernet = ethernet::instance();
					packet_v4 *Reply = reinterpret_cast<packet_v4*>(Ethernet.allocate_frame(sizeof(packet_v4), info));
					Reply->hardware_type = lightOS::net::hton16(1);
					Reply->protocol_type = lightOS::net::hton16(lightOS::net::ethernet_type::ipv4);
					Reply->hardware_length = 6;
					Reply->protocol_length = 4;
					Reply->operation = lightOS::net::hton16(2);
					info.nic_info().ethernetAddress.to(Reply->sender_address);
					Reply->sender_protocol_address = info.nic_info().ipv4Address.mAddress;
					source.to(Reply->receiver_address);
					Reply->receiver_protocol_address = Packet->sender_protocol_address;
					Ethernet.transmit_frame(Reply,
											sizeof(packet_v4),
											info,
											source,
											lightOS::net::ethernet_type::arp);
				}
			}break;
			case 2:
			{
				lightOS::net::ipv4_address destIp(Packet->receiver_protocol_address);
				lightOS::net::ipv4_address sourceIp(Packet->sender_protocol_address);
				
				// Search in the request arp cache for the IPv4 address
				vector<pair<lightOS::net::ipv4_address, lightOS::server::net::callback*> >::iterator i = mIpv4RequestCache.begin();
				for (;i != mIpv4RequestCache.end();i++)
					if ((*i).first == sourceIp)
					{
						if ((*i).second != 0)
							(*i).second->arp_received(source, sourceIp, info);
						mIpv4Cache.push_back(pair<lightOS::net::ethernet_address, lightOS::net::ipv4_address>(source, sourceIp));
						mIpv4RequestCache.erase(i);
						return true;
					}
			}break;
		}
	}
	// Is IPv6 ARP packet?
	else if (	lightOS::net::ntoh16(Packet->protocol_type) == lightOS::net::ethernet_type::ipv6 &&
				Packet->protocol_length == 16)
	{
		cout << "arp: arp for ipv6" << endl;
	}
	
	return false;;
}
bool arp::get_ethernet_address(	lightOS::net::ipv4_address addr,
								lightOS::net::ethernet_address &ethAddr,
								lightOS::server::net::info &info,
								lightOS::server::net::callback *callback)
{
	// global/local Broadcast IP address?
	if (addr.mAddress == 0xFFFFFFFF ||
		addr == info.nic_info().ipv4BroadcastAddress)
	{
		ethAddr = lightOS::net::ethernet_address(true);
		return true;
	}
	
	// Search in the arp cache for the ethernet address
	for (size_t i = 0;i < mIpv4Cache.size();i++)
		if (mIpv4Cache[i].second == addr)
		{
			ethAddr = mIpv4Cache[i].first;
			return true;
		}
	
	// Send an arp request
	ethernet &Ethernet = ethernet::instance();
	packet_v4 *Request = reinterpret_cast<packet_v4*>(Ethernet.allocate_frame(sizeof(packet_v4), info));
	Request->hardware_type = lightOS::net::hton16(1);
	Request->protocol_type = lightOS::net::hton16(lightOS::net::ethernet_type::ipv4);
	Request->hardware_length = 6;
	Request->protocol_length = 4;
	Request->operation = lightOS::net::hton16(1);
	info.nic_info().ethernetAddress.to(Request->sender_address);
	Request->sender_protocol_address = info.nic_info().ipv4Address.mAddress;
	lightOS::net::ethernet_address broadcastAddr(true);
	broadcastAddr.to(Request->receiver_address);
	Request->receiver_protocol_address = addr.mAddress;
	Ethernet.transmit_frame(Request,
							sizeof(packet_v4),
							info,
							broadcastAddr,
							lightOS::net::ethernet_type::arp);
	
	// Add to the list
	mIpv4RequestCache.push_back(pair<lightOS::net::ipv4_address, lightOS::server::net::callback*>(addr, callback));
	
	return false;
}
