/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_SERVER_NET_IP_HPP
#define LIGHTOS_SERVER_NET_IP_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstdint>
#include <cstddef>
#include <vector>
#include <utility>
#include <lightOS/net.hpp>
#include "net.hpp"

namespace lightOS
{
	namespace server
	{
		class ipv4 : public lightOS::server::net::callback
		{
			public:
				class icmp_socket : public lightOS::server::net::socket
				{
					friend class ipv4;
					public:
						icmp_socket(lightOS::socket::type Type, libkernel::port_id_t Port);
						virtual bool send(	libkernel::shared_memory &shm,
											lightOS::net::ipv4_address ip);
						virtual ~icmp_socket();
					private:
						uint16_t mSequence;
						uint16_t mIdentifier;
				};
			
				friend class icmp_socket;
				static ipv4 &instance(){return mInstance;}
				bool process_packet(void *p,
									size_t size,
									lightOS::server::net::info &info,
									lightOS::net::ethernet_address source,
									lightOS::net::ethernet_address destination);
				void *allocate_packet(size_t size, lightOS::server::net::info &info);
				void transmit_packet(	void *p,
										size_t size,
										lightOS::server::net::info &info,
										lightOS::net::ipv4_address dest,
										uint16_t protocol);
				virtual void arp_received(	lightOS::net::ethernet_address ethAddr,
											lightOS::net::ipv4_address ipv4Addr,
											lightOS::server::net::info &info);
			protected:
				std::vector<icmp_socket*> mIcmpSockets;
			private:
				static ipv4 mInstance;
				
				struct packet
				{
					uint8_t		version_ihl;
					uint8_t		type_of_service;
					uint16_t	length;
					uint16_t	identification;
					uint16_t 	flags_frag;
					uint8_t		ttl;
					uint8_t		protocol;
					uint16_t	header_checksum;
					uint32_t	source;
					uint32_t	destination;
				}__attribute__((packed));
				
				struct icmp_packet
				{
					uint8_t		type;
					uint8_t		code;
					uint16_t	checksum;
				};
				
				struct icmp_packet_echo
				{
					uint16_t	identifier;
					uint16_t	sequence_number;
				};
				
				std::vector<std::triple<packet*,size_t, lightOS::net::ipv4_address> > mWaitingforArp;
		};
	}
}

/*@}*/

#endif
