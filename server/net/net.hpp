/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_SERVER_NET_HPP
#define LIGHTOS_SERVER_NET_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstdint>
#include <vector>
#include <utility>
#include <lightOS/net.hpp>
#include <lightOS/lightOS.hpp>

namespace lightOS
{
	namespace server
	{
		class net
		{
			public:
				class info
				{
					public:
						enum
						{
							dhcp_none = 0,
							dhcp_wait_for_offer,
							dhcp_wait_for_ack
						};
						
						inline info(libkernel::port_id_t Port, lightOS::net::ethId Id, lightOS::net::nic_info &Info)
							: mPort(Port), mId(Id), mInfo(Info), mDhcp(dhcp_none), mDhcpId(0), mDhcpIp(0){}
						inline libkernel::port_id_t port() const{return mPort;}
						inline lightOS::net::ethId id() const{return mId;}
						inline lightOS::net::nic_info &nic_info(){return mInfo;}
						inline size_t dhcp_status() const{return mDhcp;}
						inline void dhcp_status(size_t newStatus){mDhcp = newStatus;}
						inline size_t dhcp_id() const{return mDhcpId;}
						inline void dhcp_id(size_t id){mDhcpId = id;}
						inline const lightOS::net::ipv4_address &dhcp_ip() const{return mDhcpIp;}
						inline void dhcp_ip(const lightOS::net::ipv4_address &ip){mDhcpIp = ip;}
					private:
						libkernel::port_id_t mPort;
						lightOS::net::ethId mId;
						lightOS::net::nic_info mInfo;
						size_t mDhcp;
						size_t mDhcpId;
						lightOS::net::ipv4_address mDhcpIp;
				};
				
				class callback
				{
					public:
						inline virtual void arp_received(	lightOS::net::ethernet_address ethAddr,
															lightOS::net::ipv4_address ipv4Addr,
															info &info){}
						inline virtual void dns_received(	const std::string &domain,
															lightOS::net::ipv4_address Address,
															info &info){}
						inline virtual ~callback(){}
				};
				
				class socket : public callback
				{
					public:
						inline socket(lightOS::socket::type Type, libkernel::port_id_t Port)
							: mType(Type), mPort(Port), mEth(lightOS::net::eth_all), mSrcPort(0), mDstIp(), mDstPort(lightOS::net::port_any){}
						inline libkernel::port_id_t id() const{return mPort;}
						inline lightOS::socket::type type() const{return mType;}
						void resolve(const char *domain);
						virtual bool bind(	lightOS::net::ethId eth,
											uint16_t srcPort,
											lightOS::net::ipv4_address dstIp,
											uint16_t dstPort);
						virtual bool connect(	lightOS::net::ipv4_address dstIp,
												uint16_t dstPort){return false;}
						virtual bool send(	libkernel::shared_memory &shm,
											lightOS::net::ipv4_address ip)=0;
						virtual bool disconnect(){return false;}
						inline virtual ~socket(){}
					protected:
						lightOS::socket::type mType;
						libkernel::port_id_t mPort;
						lightOS::net::ethId mEth;
						uint16_t mSrcPort;
						lightOS::net::ipv4_address mDstIp;
						uint16_t mDstPort;
				};
				
				static net &instance(){return mInstance;}
				inline libkernel::message_port &port(){return mPort;}
				
				size_t register_nic(libkernel::port_id_t port, lightOS::net::nic_info &info);
				void unregister_nic(libkernel::port_id_t port);
				size_t enum_nic();
				info *get_network_info(lightOS::net::ethId net);
				info *get_network_info(lightOS::net::ethId eth, lightOS::net::ipv4_address targetIp);
				info *find_dns_server();
				void received_frame(libkernel::port_id_t port, const libkernel::shared_memory &shm);
				void transmit_frame(lightOS::net::ethId net, void *data, size_t size);
				void add_socket(socket *Socket);
				void remove_socket(socket *Socket);
				socket *find_socket(libkernel::port_id_t port);
			private:
				inline net()
					: mPort(libkernel::message_port::net, true), mNICIds(0){}
				~net();
				
				libkernel::message_port mPort;
				size_t mNICIds;
				std::vector<info*> mNIC;
				std::vector<socket*> mSockets;
				
				static net mInstance;
		};
	}
}

/*@}*/

#endif
