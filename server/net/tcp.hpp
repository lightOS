/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_SERVER_NET_TCP_HPP
#define LIGHTOS_SERVER_NET_TCP_HPP

/*! \addtogroup lightOS lightOS library */
/*@{*/

#include <cstdint>
#include <cstddef>
#include <lightOS/net.hpp>
#include "net.hpp"

namespace lightOS
{
	namespace server
	{
		class tcp
		{
			private:
				struct packet
				{
					uint16_t sourceport;
					uint16_t destport;
					uint32_t sequence_number;
					uint32_t ack_number;
					uint8_t offset;
					uint8_t flags;
					uint16_t window;
					uint16_t checksum;
					uint16_t urgent_pointer;
				}__attribute__((packed));
				
			public:
				class socket : public lightOS::server::net::socket
				{
					friend class tcp;
					public:
						socket(lightOS::socket::type Type, libkernel::port_id_t Port);
						virtual bool bind(	lightOS::net::ethId eth,
											uint16_t srcPort,
											lightOS::net::ipv4_address dstIp,
											uint16_t dstPort);
						virtual bool connect(	lightOS::net::ipv4_address dstIp,
												uint16_t dstPort);
						virtual bool send(	libkernel::shared_memory &shm,
											lightOS::net::ipv4_address ip);
						virtual bool disconnect();
						virtual ~socket();
					private:
						uint16_t mWindow;
						uint16_t mMaxSegmentSize;
						
						enum status
						{
							none,
							handshake,
							established,
							disconnecting
						};
						
						status mStatus;
						size_t mSequenceNumber;
						size_t mAckNumber;
				};
				
				friend class socket;
				
				static tcp &instance(){return mInstance;}
				bool process_packet(void *p,
									size_t size,
									lightOS::server::net::info &info,
									lightOS::net::ipv4_address source,
									lightOS::net::ipv4_address destination);
				void *allocate_packet(size_t size, lightOS::server::net::info &info);
				void transmit_raw_packet(	packet *Packet,
											size_t size,
											lightOS::server::net::info &info,
											lightOS::net::ipv4_address dst);
				void transmit_packet(	void *p,
										size_t size,
										lightOS::server::net::info &info,
										uint16_t srcport,
										lightOS::net::ipv4_address dest,
										uint16_t destport);
			private:
				inline tcp()
					: mLastPort(1001){}
				static tcp mInstance;
				
				uint16_t mLastPort;
				std::vector<socket*> mSockets;
				
				enum
				{
					fin			= 0x01,
					syn			= 0x02,
					reset		= 0x04,
					push		= 0x08,
					ack			= 0x10,
					urgent		= 0x20,
					flags_mask	= fin | syn | reset | push | ack | urgent
				};
				
				uint16_t checksum(	packet *Packet,
									size_t size,
									lightOS::net::ipv4_address &src,
									lightOS::net::ipv4_address &dst);
		};
	}
}

/*@}*/

#endif
