/*
lightOS application
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <algorithm>
#include <iterator>
#include <iostream>
#include <lightOS/config.hpp>
#include <lightOS/lightOS.hpp>
using namespace lightOS;
using namespace std;

using libkernel::message;

processId execute_file(const string &cmd, size_t flags)
{
	string::const_iterator firstspace = find<string::iterator>(cmd.begin(), cmd.end(), ' ');
	string name; // TODO ("gzip://");
	string tmp(cmd.begin(), firstspace);
	name.append(tmp);
	
	file File(name.c_str(), access::read);
	if (!File)
    return 0;
	size_t size = File.blocksize() * File.blockcount();
	if (size == 0)
    return 0;
	
	libkernel::shared_memory shm = File.read(0, size);
	if (shm.size() != size)
    return 0;
	
	// HACK: This is actually not a good idea, but who cares? *g*
	char libraryName[1000];
	libraryName[0] = '\0';
	processId result;
	while(	(result = execute(shm.address<void>(), size, cmd.c_str(), flags, libraryName)) == 0 &&
			strlen(libraryName) != 0)
	{
		cout << "init: loading library \"" << libraryName << "\"" << endl;
		if (execute_file(libraryName, 0) == 0)
    {
      cerr << "init: library \"" << libraryName << "\" not found" << endl;
      return 0;
    }
		libraryName[0] = '\0';
	}
	return result;
}

bool do_boot_config(const config::section<config::key_only> &section)
{
	for (size_t i = 0;i < section.entity_count();i++)
	{
		if (section[i].is_section() == true)
			do_boot_config(*static_cast<const config::section<config::key_only>*>(&section[i]));
		else
		{
			const config::key_only *entity = static_cast<const config::key_only*>(&section[i]);
			cout << "init: " << entity->name() << endl;
			bool bServer = false;
			if (section.name() == "server")bServer = true;
			if (execute_file(entity->name(), bServer ? process::server : 0) == 0)
				return false;
		}
	}
	return true;
}

bool wait_mount(libkernel::message_port &Port,
				const char *fs,
				const char *mountpoint)
{
	cout << "init: " << fs << " server" << endl;
	waitForFilesystemRegistration(Port, fs);
	
	if (mount(Port, mountpoint, fs) == false)
	{
		cerr << "init: failed" << endl;
		return false;
	}
	return true;
}

int main(int argc, char *argv[])
{
	libkernel::message_port Port(libkernel::message_port::init, true);
	if (!Port)
	{
		cerr << "init: init already started" << endl;
		return -1;
	}
	
	// Wait for the console server
	Port.wait_for_other(libkernel::message_port::console);
	
	// Print the lightOS version
	pair<size_t,size_t> version = get_kernel_version();
	
	cout << "lightOS V" << dec << version.first << '.' << version.second;
	#ifdef X86
		cout << " x86";
	#endif
	#ifdef X86_64
		cout << " x86-64";
	#endif
	cout << endl << endl;
	
	// Find the bootdevice
	bool bGui = false;
	const char *bootdev = 0;
	const char *bootfs = 0;
	for (int i=1;i < argc;i++)
		if (strcmp(argv[i], "--boot") == 0)
		{
			++i;
			if (i < argc)bootdev = argv[i];
		}
		else if (strcmp(argv[i], "--fs") == 0)
		{
			++i;
			if (i < argc)bootfs = argv[i];
		}
		else if (strcmp(argv[i], "--gui") == 0)bGui = true;
	
	if (bootdev == 0 ||
		bootfs == 0)
	{
		cerr << "init: No boot device specified" << endl;
		return -1;
	}
	
	// Wait for vfs server
	cout << "init: vfs server" << endl;
	Port.wait_for_other(libkernel::message_port::vfs);
	
	// Mount pseudo filesystems
	if (wait_mount(Port, "devfs", "dev://") == false)return -1;

#if 0
	if (wait_mount(Port, "gzipfs", "gzip://") == false)return -1;
#endif
	
	// Mount boot device
	cout << "init: mounting \"" << bootdev << "\"" << endl;
	waitForFilesystemRegistration(Port, bootfs);
	while (file::exists(bootdev, Port) == file::none){wait(1);}
	if (mount(Port, "/", bootfs, bootdev) == false)
	{
		cerr << "init: failed" << endl;
		return -1;
	}
	
	// Execute & Mount the remaining pseudo filesystems
  cout << "init: pipefs" << endl;
	if (execute_file("/system/server/pipefs", process::server) == 0)
	{
		cerr << "init: failed" << endl;
		return 0;
	}
  cout << "init: tmpfs" << endl;
	if (execute_file("/system/server/tmpfs", process::server) == 0)
	{
		cerr << "init: failed" << endl;
		return 0;
	}
	
	// Mount the remaining pseudo filesystems
	if (wait_mount(Port, "pipefs", "pipe://") == false)return -1;
	if (wait_mount(Port, "tmpfs", "tmp://") == false)return -1;
	
	// Parse boot.cfg
	config::section<config::key_only> *result = config::parse<config::key_only>("/system/config/boot.cfg");
	if (result == 0)
	{
		cerr << "init: No configuration file" << endl;
		return -1;
	}
	if (do_boot_config(*result) == false)
	{
		cerr << "init: failed" << endl;
		return -1;
	}
	
	
	// Execute gui
	if (bGui == true)
	{
		const char *name = "/system/server/gui";
		cout << "init: " << name << endl;
		if (execute_file(name, process::server) == 0)
		{
			cerr << "init: failed" << endl;
			return 0;
		}
	}
	
	cout << "init: done" << endl;
	
	if (bGui == false)
	{
		message msg(libkernel::message_port::console, libkernel::message::init_done);
		Port.send(msg);
	}
	
	// Server
	message msg;
	while (Port.get(msg))
	{
		libkernel::shared_memory shm = msg.get_shared_memory();
		switch (msg.type())
		{
			case libkernel::message::ping:
			{
				message reply(msg.port(), libkernel::message::pong);
				Port.send(reply);
			}break;
			case libkernel::message::init_execute:
			{
				if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					// TODO: security!
					char *cmdline = shm.address<char>();
					cmdline[shm.size()] = '\0';
					processId pid = execute_file(cmdline, msg.param1());
					message reply(msg.port(), libkernel::message::init_execute, pid);
					Port.send(reply);
				}
			}break;
      // TODO: Until we have a proper kernel log
      case _LIBKERNEL_MSG_LIBOS_WARNING:
      {
        cerr << shm.address<char>();
      }
		}
	}
	
	return 0;
}
