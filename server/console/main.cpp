/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <lightOS/device.hpp>
#include <lightOS/lightOS.hpp>
#include "vram.hpp"
#include "console.hpp"
using namespace lightOS;
using namespace std;

using libkernel::message;

libkernel::message_port::wait_group WaitGroup;

vram Vram(true);
size_t iConsole = 0;
libkernel::message_port Port(libkernel::message_port::console, true);
console Console[10] = {console(libkernel::message_port::in, libkernel::message_port::out, libkernel::message_port::err)};
const char *kbdDeviceName = "dev://keyboard";

class keyboard : public lightOS::file
{
	public:
		virtual void callback_read(	const libkernel::shared_memory &shm,
									size_t block);
};
void keyboard::callback_read(	const libkernel::shared_memory &shm,
								size_t block)
{
	keyboardData *Data = shm.address<keyboardData>();
	
	if (Data->alt == 1 &&
		Data->ctrl == 0 &&
		Data->altgr == 0 &&
		Data->key.character >= L'0' &&
		Data->key.character <= L'9')
	{
		vram *tmp = Console[iConsole].unshow();
		iConsole = Data->key.character - L'0';
		Console[iConsole].show(tmp);
	}
	else
		Console[iConsole].keyboard(*Data, Port, WaitGroup);
}

int main()
{
	// Create the keyboard class
	keyboard Keyboard;
	
	Console[iConsole].show(&Vram);
	
	WaitGroup.register_handler(Port.id(), message(0, 0), 0);
	for (size_t i = 0;i < 10;i++)
		Console[i].wait_group_add(WaitGroup);
	
	while (true)
	{
		pair<libkernel::port_id_t,message> result = WaitGroup();
		if (result.first == 0)continue;
		
		libkernel::shared_memory shm = result.second.get_shared_memory();
		
		switch (result.second.type())
		{
			case libkernel::message::ping:
			{
				message reply(result.second.port(), libkernel::message::pong);
				Port.send(reply);
			}break;
			case libkernel::message::event:
			{
				if (result.second.param1() == libkernel::event::timer)
				{
					// Wait for the keyboard device
					if (Keyboard.exists(kbdDeviceName) == false)
					{
						// Set timer (200ms)
						Port.register_event(libkernel::event::timer, 200);
						break;
					}
					
					// Open the keyboard device
					if (Keyboard.open(kbdDeviceName, access::read) == false)
					{
						Console[0].get().print("console: failed to open \"", COLOR_ERROR);
						Console[0].get().print(kbdDeviceName, COLOR_ERROR);
						Console[0].get().print("\"\n", COLOR_ERROR);
						return 0;
					}
					
					// Add the keyboard file to the wait group
					WaitGroup.register_handler(Keyboard.id().first , message(0, 0), &Keyboard);
					
					// Execute a shell on console0
					Console[1].execute_shell(Port, WaitGroup);
					
					Console[0].get().print("console0: shell is on console1\n", COLOR_NORMAL);
				}
			}break;
			case libkernel::message::init_done:
			{
				if (result.second.port() == libkernel::message_port::init)
				{
					// Set timer (200ms)
					Port.register_event(libkernel::event::timer, 200);
				}
			}break;
		}
	}
}
