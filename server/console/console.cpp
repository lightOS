/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstdlib>
#include <lightOS/lightOS.hpp>
#include <libserver/console.hpp>
#include "console.hpp"
using namespace lightOS;
using namespace std;

using libkernel::message;

void console::received(libkernel::port_id_t PortID, message &msg)
{
	libkernel::shared_memory shm = msg.get_shared_memory();
	
	libkernel::message_port *Port = &mStdout;
	if (PortID == mStderr.id())Port = &mStderr;
	
	switch (msg.type())
	{
		case libkernel::message::fs_write_file:
		{
			if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
			{
				// Allocate & copy the string
				char *string = new char[shm.size() + 1];
				memcpy(	string,
						shm.address<void>(),
						shm.size());
				string[shm.size()] = '\0';
				
				if (strcmp(string, "\x1B[E") == 0)
					bEcho = true;
				else if (strcmp(string, "\x1B]E") == 0)
					bEcho = false;
				else if (strcmp(string, "\x1B]B") == 0)
					bCBreak = true;
				else if (strcmp(string, "\x1B[B") == 0)
					bCBreak = false;
				else if (strcmp(string, "\x1B[C") == 0)
					get().clear();
				else if (strncmp(string, "\x1B]C", 3) == 0)
				{
					char *cur = string + 3;
					size_t x = strtol(cur, &cur, 10);
					size_t y = strtol(cur + 1, &cur, 10);
					get().cursor(x, y);
				}
				else if (strcmp(string, "\x1B[V0") == 0)
					get().cursor_color(0);
				else if (strcmp(string, "\x1B[V1") == 0)
					get().cursor_color(COLOR_NORMAL);
				else
				{
					// Get the color
					uint8_t color = 0;
					if (PortID == mStdout.id())color = COLOR_NORMAL;
					else if (PortID == mStderr.id())color = COLOR_ERROR;
					
					// Print to the screen
					get().print(string, color);
				}
					
				// Deallocate the string
				delete []string;
				
				// Send the reply
				message reply(msg.port(), libkernel::message::fs_write_file, msg.param1(), shm.size());
				Port->send(reply);
			}
			else if (msg.attribute() == libkernel::shared_memory::read_only)
			{
				vram::copy(get(), shm.address<void>());
				
				message reply(msg.port(), libkernel::message::fs_write_file, msg.param1());
				Port->send(reply);
			}
		}break;
		case libkernel::message::fs_close_file:
		{
			/*if (msg.port() == mStdin->id().second)
			{
				// NOTE: really close things here
			}*/
			message reply(msg.port(), libkernel::message::fs_close_file, static_cast<size_t>(true));
			Port->send(reply);
		}break;
		case libkernel::message::fs_get_file_flags:
		{
			message reply(msg.port(), libkernel::message::fs_get_file_flags, lightOS::attr::tty);
			Port->send(reply);
		}break;
	}
}

void console::show(vram *Vram)
{
	mVram = Vram;
	
	// Copy to real vram
	vram::copy(*mVram, mVramCopy);
}
vram *console::unshow()
{
	// Copy the vram
	vram::copy(mVramCopy, *mVram);
	
	// Return the real vram
	vram *tmp = mVram;
	mVram = 0;
	return tmp;
}
vram &console::get()
{
	if (mVram != 0)return *mVram;
	return mVramCopy;
}
void console::wait_group_add(libkernel::message_port::wait_group &WaitGroup)
{
	WaitGroup.register_handler(mStdout.id(), message(0, 0), this);
	WaitGroup.register_handler(mStderr.id(), message(0, 0), this);
}
void console::keyboard(keyboardData &Data, libkernel::message_port &Port, libkernel::message_port::wait_group &WaitGroup)
{
	// Handle Ctrl-C
	if (mStdin != 0 &&
		bCBreak == false &&
		Data.ctrl == 1 &&
		Data.alt == 0 &&
		Data.altgr == 0 &&
		(Data.key.character == L'c' || Data.key.character == L'C'))
	{
		vector<libkernel::port_id_t> portList = mStdin->get_opener_list();
		for (size_t i = 1;i < portList.size();i++)
		{
			pair<processId,threadId> result = get_port_info(portList[i]);
			destroy_process(result.first);
		}
		if (portList.size() > 1)return;
	}
	
	// TODO: What if (no)buffer mode
	if (Data.key.character != L'\0')
	{
		// New Shell?
		if (mStdin == 0 &&
			Data.key.character == L's' &&
			Data.ctrl == 1 &&
			Data.alt == 0 &&
			Data.altgr == 0)
		{
			execute_shell(Port, WaitGroup);
			return;
		}
		
		// Echo?
		if (bEcho)
		{
			if (Data.ctrl == 0)
				get().print(static_cast<char>(Data.key.character), COLOR_NORMAL);
			else
			{
				if (Data.key.character == L' ')
					get().print("^@", COLOR_NORMAL);
				else if (	(Data.key.character >= L'a' && Data.key.character <= L'z') ||
							(Data.key.character >= L'A' && Data.key.character <= L'Z'))
				{
					get().print('^', COLOR_NORMAL);
					get().print(toupper(Data.key.character), COLOR_NORMAL);
				}
			}
		}
		
		if (mStdin != 0)
		{
			if (Data.ctrl == 0)
			{
				char c = static_cast<char>(Data.key.character);
				mStdin->write<char>(&c, 1);
			}
			else
			{
				char c;
				if (Data.key.character == L' ')
					c = 0x00;
				else if (Data.key.character >= L'a' && Data.key.character <= L'z')
					c = static_cast<char>(Data.key.character) - 'a' + 1;
				else if (Data.key.character >= L'A' && Data.key.character <= L'Z')
					c = static_cast<char>(Data.key.character) - 'A' + 1;
				mStdin->write<char>(&c, 1);
			}
		}
	}
	else if (Data.key.code != libserver::keycode::none)
	{
    std::string char_seq = libserver::convert_to_ecma48(Data.key);

		if (char_seq.length() != 0)
		{
			// Echo?
			if (bEcho)
			{
				get().print("^[", COLOR_NORMAL);
				get().print(char_seq.c_str() + 1, COLOR_NORMAL);
			}
			
			if (mStdin != 0)
			{
				libkernel::shared_memory shm(char_seq.length());
				strncpy(shm.address<char>(),
						char_seq.c_str(),
						char_seq.length());
				mStdin->write(shm);
			}
		}
		else
		{
			get().print("console: Unknown virtual keycode\n", COLOR_LOG);
		}
	}
}
void console::execute_shell(libkernel::message_port &Port, libkernel::message_port::wait_group &WaitGroup)
{
	// Execute the shell
	mShellPid = file::execute(	"/bin/shell",
								Port,
								lightOS::thread::suspended);
	
	if (mShellPid != 0)
	{
		// Create stdin
		string pipeName;
		libkernel::message_port stdinPort(create_pipe_name(pipeName));
		mStdin = new file(stdinPort, false);
		mStdin->create(pipeName.c_str(), file::stream, attr::tty | access::write);
		stdinPort.release();
		
		// Add stdin to the waitgroup
		// NOTE WaitGroup.add(mStdin->id().first, message(0, 0), this);
		
		// Open stdin pipe
		file stdinPipe(pipeName.c_str(), access::read);
		pair<libkernel::port_id_t,libkernel::port_id_t> portInfo = stdinPipe.release();
		
		// Create other ports
		libkernel::message_port stdoutPort;
		libkernel::message_port stderrPort;
		
		// Transfer ports
		transfer_port(portInfo.first, mShellPid);
		transfer_port(stdoutPort.id(), mShellPid);
		transfer_port(stderrPort.id(), mShellPid);
		
		// Redirect ports
		set_standard_port(mShellPid, libkernel::message_port::in, portInfo.first, portInfo.second);
		set_standard_port(mShellPid, libkernel::message_port::out, stdoutPort.id(), mStdout.id());
		set_standard_port(mShellPid, libkernel::message_port::err, stderrPort.id(), mStderr.id());
		
		// Initialize the member variables
		mShellStdout = stdoutPort.id();
		mShellStderr = stderrPort.id();
		
		// Release port classes
		stdoutPort.release();
		stderrPort.release();
		
		// Resume the shell
		resume_thread(mShellPid);
	}
}
