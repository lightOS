/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SERVER_CONSOLE_CONSOLE_HPP
#define SERVER_CONSOLE_CONSOLE_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup console console */
/*@{*/

#include <stdint.h>
#include <lightOS/device.hpp>
#include <lightOS/lightOS.hpp>
#include "vram.hpp"

class console : public libkernel::message_port::wait_group::handler
{
	public:
		inline console()
			:	mStdin(0), mShellPid(0), mShellStdout(0), mShellStderr(0), mVram(0), mVramCopy(false), bEcho(true),
				bCBreak(false){mVramCopy.clear();}
		inline console(libkernel::port_id_t StdIn, libkernel::port_id_t StdOut, libkernel::port_id_t StdErr)
			: mStdin(0), mStdout(StdOut, true), mStderr(StdErr, true), mShellPid(0), mShellStdout(0), mShellStderr(0), mVram(0),
			mVramCopy(false), bEcho(true), bCBreak(false){mVramCopy.clear();}
		virtual void received(libkernel::port_id_t PortID, libkernel::message &msg);
		void show(vram *Vram);
		vram *unshow();
		vram &get();
		void wait_group_add(libkernel::message_port::wait_group &WaitGroup);
		void keyboard(lightOS::keyboardData &Data, libkernel::message_port &Port, libkernel::message_port::wait_group &WaitGroup);
		void execute_shell(libkernel::message_port &Port, libkernel::message_port::wait_group &WaitGroup);
	private:
		lightOS::file *mStdin;
		libkernel::message_port mStdout;
		libkernel::message_port mStderr;
		lightOS::processId mShellPid;
		libkernel::port_id_t mShellStdout;
		libkernel::port_id_t mShellStderr;
		
		vram *mVram;
		vram mVramCopy;
		bool bEcho;
		bool bCBreak;
};

/*@}*/
/*@}*/

#endif
