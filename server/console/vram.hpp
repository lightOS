/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SERVER_CONSOLE_VRAM_HPP
#define SERVER_CONSOLE_VRAM_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup console console */
/*@{*/

#include <cstddef>
#include <cstdint>

#define COLOR_NORMAL																0x07
#define COLOR_ERROR																	0x04
#define COLOR_LOG																	0x09

class vram
{
	public:
		vram(bool bVram);
		void print(	char c,
					uint8_t color);
		void print(	const char *s,
					uint8_t color);
		void clear();
		void cursor(size_t x, size_t y);
		void cursor_color(uint8_t color){mCursorColor = color;cursor();}
		static void copy(vram &dest, const vram &src);
		static void copy(vram &dest, const void *data);
	private:
		void put(char c, uint8_t color);
		void scroll();
		void cursor();
		
		bool bReal;
		uint8_t *mVram;
		size_t mOffset;
		bool bCursorOn;
		uint8_t mCursorColor;
};

/*@}*/
/*@}*/

#endif
