/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libarch/ioport.hpp>
#include <lightOS/lightOS.hpp>
#include "vram.hpp"
using namespace lightOS;
using namespace std;

vram::vram(bool bVram)
	: bReal(bVram), mOffset(0), bCursorOn(true), mCursorColor(COLOR_NORMAL)
{
	if (bReal == true)
	{
		// Request access to the I/O ports
		// TODO FIXME HACK
		allocate_io_port_range(0, 0);
		
		// Map the video ram
		mVram = reinterpret_cast<uint8_t*>(allocate_region(	0x1000,
															context::write | context::user | context::write_through,
															reinterpret_cast<void*>(0xB8000)));
	}
	else
	{
		// Allocate space for a vram copy
		mVram = new uint8_t[2 * 80 * 25];
	}
}
void vram::clear()
{
	memset(mVram, 0, 25 * 80 * 2);
	mOffset = 0;
	cursor();
}
void vram::put(char c, uint8_t color)
{
	if (c == '\n')mOffset += 160 - (mOffset % 160);
	else if (c == '\r')mOffset -= mOffset % 160;
	else if (c == '\t')
	{
		mVram[mOffset++] = 0;
		mVram[mOffset++] = 0;
		mVram[mOffset++] = 0;
		mVram[mOffset++] = 0;
	}
	else if (c == '\b')
	{
		if (mOffset != 0)
		{
			mVram[--mOffset] = 0;
			mVram[--mOffset] = 0;
		}
	}
	else
	{
		mVram[mOffset++] = c;
		mVram[mOffset++] = color;
	}
	scroll();
}
void vram::cursor(size_t x, size_t y)
{
	mOffset = (y % 25) * 160 + (x % 80) * 2;
	cursor();
}
void vram::print(	char c,
					uint8_t color)
{
	put(c, color);
	cursor();
}
void vram::print(	const char *s,
					uint8_t color)
{
	while (*s != '\0')
		put(*s++, color);
	cursor();
}
void vram::scroll()
{
	if (mOffset >= (160 * 25))
	{
		memmove(mVram, &mVram[160], 160 * 24);
		memset(&mVram[160*24], 0, 160);
		mOffset -= 160;
	}
}
void vram::cursor()
{
	if (bCursorOn == true && mCursorColor == 0)
	{
		libarch::out8(0x3D4, 0x0A);
		libarch::out8(0x3D5, 0x2F);
		libarch::out8(0x3D4, 0x0B);
		libarch::out8(0x3D5, 0x0F);
		bCursorOn = false;
	}
	if (bCursorOn == false && mCursorColor != 0)
	{
		libarch::out8(0x3D4, 0x0A);
		libarch::out8(0x3D5, 0x0F);
		libarch::out8(0x3D4, 0x0B);
		libarch::out8(0x3D5, 0x0F);
		bCursorOn = true;
	}
	
	if (bCursorOn == true)
	{
		if (mVram[mOffset + 1] == 0)mVram[mOffset + 1] = mCursorColor;
		if (bReal)
		{
			libarch::out8(0x3D4, 0x0F);
			libarch::out8(0x3D5, mOffset >> 1);
			libarch::out8(0x3D4, 0x0E);
			libarch::out8(0x3D5, mOffset >> 9);
		}
	}
}
void vram::copy(vram &dest, const vram &src)
{
	copy(dest, src.mVram);
	
	dest.mOffset = src.mOffset;
	dest.mCursorColor = src.mCursorColor;
	dest.cursor();
}
void vram::copy(vram &dest, const void *data)
{
	memcpy(	dest.mVram,
			data,
			2 * 80 * 25);
}
