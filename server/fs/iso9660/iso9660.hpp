/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SERVER_FS_ISO9660_HPP
#define SERVER_FS_ISO9660_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup filesystem filesystem */
/*@{*/

#include <vector>
#include <utility>
#include <lightOS/cache.hpp>
#include "../filesystem.hpp"

namespace server
{
	class iso9660 : public filesystem
	{
		friend class file;
		public:
			/*! The constructor */
			inline iso9660(){}
			/*! The destructor */
			inline virtual ~iso9660(){}
			
			virtual void info(	size_t &size,
								size_t &free) const;
			virtual const char *name() const;
			virtual bool mount(const char *device);
			virtual file *findFile(const char *name);
			virtual file *createFile(	unsigned long port,
										const char *name,
										size_t type,
										size_t blocksize,
										size_t blockcount,
										size_t flags);
			virtual file *openFile(	unsigned long port,
									const char *name);
		private:
			class directory;
			class file : public filesystem::file
			{
				friend class fat;
				public:
					/*! The constructor */
					inline file(directory *dir,
								const char *name,
								size_t blockcount)
						:	filesystem::file(dir, name, lightOS::file::block, 1, blockcount){}
					/*! The destructor */
					inline virtual ~file(){}
					
					virtual void read(	unsigned long port,
										size_t block,
										size_t size);
					virtual void write(	unsigned long port,
										size_t block,
										libkernel::shared_memory &shm);
					virtual bool set_file_size(	unsigned long port,
												size_t block);
				private:
			};
			class directory : public filesystem::directory
			{
				friend class fat;
				public:
					/*! The constructor */
					inline directory(directory *dir, const char *name)
						:	filesystem::directory(dir, name){}
					/*! The destructor */
					inline virtual ~directory(){}
				protected:
			};
			
			std::vector<filesystem::file*> mFiles;
			lightOS::cache mDevice;
	};
}

/*@}*/
/*@}*/

#endif
