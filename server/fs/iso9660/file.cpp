/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include "iso9660.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

void iso9660::file::read(	unsigned long port,
							size_t block,
							size_t size)
{
	cout << "TODO: iso9660::file::read" << endl;
}
void iso9660::file::write(	unsigned long port,
							size_t block,
							libkernel::shared_memory &shm)
{
	cout << "TODO: iso9660::file::write" << endl;
}
bool iso9660::file::set_file_size(	unsigned long port,
									size_t block)
{
	cout << "TODO: fat::file::set_file_size()" << endl;
	return false;
}
