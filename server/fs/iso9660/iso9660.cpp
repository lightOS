/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cctype>
#include <iostream>
#include <iterator>
#include <algorithm>
#include "iso9660.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

filesystem &filesystem::mInst = (*new iso9660());

void iso9660::info(size_t &size, size_t &free) const
{
	// TODO: Size is not the disk size, is it?
	size = mDevice.blocksize() * mDevice.blockcount();
	free = 0;
}
const char *iso9660::name() const
{
	return "iso9660";
}
bool iso9660::mount(const char *device)
{
	cout << "iso9660: mount(\"" << device << "\")" << endl;
	
	if (mDevice.open(device, access::read) == false)return false;
	
	return true;
}
filesystem::file *iso9660::findFile(const char *name)
{
	cout << "iso9660: findFile(\"" << name << "\")" << endl;
	return 0;
}
filesystem::file *iso9660::createFile(	unsigned long port,
										const char *name,
										size_t type,
										size_t blocksize,
										size_t blockcount,
										size_t flags)
{
	cout << "iso9660: createFile(\"" << name << "\")" << endl;
	return 0;
}
filesystem::file *iso9660::openFile(unsigned long port,
									const char *name)
{
	cout << "iso9660: openFile(\"" << name << "\")" << endl;
	return 0;
}
