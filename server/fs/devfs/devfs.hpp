/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SERVER_FS_DEVFS_HPP
#define SERVER_FS_DEVFS_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup filesystem filesystem */
/*@{*/

#include <vector>
#include "../filesystem.hpp"

namespace server
{
	class devfs : public filesystem
	{
		public:
			/*! The constructor */
			inline devfs():root(0, 0){}
			/*! The destructor */
			inline virtual ~devfs(){}
			
			virtual void info(	size_t &size,
								size_t &free) const;
			virtual const char *name() const;
			virtual bool mount(const char *device);
			virtual file *findFile(const char *name);
			virtual file *createFile(	unsigned long port,
										const char *name,
										size_t type,
										size_t blocksize,
										size_t blockcount,
										size_t flags);
			virtual file *openFile(	unsigned long port,
									const char *name);
		private:
			class block : public filesystem::file
			{
				public:
					/*! The constructor */
					inline block(	directory *dir,
									const char *name,
									size_t type,
									size_t blocksize,
									size_t blockcount)
						: file(dir, name, type, blocksize, blockcount), mCreatorPort(0){}
					/*! The destructor */
					inline virtual ~block(){}
					
					virtual void create(unsigned long port);
					virtual bool open(unsigned long port);
					virtual bool close(unsigned long port);
					virtual void read(	unsigned long port,
										size_t block,
										size_t size);
					virtual void write(	unsigned long port,
										size_t block,
										libkernel::shared_memory &shm);
				private:
					unsigned long mCreatorPort;
			};
			
			filesystem::directory root;
	};
}

/*@}*/
/*@}*/

#endif
