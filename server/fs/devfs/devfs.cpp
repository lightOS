/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include "devfs.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

filesystem &filesystem::mInst = (*new devfs());

void devfs::info(size_t &size, size_t &free) const
{
	size = 0;
	free = 0;
}
const char *devfs::name() const
{
	return "devfs";
}
bool devfs::mount(const char *device)
{
	return true;
}
filesystem::file *devfs::createFile(unsigned long port,
									const char *name,
									size_t type,
									size_t blocksize,
									size_t blockcount,
									size_t flags)
{
	if ((flags & attr::mask) != 0)
	{
		cerr << "devfs: invalid file flags (0x" << hex << flags << ")" << endl;
		return 0;
	}
	
	filesystem::file *File;
	if (type == lightOS::file::block)
	{
		File = new block(0, name, type, blocksize, blockcount);
	}
	else if (type == lightOS::file::stream)
	{
		File = new block(0, name, type, blocksize, 1);
	}
	else
	{
		cerr << "devfs: unknown file type (0x" << hex << type << ")" << endl;
		return 0;
	}
	root.mFiles.push_back(File);
	return File;
}
filesystem::file *devfs::findFile(const char *name)
{
	if (name[0] == '\0')return &root;
	for (size_t i=0;i < root.mFiles.size();i++)
		if (strcmp(root.mFiles[i]->name().c_str(), name) == 0)return root.mFiles[i];
	return 0;
}
filesystem::file *devfs::openFile(	unsigned long port,
									const char *name)
{
	return findFile(name);
}
