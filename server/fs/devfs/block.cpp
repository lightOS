/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include "devfs.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

void devfs::block::create(unsigned long port)
{
	mCreatorPort = port;
}
bool devfs::block::open(unsigned long port)
{
	if (mOpener.size() != 0)return false;
	return this->file::open(port);
}
bool devfs::block::close(unsigned long port)
{
	if (mCreatorPort == port)mCreatorPort = 0;
	else if (mOpener.size() != 0 && mOpener[0] == port)mOpener[0] = 0;
	else return false;
	return true;
}
void devfs::block::read(unsigned long port,
						size_t block,
						size_t size)
{
	if ((size % mBlocksize) == 0 &&
		block < mBlockcount)
	{
		if (port == mCreatorPort)
		{
			if (mOpener.size() != 0)
			{
				message reply(mOpener[0], libkernel::message::fs_write_file, block, size);
				Port().send(reply);
			}
			message reply2(mCreatorPort, libkernel::message::fs_read_file, block, size);
			Port().send(reply2);
		}
		else if (mOpener.size() != 0 && port == mOpener[0])
		{
			if (this->type() != lightOS::file::stream)
			{
				message request(mCreatorPort, libkernel::message::fs_read_file, block, size);
				Port().send(request);
			}
		}
	}
	else
	{
		message reply(port, libkernel::message::fs_read_file, block);
		Port().send(reply);
	}
}
void devfs::block::write(	unsigned long port,
							size_t block,
							libkernel::shared_memory &shm)
{
	if ((shm.size() % mBlocksize) == 0 &&
		block < mBlockcount)
	{
		if (port == mCreatorPort)
		{
			if (mOpener.size() != 0)
			{
				message reply(mOpener[0], libkernel::message::fs_read_file, block, shm, libkernel::shared_memory::transfer_ownership);
				Port().send(reply);
			}
			message reply2(mCreatorPort, libkernel::message::fs_write_file, block, shm.size());
			Port().send(reply2);
		}
		else if (mOpener.size() != 0 && port == mOpener[0])
		{
			message request(mCreatorPort, libkernel::message::fs_write_file, block, shm, libkernel::shared_memory::transfer_ownership);
			Port().send(request);
		}
	}
	else
	{
		message reply(port, libkernel::message::fs_write_file, block);
		Port().send(reply);
	}
}
