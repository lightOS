/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include "tmpfs.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

#define BLOCK_SIZE 4096

void tmpfs::block::read(unsigned long port,
						size_t block,
						size_t size)
{
	if (block < mBlockcount)
	{
		libkernel::shared_memory shm(size);
		void *data = shm.address<void>();
		size_t iIndex = block / BLOCK_SIZE;
		size_t iOffset = block % BLOCK_SIZE;
		while (size != 0)
		{
			size_t toCpy = BLOCK_SIZE - iOffset;
			if (size < toCpy)toCpy = size;
			
			memcpy(	data,
					reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(mFileBlocks[iIndex]) + iOffset),
					toCpy);
			
			data = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(data) + toCpy);
			++iIndex;
			iOffset = 0;
			size -= toCpy;
		}
		message reply(port, libkernel::message::fs_read_file, block, shm, libkernel::shared_memory::transfer_ownership);
		Port().send(reply);
	}
	else
	{
		message reply(port, libkernel::message::fs_read_file, block);
		Port().send(reply);
	}
}
void tmpfs::block::write(	unsigned long port,
							size_t block,
							libkernel::shared_memory &shm)
{
	size_t iNeededBlocks = (block + shm.size() + BLOCK_SIZE - 1) / BLOCK_SIZE;
	while (iNeededBlocks > mFileBlocks.size())
	{
		// FIXME: The physical address is not important
		void *region = allocate_region(BLOCK_SIZE, memory::below_4gb);
		memset(region, 0, BLOCK_SIZE);
		mFileBlocks.push_back(region);
	}
	
	size_t remSize = shm.size();
	void *data = shm.address<void>();
	size_t iIndex = block / BLOCK_SIZE;
	size_t iOffset = block % BLOCK_SIZE;
	while (remSize != 0)
	{
		size_t toCpy = BLOCK_SIZE - iOffset;
		if (remSize < toCpy)toCpy = remSize;
		
		memcpy(	reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(mFileBlocks[iIndex]) + iOffset),
				data,
				toCpy);
		
		data = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(data) + toCpy);
		++iIndex;
		iOffset = 0;
		remSize -= toCpy;
	}
	
	if ((block + shm.size()) > mBlockcount)
		mBlockcount = block + shm.size();
	
	message reply(port, libkernel::message::fs_write_file, block, shm.size());
	Port().send(reply);
}
