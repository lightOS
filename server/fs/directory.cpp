/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include "filesystem.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

size_t filesystem::directory::blockcount() const
{
	return mFiles.size();
}
void filesystem::directory::read(	unsigned long port,
									size_t block,
									size_t size)
{
	if (block < blockcount())
	{
		libkernel::shared_memory shm(sizeof(lightOS::file::name_info));
		lightOS::file::name_info *info = shm.address<lightOS::file::name_info>();
		strncpy(info->name, mFiles[block]->name().c_str(), MAX_PATH);
		message reply(port, libkernel::message::fs_read_file, block, shm, libkernel::shared_memory::transfer_ownership);
		Port().send(reply);
	}
	else
	{
		message reply(port, libkernel::message::fs_read_file, block);
		Port().send(reply);
	}
}
void filesystem::directory::write(	unsigned long port,
									size_t block,
									libkernel::shared_memory &shm)
{
	cout << "filesystem::directory: writting not supported" << endl;
}
