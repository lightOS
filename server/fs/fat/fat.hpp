/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SERVER_FS_FAT_HPP
#define SERVER_FS_FAT_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup filesystem filesystem */
/*@{*/

#include <vector>
#include <utility>
#include <lightOS/cache.hpp>
#include "../filesystem.hpp"

namespace server
{
	class fat : public filesystem
	{
		friend class file;
		public:
			/*! The constructor */
			inline fat():mbLongFileName(false){}
			/*! The destructor */
			inline virtual ~fat(){}
			
			virtual void info(	size_t &size,
								size_t &free) const;
			virtual const char *name() const;
			virtual bool mount(const char *device);
			virtual file *findFile(const char *name);
			virtual file *createFile(	unsigned long port,
										const char *name,
										size_t type,
										size_t blocksize,
										size_t blockcount,
										size_t flags);
			virtual file *openFile(	unsigned long port,
									const char *name);
		private:
			/*! The bootsector */
			struct bootsector
			{
				uint8_t		jmp[3];
				char		osname[8];
				uint16_t	sectorsize;
				uint8_t		clustersize;
				uint16_t	reservedsectors;
				uint8_t		fats;
				uint16_t	rootentries;
				uint16_t	sectors;
				uint8_t		mediatype;
				uint16_t	fatsize;
				uint16_t	tracksize;
				uint16_t	heads;
				uint32_t	hidden;
				uint32_t	sectors32;
			}__attribute__((packed));
			
			struct fsentry
			{
				union
				{
					struct
					{
						char name[11];
						uint8_t attribute;
						uint8_t reserved;
						uint8_t creationMs;
						uint16_t creationTime;
						uint16_t creationDate;
						uint16_t lastAccessDate;
						uint16_t firstClusterHi;
						uint16_t writeTime;
						uint16_t writeDate;
						uint16_t firstClusterLo;
						uint32_t size;
					}__attribute__((packed)) short_entry;
					struct
					{
						uint8_t order;
						unsigned char name0[10];
						uint8_t attribute;
						uint8_t type;
						uint8_t checksum;
						unsigned char name1[12];
						uint16_t firstClusterLo;
						unsigned char name2[4];
					}__attribute__((packed)) vfat;
				};
			}__attribute__((packed));
			
			class directory;
			class file : public filesystem::file
			{
				friend class fat;
				public:
					/*! The constructor */
					inline file(directory *dir,
								const char *name,
								size_t blockcount,
								size_t index)
						:	filesystem::file(dir, name, lightOS::file::block, 1, blockcount), mDirEntry(index){}
					/*! The destructor */
					inline virtual ~file(){}
					
					virtual void read(	unsigned long port,
										size_t block,
										size_t size);
					virtual void write(	unsigned long port,
										size_t block,
										libkernel::shared_memory &shm);
					virtual bool set_file_size(	unsigned long port,
												size_t block);
				private:
					size_t mDirEntry;
			};
			class directory : public filesystem::directory
			{
				friend class fat;
				public:
					/*! The constructor */
					inline directory(	directory *dir,
										const char *name,
										size_t index)
						:	filesystem::directory(dir, name),
							mEntryCount(0), mEntries(0), mDirEntry(index){}
					/*! The destructor */
					inline virtual ~directory(){delete []mEntries;}
				protected:
					size_t mEntryCount;
					fsentry *mEntries;
					size_t mDirEntry;
			};
			
			bool mbLongFileName;
			std::string mLongFileName;
			
			uint8_t checksum(unsigned char *name);
			char *getName(const char *name);
			void parseDirectory(directory &dir);
			bool readCluster(void *data, size_t cluster);
			size_t getFatEntry(size_t cluster) const;
			bool loadDirectory(directory &dir);
			size_t getFreeCluster();
			size_t readFile(file &File, void *data, size_t block, size_t size);
			filesystem::file *findFile(directory &dir, const char *name);
			
			void setFatEntry(	lightOS::cache::changeset &changeset,
								size_t cluster,
								size_t value);
			size_t writeFile(	lightOS::cache::changeset &changeset,
								file &File,
								void *buffer,
								size_t size,
								size_t offset);
			size_t writeFile(	file &File,
								void *buffer,
								size_t size,
								size_t offset);
			
			void add_to_changeset(directory &dir, lightOS::cache::changeset &changeset);
			
			std::vector<filesystem::file*> mFiles;
			lightOS::cache mDevice;
			
			size_t mSectorSize;
			size_t mSectorCount;
			size_t mClusterSize;
			size_t mClusterCount;
			
			unsigned char *mFat;
			size_t mFatStart;
			size_t mFatCount;
			size_t mFatSize;
			
			directory *mRoot;
			size_t mRootStart;
			size_t mRootCount;
			
			size_t mDataStart;
	};
}

/*@}*/
/*@}*/

#endif
