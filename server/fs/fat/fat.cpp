/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cctype>
#include <iostream>
#include <iterator>
#include <algorithm>
#include "fat.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

namespace attribute
{
	enum
	{
		read_only		= 0x01,
		hidden			= 0x02,
		system			= 0x04,
		volumeId		= 0x08,
		directory		= 0x10,
		archive			= 0x20,
		long_filename	= 0x0F,
		filetype		= 0x18
	};
}

filesystem &filesystem::mInst = (*new fat());

void fat::info(size_t &size, size_t &free) const
{
	size = mDevice.blocksize() * mDevice.blockcount();
	free = 0;
	for (size_t i=2;i < (mSectorCount - mDataStart);i++)
	{
		if (getFatEntry(i) == 0)free++;
	}
	free *= mSectorSize;
}
const char *fat::name() const
{
	return "fat";
}
uint8_t fat::checksum(unsigned char *name)
{
	uint8_t sum = 0;
	for (size_t len = 11;len != 0;len--)
	{
		sum = ((sum & 1) ? 0x80 : 0) + (sum >> 1) + *name++;
	}
	return sum;
}
bool fat::mount(const char *device)
{
	if (mDevice.open(device, access::read | access::write) == false)return false;
	
	unsigned char cBootsector[512];
	mDevice.block(&cBootsector, 0);
	bootsector *Bootsector = reinterpret_cast<bootsector*>(cBootsector);
	
	// Save the information
	mSectorSize = Bootsector->sectorsize;
	mSectorCount = Bootsector->sectors;
	mClusterSize = Bootsector->clustersize;
	
	mFatStart = Bootsector->reservedsectors;
	mFatCount = Bootsector->fats;
	mFatSize = Bootsector->fatsize;
	
	mRootStart = mFatStart + (mFatCount * mFatSize);
	mRootCount = Bootsector->rootentries;
	
	mDataStart = mRootStart + (mRootCount * sizeof(fsentry)) / mSectorSize;
	mClusterCount = (mSectorCount - mDataStart) / mClusterSize;
	
	// Load the FAT
	mFat = new unsigned char[mFatCount * mFatSize * mSectorSize];
	mDevice.block(mFat, mFatStart, mFatCount * mFatSize);
	
	// Load the root directory
	fsentry *root = new fsentry[mRootCount];
	mDevice.block(root, mRootStart, (mRootCount * sizeof(fsentry)) / mDevice.blocksize());
	mRoot = new directory(0, 0, 0);
	mRoot->mEntries = root;
	mRoot->mEntryCount = mRootCount;
	parseDirectory(*mRoot);
	
	return true;
}
char *fat::getName(const char *name)
{
	size_t length = 0;
	size_t lengthExtension = 0;
	for (;length < 8;length++)
		if (name[length] == 0x20)break;
	for (;lengthExtension < 3;lengthExtension++)
		if (name[8 + lengthExtension] == 0x20)break;
	
	size_t size = length + lengthExtension + 1;
	if (lengthExtension != 0)++size;
	
	char *ret = new char [size];
	for (size_t i=0;i < length;i++)
	{
		if (name[i] == 0x05)ret[i] = static_cast<char>(0xE5);
		else ret[i] = name[i];
	}
	if (lengthExtension != 0)
	{
		ret[length] = '.';
		for (size_t i=0;i < lengthExtension;i++)
		{
			if (name[8+i] == 0x05)ret[length+1+i] = static_cast<char>(0xE5);
			else ret[length+1+i] = name[8+i];
		}
	}
	ret[size-1] = '\0';
	return ret;
}
void fat::parseDirectory(directory &dir)
{
	for (size_t i=0;i < dir.mEntryCount;i++)
	{
		if (dir.mEntries[i].short_entry.name[0] == '\0')break;
		else if (dir.mEntries[i].short_entry.name[0] == static_cast<char>(0xE5))continue;
		else if ((dir.mEntries[i].short_entry.attribute & attribute::long_filename) == attribute::long_filename)
		{
			char name[14];
			size_t y = 0;
			for (size_t z=0;z < 10;z += 2)
				name[y++] = dir.mEntries[i].vfat.name0[z];
			for (size_t z=0;z < 12;z += 2)
				name[y++] = dir.mEntries[i].vfat.name1[z];
			for (size_t z=0;z < 4;z += 2)
				name[y++] = dir.mEntries[i].vfat.name2[z];
			name[13] = '\0';
			if (mbLongFileName)
			{
				string tmp(mLongFileName);
				mLongFileName.assign(name);
				mLongFileName.append(tmp);
			}
			else
			{
				mLongFileName = name;
				mbLongFileName = true;
			}
		}
		else if ((dir.mEntries[i].short_entry.attribute & attribute::filetype) == 0 ||
				(dir.mEntries[i].short_entry.attribute & attribute::filetype) == attribute::directory)
		{
			if (dir.mEntries[i].short_entry.name[0] == '.' &&
				dir.mEntries[i].short_entry.name[1] == 0x20)continue;
			if (dir.mEntries[i].short_entry.name[0] == '.' &&
				dir.mEntries[i].short_entry.name[1] == '.' &&
				dir.mEntries[i].short_entry.name[2] == 0x20)continue;
			
			const char *name;
			if (mbLongFileName == false){name = getName(dir.mEntries[i].short_entry.name);}
			else name = mLongFileName.c_str();
			
			bool found = false;
			for (size_t y = 0;y < dir.mFiles.size();y++)
				if (dir.mFiles[y]->name() == name)
					found = true;
			
			if (found == false)
			{
				filesystem::file *File;
				if ((dir.mEntries[i].short_entry.attribute & attribute::filetype) == 0)
				{
					File = new file(&dir, name, dir.mEntries[i].short_entry.size, i);
				}
				else
				{
					File = new directory(&dir, name, i);
				}
				dir.mFiles.push_back(File);
			}
			
			if (mbLongFileName == false)delete []name;
			else mLongFileName.erase(mLongFileName.begin(), mLongFileName.end());
			mbLongFileName = false;
		}
		else if ((dir.mEntries[i].short_entry.attribute & attribute::filetype) == attribute::volumeId)
		{
			//TODO: Volumename
		}
	}
}
bool fat::readCluster(void *data, size_t cluster)
{
	size_t sector = (cluster - 2) * mClusterSize + mDataStart;
	if (sector >= mSectorCount)return false;
	if (mDevice.block(data, sector, mClusterSize) == 0)return false;
	return true;
}
size_t fat::getFatEntry(size_t cluster) const
{
	size_t index = (cluster * 12) / 8;
	size_t remainder = (cluster * 12) % 8;
	if (remainder == 0)
	{
		return mFat[index] | ((mFat[index+1] & 0x0F) << 8);
	}
	else
	{
		return ((mFat[index] & 0xF0) >> 4) | (mFat[index+1] << 4);
	}
	return 0;
}
void fat::setFatEntry(cache::changeset &changeset, size_t cluster, size_t value)
{
	size_t index = (cluster * 12) / 8;
	size_t remainder = (cluster * 12) % 8;
	if (remainder == 0)
	{
		mFat[index] = value;
		mFat[index+1] = (mFat[index+1] & 0xF0) | ((value >> 8) & 0x0F);
	}
	else
	{
		mFat[index] = (mFat[index] & 0x0F) | ((value & 0x0F) << 4);
		mFat[index+1] = value >> 4;
	}
	
	/* Add to changeset */
	changeset.add_blocks(mFat, mFatStart, mFatCount * mFatSize);
}
size_t fat::getFreeCluster()
{
	for (size_t i=3;i < mClusterCount;i++)
		if (getFatEntry(i) == 0)return i;
	cerr << "fat: No free cluster available" << endl;
	return 0;
}
bool fat::loadDirectory(directory &dir)
{
	if (dir.mEntries != 0)return true;
	
	// Calculate the cluster chain length
	size_t clusters = 1;
	size_t cur = (dir.get_directory<directory>()->mEntries[dir.mDirEntry].short_entry.firstClusterHi << 16) |
				  dir.get_directory<directory>()->mEntries[dir.mDirEntry].short_entry.firstClusterLo;
	if (cur == 0)return true;
	while (true)
	{
		cur = getFatEntry(cur);
		if (cur == 0xFFF)break;
		++clusters;
	}
	
	dir.mEntries = new fsentry[(clusters * mClusterSize * mSectorSize) / sizeof(fsentry)];
	cur = (dir.get_directory<directory>()->mEntries[dir.mDirEntry].short_entry.firstClusterHi << 16) |
		   dir.get_directory<directory>()->mEntries[dir.mDirEntry].short_entry.firstClusterLo;
	for (size_t i=0;i < clusters;i++)
	{
		readCluster(&dir.mEntries[(i * mClusterSize * mSectorSize) / sizeof(fsentry)], cur);
		cur = getFatEntry(cur);
	}
	
	dir.mBlockcount = dir.mFiles.size();
	dir.mEntryCount = clusters * mClusterSize * mSectorSize / sizeof(fsentry);
	parseDirectory(dir);
	return true;
}
size_t fat::readFile(file &File, void *data, size_t block, size_t size)
{
	if (block > File.blockcount())return 0;
	else if ((block + size) > File.blockcount())
	{
		size = File.blockcount() - block;
	}
	
	size_t start = block / mClusterSize / mSectorSize;
	
	// Find the first cluster, that should be read
	size_t cluster = (File.get_directory<directory>()->mEntries[File.mDirEntry].short_entry.firstClusterHi << 16) |
					  File.get_directory<directory>()->mEntries[File.mDirEntry].short_entry.firstClusterLo;
	for (size_t i=0;i < start;i++)cluster = getFatEntry(cluster);
	
	// Read the first cluster
	uint8_t *tmp = new uint8_t[mSectorSize * mClusterSize];
	readCluster(tmp, cluster);
	
	// Copy the first cluster into the buffer
	size_t firstSize = (mClusterSize * mSectorSize) - (block % (mClusterSize * mSectorSize));
	if (firstSize >= size)firstSize = size;
	memcpy(data, &tmp[block % (mClusterSize * mSectorSize)], firstSize);
	
	// Load & copy the following clusters
	size_t nextCount = (size - firstSize) / mClusterSize / mSectorSize;
	for (size_t i=0;i < nextCount;i++)
	{
		cluster = getFatEntry(cluster);
		readCluster(tmp, cluster);
		memcpy(&reinterpret_cast<char*>(data)[firstSize + i * mClusterSize * mSectorSize], tmp, mClusterSize * mSectorSize);
	}
	
	// Load & copy the last sector
	size_t lastSize = (size - firstSize) % (mClusterSize * mSectorSize);
	if (lastSize != 0)
	{
		cluster = getFatEntry(cluster);
		readCluster(tmp, cluster);
		memcpy(&reinterpret_cast<char*>(data)[firstSize + nextCount * mClusterSize * mSectorSize], tmp, lastSize);
	}
	
	delete []tmp;
	
	return size;
}
size_t fat::writeFile(	lightOS::cache::changeset &changeset,
						file &File,
						void *buffer,
						size_t size,
						size_t offset)
{
	//NOTE TODO: This cannot resize the root directory!
	
	/* Calculate the number of needed cluster */
	size_t neededCluster = (size + offset) / (mSectorSize * mClusterSize);
	if (((size + offset) % (mSectorSize * mClusterSize)) != 0)++neededCluster;
	
	/* Calculate the number of existing clusters */
	size_t cluster = (File.get_directory<directory>()->mEntries[File.mDirEntry].short_entry.size) / (mSectorSize * mClusterSize);
	if (((File.get_directory<directory>()->mEntries[File.mDirEntry].short_entry.size) % (mSectorSize * mClusterSize)) != 0)++cluster;
	
	/* Go to the last cluster */
	size_t clusterId = (File.get_directory<directory>()->mEntries[File.mDirEntry].short_entry.firstClusterHi << 16) |
						File.get_directory<directory>()->mEntries[File.mDirEntry].short_entry.firstClusterLo;
	for (size_t i = 1;i < cluster;i++)clusterId = getFatEntry(clusterId);
	
	/* Resize the file if needed */
	size_t lastCluster = 0;
	for (size_t i = cluster;i < neededCluster;i++)
	{
		lastCluster = getFreeCluster();
		setFatEntry(changeset, lastCluster, 0xFFF);
		if (i == 0)
		{
			File.get_directory<directory>()->mEntries[File.mDirEntry].short_entry.firstClusterLo = lastCluster;
			File.get_directory<directory>()->mEntries[File.mDirEntry].short_entry.firstClusterHi = lastCluster >> 16;
		}
		else
		{
			setFatEntry(changeset, clusterId, lastCluster);
		}
		clusterId = lastCluster;
	}
	
	/* Get the first cluster to write */
	size_t firstCluster = (File.get_directory<directory>()->mEntries[File.mDirEntry].short_entry.firstClusterHi << 16) |
						   File.get_directory<directory>()->mEntries[File.mDirEntry].short_entry.firstClusterLo;
	for (size_t i = 0;i < (offset / (mSectorSize * mClusterSize));i++)
	{
		firstCluster = getFatEntry(firstCluster);
	}
	
	/* Write to the file (if the write is not aligned to a clusters size) */
	size_t unalignedSize = (mSectorSize * mClusterSize) - offset % (mSectorSize * mClusterSize);
	if (unalignedSize == (mSectorSize * mClusterSize))unalignedSize = 0;
	if (unalignedSize > size)unalignedSize = size;
	size_t remainingSize = size - unalignedSize;
	if (unalignedSize != 0)
	{
		size_t unalignedOffset = (offset % (mSectorSize * mClusterSize));
		size_t unalignedSrcOffset = 0;
		for (size_t i = 0;i < mClusterSize;i++)
		{
			if (unalignedOffset < ((i + 1) * mSectorSize))
			{
				size_t cpySize = mSectorSize - unalignedOffset;
				if (unalignedSize < cpySize)cpySize = unalignedSize;
				void *buf = mDevice.uncache_block((firstCluster - 2) * mClusterSize + i + mDataStart);
				memcpy(	reinterpret_cast<uint8_t*>(buf) + (unalignedOffset % mSectorSize),
						reinterpret_cast<uint8_t*>(buffer) + unalignedSrcOffset,
						cpySize);
				changeset.add_blocks(buf, (firstCluster - 2) * mClusterSize + i + mDataStart, 1);
				
				unalignedSize -= cpySize;
			}
		}
		firstCluster = getFatEntry(firstCluster);
	}
	
	/* Write the clustersize-aligned cluster to the file and add the clusters to the changeset */
	size_t clusterCount = remainingSize / (mSectorSize * mClusterSize);
	for (size_t i = 0;i < clusterCount;i++)
	{
		void *buf = new uint8_t[mSectorSize * mClusterSize];
		memcpy(	buf,
				reinterpret_cast<uint8_t*>(buffer) + size - remainingSize,
				mSectorSize * mClusterSize);
		changeset.add_blocks(buf, (firstCluster - 2) * mClusterSize + mDataStart, mClusterSize);
		firstCluster = getFatEntry(firstCluster);
		remainingSize -= mSectorSize * mClusterSize;
	}
	
	/* Write the remaining unaligned cluster to the file */
	if (remainingSize != 0)
	{
		for (size_t i = 0;i < mClusterSize;i++)
		{
			if (remainingSize > 0)
			{
				size_t cpySize = remainingSize;
				if (remainingSize > mSectorSize)cpySize = mSectorSize;
				
				void *buf = mDevice.uncache_block((firstCluster - 2) * mClusterSize + i + mDataStart);
				memcpy(	buf,
						reinterpret_cast<uint8_t*>(buffer) + size - remainingSize,
						cpySize);
				changeset.add_blocks(buf, (firstCluster - 2) * mClusterSize + i + mDataStart, 1);
				
				remainingSize -= cpySize;
			}
		}
		firstCluster = getFatEntry(firstCluster);
	}
	
	/* Update the information in the directory entry and add to the changeset */
	size_t newSize = File.get_directory<directory>()->mEntries[File.mDirEntry].short_entry.size;
	if ((offset + size) > newSize)newSize = File.get_directory<directory>()->mEntries[File.mDirEntry].short_entry.size = offset + size;
	File.blockcount(newSize);
	add_to_changeset(*File.get_directory<directory>(), changeset);
	
	return size;
}
size_t fat::writeFile(	file &File,
						void *buffer,
						size_t size,
						size_t offset)
{
	cache::changeset changeset;
	size_t result = writeFile(changeset, File, buffer, size, offset);
	if (mDevice.commit(changeset) == false)return 0;
	return result;
}
filesystem::file *fat::findFile(const char *name)
{
	return findFile(*mRoot, name);
}
filesystem::file *fat::findFile(directory &dir, const char *name)
{
	if (loadDirectory(dir) == false)return 0;
	if (name == 0)return &dir;
	
	size_t length = 0;
	while (	name[length] != '/' &&
			name[length] != '\0' &&
			name[length] != ' '){++length;}
	if (length == 0)return &dir;
	
	for (size_t i=0;i < dir.mFiles.size();i++)
	{
		filesystem::file *cur = dir.mFiles[i];
		if (length == cur->name().length() &&
			strncmp(name, cur->name().c_str(), length) == 0)
		{
			if (name[length] == '/' &&
				cur->type() == lightOS::file::directory)
			{
				return findFile(static_cast<directory&>(*cur), &name[length+1]);
			}
			else if (	name[length] == '\0' ||
						name[length] == ' ')return cur;
			else return 0;
		}
	}
	return 0;
}
filesystem::file *fat::createFile(	unsigned long port,
									const char *name,
									size_t type,
									size_t blocksize,
									size_t blockcount,
									size_t flags)
{
	if ((flags & attr::mask) != 0)
	{
		cerr << "fat: invalid file flags (0x" << hex << flags << ")" << endl;
		return 0;
	}
	
	if (type != lightOS::file::block &&
		type != lightOS::file::directory)
	{
		cerr << "fat: invalid file type (0x" << hex << type << ")" << endl;
		return 0;
	}
	
	// Extract path and filename
	string tmp(name);
	string::iterator cur = tmp.begin();
	string::iterator last = tmp.begin();
	while (cur != tmp.end())
	{
		if (*cur == '/')last = cur;
		cur++;
	}
	string Path;
	string Name;
	if (last != tmp.begin())
	{
		Path = string(tmp.begin(), last);
		Name = string(last + 1, tmp.end());
	}
	else Name = string(last, tmp.end());
	
	// Disallow creation of . and .. entries
	if (Name.length() == 0 || Name == "." || Name == "..")
	{
		cerr << "fat: invalid file name (\"" << Name << "\")" << endl;
		return 0;
	}
	
	// Split up in name + extension
	string::iterator cur2 = Name.begin();
	string::iterator last2 = Name.begin();
	while (cur2 != Name.end())
	{
		if (*cur2 == '.')last2 = cur2;
		cur2++;
	}
	string Extension;
	if (last2 != Name.begin())Extension = string(last2 + 1, Name.end());
	
	// find directory
	filesystem::file *File = findFile(*mRoot, Path.c_str());
	if (File == 0)
	{
		cout << "fat: path not found" << endl;
		return 0;
	}
	if (File->type() != lightOS::file::directory)
	{
		cout << "fat: path is not a directory" << endl;
		return 0;
	}
	directory *Dir = static_cast<directory*>(File);
	
	char name8_3[11];
	{
		memset(name8_3, 0, 11);
		size_t iSize = Name.length();
		if (Extension.length() != 0)iSize -= 1 + Extension.length();
		if (iSize > 8)iSize = 8;
		size_t iSize2 = Extension.length();
		if (iSize2 > 3)iSize2 = 3;
		memcpy(name8_3, Name.c_str(), iSize);
		memcpy(&name8_3[8], Extension.c_str(), iSize2);
		for (size_t i = 0;i < 12;i++)
			name8_3[i] = toupper(name8_3[i]);
	}
	
	cout << "in directory: \"" << Dir->name() << "\"" << endl;
	cout << "filename: \"" << Name << "\"" << endl;
	cout << "8.3: \"" << name8_3 << "\"" << endl;
	cout << "extension: \"" << Extension << "\"" << endl;
	
	// Calculate the number of needed directory entries
	size_t neededFreeEntries = 1 + (Name.length() / 13);
	if ((Name.length() % 13) != 0)++neededFreeEntries;
	
	// Find free directory entry
	size_t found = static_cast<size_t>(-1);
	{
		size_t cont = 0;
		for (size_t i = 0;i < Dir->mEntryCount;i++)
		{
			if (Dir->mEntries[i].short_entry.name[0] == static_cast<char>(0xE5) ||
				Dir->mEntries[i].short_entry.name[0] == 0x00)
			{
				if (found == (i - cont))
					++cont;
				else
				{
					found = i;
					cont = 1;
				}
				if (cont >= neededFreeEntries)
					break;
			}
		}
		
		if (cont < neededFreeEntries)
		{
			// TODO: resize directory
			cerr << "fat::createFile(): resize the directory" << endl;
			return 0;
		}
	}
	
	// Create the long filename entries
	size_t order = 1;
	const char *_Name = Name.c_str();
	uint8_t chksum = checksum(reinterpret_cast<unsigned char*>(name8_3));
	for (size_t i = (found + neededFreeEntries - 2);i > found;i--)
	{
		memset(&Dir->mEntries[i], 0, sizeof(fsentry));
		Dir->mEntries[i].vfat.order = order++;
		for (size_t y = 0;y < 10; y+= 2)
		{
			Dir->mEntries[i].vfat.name0[y] = *_Name++;
			Dir->mEntries[i].vfat.name0[y + 1] = 0;
		}
		Dir->mEntries[i].vfat.attribute = attribute::long_filename | ((type == lightOS::file::directory) ? attribute::directory : 0);
		Dir->mEntries[i].vfat.checksum = chksum;
		for (size_t y = 0;y < 12;y += 2)
		{
			Dir->mEntries[i].vfat.name1[y] = *_Name++;
			Dir->mEntries[i].vfat.name1[y + 1] = 0;
		}
		for (size_t y = 0;y < 4;y += 2)
		{
			Dir->mEntries[i].vfat.name2[y] = *_Name++;
			Dir->mEntries[i].vfat.name2[y + 1] = 0;
		}
	}
	memset(&Dir->mEntries[found], 0, sizeof(fsentry));
	Dir->mEntries[found].vfat.order = order | 0x40;
	Dir->mEntries[found].vfat.attribute = attribute::long_filename | ((type == lightOS::file::directory) ? attribute::directory : 0);
	Dir->mEntries[found].vfat.checksum = chksum;
	size_t k = 0;
	while (*_Name != '\0')
	{
		if (k < 5)
		{
			Dir->mEntries[found].vfat.name0[k * 2] = *_Name;
			Dir->mEntries[found].vfat.name0[k * 2 + 1] = 0;
		}
		else if (k < 11)
		{
			Dir->mEntries[found].vfat.name1[k * 2 - 10] = *_Name;
			Dir->mEntries[found].vfat.name1[k * 2 - 10 + 1] = 0;
		}
		else
		{
			Dir->mEntries[found].vfat.name2[k * 2 - 22] = *_Name;
			Dir->mEntries[found].vfat.name2[k * 2 - 22 + 1] = 0;
		}
		++_Name;
		++k;
	}
	
	// Create the normal directory entry
	size_t index = found + neededFreeEntries - 1;
	memcpy(	Dir->mEntries[index].short_entry.name,
			name8_3,
			11);
	Dir->mEntries[index].short_entry.attribute = ((type == lightOS::file::directory) ? attribute::directory : 0);
	Dir->mEntries[index].short_entry.reserved = 0;
	Dir->mEntries[index].short_entry.creationMs = 0;
	Dir->mEntries[index].short_entry.creationTime = 0;
	Dir->mEntries[index].short_entry.creationDate = 0;
	Dir->mEntries[index].short_entry.lastAccessDate = 0;
	Dir->mEntries[index].short_entry.writeTime = 0;
	Dir->mEntries[index].short_entry.writeDate = 0;
	Dir->mEntries[index].short_entry.firstClusterHi = 0;
	Dir->mEntries[index].short_entry.firstClusterLo = 0;
	Dir->mEntries[index].short_entry.size = 0;
	
	// Add the directory to the changeset
	cache::changeset changeset;
	add_to_changeset(*Dir, changeset);
	mDevice.commit(changeset);
	
	// Reparse the directory
	parseDirectory(*Dir);
	
	return openFile(port, name);
}
filesystem::file *fat::openFile(unsigned long port,
								const char *name)
{
	filesystem::file *File = findFile(*mRoot, name);
	if (File == 0)return 0;
	if (File->type() == lightOS::file::directory)
		if (loadDirectory(static_cast<directory&>(*File)) == false)return 0;
	return File;
}
void fat::add_to_changeset(directory &dir, lightOS::cache::changeset &changeset)
{
	if (dir.mDirectory == 0)
	{
		for (size_t i = 0;i < (mRootCount * sizeof(fsentry) / mDevice.blocksize());i++)
		{
			changeset.add_blocks(reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(dir.mEntries) + i * mDevice.blocksize()), i + mRootStart, 1);
		}
	}
	else
	{
		size_t clusterCount = (dir.mEntryCount * sizeof(fsentry)) / (mSectorSize * mClusterSize);
		size_t cluster = (dir.get_directory<directory>()->mEntries[dir.mDirEntry].short_entry.firstClusterHi << 16) |
						  dir.get_directory<directory>()->mEntries[dir.mDirEntry].short_entry.firstClusterLo;
		for (size_t i = 0;i < clusterCount;i++)
		{
			for (size_t y = 0;y < mClusterSize;y++)
			{
				char *block = reinterpret_cast<char*>(dir.mEntries) + i * mSectorSize * mClusterSize + y * mSectorSize;
				changeset.add_blocks(block, (cluster - 2) * mClusterSize + y + mDataStart, 1);
			}
			cluster = getFatEntry(cluster);
		}
	}
}
