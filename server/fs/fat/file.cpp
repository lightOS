/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include "fat.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

void fat::file::read(	unsigned long port,
						size_t block,
						size_t size)
{
	fat &Fat = static_cast<fat&>(filesystem::instance());
	
	char *buffer = new char[size];
	size_t result = Fat.readFile(	*this,
									buffer,
									block,
									size);
	if (result != 0)
	{
		libkernel::shared_memory shm(result);
		memcpy(shm.address<void>(), buffer, result);
		message reply(port, libkernel::message::fs_read_file, block, shm, libkernel::shared_memory::transfer_ownership);
		Port().send(reply);
	}
	else
	{
		message reply(port, libkernel::message::fs_read_file, block);
		Port().send(reply);
	}
	delete []buffer;
}
void fat::file::write(	unsigned long port,
						size_t block,
						libkernel::shared_memory &shm)
{
	fat &Fat = static_cast<fat&>(filesystem::instance());
	
	size_t result = Fat.writeFile(*this, shm.address<void>(), shm.size(), block);
	
	message reply(port, libkernel::message::fs_write_file, block, result);
	Port().send(reply);
}
bool fat::file::set_file_size(	unsigned long port,
								size_t block)
{
	cout << "TODO: fat::file::set_file_size()" << endl;
	return false;
}
