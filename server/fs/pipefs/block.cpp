/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include "pipefs.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

bool pipefs::block::send_to_opener()
{
	size_t index = mOpener.size() - 1;
	
	if (mOpenerReq[index].first == 0)return false;
	if (mOpenerBuffer.size() == 0 && mCreatorPort == 0)
	{
		message msg(mOpener[index], libkernel::message::fs_read_file, 0, static_cast<size_t>(-1));
		Port().send(msg);
		return true;
	}
	if (mOpenerBuffer.size() == 0)return false;
	
	size_t copied = 0;
	libkernel::shared_memory shm(mOpenerReq[index].first);
	while (copied < mOpenerReq[index].first)
	{
		if (mOpenerBuffer.size() == 0)break;
		
		size_t toCpy = mOpenerReq[index].first - copied;
		if (toCpy > mOpenerBuffer[0].second)toCpy = mOpenerBuffer[0].second;
		
		memcpy(	&shm.address<uint8_t>()[copied],
				mOpenerBuffer[0].first,
				toCpy);
		
		copied += toCpy;
		
		if (toCpy == mOpenerBuffer[0].second)
		{
			mOpenerBuffer.erase(mOpenerBuffer.begin());
		}
		else if (toCpy < mOpenerBuffer[0].second)
		{
			mOpenerBuffer[0].second -= toCpy;
			memmove(&reinterpret_cast<uint8_t*>(mOpenerBuffer[0].first)[0],
					&reinterpret_cast<uint8_t*>(mOpenerBuffer[0].first)[toCpy],
					mOpenerBuffer[0].second);
		}
	}
	if (copied < mOpenerReq[index].first)
	{
		// FIXME Could use shared-memory resizing here
		libkernel::shared_memory shared(copied);
		memcpy(	shared.address<void>(),
				shm.address<void>(),
				copied);
		shm.destroy();
		shm = shared;
	}
	
	message reply(mOpener[index], libkernel::message::fs_read_file, mOpenerReq[index].second, shm, libkernel::shared_memory::transfer_ownership);
	if (Port().send(reply) == false)
	{
		close(mOpener[index]);
	}
	else
	{
		mOpenerReq[index].first = 0;
		mOpenerReq[index].second = 0;
	}
	return true;
}
void pipefs::block::create(unsigned long port)
{
	mCreatorPort = port;
}
bool pipefs::block::open(unsigned long port)
{
	this->file::open(port);
	mOpenerReq.push_back(pair<size_t,size_t>(0,0));
	return true;
}
bool pipefs::block::close(unsigned long port)
{
	bool ret = true;
	if (mCreatorPort == port)
	{
		mCreatorPort = 0;
		if (mOpener.size() != 0)
			send_to_opener();
	}
	else
	{
		size_t index = get_opener_index(port);
		if (index != static_cast<size_t>(-1))
		{
			mOpener.erase(mOpener.begin() + index);
			mOpenerReq.erase(mOpenerReq.begin() + index);
		}
		else
		{
			ret = false;
		}
	}
	
	if (mCreatorPort == 0 &&
		mOpener.size() == 0)
	{
		// Delete the file
		pipefs &Pipefs = static_cast<pipefs&>(filesystem::instance());
		Pipefs.deleteFile(this);
	}
	
	return ret;
}
void pipefs::block::read(	unsigned long port,
							size_t block,
							size_t size)
{
	if ((size % mBlocksize) == 0)
	{
		if (port == mCreatorPort)
		{
			cerr << "pipefs: creator read" << endl;
		}
		else
		{
			size_t index = get_opener_index(port);
			if (index != static_cast<size_t>(-1))
			{
				mOpenerReq[index] = pair<size_t,size_t>(size, block);
				send_to_opener();
			}
		}
	}
	else
	{
		message reply(port, libkernel::message::fs_read_file, block);
		Port().send(reply);
	}
}
void pipefs::block::write(	unsigned long port,
							size_t block,
							libkernel::shared_memory &shm)
{
	if ((shm.size() % mBlocksize) == 0)
	{
		if (port == mCreatorPort)
		{
			if (mOpener.size() != 0)
			{
				// Buffer
				pair<void*,size_t> Pair;
				Pair.first = new uint8_t[shm.size()];
				Pair.second = shm.size();
				memcpy(	Pair.first,
						shm.address<void>(),
						Pair.second);
				mOpenerBuffer.push_back(Pair);
				
				// Try to send something to the opener
				send_to_opener();
			}
			message reply2(mCreatorPort, libkernel::message::fs_write_file, block, shm.size());
			Port().send(reply2);
		}
		else
		{
			cout << "pipefs: opener write" << endl;
		}
	}
	else
	{
		message reply(port, libkernel::message::fs_write_file, block);
		Port().send(reply);
	}
}
size_t pipefs::block::get_opener_index(libkernel::port_id_t Port)
{
	for (size_t i = 0;i < mOpener.size();i++)
		if (mOpener[i] == Port)
			return i;
	return static_cast<size_t>(-1);
}
