/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <iterator>
#include <algorithm>
#include "pipefs.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

filesystem &filesystem::mInst = (*new pipefs());

void pipefs::info(size_t &size, size_t &free) const
{
	size = 0;
	free = 0;
}
const char *pipefs::name() const
{
	return "pipefs";
}
bool pipefs::mount(const char *device)
{
	return true;
}
filesystem::file *pipefs::createFile(	unsigned long port,
										const char *name,
										size_t type,
										size_t blocksize,
										size_t blockcount,
										size_t flags)
{
	filesystem::file *File;
	if (type == lightOS::file::stream)
	{
		File = new block(0, name, type, blocksize, 1, flags & attr::mask);
	}
	else if (type == lightOS::file::block)
	{
		cerr << "pipefs: block files not supported" << endl;
		return 0;
	}
	else
	{
		cerr << "pipefs: unknown file type (0x" << hex << type << ")" << endl;
		return 0;
	}
	root.mFiles.push_back(File);
	return File;
}
filesystem::file *pipefs::findFile(const char *name)
{
	if (name[0] == '\0')return &root;
	for (size_t i=0;i < root.mFiles.size();i++)
		if (strcmp(root.mFiles[i]->name().c_str(), name) == 0)return root.mFiles[i];
	return 0;
}
filesystem::file *pipefs::openFile(	unsigned long port,
									const char *name)
{
	return findFile(name);
}
void pipefs::deleteFile(block *File)
{
	vector<filesystem::file*>::iterator i = root.mFiles.begin();
	for (;i != root.mFiles.end();i++)
		if ((*i) == File)
		{
			root.mFiles.erase(i);
			delete File;
			return;
		}
}
