/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <iterator>
#include "filesystem.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

string mountpoint;

int main()
{
	filesystem &fs = filesystem::instance();
	
	libkernel::message_port &Port = fs.port();
	Port.wait_for_other(libkernel::message_port::vfs);
	registerFilesystem(Port, fs.name());
	
	message msg;
	while (Port.get(msg))
	{
		libkernel::shared_memory shm = msg.get_shared_memory();
		switch (msg.type())
		{
			case libkernel::message::ping:
			{
				message reply(msg.port(), libkernel::message::pong, 0, 0, 0);
				Port.send(reply);
			}break;
			case libkernel::message::vfs_mount:
			{
				if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					mount_fs_info *info = shm.address<mount_fs_info>();
					info->name[MAX_PATH] = '\0';
					info->filesystem[MAX_FILESYSTEM_NAME] = '\0';
					info->device[MAX_PATH] = '\0';
					
					bool result = fs.mount(info->device);
					
					if (result == true)
					{
						mountpoint = info->name;
					}
					
					message reply(libkernel::message_port::vfs, libkernel::message::vfs_mount, static_cast<unsigned long>(result));
					Port.send(reply);
				}
			}break;
			case libkernel::message::fs_get_info:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					size_t size, free;
					fs.info(size, free);
					message reply(msg.port(), libkernel::message::fs_get_info, size, free);
					Port.send(reply);
				}
			}break;
			case libkernel::message::fs_create_file:
			{
				if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					lightOS::file::info *info = shm.address<lightOS::file::info>();
					info->name[MAX_PATH] = '\0';
					
					filesystem::file *File = fs.createFile(	msg.port(),
															info->name,
															info->type,
															info->blocksize,
															info->blockcount,
															msg.param1());
					if (File != 0)
					{
						File->create(msg.port());
						fs.addOpenedFile(msg.port(), File, msg.param1());
					}
					
					message reply(msg.port(), libkernel::message::fs_create_file, static_cast<unsigned long>(File != 0));
					Port.send(reply);
				}
			}break;
			case libkernel::message::fs_exists_file:
			{
				if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					lightOS::file::name_info *info = shm.address<lightOS::file::name_info>();
					info->name[MAX_PATH] = '\0';
					
					filesystem::file *File = fs.openFile(	msg.port(),
															info->name);
					
					size_t type = lightOS::file::none;
					if (File != 0)type = File->type();
					
					message reply(msg.port(), libkernel::message::fs_exists_file, type);
					Port.send(reply);
				}
			}break;
			case libkernel::message::fs_open_file:
			{
				if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					lightOS::file::info *info = shm.address<lightOS::file::info>();
					info->name[MAX_PATH] = '\0';
					
					filesystem::file *File = fs.openFile(	msg.port(),
															info->name);
					
					bool success = File != 0;
					unsigned long flags = msg.param1() & access::mask;
					if (success)
					{
						if ((flags & (~lightOS::access::info)) != 0)
						{
							success = File->open(msg.port());
						}
					}
					if (success)
					{
						fs.addOpenedFile(msg.port(), File, msg.param1());
					}
					
					message reply(msg.port(), libkernel::message::fs_open_file, static_cast<unsigned long>(success));
					Port.send(reply);
				}
			}break;
			case libkernel::message::fs_file_info:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					pair<filesystem::file*, unsigned long> File = fs.findOpenedFile(msg.port());
					if (File.first != 0)
					{
						message reply(	msg.port(),
										libkernel::message::fs_file_info,
										File.first->type(),
										File.first->blocksize(),
										File.first->blockcount());
						Port.send(reply);
					}
					else
					{
						message reply(msg.port(), libkernel::message::fs_file_info);
						Port.send(reply);
					}
				}
				else if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					// Get the file name
					lightOS::file::name_info *Info = shm.address<lightOS::file::name_info>();
					Info->name[MAX_PATH] = '\0';
					
					// Find the file
					filesystem::file *File = fs.findFile(Info->name);
					
					// Reply to the message
					size_t type = lightOS::file::none;
					size_t blocksize = 0;
					size_t blockcount = 0;
					if (File != 0)
					{
						type = File->type();
						blocksize = File->blocksize();
						blockcount = File->blockcount();
					}
					message reply(msg.port(), libkernel::message::fs_file_info, type, blocksize, blockcount);
					Port.send(reply);
				}
			}break;
			case libkernel::message::fs_read_file:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					pair<filesystem::file*,unsigned long> File = fs.findOpenedFile(msg.port());
					if (File.first != 0 && (File.second & lightOS::access::read) != 0)
					{
						File.first->read(	msg.port(),
											msg.param1(),
											msg.param2());
					}
					else
					{
						message reply(msg.port(), libkernel::message::fs_read_file, msg.param1());
						Port.send(reply);
					}
				}
			}break;
			case libkernel::message::fs_write_file:
			{
				if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
				{
					pair<filesystem::file*, unsigned long> File = fs.findOpenedFile(msg.port());
					if (File.first != 0 && (File.second & lightOS::access::write) != 0)
					{
						File.first->write(	msg.port(),
											msg.param1(),
											shm);
					}
					else
					{
						message reply(msg.port(), libkernel::message::fs_write_file, msg.param1());
						Port.send(reply);
					}
				}
			}break;
			case libkernel::message::fs_close_file:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					pair<filesystem::file*, unsigned long> File = fs.findOpenedFile(msg.port());
					bool result = false;
					if (File.first != 0)
					{
						result = File.first->close(msg.port());
						if (result == true)
						{
							fs.removeOpenedFile(msg.port());
						}
					}
					message reply(msg.port(), libkernel::message::fs_close_file, static_cast<unsigned long>(result));
					Port.send(reply);
				}
			}break;
			case libkernel::message::fs_set_file_size:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					pair<filesystem::file*, unsigned long> File = fs.findOpenedFile(msg.port());
					if (File.first != 0 && (File.second & lightOS::access::write) != 0)
					{
						bool result = File.first->set_file_size(msg.port(), msg.param1());
						message reply(msg.port(), libkernel::message::fs_set_file_size, msg.param1(), static_cast<size_t>(result));
						Port.send(reply);
					}
					else
					{
						message reply(msg.port(), libkernel::message::fs_set_file_size, msg.param1(), static_cast<size_t>(false));
						Port.send(reply);
					}
				}
			}break;
			case libkernel::message::fs_get_filename:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					pair<filesystem::file*,unsigned long> File = fs.findOpenedFile(msg.port());
					if (File.first != 0 && (File.second & lightOS::access::read) != 0)
					{
						libkernel::shared_memory shared(sizeof(_LIBSERVER_file_name_info));
						_LIBSERVER_file_name_info *Info = shared.address<_LIBSERVER_file_name_info>();
						strcpy(Info->name, mountpoint.c_str());
						vector<server::filesystem::directory*> dirList;
						server::filesystem::directory *Dir = File.first->get_directory<server::filesystem::directory>();
						while (Dir != 0 && Dir->name().length() != 0)
						{
							dirList.push_back(Dir);
							Dir = Dir->get_directory<server::filesystem::directory>();
						}
						for (vector<server::filesystem::directory*>::reverse_iterator i = dirList.rbegin();i != dirList.rend();i++)
						{
							strcat(Info->name, (*i)->name().c_str());
							strcat(Info->name, "/");
						}
						if (File.first->name().length() != 0)
							strcat(Info->name, File.first->name().c_str());
						
						message reply(msg.port(), libkernel::message::fs_get_filename, 0, shared, libkernel::shared_memory::transfer_ownership);
						Port.send(reply);
					}
					else
					{
						message reply(msg.port(), libkernel::message::fs_get_filename);
						Port.send(reply);
					}
				}
			}break;
			case libkernel::message::fs_get_file_flags:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					pair<filesystem::file*,unsigned long> File = fs.findOpenedFile(msg.port());
					size_t result = 0;
					if (File.first != 0)
					{
						result = File.first->get_flags();
					}
					message reply(msg.port(), libkernel::message::fs_get_file_flags, result);
					Port.send(reply);
				}
			}break;
			case libkernel::message::fs_get_opener_count:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					pair<filesystem::file*,unsigned long> File = fs.findOpenedFile(msg.port());
					size_t result = 0;
					if (File.first != 0)
					{
						result = File.first->get_opener_count();
					}
					message reply(msg.port(), libkernel::message::fs_get_opener_count, result);
					Port.send(reply);
				}
			}break;
			case libkernel::message::fs_enum_opener:
			{
				if (msg.attribute() == libkernel::shared_memory::none)
				{
					pair<filesystem::file*,unsigned long> File = fs.findOpenedFile(msg.port());
					size_t result = 0;
					if (File.first != 0)
					{
						result = File.first->get_opener(msg.param1());
					}
					message reply(msg.port(), libkernel::message::fs_enum_opener, result);
					Port.send(reply);
				}
			}break;
		}
	}
	filesystem::uninstance();
	return 0;
}
pair<filesystem::file*, unsigned long> filesystem::findOpenedFile(unsigned long port)
{
	for (unsigned int i=0;i < mOpenedFiles.size();i++)
		if (mOpenedFiles[i].first == port)return mOpenedFiles[i].second;
	return pair<filesystem::file*, unsigned long>(0, 0);
}
void filesystem::removeOpenedFile(unsigned long port)
{
	for (vector<pair<unsigned long,pair<file*, unsigned long> > >::iterator i=mOpenedFiles.begin();i != mOpenedFiles.end();i++)
		if ((*i).first == port)
		{
			mOpenedFiles.erase(i);
			return;
		}
}
bool filesystem::file::set_file_size(	unsigned long port,
										size_t block)
{
	cerr << "fs: file::set_file_size() unsupported" << endl;
	return false;
}
