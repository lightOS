/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SERVER_FS_EXT2_HPP
#define SERVER_FS_EXT2_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup filesystem filesystem */
/*@{*/

#include <vector>
#include <string>
#include <cstdint>
#include <lightOS/cache.hpp>
#include "../filesystem.hpp"

#define NAME_LEN 255

namespace server
{
	class ext2 : public filesystem
	{
		friend class file;
		
		public:
			/*! The constructor */
			inline ext2(){}
			/*! The destructor */
			inline virtual ~ext2(){}
			
			virtual void info(	size_t &size,
								size_t &free) const;
			virtual const char *name() const;
			virtual bool mount(const char *device);
			virtual file *findFile(const char *name);
			virtual file *createFile(	unsigned long port,
										const char *name,
										size_t type,
										size_t blocksize,
										size_t blockcount,
										size_t flags);
			virtual file *openFile(	unsigned long port,
									const char *name);
		private:
			class directory;
			class file : public filesystem::file
			{
				friend class ext2;
				public:
					/*! The constructor */
					inline file(directory *dir,
								const char *name,
								size_t blockcount,
								size_t inode)
						: filesystem::file(dir, name, lightOS::file::block, 1, blockcount), mInode(inode){}
					/*! The destructor */
					inline virtual ~file(){}
					
					virtual void read(	unsigned long port,
										size_t block,
										size_t size);
					virtual void write(	unsigned long port,
										size_t block,
										libkernel::shared_memory &shm);
					
					size_t inode() const{return mInode;}
					
				private:
					size_t mInode;
			};
			class directory : public filesystem::directory
			{
				friend class ext2;
				public:
					/*! The constructor */
					inline directory(directory *dir, const char *name, size_t inode)
						: filesystem::directory(dir, name), mInode(inode), mLoaded(false){}
							
					/*! The destructor */
					inline virtual ~directory(){}
					
					size_t inode() const{return mInode;}
				private:
					size_t mInode;
					bool mLoaded;
			};
			
			struct superblock
			{
				uint32_t inodeCount;		// Inodes count *
				uint32_t blockCount;		// Blocks count *
				uint32_t resBlockCount;		// Reserved blocks count *
				uint32_t freeBlockCount;	// Free blocks count *
				uint32_t freeInodeCount;	// Free inodes count *
				uint32_t firstDataBlock;	// First Data Block *
				uint32_t logBlockSize;		// Block size *
				uint32_t logFragSize;		// Fragment size *
				uint32_t blocksPerGroup;	// Blocks per group *
				uint32_t fragsPerGroup;		// Fragments per group *
				uint32_t inodesPerGroup;	// Inodes per group *
				uint32_t mountTime;			// Mount time *
				uint32_t writetime;			// Write time *
				uint16_t mntCount;		// Mount count *
				uint16_t maxMntCount;		// Maximal mount count *
				uint16_t magic;			// Magic signature *
				uint16_t state;			// File system state *
				uint16_t errors;			// Behaviour when detecting errors *
				uint16_t pad;
				uint32_t lastcheck;			// time of last check *
				uint32_t checkInterval;		// max. time between checks *
				uint32_t creatorOS;			// OS *
				uint32_t revLevel;			// Revision level *
				uint16_t defResuid;		// Default uid for reserved blocks *
				uint16_t defResgid;		// Default gid for reserved blocks *
				uint32_t reserved[235];		// Padding to the end of the block *
			} __attribute__((packed));
			
			struct groupDescriptor
			{
				uint32_t blockBitmap;
				uint32_t inodeBitmap;
				uint32_t inodeTable;
				uint16_t freeBlockCount;
				uint16_t freeInodeCount;
				uint16_t usedDirsCount;
				uint16_t pad;
				uint32_t reserved[3];
			} __attribute__((packed));
			
			struct inode
			{
				uint16_t mode;
				uint16_t uid;
				uint32_t size;
				uint32_t atime;
				uint32_t ctime;
				uint32_t mtime;
				uint32_t dtime;
				uint16_t gid;
				uint16_t linksCount;
				uint32_t blocks;
				uint32_t flags;
				uint32_t osd1;
				uint32_t block[12];
				uint32_t indirectBlock;
				uint32_t biindirectBlock;
				uint32_t triindirectBlock;
				uint32_t generation;
				uint32_t fileAcl;
				uint32_t dirAcl;
				uint32_t fAddr;
				uint8_t osd2[12];
			};
			
			struct group
			{
				groupDescriptor descriptor;
				uint32_t *blockBitmap;
				uint32_t *inodeBitmap;
				inode *inodeTable;
				
				group(){}
				group(const groupDescriptor &desc)
					: descriptor(desc), blockBitmap(0), inodeBitmap(0), inodeTable(0){}
			};
			
			struct directory_entry
			{
				uint32_t inode;
				uint16_t length;
				uint8_t name_length;
				uint8_t file_type;
			};
			
			superblock mSuperblock;
			size_t mBlockSize;
			size_t mLogicalBlockSize;
			std::vector<group> mGroupDescriptors;
			
			std::vector<filesystem::file*> mFiles;
			lightOS::cache mDevice;
			
			directory *mRootDirectory;
			
			bool readBlock(void *buffer, size_t block);
			bool writeBlock(const void *buffer, size_t block);
			bool loadInodeTable(size_t Group);
			/*! Load an Inode from device*/
			uint32_t *inode_bitmap(group &Group);
			inode *inode_table(group &Group);
			uint32_t *block_bitmap(group &Group);
			bool write_inode_bitmap(group &Group, size_t inodeId);
			bool write_inode_table(group &Group, size_t inodeId);
			bool write_block_bitmap(group &Group, size_t blockId);
			ext2::inode *getInode(size_t inodeID);
			size_t allocateInode();
			
			bool fillDirectory(directory &dir);
			size_t readInode(void *buffer, inode *Inode, size_t start, size_t end);
			filesystem::file* findFile(directory &dir, const char* name);
			size_t readFile(file &File, void *data, size_t block, size_t size);
			

	};
}

/*@}*/
/*@}*/

#endif
