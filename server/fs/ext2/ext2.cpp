/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <memory>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <string>
#include <lightOS/lightOS.hpp>
#include "ext2.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

filesystem &filesystem::mInst = (*new ext2());

void ext2::info(size_t &size, size_t &free) const
{
	size = mSuperblock.blockCount * mBlockSize * mDevice.blocksize();
	free = mSuperblock.freeBlockCount * mBlockSize * mDevice.blocksize();
}
const char *ext2::name() const
{
	return "ext2";
}
bool ext2::mount(const char *device)
{
	if (mDevice.open(device, access::read | access::write) == false)return false;
	
	//Load superblock
	if (mDevice.block(&mSuperblock, 2, 2) == false)
	{
		cerr << "ext2: could not load the superblock" << endl;
		return false;
	}
	
	// Check the superblock magic field
	if (mSuperblock.magic != 0xEF53)
	{
		cerr << "ext2: invalid ext2 magic field" << endl;
		return false;
	}
	
	mBlockSize = mDevice.blockcount() / mSuperblock.blockCount;
	mLogicalBlockSize = mBlockSize * mDevice.blocksize();
	
	// Check
	//TODO: too many/too less blocks for that device?
	//TODO: Check the "state"
	//TODO: Check for errors
	
	// Write
	//TODO: The mount time
	//TODO: Write time
	//TODO: mount count
	//TODO: Write the "state"
	
	
	//Load group descriptors
	// Calculate the number of groups
	size_t groups = mSuperblock.blockCount / mSuperblock.blocksPerGroup;
	if ((mSuperblock.blockCount % mSuperblock.blocksPerGroup) != 0)++groups;
	
	// Calculate the number of blocks needed
	size_t groupBlocks = groups / mLogicalBlockSize;
	if ((groups % (mBlockSize * mDevice.blocksize())) != 0)++groupBlocks;
	
	// Create a buffer for blocks with group descriptors an fill it
	auto_array<uint8_t> groupBuffer(new uint8_t[groupBlocks * mDevice.blocksize()]);
	for (size_t i=0;i < groupBlocks;i++)
	{
		if (readBlock(groupBuffer.get(), 2 + i) == false)
		{
			cerr << "ext2: Failed to load the group descriptors" << endl;
			return false;
		}
	}
	
	// Save group descriptors
	groupDescriptor *desc = reinterpret_cast<groupDescriptor*>(groupBuffer.get());
	for (size_t i=0;i < groups;i++)
	{
		mGroupDescriptors.push_back(group(desc[i]));
	}
	
	mRootDirectory = new directory(0, 0, 2);
	if (fillDirectory(*mRootDirectory) == false)
	{
		cerr << "ext2: Failed to load the root directory" << endl;
		return false;
	}
	
	return true;
}
bool ext2::readBlock(void *buffer, size_t block)
{
	if (block >= mSuperblock.blockCount)return false;
	return mDevice.block(buffer, block * mBlockSize, mBlockSize);
}
bool ext2::writeBlock(const void *buffer, size_t block)
{
	/*if (block >= mSuperblock.blockCount)return false;
	size_t test = mDevice.write(buffer, mLogicalBlockSize, block * mBlockSize);
	if (test != mLogicalBlockSize)
	{
		cerr << "ext2: written " << dec << test << "byte" << endl;
		return false;
	}
	return true;*/
	cerr << "ext2::writeBlock()" << endl;
	return false;
}
uint32_t *ext2::inode_bitmap(group &Group)
{
	if (Group.inodeBitmap != 0)return Group.inodeBitmap;
	
	// Load the inode Bitmap
	uint8_t *tmpBuf = new uint8_t[mLogicalBlockSize];
	Group.inodeBitmap = new uint32_t[mSuperblock.inodesPerGroup / 32];
	size_t block = Group.descriptor.inodeBitmap;
	size_t blockCount = (mSuperblock.inodesPerGroup / 8) / mLogicalBlockSize;
	if (((mSuperblock.inodesPerGroup / 8) % mLogicalBlockSize) != 0)++blockCount;
	for (size_t i = 0;i < blockCount;i++)
	{
		if (readBlock(tmpBuf, block) == false)
		{
			cerr << "ext2: failed to load the inode bitmap" << endl;
			delete []Group.inodeBitmap;
			delete []tmpBuf;
			Group.inodeBitmap = 0;
			return 0;
		}
		++block;
		
		size_t cpysize = mLogicalBlockSize;
		if ((i + 1) == blockCount)cpysize = (mSuperblock.inodesPerGroup / 8) % mLogicalBlockSize;
		memcpy(	reinterpret_cast<char*>(Group.inodeBitmap) + i * mLogicalBlockSize,
				tmpBuf,
				cpysize);
	}
	delete []tmpBuf;
	return Group.inodeBitmap;
}
bool ext2::write_inode_bitmap(group &Group, size_t inodeId)
{
	//TODO: Update just the changed blocks
	uint8_t *tmpBuf = new uint8_t[mLogicalBlockSize];
	size_t block = Group.descriptor.inodeBitmap;
	size_t blockCount = (mSuperblock.inodesPerGroup / 8) / mLogicalBlockSize;
	if (((mSuperblock.inodesPerGroup / 8) % mLogicalBlockSize) != 0)++blockCount;
	for (size_t i = 0;i < blockCount;i++)
	{
		size_t cpysize = mLogicalBlockSize;
		if ((i + 1) == blockCount)cpysize = (mSuperblock.inodesPerGroup / 8) % mLogicalBlockSize;
		memcpy(	tmpBuf,
				reinterpret_cast<char*>(Group.inodeBitmap) + i * mLogicalBlockSize,
				cpysize);
		
		if (writeBlock(tmpBuf, block) == false)
		{
			cerr << "ext2: failed to save the inode bitmap" << endl;
			delete []tmpBuf;
			return false;
		}
		++block;
	}
	delete []tmpBuf;
	return true;
}
uint32_t *ext2::block_bitmap(group &Group)
{
	if (Group.blockBitmap != 0)return Group.blockBitmap;
	
	// Load the inode Bitmap
	uint8_t *tmpBuf = new uint8_t[mLogicalBlockSize];
	Group.blockBitmap = new uint32_t[mSuperblock.blocksPerGroup / 32];
	size_t block = Group.descriptor.blockBitmap;
	size_t blockCount = (mSuperblock.blocksPerGroup / 8) / mLogicalBlockSize;
	if (((mSuperblock.blocksPerGroup / 8) % mLogicalBlockSize) != 0)++blockCount;
	for (size_t i = 0;i < blockCount;i++)
	{
		if (readBlock(tmpBuf, block) == false)
		{
			cerr << "ext2: failed to load the block bitmap" << endl;
			delete []Group.blockBitmap;
			delete []tmpBuf;
			Group.blockBitmap = 0;
			return 0;
		}
		++block;
		
		size_t cpysize = mLogicalBlockSize;
		if ((i + 1) == blockCount)cpysize = (mSuperblock.blocksPerGroup / 8) % mLogicalBlockSize;
		memcpy(	reinterpret_cast<char*>(Group.blockBitmap) + i * mLogicalBlockSize,
				tmpBuf,
				cpysize);
	}
	delete []tmpBuf;
	return Group.blockBitmap;
}
bool ext2::write_block_bitmap(group &Group, size_t blockId)
{
	//TODO: Update just the changed blocks
	uint8_t *tmpBuf = new uint8_t[mLogicalBlockSize];
	size_t block = Group.descriptor.blockBitmap;
	size_t blockCount = (mSuperblock.blocksPerGroup / 8) / mLogicalBlockSize;
	if (((mSuperblock.blocksPerGroup / 8) % mLogicalBlockSize) != 0)++blockCount;
	for (size_t i = 0;i < blockCount;i++)
	{
		size_t cpysize = mLogicalBlockSize;
		if ((i + 1) == blockCount)cpysize = (mSuperblock.blocksPerGroup / 8) % mLogicalBlockSize;
		memcpy(	tmpBuf,
				reinterpret_cast<char*>(Group.blockBitmap) + i * mLogicalBlockSize,
				cpysize);
		
		if (writeBlock(tmpBuf, block) == false)
		{
			cerr << "ext2: failed to save the block bitmap" << endl;
			delete []tmpBuf;
			return false;
		}
		++block;
	}
	delete []tmpBuf;
	return true;
}
bool ext2::write_inode_table(group &Group, size_t inodeId)
{
	//TODO: Update just the changed blocks
	uint8_t *tmpBuf = new uint8_t[mLogicalBlockSize];
	size_t block = Group.descriptor.inodeTable;
	size_t blockCount = (mSuperblock.inodesPerGroup * sizeof(inode)) / mLogicalBlockSize;
	if (((mSuperblock.inodesPerGroup * sizeof(inode)) % mLogicalBlockSize) != 0)++blockCount;
	for (size_t i = 0;i < blockCount;i++)
	{
		size_t cpysize = mLogicalBlockSize;
		if ((i + 1) == blockCount)cpysize = (mSuperblock.inodesPerGroup * sizeof(inode)) % mLogicalBlockSize;
		memcpy(	tmpBuf,
				reinterpret_cast<char*>(Group.inodeTable) + i * mLogicalBlockSize,
				cpysize);
		
		if (writeBlock(tmpBuf, block) == false)
		{
			cerr << "ext2: failed to save the inode bitmap" << endl;
			delete []tmpBuf;
			return false;
		}
		++block;
	}
	delete []tmpBuf;
	return true;
}
ext2::inode *ext2::inode_table(group &Group)
{
	if (Group.inodeTable != 0)return Group.inodeTable;
	
	// Load the inode table
	uint8_t *tmpBuf = new uint8_t[mLogicalBlockSize];
	Group.inodeTable = new inode[mSuperblock.inodesPerGroup];
	size_t block = Group.descriptor.inodeTable;
	size_t blockCount = (mSuperblock.inodesPerGroup * sizeof(inode)) / mLogicalBlockSize;
	if (((mSuperblock.inodesPerGroup * sizeof(inode)) % mLogicalBlockSize) != 0)++blockCount;
	for (size_t i = 0;i < blockCount;i++)
	{
		if (readBlock(tmpBuf, block) == false)
		{
			cerr << "ext2: failed to load the inode table" << endl;
			delete []Group.inodeTable;
			delete []tmpBuf;
			Group.inodeTable = 0;
			return 0;
		}
		++block;
		
		size_t cpysize = mLogicalBlockSize;
		if ((i + 1) == blockCount)cpysize = (mSuperblock.inodesPerGroup * sizeof(inode)) % mLogicalBlockSize;
		memcpy(	reinterpret_cast<char*>(Group.inodeTable) + i * mLogicalBlockSize,
				tmpBuf,
				cpysize);
	}
	delete []tmpBuf;
	return Group.inodeTable;
}
ext2::inode *ext2::getInode(size_t inodeID)
{
	inodeID--;
	size_t blockGroup = inodeID / mSuperblock.inodesPerGroup;
	size_t inodeOffset = inodeID - (blockGroup * mSuperblock.inodesPerGroup);
	
	if (blockGroup >= mGroupDescriptors.size())return 0;
	group Group = mGroupDescriptors[blockGroup];
	
	inode *inodeTable = inode_table(Group);
	if (inodeTable == 0)return 0;
	return &inodeTable[inodeOffset];
}
size_t ext2::allocateInode()
{
	/* Go through the group descriptors and find a free inode */
	for (size_t i = 0;i < mGroupDescriptors.size();i++)
	{
		if (mGroupDescriptors[i].descriptor.freeInodeCount > 0)
		{
			uint32_t *inodeBitmap = inode_bitmap(mGroupDescriptors[i]);
			if (inodeBitmap == 0)return 0;
			
			// Find a free inode id
			for (size_t y = 0;y < (mSuperblock.inodesPerGroup / 32);y++)
				if (inodeBitmap[y] != 0xFFFFFFFF)
					for (size_t z = 0;z < 32;z++)
						if ((inodeBitmap[y] & (1 << z)) == 0)
						{
							size_t inodeId = (y * 32) + z;
							inodeBitmap[y] |= (1 << z);
							if (write_inode_bitmap(mGroupDescriptors[i], inodeId) == false)return 0;
							return inodeId;
						}
		}
	}
	return 0;
}
size_t ext2::readInode(void *buffer, inode *Inode, size_t start, size_t end)
{
	if (end > Inode->size)end = Inode->size;
	
	size_t startblock = start / mLogicalBlockSize;
	size_t endblock = end / mLogicalBlockSize;
	if ((end % mLogicalBlockSize) != 0)endblock++;
	
	size_t read = 0;
	uint8_t *tmpBuf = new uint8_t[mLogicalBlockSize];
	auto_array<uint32_t> indirectBlock;
	for (size_t i = 0;i < (endblock - startblock);i++)
	{
		size_t block;
		if ((startblock + i) < 12)block = Inode->block[i + startblock];
		else if ((startblock + i) < (12 + (mLogicalBlockSize / 4)))
		{
			// Load the indirect block
			if (indirectBlock.get() == 0)
			{
				indirectBlock = auto_array<uint32_t>(new uint32_t[mLogicalBlockSize / 4]);
				if (readBlock(indirectBlock.get(), Inode->indirectBlock) == false)break;
			}
			// Get the blocknumber from the indirect block
			block = indirectBlock.get()[i + startblock - 12];
		}
		else
		{
			cerr << "ext2: bi- & triindirect blocks not yet supported" << endl;
			delete []tmpBuf;
			return 0;
		}
		
		if (readBlock(tmpBuf, block) == false)break;
		
		if (i == 0)
		{
			size_t cpystart = start % mLogicalBlockSize;
			size_t cpysize = mLogicalBlockSize - cpystart;
			if (i == (endblock - startblock - 1))cpysize = end - start;
			memcpy(	reinterpret_cast<char*>(buffer),
					&tmpBuf[cpystart],
					cpysize);
			read += cpysize;
		}
		else if (i == (endblock - startblock - 1))
		{
			size_t cpysize = end % mLogicalBlockSize;
			memcpy(	reinterpret_cast<char*>(buffer) + (mLogicalBlockSize - (start % mLogicalBlockSize)) + (i - 1) * mLogicalBlockSize,
					tmpBuf,
					cpysize);
			read += cpysize;
		}
		else
		{
			memcpy(	reinterpret_cast<char*>(buffer) + (mLogicalBlockSize - (start % mLogicalBlockSize)) + (i - 1) * mLogicalBlockSize,
					tmpBuf,
					mLogicalBlockSize);
			read += mLogicalBlockSize;
		}
	}
	delete []tmpBuf;
	return read;
}
bool ext2::fillDirectory(directory &dir)
{
	if (dir.mLoaded == true)return true;
	
	//Read the inode from inodetable
	inode *Inode = getInode(dir.mInode);
	if(Inode == 0)return false;
	
	// calculate the number of blocks to read
	size_t blockcount = Inode->size / mLogicalBlockSize;
	if ((Inode->size % mLogicalBlockSize) != 0)blockcount++;
	
	// Read the inode data
	auto_array<char> data(new char[Inode->size]);
	size_t read = readInode(data.get(), Inode, 0, Inode->size);
	if (read != Inode->size)return false;
	
	// Parse the inode data
	directory_entry *entry = reinterpret_cast<directory_entry*>(data.get());
	while (true)
	{
		if (entry->inode != 0)
		{
			char name[256];
			strncpy(name,
					reinterpret_cast<char*>(reinterpret_cast<uintptr_t>(entry) + sizeof(directory_entry)),
					entry->name_length);
			name[entry->name_length] = '\0';
			if (strcmp(name, ".") != 0 &&
				strcmp(name, "..") != 0)
			{
				inode *curInode = getInode(entry->inode);
				size_t type = (curInode->mode >> 12) & 0x0F;
				if (type == 0x04)
				{
					dir.mFiles.push_back(new directory(&dir, name, entry->inode));
				}
				else if (type == 0x08)
				{
					dir.mFiles.push_back(new file(&dir, name, curInode->size, entry->inode));
				}
				else
				{
					cout << "ext2: unsupported file type 0x" << hex << type << " for file \"" << name << "\"" << endl;
				}
			}
		}
		
		entry = reinterpret_cast<directory_entry*>(reinterpret_cast<uintptr_t>(entry) + entry->length);
		if ((reinterpret_cast<uintptr_t>(entry) - reinterpret_cast<uintptr_t>(data.get())) >= Inode->size)break;
	}
	
	dir.mLoaded = true;
	return true;
}
filesystem::file *ext2::findFile(const char *name)
{
	return findFile(*mRootDirectory, name);
}
filesystem::file *ext2::findFile(directory &dir, const char *name)
{
   if (name == 0)return &dir;
	
	size_t length = 0;
	// Cut of the lowest directoy
	while (	name[length] != '/' &&
			name[length] != '\\' &&
			name[length] != '\0' &&
			name[length] != ' '){length++;}
	if (length == 0)return &dir;
	
	for(vector<filesystem::file*>::iterator i=dir.mFiles.begin();i != dir.mFiles.end();i++)
	{
		// Check if it is the searched filename
		if (i->name().length() == length &&
			strncmp(i->name().c_str(), name, length) == 0)
		{
			if (name[length] == '/' &&
				i->type() == lightOS::file::directory)
			{
				directory &newdir = static_cast<directory&>(**i);
				if (fillDirectory(newdir) == false)return 0;
				return findFile(newdir, &name[length+1]);
			}
			else if (	name[length] == '\0' ||
						name[length] == ' ')return *i;
			else return 0;
		}
	}
	return 0;
}
filesystem::file *ext2::createFile(	unsigned long port,
									const char *name,
									size_t type,
									size_t blocksize,
									size_t blockcount,
									size_t flags)
{
	if ((flags & attr::mask) != 0)
	{
		cerr << "ext2: invalid file flags (0x" << hex << flags << ")" << endl;
		return 0;
	}
	
	if (type != lightOS::file::directory &&
		type != lightOS::file::block)
	{
		cerr << "ext2: file type not supported" << endl;
		return 0;
	}
	
	/* Allocate Inode */
	size_t inodeId = allocateInode();
	if (inodeId == 0)return 0;
	
	/* Initialize the inode */
	inode *Inode = getInode(inodeId);
	memset(Inode, 0, sizeof(inode));
	if (type == lightOS::file::directory)Inode->mode = 0x4000;
	else if (type == lightOS::file::block)Inode->mode = 0x8000;
	Inode->mode |= 0x1FF;
	
	return 0;
}
filesystem::file *ext2::openFile(	unsigned long port,
									const char *name)
{
	filesystem::file *File = findFile(*mRootDirectory, name);
	if (File != 0 &&
		File->type() == lightOS::file::directory)
	{
		fillDirectory(static_cast<directory&>(*File));
	}
	return File;
}
