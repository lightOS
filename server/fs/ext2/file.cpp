/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include "ext2.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

using libkernel::message;

void ext2::file::read(	unsigned long port,
						size_t block,
						size_t size)
{
	ext2 &Ext2 = static_cast<ext2&>(filesystem::instance());
	ext2::inode *Inode = Ext2.getInode(mInode);
	libkernel::shared_memory shm(size);
	size_t result = Ext2.readInode(shm.address<void>(), Inode, block, block + size);
	
	if (result == size)
	{
		message reply(port, libkernel::message::fs_read_file, block, shm, libkernel::shared_memory::transfer_ownership);
		Port().send(reply);
	}
	else
	{
		message reply(port, libkernel::message::fs_read_file, block);
		Port().send(reply);
	}
}
void ext2::file::write(	unsigned long port,
						size_t block,
						libkernel::shared_memory &shm)
{
	cout << "ext2::file: writting not supported" << endl;
}
