/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SERVER_FILESYSTEM_HPP
#define SERVER_FILESYSTEM_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup filesystem filesystem */
/*@{*/

#include <cstdlib>
#include <vector>
#include <utility>
#include <lightOS/lightOS.hpp>

namespace server
{
	class filesystem
	{
		friend class file;
		public:
			class directory;
			class file
			{
				public:
					inline const std::string &name() const{return mName;}
					inline size_t type() const{return mType;}
					inline size_t blocksize() const{return mBlocksize;}
					virtual size_t blockcount() const;
					inline void blockcount(size_t count){mBlockcount = count;}
					inline filesystem &fs(){return filesystem::instance();}
					inline libkernel::message_port &Port(){return fs().mPort;}
					
					virtual void create(unsigned long port);
					virtual bool open(unsigned long port);
					virtual bool close(unsigned long port);
					virtual void read(	unsigned long port,
										size_t block,
										size_t size)=0;
					virtual void write(	unsigned long port,
										size_t block,
										libkernel::shared_memory &shm)=0;
					virtual bool set_file_size(	unsigned long port,
												size_t block);
					size_t get_flags() const{return mFlags;}
					size_t get_opener_count() const{return mOpener.size();}
					libkernel::port_id_t get_opener(size_t index) const{if (index >= mOpener.size())return 0;return mOpener[index];}
					template<class T>
					inline T *get_directory(){return static_cast<T*>(mDirectory);}
				protected:
					/*! The constructor */
					inline file(directory *dir,
								const char *name,
								size_t type,
								size_t blocksize,
								size_t blockcount,
								size_t flags = 0)
						: mDirectory(dir), mName(name), mType(type), mBlocksize(blocksize), mBlockcount(blockcount), mFlags(flags){}
					/*! The destructor */
					inline virtual ~file(){}
					
					directory *mDirectory;
					std::string mName;
					size_t mType;
					size_t mBlocksize;
					size_t mBlockcount;
					size_t mFlags;
					std::vector<libkernel::port_id_t> mOpener;
			};
			
			class directory : public file
			{
				friend class devfs;
				friend class pipefs;
				friend class gzipfs;
				friend class tmpfs;
				public:
					virtual size_t blockcount() const;
					virtual void read(	unsigned long port,
										size_t block,
										size_t size);
					virtual void write(	unsigned long port,
										size_t block,
										libkernel::shared_memory &shm);
				protected:
					/*! The constructor */
					inline directory(directory *dir, const char *name)
						: file(dir, name, lightOS::file::directory, 0, 0){}
					/*! The destructor */
					inline virtual ~directory(){}
					
					/*! The files */
					std::vector<filesystem::file*> mFiles;
			};
			
			/*! Get the filesystem instance */
			inline static filesystem &instance(){return mInst;}
			/*! Delete the filesystem instance */
			inline static void uninstance(){delete &mInst;}
			/*! Get the port */
			inline libkernel::message_port &port(){return mPort;}
			/*! Add an opened file */
			inline void addOpenedFile(	unsigned long port,
										file *File,
										unsigned long access)
			{
				mOpenedFiles.push_back(std::pair<unsigned long,std::pair<file*, unsigned long> >(port, std::pair<file*, unsigned long>(File, access)));
			}
			/*! Find an opened file */
			std::pair<file*, unsigned long> findOpenedFile(unsigned long port);
			/*! Remove an opened file */
			void removeOpenedFile(unsigned long port);
			
			/*! Get information about the filesystem */
			virtual void info(	size_t &size,
								size_t &free) const=0;
			/*! Get the filesystem name */
			virtual const char *name() const=0;
			/*! Mount the filesystem */
			virtual bool mount(const char *device)=0;
			
			virtual file *findFile(const char *name)=0;
			virtual file *createFile(	unsigned long port,
										const char *name,
										size_t type,
										size_t blocksize,
										size_t blockcount,
										size_t flags)=0;
			virtual file *openFile(	unsigned long port,
									const char *name)=0;
		protected:
			/*! The constructor */
			inline filesystem(){};
			/*! The destructor */
			inline virtual ~filesystem(){}
			/*! The port the filesystem is running on */
			libkernel::message_port mPort;
			/*! The opened file list */
			std::vector<std::pair<unsigned long,std::pair<file*, unsigned long> > > mOpenedFiles;
		private:
			/*! The filesystem instance */
			static filesystem &mInst;
	};
}

/*@}*/
/*@}*/

#endif
