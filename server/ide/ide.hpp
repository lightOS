/*
lightOS server
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_SERVER_IDE_HPP
#define LIGHTOS_SERVER_IDE_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup ide ide */
/*@{*/

#include <cstddef>
#include <cstdint>
#include <lightOS/lightOS.hpp>

namespace server
{
  namespace ide
  {
    class controller
    {
      public:
        /*! Type of a device */
        enum type
        {
          none,
          unknown,
          ata,
          atapi
        };

        static bool is_present(uint16_t command_block_port,
                               uint16_t control_block_port);
        static bool has_device(uint16_t command_block_port,
                               uint16_t control_block_port);

        controller(size_t iController,
                   uint16_t command_block_port,
                   uint16_t control_block_port,
                   uint8_t irq);
        bool init_devices();

        void loop();

      private:
        struct device_identification
        {
          // TODO: change into (u)intX_t types
          unsigned short    config;
          unsigned short    trackCount;
          //! ata/atapi 5+
          unsigned short    specificConfig;
          unsigned short    headCount;
          unsigned short    reserved1;
          unsigned short    reserved2;
          unsigned short    sectorPerTrack;
          unsigned short    reserved3[2];
          unsigned short    reserved5;
          char        serialNumber[20];
          unsigned short    reserved6;
          unsigned short    reserved7;
          unsigned short    TODO1;
          char        revision[8];
          char        name[40];
          unsigned short    TODO2;
          unsigned short    reserved8;
          unsigned short    caps;
          //! ata/atapi 5+
          unsigned short    caps2;
          unsigned short    reserved9;
          unsigned short    reserved10;
          unsigned short    TODO3;
          unsigned short    currentTrackCount;
          unsigned short    currentHeadCount;
          unsigned short    currentSectorPerTrack;
          unsigned int    capacity;
          unsigned short    TODO4;
          unsigned int    lbaSectorCount;
          unsigned short    reserved11;
          unsigned short    multiDmaMode;
          unsigned short    advancedPio;
          unsigned short    TODO5;
          unsigned short    TODO6;
          unsigned short    TODO7;
          unsigned short    TODO8;
          unsigned short    reserved12[2];
          unsigned short    reserved13[4];
          //! ata/atapi 4+
          unsigned short    maxQueue;
          unsigned short    reserved14[4];
          unsigned short    ataMajor;
          unsigned short    ataMinor;
          unsigned short    supportedFeatures[3];
          //! ata/atapi 4+
          unsigned short    enabledFeatures[3];
          //! ata/atapi 4+
          unsigned short    ultraDma;
          unsigned short    TODO9;
          unsigned short    TODO10;
          //! ata/atapi 4+
          unsigned short    advancedPM;
          unsigned short    TODO11;
          unsigned short    TODO12;
          unsigned short    TODO13;
          unsigned short    reserved15[5];
          unsigned int    lba48SectorCount[2];
          unsigned short    reserved16[23];
          //! ata/atapi 4+
          unsigned short    mediaChange;
          unsigned short    reserved17[32];
          unsigned short    TODO14;
          unsigned short    reserved18[94];
          //! ata/atapi 5+
          unsigned short    integrity;
        } __attribute__((packed));

        static_assert(sizeof(device_identification) == 512, "server::ide::controller::device_identification has invalid size");

        static bool check_device_presence(uint16_t command_block_port,
                                          uint16_t control_block_port,
                                          bool master_dev);

        void select(bool master_dev);
        bool wait_status(uint8_t mask, uint8_t state);
        bool wait_irq();
        uint8_t read_errors();
        bool software_reset(bool master_dev);

        bool identify_device(bool master_dev,
                             device_identification &id);
        bool identify_packet_device(bool master_dev,
                                    device_identification &id);

        bool write_command(bool master_dev,
                           uint8_t command,
                           uint8_t sector_count,
                           uint64_t sector_number,
                           uint8_t flags);
        bool command_no_data(bool master_dev,
                             uint8_t command,
                             uint8_t sector_count,
                             uint64_t sector_number,
                             uint8_t flags);
        bool command_pio_in(bool master_dev,
                            uint8_t command,
                            uint8_t sector_count,
                            uint64_t sector_number,
                            uint8_t flags,
                            void *data);

        /*! I/O port offsets */
        enum
        {
          /* Command block */
          port_data                    = 0,
          port_error                   = 1,
          port_sector_count            = 2,
          port_lba0                    = 3,
          port_lba1                    = 4,
          port_lba2                    = 5,
          port_device_lba3             = 6,
          port_status                  = 7,
          port_command                 = 7,

          port_atapi_feature           = 1,
          port_atapi_interrupt_reason  = 2,
          port_atapi_bytecount0        = 4,
          port_atapi_bytecount1        = 5,

          /* Control Block */
          port_alternative_status      = 0,
          port_control                 = 0
        };

        /*! Bits of the device control register */
        enum
        {
          IEN                          = 0x02,
          SRST                         = 0x04,
          HOB                          = 0x08
        };

        /*! Bits in the status register */
        enum
        {
          ERR_CHK                      = 0x01,
          DRQ                          = 0x08,
          DF_SE                        = 0x20,
          DRDY                         = 0x40,
          BSY                          = 0x80
        };

        /*! The controller number */
        size_t miController;
        /*! Command block base port */
        uint16_t mCommandBase;
        /*! Control block base port */
        uint16_t mControlBase;
        /*! the irq number */
        uint8_t mIrq;
        /*! the interrupt vector */
        uint8_t mIntVector;
        /*! The port */
        libkernel::message_port mPort;
        /*! Device type */
        type mDevice[2];
    };
    
      /*! (alternative) status register */
      /*enum
      {
        status_error        = 0x01,
        status_data_ready     = 0x08,
        status_ready        = 0x40,
        status_busy         = 0x80,
      };*/
      
      /*! atapi interrupt reason */
      /*enum
      {
        interrupt_reason_cd     = 0x01,
        interrupt_reason_io     = 0x02,
      };
      */
      /*! timeouts */
      /*enum
      {
        timeout_busy        = 0x100000,
        timeout_ready       = 0x100000,
        timeout_data_ready      = 0x100000,
        timeout_interrupt     = 0x100,
      };*/
      
      /*! ATA commands */
      /*enum command
      {
        read_sector       = 0x21,
        identify        = 0xEC,
        packet          = 0xA0,
        packet_identify     = 0xA1,
      };*/
      
      /*! Wait until data ready bit of a controller is set and read
       * the data */
      //bool getData( size_t device,
      //        unsigned short &buffer);
      /*! Wait until data ready bit of a controller is set and send
       * the data */
      //bool sendData(  size_t device,
      //        unsigned short buffer);
      /*! Execute an ATA PIO In command */
      //bool executePIO(size_t device,
      //        command cmd,
      //        size_t count,
      //        size_t lba,
      //        size_t size,
      //        void *buffer,
      //        bool in);
      /*! Execute a ATAPI PACKET command */
      //bool executePACKET( size_t device,
      //          size_t packetsize,
      //          void *packet,
      //          size_t size,
      //          void *buffer,
      //          bool in);
      
      /*! Device type */
      //type mDevice[4];
  }
}

/*@}*/
/*@}*/

#endif
