/*
lightOS server
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libarch/ioport.hpp>
#include <lightOS/lightOS.hpp>
#include <iostream>
#include "ide.hpp"
using namespace lightOS;
using namespace server;
using namespace ide;
using namespace std;

using libkernel::message;

#define DEVICE_ERROR(x) cerr << "ide[" << miController << ":" << (master_dev ? "0" : "1") << "]: " << x << endl;

#define CMD_NO_SECTOR_NUMBER 0
#define CMD_SECTOR_NUMBER    1
#define CMD_NO_SECTOR_COUNT  0
#define CMD_SECTOR_COUNT     2

bool controller::is_present(uint16_t command_block_port,
                            uint16_t control_block_port)
{
  // Select the master device
  libarch::out8(command_block_port + port_device_lba3, 0xA0);

  // TODO: Delay

  // Floating Bus
  if (libarch::in8(command_block_port + port_status) != 0xFF)
    return true;

  // Select the master device
  libarch::out8(command_block_port + port_device_lba3, 0xA0 | (1 << 4));

  // TODO: Delay

  // Floating Bus
  if (libarch::in8(command_block_port + port_status) != 0xFF)
    return true;
  return false;
}

bool controller::has_device(uint16_t command_block_port,
                            uint16_t control_block_port)
{
  return check_device_presence(command_block_port, control_block_port, true) ||
         check_device_presence(command_block_port, control_block_port, false);
}

controller::controller(size_t iController,
                       uint16_t command_block_port,
                       uint16_t control_block_port,
                       uint8_t irq)
  : miController(iController), mCommandBase(command_block_port), mControlBase(control_block_port), mIrq(irq)
{
  // Request the IRQ line
  mIntVector = mPort.register_isa_irq(mIrq, 0);

  // Enable interrupts
  libarch::out8(mControlBase + port_control, 0x00);

  // Probe for a master device
  if (check_device_presence(mCommandBase,
                            mControlBase,
                            true) == true)
    mDevice[0] = unknown;
  else
    mDevice[0] = none;

  // Probe for a slave device
  if (check_device_presence(mCommandBase,
                            mControlBase,
                            false) == true)
    mDevice[1] = unknown;
  else
    mDevice[1] = none;
}

bool controller::init_devices()
{
  // TODO Soft reset the devices
  /*if (software_reset(true) == false)
  {
    cerr << "ide[" << miController << ":0]: failed to reset the device" << endl;
    mDevice[0] = none;
  }*/
  /*if (software_reset(false) == false)
  {
    cerr << "ide[" << miController << ":1]: failed to reset the device" << endl;
    mDevice[1] = none;
  }*/

  // Execute a NOP-Command
  // TODO: Remove

  device_identification did;
  if (mDevice[0] == unknown)
    if (identify_device(true, did) == false)
      mDevice[0] = none;
  if (mDevice[1] == unknown)
    if (identify_device(false, did) == false)
      mDevice[1] = none;
  return true;
}

void controller::loop()
{
}

bool controller::check_device_presence(uint16_t command_block_port,
                                       uint16_t control_block_port,
                                       bool master_dev)
{
  // Select the device
  libarch::out8(command_block_port + port_device_lba3, 0xA0 | ((master_dev) ? 0 : (1 << 4)));

  // TODO: Delay

  // Put some values in the sector-count & the LBA0 register
  libarch::out8(command_block_port + port_sector_count, 0x55);
  libarch::out8(command_block_port + port_lba0, 0xAA);

  // Read the values back
  uint8_t sectorCount = libarch::in8(command_block_port + port_sector_count);
  uint8_t sector = libarch::in8(command_block_port + port_lba0);

  // Check for inconsistencies
  if (sectorCount != 0x55 || sector != 0xAA)
    return false;
  return true;
}

// TODO: currently unused

void controller::select(bool master_dev)
{
  // Set/Reset the DEV bit in the Device I/O register
  libarch::out8(mCommandBase + port_device_lba3, 0xA0 | (master_dev ? 0x00 : 0x10));
}

bool controller::wait_status(uint8_t mask, uint8_t state)
{
  while ((libarch::in8(mCommandBase + port_status) & mask) != state)
    ;
  return true;
}

bool controller::wait_irq()
{
  // TODO: multiple wait messages not yet supported by the kernel
  // Register timer
  mPort.register_event(libkernel::event::timer, 500);
  message irq(libkernel::message_port::kernel, libkernel::message::event, libkernel::event::irq, mIntVector);
  message msg(libkernel::message_port::kernel, libkernel::message::event, libkernel::event::timer);

  // Wait for timer or irq
  mPort.wait(msg, irq);
  cout << msg.port() << ", " << msg.param1() << ", " << msg.param2() << ", " << msg.param3() << endl;

  // timer elapsed first
  if (msg.param1() == libkernel::event::timer)
    return false;

  // irq
  mPort.unregister_event(libkernel::event::timer);
  mPort.acknoledge_irq(mIntVector);
  return true;
}

uint8_t controller::read_errors()
{
  if ((libarch::in8(mCommandBase + port_status) & ERR_CHK) == 0)return 0;
  return libarch::in8(mCommandBase + port_error);
}

// TODO: Does not work on other controllers than ide0
bool controller::software_reset(bool master_dev)
{
  select(master_dev);
  
  // Set the SRST bit in the device control register
  libarch::out8(mControlBase + port_control, SRST);
  
  // Wait for at least 5µs
  // NOTE: We wait for 1ms
  wait(1);
  
  // Clear SRST bit in the device control register
  libarch::out8(mControlBase + port_control, 0x00);
  
  // Wait for 2ms
  wait(2);
  
  // Wait for BSY = 0
  if (wait_status(BSY, 0) == false)
    return false;
  
  // Check the error register
  if ((libarch::in8(mControlBase + port_status) & ERR_CHK) == ERR_CHK)
  {
    DEVICE_ERROR("error register = 0x" << hex << static_cast<size_t>(libarch::in8(mControlBase + port_error)));
    return false;
  }
  
  return true;
}

bool controller::identify_device(bool master_dev,
                                 device_identification &id)
{
  return command_pio_in(master_dev,
                        0xEC,
                        0,
                        0,
                        CMD_NO_SECTOR_COUNT | CMD_NO_SECTOR_NUMBER,
                        reinterpret_cast<void*>(&id));
}

bool controller::identify_packet_device(bool master_dev,
                                        device_identification &id)
{
  return command_pio_in(master_dev,
                        0xA1,
                        0,
                        0,
                        CMD_NO_SECTOR_COUNT | CMD_NO_SECTOR_NUMBER,
                        reinterpret_cast<void*>(&id));
}


bool controller::write_command(bool master_dev,
                               uint8_t command,
                               uint8_t sector_count,
                               uint64_t sector_number,
                               uint8_t flags)
{
  // Wait for BSY = 0
  if (wait_status(BSY, 0) == false)
  {
    DEVICE_ERROR("device busy");
    return false;
  }

  // Select the device
  select(master_dev);

  // Wait 400ns
  // NOTE: We wait for 1ms
  wait(1);

  // Write sector count/number & device/head register
  if ((flags & CMD_SECTOR_NUMBER) == CMD_SECTOR_NUMBER)
  {
    libarch::out8(mCommandBase + port_lba0, sector_number & 0xFF);
    libarch::out8(mCommandBase + port_lba1, (sector_number >> 8) & 0xFF);
    libarch::out8(mCommandBase + port_lba2, (sector_number >> 16) & 0xFF);
  }
  if ((flags & CMD_SECTOR_COUNT) == CMD_SECTOR_COUNT)
    libarch::out8(mCommandBase + port_sector_count, sector_count);
  if ((flags & CMD_SECTOR_NUMBER) == CMD_SECTOR_NUMBER)
    libarch::out8(mCommandBase + port_device_lba3, 0xE0 | (master_dev ? 0x00 : 0x10) | ((sector_number >> 24) & 0x0F));

  // Write the command code
  libarch::out8(mCommandBase + port_command, command);
  return true;
}

// TODO Does not, at least not with NOP on qemu
bool controller::command_no_data(bool master_dev,
                                 uint8_t command,
                                 uint8_t sector_count,
                                 uint64_t sector_number,
                                 uint8_t flags)
{
  if (write_command(master_dev,
                    command,
                    sector_count,
                    sector_number,
                    flags) == false)
    return false;

  // Wait for interrupt
  if (wait_irq() == false)
  {
    DEVICE_ERROR("no interrupt");
    return false;
  }

  // BSY = 0?
  if (wait_status(BSY, 0) == false)
  {
    DEVICE_ERROR("device still busy");
    return false;
  }

  // Any errors?
  uint8_t errCode = read_errors();
  if (errCode != 0)
  {
    DEVICE_ERROR("error register = 0x" << static_cast<size_t>(errCode));
    return false;
  }

  return true;
}

bool controller::command_pio_in(bool master_dev,
                                uint8_t command,
                                uint8_t sector_count,
                                uint64_t sector_number,
                                uint8_t flags,
                                void *data)
{
  if (write_command(master_dev,
                    command,
                    sector_count,
                    sector_number,
                    flags) == false)
    return false;

  cout << "HERE" << endl;
  // Wait for interrupt
  if (wait_irq() == false)
  {
    DEVICE_ERROR("no interrupt");
    return false;
  }
  cout << "THERE" << endl;

  // BSY = 0?
  if (wait_status(BSY, 0) == false)
  {
    DEVICE_ERROR("device still busy");
    return false;
  }

  // Any errors?
  uint8_t errCode = read_errors();
  if (errCode != 0)
  {
    DEVICE_ERROR("error register = 0x" << static_cast<size_t>(errCode));
    return false;
  }

  return true;
}

// TODO: command_PIO_in() chapter 11.5
// TODO: command_PIO_out() chapter 11.6
// TODO: command_packet() chapter 11.8
// TODO: identify_device() chapter 6.17
// TODO: identify_packet_decice() chapter 6.18
// TODO: ata_read_sector() chapter 6.36
  // TODO: add LBA48 extension
// TODO: ata_write_sector() sector 6.68
  // TODO: add LBA48 extension

/*ide::ide( unsigned short commandBase0,
      unsigned short commandBase1,
      unsigned short controlBase0,
      unsigned short controlBase1,
      unsigned char irq0,
      unsigned char irq1)
{
  for (size_t i = 0;i < 4;i++)
  {
    if (mDevice[i] == unknown)
    {
      size_t ctrl = i / 2;
      unsigned short port = mCommandBase[ctrl];
      unsigned short port2 = mControlBase[ctrl];
      
      // Software reset
      // TODO: some select_device function
      if ((i % 2) == 0)out8(port + reg_device_lba3, 0);
      else out8(port + reg_device_lba3, 1 << 4);
      out8(port2 + reg_control, 0x0E);
      //TODO: wait 400ns
      out8(port2 + reg_control, 0x08);
      //TODO: wait 400ns
      
      if (waitBusy(i, false) == false)continue;
      
      // No device present (master)?
      // TODO: select_device again
      if ((i % 2) == 0)out8(port + reg_device_lba3, 0);
      else out8(port + reg_device_lba3, 1 << 4);
      uint8_t sectorCount = in8(port + reg_sector_count);
      uint8_t sector = in8(port + reg_lba0);
      if (sectorCount == 0x01 && sector == 0x01)
      {
        unsigned char lba1 = in8(port + reg_lba1);
        unsigned char lba2 = in8(port + reg_lba2);
        unsigned char stat = in8(port + reg_status);
        
        if (lba1 == 0x14 && lba2 == 0xEB)
        {
          cout << "ide[" << i << "]: atapi" << endl;
          mDevice[i] = atapi;
          executePIO(i, packet_identify, 0, 0, 512, reinterpret_cast<void*>(&mDeviceId[i]), true);
          cout << "ide[" << i << "]: 0x" << hex << mDeviceId[i].atapi.config << endl;
          
          uint8_t Packet[12];
          uint8_t buffer[96];
          Packet[0] = 0x12;
          Packet[1] = 0;
          Packet[2] = 0;
          Packet[3] = 0;
          Packet[4] = 0;
          Packet[5] = 0;
          
          executePACKET(i, 12, Packet, 96, buffer, true);
        }
        else if (lba1 == 0 && lba2 == 0 && stat != 0)
        {
          cout << "ide[" << i << "]: ata" << endl;
          mDevice[i] = ata;
          executePIO(i, identify, 0, 0, 512, reinterpret_cast<void*>(&mDeviceId[i]), true);
        }
      }
    }
  }
}
bool ide::getData(size_t device, unsigned short &buffer)
{
  for (size_t i=0;i < timeout_data_ready;i++)
  {
    if ((in8(mControlBase[device / 2] + reg_alternative_status) & status_data_ready) == status_data_ready)
    {
      buffer = in16(mCommandBase[device / 2] + reg_data);
      return true;
    }
  }
  return false;
}
bool ide::sendData( size_t device,
          unsigned short buffer)
{
  for (size_t i=0;i < timeout_data_ready;i++)
  {
    if ((in8(mControlBase[device / 2] + reg_alternative_status) & status_data_ready) == status_data_ready)
    {
      out16(mCommandBase[device / 2] + reg_data, buffer);
      return true;
    }
  }
  return false;
}
bool ide::executePIO( size_t device,
            command cmd,
            size_t count,
            size_t lba,
            size_t size,
            void *buffer,
            bool in)
{
  //TODO: PIO out
  size_t ctrl = device / 2;
  unsigned short port = mCommandBase[ctrl];
  //unsigned short port2 = mControlBase[ctrl];
  
  // BSY = 0?
  if (waitBusy(device, false) == false)
  {
    cout << "ide[" << device << "]: device busy" << endl;
    return false;
  }
  
  // Write device/head register (DEV bit only)
  out8(port + reg_device_lba3, (device % 2) << 4);
  
  //TODO: wait 400ns
  
  // BSY = 0 && DRY = 1?
  if ((waitBusy(device, false) || waitReady(device, true)) == false)
  {
    cout << "ide[" << device << "]: device not ready" << endl;
    return false;
  }
  
  // Write sector count/number & device/head register
  out8(port + reg_lba0, lba & 0xFF);
  out8(port + reg_lba1, (lba >> 8) & 0xFF);
  out8(port + reg_lba2, (lba >> 16) & 0xFF);
  out8(port + reg_sector_count, count);
  out8(port + reg_device_lba3, 0xE0 | ((device % 2) << 4) | ((lba >> 24) & 0x0F));
  
  // Write the command code
  out8(port + reg_command, cmd);
  
  // Wait for interrupt
  message irq(port::kernel, message::event, event::irq, mIrq[ctrl]);
  mPort[ctrl].wait(irq);
  
  // Read status register (Negate INTRQ)
  if ((in8(port + reg_status) & status_error) == status_error)
  {
    cout << "ide[" << device << "]: error 0x" << hex << (unsigned int)in8(port + reg_error) << endl;
    // TODO: Inspect the errorcode
    return false;
  }
  
  // Read the data (only if PIO in)
  if (in)
  {
    size_t i  = 0;
    for (;i < (size / 2);i++)
    {
      unsigned short data;
      if (getData(device, data) == false)
      {
        cout << "ide[" << device << "]: could not get the data (" << i << ")" << endl;
        return false;
      }
      reinterpret_cast<unsigned short*>(buffer)[i] = data;
    }
  }
  return true;
}
bool ide::executePACKET(size_t device,
            size_t packetsize,
            void *packet,
            size_t size,
            void *buffer,
            bool in)
{
  //TODO: PIO out
  size_t ctrl = device / 2;
  unsigned short port = mCommandBase[ctrl];
  
  // BSY = 0?
  if (waitBusy(device, false) == false)return false;
  
  // Write device/head register (DEV bit only)
  out8(port + reg_device_lba3, (device % 2) << 4);
  
  //TODO: wait 400ns
  
  // BSY = 0 && DRY = 1?
  if ((waitBusy(device, false) || waitReady(device, true)) == false)return false;
  
  // Write sector count/number & device/head register
  out8(port + reg_atapi_feature, 0); // Disable DMA & OVERLAP
  out8(port + reg_atapi_bytecount0, size & 0xFF);
  out8(port + reg_atapi_bytecount1, (size >> 8) & 0xFF);
  out8(port + reg_device_lba3, ((device % 2) << 4));
  
  // Write the command code
  out8(port + reg_command, ide::packet);
  
  // Wait for irq
  cout << "waiting" << endl;
  message irq(port::kernel, message::event, event::irq, mIrq[ctrl]);
  mPort[ctrl].wait(irq);
  
  cout << "first irq fired" << endl;
  
  // Negate INTRQ
  in8(port + reg_status);
  
  // Send Packet
  for (size_t i=0;i < (packetsize / 2);i++)
  {
    unsigned short data;
    data = reinterpret_cast<unsigned short*>(packet)[i];
    if (sendData(device, data) == false)return false;
  }
  
  //TODO: wait 400ns
  
  if (size != 0)
  {
    // Wait for irq
    mPort[ctrl].wait(irq);
    cout << "second irq fired" << endl;
    
    // Negate INTRQ
    in8(port + reg_status);
    
    if (in)
    {
      //TODO: Read the number of bytes from the ATAPI register?
      for (size_t i=0;i < (size / 2);i++)
      {
        unsigned short data;
        if (getData(device, data) == false)return false;
        reinterpret_cast<unsigned short*>(buffer)[i] = data;
      }
    }
    else
    {
      //TODO: write data
    }
  }
  
  // Wait for irq
  mPort[ctrl].wait(irq);
  cout << "third irq fired" << endl;
  
  // Read status register (Negate INTRQ)
  if ((in8(port + reg_status) & status_error) == status_error)
  {
    //cout << "ide[" << device << "]: error 0x" << hex << (unsigned int)in8(mControlBase[ctrl] + reg_error) << endl;
    // TODO: Inspect the errorcode
    return false;
  }
  
  return true;
}
bool ide::read( size_t device,
        size_t blocknumber,
        size_t blockcount,
        void *buffer)
{
  if (mDevice[device] == ata)
  {
    return executePIO(device, read_sector, blockcount, blocknumber, blockcount * 512, buffer, true);
  }
  cerr << "ide: read not supported for this device type" << endl;
  return false;
}
ide::~ide()
{
}*/
