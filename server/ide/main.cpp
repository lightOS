/*
lightOS server
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include "ide.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

#define IDE_MAX_CONTROLLER_COUNT 4

size_t ControllerCount = 0;
ide::controller *Controller[IDE_MAX_CONTROLLER_COUNT];

/*! Default ISA IDE Controller IRQ numbers */
uint8_t isaIrq[] = {14, 15, 11, 10};
/*! Default ISA IDE Controller command block I/O Ports */
uint16_t isaCommandBlock[] = {0x1F0, 0x170, 0x1E8, 0x168};
/*! Default ISA IDE Controller control block I/O Ports*/
uint16_t isaControlBlock[] = {0x3F6, 0x376, 0x3EE, 0x36E};

static void ideController(size_t device)
{
  // Grant access to the I/O Ports
  // TODO FIXME HACK
  allocate_io_port_range(0, 0);

  // IDE Controller main loop
  Controller[device]->loop();
}

int main()
{
  // Grant access to the I/O Ports
  // TODO FIXME HACK
  allocate_io_port_range(0, 0);

  // TODO: Search a PCI IDE Controller Device and use it

  // Do the ISA IDE Controller probing
  for (size_t i = 0;i < IDE_MAX_CONTROLLER_COUNT;i++)
  {
    // Is the ISA IDE Controller present?
    bool bPresent = ide::controller::is_present(isaCommandBlock[i], isaControlBlock[i]) &&
                    ide::controller::has_device(isaCommandBlock[i], isaControlBlock[i]);
    if (bPresent == true)
    {
      // Create the IDE Controller class
      Controller[ControllerCount] = new ide::controller(i,
                                                        isaCommandBlock[i],
                                                        isaControlBlock[i],
                                                        isaIrq[i]);

      cout << "init_devices()" << endl;
      if (Controller[ControllerCount]->init_devices() == false)
      {
        cerr << "ide[" << i << "]: failed to initialize the devices" << endl;
        return -1;
      }
      cout << "init_devices() done" << endl;

      ++ControllerCount;
    }
  }

  // Any IDE Controllers present?
  if (ControllerCount == 0)
  {
    cerr << "ide: no IDE controller present" << endl;
    return -1;
  }

  cout << "DONE" << endl;

  // Create a thread for all IDE Controller (except the last one)
  for (size_t i = 0;i < (ControllerCount - 1);i++)
    create_thread<size_t>(ideController, i);

  // Last IDE Controller
  ideController(ControllerCount);

  return 0;
}
