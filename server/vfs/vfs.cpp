/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <iterator>
#include <algorithm>
#include <lightOS/lightOS.hpp>
using namespace lightOS;
using namespace std;

using libkernel::message;

struct pending_mount
{
  pending_mount(libkernel::port_id_t _process_portid,
                libkernel::port_id_t _fs_portid,
                const char* _dev_name,
                const char* _path,
                const char* _fs_name);

  libkernel::port_id_t process_port;
  libkernel::port_id_t fs_port;
  std::string dev_name;
  std::string path;
  std::string fs_name;
};

pending_mount::pending_mount(libkernel::port_id_t _process_port,
                            libkernel::port_id_t _fs_port,
                            const char* _dev_name,
                            const char* _path,
                            const char* _fs_name)
  : process_port(_process_port), fs_port(_fs_port),
    dev_name(_dev_name), path(_path), fs_name(_fs_name)
{
}

struct mountpoint
{
  mountpoint(const pending_mount& x);

  libkernel::port_id_t fs_port;
  std::string dev_name;
  std::string path;
  std::string fs_name;
};

mountpoint::mountpoint(const pending_mount& x)
  : fs_port(x.fs_port), dev_name(x.dev_name), path(x.path), fs_name(x.fs_name)
{
}


// port of the filesystem, name of the filesystem
vector<pair<unsigned long,string*> > registeredFilesystems;
// port of the waiter, name of the filesystem
vector<pair<unsigned long,string*> > pendingRegistrationWaits;
// port of the mounting process, port of the filesystem, mountpoint path
vector<pending_mount> pendingMounts;
// port of the filesystem, mountpoint path
vector<mountpoint> mountedFilesystems;

// Working directories
vector<pair<unsigned long,string> > workingDirectory;
// Console filenames
vector<pair<processId,string> > consoleName;

// Get the process's current working directory
string getCwd(unsigned long pid)
{
  string cwd("");
  for (size_t i=0;i < workingDirectory.size();i++)
    if (workingDirectory[i].first == pid)
      cwd = workingDirectory[i].second;
  return cwd;
}
int main(int argc, char *argv[])
{
  libkernel::message_port Port(libkernel::message_port::vfs, true);
  if (!Port)
  {
    cerr << "vfs: vfs already started" << endl;
    return -1;
  }
  
  message msg;
  while (Port.get(msg))
  {
    libkernel::shared_memory shm = msg.get_shared_memory();
    switch (msg.type())
    {
      case libkernel::message::ping:
      {
        message reply(msg.port(), libkernel::message::pong);
        Port.send(reply);
      }break;
      case libkernel::message::vfs_register_fs:
      {
        if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
        {
          register_fs_info *info = shm.address<register_fs_info>();
          
          // Register the filesystem
          info->name[MAX_FILESYSTEM_NAME] = '\0';
          string *name = new string(info->name);
          registeredFilesystems.push_back(pair<unsigned long,string*>(msg.port(), name));
          
          for (size_t i=0;i < pendingRegistrationWaits.size();i++)
            if (strcmp(info->name, pendingRegistrationWaits[i].second->c_str()) == 0)
            {
              message replywait(pendingRegistrationWaits[i].first, libkernel::message::vfs_wait_fs_registration);
              Port.send(replywait);
              break;
            }
          
          // Send reply
          message reply(msg.port(), libkernel::message::vfs_register_fs, static_cast<unsigned long>(true));
          Port.send(reply);
        }
      }break;
      case libkernel::message::vfs_wait_fs_registration:
      {
        if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
        {
          register_fs_info *info = shm.address<register_fs_info>();
          info->name[MAX_FILESYSTEM_NAME] = '\0';
          
          // Is the fs already registered
          bool found = false;
          for (size_t i=0;i < registeredFilesystems.size();i++)
            if (strcmp(info->name, registeredFilesystems[i].second->c_str()) == 0)
            {
              message reply(msg.port(), libkernel::message::vfs_wait_fs_registration);
              Port.send(reply);
              found = true;
              break;
            }
          
          // Add to list
          if (!found)
            pendingRegistrationWaits.push_back(pair<unsigned long,string*>(msg.port(), new string(info->name)));
        }
      }break;
      case libkernel::message::vfs_mount:
      {
        if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
        {
          mount_fs_info *info = shm.address<mount_fs_info>();
          info->name[MAX_PATH] = '\0';
          info->filesystem[MAX_FILESYSTEM_NAME] = '\0';
          info->device[MAX_PATH] = '\0';
          
          unsigned long port = 0;
          // Find the filesystem
          for (size_t i=0;i < registeredFilesystems.size();i++)
            if (strcmp(info->filesystem, registeredFilesystems[i].second->c_str()) == 0)
            {
              port = registeredFilesystems[i].first;
            }
          
          if (port == 0)
          {
            message reply(msg.port(), libkernel::message::vfs_mount, static_cast<unsigned long>(false));
            Port.send(reply);
          }
          else
          {
            pendingMounts.push_back(pending_mount(msg.port(),
                                                  port,
                                                  info->device,
                                                  info->name,
                                                  info->filesystem));
            message request(port, libkernel::message::vfs_mount, 0, shm, libkernel::shared_memory::transfer_ownership);
            Port.send(request);
          }
        }
        else if (msg.attribute() == libkernel::shared_memory::none)
        {
          // Is it a reply to a pending mount?
          auto i = pendingMounts.begin();
          auto end = pendingMounts.end();
          for (;i != end;++i)
            if ((*i).fs_port == msg.port())
            {
              if (static_cast<bool>(msg.param1()) == true)
              {
                mountedFilesystems.push_back(*i);
              }
              message reply((*i).process_port, libkernel::message::vfs_mount, msg.param1());
              Port.send(reply);
              pendingMounts.erase(i);
              break;
            }
        }
      }break;
      case libkernel::message::vfs_set_working_dir:
      {
        if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
        {
          file::name_info *info = shm.address<file::name_info>();
          info->name[MAX_PATH] = '\0';
          
          bool found = false;
          unsigned long pid = msg.param1();
          for (size_t i=0;i < workingDirectory.size();i++)
            if (workingDirectory[i].first == pid)
            {
              // TODO: This could be done better
              if (info->name[0] == '/' ||
                  strchr(info->name, ':') != NULL)
                workingDirectory[i].second = info->name;
              else
              {
                if (info->name[strlen(info->name) - 1] == '/')
                  info->name[strlen(info->name) - 1] = '\0';
                if (workingDirectory[i].second[workingDirectory[i].second.length() - 1] != '/')
                  workingDirectory[i].second.append(1, '/');
                workingDirectory[i].second.append(info->name);
                std::cout << workingDirectory[i].second << std::endl;
              }
              found = true;
            }
          if (!found)workingDirectory.push_back(pair<processId,string>(pid, string(info->name)));
          
          message reply(msg.port(), libkernel::message::vfs_set_working_dir, static_cast<size_t>(true));
          Port.send(reply);
        }
      }break;
      case libkernel::message::vfs_get_working_dir:
      {
        if (msg.attribute() == libkernel::shared_memory::none)
        {
          libkernel::shared_memory shared(sizeof(file::name_info));
          file::name_info *info = shared.address<file::name_info>();
          string name = getCwd(msg.param1());
          strncpy(info->name, name.c_str(), MAX_PATH+1);
          message reply(msg.port(), libkernel::message::vfs_get_working_dir, 0, shared, libkernel::shared_memory::transfer_ownership);
          Port.send(reply);
        }
      }break;
      case libkernel::message::vfs_set_console:
      {
        if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
        {
          file::name_info *info = shm.address<file::name_info>();
          info->name[MAX_PATH] = '\0';
          
          bool found = false;
          processId pid = msg.param1();
          for (size_t i=0;i < consoleName.size();i++)
            if (consoleName[i].first == pid)
            {
              consoleName[i].second = info->name;
              found = true;
            }
          consoleName.push_back(pair<processId,string>(pid, info->name));
          
          message reply(msg.port(), libkernel::message::vfs_set_console, static_cast<size_t>(true));
          Port.send(reply);
        }
      }break;
      case libkernel::message::vfs_get_console:
      {
        if (msg.attribute() == libkernel::shared_memory::none)
        {
          libkernel::shared_memory shared(sizeof(file::name_info));
          file::name_info *info = shared.address<file::name_info>();
          info->name[0] = '\0';
          processId pid = msg.param1();
          for (size_t i=0;i < consoleName.size();i++)
            if (consoleName[i].first == pid)
            {
              strncpy(info->name,
                  consoleName[i].second.c_str(),
                  MAX_PATH);
              info->name[MAX_PATH] = '\0';
            }
          message reply(msg.port(), libkernel::message::vfs_get_console, static_cast<size_t>(true), shared, libkernel::shared_memory::transfer_ownership);
          Port.send(reply);
        }
      }break;
      case libkernel::message::vfs_find_fs:
      {
        if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
        {
          file::name_info *info = shm.address<file::name_info>();
          info->name[MAX_PATH] = '\0';
          
          // Find the mounted Filesystem
          string name(info->name);
          string::iterator i = find<string::iterator>(name.begin(), name.end(), '/');
          if (i == name.begin() ||
            (*(i-1) == ':' && i != name.end() && (i+1) != name.end() && *(i+1) == '/'))
          {
            size_t index = 0;
            unsigned long port = 0;
            auto i = mountedFilesystems.begin();
            auto end = mountedFilesystems.end();
            for (;i != end;++i)
            {
              if (strncmp((*i).path.c_str(), info->name, (*i).path.length()) == 0)
              {
                index = (*i).path.length();
                port = (*i).fs_port;
              }
            }
            message reply(msg.port(), libkernel::message::vfs_find_fs, port, index);
            Port.send(reply);
          }
          else
          {
            unsigned long pid = get_port_info(msg.port()).first;
            string path(getCwd(pid));
            if (name != ".")
            {
              if (name.length() != 0 &&
                path[path.length()-1] != '/')path.append("/");
              path.append(name);
            }
            else if (path == "")path = "/";
            
            size_t index = 0;
            unsigned long port = 0;
            auto i = mountedFilesystems.begin();
            auto end = mountedFilesystems.end();
            for (;i != end;++i)
            {
              if (strncmp((*i).path.c_str(), path.c_str(), (*i).path.length()) == 0)
              {
                index = (*i).path.length();
                port = (*i).fs_port;
              }
            }
            strncpy(info->name, &path.c_str()[index], MAX_PATH+1);
            message reply(msg.port(), libkernel::message::vfs_find_fs, port, shm, libkernel::shared_memory::transfer_ownership);
            Port.send(reply);
          }
        }
      }break;
      case libkernel::message::vfs_get_mount_count:
      {
        if (msg.attribute() == libkernel::shared_memory::none)
        {
          message reply(msg.port(), libkernel::message::vfs_get_mount_count, mountedFilesystems.size());
          Port.send(reply);
        }
      }break;
      case libkernel::message::vfs_get_mount_info:
      {
        if (msg.attribute() == libkernel::shared_memory::none)
        {
          libkernel::shared_memory shared(sizeof(mount_fs_info));
          mount_fs_info *info = shared.address<mount_fs_info>();
          strcpy(info->name, mountedFilesystems[msg.param1()].path.c_str());
          strcpy(info->filesystem, mountedFilesystems[msg.param1()].fs_name.c_str());
          strcpy(info->device, mountedFilesystems[msg.param1()].dev_name.c_str());
          message reply(msg.port(), libkernel::message::vfs_get_mount_info, 0, shared, libkernel::shared_memory::transfer_ownership);
          Port.send(reply);
        }
      }break;
    }
  }
  return 0;
}
