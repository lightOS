/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <libarch/ioport.hpp>
#include <lightOS/lightOS.hpp>
#include "pci.hpp"
using namespace lightOS;
using namespace server;
using namespace std;

void callback_ping(libkernel::message_port& port,
                   const libkernel::message& message)
{
  libkernel::message reply(message.port(),
                           libkernel::message::pong);
  port.send(reply);
}

void callback_pci_enum_devices(libkernel::message_port& port,
                               const libkernel::message& message)
{
  server::pci& Pci = server::pci::instance();
  const size_t size = sizeof(lightOS::pci::deviceAddr) * Pci.devices().size();

  /* Create the shared-memory region and copy the device list */
  libkernel::shared_memory shared(size);
  lightOS::pci::deviceAddr* device = shared.address<lightOS::pci::deviceAddr>();
  for (size_t i = 0;i < Pci.devices().size();i++)
  {
    device[i].bus = Pci.devices()[i].first.bus;
    device[i].device = Pci.devices()[i].first.device;
    device[i].function = Pci.devices()[i].first.function;
  }

  /* Send the response */
  libkernel::message reply(message.port(),
                           libkernel::message::pci_enum_devices,
                           Pci.devices().size(),
                           shared,
                           libkernel::shared_memory::transfer_ownership);
  port.send(reply);
}

void callback_pci_get_device_info(libkernel::message_port& port,
                                  const libkernel::message& message)
{
  server::pci& Pci = server::pci::instance();

  /* Get the information about the device and copy it */
  libkernel::shared_memory shared(sizeof(lightOS::pci::deviceInfo));
  lightOS::pci::deviceInfo *info = Pci.findDevice(message.param1(),
                                                  message.param2(),
                                                  message.param3());
  if (info != 0)
    memcpy(shared.address<void>(), info, sizeof(lightOS::pci::deviceInfo));
  else
    memset(shared.address<void>(), 0, sizeof(lightOS::pci::deviceInfo));

  /* Send the response */
  libkernel::message reply(message.port(),
                           libkernel::message::pci_get_device_info,
                           0,
                           shared,
                           libkernel::shared_memory::transfer_ownership);
  port.send(reply);
}

void callback_pci_find_device(libkernel::message_port& port,
                              const libkernel::message& message)
{
  server::pci& Pci = server::pci::instance();

  /* Search for the device */
  lightOS::pci::deviceAddr addr = Pci.findDevice(message.param1(),
                                                 message.param2());

  /* Send the response */
  libkernel::message reply(message.port(),
                           libkernel::message::pci_find_device,
                           addr.bus,
                           addr.device,
                           addr.function);
  port.send(reply);
}

void callback_pci_enable_busmaster(libkernel::message_port& port,
                                   const libkernel::message& message)
{
  server::pci& Pci = server::pci::instance();

  /* Enable PCI Busmastering */
  bool result = Pci.enableBusmaster(message.param1(),
                                    message.param2(),
                                    message.param3());

  /* Send the response */
  libkernel::message reply(message.port(),
                           libkernel::message::pci_enable_busmaster,
                           static_cast<size_t>(result));
  port.send(reply);
}

int main()
{
  libkernel::message_port Port(libkernel::message_port::pci, true);
  if (!Port)
  {
    cerr << "pci: pci already started" << endl;
    return -1;
  }

  libkernel::message_port::callback callback;
  callback.register_handler(libkernel::message::ping,
                            &callback_ping);
  callback.register_handler(libkernel::message::pci_enum_devices,
                            &callback_pci_enum_devices);
  callback.register_handler(libkernel::message::pci_get_device_info,
                            &callback_pci_get_device_info);
  callback.register_handler(libkernel::message::pci_find_device,
                            &callback_pci_find_device);
  callback.register_handler(libkernel::message::pci_enable_busmaster,
                            &callback_pci_enable_busmaster);
  callback(Port);
}

SINGLETON_INSTANCE(server::pci);

server::pci::pci()
{
  // TODO FIXME HACK
  allocate_io_port_range(0, 0);

  for (size_t bus=0;bus < 5;bus++)
    for (size_t device=0;device < 32;device++)
      if (isPresent(bus, device, 0) == true)
      {
        lightOS::pci::deviceInfo info = readHeader(bus, device, 0);
        mDevices.push_back(make_pair(lightOS::pci::deviceAddr(bus, device, 0), info));
        if (isMultifunction(bus, device) == true)
          for (size_t function = 1;function < 8;function++)
            if (isPresent(bus, device, function) == true)
            {
              info = readHeader(bus, device, function);
              mDevices.push_back(make_pair(lightOS::pci::deviceAddr(bus, device, function), info));
            }
      }
}

server::pci::~pci()
{
}

uint32_t server::pci::read32( size_t bus,
                size_t device,
                size_t function,
                size_t reg)
{
  libarch::out32(config_address, 0x80000000|(bus << 16)|((device & 0x1F) << 11)|((function & 0x07) << 8)|(reg & (~3)));
  return libarch::in32(config_data);
}
void server::pci::write32(  size_t bus,
              size_t device,
              size_t function,
              size_t reg,
              uint32_t value)
{
  libarch::out32(config_address, 0x80000000|(bus << 16)|((device & 0x1F) << 11)|((function & 0x07) << 8)|(reg & (~3)));
  libarch::out32(config_data, value);
}
bool server::pci::isPresent(size_t bus,
              size_t device,
              size_t function)
{
  if (read32(bus, device, function, 0) == 0xFFFFFFFF)return false;
  return true;
}
bool server::pci::isMultifunction(  size_t bus,
                  size_t device)
{
  if ((read32(bus, device, 0, 12) & 0x800000) != 0)return true;
  return false;
}
bool server::pci::enableBusmaster(size_t bus, size_t device, size_t function)
{
  lightOS::pci::deviceInfo *info = findDevice(bus, device, function);
  if (info == 0)return false;
  
  if ((info->command & lightOS::pci::command::enable_busmaster) != 0)return true;
  
  uint32_t tmp = read32(bus, device, function, 0x04);
  write32(bus, device, function, 0x04, tmp | lightOS::pci::command::enable_busmaster);
  if (((tmp = read32(bus, device, function, 0x04)) & lightOS::pci::command::enable_busmaster) != 0)
  {
    info->command |= lightOS::pci::command::enable_busmaster;
    return true;
  }
  return false;
}
lightOS::pci::deviceInfo server::pci::readHeader( size_t bus,
                          size_t device,
                          size_t function)
{
  lightOS::pci::deviceInfo Header;
  memset(&Header, 0, sizeof(lightOS::pci::deviceInfo));
  uint32_t value = read32(bus, device, function, 0);
  Header.vendorId = value & 0xFFFF;
  Header.deviceId = value >> 16;
  value = read32(bus, device, function, 4);
  Header.command = value & 0xFFFF;
  Header.status = value >> 16;
  value = read32(bus, device, function, 8);
  Header.revision = value & 0xFF;
  Header.interface = (value >> 8) & 0xFF;
  Header.subclass = (value >> 16) & 0xFF;
  Header.baseclass = (value >> 24) & 0xFF;
  value = read32(bus, device, function, 12);
  Header.clg = value & 0xFF;
  Header.latency = (value >> 8) & 0xFF;
  Header.header = (value >> 16) & 0xFF;
  Header.bist = (value >> 24) & 0xFF;
  value = read32(bus, device, function, 60);
  Header.irq = (value >> 8) & 0xFF;
  if (Header.irq != 0)
    Header.irq = value & 0xFF;
  
  uint32_t baseAddressRegisters[6];
  uint32_t baseAddressSizes[6];
  for (size_t i = 0;i < 6;i++)
    baseAddressRegisters[i] = read32(bus, device, function, 16 + i * 4);
  for (size_t i = 0;i < 6;i++)
  {
    write32(bus, device, function, 16 + i * 4, 0xFFFFFFFF);
    baseAddressSizes[i] = read32(bus, device, function, 16 + i * 4);
    write32(bus, device, function, 16 + i * 4, baseAddressRegisters[i]);
  }
  
  size_t ioIndex = 0;
  size_t mmIndex = 0;
  uint32_t *cur = baseAddressRegisters;
  uint32_t *curSize = baseAddressSizes;
  while ((Header.header & 0x3F) == 0 && cur != &baseAddressRegisters[6])
  {
    if (*cur != 0)
    {
      // memory region
      if ((*cur & 1) == 0)
      {
        if ((*cur & 6) == 0)
        {
          Header.mm_space[mmIndex] = (*cur) & 0xFFFFFF0;
          Header.mm_space_size[mmIndex] = (~(*curSize & 0xFFFFFFF0)) + 1;
        }
        else if ((*cur & 6) == 2)
        {
          Header.mm_space[mmIndex] = (*cur) & 0xFFFFFF0;
          ++cur;
          Header.mm_space[mmIndex] |= static_cast<uint64_t>((*cur) & 0xFFFFFFFF) << 32;
          Header.mm_space_size[mmIndex] = (~(*curSize & 0xFFFFFFF0)) + 1;
          ++curSize;
          Header.mm_space_size[mmIndex] |= static_cast<uint64_t>((~ *curSize) + 1) << 32;
        }
        ++mmIndex;
      }
      // i/o region
      else
      {
        Header.io_space[ioIndex] = (*cur) & 0xFFFFFFFC;
        Header.io_space_size[ioIndex] = (~ (*curSize & 0xFFFFFFFC)) + 1;
        ++ioIndex;
      }
    }
    ++cur;
    ++curSize;
  }
  
  return Header;
}
lightOS::pci::deviceAddr server::pci::findDevice(size_t vendor, size_t device)
{
  for (size_t i = 0;i < mDevices.size();i++)
    if ((vendor == PCI_DEVICE_NONE || mDevices[i].second.vendorId == vendor) &&
      (device == PCI_DEVICE_NONE || mDevices[i].second.deviceId == device))
      return lightOS::pci::deviceAddr(mDevices[i].first.bus, mDevices[i].first.device, mDevices[i].first.function);
  return lightOS::pci::deviceAddr();
}
lightOS::pci::deviceInfo *server::pci::findDevice(  size_t bus,
                          size_t device,
                          size_t function)
{
  for (size_t i = 0;i < mDevices.size();i++)
    if (mDevices[i].first.bus == bus &&
      mDevices[i].first.device == device &&
      mDevices[i].first.function == function)
      return &mDevices[i].second;
  return 0;
}
