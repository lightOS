/*
lightOS server
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_SERVER_PCI_HPP
#define LIGHTOS_SERVER_PCI_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup pci pci */
/*@{*/

#include <cstddef>
#include <cstdint>
#include <vector>
#include <utility>
#include <lightOS/pci.hpp>

#define SINGLETON(x)                   public: \
                                         inline static x& instance(){return m_instance;} \
                                       private: \
                                         x(); \
                                         x(const x&) = delete; \
                                         x& operator = (const x&) = delete; \
                                         ~x(); \
                                         static x m_instance;

#define SINGLETON_INSTANCE(x)          x x::m_instance

namespace server
{
  class pci
  {
    SINGLETON(pci)

    public:
      uint32_t read32(size_t bus,
                      size_t device,
                      size_t function,
                      size_t reg);
      void write32(size_t bus,
                   size_t device,
                   size_t function,
                   size_t reg,
                   uint32_t value);
      bool isPresent(size_t bus,
                     size_t device,
                     size_t function);
      bool isMultifunction(size_t bus,
                           size_t device);
      lightOS::pci::deviceInfo readHeader(size_t bus,
                                          size_t device,
                                          size_t function);
      bool enableBusmaster(size_t bus,
                           size_t device,
                           size_t function);
      void enumDevices();
      lightOS::pci::deviceAddr findDevice(size_t vendor,
                                          size_t device);
      lightOS::pci::deviceInfo *findDevice(size_t bus,
                                           size_t device,
                                           size_t function);
      const std::vector<std::pair<lightOS::pci::deviceAddr, lightOS::pci::deviceInfo> > &devices() const{return mDevices;}

    private:
      /*! The pci devices */
      std::vector<std::pair<lightOS::pci::deviceAddr, lightOS::pci::deviceInfo> > mDevices;

      /*! The pci ports */
      enum port
      {
        /*! The pci configuration address port */
        config_address  = 0xCF8,
        /*! The pci configuration data port */
        config_data   = 0xCFC
      };
  };
}

/*@}*/
/*@}*/

#endif
