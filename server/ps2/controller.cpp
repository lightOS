/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/debug.hpp>
#include <libarch/ioport.hpp>
#include "controller.hpp"
using namespace std;
using namespace lightOS;
using namespace server;
using namespace ps2;

using libkernel::message;

controller::controller()
	: mPort(), mKeyboard(0), mMouse(0)
{
	// Wait for devfs
	waitForDevfs(mPort);
}
bool controller::init()
{
	// Grant access to the I/O ports
	// TODO FIXME HACK
	allocate_io_port_range(0, 0);
	
	// Read all pending keys
	while ((libarch::in8(status_port) & 0x01) != 0)
	{
		libarch::in8(data_port);
		wait(10);
	}
	
	// Disable keyboard and mouse
	if (enable_keyboard(false) == false)
	{
		cout << "ps2: Failed to disable the keyboard" << endl;
		return false;
	}
	
	// Disable scancode set 1 translation
	command(0x60, 0x87);
	
	// Probe for a keyboard & mouse
	bool bKeyboard = detect_keyboard();
	bool bMouse = detect_mouse();
	
	// No device found
	if (bKeyboard == false && bMouse == false)
	{
		cerr << "ps2: no device found" << endl;
		return false;
	}
	
	if (bKeyboard)
	{
		cout << "ps2: keyboard found" << endl;
		
		// Register the keyboard IRQ
		mKeyboardVector = mPort.register_isa_irq(keyboard_irq, libkernel::irq_priority::keyboard);
		
		// Create the keyboard device
		mKeyboard = new keyboard(*this);
		
		// Initialize the device
		if (mKeyboard->init_device() == false)
			return false;
	}
	if (bMouse)
	{
		cout << "ps2: mouse found" << endl;
		
		// Register the mouse IRQ
		mMouseVector = mPort.register_isa_irq(mouse_irq, libkernel::irq_priority::mouse);
		
		// Create the mouse device
		mMouse = new mouse(*this);
		
		// Initialize the device
		if (mMouse->init_device() == false)
			return false;
	}
	
	// Enable the keyboard
	if (bKeyboard)
		enable_keyboard(true);
	
	// Enable the mouse
	if (bMouse)
		enable_mouse(true);
	
	return true;
}
void controller::loop()
{
	message msg;
	while (mPort.get(msg))
	{
		libkernel::shared_memory shm = msg.get_shared_memory();
		switch (msg.type())
		{
			case libkernel::message::ping:
			{
				message reply(msg.port(), libkernel::message::pong, 0, 0, 0);
				mPort.send(reply);
			}break;
			case libkernel::message::event:
			{
				if (msg.param1() == libkernel::event::irq &&
					msg.port() == libkernel::message_port::kernel)
				{
					uint8_t status = libarch::in8(status_port);
					if ((status & 0x80) != 0 ||
						(status & 0x40) != 0)
					{
						cerr << "ps2: error" << endl;
						//TODO: Parity error or timeout -> resend
					}
					else
					{
						uint8_t data = libarch::in8(data_port);
            mPort.acknoledge_irq(msg.param2());
						if ((status & 0x20) != 0)mMouse->received(data);
						else if ((status & 0x01) != 0)mKeyboard->received(data);
					}
				}
			}break;
		}
	}
}
bool controller::enable_keyboard(bool b)
{
	return command(b ? 0xAE : 0xAD);
}
bool controller::enable_mouse(bool b)
{
	return command(b ? 0xA8 : 0xA7);
}
bool controller::keyboard_command(uint8_t cmd)
{
	if (wait_ready() == false)return false;
	libarch::out8(data_port, cmd);
	wait_keyboard_irq();
	if (libarch::in8(data_port) == 0xFA)return true;
	return false;
}
bool controller::keyboard_command(uint8_t cmd, uint8_t param)
{
	if (keyboard_command(cmd) == false)return false;
	if (keyboard_command(param) == false)return false;
	return true;
}
bool controller::mouse_command(uint8_t cmd)
{
	command(0xD4, cmd);
	wait_mouse_irq();
	if (libarch::in8(data_port) == 0xFA)return true;
	return false;
}
bool controller::mouse_command(uint8_t cmd, uint8_t param)
{
	if (mouse_command(cmd) == false)return false;
	if (mouse_command(param) == false)return false;
	return true;
}
void controller::wait_mouse_irq()
{
	message msg(libkernel::message_port::kernel, libkernel::message::event, libkernel::event::irq, mMouseVector);
	mPort.wait(msg);
	mPort.acknoledge_irq(mMouseVector);
}
void controller::wait_keyboard_irq()
{
	message msg(libkernel::message_port::kernel, libkernel::message::event, libkernel::event::irq, mKeyboardVector);
	mPort.wait(msg);
	mPort.acknoledge_irq(mKeyboardVector);
}
bool controller::detect_keyboard()
{
	// Check the interface
	if (command(0xAB) == false)return false;
	uint8_t retVal;
	if (get_return_value(retVal) == false ||
		retVal != 0x00)
		return false;
	
	// Check the 'echo' keyboard command
	if (wait_ready() == false)return false;
	libarch::out8(data_port, 0xEE);
	uint8_t val;
	if (get_return_value(val) == false ||
		val != 0xEE)
		return false;
	
	return true;
}
bool controller::detect_mouse()
{
	// Check the interface
	if (command(0xA9) == false)return false;
	uint8_t retVal;
	if (get_return_value(retVal) == false ||
		retVal != 0x00)
		return false;
	
	// Check the 'identify' mouse command
	if (command(0xD4, 0xF2) == false)return false;
	uint8_t val;
	if (get_return_value(val) == false ||
		val != 0xFA)return false;
	if (get_return_value_aux(val) == false)return false;
	
	return true;
}
bool controller::command(uint8_t cmd)
{
	if (wait_ready() == false)return false;
	libarch::out8(command_port, cmd);
	return true;
}
bool controller::command(uint8_t cmd, uint8_t param)
{
	if (command(cmd) == false)return false;
	if (wait_ready() == false)return false;
	libarch::out8(data_port, param);
	return true;
}
bool controller::get_return_value(uint8_t &val)
{
	if (wait_data() == false)return false;
	val = libarch::in8(data_port);
	return true;
}
bool controller::get_return_value_aux(uint8_t &val)
{
	if (wait_data_aux() == false)return false;
	val = libarch::in8(data_port);
	return true;
}
bool controller::wait_ready()
{
	size_t counter = timeout;
	while ((libarch::in8(status_port) & 0x02) != 0)
	{
		--counter;
		if (counter == 0)return false;
	}
	return true;
}
bool controller::wait_data()
{
	size_t counter = timeout;
	while ((libarch::in8(status_port) & 0x01) == 0)
	{
		--counter;
		if (counter == 0)return false;
	}
	return true;
}
bool controller::wait_data_aux()
{
	size_t counter = timeout;
	while ((libarch::in8(status_port) & 0x20) == 0)
	{
		--counter;
		if (counter == 0)return false;
	}
	return true;
}
controller::~controller()
{
	delete mKeyboard;
	delete mMouse;
}

int main(int argc, char *argv[])
{
	controller Controller;
	
	// Initialize the ps/2 controller and the devices
	if (Controller.init() == false)
	{
		// TODO: Do the cleanup (irq, etc...)
		cout << "ps2: exiting" << endl;
		return -1;
	}
	
	// The main loop
	Controller.loop();
	
	return 0;
}
