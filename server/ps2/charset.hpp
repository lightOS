/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include<vector>
#include<string>
#include<lightOS/lightOS.hpp>

using namespace std;
using namespace lightOS;
class charset
{
	public:
		/*! The constructor */
		inline charset(){}


		/*! Get the char wich belongs to the name name
		 *\param[name] name of the char */
		wchar_t getCharCode(string charName)
		{
			for(size_t i=0; i < mCharset.size(); i++)
			{
				if(mCharset[i].charName == charName)
				{
					return mCharset[i].charCode;
				}
			}
			return NVchar;
		}


		/*! Get the name of a character
		 *\param[charCode] name of the char */
		string getCharName(wchar_t charCode)
		{
			for(size_t i=0; i < mCharset.size(); i++)
			{
				if(mCharset[i].charCode == charCode)
				{
					return mCharset[i].charName;
				}


			}
			return NVstring;
		}
		
		
		wchar_t operator[](string charName) {
			return getCharCode(charName);
		}

		string operator[](wchar_t charCode) {
			return getCharName(charCode);
		}

		bool load(string fileName)
		{
			file charsetFile;
			//cout << "charset::load(...): begin to open the charset" << endl;
			if(!charsetFile.open(fileName.c_str(), access::read))
			{
				return false;
			}
			
			
			size_t size = charsetFile.blocksize() * charsetFile.blockcount();
			auto_array<char> data(new char[size+1]);
			
			//cout << "charset::load(...): begin to read the charset" << endl;
			
			if (charsetFile.read(data.get(), 0, size) != size)
			{
				return false;
			}
			
			// set last byte to zero
			data.get()[size] = '\0';
			
			string charsetStr(data.get());
			string::iterator lineBegin = charsetStr.begin();
			charsetEntry entry;
			
			// Parse the file
			while (lineBegin != charsetStr.end())
			{
				
				string::iterator lineEnd = find<string::iterator>(lineBegin, charsetStr.end(), '\n');
				string line(lineBegin, lineEnd);
				line.trim();
				
				//cout << "charset::load(...): line='" << line << "'" << endl;
				
				string::iterator equalSign = find<string::iterator>(line.begin(), line.end(), '=');
				
				// if there is no equal sign on the line, or the first sign is a #
				if((equalSign != line.end()) && (line[0] != '#'))
				{
					string part1(line.begin(), equalSign);
					string part2(++equalSign, line.end());
					part1.trim();
					part2.trim();
					entry.charName = part1;
					entry.charCode = strtol(part2.c_str(), 0, 10);
					mCharset.push_back(entry);
				}
				
				if (lineEnd == charsetStr.end())break;
				lineBegin = ++lineEnd;
			}
			//cout << "charset::load(...): charset successfully loaded" << endl;
			return true;
		}

	protected:
		string NVstring;
		wchar_t NVchar;

	private:
		struct charsetEntry
		{
			wchar_t charCode;
			string charName;
		};

		vector<charsetEntry> mCharset;
};
