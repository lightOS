/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SERVER_PS2_MOUSE_HPP
#define SERVER_PS2_MOUSE_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup ps2 ps/2 */
/*@{*/

#include <lightOS/lightOS.hpp>

namespace server
{
	namespace ps2
	{
		class controller;
		class mouse
		{
			public:
				/*! The constructor */
				mouse(controller &Controller);
				/*! Initialize the mouse */
				bool init_device();
				/*! Byte received */
				void received(uint8_t byte);
				/*! The destructor */
				inline ~mouse(){}
			private:
				/*! Set the stream mode
				 *\return true, if successfull, false otherwise */
				bool set_sample_rate(uint8_t rate);
				/*! Read the device id
				 *\param[in,out] id the device id
				 *\return true, if successfull, false otherwise */
				bool get_device_id();
				/*! The controller */
				controller &mController;
				/*! mouse device */
				lightOS::file mDev;
				/*! The device id */
				uint8_t mDeviceId;
		};
	}
}

/*@}*/
/*@}*/

#endif
