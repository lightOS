/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <libarch/ioport.hpp>
#include <lightOS/device.hpp>
#include "mouse.hpp"
#include "controller.hpp"
using namespace std;
using namespace lightOS;
using namespace server;
using namespace ps2;

mouse::mouse(controller &Controller)
	: mController(Controller), mDev("dev://mouse", file::stream, access::write, sizeof(mouseData)), mDeviceId(0)
{
}
bool mouse::init_device()
{
	// Reset
	if (mController.mouse_command(0xFF) == false)return false;
	mController.wait_mouse_irq();
	uint8_t bat = libarch::in8(0x60);
	mController.wait_mouse_irq();
	uint8_t id = libarch::in8(0x60);
	if (bat != 0xAA || id != 0x00)
	{
		cerr << "ps2: mouse reset failed" << endl;
		return false;
	}
	
	// Set defaults
	if (mController.mouse_command(0xF6) != true)
	{
		cerr << "ps2: failed to set mouse default values" << endl;
		return false;
	}
	
	// Set scalling 1:1
	if (mController.mouse_command(0xE6) != true)
	{
		cerr << "ps2: failed to set scalling to 1:1" << endl;
		return false;
	}
	
	// Stream mode
	if (mController.mouse_command(0xEA) != true)
	{
		cerr << "ps2: failed entering stream mode" << endl;
		return false;
	}
	
	// Enable data reporting
	if (mController.mouse_command(0xF4) != true)
	{
		cerr << "ps2: failed to enable data reporting" << endl;
		return false;
	}
	
	// Check for Intellimouse Extension
	if (set_sample_rate(0xC8) != true ||
		set_sample_rate(0x64) != true ||
		set_sample_rate(0x50) != true)
	{
		cerr << "ps2: failed to set the sample rate" << endl;
		return false;
	}
	
	if (get_device_id() == false)
	{
		cerr << "ps2: failed to get the device id" << endl;
		return false;
	}
	
	if (set_sample_rate(0xC8) != true ||
		set_sample_rate(0xC8) != true ||
		set_sample_rate(0x50) != true)
	{
		cerr << "ps2: failed to set the sample rate" << endl;
		return false;
	}
	
	if (get_device_id() == false)
	{
		cerr << "ps2: failed to get the device id" << endl;
		return false;
	}
	
	// Reset the sample rate
	if (set_sample_rate(0xC8) != true)
	{
		cerr << "ps2: failed to set the sample rate" << endl;
		return false;
	}
	
	return true;
}
void mouse::received(uint8_t byte)
{
	static size_t index = 0;
	static uint8_t data[5];
	
	data[index] = byte;
	
	if ((mDeviceId == 0x00 && index == 2) ||
		(mDeviceId == 0x03 && index == 3) ||
		(mDeviceId == 0x04 && index == 3))
	{
		// Initialize the mouse data
		mouseData mousePacket;
		memset(&mousePacket, 0, sizeof(mouseData));
		
		// Normal PS/2 mouse
		mousePacket.left = data[0] & 1;
		mousePacket.right = (data[0] >> 1) & 1;
		mousePacket.middle = (data[0] >> 2) & 1;
		if (((data[0] >> 4) & 1) == 1)mousePacket.x = -static_cast<uint8_t>(~data[1]);
		else mousePacket.x = data[1];
		if (((data[0] >> 5) & 1) == 1)mousePacket.y = -static_cast<uint8_t>(~data[2]);
		else mousePacket.y = data[2];
		
		// Scroll wheel?
		if (mDeviceId == 0x03 ||
			mDeviceId == 0x04)
		{
			mousePacket.z = (data[3] & 0x07);
			if ((data[3] & 0x08) != 0)mousePacket.z = -static_cast<uint8_t>(~mousePacket.z);
		}
		// 5 buttons?
		if (mDeviceId == 0x04)
		{
			mousePacket.button4 = (data[3] >> 4) & 1;
			mousePacket.button5 = (data[3] >> 5) & 1;
		}
		
		mDev.write<mouseData>(mousePacket);
		index = 0;
	}
	else ++index;
}
bool mouse::set_sample_rate(uint8_t rate)
{
	if (mController.mouse_command(0xF3, rate) != true)
	{
		cerr << "ps2: failed to set the sample rate" << endl;
		return false;
	}
	return true;
}
bool mouse::get_device_id()
{
	if (mController.mouse_command(0xF2) != true ||
		mController.get_return_value_aux(mDeviceId) != true)
	{
		cerr << "ps2: failed to get the device id" << endl;
		return false;
	}
	return true;
}
