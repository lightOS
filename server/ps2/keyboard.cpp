/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <lightOS/device.hpp>
#include <lightOS/lightOS.hpp>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
#include <stdlib.h>
#include "controller.hpp"
#include "charset.hpp"
#include "keyboard.hpp"

using namespace std;
using namespace lightOS;
using namespace server;
using namespace ps2;

#define KEY_COUNT 256

// TODO: NumLock
// TODO: do keys change their meaning, when Ctrl is pressed?

struct keymapEntry
{
	wchar_t normal;
	wchar_t shift;
	wchar_t control;
	wchar_t altgr;
};

charset currentCharset;
unsigned char keycode[256] =
{
//0																											Tab
	0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0x0F,	0,		0,
//1											Q		1								Y		S		A		W		2
	0,		0,		0,		0,		0,		0x10,	0x02,	0,		0,		0,		0x2C,	0x1F,	0x1E,	0x11,	0x03,	0,
//2			C		X		D		E		4		3						Space	V		F		T		R		5
	0,		0x2E,	0x2D,	0x20,	0x12,	0x05,	0x04,	0,		0,		0x39,	0x2F,	0x21,	0x14,	0x13,	0x06,	0,
//3			N		B		H		G		Z		6								M		J		U		7		8
	0,		0x31,	0x30,	0x23,	0x22,	0x15,	0x07,	0,		0,		0,		0x32,	0x24,	0x16,	0x08,	0x09,	0,
//4			,		K		I		o		0		9						.		-		L				P		�
	0,		0x33,	0x25,	0x17,	0x18,	0x0B,	0x0A,	0,		0,		0x34,	0x35,	0x26,	0,		0x19,	0x0C,	0,
//5											`´										Enter	Plus
	0,		0,		0,		0,		0,		0x0D,	0,		0,		0,		0,		0x1C,	0x1B,	0,		0,		0,		0,
//6			<										Backspace				Num: 1			Num: 4	Num: 7
	0,		0x56,	0,		0,		0,		0,		0x0E,	0,		0,		0x4F,	0,		0x4B,	0x47,	0,		0,		0,
//7	Num 0	Num: ,	Num: 2	Num: 5	Num: 6	Num: 8	Esc						Num: +			Num: -	Num: *	Num: 9
	0x52,	0x53,	0x50,	0x4C,	0x4D,	0x48,	0,		0,		0,		0x4E,	0,		0x35,	0x37,	0x49,	0,		0,
	0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,
	0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,
	0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,
	0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,
	0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,
	0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,
	0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,
	0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,		0,
};

libserver::keycode vkeycode[256] =
{
// 0				F9				F7				F5				F3				F1				F2				F12								F10				F8				F6				F4													
	libserver::keycode::none,		libserver::keycode::f9,			libserver::keycode::f7,			libserver::keycode::f5,			libserver::keycode::f3,			libserver::keycode::f1,			libserver::keycode::f2,			libserver::keycode::f12,		libserver::keycode::none,		libserver::keycode::f10,		libserver::keycode::f8,			libserver::keycode::f6,			libserver::keycode::f4,			libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//1																																																														
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//2																																																														
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//3																																																															
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//4																																																															
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//5																																																															
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//6																																																															
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//7																									ESC								F11																														
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::escape,		libserver::keycode::none,		libserver::keycode::f11,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//8																																																															
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
};
libserver::keycode vkeycodeExt[256] =
{
//0																																																															
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//1																																																															
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//2																																																															
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//3																																																															
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//4																																																															
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//5																																																															
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//6																																					End								arrow left		Pos1													
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::end,		libserver::keycode::none,		libserver::keycode::arrow_left,	libserver::keycode::pos1,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
//7					Erase			arrow down						arrow right		arrow up																		page down										page up									
	libserver::keycode::none,		libserver::keycode::erase,		libserver::keycode::arrow_down,	libserver::keycode::none,		libserver::keycode::arrow_right,libserver::keycode::arrow_up,	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::page_down,	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::page_up,	libserver::keycode::none,		libserver::keycode::none,
//8																																																															
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
	libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,		libserver::keycode::none,
};

keymapEntry keymap[KEY_COUNT];

keyboard::keyboard(controller &Controller)
	: mController(Controller), mDev("dev://keyboard", file::stream, access::write, sizeof(keyboardData)), mNum(false), mCaps(false),
	  mCapslock(false), mAlt(false), mAltgr(false), mControl(false), mBreakcode(false)
{
	// Read the config file
	mConfig = config::parse<config::key_value_pair>("/system/config/keyboard.cfg");
	if (mConfig == 0)
	{
		cerr << "ps2: no config file" << endl;
		return;
	}
	
	// Load the charset
	const config::key_value_pair *charset = (*mConfig)["charset"];
	if (charset == 0)
	{
		cerr << "ps2: no \"charset\" entry in the config file" << endl;
		return;
	}
	if(currentCharset.load(charset->get_value()) == false)
	{
		cerr << "ps2: error loading charset" << endl;
		return;
	}
	
	// Include directory
	const config::key_value_pair *include = (*mConfig)["include"];
	if (include == 0)
	{
		cerr << "ps2: no \"include\" entry in the config file" << endl;
		return;
	}
	
	// Load the default keymap
	const config::key_value_pair *keymap = (*mConfig)["keymap::default"];
	if (keymap == 0)
	{
		cerr << "ps2: no \"keymap::default\" entry in the config file" << endl;
		return;
	}
	loadKeymap(keymap->get_value().c_str());
}
bool keyboard::init_device()
{
	// Set typematic rate (30key/s) / delay (250ms)
	if (mController.keyboard_command(0xF3, 0x00) == false)
	{
		cerr << "ps2: failed to set typematic rate/delay" << endl;
		return false;
	}
	
	// Set the leds
	if (set_leds() == false)
	{
		cerr << "ps2: failed to set the LEDs" << endl;
		return false;
	}
	
	return true;
}
bool keyboard::set_leds()
{
	uint8_t leds = 0;
	if (mNum == true)leds |= 0x02;
	if (mCapslock == true)leds |= 0x04;
	return mController.keyboard_command(0xED, leds);
}
void keyboard::received(uint8_t byte)
{
	static bool extendedScancode = false;
	
	if (byte == 0xF0)
	{
		mBreakcode = true;
		return;
	}
	
	/* Extended scancodes? */
	if (byte == 0xE0)
	{
		extendedScancode = true;
		return;
	}
	
	/* num Lock */
	if ((!extendedScancode) && byte == 0x77 && !mBreakcode)
	{
		mNum = !mNum;
		set_leds();
	}
	/* caps Lock */
	else if ((!extendedScancode) && byte == 0x58 && !mBreakcode)
	{
		mCapslock = !mCapslock;
		set_leds();
	}
	// alt
	else if ((!extendedScancode) && byte == 0x11)
	{
		mAlt = !mBreakcode;
	}
	// altgr
	else if (extendedScancode && byte == 0x11)
	{
		mAltgr = !mBreakcode;
	}
	// Ctrl
	else if (byte == 0x14)
	{
		mControl = !mBreakcode;
	}
	// shift
	else if ((!extendedScancode && byte == 0x12) ||
			 (!extendedScancode && byte == 0x59))
	{
		mCaps = !mBreakcode;
	}
	else if (!mBreakcode)
	{
		keyboardData data;
		memset(&data, 0, sizeof(keyboardData));
		data.ctrl = mControl;
		data.alt = mAlt;
		data.altgr = mAltgr;
		
		bool bSend = false;
		size_t keyCode = 0;
		if (!extendedScancode)
			keyCode =  keycode[byte];
		if (keyCode != 0)
		{
			data.key.code = libserver::keycode::none;
			
			if(mCaps ^ mCapslock)
			{
				data.key.character = keymap[keyCode-1].shift;
			}
			else if(mAltgr)
			{
				data.key.character = keymap[keyCode-1].altgr;
			}
			/*else if(mControl)
			{
				data.unicode = keymap[keyCode-1].control;
			}*/
			else
			{
				data.key.character = keymap[keyCode-1].normal;
			}
			
			bSend = true;
		}
		else
		{
			data.key.character = L'\0';
			
			if (extendedScancode)
				data.key.code = vkeycodeExt[byte];
			else
				data.key.code = vkeycode[byte];
			
			if (data.key.code == libserver::keycode::none)
			{
				clog << "ps2: unknown scancode 0x" << hex << static_cast<size_t>(byte);
				if (extendedScancode)clog << " (extended)";
				clog << endl;
			}
			else bSend = true;
		}
		
		if (bSend)mDev.write<keyboardData>(data);
	}
	
	if (extendedScancode == true)
	{
		extendedScancode = false;
	}
	
	mBreakcode = false;
}
bool setKeycode(int keycodeID, string modNormal, string modShift, string modControl, string modAltgr)
{
	if((keycodeID > 0) && (keycodeID <= KEY_COUNT))
	{
		keymap[keycodeID - 1].normal = currentCharset.getCharCode(modNormal);
		keymap[keycodeID - 1].shift = currentCharset.getCharCode(modShift);
		keymap[keycodeID - 1].control = currentCharset.getCharCode(modControl);
		keymap[keycodeID - 1].altgr =  currentCharset.getCharCode(modAltgr);
		return true;
	}
	return false;
}
bool keyboard::loadKeymap(const char *path)
{	
	file keymapFile;
	
	
	// Open keymap
	if(keymapFile.open(path, access::read) == false)
	{
		cerr << "error loading keymap(open):'" << path << "'" << endl;
		return false;
	}
	size_t size = keymapFile.blocksize() * keymapFile.blockcount();
	auto_array<char> data(new char[size+1]);

	// Read the content of the keymap-file
	if(keymapFile.read(data.get(), 0, size) != size)
	{
		cerr << "error loading keymap(read):" << path << "'" << endl;
		return false;
	}
	// Set last char to null
	data.get()[size] = '\0';
	
	string keymapStr(data.get());
	string::iterator cur = keymapStr.begin();
	
	while (cur != keymapStr.end())
	{
		string::iterator next = find<string::iterator>(cur, keymapStr.end(), '\n');
		string line(cur, next);
		
		line.trim();

		// don't parse empty lines and comments
		if((line.size() != 0) && (line[0] != '#'))
		{
			// cut off the command (the first word)
			string command(line.begin(), find<string::iterator>(line.begin(), line.end(), ' '));

			// move the rest into the arguments string
			string arguments(find<string::iterator>(line.begin(), line.end(), ' '), line.end());
			
			if(command == "keycode")
			{
				string keycodeStr(arguments.begin(), find<string::iterator>(arguments.begin(), arguments.end(), '='));
				//cout << "Set Keycode:";

				keycodeStr.trim();
				int keycodeInt = strtol(keycodeStr.c_str(), 0, 10);
				
				string modifier[4];
				string modifierString(find<string::iterator>(arguments.begin(), arguments.end(), '=') + 1, arguments.end());

				// this iterator allways points to the end of the current modifier
				string::iterator modEnd = modifierString.begin();
				bool endedParsing = false;
				// parse columns
				for(size_t modifierID=0; modifierID < 4; modifierID++)
				{
					if(endedParsing)
					{
						modifier[modifierID] = "";
					}
					else
					{
						modifierString = string(modEnd, modifierString.end()).trim();
						
						
						// update the iterator
						modEnd = find<string::iterator>(modifierString.begin(), modifierString.end(), ' ');
					
						if(modEnd == modifierString.end())
						{
							endedParsing = true;
						}
						
						
						modifier[modifierID] = string(modifierString.begin(), modEnd);
					}
				}
				
				if(setKeycode(keycodeInt, modifier[0], modifier[1], modifier[3], modifier[2]) == false)
					cout << "error setting keycode" << endl;
			}
			else if(command == "include")
			{
				arguments.trim();
				
				string includeFilename((*mConfig)["include"]->get_value());
				includeFilename.append(string(arguments.begin() + 1, arguments.end() - 1));
				
				if(find<string::iterator>(arguments.begin(), arguments.end(), '.') == arguments.end())
				{
					includeFilename = includeFilename.append(".inc");
				}
				

				loadKeymap(includeFilename.c_str());
			}
		}

		if (next == keymapStr.end())break;
		cur = ++next;
	}
	
	return true;
}
