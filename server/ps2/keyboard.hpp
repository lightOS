/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SERVER_PS2_KEYBOARD_HPP
#define SERVER_PS2_KEYBOARD_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup ps2 ps/2 */
/*@{*/

#include <lightOS/lightOS.hpp>
#include <lightOS/config.hpp>

namespace server
{
	namespace ps2
	{
		class controller;
		class keyboard
		{
			public:
				/*! The constructor */
				keyboard(controller &Controller);
				/*! Initialize the keyboard */
				bool init_device();
				/*! Byte received */
				void received(uint8_t byte);
				/*! The destructor */
				inline ~keyboard(){delete mConfig;}
			private:
				/*! Load a new keymap */
				bool loadKeymap(const char *path);
				/*! Set leds */
				bool set_leds();
				/*! The controller */
				controller &mController;
				/*! keyboard device */
				lightOS::file mDev;
				/*! Numlock? */
				bool mNum;
				/*! Caps? */
				bool mCaps;
				/*! Capslock? */
				bool mCapslock;
				/*! Alt? */
				bool mAlt;
				/*! Altgr? */
				bool mAltgr;
				/*! Control */
				bool mControl;
				/*! Breakcode? */
				bool mBreakcode;
				/*! Config file */
				lightOS::config::section<lightOS::config::key_value_pair> *mConfig;
		};
	}
}

/*@}*/
/*@}*/

#endif
