/*
lightOS server
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SERVER_PS2_CONTROLLER_HPP
#define SERVER_PS2_CONTROLLER_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup ps2 ps/2 */
/*@{*/

#include <cstdint>
#include <lightOS/lightOS.hpp>
#include "keyboard.hpp"
#include "mouse.hpp"

namespace server
{
	namespace ps2
	{
		enum
		{
			keyboard_irq				= 1,
			mouse_irq					= 12
		};
		enum
		{
			command_port				= 0x64,
			status_port					= 0x64,
			data_port					= 0x60
		};
		enum
		{
			timeout						= 0x10000
		};
		
		class controller
		{
			public:
				/*! The constructor */
				controller();
				/*! Initialize
				 *\return true, if successfull, false otherwise */
				bool init();
				/*! The loop */
				void loop();
				/*! Enable/disable the keyboard
				 *\param[in] b Enable the keyboard?
				 *\return true, if successfull, false otherwise */
				bool enable_keyboard(bool b);
				/*! Enable/disable the mouse
				 *\param[in] b Enable the mouse?
				 *\return true, if successfull, false otherwise */
				bool enable_mouse(bool b);
				/*! Send command to the keyboard
				 *\param[in] cmd the command
				 *\return true, if successfull, false otherwise */
				bool keyboard_command(uint8_t cmd);
				/*! Send command to the keyboard with parameter
				 *\param[in] cmd the command
				 *\param[in] param the parameter
				 *\return true, if successfull, false otherwise */
				bool keyboard_command(uint8_t cmd, uint8_t param);
				/*! Send command to the mouse
				 *\param[in] cmd the command
				 *\return true, if successfull, false otherwise */
				bool mouse_command(uint8_t cmd);
				/*! Send command to the mouse with parameter
				 *\param[in] cmd the command
				 *\param[in] param the parameter
				 *\return true, if successfull, false otherwise */
				bool mouse_command(uint8_t cmd, uint8_t param);
				/*! Wait for a mouse irq */
				void wait_mouse_irq();
				/*! Wait for a keyboard irq */
				void wait_keyboard_irq();
				/*! Get the return value of a successfully executed command
				 *\param[in,out] val the return value
				 *\return true, if successfull, false otherwise */
				bool get_return_value(uint8_t &val);
				/*! Get the return value of a successfully executed command
				 *\param[in,out] val the return value
				 *\return true, if successfull, false otherwise */
				bool get_return_value_aux(uint8_t &val);
				/*! The destructor */
				~controller();
			private:
				/*! Detect a keyboard
				 *\return true, if keyboard present, false otherwise */
				bool detect_keyboard();
				/*! Detect a mouse
				 *\return true, if mouse present, false otherwise */
				bool detect_mouse();
				/*! Send a command to the controller
				 *\param[in] cmd the command
				 *\return true, if successfull, false otherwise */
				bool command(uint8_t cmd);
				/*! Send a command with parameter to the controller
				 *\param[in] cmd the command
				 *\param[in] param the parameter
				 *\return true, if successfull, false otherwise */
				bool command(uint8_t cmd, uint8_t param);
				
				bool wait_ready();
				bool wait_data();
				bool wait_data_aux();
				
				libkernel::message_port mPort;
				keyboard *mKeyboard;
				mouse *mMouse;
				size_t mKeyboardVector;
				size_t mMouseVector;
		};
	}
}

/*@}*/
/*@}*/

#endif
