/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SERVER_FLOPPY_FLOPPY_HPP
#define SERVER_FLOPPY_FLOPPY_HPP

/*! \addtogroup server server */
/*@{*/
/*! \addtogroup floppy floppy */
/*@{*/

#include <lightOS/lightOS.hpp>
#include <libkernel/io_port.hpp>

namespace server
{
  class floppy
  {
    public:
      /*! The constructor */
      floppy(libkernel::message_port &msgPort);
      /*! Reset */
      void reset();
      /*! Turn the floppy motor on/off */
      void motor(unsigned char drive, bool on);
      /*! Select drive */
      void select(unsigned char drive);
      /*! Is motor on? */
      bool motor(unsigned char drive);
      /*! Send a byte to the controller
       *\param[in] byte the byte to send
       *\return true, if successfull, false otherwise  */
      void send(unsigned char byte);
      /*! Get a byte from the controller
       *\return the byte */
      unsigned char get();
      /*! interrupt sense */
      void interruptSense();
      /*! Recalibrate */
      bool recalibrate(unsigned char drive);
      /*! Seek */
      bool seek(unsigned char drive, unsigned long cylinder);
      /*! Read sector
       *\param[in] drive the drive number
       *\param[in] lba the linear block address
       *\return true, if successfull, false otherwise */
      bool read(unsigned char drive, unsigned long lba, void *data);
      /*! Write sector
       *\param[in] drive the drive number
       *\param[in] lba the linear block address
       *\return true, if successfull, false otherwise */
      bool write(unsigned char drive, unsigned long lba, void *data);
      /*! The destructor */
      ~floppy();

    private:
      enum io_ports : _LIBARCH_ioport_t
      {
        status_a    = 0x01,
        status_b    = 0x01,
        dor         = 0x02,
        main_status = 0x04,
        data_rate   = 0x04,
        data        = 0x05,
        dir         = 0x07
      };

      /** Floppy I/O ports */
      libkernel::io_port<io_ports, io_ports::status_a> m_io;

      /*! Wait for a floppy interrupt */
      bool waitIRQ();
      /*! The used message port */
      libkernel::message_port &mPort;
      /*! The virtual dma region */
      void *mVirtualDmaRegion;
      /*! The physical dma region */
      void *mPhysicalDmaRegion;
      /*! The selected drive */
      unsigned char mCurrentDrive;
      /*! The motor status */
      bool mMotorState[2];
      /*! Current cylinder */
      unsigned char mCylinder[2];
      /*! St0 */
      unsigned char mSt0[2];
  };
}

/*@}*/
/*@}*/

#endif
