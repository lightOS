/*
lightOS server
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <iostream>
#include <cstdlib>
#include <libarch/ioport.hpp>
#include "floppy.hpp"
using namespace lightOS;
using namespace std;
using namespace server;

using libkernel::message;

//TODO: Check if disk really inserted
//TODO: Check for diskchange
//TODO: Stop motor

static size_t int_vector = 0;

int main(int argc, char *argv[])
{
  libkernel::message_port Port;
  int_vector = Port.register_isa_irq(6, libkernel::irq_priority::floppy);
  if (int_vector == 0)
  {
    cerr << "floppy: could not register ISA-IRQ6" << endl;
    return -1;
  }

  waitForDevfs(Port);

  floppy Floppy(Port);
  file fd0(Port);
  fd0.create( "dev://floppy0",
        file::block,
        access::read | access::write,
        512,
        2880);

  message msg;
  while (Port.get(msg))
  {
    libkernel::shared_memory shm = msg.get_shared_memory();
    switch (msg.type())
    {
      case libkernel::message::ping:
      {
        message reply(msg.port(), libkernel::message::pong, 0, 0, 0);
        Port.send(reply);
      }break;
      case libkernel::message::event:
      {
        if (msg.param1() == libkernel::event::irq && msg.param2() == int_vector)
        {
          cerr << "floppy: spurious interrupt" << endl;
          Port.acknoledge_irq(int_vector);
        }
      }break;
      case libkernel::message::fs_read_file:
      {
        if (msg.attribute() == libkernel::shared_memory::none)
        {
          libkernel::shared_memory reply(msg.param2());
          for (unsigned int i=0;i < (msg.param2() / 512);i++)
          {
            if (Floppy.read(0, msg.param1() + i, reply.address<char>() + i * 512) == false)
            {
              cout << "floppy: read error" << endl;
            }
          }
          fd0.write(reply, msg.param1());
        }
      }break;
      case libkernel::message::fs_write_file:
      {
        if (msg.attribute() == libkernel::shared_memory::transfer_ownership)
        {
          for (unsigned int i=0;i < (shm.size() / 512);i++)
          {
            Floppy.write(0, msg.param1()+i, shm.address<char>() + i * 512);
          }
          fd0.read<void>(0, msg.param1(), shm.size());
        }
      }break;
    }
  }
  return 0;
}

floppy::floppy(libkernel::message_port &msgPort)
  : m_io(), mPort(msgPort)
{
  if (m_io.allocate(0x3F0, 0x07) == false)
  {
    // TODO: Throw an exception
  }

  mVirtualDmaRegion = allocate_region(512, memory::below_1mb | memory::continuous);
  mPhysicalDmaRegion = get_physical_address(mVirtualDmaRegion);
  
  mCurrentDrive = 0;
  mMotorState[0] = false;
  mMotorState[1] = false;
  mCylinder[0] = 255;
  mCylinder[1] = 255;
  
  reset();
}

bool floppy::waitIRQ()
{
  //TODO static bool resetting = false;
  message msg(libkernel::message_port::kernel, libkernel::message::event, libkernel::event::irq, int_vector);
  mPort.wait(msg);
  mPort.acknoledge_irq(int_vector);
  /*unsigned int timeout = 0;
  while (mPort.peek(msg) == false)
  {
    if (timeout++ == 1000)
    {
      if (resetting == false){resetting = true;reset();resetting = false;}
      return false;
    }
    wait(1);
  }*/
  return true;
}

void floppy::reset()
{
  // Reset
  m_io.write8(0x00, io_ports::dor);
  wait(20);
  m_io.write8(0x00, io_ports::data_rate);
  m_io.write8(0x0C, io_ports::dor);
  waitIRQ();
  interruptSense();
  
  // Specify
  send(0x03);
  send(0xDF);
  send(0x02);
  
  // Recalibrate
  recalibrate(0);
}

void floppy::select(unsigned char drive)
{
  if (mCurrentDrive == drive)
    return;

  uint8_t tmp = m_io.read8(io_ports::dor);
  m_io.write8((tmp & (~0x03)) | drive, io_ports::dor);
  mCurrentDrive = drive;
}

void floppy::motor(unsigned char drive, bool on)
{
  if (mMotorState[drive] == on)
    return;

  select(drive);
  uint8_t tmp = m_io.read8(io_ports::dor);
  if (on == true)
    m_io.write8((tmp & 0x0F) | (0x10 << drive), io_ports::dor);
  else
    m_io.write8(tmp & ~(0x10 << drive), io_ports::dor);
  wait(500);
  mCurrentDrive = drive;
  mMotorState[drive] = on;
  mCylinder[drive] = 255;
}

bool floppy::motor(unsigned char drive)
{
  return mMotorState[drive];
}

void floppy::send(unsigned char byte)
{
  for (unsigned int i=0;i < 100000;i++)
    if ((m_io.read8(io_ports::main_status) & 0xC0) == 0x80)
    {
      m_io.write8(byte, io_ports::data);
      return;
    }

  cerr << "floppy: send failed" << endl;
}

unsigned char floppy::get()
{
  for (unsigned int i=0;i < 100000;i++)
    if ((m_io.read8(io_ports::main_status) & 0xD0) == 0xD0)
      return m_io.read8(io_ports::data);

  cerr << "floppy: get failed" << endl;
  return 0;
}

void floppy::interruptSense()
{
  send(0x08);
  mSt0[mCurrentDrive] = get();
  mCylinder[mCurrentDrive] = get();
}

bool floppy::recalibrate(unsigned char drive)
{
  select(drive);
  motor(drive, true);
  mCylinder[drive] = 255;
  unsigned int i = 0;
  while ((i < 5) && (mCylinder[drive] != 0))
  {
    send(0x07);
    send(drive);
    waitIRQ();
    interruptSense();
    ++i;
  }
  return (i < 5);
}

bool floppy::seek(unsigned char drive, unsigned long cylinder)
{
  if (mCylinder[drive] == cylinder)return true;
  
  select(drive);
  motor(drive, true);
  
  send(0x0F);
  send(drive);
  send(cylinder);
  if (waitIRQ() == false)return false;
  interruptSense();
  
  wait(15);
  
  if (mSt0[drive] != 0x20)return false;
  return true;
}

bool floppy::read(unsigned char drive, unsigned long lba, void *data)
{
  for (unsigned int i=0;i < 3;i++)
  {
    //cout << "attemp: " << i << endl;
    select(drive);
    motor(drive, true);
    
    // Calculate the CHS values
    unsigned long sector = (lba % 18) + 1;
    unsigned long cylinder = (lba / 18) / 2;
    unsigned long head = (lba / 18) % 2;
    
    if (seek(drive, cylinder) != true)continue;
    
    // Set the DMA channel up
    setupDMAChannel(2, mPhysicalDmaRegion, 512, 0x44);
    
    // Send Command bytes
    send(0x46);
    send((head << 2) | drive);
    send(cylinder);
    send(head);
    send(sector);
    send(2);
    send(18);
    send(27);
    send(0);
    
    // Wait for interrupt
    if (waitIRQ() == false)continue;
    unsigned char st0 = get();
    get(); // St1
    get(); // St2
    get(); // cylinder
    get(); // head
    get(); // sector
    get(); // size
    
    if ((st0 & 0xC0) == 0)
    {
      memcpy(data, mVirtualDmaRegion, 512);
      return true;
    }
  }
  return false;
}

bool floppy::write(unsigned char drive, unsigned long lba, void *data)
{
  for (unsigned int i=0;i < 3;i++)
  {
    select(drive);
    motor(drive, true);
    
    // Calculate the CHS values
    unsigned long sector = (lba % 18) + 1;
    unsigned long cylinder = (lba / 18) / 2;
    unsigned long head = (lba / 18) % 2;
    
    if (seek(drive, cylinder) != true)continue;
    
    memcpy(mVirtualDmaRegion, data, 512);
    
    // Set the DMA channel up
    setupDMAChannel(2, mPhysicalDmaRegion, 512, 0x48);
    
    // Send Command bytes
    send(0x45);
    send((head << 2) | drive);
    send(cylinder);
    send(head);
    send(sector);
    send(2);
    send(18);
    send(27);
    send(0);
    
    // Wait for interrupt
    waitIRQ();
    unsigned char st0 = get();
    get(); // St1
    get(); // St2
    get(); // cylinder
    get(); // head
    get(); // sector
    get(); // size
    
    if ((st0 & 0xC0) == 0)return true;
  }
  return false;
}

floppy::~floppy()
{
  for (unsigned int i=0;i < 2;i++)
    motor(i, false);
  
  free_region(mVirtualDmaRegion);
}
