/*
lightOS supc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SUPCPP_CPPTYPEINFO_HPP
#define SUPCPP_CPPTYPEINFO_HPP

/*! \addtogroup supcpp supc++ */
/*@{*/

#include <cstddef>
#include <typeinfo>

namespace __cxxabiv1
{
	class __fundamental_type_info : public std::type_info
	{
		protected:
			//! The copy-constructor
			__fundamental_type_info(const __fundamental_type_info &rhs);
			//! The = operator
			__fundamental_type_info &operator = (const __fundamental_type_info &rhs);
	};
	
	class __array_type_info : public std::type_info
	{
		protected:
			//! The copy-constructor
			__array_type_info(const __array_type_info &rhs);
			//! The = operator
			__array_type_info &operator = (const __array_type_info &rhs);
	};
	
	class __function_type_info : public std::type_info
	{
		protected:
			//! The copy-constructor
			__function_type_info(const __function_type_info &rhs);
			//! The = operator
			__function_type_info &operator = (const __function_type_info &rhs);
	};
	
	class __enum_type_info : public std::type_info
	{
		protected:
			//! The copy-constructor
			__enum_type_info(const __enum_type_info &rhs);
			//! The = operator
			__enum_type_info &operator = (const __enum_type_info &rhs);
	};
	
	class __class_type_info : public std::type_info
	{
		protected:
			//! The copy-constructor
			__class_type_info(const __class_type_info &rhs);
			//! The = operator
			__class_type_info &operator = (const __class_type_info &rhs);
			//! The destrutor
	};
	
	class __si_class_type_info : public __class_type_info
	{
		public:
			const __class_type_info *__base_type;
		protected:
			//! The copy-constructor
			__si_class_type_info(const __si_class_type_info &rhs);
			//! The = operator
			__si_class_type_info &operator = (const __si_class_type_info &rhs);
	};
	
	struct __base_class_type_info
	{
		public:
			const __class_type_info *__base_type;
			long __offset_flags;
			
			enum __offset_flags_masks
			{
				__virtual_mask	= 0x1,
				__public_mask	= 0x2,
				__offset_shift	= 8
			};
	};
	
	class __vmi_class_type_info : public __class_type_info
	{
		public:
			unsigned int __flags;
			unsigned int __base_count;
			__base_class_type_info __base_info[1];
			
			enum __flags_masks
			{
				__non_diamond_repeat_mask	= 0x1,
				__diamond_shaped_mask		= 0x2
			};
		protected:
			//! The copy-constructor
			__vmi_class_type_info(const __vmi_class_type_info &rhs);
			//! The = operator
			__vmi_class_type_info &operator = (const __vmi_class_type_info &rhs);
	};
	
	class __pbase_type_info : public std::type_info
	{
		public:
			unsigned int __flags;
			const std::type_info *__pointee;
			
			enum __masks
			{
				__const_mask = 0x1,
				__volatile_mask = 0x2,
				__restrict_mask = 0x4,
				__incomplete_mask = 0x8,
				__incomplete_class_mask = 0x10
			};
	};
	
	class __pointer_type_info : public __pbase_type_info
	{
		protected:
			//! The copy-constructor
			__pointer_type_info(const __pointer_type_info &rhs);
			//! The = operator
			__pointer_type_info &operator = (const __pointer_type_info &rhs);
	};
}

/* ! The dynamic_cast operator */
extern "C" void* __dynamic_cast (	const void *sub,
									const __cxxabiv1::__class_type_info *src,
									const __cxxabiv1::__class_type_info *dst,
									ptrdiff_t src2dst_offset);

/*@}*/

#endif
