/*
lightOS supc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include "c++abi.hpp"
using namespace __cxxabiv1;

unsigned int iObject = 0;
finalizeObject object[32];
dso_handle __dso_handle = reinterpret_cast<void*>(&iObject);

int __cxxabiv1::__cxa_atexit(	void (*f)(void *),
								void *p,
								dso_handle d)
{
	if (iObject >= 32)return -1;
	object[iObject].function = f;
	object[iObject].param = p;
	object[iObject].dso = d;
	++iObject;
	return 0;
}
void __cxxabiv1::__cxa_finalize(dso_handle d)
{
	unsigned int i = iObject;
	for (;i > 0;i--)
	{
		--iObject;
		object[iObject].function(object[iObject].param);
	}
}
