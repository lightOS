/*
lightOS supc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include "c++abi.hpp"
#include "c++typeinfo.hpp"
using namespace std;
using namespace __cxxabiv1;

type_info::type_info(const type_info &rhs)
{
	_name = rhs._name;
}

type_info::~type_info(){}

__fundamental_type_info::__fundamental_type_info(const __fundamental_type_info &rhs)
	: type_info(rhs){}

__array_type_info::__array_type_info(const __array_type_info &rhs)
	: type_info(rhs){}

__function_type_info::__function_type_info(const __function_type_info &rhs)
	: type_info(rhs){}

__enum_type_info::__enum_type_info(const __enum_type_info &rhs)
	: type_info(rhs){}

__class_type_info::__class_type_info(const __class_type_info &rhs)
	: type_info(rhs){}

__si_class_type_info::__si_class_type_info(const __si_class_type_info &rhs)
	: __class_type_info(rhs){}

__vmi_class_type_info::__vmi_class_type_info(const __vmi_class_type_info &rhs)
	: __class_type_info(rhs){}

__pointer_type_info::__pointer_type_info(const __pointer_type_info &rhs)
	: __pbase_type_info(rhs){}

#include <iostream>
void *__dynamic_cast (	const void *sub,
						const __class_type_info *src,
						const __class_type_info *dst,
						ptrdiff_t src2dst_offset)
{
	cerr << "supc++: dynamic_cast used" << endl;
	cerr << "        sub = 0x" << hex << reinterpret_cast<size_t>(sub) << endl;
	cerr << "        src = 0x" << hex << reinterpret_cast<size_t>(src) << endl;
	cerr << "        dst = 0x" << hex << reinterpret_cast<size_t>(dst) << endl;
	cerr << "        src2dst_offset = 0x" << hex << static_cast<size_t>(src2dst_offset) << endl;
	return 0;
}
