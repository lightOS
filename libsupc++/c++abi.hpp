/*
lightOS supc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef SUPCPP_CPPABI_HPP
#define SUPCPP_CPPABI_HPP

/*! \addtogroup supcpp supc++ */
/*@{*/

/*! Typedef the dso_handle */
typedef void *dso_handle;
/*! This dso_handle */
extern dso_handle __dso_handle;

#include <new>
#include <cstddef>

namespace __cxxabiv1
{
  typedef void (*constructor)();

  extern "C"
  {
    /*! Support function for pure virtual functions */
    void __cxa_pure_virtual();
    /*! Support function for global/static object construction */
    void __cxa_initialize(constructor *begin,
                          constructor *end);
    /*! Support function for global/static object construction */
    void __cxa_initialize_lib();
    /*! Register a function to be called at destruction
     *\param[in] f pointer to the funtion
     *\param[in] p additional parameter
     *\param[in] d the dso handle
     *\return 0, if successfull */
    int __cxa_atexit(void (*f)(void *), void *p, dso_handle d);
    /*! Support function for global/static object destruction
     *\param[in] d the dso handle */
    void __cxa_finalize(dso_handle d);
  }

  /*! Saves information about objects that must be destroyed at exit */
  struct finalizeObject
  {
    /*! Function to call */
    void (*function)(void *);
    /*! Parameter */
    void *param;
    /*! Dynamic shared object */
    void *dso;
  };
}

/*@}*/

#endif
