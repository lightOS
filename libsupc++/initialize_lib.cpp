/*
lightOS supc++
Copyright (C) 2006-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <cstddef>
#include "c++abi.hpp"
using namespace std;
using namespace __cxxabiv1;

/*! Beginning of the .ctors section */
extern void (*__lib_ctors_start__)();
/*! End of the .ctors section */
extern void (*__lib_ctors_end__)();

// TODO: rework
void __cxxabiv1::__cxa_initialize_lib()
{
  void (**Constructor)() = &__lib_ctors_start__;

  size_t i = (reinterpret_cast<size_t>(&__lib_ctors_end__) -
        reinterpret_cast<size_t>(&__lib_ctors_start__)) / sizeof(void *);
  for (;i > 0;i--)
  {
    (*Constructor)();
    ++Constructor;
  }
}
