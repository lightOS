/*
lightOS libarch
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBARCH_SETJMP_H
#define LIGHTOS_LIBARCH_SETJMP_H

/*! \addtogroup libarch libarch */
/*@{*/

#include <libarch/arch/setjmp.h>

#ifdef __cplusplus
  extern "C"
  {
#endif

    /** Standard C setjmp */
    int _LIBARCH_setjmp(_LIBARCH_jmp_buf* env);

    /** Standard C longjmp */
    void _LIBARCH_longjmp(_LIBARCH_jmp_buf* env,
                          int val);

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
