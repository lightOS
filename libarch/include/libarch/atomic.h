/*
lightOS libarch
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBARCH_ATOMIC_H
#define LIGHTOS_LIBARCH_ATOMIC_H

/*! \addtogroup libarch libarch */
/*@{*/

#ifdef _LIGHTOS_SMP
  #define LOCK_PREFIX "lock"
#else
  #define LOCK_PREFIX
#endif

#include <stdbool.h> // NOTE: provided by the gcc crosscompiler
#include <libarch/type.h>

#ifdef __cplusplus
  extern "C"
  {
#endif

    void atomic_add(_LIBARCH_atomic_int_t *Atom,
                    _LIBARCH_atomic_int_t i);
    void atomic_sub(_LIBARCH_atomic_int_t *Atom,
                    _LIBARCH_atomic_int_t i);
    bool atomic_sub_and_test(_LIBARCH_atomic_int_t *Atom,
                             _LIBARCH_atomic_int_t i);
    bool atomic_compare_and_exchange(_LIBARCH_atomic_int_t *Atom,
                                     _LIBARCH_atomic_int_t cmpval,
                                     _LIBARCH_atomic_int_t newval);
    void atomic_inc(_LIBARCH_atomic_int_t *Atom);
    bool atomic_inc_and_test(_LIBARCH_atomic_int_t *Atom);
    void atomic_dec(_LIBARCH_atomic_int_t *Atom);
    bool atomic_dec_and_test(_LIBARCH_atomic_int_t *Atom);

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
