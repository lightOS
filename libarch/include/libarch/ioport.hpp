/*
lightOS libarch
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBARCH_IOPORT_HPP
#define LIGHTOS_LIBARCH_IOPORT_HPP

/*! \addtogroup libarch libarch */
/*@{*/

#include <libarch/arch/ioport.h>

#if defined(_LIBARCH_HAS_IOPORT)



namespace libarch
{
  /** Typedef ioport_t */
  typedef _LIBARCH_ioport_t ioport_t;


  /** Read 8bits from an I/O port
   *\param[in] port the I/O port to read from
   *\return the value that has been read */
  inline _LIBARCH_uint8_t in8(libarch::ioport_t port);

  /** Read 16bits from an I/O port
   *\param[in] port the I/O port to read from
   *\return the value that has been read */
  inline _LIBARCH_uint16_t in16(libarch::ioport_t port);

  /** Read 32bits from an I/O port
   *\param[in] port the I/O port to read from
   *\return the value that has been read */
  inline _LIBARCH_uint32_t in32(libarch::ioport_t port);


  /** Write 8bits to an I/O port
   *\param[in] port  the I/O port to write to
   *\param[in] value the value to write to the port */
  inline void out8(libarch::ioport_t port,
                   _LIBARCH_uint8_t value);

  /** Write 16bits to an I/O port
   *\param[in] port  the I/O port to write to
   *\param[in] value the value to write to the port */
  inline void out16(libarch::ioport_t port,
                    _LIBARCH_uint16_t value);

  /** Write 32bits to an I/O port
   *\param[in] port  the I/O port to write to
   *\param[in] value the value to write to the port */
  inline void out32(libarch::ioport_t port,
                    _LIBARCH_uint32_t value);


  #if defined(_LIBARCH_HAS_IOPORT64)

    /** Read 64bits from an I/O port
    *\param[in] port the I/O port to read from
    *\return the value that has been read */
    inline _LIBARCH_uint64_t in64(libarch::ioport_t port);


    /** Write 64bits to an I/O port
    *\param[in] port  the I/O port to write to
    *\param[in] value the value to write to the port */
    inline void out64(libarch::ioport_t port,
                      _LIBARCH_uint64_t value);

  #endif
}



/*
 * Implementation
 */

_LIBARCH_uint8_t libarch::in8(libarch::ioport_t port)
{
  return _LIBARCH_in8(port);
}

_LIBARCH_uint16_t libarch::in16(libarch::ioport_t port)
{
  return _LIBARCH_in16(port);
}

_LIBARCH_uint32_t libarch::in32(libarch::ioport_t port)
{
  return _LIBARCH_in32(port);
}

void libarch::out8(libarch::ioport_t port,
                   _LIBARCH_uint8_t value)
{
  _LIBARCH_out8(port,
                value);
}

void libarch::out16(libarch::ioport_t port,
                    _LIBARCH_uint16_t value)
{
  _LIBARCH_out16(port,
                 value);
}

void libarch::out32(libarch::ioport_t port,
                    _LIBARCH_uint32_t value)
{
  _LIBARCH_out32(port,
                 value);
}

#if defined(_LIBARCH_HAS_IOPORT64)

  _LIBARCH_uint64_t libarch::in64(libarch::ioport_t port)
  {
    return _LIBARCH_in64(port);
  }

  void libarch::out64(libarch::ioport_t port,
                      _LIBARCH_uint64_t value)
  {
    _LIBARCH_out64(port,
                   value);
  }

#endif



#endif

/*@}*/

#endif
