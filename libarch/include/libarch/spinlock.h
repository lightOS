/*
lightOS library
Copyright (C) 2007-2008 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBARCH_SPINLOCK_H
#define LIGHTOS_LIBARCH_SPINLOCK_H

/*! \addtogroup libarch libarch */
/*@{*/

#include <libarch/atomic.h>

// TODO: Rework the whole file

#ifdef _LIGHTOS_SMP
  #ifndef DECLARE_SPINLOCK
    #define DECLARE_SPINLOCK(name) atom_t name = 1
  #endif
  #ifndef LOCK
    #define LOCK(name) while (!atomic_compare_and_exchange(&(name), 1, 0)){}
  #endif
  #ifndef UNLOCK
    #define UNLOCK(name) (name) = 1
  #endif
#else
  #ifndef DECLARE_SPINLOCK
    #define DECLARE_SPINLOCK(name)
  #endif
  #ifndef LOCK
    #define LOCK(name)
  #endif
  #ifndef UNLOCK
    #define UNLOCK(name)
  #endif
#endif

/*@}*/

#endif
