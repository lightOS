/*
lightOS libarch
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBARCH_EXCLUSIVE_POINTER_HPP
#define LIGHTOS_LIBARCH_EXCLUSIVE_POINTER_HPP

/*! \addtogroup libarch libarch */
/*@{*/

namespace libarch
{
  /* Forward declaration */
  template<typename T,
           typename Lock>
  class exclusive_pointer;

  namespace helper
  {
    /** Reference to a exclusive_pointer */
    template<typename T,
             typename Lock>
    struct exclusive_pointer_ref
    {
      /** Construct from an reference to an exclusive_pointer
       *\param[in] ref the reference to the exclusive_pointer */
      exclusive_pointer_ref(exclusive_pointer<T, Lock>& ref)
        : m_ref(ref){}

      /** Destructor */
      ~exclusive_pointer_ref(){}

      /** Reference of the exclusive_pointer */
      exclusive_pointer<T, Lock>& m_ref;
    };



    /** Create an exclusive_pointer with automatic type deduction
     *\param[in] p    pointer to the ressource
     *\param[in] lock reference of the lock */
    template<typename T,
             typename Lock>
    exclusive_pointer<T, Lock> create_exclusive_pointer(T* p,
                                                        Lock& lock)
    {
      return exclusive_pointer<T, Lock>(p, &lock);
    }

    /** Create an exclusive_pointer with automatic type deduction
     *\param[in] p    pointer to the ressource */
    template<typename T>
    exclusive_pointer<T, void> create_exclusive_pointer(T* p)
    {
      return exclusive_pointer<T, void>(p);
    }
  }

  /** The exclusive_pointer holds a pointer to a ressource, guarded by a
   *  lock
   *\param[in] T    the type of the ressource
   *\param[in] Lock the type of the lock */
  template<typename T,
           typename Lock>
  class exclusive_pointer
  {
    public:

      /** Default constructor */
      exclusive_pointer();

      /** Construct from a pointer and a given lock. Locks the ressource if it is
       *  not the null pointer and if the lock is not the null pointer
       *\param[in] p    pointer to the ressource or the null pointer
       *\param[in] lock pointer to the lock or the null pointer */
      exclusive_pointer(T* p,
                        Lock* lock);

      /** Copy constructor
       *\note x has lost its pointer after the call
       *\param[in] x reference to the object that should be copied */
      exclusive_pointer(exclusive_pointer& x);

      /** Construct from an exclusive_pointer_ref
       *\note this function is needed to implement copying of an exclusive_pointer
       *      properly. The copy-construtor does not do the job, because non-const
       *      reference do not bind temporary objects. */
      exclusive_pointer(helper::exclusive_pointer_ref<T, Lock> ref);

      /** The destructor frees the lock */
      ~exclusive_pointer();

      /** The -> operator */
      T* operator ->();

      /** The dereference operator */
      T* operator *();

      /** Conversion to exclusive_pointer_ref operator needed for correct copying */
      operator helper::exclusive_pointer_ref<T, Lock>();


    private:
      /** Pointer to the ressource */
      T*    m_p;
      /** Pointer to the lock */
      Lock* m_lock;
  };

  /** The exclusive_pointer specialisation for void holds a ressource, without
   *  guarding it by a lock
   *\param[in] T the type of the ressource */
  template<typename T>
  class exclusive_pointer<T, void>
  {
    public:
      /** Default constructor */
      exclusive_pointer();

      /** Construct from a pointer to the ressource
       *\param[in] p pointer to the ressource */
      exclusive_pointer(T* p);

      /** Copy-constructor
       *\note x has lost its pointer after the call
       *\param[in] x reference to the object that should be copied */
      exclusive_pointer(exclusive_pointer& x);

      /** Construct from an exclusive_pointer_ref
       *\note this function is needed to implement copying of an exclusive_pointer
       *      properly. The copy-construtor does not do the job, because non-const
       *      reference do not bind temporary objects. */
      exclusive_pointer(helper::exclusive_pointer_ref<T, void> ref);

      /** The destructor */
      ~exclusive_pointer();

      /** The -> operator */
      T* operator ->();

      /** The dereference operator */
      T* operator *();

      /** Conversion to exclusive_pointer_ref operator needed for correct copying */
      operator helper::exclusive_pointer_ref<T, void>();

    private:
      /** The pointer to the ressource */
      T*    m_p;
  };



  /*
   * Implementation of libarch::exclusive_pointer
   */

  template<typename T,
           typename Lock>
  exclusive_pointer<T, Lock>::exclusive_pointer()
    : m_p(), m_lock()
  {
  }

  template<typename T,
           typename Lock>
  exclusive_pointer<T, Lock>::exclusive_pointer(T* p,
                                                Lock* lock)
    : m_p(p), m_lock(lock)
  {
    if (m_p != 0 && m_lock != 0)
      m_lock->lock();
  }

  template<typename T,
           typename Lock>
  exclusive_pointer<T, Lock>::exclusive_pointer(exclusive_pointer& x)
    : m_p(x.p), m_lock(x.m_lock)
  {
    x.m_p = 0;
  }

  template<typename T,
           typename Lock>
  exclusive_pointer<T, Lock>::exclusive_pointer(helper::exclusive_pointer_ref<T, Lock> ref)
    : m_p(ref.m_ref.m_p), m_lock(ref.m_ref.m_lock)
  {
    ref.m_ref.m_p = 0;
  }

  template<typename T,
           typename Lock>
  exclusive_pointer<T, Lock>::~exclusive_pointer()
  {
    if (m_p != 0 && m_lock != 0)
      m_lock->unlock();
  }

  template<typename T,
           typename Lock>
  T* exclusive_pointer<T, Lock>::operator ->()
  {
    return m_p;
  }

  template<typename T,
           typename Lock>
  T* exclusive_pointer<T, Lock>::operator *()
  {
    return m_p;
  }

  template<typename T,
           typename Lock>
  exclusive_pointer<T, Lock>::operator helper::exclusive_pointer_ref<T, Lock>()
  {
    return helper::exclusive_pointer_ref<T, Lock>(*this);
  }

  template<typename T>
  exclusive_pointer<T, void>::exclusive_pointer()
    : m_p()
  {
  }

  template<typename T>
  exclusive_pointer<T, void>::exclusive_pointer(T* p)
    : m_p(p)
  {
  }

  template<typename T>
  exclusive_pointer<T, void>::exclusive_pointer(exclusive_pointer& x)
    : m_p(x.m_p)
  {
    x.m_p = 0;
  }

  template<typename T>
  exclusive_pointer<T, void>::exclusive_pointer(helper::exclusive_pointer_ref<T, void> ref)
    : m_p(ref.m_ref.m_p)
  {
    ref.m_ref.m_p = 0;
  }

  template<typename T>
  exclusive_pointer<T, void>::~exclusive_pointer()
  {
  }

  template<typename T>
  T* exclusive_pointer<T, void>::operator ->()
  {
    return m_p;
  }

  template<typename T>
  T* exclusive_pointer<T, void>::operator *()
  {
    return m_p;
  }

  template<typename T>
  exclusive_pointer<T, void>::operator helper::exclusive_pointer_ref<T, void>()
  {
    return helper::exclusive_pointer_ref<T, void>(*this);
  }
}



/*
 * Helper macros
 */

#if defined(_LIGHTOS_MULTIPROCESSOR)
  #define EXCLUSIVE_POINTER(pointer, lock)                 libarch::helper::create_exclusive_pointer(pointer, lock)
#else
  #define EXCLUSIVE_POINTER(pointer, lock)                 libarch::helper::create_exclusive_pointer(pointer)
#endif



/*@}*/

#endif
