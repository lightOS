/*
lightOS libarch
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBARCH_X86_IOPORT_H
#define LIGHTOS_LIBARCH_X86_IOPORT_H

#include <libarch/type.h>

/*! \addtogroup libarch libarch */
/*@{*/
/*! \addtogroup libarch_x86_shared x86 shared */
/*@{*/



/*
 * Macros
 */

/** x86 and x86-64 have I/O ports */
#define _LIBARCH_HAS_IOPORT                      1
/* x86 and x86-64 do not support 64bit I/O port operations */
#undef  _LIBARCH_HAS_IOPORT64



/*
 * Types
 */

/** Typedef _LIBARCH_ioport_t, the type that can hold an identifier of
 *  and I/O port */
typedef _LIBARCH_uint16_t _LIBARCH_ioport_t;



/*
 * Functions
 */

/** Read 8bits from an I/O port
 *\param[in] port the I/O port to read from
 *\return the value that has been read */
inline _LIBARCH_uint8_t _LIBARCH_in8(_LIBARCH_ioport_t port)
{
  _LIBARCH_uint8_t value;
  asm volatile("inb %%dx, %%al"
               : "=a" (value)
               : "d" (port));
  return value;
}

/** Read 16bits from an I/O port
 *\param[in] port the I/O port to read from
 *\return the value that has been read */
inline _LIBARCH_uint16_t _LIBARCH_in16(_LIBARCH_ioport_t port)
{
  _LIBARCH_uint16_t value;
  asm volatile("inw %%dx, %%ax"
               : "=a" (value)
               : "d" (port));
  return value;
}

/** Read 32bits from an I/O port
 *\param[in] port the I/O port to read from
 *\return the value that has been read */
inline _LIBARCH_uint32_t _LIBARCH_in32(_LIBARCH_ioport_t port)
{
  _LIBARCH_uint32_t value;
  asm volatile("in %%dx, %%eax"
               : "=a" (value)
               : "d" (port));
  return value;
}

/** Write 8bits to an I/O port
 *\param[in] port  the I/O port to write to
 *\param[in] value the value to write to the port */
inline void _LIBARCH_out8(_LIBARCH_ioport_t port,
                          _LIBARCH_uint8_t value)
{
  asm volatile("outb %%al, %%dx"
               :: "d" (port), "a" (value));
}

/** Write 16bits to an I/O port
 *\param[in] port  the I/O port to write to
 *\param[in] value the value to write to the port */
inline void _LIBARCH_out16(_LIBARCH_ioport_t port,
                           _LIBARCH_uint16_t value)
{
  asm volatile("outw %%ax, %%dx"
               :: "d" (port), "a" (value));
}

/** Write 32bits to an I/O port
 *\param[in] port  the I/O port to write to
 *\param[in] value the value to write to the port */
inline void _LIBARCH_out32(_LIBARCH_ioport_t port,
                           _LIBARCH_uint32_t value)
{
  asm volatile("out %%eax, %%dx"
               :: "d" (port), "a" (value));
}



/*@}*/
/*@}*/

#endif
