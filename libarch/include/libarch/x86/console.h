/*
lightOS libarch
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBARCH_X86_CONSOLE_H
#define LIGHTOS_LIBARCH_X86_CONSOLE_H

/*! \addtogroup libarch libarch */
/*@{*/
/*! \addtogroup libarch_x86_shared x86 shared */
/*@{*/



/*
 * Macros - console colors
 */

/** Black */
#define _LIBARCH_CONSOLE_BLACK                   0x00
/** Blue */
#define _LIBARCH_CONSOLE_BLUE                    0x01
/** Green */
#define _LIBARCH_CONSOLE_GREEN                   0x02
/** Cyan */
#define _LIBARCH_CONSOLE_CYAN                    0x03
/** Red */
#define _LIBARCH_CONSOLE_RED                     0x04
/** Magenta */
#define _LIBARCH_CONSOLE_MAGENTA                 0x05
/** Brown */
#define _LIBARCH_CONSOLE_BROWN                   0x06
/** Light gray */
#define _LIBARCH_CONSOLE_LIGHT_GRAY              0x07
/** Dark gray */
#define _LIBARCH_CONSOLE_DARK_GRAY               0x08
/** Light blue */
#define _LIBARCH_CONSOLE_LIGHT_BLUE              0x09
/** Light green */
#define _LIBARCH_CONSOLE_LIGHT_GREEN             0x0A
/** Light cyan */
#define _LIBARCH_CONSOLE_LIGHT_CYAN              0x0B
/** Light red */
#define _LIBARCH_CONSOLE_LIGHT_RED               0x0C
/** Light magenta */
#define _LIBARCH_CONSOLE_LIGHT_MAGENTA           0x0D
/** Light yellow */
#define _LIBARCH_CONSOLE_YELLOW                  0x0E
/** White */
#define _LIBARCH_CONSOLE_WHITE                   0x0F



/*@}*/
/*@}*/

#endif
