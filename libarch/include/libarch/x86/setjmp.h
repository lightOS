/*
lightOS libarch
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBARCH_X86_SETJMP_H
#define LIGHTOS_LIBARCH_X86_SETJMP_H

/*! \addtogroup libarch libarch */
/*@{*/
/*! \addtogroup libarch_x86 x86 */
/*@{*/



/*
 * Types
 */

/** Structure holding the information for setjmp and longjmp */
typedef struct
{
  /** eax cpu register */
  unsigned int ebx; // 0
  /** esi cpu register */
  unsigned int esi; // 4
  /** edi cpu register */
  unsigned int edi; // 8
  /** ebp cpu register */
  unsigned int ebp; // 12
  /** esp cpu register */
  unsigned int esp; // 16
  /** eip cpu register */
  unsigned int eip; // 20
} _LIBARCH_jmp_buf;



/*@}*/

#endif
