/*
lightOS libarch
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBARCH_X86_64_TYPE_H
#define LIGHTOS_LIBARCH_X86_64_TYPE_H

/*! \addtogroup libarch libarch */
/*@{*/
/*! \addtogroup libarch_x86_64 x86-64 */
/*@{*/



/*
 * Macros
 */

/** The char datatype is signed */
#define _LIBARCH_CHAR_SIGNED       1
/** The number of bits the char datatype consists of (definition of one byte) */
#define _LIBARCH_CHAR_BIT          8
/** The size in bytes of the char datatype */
#define _LIBARCH_CHAR_SIZE         1
/** The size in bytes of the short datatype */
#define _LIBARCH_SHORT_SIZE        2
/** The size in bytes of the int datatype */
#define _LIBARCH_INT_SIZE          4
/** The size in bytes of the long datatype */
#define _LIBARCH_LONG_SIZE         8
/** The size in bytes of the long long datatype */
#define _LIBARCH_LONGLONG_SIZE     8
/** The requested size for intmax_t */
#define _LIBARCH_INTMAX_SIZE       8
/** The requested size for intmax_t */
#define _LIBARCH_SIZE              8
/** The size of any pointer */
#define _LIBARCH_POINTER_SIZE      8
/** The size of any pointer */
#define _LIBARCH_ATOMIC_SIZE       8
/** The size of any pointer */
#define _LIBARCH_PAGESIZE          4096



/*@}*/
/*@}*/

#endif
