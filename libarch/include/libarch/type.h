/*
lightOS libarch
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBARCH_TYPE_H
#define LIGHTOS_LIBARCH_TYPE_H

#include <limits.h> // NOTE: provided by the gcc crosscompiler
#include <libarch/arch/type.h>

/*! \addtogroup libarch libarch */
/*@{*/

/* Some checks */
#ifdef __cplusplus
  static_assert(_LIBARCH_CHAR_SIZE == sizeof(char), "libarch: _LIBARCH_CHAR_SIZE wrong");
  static_assert(_LIBARCH_SHORT_SIZE == sizeof(short), "libarch: _LIBARCH_SHORT_SIZE wrong");
  static_assert(_LIBARCH_INT_SIZE == sizeof(int), "libarch: _LIBARCH_INT_SIZE wrong");
  static_assert(_LIBARCH_LONG_SIZE == sizeof(long), "libarch: _LIBARCH_LONG_SIZE wrong");
  static_assert(_LIBARCH_LONGLONG_SIZE == sizeof(long long), "libarch: _LIBARCH_LONGLONG_SIZE wrong");
  static_assert(_LIBARCH_POINTER_SIZE == sizeof(void*), "libarch: _LIBARCH_POINTER_SIZE wrong");
#endif

/* The types */
#if _LIBARCH_CHAR_BIT == 8
  /* Define _LIBARCH_(u)int8_t */
  #if _LIBARCH_CHAR_SIZE == 1
    typedef char                       _LIBARCH_int8_t;
    typedef unsigned char              _LIBARCH_uint8_t;
    #define _LIBARCH_INT8_MIN          CHAR_MIN
    #define _LIBARCH_INT8_MAX          CHAR_MAX
    #define _LIBARCH_UINT8_MAX         UCHAR_MAX
  #else
    #error No _LIBARCH_int8_t/uint8_t type
  #endif

  /* Define _LIBARCH_(u)int16_t */
  #if _LIBARCH_SHORT_SIZE == 2
    typedef short                      _LIBARCH_int16_t;
    typedef unsigned short             _LIBARCH_uint16_t;
    #define _LIBARCH_INT16_MIN         SHRT_MIN
    #define _LIBARCH_INT16_MAX         SHRT_MAX
    #define _LIBARCH_UINT16_MAX        USHRT_MAX
  #else
    #error No _LIBARCH_int16_t/uint16_t type
  #endif

  /* Define _LIBARCH_(u)int32_t */
  #if _LIBARCH_INT_SIZE == 4
    typedef int                        _LIBARCH_int32_t;
    typedef unsigned int               _LIBARCH_uint32_t;
    #define _LIBARCH_INT32_MIN         INT_MIN
    #define _LIBARCH_INT32_MAX         INT_MAX
    #define _LIBARCH_UINT32_MAX        UINT_MAX
    #define _LIBARCH_INT32_T_C(x)      (x)
    #define _LIBARCH_UINT32_T_C(x)     (x##u)
  #elif _LIBARCH_LONG_SIZE == 4
    typedef long                       _LIBARCH_int32_t;
    typedef unsigned long              _LIBARCH_uint32_t;
    #define _LIBARCH_INT32_MIN         LONG_MIN
    #define _LIBARCH_INT32_MAX         LONG_MAX
    #define _LIBARCH_UINT32_MAX        ULONG_MAX
    #define _LIBARCH_INT32_T_C(x)      (x##L)
    #define _LIBARCH_UINT32_T_C(x)     (x##uL)
  #else
    #error No _LIBARCH_int32_t/uint32_t type
  #endif

  /* Define _LIBARCH_(u)int64_t */
  #if _LIBARCH_LONG_SIZE == 8
    typedef long                       _LIBARCH_int64_t;
    typedef unsigned long              _LIBARCH_uint64_t;
    #define _LIBARCH_INT64_MIN         LONG_MIN
    #define _LIBARCH_INT64_MAX         LONG_MAX
    #define _LIBARCH_UINT64_MAX        ULONG_MAX
    #define _LIBARCH_INT64_T_C(x)      (x##L)
    #define _LIBARCH_UINT64_T_C(x)     (x##uL)
  #elif _LIBARCH_LONGLONG_SIZE == 8
    typedef long long                  _LIBARCH_int64_t;
    typedef unsigned long long         _LIBARCH_uint64_t;
    #define _LIBARCH_INT64_MIN         LLONG_MIN
    #define _LIBARCH_INT64_MAX         LLONG_MAX
    #define _LIBARCH_UINT64_MAX        ULLONG_MAX
    #define _LIBARCH_INT64_T_C(x)      (x##LL)
    #define _LIBARCH_UINT64_T_C(x)     (x##uLL)
  #else
    #error No _LIBARCH_int64_t/uint64_t type
  #endif

  /* Define the "_LIBARCH_(u)int_leastX_t" types */
  typedef _LIBARCH_int8_t              _LIBARCH_int_least8_t;
  typedef _LIBARCH_uint8_t             _LIBARCH_uint_least8_t;
  #define _LIBARCH_INT_LEAST8_MIN      _LIBARCH_INT8_MIN
  #define _LIBARCH_INT_LEAST8_MAX      _LIBARCH_INT8_MAX
  #define _LIBARCH_UINT_LEAST8_MAX     _LIBARCH_UINT8_MAX
  #define _LIBARCH_INT8_C(x)           ((_LIBARCH_int_least8_t)x)
  #define _LIBARCH_UINT8_C(x)          ((_LIBARCH_uint_least16_t)x)
  typedef _LIBARCH_int16_t             _LIBARCH_int_least16_t;
  typedef _LIBARCH_uint16_t            _LIBARCH_uint_least16_t;
  #define _LIBARCH_INT_LEAST16_MIN     _LIBARCH_INT16_MIN
  #define _LIBARCH_INT_LEAST16_MAX     _LIBARCH_INT16_MAX
  #define _LIBARCH_UINT_LEAST16_MAX    _LIBARCH_UINT16_MAX
  #define _LIBARCH_INT16_C(x)          ((_LIBARCH_int_least16_t)x)
  #define _LIBARCH_UINT16_C(x)         ((_LIBARCH_uint_least16_t)x)
  typedef _LIBARCH_int32_t             _LIBARCH_int_least32_t;
  typedef _LIBARCH_uint32_t            _LIBARCH_uint_least32_t;
  #define _LIBARCH_INT_LEAST32_MIN     _LIBARCH_INT32_MIN
  #define _LIBARCH_INT_LEAST32_MAX     _LIBARCH_INT32_MAX
  #define _LIBARCH_UINT_LEAST32_MAX    _LIBARCH_UINT32_MAX
  #define _LIBARCH_INT32_C(x)          _LIBARCH_INT32_T_C(x)
  #define _LIBARCH_UINT32_C(x)         _LIBARCH_UINT32_T_C(x)
  typedef _LIBARCH_int64_t             _LIBARCH_int_least64_t;
  typedef _LIBARCH_uint64_t            _LIBARCH_uint_least64_t;
  #define _LIBARCH_INT_LEAST64_MIN     _LIBARCH_INT64_MIN
  #define _LIBARCH_INT_LEAST64_MAX     _LIBARCH_INT64_MAX
  #define _LIBARCH_UINT_LEAST64_MAX    _LIBARCH_UINT64_MAX
  #define _LIBARCH_INT64_C(x)          _LIBARCH_INT64_T_C(x)
  #define _LIBARCH_UINT64_C(x)         _LIBARCH_UINT64_T_C(x)

  /* Define the "_LIBARCH_(u)int_fastX_t" types */
  typedef _LIBARCH_int8_t              _LIBARCH_int_fast8_t;
  typedef _LIBARCH_uint8_t             _LIBARCH_uint_fast8_t;
  #define _LIBARCH_INT_FAST8_MIN       _LIBARCH_INT8_MIN
  #define _LIBARCH_INT_FAST8_MAX       _LIBARCH_INT8_MAX
  #define _LIBARCH_UINT_FAST8_MAX      _LIBARCH_UINT8_MAX
  typedef _LIBARCH_int16_t             _LIBARCH_int_fast16_t;
  typedef _LIBARCH_uint16_t            _LIBARCH_uint_fast16_t;
  #define _LIBARCH_INT_FAST16_MIN      _LIBARCH_INT16_MIN
  #define _LIBARCH_INT_FAST16_MAX      _LIBARCH_INT16_MAX
  #define _LIBARCH_UINT_FAST16_MAX     _LIBARCH_UINT16_MAX
  typedef _LIBARCH_int32_t             _LIBARCH_int_fast32_t;
  typedef _LIBARCH_uint32_t            _LIBARCH_uint_fast32_t;
  #define _LIBARCH_INT_FAST32_MIN      _LIBARCH_INT32_MIN
  #define _LIBARCH_INT_FAST32_MAX      _LIBARCH_INT32_MAX
  #define _LIBARCH_UINT_FAST32_MAX     _LIBARCH_UINT32_MAX
  typedef _LIBARCH_int64_t             _LIBARCH_int_fast64_t;
  typedef _LIBARCH_uint64_t            _LIBARCH_uint_fast64_t;
  #define _LIBARCH_INT_FAST64_MIN      _LIBARCH_INT64_MIN
  #define _LIBARCH_INT_FAST64_MAX      _LIBARCH_INT64_MAX
  #define _LIBARCH_UINT_FAST64_MAX     _LIBARCH_UINT64_MAX

  /* Define _LIBARCH_(u)intptr_t */
  #if _LIBARCH_POINTER_SIZE == 4
    typedef _LIBARCH_int32_t           _LIBARCH_intptr_t;
    typedef _LIBARCH_uint32_t          _LIBARCH_uintptr_t;
    #define _LIBARCH_INTPTR_MIN        _LIBARCH_INT32_MIN
    #define _LIBARCH_INTPTR_MAX        _LIBARCH_INT32_MAX
    #define _LIBARCH_UINTPTR_MAX       _LIBARCH_UINT32_MAX
  #elif _LIBARCH_POINTER_SIZE == 8
    typedef _LIBARCH_int64_t           _LIBARCH_intptr_t;
    typedef _LIBARCH_uint64_t          _LIBARCH_uintptr_t;
    #define _LIBARCH_INTPTR_MIN        _LIBARCH_INT64_MIN
    #define _LIBARCH_INTPTR_MAX        _LIBARCH_INT64_MAX
    #define _LIBARCH_UINTPTR_MAX       _LIBARCH_UINT64_MAX
  #else
    #error No _LIBARCH_intptr_t/uintptr_t type
  #endif

  /* Define _LIBARCH_(u)intmax_t */
  #if _LIBARCH_INTMAX_SIZE == 4
    typedef _LIBARCH_int32_t           _LIBARCH_intmax_t;
    typedef _LIBARCH_uint32_t          _LIBARCH_uintmax_t;
    #define _LIBARCH_INTMAX_MIN        _LIBARCH_INT32_MIN
    #define _LIBARCH_INTMAX_MAX        _LIBARCH_INT32_MAX
    #define _LIBARCH_UINTMAX_MAX       _LIBARCH_UINT32_MAX
    #if _LIBARCH_INT_SIZE == 4
      #define _LIBARCH_INTMAX_C(x)     (x)
      #define _LIBARCH_UINTMAX_C(x)    (x)
    #elif _LIBARCH_LONG_SIZE == 4
      #define _LIBARCH_INTMAX_C(x)     (x##L)
      #define _LIBARCH_UINTMAX_C(x)    (x##UL)
    #endif
  #elif _LIBARCH_INTMAX_SIZE == 8
    typedef _LIBARCH_int64_t           _LIBARCH_intmax_t;
    typedef _LIBARCH_uint64_t          _LIBARCH_uintmax_t;
    #define _LIBARCH_INTMAX_MIN        _LIBARCH_INT64_MIN
    #define _LIBARCH_INTMAX_MAX        _LIBARCH_INT64_MAX
    #define _LIBARCH_UINTMAX_MAX       _LIBARCH_UINT64_MAX
    #if _LIBARCH_LONG_SIZE == 8
      #define _LIBARCH_INTMAX_C(x)     (x##L)
      #define _LIBARCH_UINTMAX_C(x)    (x##UL)
    #elif _LIBARCH_LONGLONG_SIZE == 8
      #define _LIBARCH_INTMAX_C(x)     (x##LL)
      #define _LIBARCH_UINTMAX_C(x)    (x##ULL)
    #endif
  #else
    #error No _LIBARCH_intmax_t/uintmax_t type
  #endif
#else
  #error _LIBARCH_CHAR_BIT != 8
#endif

/* Define ptrdiff_t limits */
#define _LIBARCH_PTRDIFF_MIN           _LIBARCH_INTPTR_MIN
#define _LIBARCH_PTRDIFF_MAX           _LIBARCH_INTPTR_MAX

/* TODO: sig_atomic_t limits */

/* Define _LIBARCH_atomic_int_t */
#if _LIBARCH_ATOMIC_SIZE == 4
  typedef _LIBARCH_int32_t  _LIBARCH_atomic_int_t;
  typedef _LIBARCH_uint32_t _LIBARCH_atomic_uint_t;
#elif _LIBARCH_ATOMIC_SIZE == 8
  typedef _LIBARCH_int64_t  _LIBARCH_atomic_int_t;
  typedef _LIBARCH_uint64_t _LIBARCH_atomic_uint_t;
#else
  #error No _LIBARCH_atomic_int_t
#endif


/* Define size_t limits */
#define _LIBARCH_SIZE_MAX              _LIBARCH_UINTMAX_MAX

/* TODO: wchar_t limits */

/* TODO: wint_t limits */



/* _LIBARCH_ssize_t */
#if _LIBARCH_SIZE == 4
  typedef _LIBARCH_int32_t   _LIBARCH_ssize_t;
  #define _LIBARCH_SSIZE_MAX _LIBARCH_INT32_MAX
#elif _LIBARCH_SIZE == 8
  typedef _LIBARCH_int64_t _LIBARCH_ssize_t;
  #define _LIBARCH_SSIZE_MAX _LIBARCH_INT64_MAX
#else
  #error No _LIBARCH_ssize_t type
#endif

/*@}*/

#endif
