/*
lightOS libarch
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBARCH_SCOPED_LOCK_HPP
#define LIGHTOS_LIBARCH_SCOPED_LOCK_HPP

/*! \addtogroup libarch libarch */
/*@{*/

namespace libarch
{
  /** Wrapper to allow scope-based locking of an object. The lock is
   *  acquired by the constructor and freed by the destructor */
  template<typename T>
  class scoped_lock
  {
    public:
      /** The constructor acquires the lock */
      scoped_lock(T& lock);

      scoped_lock(scoped_lock& x) = delete;
      scoped_lock& operator = (scoped_lock& x) = delete;

      /** The destructor frees the aquired lock */
      ~scoped_lock();

    private:
      /** Reference to the actual lock object */
      T* m_lock;
  };

  namespace helper
  {
    /** Create a scoped_lock with automatic type deduction
     * \param[in] lock the lock */
    template<typename T>
    scoped_lock<T> create_scoped_lock(T& lock)
    {
      return libarch::scoped_lock<T>(lock);
    }
  }



  /*
   * Implementation of scoped_lock
   */

  template<typename T>
  scoped_lock<T>::scoped_lock(T& lock)
    : m_lock(lock)
  {
    m_lock.lock();
  }

  template<typename T>
  scoped_lock<T>::~scoped_lock()
  {
    m_lock.unlock();
  }
}



/*
 * Helper macros
 */

#ifdef _LIGHTOS_SMP

  /** Define a scoped_lock (Multiprocessor version)
   *\param[in] l     the lock that is being held by the scoped_lock
   *\param[in] scope the name of the scoped_lock object */
  #define SCOPED_LOCK(l, scope)                  auto scope = create_scoped_lock(l)

  /** Declare a scoped_lock (Multiprocessor version)
   *\param[in] scope the name of the scoped_lock object
   *\param[in] T     the type of the lock being held */
  #define SCOPED_LOCK_DECL(scope, T)             libarch::scoped_lock<T> scope;

#else

  /** Define a scoped_lock (Singleprocessor version)
   *\param[in] l     the lock that is being held by the scoped_lock
   *\param[in] scope the name of the scoped_lock object */
  #define SCOPED_LOCK(l, scope)

  /** Declare a scoped_lock (Singleprocessor version)
   *\param[in] scope the name of the scoped_lock object
   *\param[in] T     the type of the lock being held */
  #define SCOPED_LOCK_DECL(scope, T)

#endif

/*@}*/

#endif
