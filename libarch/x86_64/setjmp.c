/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <setjmp.h>

int _LIBARCH_setjmp(_LIBARCH_jmp_buf *env)
{
  // TODO
  __asm__ volatile("hlt"
                   ::"a" (env));
  return 0;
}

void _LIBARCH_longjmp(_LIBARCH_jmp_buf *env,
                      int val)
{
  // TODO
  __asm__ volatile("hlt"
                   ::"a"(val), "c" (env));
}
