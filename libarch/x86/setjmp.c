/*
lightOS libc
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libarch/setjmp.h>

int _LIBARCH_setjmp(_LIBARCH_jmp_buf *env)
{
  __asm__ volatile("mov %%ebx, (%%eax)\n"
                   "mov %%esi, 4(%%eax)\n"
                   "mov %%edi, 8(%%eax)\n"
                   "mov %%esp, %%ebx\n"
                   "mov (%%ebx), %%ecx\n"
                   "mov %%ecx, 12(%%eax)\n"
                   "mov 4(%%ebx), %%ecx\n"
                   "mov %%ecx, 20(%%eax)\n"
                   "add $0x08, %%ebx\n"
                   "mov %%ebx, 16(%%eax)\n"
                   "mov (%%eax), %%ebx\n"
                   "mov 4(%%eax), %%ecx\n"
                   ::"a" (env));
  return 0;
}

void _LIBARCH_longjmp(_LIBARCH_jmp_buf *env, int val)
{
  __asm__ volatile("mov (%%ecx), %%ebx\n"
                   "mov 4(%%ecx), %%esi\n"
                   "mov 8(%%ecx), %%edi\n"
                   "mov 12(%%ecx), %%ebp\n"
                   "mov 16(%%ecx), %%esp\n"
                   "jmp *20(%%ecx)\n"
                   ::"a"(val), "c" (env));
}
