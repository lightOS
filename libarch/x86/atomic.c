/*
lightOS library
Copyright (C) 2007-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libarch/atomic.h>

void atomic_add(_LIBARCH_atomic_int_t *Atom,
                _LIBARCH_atomic_int_t i)
{
  __asm__ volatile(LOCK_PREFIX " addl %1,%0":
                   "=m" (*Atom):
                   "ir" (i), "m" (*Atom));
}

void atomic_sub(_LIBARCH_atomic_int_t *Atom,
                _LIBARCH_atomic_int_t i)
{
  __asm__ volatile(LOCK_PREFIX " subl %1,%0":
                   "=m" (*Atom):
                   "ir" (i), "m" (*Atom));
}

bool atomic_sub_and_test(_LIBARCH_atomic_int_t *Atom,
                         _LIBARCH_atomic_int_t i)
{
  bool res;
  __asm__ volatile(LOCK_PREFIX " subl %2,%0; sete %1":
                   "=m" (*Atom), "=qm" (res):
                   "ir" (i), "m" (*Atom) :
                   "memory");
  return res;
}

void atomic_inc(_LIBARCH_atomic_int_t *Atom)
{
  __asm__ volatile(LOCK_PREFIX " incl %0":
                   "=m" (*Atom):
                   "m" (*Atom));
}

bool atomic_inc_and_test(_LIBARCH_atomic_int_t *Atom)
{
  bool res;
  __asm__ volatile(LOCK_PREFIX " incl %0; setnge %1":
                   "=m" (*Atom), "=qm" (res):
                   "m" (*Atom):
                   "memory");
  return res;
}

void atomic_dec(_LIBARCH_atomic_int_t *Atom)
{
  __asm__ volatile(LOCK_PREFIX " decl %0":
                   "=m" (*Atom):
                   "m" (*Atom));
}

bool atomic_dec_and_test(_LIBARCH_atomic_int_t *Atom)
{
  bool res;
  __asm__ volatile(LOCK_PREFIX " decl %0; setg %1":
                   "=m" (*Atom), "=qm" (res):
                   "m" (*Atom):
                   "memory");
  return res;
}

bool atomic_compare_and_exchange(_LIBARCH_atomic_int_t *Atom,
                                 _LIBARCH_atomic_int_t cmpval,
                                 _LIBARCH_atomic_int_t newval)
{
  bool res;
  __asm__ volatile("mov $0, %%edx\n"
                   LOCK_PREFIX " cmpxchgl %%ecx, (%%ebx)\n"
                   "jnz .end\n"
                   "mov $1, %%edx\n"
                   ".end:\n":
                   "=d" (res):
                   "a" (cmpval), "b" (Atom), "c" (newval));
  return res;
}
