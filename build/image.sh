#! /bin/sh

if [ -z "$1" ]
then
  echo "Error: No architecture selected"
  exit 1
fi

if [ $1 = "x86" ] ||
   [ $1 = "x86_64" ]
then
  # ISO Image
  mkdir iso
  ./cpimage.sh $1 iso
  rm iso/boot/grub/stage1
  rm iso/boot/grub/stage2
  rm iso/boot/grub/menu-floppy.lst
  mkisofs -R -b boot/grub/stage2_eltorito -no-emul-boot -boot-load-size 4 -boot-info-table -o lightOS-$1.iso iso
  rm -rf iso

  # Floppy Image
  dd if=/dev/zero of=lightOS-$1.img bs=1024 count=1440
  # NOTE mke2fs -F lightOS-$1.img
  mkdosfs lightOS-$1.img
  sudo mkdir fd0
  sudo mount -oloop lightOS-$1.img fd0
  sudo mkdir fd0/bin
  sudo cp $1/bin/arp fd0/bin
  sudo cp $1/bin/cat fd0/bin
  sudo cp $1/bin/date fd0/bin
  sudo cp $1/bin/df fd0/bin
  sudo cp $1/bin/dmesg fd0/bin
  sudo cp $1/bin/dns fd0/bin
  sudo cp $1/bin/echo fd0/bin
  sudo cp $1/bin/help fd0/bin
  sudo cp $1/bin/ifconfig fd0/bin
  sudo cp $1/bin/kill fd0/bin
  sudo cp $1/bin/less fd0/bin
  sudo cp $1/bin/ls fd0/bin
  sudo cp $1/bin/lslib fd0/bin
  sudo cp $1/bin/lspci fd0/bin
  sudo cp $1/bin/mem fd0/bin
  sudo cp $1/bin/mkdir fd0/bin
  sudo cp $1/bin/ping fd0/bin
  sudo cp $1/bin/ps fd0/bin
  sudo cp $1/bin/pwd fd0/bin
  sudo cp $1/bin/server fd0/bin
  sudo cp $1/bin/shell fd0/bin
  sudo cp $1/bin/uptime fd0/bin
  sudo cp $1/bin/version fd0/bin
  if [ -e $1/bin/nano ]
  then
    sudo cp $1/bin/nano fd0/bin
  fi
  sudo mkdir fd0/boot
  sudo mkdir fd0/boot/grub
  sudo cp $1/boot/grub/stage1 fd0/boot/grub
  sudo cp $1/boot/grub/stage2 fd0/boot/grub
  sudo cp $1/boot/grub/menu-floppy.lst fd0/boot/grub/menu.lst
  sudo mkdir fd0/lib
  sudo cp $1/lib/liblightOS.so fd0/lib
  sudo cp $1/lib/libc.so fd0/lib
  sudo cp $1/lib/libc++.so fd0/lib
  sudo cp $1/lib/libcurses++.so fd0/lib
  sudo cp $1/lib/liblightOS++.so fd0/lib
  sudo mkdir fd0/system
  sudo cp $1/system/kernel fd0/system
  sudo cp $1/system/kernel-dbg fd0/system
  sudo mkdir fd0/system/config
  sudo cp config/net.cfg fd0/system/config
  sudo cp config/boot.cfg fd0/system/config
  sudo cp config/keyboard.cfg fd0/system/config
  sudo cp config/net.cfg fd0/system/config
  sudo mkdir fd0/system/config/charsets
  sudo cp config/charsets/ascii fd0/system/config/charsets
  sudo mkdir fd0/system/config/keymaps
  sudo cp config/keymaps/de.map fd0/system/config/keymaps
  sudo mkdir fd0/system/config/keymaps/includes
  sudo cp config/keymaps/includes/compose.lat fd0/system/config/keymaps/includes
  sudo cp config/keymaps/includes/euro2.map fd0/system/config/keymaps/includes
  sudo cp config/keymaps/includes/linuxaa.inc fd0/system/config/keymaps/includes
  sudo cp config/keymaps/includes/linuxkb.inc fd0/system/config/keymaps/includes
  sudo cp config/keymaps/includes/qwertzl.inc fd0/system/config/keymaps/includes
  sudo mkdir fd0/system/server
  sudo cp $1/system/server/console fd0/system/server
  sudo cp $1/system/server/floppy fd0/system/server
  sudo cp $1/system/server/init fd0/system/server
  sudo cp $1/system/server/klog fd0/system/server
  sudo cp $1/system/server/pci fd0/system/server
  sudo cp $1/system/server/ps2 fd0/system/server
  sudo cp $1/system/server/vfs fd0/system/server
  sudo cp $1/system/server/devfs fd0/system/server
  sudo cp $1/system/server/ext2 fd0/system/server
  sudo cp $1/system/server/fat fd0/system/server
  sudo cp $1/system/server/pipefs fd0/system/server
  sudo cp $1/system/server/tmpfs fd0/system/server
  sudo cp $1/system/server/net fd0/system/server
  sudo cp $1/system/server/pcnet fd0/system/server
  sudo cp $1/system/server/rtl8139 fd0/system/server
  sudo cp $1/system/server/serial fd0/system/server
  sudo umount fd0
  rmdir fd0
  ./grub.sh $1

else
  echo "Error: Unsupported architecture selected: " $1
  exit 1
fi
