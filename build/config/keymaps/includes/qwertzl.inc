# qwertz-layout
#             Normal            Shift            AltGr           Strg
keycode 16 =  q                 Q
keycode 17 =  w                 W
keycode 18 =  e                 E
keycode 19 =  r                 R
keycode 20 =  t                 T
keycode 21 =  z                 Z
keycode 22 =  u                 U
keycode 23 =  i                 I
keycode 24 =  o                 O
keycode 25 =  p                 P
#
keycode 30 =  a                 A
keycode 31 =  s                 S
keycode 32 =  d                 D
keycode 33 =  f                 F
keycode 34 =  g                 G
keycode 35 =  h                 H
keycode 36 =  j                 J
keycode 37 =  k                 K
keycode 38 =  l                 L
#
keycode 44 =  y                 Y
keycode 45 =  x                 X
keycode 46 =  c                 C
keycode 47 =  v                 V
keycode 48 =  b                 B
keycode 49 =  n                 N
keycode 50 =  m                 M
#
