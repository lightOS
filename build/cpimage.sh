#/bin/bash

# script parameter
# $1 architecture
# $2 destination directory

cp -rL config $1/system
LIST_SRC=$(find $1/* | sed -e "s/.*\/\.svn.*//")
for a in $LIST_SRC; do
	b=$(echo $a | sed 's/'$1'\//'$2'\//')
	if (test -d $a); then
		if !(test -e $b); then
			mkdir $b
		fi
	else
		cp -L $a $b
	fi
done
rm -rf $1/system/config
