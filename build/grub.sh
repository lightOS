###############################################################################
##### lightOS                                                             #####
###############################################################################
grub --batch --no-floppy <<EOT 1>/dev/null  || exit 1
device (fd0) lightOS-$1.img
install (fd0)/boot/grub/stage1 (fd0) (fd0)/boot/grub/stage2 p (fd0)/boot/grub/menu.lst
quit
EOT
