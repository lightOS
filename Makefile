###############################################################################
######### lightOS Makefile                                           ##########
###############################################################################
default: all

-include Makefile.config
-include Makefile.rules

.PHONY : doc

doc:
	@rm -rf doc
	@mkdir doc
	@doxygen config/Doxyfile.kernel
	@doxygen config/Doxyfile.libc

install-headers:
	@make --no-print-directory -C libarch install-headers
	@make --no-print-directory -C libkernel install-headers
	@make --no-print-directory -C libserver install-headers
	@make --no-print-directory -C lib/libc install-headers
	@make --no-print-directory -C lib/libc++ install-headers
	@make --no-print-directory -C lib/lightOS++ install-headers
	@make --no-print-directory -C lib/curses++ install-headers

uninstall-headers:
	@make --no-print-directory -C lib/curses++ uninstall-headers
	@make --no-print-directory -C lib/lightOS++ uninstall-headers
	@make --no-print-directory -C lib/libc++ uninstall-headers
	@make --no-print-directory -C lib/libc uninstall-headers
	@make --no-print-directory -C libserver uninstall-headers
	@make --no-print-directory -C libkernel uninstall-headers
	@make --no-print-directory -C libarch uninstall-headers

all: install-headers
	@make --no-print-directory -C lib/lightOS all
	@make --no-print-directory -C lib/libc all
	@make --no-print-directory -C lib/lightOS++ all
	@make --no-print-directory -C lib/libc++ all
	@make --no-print-directory -C lib/curses++ all
	@make --no-print-directory -C lib/ports all
	@make --no-print-directory -C kernel all
	@make --no-print-directory -C server all
	@make --no-print-directory -C apps all

image:
	@cd build && ./image.sh $(ARCH)

vmware-test:
	@vmware

bochs-test:
	@./toolchain/$(ARCH)-bochs -f config/bochs-$(ARCH).config

bochs-test-iso:
	@./toolchain/$(ARCH)-bochs -f config/bochs-iso-$(ARCH).config

qemu-test:
	@./toolchain/$(ARCH)-qemu -d int -k de -net nic,model=pcnet -net user -fda build/lightOS-$(ARCH).img \
                                  -boot a -m 32 -localtime
	###-smp 2 -soundhw sb16 -net nic,model=rtl8139 -net user

qemu-test-iso:
	@./toolchain/$(ARCH)-qemu -k de -net nic,model=pcnet -net user -cdrom build/lightOS-$(ARCH).iso \
                                  -boot d -m 32 -localtime

kloc:
	@sloccount apps/*.cpp apps/shell kernel lib/curses lib/curses++ lib/libc lib/libc++ \
                   lib/lightOS lib/lightOS++ libarch libkernel libserver libsupc++ server

clean:
	@./scripts/clean.sh
	@./scripts/tmpdir.sh $(ARCH) remove
