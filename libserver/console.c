/*
lightOS libserver
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <stddef.h>
#include <libserver/console.h>

/*
 * Macros
 */

#define _CHAR(c)                       _internal_write_char(c, &output, &size)
#define _CHAR_F(c)                     _internal_write_char_final(c, &output, &size)
#define _STRING(str)                   _internal_write_string(str, &output, &size)
#define _STRING_F(str)                 _internal_write_string_final(str, &output, &size)
#define MIN_INPUT(n)                   if (length < n)return _LIBSERVER_CONVERT_NEED_INPUT



/*
 * Kecode to ECMA-48 character sequence conversion table
 */

const char* key2char[] =
{
  "",                                  // _LIBSERVER_KEYCODE_NONE                  0x000
  "",                                  // TODO _LIBSERVER_KEYCODE_ESC                   0x001
  "\x1B[A",                            // _LIBSERVER_KEYCODE_ARROW_UP              0x002
  "\x1B[B",                            // _LIBSERVER_KEYCODE_ARROW_DOWN            0x003
  "\x1B[D",                            // _LIBSERVER_KEYCODE_ARROW_LEFT            0x004
  "\x1B[C",                            // _LIBSERVER_KEYCODE_ARROW_RIGHT           0x005
  "\x1B[5~",                           // _LIBSERVER_KEYCODE_PAGE_UP               0x006
  "\x1B[6~",                           // _LIBSERVER_KEYCODE_PAGE_DOWN             0x007
  "\x1B[H",                            // _LIBSERVER_KEYCODE_POS1                  0x008
  "\x1B[F",                            // _LIBSERVER_KEYCODE_END                   0x009
  "\x1B[3~",                           // _LIBSERVER_KEYCODE_ERASE                 0x00A
  "",                                  // TODO _LIBSERVER_KEYCODE_INSERT                0x00B
  "",                                  // _LIBSERVER_KEYCODE_F0                    0x00C
  "\x1BOP",                            // _LIBSERVER_KEYCODE_F1                    0x00D
  "\x1BOQ",                            // _LIBSERVER_KEYCODE_F2                    0x00E
  "\x1BOR",                            // _LIBSERVER_KEYCODE_F3                    0x00F
  "\x1BOS",                            // _LIBSERVER_KEYCODE_F4                    0x010
  "\x1B[15~",                          // _LIBSERVER_KEYCODE_F5                    0x011
  "\x1B[17~",                          // _LIBSERVER_KEYCODE_F6                    0x012
  "\x1B[18~",                          // _LIBSERVER_KEYCODE_F7                    0x013
  "\x1B[19~",                          // _LIBSERVER_KEYCODE_F8                    0x014
  "\x1B[20~",                          // _LIBSERVER_KEYCODE_F9                    0x015
  "\x1B[21~",                          // _LIBSERVER_KEYCODE_F10                   0x016
  "\x1B[23~",                          // _LIBSERVER_KEYCODE_F11                   0x017
  "\x1B[24~"                           // _LIBSERVER_KEYCODE_F12                   0x018
  "",                                  // TODO _LIBSERVER_KEYCODE_F13                   0x019
  "",                                  // TODO _LIBSERVER_KEYCODE_F14                   0x01A
  "",                                  // TODO _LIBSERVER_KEYCODE_F15                   0x01B
  "",                                  // TODO _LIBSERVER_KEYCODE_F16                   0x01C
  "",                                  // TODO _LIBSERVER_KEYCODE_NUMPAD_POS1           0x01D
  "",                                  // TODO _LIBSERVER_KEYCODE_NUMPAD_END            0x01E
  "",                                  // TODO _LIBSERVER_KEYCODE_NUMPAD_PAGE_UP        0x01F
  "",                                  // TODO _LIBSERVER_KEYCODE_NUMPAD_PAGE_DOWN      0x020
  "",                                  // TODO _LIBSERVER_KEYCODE_NUMPAD_CENTER         0x021
};



/*
 * File-local functions
 */

static void _internal_write_char(char input,
                                 char** output,
                                 size_t *size)
{
  if (*size > 1)
  {
    *(*output)++ = input;
    --*size;
  }
}

static void _internal_write_string(const char* input,
                                   char** output,
                                   size_t *size)
{
  while (*input != '\0')
    _internal_write_char(*input++,
                         output,
                         size);
}

static void _internal_write_end(char** output,
                                size_t *size)
{
  if (*size != 0)
  {
    *(*output)++ = '\0';
    --*size;
  }
}

static void _internal_write_char_final(char input,
                                       char** output,
                                       size_t *size)
{
  _internal_write_char(input, output, size);
  _internal_write_end(output, size);
}

static void _internal_write_string_final(const char* input,
                                         char** output,
                                         size_t *size)
{
  _internal_write_string(input, output, size);
  _internal_write_end(output, size);
}



/*
 * Global functions
 */

void _LIBSERVER_convert_to_ecma48(_LIBSERVER_key_t key,
                                  char* output,
                                  size_t size)
{
  // We have a unicode character
  if (key.character != L'\0')
  {
    // TODO Convert to UTF-8
    _CHAR_F((char)key.character);
  }
  // We have a known keycode
  else if (key.code <= _LIBSERVER_KEYCODE_MAX)
  {
    _STRING_F(key2char[key.code]);
  }
  // We have an unknown keycode
  else
    _STRING_F("");
}

int _LIBSERVER_convert_from_ecma48(const char* input,
                                   size_t length,
                                   _LIBSERVER_key_t* key)
{
  MIN_INPUT(1);

  key->character = L'\0';
  key->code      = _LIBSERVER_KEYCODE_NONE;

  // Control sequence
  // TODO: F13, F14, F15, F16
  if (*input == '\x1B')
  {
    ++input;
    MIN_INPUT(2);

    if (*input == 'O')
    {
      ++input;
      MIN_INPUT(3);

      if (*input == 'P')
        key->code = _LIBSERVER_KEYCODE_F1;
      else if (*input == 'Q')
        key->code = _LIBSERVER_KEYCODE_F2;
      else if (*input == 'R')
        key->code = _LIBSERVER_KEYCODE_F3;
      else if (*input == 'S')
        key->code = _LIBSERVER_KEYCODE_F4;
      else
        return _LIBSERVER_CONVERT_FAILED;
    }
    else if (*input == '[')
    {
      ++input;
      MIN_INPUT(3);

      if (*input == 'D')
        key->code = _LIBSERVER_KEYCODE_ARROW_LEFT;
      else if (*input == 'C')
        key->code = _LIBSERVER_KEYCODE_ARROW_RIGHT;
      else if (*input == 'A')
        key->code = _LIBSERVER_KEYCODE_ARROW_UP;
      else if (*input == 'B')
        key->code = _LIBSERVER_KEYCODE_ARROW_DOWN;
      else if (*input == 'H')
        key->code = _LIBSERVER_KEYCODE_POS1;
      else if (*input == 'F')
        key->code = _LIBSERVER_KEYCODE_END;
      else if (*input == '1')
      {
        ++input;
        MIN_INPUT(5);

        if (*(input + 1) != '~')
          return _LIBSERVER_CONVERT_FAILED;

        if (*input == '5')
          key->code = _LIBSERVER_KEYCODE_F5;
        else if (*input == '7')
          key->code = _LIBSERVER_KEYCODE_F6;
        else if (*input == '8')
          key->code = _LIBSERVER_KEYCODE_F7;
        else if (*input == '9')
          key->code = _LIBSERVER_KEYCODE_F8;
        else
          return _LIBSERVER_CONVERT_FAILED;
      }
      else if (*input == '2')
      {
        ++input;
        MIN_INPUT(5);

        if (*(input + 1) != '~')
          return _LIBSERVER_CONVERT_FAILED;

        if (*input == '0')
          key->code = _LIBSERVER_KEYCODE_F9;
        else if (*input == '1')
          key->code = _LIBSERVER_KEYCODE_F10;
        else if (*input == '3')
          key->code = _LIBSERVER_KEYCODE_F11;
        else if (*input == '4')
          key->code = _LIBSERVER_KEYCODE_F12;
        else
          return _LIBSERVER_CONVERT_FAILED;
      }
      else if (*input == '3')
      {
        ++input;
        MIN_INPUT(4);

        if (*input == '~')
          key->code = _LIBSERVER_KEYCODE_ERASE;
        else
          return _LIBSERVER_CONVERT_FAILED;
      }
      else if (*input == '5')
      {
        ++input;
        MIN_INPUT(4);

        if (*input == '~')
          key->code = _LIBSERVER_KEYCODE_PAGE_UP;
        else
          return _LIBSERVER_CONVERT_FAILED;
      }
      else if (*input == '6')
      {
        ++input;
        MIN_INPUT(4);

        if (*input == '~')
          key->code = _LIBSERVER_KEYCODE_PAGE_DOWN;
        else
          return _LIBSERVER_CONVERT_FAILED;
      }
      else
        return _LIBSERVER_CONVERT_FAILED;
    }
    else
      return _LIBSERVER_CONVERT_FAILED;
  }
  else
    key->character = *input;

  return _LIBSERVER_CONVERT_OK;
}
