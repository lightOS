/*
lightOS libserver
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBSERVER_NOPREFIX_FS_H
#define LIGHTOS_LIBSERVER_NOPREFIX_FS_H

#include <libserver/fs.h>

/*! \addtogroup libserver libserver */
/*@{*/

// NOTE: this is also defined in lib/libc/_LIBC.h
#ifndef FILENAME_MAX
  #define FILENAME_MAX                 _LIBSERVER_FILENAME_MAX
#endif

#define FILE_ACCESS_INFO               _LIBSERVER_FILE_ACCESS_INFO
#define FILE_ACCESS_READ               _LIBSERVER_FILE_ACCESS_READ
#define FILE_ACCESS_WRITE              _LIBSERVER_FILE_ACCESS_WRITE
#define FILE_ACCESS_MASK               _LIBSERVER_FILE_ACCESS_MASK

#define FILE_ATTRIBUTE_TTY             _LIBSERVER_FILE_ATTRIBUTE_TTY
#define FILE_ATTRIBUTE_MASK            _LIBSERVER_FILE_ATTRIBUTE_MASK

#define FILE_TYPE_NONE                 _LIBSERVER_FILE_TYPE_NONE
#define FILE_TYPE_BLOCK                _LIBSERVER_FILE_TYPE_BLOCK
#define FILE_TYPE_STREAM               _LIBSERVER_FILE_TYPE_STREAM
#define FILE_TYPE_DIRECTORY            _LIBSERVER_FILE_TYPE_DIRECTORY

typedef _LIBSERVER_file_name_info file_name_info;
typedef _LIBSERVER_file_info      file_info;

/*@}*/

#endif
