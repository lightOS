/*
lightOS libserver
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBSERVER_CONSOLE_H
#define LIGHTOS_LIBSERVER_CONSOLE_H

#include <libserver/keyboard.h>

/*! \addtogroup libserver libserver */
/*@{*/



/*
 * Macros
 */

/** Returned by _LIBSERVER_convert_from_ecma48 when the conversion
 *  succeeded */
#define _LIBSERVER_CONVERT_OK                    1

/** Returned by _LIBSERVER_convert_from_ecma48 when the conversion
 *  could not be completed due to insuficient input */
#define _LIBSERVER_CONVERT_NEED_INPUT            0

/** Returned by _LIBSERVER_convert_from_ecma48 when the conversion
 *  failed */
#define _LIBSERVER_CONVERT_FAILED                -1

/** Enable/disable the 'hardware' eching of the console
 *\param[in] file the console file handle
 *\param[in] b    true to enable, false to disable */
#define _LIBSERVER_CONSOLE_HW_ECHO(file, b)           fflush(file); \
                                                      fprintf(file, (b) ? "\x1B[E" : "\x1B]E"); \
                                                      fflush(file)

/** Clear the console screen
 *\param[in] file the console file handle */
#define _LIBSERVER_CONSOLE_CLEAR(file)                fflush(file); \
                                                      fprintf(file, "\x1B[C"); \
                                                      fflush(file)

/** Enable/Disable the console cursor
 *\param[in] file the console file handle
 *\param[in] b    true to enable, false to disable */
#define _LIBSERVER_CONSOLE_CURSOR(file, b)            fflush(file); \
                                                      fprintf(file, (b) ? "\x1B[V1" : "\x1B[V0"); \
                                                      fflush(file)

/** Set the console cursor position
 *\param[in] file the console file handle
 *\param[in] x    the x-coordinate of the cursor
 *\param[in] y    the y-coordinate of the cursor */
#define _LIBSERVER_CONSOLE_SET_CURSOR(file, x, y)     fflush(file); \
                                                      fprintf(file, "\x1B]C%i;%i", (int)(x), (int)(y)); \
                                                      fflush(file);



/*
 * Functions
 */

#ifdef __cplusplus
  extern "C"
  {
#endif

  /** Convert a lightOS keycode to a ECMA-48 character sequence
   *\param[in]     key    the lightOS keycode
   *\param[in,out] output pointer to the output character sequence
   *\param[in]     size   size of the output character sequence
   */
  void _LIBSERVER_convert_to_ecma48(_LIBSERVER_key_t key,
                                    char* output,
                                    size_t size);

  /** Convert an ECMA-48 character sequence into a lightOS keycode
   *\param[in]     input  the ECMA-48 character sequence
   *\param[in]     length length of the character sequence
   *\param[in,out] key    the lightOS keycode
   *\return _LIBSERVER_CONVERT_OK, if the conversion was successfull,
   *        _LIBSERVER_CONVERT_NEED_INPUT if we need more input and
   *        _LIBSERVER_CONVERT_FAILED if the conversion failed
   */
  int _LIBSERVER_convert_from_ecma48(const char* input,
                                     size_t length,
                                     _LIBSERVER_key_t* key);

#ifdef __cplusplus
  }
#endif



/*@}*/

#endif
