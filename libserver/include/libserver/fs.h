/*
lightOS libserver
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBSERVER_FS_H
#define LIGHTOS_LIBSERVER_FS_H

#include <stddef.h>
#ifndef __cplusplus
  #include <stdbool.h>
#endif
#include <libkernel/type.h>

/*! \addtogroup libserver libserver */
/*@{*/

// NOTE: keep in sync with lib/libc/include/libOS/lightOS/LIBC_glue.h
#define _LIBSERVER_FILENAME_MAX        1024

#define _LIBSERVER_FILE_ACCESS_INFO    0x01
#define _LIBSERVER_FILE_ACCESS_READ    0x02
#define _LIBSERVER_FILE_ACCESS_WRITE   0x04
#define _LIBSERVER_FILE_ACCESS_MASK    (_LIBSERVER_FILE_ACCESS_INFO | \
                                        _LIBSERVER_FILE_ACCESS_READ | \
                                        _LIBSERVER_FILE_ACCESS_WRITE)

#define _LIBSERVER_FILE_ATTRIBUTE_TTY  0x08
#define _LIBSERVER_FILE_ATTRIBUTE_MASK (_LIBSERVER_FILE_ATTRIBUTE_TTY)

#define _LIBSERVER_FILE_TYPE_NONE      0x00
#define _LIBSERVER_FILE_TYPE_BLOCK     0x01
#define _LIBSERVER_FILE_TYPE_STREAM    0x02
#define _LIBSERVER_FILE_TYPE_DIRECTORY 0x03

typedef struct
{
  char name[_LIBSERVER_FILENAME_MAX + 1];
} _LIBSERVER_file_name_info;

typedef struct
{
  char name[_LIBSERVER_FILENAME_MAX + 1];
  _LIBKERNEL_message_param_t type;
  _LIBKERNEL_message_param_t blocksize;
  _LIBKERNEL_message_param_t blockcount;
} _LIBSERVER_file_info;

#ifdef __cplusplus
  extern "C"
  {
#endif

    _LIBKERNEL_port_id_t _LIBSERVER_get_fs_from_name(_LIBKERNEL_port_id_t port,
                                                     const char *name,
                                                     char **fsname,
                                                     void *(*malloc)(size_t));
    bool _LIBSERVER_file_exists(_LIBKERNEL_port_id_t port,
                                _LIBKERNEL_port_id_t fsport,
                                const char *relname);
    bool _LIBSERVER_create_file(_LIBKERNEL_port_id_t port,
                                _LIBKERNEL_port_id_t fsport,
                                const char *relname,
                                _LIBKERNEL_message_param_t type,
                                _LIBKERNEL_message_param_t flags,
                                _LIBKERNEL_message_param_t blocksize,
                                _LIBKERNEL_message_param_t blockcount);
    bool _LIBSERVER_open_file(_LIBKERNEL_port_id_t port,
                              _LIBKERNEL_port_id_t fsport,
                              const char *relname,
                              _LIBKERNEL_message_param_t flags);

#ifdef __cplusplus
  }
#endif

/*@}*/

#endif
