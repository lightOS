/*
lightOS libserver
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBSERVER_KEYBOARD_H
#define LIGHTOS_LIBSERVER_KEYBOARD_H

#include <stddef.h>
#include <libarch/type.h>

/*! \addtogroup libserver libserver */
/*@{*/



/*
 * Macros
 * NOTE: Also change libserver/console.c
 */

#define _LIBSERVER_KEYCODE_NONE                  0x000
#define _LIBSERVER_KEYCODE_ESC                   0x001
#define _LIBSERVER_KEYCODE_ARROW_UP              0x002
#define _LIBSERVER_KEYCODE_ARROW_DOWN            0x003
#define _LIBSERVER_KEYCODE_ARROW_LEFT            0x004
#define _LIBSERVER_KEYCODE_ARROW_RIGHT           0x005
#define _LIBSERVER_KEYCODE_PAGE_UP               0x006
#define _LIBSERVER_KEYCODE_PAGE_DOWN             0x007
#define _LIBSERVER_KEYCODE_POS1                  0x008
#define _LIBSERVER_KEYCODE_END                   0x009
#define _LIBSERVER_KEYCODE_ERASE                 0x00A
#define _LIBSERVER_KEYCODE_INSERT                0x00B
#define _LIBSERVER_KEYCODE_F0                    0x00C
#define _LIBSERVER_KEYCODE_F1                    0x00D
#define _LIBSERVER_KEYCODE_F2                    0x00E
#define _LIBSERVER_KEYCODE_F3                    0x00F
#define _LIBSERVER_KEYCODE_F4                    0x010
#define _LIBSERVER_KEYCODE_F5                    0x011
#define _LIBSERVER_KEYCODE_F6                    0x012
#define _LIBSERVER_KEYCODE_F7                    0x013
#define _LIBSERVER_KEYCODE_F8                    0x014
#define _LIBSERVER_KEYCODE_F9                    0x015
#define _LIBSERVER_KEYCODE_F10                   0x016
#define _LIBSERVER_KEYCODE_F11                   0x017
#define _LIBSERVER_KEYCODE_F12                   0x018
#define _LIBSERVER_KEYCODE_F13                   0x019
#define _LIBSERVER_KEYCODE_F14                   0x01A
#define _LIBSERVER_KEYCODE_F15                   0x01B
#define _LIBSERVER_KEYCODE_F16                   0x01C
#define _LIBSERVER_KEYCODE_NUMPAD_POS1           0x01D
#define _LIBSERVER_KEYCODE_NUMPAD_END            0x01E
#define _LIBSERVER_KEYCODE_NUMPAD_PAGE_UP        0x01F
#define _LIBSERVER_KEYCODE_NUMPAD_PAGE_DOWN      0x020
#define _LIBSERVER_KEYCODE_NUMPAD_CENTER         0x021

#define _LIBSERVER_KEYCODE_MAX                   0x021

#define _LIBSERVER_KEYCODE_USERDEF               0x100



/*
 * Types
 */

typedef _LIBARCH_uint32_t _LIBSERVER_keycode_t;

typedef struct
{
  wchar_t              character;
  _LIBSERVER_keycode_t code;
} _LIBSERVER_key_t;



/*@}*/

#endif
