/*
lightOS libserver
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBSERVER_CONSOLE_HPP
#define LIGHTOS_LIBSERVER_CONSOLE_HPP

#include <string>
#include <libserver/console.h>
#include <libserver/keyboard.hpp>

/*! \addtogroup libserver libserver */
/*@{*/

namespace libserver
{
  inline std::string convert_to_ecma48(libserver::key_t key);

  std::string convert_to_ecma48(libserver::key_t key)
  {
    char tmp[10];
    _LIBSERVER_convert_to_ecma48(key, tmp, 10);
    return std::string(tmp);
  }
}

/*@}*/

#endif
