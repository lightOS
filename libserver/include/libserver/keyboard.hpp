/*
lightOS libserver
Copyright (C) 2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef LIGHTOS_LIBSERVER_KEYBOARD_HPP
#define LIGHTOS_LIBSERVER_KEYBOARD_HPP

#include <libserver/keyboard.h>

/*! \addtogroup libserver libserver */
/*@{*/

namespace libserver
{
  enum keycode : _LIBSERVER_keycode_t
  {
    none                               = _LIBSERVER_KEYCODE_NONE,
    escape                             = _LIBSERVER_KEYCODE_ESC,
    arrow_up                           = _LIBSERVER_KEYCODE_ARROW_UP,
    arrow_down                         = _LIBSERVER_KEYCODE_ARROW_DOWN,
    arrow_left                         = _LIBSERVER_KEYCODE_ARROW_LEFT,
    arrow_right                        = _LIBSERVER_KEYCODE_ARROW_RIGHT,
    page_up                            = _LIBSERVER_KEYCODE_PAGE_UP,
    page_down                          = _LIBSERVER_KEYCODE_PAGE_DOWN,
    pos1                               = _LIBSERVER_KEYCODE_POS1,
    end                                = _LIBSERVER_KEYCODE_END,
    erase                              = _LIBSERVER_KEYCODE_ERASE,
    insert                             = _LIBSERVER_KEYCODE_INSERT,

    /* Function keys */
    f0                                 = _LIBSERVER_KEYCODE_F0,
    f1                                 = _LIBSERVER_KEYCODE_F1,
    f2                                 = _LIBSERVER_KEYCODE_F2,
    f3                                 = _LIBSERVER_KEYCODE_F3,
    f4                                 = _LIBSERVER_KEYCODE_F4,
    f5                                 = _LIBSERVER_KEYCODE_F5,
    f6                                 = _LIBSERVER_KEYCODE_F6,
    f7                                 = _LIBSERVER_KEYCODE_F7,
    f8                                 = _LIBSERVER_KEYCODE_F8,
    f9                                 = _LIBSERVER_KEYCODE_F9,
    f10                                = _LIBSERVER_KEYCODE_F10,
    f11                                = _LIBSERVER_KEYCODE_F11,
    f12                                = _LIBSERVER_KEYCODE_F12,
    f13                                = _LIBSERVER_KEYCODE_F13,
    f14                                = _LIBSERVER_KEYCODE_F14,
    f15                                = _LIBSERVER_KEYCODE_F15,
    f16                                = _LIBSERVER_KEYCODE_F16,

    /* Numpad keys */
    numpad_pos1                        = _LIBSERVER_KEYCODE_NUMPAD_POS1,
    numpad_end                         = _LIBSERVER_KEYCODE_NUMPAD_END,
    numpad_page_up                     = _LIBSERVER_KEYCODE_NUMPAD_PAGE_UP,
    numpad_page_down                   = _LIBSERVER_KEYCODE_NUMPAD_PAGE_DOWN,
    numpad_center                      = _LIBSERVER_KEYCODE_NUMPAD_CENTER
  };

  typedef _LIBSERVER_key_t key_t;
}

/*@}*/

#endif
