/*
lightOS libserver
Copyright (C) 2006-2009 Jörg Pfähler

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <libkernel/kernel.h>
#include <libkernel/type_noprefix.h>
#include <libkernel/message_noprefix.h>
#include <libserver/fs_noprefix.h>

// TODO: This is a HACK
size_t strlen(const char * s)
{
  size_t rc = 0;
  while (s[rc])
  {
    ++rc;
  }
  return rc;
}
char *strcpy(char *s1, const char *s2)
{
  char * rc = s1;
  while ((*s1++ = *s2++));
  return rc;
}
char *strncpy(char *s1, const char *s2, size_t n)
{
  char * rc = s1;
  while (( n > 0) && (*s1++ = *s2++))
  {
    --n;
  }
  if (n > 0)
  {
    while (--n)
    {
      *s1++ = '\0';
    }
  }
  return rc;
}


port_id_t _LIBSERVER_get_fs_from_name(port_id_t port,
                                      const char *name,
                                      char **fsname,
                                      void *(*malloc)(size_t))
{
  shared_memory_t shm = _LIBKERNEL_create_shared_memory(sizeof(file_name_info));
  file_name_info *info = shm.address;
  strncpy(info->name, name, FILENAME_MAX);

  message_t msg = _LIBKERNEL_create_message_shm(PORT_VFS,
                                                MSG_VFS_FIND_FS,
                                                0,
                                                &shm,
                                                SHM_TRANSFER_OWNERSHIP);
  _LIBKERNEL_send_message(port, &msg);
  message_t reply = _LIBKERNEL_create_message(PORT_VFS,
                                              MSG_VFS_FIND_FS,
                                              0,
                                              0,
                                              0);
  _LIBKERNEL_add_wait_message(port, &reply);
  _LIBKERNEL_wait_message(&reply);

  if (_LIBKERNEL_get_message_attribute(&reply) == SHM_TRANSFER_OWNERSHIP)
  {
    shared_memory_t shared = _LIBKERNEL_get_shared_memory(&reply);
    file_name_info *tmp = shared.address;
    *fsname = malloc(strlen(tmp->name) + 1);
    strcpy(*fsname, tmp->name);
    _LIBKERNEL_destroy_shared_memory(&shared);
  }
  else
  {
    *fsname = malloc(strlen(&name[reply.param2]) + 1);
    strcpy(*fsname, &name[reply.param2]);
  }
  _LIBKERNEL_destroy_shared_memory(&shm);
  return reply.param1;
}

bool _LIBSERVER_file_exists(port_id_t port,
                            port_id_t fsport,
                            const char *relname)
{
  shared_memory_t shm = _LIBKERNEL_create_shared_memory(sizeof(file_name_info));
  strncpy(shm.address, relname, FILENAME_MAX);

  message_t msg = _LIBKERNEL_create_message_shm(fsport,
                                                MSG_FS_EXISTS_FILE,
                                                0,
                                                &shm,
                                                SHM_TRANSFER_OWNERSHIP);
  _LIBKERNEL_send_message(port, &msg);
  message_t reply = _LIBKERNEL_create_message(fsport,
                                              MSG_FS_EXISTS_FILE,
                                              0,
                                              0,
                                              0);
  _LIBKERNEL_add_wait_message(port, &reply);
  _LIBKERNEL_wait_message(&reply);
  return reply.param1;
}

bool _LIBSERVER_create_file(port_id_t port,
                            port_id_t fsport,
                            const char *relname,
                            message_param_t type,
                            message_param_t flags,
                            message_param_t blocksize,
                            message_param_t blockcount)
{
  shared_memory_t shm = _LIBKERNEL_create_shared_memory(sizeof(file_info));
  file_info *info = shm.address;
  strncpy(info->name, relname, FILENAME_MAX);
  info->type = type;
  info->blocksize = blocksize;
  info->blockcount = blockcount;

  message_t msg = _LIBKERNEL_create_message_shm(fsport,
                                                MSG_FS_CREATE_FILE,
                                                flags,
                                                &shm,
                                                SHM_TRANSFER_OWNERSHIP);
  _LIBKERNEL_send_message(port, &msg);
  message_t reply = _LIBKERNEL_create_message(fsport,
                                              MSG_FS_CREATE_FILE,
                                              0,
                                              0,
                                              0);
  _LIBKERNEL_add_wait_message(port, &reply);
  _LIBKERNEL_wait_message(&reply);
  return reply.param1;
}

bool _LIBSERVER_open_file(port_id_t port,
                          port_id_t fsport,
                          const char *relname,
                          message_param_t flags)
{
  shared_memory_t shm = _LIBKERNEL_create_shared_memory(sizeof(file_info));
  _LIBSERVER_file_info *info = shm.address;
  strncpy(info->name, relname, FILENAME_MAX);

  message_t msg = _LIBKERNEL_create_message_shm(fsport,
                                                MSG_FS_OPEN_FILE,
                                                flags,
                                                &shm,
                                                SHM_TRANSFER_OWNERSHIP);
  _LIBKERNEL_send_message(port, &msg);
  message_t reply = _LIBKERNEL_create_message(fsport,
                                              MSG_FS_OPEN_FILE,
                                              0,
                                              0,
                                              0);
  _LIBKERNEL_add_wait_message(port, &reply);
  _LIBKERNEL_wait_message(&reply);
  if (reply.param1 == 0)
    return false;
  return true;
}
